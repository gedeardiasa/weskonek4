<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_activity','',TRUE);
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['typetoolbar']='ACTIVITY';
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('activity',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_activity->GetAllData();
			$data['listgl']=$this->m_coa->GetAllDataLevel5();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "add":
				if ($data['crudaccess']->intCreate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['UoM'] = isset($_POST['UoM'])?$_POST['UoM']:''; // get the requested page
				$data['Gl'] = isset($_POST['Gl'])?$_POST['Gl']:''; // get the requested page
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Code']);
				$cek=$this->m_activity->insert($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			case "edit":
				if ($data['crudaccess']->intUpdate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['UoM'] = isset($_POST['UoM'])?$_POST['UoM']:''; // get the requested page
				$data['Gl'] = isset($_POST['Gl'])?$_POST['Gl']:''; // get the requested page
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
				$cek=$this->m_activity->edit($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successedit';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroredit';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
	function prosesdelete()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$id=$_POST['ID2'];
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_activity->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
	function prosesupdateplanrate(){
		$data['activity'] = isset($_POST['activity'])?$_POST['activity']:''; // get the requested page
		$data['month'] = isset($_POST['month'])?$_POST['month']:''; // get the requested page
		$data['year'] = isset($_POST['year'])?$_POST['year']:''; // get the requested page
		$data['cost'] = isset($_POST['cost'])?$_POST['cost']:''; // get the requested page

		$this->m_activity->insertupdateplanrate($data);
	}
	function tablerate()
	{
		if($_GET['year']>2021 and $_GET['year']<date('Y')+2)
		{
			echo '
			<br>
				<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
					<thead>
					<tr>
					<th>No</th>
					<th>Month</th>
					<th>Plan Activity</th>
					<th>Actual Activity</th>
					</tr>
					</thead>
					<tbody>
			';
			for($i=1;$i<=12;$i++){
				echo '<tr>
					<td>'.$i.'</td>
					<td>'.date('F',strtotime('28-'.$i.'-2000')).'</td>
				';

				$getdata = $this->m_activity->getratebyactmthyear($_GET['activity'],$_GET['year'],$i);
				echo '
					<td><input type="number" value="'.$getdata->cost.'" id="planid'.$i.'"></td>
					<td><input type="number" value="" id="actualid'.$i.'" readonly="true" style="background-color:#E7E7E7"></td>
				</tr>';
			}
			echo "
					</tbody>
				</table><br>
				<div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\" id=\"successchangeplanrate\" style=\"display:none\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                  </button>
                  <strong>".$this->lang->line("Success")."!</strong> you have successfully changed the plan rate.
            	</div>
				<button type=\"button\" class=\"btn btn-primary\" id=\"buttonupdateplanrate\" onclick=\"updateplanrate()\">Update</button>
				";
			echo '
			
			<script>
			function updateplanrate(){
				';
			for($i=1;$i<=12;$i++){
				echo '
				var cost = $("#planid'.$i.'").val();
				$.ajax({ 
					type: "POST",
					url: "'.base_url().'index.php/'.$this->uri->segment(1).'/prosesupdateplanrate?", 
					data: "activity='.$_GET['activity'].'&month='.$i.'&year='.$_GET['year'].'&cost="+cost+"",
					cache: true, 
					success: function(data){
						
					},
				});
				';
			}
			echo '
				$("#successchangeplanrate").show();
			}
			';

			echo '
			$(function () {
				$("#successchangeplanrate").hide();
				$("#example3").DataTable({
					"searching": false,
					"paging":   false,
					"ordering": false,
					"info":     false
				});
			});
			
			
			</script>';
		}else{
			echo 'No Data';
		}
	}
}
