<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ar extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_ardp','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_jurnal','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_inpay','',TRUE);
		$this->load->model('m_group_cf','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_udf','',TRUE);
		$this->load->model('m_block','',TRUE);
		$this->load->model('m_batch','',TRUE);

		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			$listudf2 = $this->m_udf->getUDF2byName('AR');
			foreach($listudf2->result() as $udf2){
				unset($_SESSION['udf2'.$udf2->vcCode]);
			}
			
			$data['typetoolbar']='AR';
			$data['typeudf']='AR';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('A/R Invoice',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_ar->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listso']=$this->m_so->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listdn']=$this->m_dn->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwallet']=$this->m_wallet->GetAllDataWithPlanAccessandWalletAccess($_SESSION['IDPOS']);
			$data['listgl']=$this->m_coa->GetAllDataLevel5();
			$data['listgl4']=$this->m_coa->GetAllDataLevel5ByHead(4);
			$data['defaultAccount'] = $this->m_coa_setting->GetValue('pen_jasa');
			
			$data['listudf2'] = $this->m_udf->getUDF2byName('AR');
			
			$data['backdate']=$this->m_docnum->GetBackdate('AR');
			
			$data['listcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['listpricelist']=$this->m_bp->GetAllPriceList();
			
			$data['ar_custom_doc'] = $this->m_setting->getValueByCode('ar_custom_doc');
			$data['list_custom_doc']=$this->m_ar->GetDocCustomList();
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function addnewbp()
	{
		
		//general
		$data['Type'] = 'C';
		$data['Category'] = isset($_POST['Category'])?$_POST['Category']:''; // get the requested page
		$data['Code'] = $this->m_bp->GetLastCode('C');
		$data['Name'] = isset($_POST['BPNameNew'])?$_POST['BPNameNew']:''; // get the requested page
		$data['TaxNumber'] = isset($_POST['TaxNumber'])?$_POST['TaxNumber']:''; // get the requested page
		$data['PriceList'] = isset($_POST['PriceList'])?$_POST['PriceList']:''; // get the requested page
		$data['CreditLimit'] = '';
		$data['PaymentTerm'] = '';
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		//contact
		$data['Phone1'] = '';
		$data['Phone2'] = '';
		$data['Fax'] = '';
		$data['Email'] = '';
		$data['Website'] = '';
		$data['ContactPerson'] = '';
		
		//address
		$data['CityBillTo'] = '';
		$data['CountryBillTo'] = '';
		$data['AddressBillTo'] = '';
		$data['CityShipTo'] = '';
		$data['CountryShipTo'] = '';
		$data['AddressShipTo'] = '';
		
		//payment
		$data['DefWallet'] = 0;
		
		
		if($this->m_bp->getIDByName($data['Name'])!='')
		{
			echo 'DUP';
			exit();
		}
		$this->db->trans_begin();
		$cek=$this->m_bp->insert($data);
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'addbp',$data['Code']);
		$this->db->trans_complete();
		echo $data['Code'];
	}
	/*
	
		GET FUNCTION
	
	*/
	function getcustomdoc()
	{
		$DocType=$_POST['DocType'];
		
		echo $this->m_docnum->GetCustomLastDocNum('hAR',$DocType,'mardoctype');	
	}
	function getardp()
	{
		$bp=$_POST['BPCode'];
		$data=$this->m_ardp->GeteBalanceByBPCode($bp);
		echo $data;
	}
	function loadDD()
	{
		$id = $_POST['id'];
		$list = $this->m_ar->getDD($id);
		$i=0;
		$responce=new stdClass();
		foreach($list->result() as $l)
		{
			$responce->rows[$i]['name'] = $l->vcItemName;
			$responce->rows[$i]['val'] = $l->intValue;
			$responce->rows[$i]['account'] = $l->vcAccountCode;
			$i++;
		}
		echo json_encode($responce);
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ar->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			$responce->PPH2 = $this->m_ar->getdataPPH2($responce->DocNum);
			$responce->Account = $r->vcAccount;
			
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocType 		= $r->intDocType;
				$responce->DocNumCustom = $r->vcDocNumCustom;
			}
			else
			{
				$responce->DocType 		= 1;
				$responce->DocNumCustom = $this->m_docnum->GetCustomLastDocNum('hAR',$responce->DocType,'mardoctype');	
				
			}
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->DueDate = date('m/d/Y',strtotime($r->dtDueDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$responce->AppliedAmount = $r->intApplied;
			$responce->BalanceDue = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$business_type = $this->m_setting->getValueByCode('business_type');
			if($business_type==''){
				$responce->Service = '0';
			}else{
				$responce->Service = '1';
			}
			$responce->PPH2 = '0';
			$responce->Account = $this->m_coa_setting->GetValue('pen_jasa');
			
			$responce->DocType 		= 1;
			$responce->DocNumCustom = $this->m_docnum->GetCustomLastDocNum('hAR',$responce->DocType,'mardoctype');	
			
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->DueDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			$responce->AppliedAmount = 0;
			$responce->BalanceDue = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSQ()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			$responce->PPH2 = '0';
			$responce->Account = $this->m_coa_setting->GetValue('pen_jasa');
			
			$responce->DocType 		= 1;
			$responce->DocNumCustom = $this->m_docnum->GetCustomLastDocNum('hAR',$responce->DocType,'mardoctype');
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$payterm = $this->m_bp->getPaymentTerm($r->vcBPCode);
			$responce->DueDate = date('m/d/Y', strtotime($responce->DocDate. ' + '.$payterm.' days'));
			
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	function getDataHeaderSO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			$responce->PPH2 = '0';
			$responce->Account = $this->m_coa_setting->GetValue('pen_jasa');
			
			$responce->DocType 		= 1;
			$responce->DocNumCustom = $this->m_docnum->GetCustomLastDocNum('hAR',$responce->DocType,'mardoctype');
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$payterm = $this->m_bp->getPaymentTerm($r->vcBPCode);
			$responce->DueDate = date('m/d/Y', strtotime($responce->DocDate. ' + '.$payterm.' days'));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderDN()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_dn->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->PPH2 = '0';
			
			$responce->DocType 		= 1;
			$responce->DocNumCustom = $this->m_docnum->GetCustomLastDocNum('hAR',$responce->DocType,'mardoctype');
			
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$payterm = $this->m_bp->getPaymentTerm($r->vcBPCode);
			$responce->DueDate = date('m/d/Y', strtotime($responce->DocDate. ' + '.$payterm.' days'));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getduedate()
	{
		$payterm = $this->m_bp->getPaymentTerm($_POST['BPCode']);
		echo date('m/d/Y', strtotime($_POST['DocDate']. ' + '.$payterm.' days'));
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			$listudf2 = $this->m_udf->getUDF2byName('AR');
			foreach($listudf2->result() as $udf2){
				unset($_SESSION['udf2'.$udf2->vcCode]);
			}
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			$listudf2 = $this->m_udf->getUDF2byName('AR');
			foreach($listudf2->result() as $udf2){
				unset($_SESSION['udf2'.$udf2->vcCode]);
			}
			
			$r=$this->m_ar->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idAR'][$j]=$d->intID;
				$_SESSION['idBaseRefAR'][$j]=$d->intBaseRef;
				$_SESSION['BaseRefAR'][$j]=$d->vcBaseType;
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusAR'][$j]='O';
				}
				else
				{
					$_SESSION['statusAR'][$j]=$d->vcStatus;
				}
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$docnumforudf = $this->m_ar->GetHeaderByHeaderID($id)->vcDocNum;
					$dataudf2 = $this->m_udf->getdataudf2($udf2->intID,$docnumforudf,$d->intID);
					$_SESSION['udf2'.$udf2->vcCode][$j]=$dataudf2->vcValue;
				}
				$j++;
			}
			$_SESSION['totitemAR']=$j;
		}
	}
	function loaddetailsq()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		$listudf2 = $this->m_udf->getUDF2byName('AR');
		foreach($listudf2->result() as $udf2){
			unset($_SESSION['udf2'.$udf2->vcCode]);
		}
		
		$r=$this->m_sq->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='SQ';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = '';
				}
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loaddetailso()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		$listudf2 = $this->m_udf->getUDF2byName('AR');
		foreach($listudf2->result() as $udf2){
			unset($_SESSION['udf2'.$udf2->vcCode]);
		}
		
		$r=$this->m_so->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='SO';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = '';
				}
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loaddetaildn()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		$listudf2 = $this->m_udf->getUDF2byName('AR');
		foreach($listudf2->result() as $udf2){
			unset($_SESSION['udf2'.$udf2->vcCode]);
		}
		
		$r=$this->m_dn->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='DN';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				$_SESSION['uomAR'][$j]=$d->intUoMType;
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = '';
				}
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if(($_SESSION['itemcodeAR'][$j]!='' and $_SESSION['itemcodeAR'][$j]!=null) or ($_SESSION['itemnameAR'][$j]!='' and $_SESSION['itemnameAR'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyAR'][$j]*$_SESSION['priceAR'][$j])-($_SESSION['discAR'][$j]/100*($_SESSION['qtyAR'][$j]*$_SESSION['priceAR'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if(isset($_SESSION['itemcodeAR'][$j]) or isset($_SESSION['itemnameAR'][$j]))
			{
				if($_SESSION['itemcodeAR'][$j]!='' or $_SESSION['itemnameAR'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$hasil='';
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeAR'][$j]);
				if($_SESSION['uomAR'][$j]==1)// konversi uom
				{
					$da['qtyinvAR']=$_SESSION['qtyAR'][$j];
				}
				else if($_SESSION['uomAR'][$j]==2)
				{
					$da['qtyinvAR']=$this->m_item->convert_qty($item,$_SESSION['qtyAR'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomAR'][$j]==3)
				{
					$da['qtyinvAR']=$this->m_item->convert_qty($item,$_SESSION['qtyAR'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvAR'],$_SESSION['whsAR'][$j],$data['DocDate']);
				if($res==1 or $_SESSION['BaseRefAR'][$j]=='DN')
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeAR'][$j].' - '.$_SESSION['itemnameAR'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsAR'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSQ($id,$item,$qty,$qtyinv)
	{
		$this->m_sq->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseSO($id,$item,$qty,$qtyinv)
	{
		$this->m_so->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseDN($id,$item,$qty,$qtyinv)
	{
		$this->m_dn->cekclose($id,$item,$qty,$qtyinv);
	}
	function clearing()
	{
		$data['idHeader'] 	= isset($_POST['idHeader'])?$_POST['idHeader']:''; // get the requested page
		$data['AccountOP'] 	= isset($_POST['AccountOP'])?$_POST['AccountOP']:''; // get the requested page
		$ar					= $this->m_ar->GetHeaderByHeaderID($data['idHeader']);
		$balance			= $ar->intBalance;
		
		for($j=0;$j<$_SESSION['totitemAR'];$j++)// proses mencari data locationa
		{
			if($_SESSION['whsAR'][$j]!="" or $_SESSION['whsAR'][$j]!=null)
			{
				$data['Whs']=$_SESSION['whsAR'][$j];
			}
		}
		$this->db->trans_begin();
		$dataJurnal['Plan']		=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
		$dataJurnal['DocNum']	='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']	=date('Y-m-d');
		$dataJurnal['RefNum']	=$ar->vcDocNum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']	='Repayment';
		$dataJurnal['RefType']	='AR';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		
			$debact = $data['AccountOP'];
			$creact = $this->m_bp_category->GetArActByBPCode($ar->vcBPCode);
						
			$daJurnal['intHID']=$headJurnal;
			$daJurnal['DCJE']='D';
										
			$daJurnal['GLCodeJE']		=$debact;
			$daJurnal['GLNameJE']		=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']		=$creact;
			$daJurnal['GLNameJEX']		=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']		=$balance;
			
			$val_min=$daJurnal['ValueJE']*-1;
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
			//end jurnal entry
		
		$this->m_ar->cekclose($data['idHeader'],$balance); // set ar to close
		
		$this->db->trans_complete();
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		$data['Account'] = isset($_POST['Account'])?$_POST['Account']:''; // get the requested page
		$data['PPH2'] = isset($_POST['PPH2'])?$_POST['PPH2']:0; // get the requested page
		
		$data['DocType'] = isset($_POST['DocType'])?$_POST['DocType']:''; // get the requested page
		$data['DocNumCustom'] = isset($_POST['DocNumCustom'])?$_POST['DocNumCustom']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		$data['ARDP'] = isset($_POST['ARDP'])?$_POST['ARDP']:''; // get the requested page
		$data['DocTotalINPAY'] = isset($_POST['DocTotalINPAY'])?$_POST['DocTotalINPAY']:''; // get the requested page
		
		$data['DocTotal'] = $data['DocTotal'] + $data['ARDP'];
		
		$data['AmmountTendered'] = 0;
		$data['Change'] = 0;
		$data['PaymentNote'] = '';
		$data['PaymentCode'] = '';
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$batch = json_decode($_POST['batch']); // batch
		$this->db->trans_begin();
		$head=$this->m_ar->insertH($data);
		$data['DocTotalmin']=$data['DocTotal']*-1;
		//proses jurnal
		$headDocnum=$this->m_ar->GetHeaderByHeaderID($head)->vcDocNum;
		$headDocnumAR = $headDocnum;
		
		// PPH2 %
		$this->m_ar->insertPPH2($headDocnumAR,$data['PPH2']);
		if($data['PPH2']==1)
		{
			$pphvalue = 2/100*$data['DocTotal'];
			$dataarcm['DocNum'] = '';
			$dataarcm['BPCode'] = $data['BPCode'];
			$dataarcm['BPName'] = $data['BPName'];
			$dataarcm['RefNum'] = $headDocnumAR;
			$dataarcm['DocDate'] = $data['DocDate'];
			$dataarcm['DelDate'] = $data['DelDate'];
			$dataarcm['SalesEmp'] = $data['SalesEmp'];
			$dataarcm['Remarks'] = 'PPH 2 % dari '.$headDocnumAR.'';
			$dataarcm['Service'] = $data['Service'];
			
			$dataarcm['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
			$dataarcm['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
			
			$dataarcm['DocTotalBefore'] = $pphvalue;
			$dataarcm['DiscPer'] = 0;
			$dataarcm['Disc'] = 0;
			$dataarcm['Freight'] = 0;
			$dataarcm['TaxPer'] = 0;
			$dataarcm['Tax'] = 0;
			$dataarcm['DocTotal'] = $pphvalue;
			$dataarcm['AmmountTendered'] = 0;
			$dataarcm['Change'] = 0;
			$dataarcm['PaymentNote'] = '';
			$dataarcm['PaymentCode'] = '';
			
			$dataarcm['BPId']=$data['BPId'];
			
			$headarcm=$this->m_arcm->insertH($dataarcm);
			$headDocnumarcm=$this->m_arcm->GetHeaderByHeaderID($headarcm)->vcDocNum;
			$dataarcm['DocTotalmin']=$dataarcm['DocTotal']*-1;
			
			$daarcm['intHID']=$headarcm;
			$daarcm['itemID']=0;
			$daarcm['itemcodeARCM']='PPH 2 % dari '.$headDocnumAR.'';
			$daarcm['itemnameARCM']='PPH 2 % dari '.$headDocnumAR.'';
			$daarcm['qtyARCM'] = 1;
			// get whs data from detail AR
			for($j=0;$j<$_SESSION['totitemAR'];$j++)// save detail
			{
				$whsarcm=$this->m_location->GetNameByID($_SESSION['whsAR'][$j]);
				$whsarcmcode = $_SESSION['whsAR'][$j];
			}
			
			$daarcm['whsARCM']=$whsarcmcode;
			$daarcm['whsNameARCM']=$this->m_location->GetNameByID($whsarcmcode);
			$daarcm['uomtypeARCM']='INV';
			$daarcm['uomARCM']='INV';
			$daarcm['qtyinvARCM']=1;
			$daarcm['idBaseRefARCM']=0;
			$daarcm['BaseRefARCM']='';
			$daarcm['uominvARCM'] = 'INV';
			$daarcm['costARCM']=$pphvalue;
			
			$daarcm['priceARCM']= $pphvalue;
			$daarcm['discperARCM'] = 0;
			$daarcm['discARCM'] = 0;
			$daarcm['priceafterARCM']=$pphvalue;
			$daarcm['linetotalARCM']= $pphvalue;
			$daarcm['linecostARCM']=$pphvalue;
			$detailarcm=$this->m_arcm->insertD($daarcm);
			
			$dataJurnalarcm['Plan']=$this->m_location->GetIDPlanByLocationId($whsarcmcode);//Ambil id plan
			$dataJurnalarcm['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
			$dataJurnalarcm['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
			$dataJurnalarcm['RefNum']=$headDocnumarcm; // refnumber jurnal adalah docnumber transaksi
			$dataJurnalarcm['Remarks']='PPH 2 % dari '.$headDocnumAR.'';
			$dataJurnalarcm['RefType']='ARCM';
			$headJurnalarcm=$this->m_jurnal->insertH($dataJurnalarcm); // create jurnal
		
			$debact = $this->m_coa_setting->GetValue('pen_jasa');
			$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
			
			$daJurnal['intHID']=$headJurnalarcm;
			$daJurnal['DCJE']='D';
						
			$daJurnal['GLCodeJE']=$debact;
			$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']=$creact;
			$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']=$daarcm['linetotalARCM'];
			$val_min=$daJurnal['ValueJE']*-1;
						
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits*/
						
		}
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		for($j=0;$j<$_SESSION['totitemAR'];$j++)// proses mencari data locationa
		{
			if($_SESSION['whsAR'][$j]!="" or $_SESSION['whsAR'][$j]!=null)
			{
				$data['Whs']=$_SESSION['whsAR'][$j];
			}
		}
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']=$data['Remarks'];
		$dataJurnal['RefType']='AR';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		
		//$this->m_bp->updateBDO($data['BPId'],'intBalance',$data['DocTotal']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemAR'];$j++)// save detail
			{
				if($_SESSION['itemcodeAR'][$j]!="" or $_SESSION['itemnameAR'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameAR'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameAR'][$j]);
					}
					else
					{
						$item=$_SESSION['itemnameAR'][$j];
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsAR'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeAR']=$_SESSION['itemcodeAR'][$j];
					$da['itemnameAR']=$_SESSION['itemnameAR'][$j];
					$da['qtyAR']=$_SESSION['qtyAR'][$j];
					$da['whsAR']=$_SESSION['whsAR'][$j];
					$da['whsNameAR']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discAR'][$j])/100*$_SESSION['priceAR'][$j];
					
					if($_SESSION['uomAR'][$j]==1 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcUoM;
						$da['qtyinvAR']=$da['qtyAR'];
					}
					else if($_SESSION['uomAR'][$j]==2 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcSlsUoM;
						$da['qtyinvAR']=$this->m_item->convert_qty($item,$da['qtyAR'],'intSlsUoM',1);
					}
					else if($_SESSION['uomAR'][$j]==3 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcPurUoM;
						$da['qtyinvAR']=$this->m_item->convert_qty($item,$da['qtyAR'],'intPurUoM',1);
					}
					else
					{
						$da['uomAR']   = $_SESSION['uomAR'][$j];
						$da['qtyinvAR']= $da['qtyAR'];
					}
					
					if(isset($_SESSION['idBaseRefAR'][$j]))
					{
						$da['idBaseRefAR']=$_SESSION['idBaseRefAR'][$j];
						$da['BaseRefAR']=$_SESSION['BaseRefAR'][$j];
					}
					else
					{
						$da['idBaseRefAR']=0;
						$da['BaseRefAR']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefAR']=='SQ')
					{
						$this->cekcloseSQ($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					else if($da['BaseRefAR']=='SO')
					{
						$this->cekcloseSO($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					else if($da['BaseRefAR']=='DN')
					{
						$this->cekcloseDN($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					
					$da['uomtypeAR']=$_SESSION['uomAR'][$j];
					
					if($data['Service']==0)
					{
						$da['uominvAR']=$vcUoM->vcUoM;
						$da['costAR']=$this->m_stock->GetCostItem($item,$da['whsAR']);
					}
					else
					{
						$da['uominvAR'] = $da['uomAR'];
						$da['costAR']=0;// jika service maka tidak ada cost yang terbentuk
					}
					
					
					$da['priceAR']= $_SESSION['priceAR'][$j];
					$da['discperAR'] = $_SESSION['discAR'][$j];
					$da['discAR'] = $_SESSION['discAR'][$j]/100*$da['priceAR'];
					$da['priceafterAR']=(100-$_SESSION['discAR'][$j])/100*$da['priceAR'];
					$da['linetotalAR']= $da['priceafterAR']*$da['qtyAR'];
					$da['linecostAR']=$da['costAR']*$da['qtyinvAR'];
					
					$detail=$this->m_ar->insertD($da);
					
					// insert udf2
					$listudf2 = $this->m_udf->getUDF2byName('AR');
					foreach($listudf2->result() as $udf2){
						$this->m_udf->insertudf2($udf2->intID,$headDocnumAR,$detail,$_SESSION['udf2'.$udf2->vcCode][$j]);
					}
					
					//start jurnal AR
					if($data['Service']==1)// jika service
					{
						$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						//$creact = $this->m_coa_setting->GetValue('pen_jasa');
						$creact = $data['Account'];
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linetotalAR'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}		
					else
					{
						$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						$creact = $this->m_item_category->GetAccountRevByItemCode($da['itemcodeAR']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linetotalAR'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal AR
					
					$da['qtyinvAR']=$da['qtyinvAR']*-1;
					if($da['BaseRefAR']!='DN' and $data['Service']==0)
					{
						//start detail jurnal inventory jika base on dokumen bukan DN (karena harus menambah stock)
						$actinv = $this->m_item_category->GetAccountHppByItemCode($da['itemcodeAR']);
						$acthpp = $this->m_item_category->GetAccountByItemCode($da['itemcodeAR']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
					
						$daJurnal['GLCodeJE']=$actinv;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
						$daJurnal['GLCodeJEX']=$acthpp;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
						$daJurnal['ValueJE']=$da['linecostAR'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
						
						$this->m_stock->updateStock($item,$da['qtyinvAR'],$da['whsAR']);//update stok menambah/mengurangi di gudang
						$idmutasi = $this->m_stock->addMutation($item,$da['qtyinvAR'],$da['costAR'],$da['whsAR'],'AR',$data['DocDate'],$data['DocNum']);//add mutation
						//start batch
					
						$this->m_batch->BatchProcessingGI('AR',$item,$detail,$idmutasi,$batch,$da['whsAR']);
						
						//endbatch
					}
				}
			}
			// add DD
			$dName 		= isset($_POST['JddName'])?$_POST['JddName']:''; // get the requested page
			$dValue 	= isset($_POST['JddValue'])?$_POST['JddValue']:''; // get the requested page
			$dAccount 	= isset($_POST['JddAccount'])?$_POST['JddAccount']:''; // get the requested page
			
			$ddName		= json_decode($dName);
			$ddValue	= json_decode($dValue);
			$ddAccount	= json_decode($dAccount);
			for($j=0;$j<count($ddName);$j++)
			{
				$this->m_ar->insertDD($head,$ddName[$j],$ddValue[$j],$ddAccount[$j]);
				// create jurnal for DD
				$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
				$creact = $ddAccount[$j];
						
				$daJurnal['intHID']=$headJurnal;
				$daJurnal['DCJE']='D';
						
				$daJurnal['GLCodeJE']=$debact;
				$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
				$daJurnal['GLCodeJEX']=$creact;
				$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
				$daJurnal['ValueJE']=$ddValue[$j];
				$val_min=$daJurnal['ValueJE']*-1;
						
				$detailJurnal=$this->m_jurnal->insertD($daJurnal);
				$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
				$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
			}
		}
		//start jurnal AR (Diskon)
		if($data['Disc']>0)
		{
			$debact = $this->m_coa_setting->GetValue('pot_disc');
			$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							
			$daJurnal['intHID']=$headJurnal;
			$daJurnal['DCJE']='D';
							
			$daJurnal['GLCodeJE']=$debact;
			$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']=$creact;
			$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']=$data['Disc'];
			$val_min=$daJurnal['ValueJE']*-1;
							
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
		}
		//end jurnal AR (Diskon)
		
		//start jurnal AR (Ongkir)
		if($data['Freight']>0)
		{
			$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
			$creact = $this->m_coa_setting->GetValue('pen_ongkir');
							
			$daJurnal['intHID']=$headJurnal;
			$daJurnal['DCJE']='D';
							
			$daJurnal['GLCodeJE']=$debact;
			$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']=$creact;
			$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']=$data['Freight'];
			$val_min=$daJurnal['ValueJE']*-1;
							
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
		}
		//end jurnal AR (Ongkir)
		
		//start jurnal Ar (Tax)
		if($data['Tax']>0 or $data['Tax']<0)
		{
			$listcoatax = $this->m_tax->GetCoaTaxByRate($data['TaxPer']);
			foreach($listcoatax->result() as $lct)
			{
				$valcoatax 	= $lct->intRate/$data['TaxPer']*$data['Tax'];
				if($valcoatax>0)
				{
					$debact 	= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
					$creact 	= $lct->vcGLAR;
				}
				else
				{
					$creact 	= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
					$debact 	= $lct->vcGLAR;
					$valcoatax 	= $valcoatax*-1;
				}
				$daJurnal['intHID']=$headJurnal;
				$daJurnal['DCJE']='D';
								
				$daJurnal['GLCodeJE']=$debact;
				$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
				$daJurnal['GLCodeJEX']=$creact;
				$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
				$daJurnal['ValueJE']=$valcoatax;
				$val_min=$daJurnal['ValueJE']*-1;
								
				$detailJurnal=$this->m_jurnal->insertD($daJurnal);
				$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
				$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
			}
		}
		//end jurnal AR (Tax)
		
		if($data['ARDP']>0) // jika ada DP buat konfirmasi DP
		{
			$listdp = $this->m_ardp->GetDataByBPCode($data['BPCode']);
			foreach($listdp->result() as $ldp)
			{
				$ardp =  $this->m_ardp->GetHeaderByDocNum($ldp->vcDocNum);
				$ar   =  $this->m_ar->GetHeaderByDocNum($headDocnumAR);
				
				if($ardp->intBalance>$ar->intBalance)
				{
					$val = $ar->intBalance;
				}
				else
				{
					$val = $ardp->intBalance;
				}
				//$this->db->trans_begin();
				
				$this->m_ardp->cekclose($ardp->intID,$val);
				$this->m_ar->cekclose($ar->intID,$val);
				
				//createconfirm
		
				$dc['AR'] = $ar->intID;
				$dc['ARDP'] = $ardp->intID;
				$dc['date'] = date('Y-m-d');
				$dc['Applied'] = $val;
				$dc['Remarks'] = '';
				$idconfirm = $this->m_ardp->createConfirm($dc);
				
				//proses jurnal
				$headDocnum=$ardp->vcDocNum;
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'apply',$headDocnum);
				
				$dataJurnal['Plan']=$this->m_wallet->GetPlanByID($ardp->intWallet);//Ambil id plan
				$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
				$dataJurnal['DocDate']=date('Y-m-d');
				$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
				$dataJurnal['Remarks']='';
				$dataJurnal['RefType']='ARDP';
				$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
				
				
				$debact = $this->m_coa_setting->GetValue('uang_muka_jual');
				$creact = $this->m_bp_category->GetArActByBPCode($ardp->vcBPCode);
								
				$daJurnal['intHID']=$headJurnal;
				$daJurnal['DCJE']='D';
								
				$daJurnal['GLCodeJE']=$debact;
				$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
				$daJurnal['GLCodeJEX']=$creact;
				$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
				$daJurnal['ValueJE']=$val;
				$val_min=$daJurnal['ValueJE']*-1;
								
				$detailJurnal=$this->m_jurnal->insertD($daJurnal);
				$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
				$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits*/
			}
		}
		if($data['DocTotalINPAY']>0) // create INPAY jika doctotal inpay di isi
		{
			$_SESSION['totitemINPAY']=0;
			unset($_SESSION['docnumINPAY']);
			unset($_SESSION['idINPAY']);
			unset($_SESSION['idBaseRefINPAY']);
			unset($_SESSION['checkINPAY']);
			
			$j=0;
			$_SESSION['idINPAY'][$j]=0;
			$_SESSION['HIDINPAY'][$j]=0;
			$_SESSION['idBaseRefINPAY'][$j]=$head;
			$_SESSION['BaseRefINPAY'][$j]='AR';
			$_SESSION['docnumINPAY'][$j]=$headDocnum;
			$_SESSION['DocTotalINPAY'][$j]=$this->m_ar->GetHeaderByHeaderID($head)->intDocTotal;
			$_SESSION['AppliedINPAY'][$j]=$this->m_ar->GetHeaderByHeaderID($head)->intApplied;
			
			$j++;
			$_SESSION['totitemINPAY']=$j;
			
			//
			$data['DocTotalBefore'] = $data['DocTotal'];
			$data['AppliedAmount'] = $data['DocTotalINPAY'];
			$data['BalanceDue'] = $data['DocTotalBefore'] - $data['AppliedAmount'];
			$data['Wallet'] = isset($_POST['Wallet'])?$_POST['Wallet']:''; // get the requested page
			
			$sisa=$data['AppliedAmount'];
			
			$head=$this->m_inpay->insertH($data);
			
			$headDocnum=$this->m_inpay->GetHeaderByHeaderID($head)->vcDocNum;
			//proses jurnal
			
			$dataJurnal['Plan']=$this->m_wallet->GetPlanByID($data['Wallet']);//Ambil id plan
			$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
			$dataJurnal['DocDate']=date('Y-m-d');
			$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
			$dataJurnal['Remarks']='';
			$dataJurnal['RefType']='INPAY';
			$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
			
			if($head!=0)
			{
				for($j=0;$j<$_SESSION['totitemINPAY'];$j++)// save detail
				{
					if($_SESSION['docnumINPAY'][$j]!="")
					{
						$da['intHID']=$head;
						$da['intBaseRef']=$_SESSION['idBaseRefINPAY'][$j];
						$da['vcBaseType']=$_SESSION['BaseRefINPAY'][$j];
						$da['vcDocNum']=$_SESSION['docnumINPAY'][$j];
						$da['intDocTotal']=$_SESSION['DocTotalINPAY'][$j];
						$da['intBalance']=$_SESSION['DocTotalINPAY'][$j]-$_SESSION['AppliedINPAY'][$j];
							
						if($da['intBalance']<=$sisa)
						{
							$da['intApplied']=$da['intBalance'];
							$sisa=$sisa-$da['intBalance'];
						}
						else
						{
							if($sisa<=0)
							{
								$da['intApplied']=0;
							}
							else
							{
								$da['intApplied']=$sisa;
							}
							$sisa=$sisa-$da['intBalance'];
						}
						$detail=$this->m_inpay->insertD($da);
													
						//jurnal entry
						if($_SESSION['BaseRefINPAY'][$j]=='AR')
						{
							$data['BPCode'] = $this->m_ar->GetHeaderByHeaderID($_SESSION['idBaseRefINPAY'][$j])->vcBPCode;
							$debact 		= $this->m_wallet->GetGLByID($data['Wallet']);
							$creact 		= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							$daJurnal['ValueJE']=$da['intApplied'];
						}
								
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$val_min=$daJurnal['ValueJE']*-1;
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end jurnal entry
						
						$cf 	= $this->m_setting->getValueByCode('cus_group_cf');
						$idcf	= $this->m_group_cf->GetIDByCode($cf);
						
						$mutremarks = "Payment AR-".$da['vcDocNum']."";
						$this->m_wallet->addMutation($data['Wallet'],$data['DocDate'],$da['vcBaseType'],$da['intApplied'],$da['vcDocNum'],$headDocnum,$idcf,$mutremarks);// add mutation wallet
						$this->m_wallet->updateBalance($data['Wallet'],$da['intApplied']); // change wallet balance
						$da['intAppliedminus']=$da['intApplied']*-1;
						if($da['vcBaseType']=='AR')
						{
							$this->m_ar->cekclose($da['intBaseRef'],$da['intApplied']); // set ar to close
						}
						
					}
				}
			}
			$_SESSION['totitemINPAY']=0;
			unset($_SESSION['docnumINPAY']);
			unset($_SESSION['idINPAY']);
			unset($_SESSION['idBaseRefINPAY']);
			unset($_SESSION['checkINPAY']);
			
		}
		$cekblock = $this->m_block->cekblock($head,'AR');
			
		if($cekblock=='')
		{
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			
			$this->db->trans_complete();
			echo $headDocnumAR;
		}
		else
		{
			echo $cekblock;
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_ar->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_ar->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table hover">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
				  <th>Doc. Total</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="right">'.number_format($d->intDocTotal,0).'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
			<tfoot>
            <tr>
                <th colspan="5" style="text-align:right">Total:</th>
                <th style="text-align:right; padding-right:9px"></th>
				<th></th>
            </tr>
        </tfoot>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"footerCallback": function ( row, data, start, end, display ) {
					var api = this.api(), data;
		 
					// Remove the formatting to get integer data for summation
					var intVal = function ( i ) {
						return typeof i === \'string\' ?
							i.replace(/[\$,]/g, \'\')*1 :
							typeof i === \'number\' ?
								i : 0;
					};
		 
					// Total over all pages
					total = api
						.column( 5 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
		 
					// Total over this page
					pageTotal = api
						.column( 5, { page: \'current\'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
					pageTotalI = pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 
					// Update footer
					$( api.column( 5 ).footer() ).html(
						\'\'+pageTotalI +\'\'
					);
				},
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_ar->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemAR'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]==$code and $code!=null and $_POST['Service']==0)
			{
				$_SESSION['qtyAR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$j]=$_POST['detailQty'];
				$_SESSION['uomAR'][$j]=$_POST['detailUoM'];
				$_SESSION['priceAR'][$j]=$_POST['detailPrice'];
				$_SESSION['discAR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$j]=$_POST['detailWhs'];
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = $_POST['udf2'.$udf2->vcCode];
				}
				//$_SESSION['BaseRefAR'][$j]='';
				$updateqty=1;
			}
			/*if($_POST['Service']==1 and $_SESSION['itemnameAR'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyAR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$j]=$_POST['detailQty'];
				$_SESSION['uomAR'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceAR'][$j]=$_POST['detailPrice'];
				$_SESSION['discAR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$j]=$_POST['detailWhs'];
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = $_POST['udf2'.$udf2->vcCode];
				}
				
				//$_SESSION['BaseRefAR'][$j]='';
				$updateqty=1;
			}*/
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeAR'][$i]=$code;
				$_SESSION['itemnameAR'][$i]=$_POST['detailItem'];
				$_SESSION['qtyAR'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$i]=$_POST['detailQty'];
				if($_POST['Service']==1)
				{
					$_SESSION['uomAR'][$i]=$_POST['detailUoMS'];
				}else
				{
					$_SESSION['uomAR'][$i]=$_POST['detailUoM'];
				}
				$_SESSION['priceAR'][$i]=$_POST['detailPrice'];
				$_SESSION['discAR'][$i]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$i]=$_POST['detailWhs'];
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$i] = $_POST['udf2'.$udf2->vcCode];
				}
				$_SESSION['BaseRefAR'][$i]='';
				$_SESSION['statusAR'][$i]='O';
				$_SESSION['totitemAR']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeAR'][$i]='';
					$_SESSION['itemnameAR'][$i]=$_POST['detailItem'];
					$_SESSION['qtyAR'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenAR'][$i]=$_POST['detailQty'];
					$_SESSION['uomAR'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceAR'][$i]=$_POST['detailPrice'];
					$_SESSION['discAR'][$i]=$_POST['detailDisc'];
					$_SESSION['whsAR'][$i]=$_POST['detailWhs'];
					$listudf2 = $this->m_udf->getUDF2byName('AR');
					foreach($listudf2->result() as $udf2){
						$_SESSION['udf2'.$udf2->vcCode][$i] = $_POST['udf2'.$udf2->vcCode];
					}
					$_SESSION['BaseRefAR'][$i]='';
					$_SESSION['statusAR'][$i]='O';
					$_SESSION['totitemAR']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeAR'][$j]="";
				$_SESSION['itemnameAR'][$j]="";
				$_SESSION['qtyAR'][$j]="";
				$_SESSION['qtyOpenAR'][$j]="";
				$_SESSION['uomAR'][$j]="";
				$_SESSION['priceAR'][$j]="";
				$_SESSION['discAR'][$j]="";
				$_SESSION['whsAR'][$j]="";
				$_SESSION['BaseRefAR'][$j]='';
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = "";
				}
			}
			if($_SESSION['itemnameAR'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeAR'][$j]="";
				$_SESSION['itemnameAR'][$j]="";
				$_SESSION['qtyAR'][$j]="";
				$_SESSION['qtyOpenAR'][$j]="";
				$_SESSION['uomAR'][$j]="";
				$_SESSION['priceAR'][$j]="";
				$_SESSION['discAR'][$j]="";
				$_SESSION['whsAR'][$j]="";
				$_SESSION['BaseRefAR'][$j]='';
				$listudf2 = $this->m_udf->getUDF2byName('AR');
				foreach($listudf2->result() as $udf2){
					$_SESSION['udf2'.$udf2->vcCode][$j] = "";
				}
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		$listudf2 = $this->m_udf->getUDF2byName('AR');
		foreach($listudf2->result() as $udf2){
			echo '<th>'.$udf2->vcName.'</th>';
		}
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemAR'];$j++)
			{
				if($_SESSION['itemnameAR'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAR'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameAR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsAR'][$j]);
					$cekbatch = $this->m_item->cekbatch($item);
					
					if($_SESSION['uomAR'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomAR'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomAR'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomAR'][$j];
					}
					
					if($_SESSION['discAR'][$j]=='')
					{
						$_SESSION['discAR'][$j]=0;
					}
					
					if($_SESSION['statusAR'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discAR'][$j])/100)*$_SESSION['priceAR'][$j]*$_SESSION['qtyAR'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeAR'][$j].'</td>
						<td>'.$_SESSION['itemnameAR'][$j].'</td>
					';
					if($cekbatch==1 and $_SESSION['BaseRefAR'][$j]!='DN')
					{
						echo '
							<td align="right"><a href="#" onclick="changebatch(\''.$item.'\',\''.$_SESSION["qtyAR"][$j].'\',\''.$_SESSION["whsAR"][$j].'\')">'.number_format($_SESSION['qtyAR'][$j],'2').'</a></td>
						';
					}
					else
					{
						echo '
						<td align="right">'.number_format($_SESSION['qtyAR'][$j],'2').'</td>';
					}
					

					echo '
						<td align="right">'.number_format($_SESSION['qtyOpenAR'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceAR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discAR'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					$listudf2 = $this->m_udf->getUDF2byName('AR');
					$editfunction = '';
					foreach($listudf2->result() as $udf2){
						echo '<td>'.$_SESSION['udf2'.$udf2->vcCode][$j].'</td>';
						$editfunction = $editfunction.",'".str_replace("'","\'",$_SESSION['udf2'.$udf2->vcCode][$j])."'";
					}
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusAR'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnameAR"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodeAR"][$j]).'\',\''.$_SESSION["qtyAR"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomAR"][$j]).'\',\''.str_replace("'","\'",$viewUoM).'\',\''.$_SESSION["priceAR"][$j].'\',\''.$_SESSION["discAR"][$j].'\',\''.str_replace("'","\'",$_SESSION["whsAR"][$j]).'\''.$editfunction.')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodeAR"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemnameAR"][$j]).'\',\''.$_SESSION["qtyAR"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
