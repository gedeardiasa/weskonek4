<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arcm extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_sr','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_jurnal','',TRUE);
		$this->load->model('m_udf','',TRUE);
		$this->load->model('m_block','',TRUE);
		$this->load->model('m_batch','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemARCM']=0;
			unset($_SESSION['itemcodeARCM']);
			unset($_SESSION['idARCM']);
			unset($_SESSION['idBaseRefARCM']);
			unset($_SESSION['itemnameARCM']);
			unset($_SESSION['qtyARCM']);
			unset($_SESSION['qtyOpenARCM']);
			unset($_SESSION['uomARCM']);
			unset($_SESSION['priceARCM']);
			unset($_SESSION['discARCM']);
			unset($_SESSION['whsARCM']);
			unset($_SESSION['statusARCM']);
			
			$data['typetoolbar']='ARCM';
			$data['typeudf']='ARCM';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('A/R Credit Memo',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_arcm->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listar']=$this->m_ar->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listsr']=$this->m_sr->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['backdate']=$this->m_docnum->GetBackdate('ARCM');
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_arcm->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hARCM');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->DueDate = date('m/d/Y',strtotime($r->dtDueDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$responce->AppliedAmount = $r->intApplied;
			$responce->BalanceDue = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hARCM');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->DueDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';

			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			$responce->AppliedAmount = 0;
			$responce->BalanceDue = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSR()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sr->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hARCM');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$payterm = $this->m_bp->getPaymentTerm($r->vcBPCode);
			$responce->DueDate = date('m/d/Y', strtotime($responce->DocDate. ' + '.$payterm.' days'));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	function getDataHeaderAR()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ar->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hARCM');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$payterm = $this->m_bp->getPaymentTerm($r->vcBPCode);
			$responce->DueDate = date('m/d/Y', strtotime($responce->DocDate. ' + '.$payterm.' days'));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemARCM']=0;
			unset($_SESSION['itemcodeARCM']);
			unset($_SESSION['idARCM']);
			unset($_SESSION['idBaseRefARCM']);
			unset($_SESSION['itemnameARCM']);
			unset($_SESSION['qtyARCM']);
			unset($_SESSION['qtyOpenARCM']);
			unset($_SESSION['uomARCM']);
			unset($_SESSION['priceARCM']);
			unset($_SESSION['discARCM']);
			unset($_SESSION['whsARCM']);
			unset($_SESSION['statusARCM']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemARCM']=0;
			unset($_SESSION['itemcodeARCM']);
			unset($_SESSION['idARCM']);
			unset($_SESSION['idBaseRefARCM']);
			unset($_SESSION['itemnameARCM']);
			unset($_SESSION['qtyARCM']);
			unset($_SESSION['qtyOpenARCM']);
			unset($_SESSION['uomARCM']);
			unset($_SESSION['priceARCM']);
			unset($_SESSION['discARCM']);
			unset($_SESSION['whsARCM']);
			unset($_SESSION['statusARCM']);
			
			$r=$this->m_arcm->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idARCM'][$j]=$d->intID;
				$_SESSION['idBaseRefARCM'][$j]=$d->intBaseRef;
				$_SESSION['BaseRefARCM'][$j]=$d->vcBaseType;
				$_SESSION['itemcodeARCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameARCM'][$j]=$d->vcItemName;
				$_SESSION['qtyARCM'][$j]=$d->intQty;
				$_SESSION['qtyOpenARCM'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomARCM'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomARCM'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceARCM'][$j]=$d->intPrice;
				$_SESSION['discARCM'][$j]=$d->intDiscPer;
				$_SESSION['whsARCM'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusARCM'][$j]='O';
				}
				else
				{
					$_SESSION['statusARCM'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemARCM']=$j;
		}
	}
	function loaddetailsr()
	{
		$id=$_POST['id'];
		$_SESSION['totitemARCM']=0;
		unset($_SESSION['itemcodeARCM']);
		unset($_SESSION['idARCM']);
		unset($_SESSION['idBaseRefARCM']);
		unset($_SESSION['itemnameARCM']);
		unset($_SESSION['qtyARCM']);
		unset($_SESSION['qtyOpenARCM']);
		unset($_SESSION['uomARCM']);
		unset($_SESSION['priceARCM']);
		unset($_SESSION['discARCM']);
		unset($_SESSION['whsARCM']);
		unset($_SESSION['statusARCM']);
		
		$r=$this->m_sr->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefARCM'][$j]=$d->intHID;
				$_SESSION['BaseRefARCM'][$j]='SR';
				$_SESSION['itemcodeARCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameARCM'][$j]=$d->vcItemName;
				$_SESSION['qtyARCM'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenARCM'][$j]=$d->intOpenQty;
				$_SESSION['uomARCM'][$j]=$d->intUoMType;
				$_SESSION['priceARCM'][$j]=$d->intPrice;
				$_SESSION['discARCM'][$j]=$d->intDiscPer;
				$_SESSION['whsARCM'][$j]=$d->intLocation;
				$_SESSION['statusARCM'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemARCM']=$j;
	}
	function loaddetailar()
	{
		$id=$_POST['id'];
		$_SESSION['totitemARCM']=0;
		
		$r=$this->m_ar->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefARCM'][$j]=$d->intHID;
				$_SESSION['BaseRefARCM'][$j]='AR';
				$_SESSION['itemcodeARCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameARCM'][$j]=$d->vcItemName;
				$_SESSION['qtyARCM'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenARCM'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomARCM'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomARCM'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceARCM'][$j]=$d->intPrice;
				$_SESSION['discARCM'][$j]=$d->intDiscPer;
				$_SESSION['whsARCM'][$j]=$d->intLocation;
				$_SESSION['statusARCM'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemARCM']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if(($_SESSION['itemcodeARCM'][$j]!='' and $_SESSION['itemcodeARCM'][$j]!=null) or ($_SESSION['itemnameARCM'][$j]!='' and $_SESSION['itemnameARCM'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyARCM'][$j]*$_SESSION['priceARCM'][$j])-($_SESSION['discARCM'][$j]/100*($_SESSION['qtyARCM'][$j]*$_SESSION['priceARCM'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if(isset($_SESSION['itemcodeARCM'][$j]) or isset($_SESSION['itemnameARCM'][$j]))
			{
				if($_SESSION['itemcodeARCM'][$j]!='' or $_SESSION['itemnameARCM'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$hasil='';
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if($_SESSION['itemcodeARCM'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeARCM'][$j]);
				if($_SESSION['uomARCM'][$j]==1)// konversi uom
				{
					$da['qtyinvARCM']=$_SESSION['qtyARCM'][$j];
				}
				else if($_SESSION['uomARCM'][$j]==2)
				{
					$da['qtyinvARCM']=$this->m_item->convert_qty($item,$_SESSION['qtyARCM'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomARCM'][$j]==3)
				{
					$da['qtyinvARCM']=$this->m_item->convert_qty($item,$_SESSION['qtyARCM'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvARCM'],$_SESSION['whsARCM'][$j],$data['DocDate']);
				if($res==1 or $_SESSION['BaseRefARCM'][$j]=='DN')
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeARCM'][$j].' - '.$_SESSION['itemnameARCM'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if($_SESSION['itemcodeARCM'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsARCM'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSR($id,$item,$qty,$qtyinv)
	{
		$this->m_sr->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseAR($id,$item,$qty,$qtyinv)
	{
		$this->m_ar->cekclose2($id,$item,$qty,$qtyinv);
	}
	function cekcloseARCM($id,$item,$qty,$qtyinv)
	{
		$this->m_arcm->cekclose2($id,$item,$qty,$qtyinv);
	}
	
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		$data['AmmountTendered'] = 0;
		$data['Change'] = 0;
		$data['PaymentNote'] = '';
		$data['PaymentCode'] = '';
		$batch = json_decode($_POST['batch']); // batch
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$this->db->trans_begin();
		
		$head=$this->m_arcm->insertH($data);
		$data['DocTotalmin']=$data['DocTotal']*-1;
		
		//proses jurnal
		$headDocnum=$this->m_arcm->GetHeaderByHeaderID($head)->vcDocNum;
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)// proses mencari data locationa
		{
			if($_SESSION['whsARCM'][$j]!="" or $_SESSION['whsARCM'][$j]!=null)
			{
				$data['Whs']=$_SESSION['whsARCM'][$j];
			}
		}
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']=$data['Remarks'];
		$dataJurnal['RefType']='ARCM';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		
		//$this->m_bp->updateBDO($data['BPId'],'intBalance',$data['DocTotalmin']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemARCM'];$j++)// save detail
			{
				if($_SESSION['itemcodeARCM'][$j]!="" or $_SESSION['itemnameARCM'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameARCM'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameARCM'][$j]);
					}
					else
					{
						$item=$_SESSION['itemnameARCM'][$j];
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsARCM'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeARCM']=$_SESSION['itemcodeARCM'][$j];
					$da['itemnameARCM']=$_SESSION['itemnameARCM'][$j];
					$da['qtyARCM']=$_SESSION['qtyARCM'][$j];
					$da['whsARCM']=$_SESSION['whsARCM'][$j];
					$da['whsNameARCM']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discARCM'][$j])/100*$_SESSION['priceARCM'][$j];
					
					if($_SESSION['uomARCM'][$j]==1 and $data['Service']==0)
					{
						$da['uomARCM']=$vcUoM->vcUoM;
						$da['qtyinvARCM']=$da['qtyARCM'];
					}
					else if($_SESSION['uomARCM'][$j]==2 and $data['Service']==0)
					{
						$da['uomARCM']=$vcUoM->vcSlsUoM;
						$da['qtyinvARCM']=$this->m_item->convert_qty($item,$da['qtyARCM'],'intSlsUoM',1);
					}
					else if($_SESSION['uomARCM'][$j]==3 and $data['Service']==0)
					{
						$da['uomARCM']=$vcUoM->vcPurUoM;
						$da['qtyinvARCM']=$this->m_item->convert_qty($item,$da['qtyARCM'],'intPurUoM',1);
					}
					else
					{
						$da['uomARCM']   = $_SESSION['uomARCM'][$j];
						$da['qtyinvARCM']= $da['qtyARCM'];
					}
					
					if(isset($_SESSION['idBaseRefARCM'][$j]))
					{
						$da['idBaseRefARCM']=$_SESSION['idBaseRefARCM'][$j];
						$da['BaseRefARCM']=$_SESSION['BaseRefARCM'][$j];
					}
					else
					{
						$da['idBaseRefARCM']=0;
						$da['BaseRefARCM']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefARCM']=='SR')
					{
						$this->cekcloseSR($da['idBaseRefARCM'], $da['itemID'], $da['qtyARCM'], $da['qtyinvARCM']);
					}
					/*else if($da['BaseRefARCM']=='AR')
					{
						
						$this->cekcloseAR($da['idBaseRefARCM'], $da['itemID'], $da['qtyARCM'], $da['qtyinvARCM']);
						
					}*/
					
					$da['uomtypeARCM']=$_SESSION['uomARCM'][$j];
					if($data['Service']==0)
					{
						$da['uominvARCM']=$vcUoM->vcUoM;
						$da['costARCM']=$this->m_stock->GetCostItem($item,$da['whsARCM']);
					}
					else
					{
						$da['uominvARCM'] = $da['uomARCM'];
						$da['costARCM']=$hargasetelahdiskon;
					}
					
					
					$da['priceARCM']= $_SESSION['priceARCM'][$j];
					$da['discperARCM'] = $_SESSION['discARCM'][$j];
					$da['discARCM'] = $_SESSION['discARCM'][$j]/100*$da['priceARCM'];
					$da['priceafterARCM']=(100-$_SESSION['discARCM'][$j])/100*$da['priceARCM'];
					$da['linetotalARCM']= $da['priceafterARCM']*$da['qtyARCM'];
					$da['linecostARCM']=$da['costARCM']*$da['qtyinvARCM'];
					
					$detail=$this->m_arcm->insertD($da);
					//start jurnal ARCM
					if($data['Service']==1)// jika service
					{
						$debact = $this->m_coa_setting->GetValue('pen_jasa');
						$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linetotalARCM'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}		
					else
					{
						$debact = $this->m_item_category->GetAccountRetByItemCode($da['itemcodeARCM']);
						$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linetotalARCM'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal AR
					if($da['BaseRefARCM']!='SR' and $data['Service']==0)
					{
						//start detail jurnal inventory jika base on dokumen bukan SR (karena harus menambah stock)
						$actinv = $this->m_item_category->GetAccountByItemCode($da['itemcodeARCM']);
						$acthpp = $this->m_item_category->GetAccountHppByItemCode($da['itemcodeARCM']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
					
						$daJurnal['GLCodeJE']=$actinv;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
						$daJurnal['GLCodeJEX']=$acthpp;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
						$daJurnal['ValueJE']=$da['linecostARCM'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
						
						$this->m_stock->updateStock($item,$da['qtyinvARCM'],$da['whsARCM']);//update stok menambah/mengurangi di gudang
						$idmutasi = $this->m_stock->addMutation($item,$da['qtyinvARCM'],$da['costARCM'],$da['whsARCM'],'ARCM',$data['DocDate'],$headDocnum);//add mutation
						//start batch
					
						$this->m_batch->BatchProcessingGR('ARCM',$item,$detail,$idmutasi,$da['qtyARCM'],$data['Whs'],$batch);
						
						//endbatch
					}
					/*if($da['BaseRefARCM']=='AR')
					{
						$this->cekcloseARCM($head, $da['itemID'], $da['qtyARCM'], $da['qtyinvARCM']); // jika baseref ar maka arcm langsung close
					}*/
				}
			}
			
		}
		//start jurnal Arcm (Diskon)
		if($data['Disc']>0)
		{
			$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
			$creact = $this->m_coa_setting->GetValue('pot_disc');
							
			$daJurnal['intHID']=$headJurnal;
			$daJurnal['DCJE']='D';
							
			$daJurnal['GLCodeJE']=$debact;
			$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']=$creact;
			$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']=$data['Disc'];
			$val_min=$daJurnal['ValueJE']*-1;
							
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
		}
		//end jurnal Arcm (Diskon)
		
		//start jurnal Arcm (Ongkir)
		if($data['Freight']>0)
		{
			$debact = $this->m_coa_setting->GetValue('pen_ongkir');
			$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							
			$daJurnal['intHID']=$headJurnal;
			$daJurnal['DCJE']='D';
							
			$daJurnal['GLCodeJE']=$debact;
			$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
			$daJurnal['GLCodeJEX']=$creact;
			$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
			$daJurnal['ValueJE']=$data['Freight'];
			$val_min=$daJurnal['ValueJE']*-1;
							
			$detailJurnal=$this->m_jurnal->insertD($daJurnal);
			$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
			$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
		}
		//end jurnal Arcm (Ongkir)
		
		//start jurnal Arcm (Tax)
		if($data['Tax']>0 or $data['Tax']<0)
		{
			$listcoatax = $this->m_tax->GetCoaTaxByRate($data['TaxPer']);
			foreach($listcoatax->result() as $lct)
			{
				$valcoatax 	= $lct->intRate/$data['TaxPer']*$data['Tax'];
				
				if($valcoatax>0)
				{
					$debact 	= $lct->vcGLAR;
					$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
				}
				else
				{
					$creact 	= $lct->vcGLAR;
					$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
					$valcoatax = $valcoatax*-1;
				}
								
				$daJurnal['intHID']=$headJurnal;
				$daJurnal['DCJE']='D';
								
				$daJurnal['GLCodeJE']=$debact;
				$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
				$daJurnal['GLCodeJEX']=$creact;
				$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
				$daJurnal['ValueJE']=$valcoatax;
				$val_min=$daJurnal['ValueJE']*-1;
								
				$detailJurnal=$this->m_jurnal->insertD($daJurnal);
				$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
				$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
			}
		}
		//end jurnal ARcm (Tax)
		$cekblock = $this->m_block->cekblock($head,'ARCM');
			
		if($cekblock=='')
		{
			$_SESSION['totitemARCM']=0;
			unset($_SESSION['itemcodeARCM']);
			unset($_SESSION['idARCM']);
			unset($_SESSION['idBaseRefARCM']);
			unset($_SESSION['itemnameARCM']);
			unset($_SESSION['qtyARCM']);
			unset($_SESSION['qtyOpenARCM']);
			unset($_SESSION['uomARCM']);
			unset($_SESSION['priceARCM']);
			unset($_SESSION['discARCM']);
			unset($_SESSION['whsARCM']);
			unset($_SESSION['statusARCM']);
			
			$this->db->trans_complete();
			echo $headDocnum;
		}
		else
		{
			echo $cekblock;
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_arcm->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_arcm->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table hover">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
				  <th>Doc. Total</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatusName.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="right">'.number_format($d->intDocTotal,0).'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
			<tfoot>
            <tr>
                <th colspan="5" style="text-align:right">Total:</th>
                <th style="text-align:right; padding-right:9px"></th>
				<th></th>
            </tr>
        </tfoot>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"footerCallback": function ( row, data, start, end, display ) {
					var api = this.api(), data;
		 
					// Remove the formatting to get integer data for summation
					var intVal = function ( i ) {
						return typeof i === \'string\' ?
							i.replace(/[\$,]/g, \'\')*1 :
							typeof i === \'number\' ?
								i : 0;
					};
		 
					// Total over all pages
					total = api
						.column( 5 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
		 
					// Total over this page
					pageTotal = api
						.column( 5, { page: \'current\'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
					pageTotalI = pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 
					// Update footer
					$( api.column( 5 ).footer() ).html(
						\'\'+pageTotalI +\'\'
					);
				},
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_arcm->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemARCM'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if($_SESSION['itemcodeARCM'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyARCM'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenARCM'][$j]=$_POST['detailQty'];
				$_SESSION['uomARCM'][$j]=$_POST['detailUoM'];
				$_SESSION['priceARCM'][$j]=$_POST['detailPrice'];
				$_SESSION['discARCM'][$j]=$_POST['detailDisc'];
				$_SESSION['whsARCM'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefARCM'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameARCM'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyARCM'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenARCM'][$j]=$_POST['detailQty'];
				$_SESSION['uomARCM'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceARCM'][$j]=$_POST['detailPrice'];
				$_SESSION['discARCM'][$j]=$_POST['detailDisc'];
				$_SESSION['whsARCM'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefARCM'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeARCM'][$i]=$code;
				$_SESSION['itemnameARCM'][$i]=$_POST['detailItem'];
				$_SESSION['qtyARCM'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenARCM'][$i]=$_POST['detailQty'];
				if($_POST['Service']==1)
				{
					$_SESSION['uomARCM'][$i]=$_POST['detailUoMS'];
				}else
				{
					$_SESSION['uomARCM'][$i]=$_POST['detailUoM'];
				}
				$_SESSION['priceARCM'][$i]=$_POST['detailPrice'];
				$_SESSION['discARCM'][$i]=$_POST['detailDisc'];
				$_SESSION['whsARCM'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefARCM'][$i]='';
				$_SESSION['statusARCM'][$i]='O';
				$_SESSION['totitemARCM']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeARCM'][$i]='';
					$_SESSION['itemnameARCM'][$i]=$_POST['detailItem'];
					$_SESSION['qtyARCM'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenARCM'][$i]=$_POST['detailQty'];
					$_SESSION['uomARCM'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceARCM'][$i]=$_POST['detailPrice'];
					$_SESSION['discARCM'][$i]=$_POST['detailDisc'];
					$_SESSION['whsARCM'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefARCM'][$i]='';
					$_SESSION['statusARCM'][$i]='O';
					$_SESSION['totitemARCM']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemARCM'];$j++)
		{
			if($_SESSION['itemcodeARCM'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeARCM'][$j]="";
				$_SESSION['itemnameARCM'][$j]="";
				$_SESSION['qtyARCM'][$j]="";
				$_SESSION['qtyOpenARCM'][$j]="";
				$_SESSION['uomARCM'][$j]="";
				$_SESSION['priceARCM'][$j]="";
				$_SESSION['discARCM'][$j]="";
				$_SESSION['whsARCM'][$j]="";
				$_SESSION['BaseRefARCM'][$j]='';
			}
			if($_SESSION['itemnameARCM'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeARCM'][$j]="";
				$_SESSION['itemnameARCM'][$j]="";
				$_SESSION['qtyARCM'][$j]="";
				$_SESSION['qtyOpenARCM'][$j]="";
				$_SESSION['uomARCM'][$j]="";
				$_SESSION['priceARCM'][$j]="";
				$_SESSION['discARCM'][$j]="";
				$_SESSION['whsARCM'][$j]="";
				$_SESSION['BaseRefARCM'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemARCM'];$j++)
			{
				if($_SESSION['itemnameARCM'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameARCM'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameARCM'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsARCM'][$j]);
					$cekbatch = $this->m_item->cekbatch($item);
					
					if($_SESSION['uomARCM'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomARCM'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomARCM'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomARCM'][$j];
					}
					
					if($_SESSION['discARCM'][$j]=='')
					{
						$_SESSION['discARCM'][$j]=0;
					}
					
					if($_SESSION['statusARCM'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discARCM'][$j])/100)*$_SESSION['priceARCM'][$j]*$_SESSION['qtyARCM'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeARCM'][$j].'</td>
						<td>'.$_SESSION['itemnameARCM'][$j].'</td>
					';
					if($cekbatch==1 and $_SESSION['BaseRefARCM'][$j]!='SR')
					{
						echo '
							<td align="right"><a href="#" onclick="changebatch(\''.$item.'\',\''.$_SESSION["qtyARCM"][$j].'\',\''.$_SESSION["whsARCM"][$j].'\')">'.number_format($_SESSION['qtyARCM'][$j],'2').'</a></td>
						';
					}
					else
					{
						echo '
						<td align="right">'.number_format($_SESSION['qtyARCM'][$j],'2').'</td>';
					}
					echo '
						<td align="right">'.number_format($_SESSION['qtyOpenARCM'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceARCM'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discARCM'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusARCM'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnameARCM"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodeARCM"][$j]).'\',\''.$_SESSION["qtyARCM"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomARCM"][$j]).'\',\''.str_replace("'","\'",$viewUoM).'\',\''.$_SESSION["priceARCM"][$j].'\',\''.$_SESSION["discARCM"][$j].'\',\''.str_replace("'","\'",$_SESSION["whsARCM"][$j]).'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodeARCM"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemnameARCM"][$j]).'\',\''.$_SESSION["qtyARCM"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
