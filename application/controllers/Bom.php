<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bom extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemBOM']=0;
			unset($_SESSION['itemcodeBOM']);
			unset($_SESSION['itemnameBOM']);
			unset($_SESSION['qtyBOM']);
			unset($_SESSION['uomBOM']);
			unset($_SESSION['sourceBOM']);
			unset($_SESSION['costBOM']);
			unset($_SESSION['idBOM']);
			unset($_SESSION['autoBOM']);
			
			
			$data['typetoolbar']='BOM';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Bom',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_bom->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_bom->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hBOM');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->FGItem = $r->vcItemName;
			$responce->Whs = $r->intLocation;
			$responce->FGQty = $r->intQty;
			$responce->Remarks = $r->vcRemarks;
			$responce->vcRef = $r->vcRef;
			$responce->FGCost = $r->intCost;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hBOM');
			$responce->DocDate = date('m/d/Y');
			$responce->FGItem = '';
			$responce->Whs = '';
			$responce->FGQty = '';
			$responce->Remarks = '';
			$responce->vcRef = '';
			$responce->FGCost = '';
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		if($data['Whs']=='A')
		{
			$item=$this->m_item->GetIDByName($data['detailItem']);
			$cost=$this->m_stock->GetCostAverage($item);
		}
		else if($data['Whs']=='MAX')
		{
			$item=$this->m_item->GetIDByName($data['detailItem']);
			$cost=$this->m_stock->GetCostMax($item);
		}
		else if($data['Whs']=='MIN')
		{
			$item=$this->m_item->GetIDByName($data['detailItem']);
			$cost=$this->m_stock->GetCostMin($item);
		}
		else
		{
			$item=$this->m_item->GetIDByName($data['detailItem']);
			$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		}
		echo $cost;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemBOM']=0;
			unset($_SESSION['idBOM']);
			unset($_SESSION['itemcodeBOM']);
			unset($_SESSION['itemnameBOM']);
			unset($_SESSION['qtyBOM']);
			unset($_SESSION['uomBOM']);
			unset($_SESSION['sourceBOM']);
			unset($_SESSION['costBOM']);
			unset($_SESSION['autoBOM']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemBOM']=0;
			unset($_SESSION['itemcodeBOM']);
			unset($_SESSION['itemnameBOM']);
			unset($_SESSION['qtyBOM']);
			unset($_SESSION['uomBOM']);
			unset($_SESSION['sourceBOM']);
			unset($_SESSION['costBOM']);
			unset($_SESSION['idBOM']);
			unset($_SESSION['autoBOM']);
			
			$r=$this->m_bom->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idBOM'][$j]=$d->intID;
				$_SESSION['itemcodeBOM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameBOM'][$j]=$d->vcItemName;
				$_SESSION['qtyBOM'][$j]=$d->intQty;
				$_SESSION['sourceBOM'][$j]=$d->vcSourceCost;
				$_SESSION['uomBOM'][$j]=$d->vcUoM;
				$_SESSION['costBOM'][$j]=$d->intCost;
				$_SESSION['autoBOM'][$j]=$d->intAutoIssue;
				$j++;
			}
			$_SESSION['totitemBOM']=$j;
		}
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemBOM'];$j++)
		{
			if(isset($_SESSION['itemcodeBOM'][$j]))
			{
				if($_SESSION['itemcodeBOM'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekrev()
	{
		$data['FGItem'] = isset($_POST['FGItem'])?$_POST['FGItem']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		$data['FGItemID'] = $this->m_item->GetIDByName($data['FGItem']);
		$result=$this->m_bom->cekrev($data);
		echo $result;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['FGItem'] = isset($_POST['FGItem'])?$_POST['FGItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		$data['FGCost'] = isset($_POST['FGCost'])?$_POST['FGCost']:''; // get the requested page
		
		$this->db->trans_begin();
		
		$head=$this->m_bom->insertH($data);
		$headDocnum=$this->m_bom->GetHeaderByHeaderID($head)->vcDocNum;
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		$totalcost=0;
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemBOM'];$j++)// save detail
			{
				if($_SESSION['itemcodeBOM'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameBOM'][$j]);
					$da['intHID']=$head;
					$da['itemcodeBOM']=$_SESSION['itemcodeBOM'][$j];
					$da['itemnameBOM']=$_SESSION['itemnameBOM'][$j];
					$da['qtyBOM']=$_SESSION['qtyBOM'][$j];
					$da['uomBOM']=$_SESSION['uomBOM'][$j];
					$da['sourceBOM']=$_SESSION['sourceBOM'][$j];
					$da['costBOM']=$_SESSION['costBOM'][$j];
					$da['autoBOM']=$_SESSION['autoBOM'][$j];
					$detail=$this->m_bom->insertD($da);
					$totalcost=$totalcost+($da['qtyBOM']*$da['costBOM']);
					
				}
			}
			$this->m_bom->updateCost($head,$totalcost);
			$_SESSION['totitemBOM']=0;
			unset($_SESSION['itemcodeBOM']);
			unset($_SESSION['itemnameBOM']);
			unset($_SESSION['qtyBOM']);
			unset($_SESSION['uomBOM']);
			unset($_SESSION['sourceBOM']);
			unset($_SESSION['costBOM']);
			unset($_SESSION['idBOM']);
			unset($_SESSION['autoBOM']);
			echo $headDocnum;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_bom->editH($data);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemBOM'];$j++)// save detail
			{
				if($_SESSION['itemcodeBOM'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
				{
					if(isset($_SESSION['idBOM'][$j]))
					{
						$this->m_bom->deleteD($_SESSION['idBOM'][$j]);
					}
				}
				else
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameBOM'][$j]);
					$da['intHID']=$data['id'];
					$da['itemcodeBOM']=$_SESSION['itemcodeBOM'][$j];
					$da['itemnameBOM']=$_SESSION['itemnameBOM'][$j];
					$da['qtyBOM']=$_SESSION['qtyBOM'][$j];
					$da['uomBOM']=$_SESSION['uomBOM'][$j];
					$da['sourceBOM']=$_SESSION['sourceBOM'][$j];
					$da['costBOM']=$_SESSION['costBOM'][$j];
					$da['autoBOM']=$_SESSION['autoBOM'][$j];
					if(isset($_SESSION['idBOM'][$j])) //jika ada session id artinya data detail dirubah
					{
						$da['intID']=$_SESSION['idBOM'][$j];
						$detail=$this->m_bom->editD($da);
					}
					else //jika tidak artinya ini merupakan detail tambahan
					{
						$detail=$this->m_bom->insertD($da);
					}
				}
				
			}
			$listdetail=$this->m_bom->GetDetailByHeaderID($data['id']);
			$totalcost=0;
			foreach($listdetail->result() as $ld)
			{
				$totalcost=$totalcost+($ld->intQty*$ld->intCost);
			}
			$this->m_bom->updateCost($data['id'],$totalcost);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemBOM'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemBOM'];$j++)
		{
			if($_SESSION['itemcodeBOM'][$j]==$code)
			{
				$_SESSION['qtyBOM'][$j]=$_POST['detailQty'];
				$_SESSION['sourceBOM'][$j]=$_POST['SourceCost'];
				$_SESSION['costBOM'][$j]=$_POST['detailCost'];
				$_SESSION['autoBOM'][$j]=$_POST['detailAuto'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeBOM'][$i]=$code;
				$_SESSION['itemnameBOM'][$i]=$_POST['detailItem'];
				$_SESSION['qtyBOM'][$i]=$_POST['detailQty'];
				$_SESSION['sourceBOM'][$i]=$_POST['SourceCost'];
				$_SESSION['costBOM'][$i]=$_POST['detailCost'];
				$_SESSION['uomBOM'][$i]=$UoM;
				$_SESSION['autoBOM'][$i]=$_POST['detailAuto'];
				$_SESSION['totitemBOM']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemBOM'];$j++)
		{
			if($_SESSION['itemcodeBOM'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeBOM'][$j]="";
				$_SESSION['itemnameBOM'][$j]="";
				$_SESSION['qtyBOM'][$j]="";
				$_SESSION['uomBOM'][$j]="";
				$_SESSION['sourceBOM'][$j]="";
				$_SESSION['costBOM'][$j]="";
				$_SESSION['autoBOM'][$i]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
				  <th>Source</th>
				  <th>Cost</th>
				  <th>Issue Method</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemBOM'];$j++)
			{
				if($_SESSION['itemnameBOM'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameBOM'][$j]);
					if($_SESSION['sourceBOM'][$j] != 'A' and $_SESSION['sourceBOM'][$j] != 'MAX' and $_SESSION['sourceBOM'][$j] != 'MIN')
					{
						$source =$this->m_location->GetNameByID($_SESSION['sourceBOM'][$j]); 
					}
					else
					{
						$source = $_SESSION['sourceBOM'][$j];
					}
					if($_SESSION["autoBOM"][$j]==1)
					{
						$autobom = 'Auto';
					}
					else
					{
						$autobom = 'Manual';
					}
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeBOM'][$j].'</td>
						<td>'.$_SESSION['itemnameBOM'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyBOM'][$j],'2').'</td>
						<td>'.$_SESSION['uomBOM'][$j].'</td>
						<td>'.$source.'</td>
						<td align="right">'.number_format($_SESSION['costBOM'][$j],'2').'</td>
						<td>'.$autobom.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnameBOM"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodeBOM"][$j]).'\',\''.$_SESSION["qtyBOM"][$j].'\',\''.$_SESSION["costBOM"][$j].'\',\''.$_SESSION["autoBOM"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomBOM"][$j]).'\',\''.str_replace("'","\'",$_SESSION["sourceBOM"][$j]).'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodeBOM"][$j]).'\',\''.$_SESSION["qtyBOM"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
