<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cancel_doc extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_docnum','',TRUE);
		
		$this->load->model('m_inpay','',TRUE);
		$this->load->model('m_outpay','',TRUE);
		
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_batch','',TRUE);
		
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_apcm','',TRUE);
		$this->load->model('m_pr','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_do','',TRUE);
		$this->load->model('m_sr','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_jurnal','',TRUE);
		
		$this->load->model('m_group_cf','',TRUE);
		$this->load->model('m_setting','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['araccess'] = $this->authorization->getcrudaccess('ar');
			$data['adaccess'] = $this->authorization->getcrudaccess('adjustment');
			$data['apaccess'] = $this->authorization->getcrudaccess('ap');
			$data['dnaccess'] = $this->authorization->getcrudaccess('dn');
			$data['grpoaccess'] = $this->authorization->getcrudaccess('grpo');
			$data['sraccess'] = $this->authorization->getcrudaccess('sr');
			$data['apcmaccess'] = $this->authorization->getcrudaccess('apcm');
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Cancel Document',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['listdoc']=$this->m_docnum->GetAllDocType();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function getdata()
	{
		$data['DocNum'] = isset($_GET['DocNum'])?$_GET['DocNum']:''; // get the requested page
		$data['DocType'] = isset($_GET['DocType'])?$_GET['DocType']:''; // get the requested page
		if($data['DocType']==''){
			echo 'Data Does Not Exist';
			exit();
		}
		
		if($data['DocType']=='AD'){
			$tableH = 'hAdjustment';
			$tableD = 'dAdjustment';
		}else{
			$tableH = 'h'.$data['DocType'];
			$tableD = 'd'.$data['DocType'];
		}
		
		$dataH = $this->m_docnum->GetDataH($tableH,$tableD,$data['DocNum']);
		if(!$dataH){
			echo 'Document Not Found';
			exit();
		}
		if($dataH->vcStatus!='O'){
			echo 'You can only cancel documents with closed status';
			exit();
		}
		$dataD = $this->m_docnum->GetDataDD($tableH,$tableD,$data['DocNum']);
		$dataDH = $this->m_docnum->GetDataDDHH($tableH,$tableD,$data['DocNum']);
		echo '
		<div class="row">
			<div class="col-sm-6">
		';
				if(isset($dataH->vcDocNum)){
				echo'
				<div class="form-group">
				<label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="DocNum" name="DocNum" data-toggle="tooltip" data-placement="top" title="Document Number" readonly="true" value="'.$dataH->vcDocNum.'">
					</div>
				</div>';
				}
				if(isset($dataH->vcBPCode)){
				echo'
				<div class="form-group">
				<label for="RefNum" class="col-sm-4 control-label" style="height:20px">BP Code</label>
					<div class="col-sm-8" style="height:45px">
					<div class="input-group">
					<div class="input-group-addon" id="triggermodalbp">
						<i class="fa fa-users" aria-hidden="true"></i>
					</div>
					<input type="text" class="form-control" id="BPCode" name="BPCode" placeholder="Bussiness Partner Code" data-toggle="tooltip" data-placement="top" value="'.$dataH->vcBPCode.'"title="Bussiness Partner Code" disabled="true">
					</div>
					</div>
				</div>
				';
				}
				if(isset($dataH->vcBPName)){
				echo'
				<div class="form-group">
				<label for="BPName" class="col-sm-4 control-label" style="height:20px">BP Name</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="BPName" name="BPName" placeholder="Bussiness Partner Name"  data-toggle="tooltip" data-placement="top" value="'.$dataH->vcBPName.'" title="Bussiness Partner Name" disabled="true">
					</div>
				</div>
				';
				}
				if(isset($dataH->vcRef)){
				echo'
				<div class="form-group">
				<label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" data-toggle="tooltip" data-placement="top" title="Reference Number" value="'.$dataH->vcRef.'" disabled="true">
					</div>
				</div>
				';
				}
				if(isset($dataH->intService)){
				echo'
				<div class="form-group">
				<label for="Service" class="col-sm-4 control-label" style="height:20px">Type</label>
					<div class="col-sm-8" style="height:45px">
					<select id="Service" name="Service" class="form-control select2" style="width: 100%; height:35px"  data-toggle="tooltip" data-placement="top" title="Service" disabled="true">
			';
					if($dataH->intService==1){
						echo '<option value="1" >Service</option>';
					}else{
						echo '<option value="0" >Item</option>';
					}
				echo '</select>
					</div>
				</div>
				';
				}
			echo '
			</div>

			<div class="col-md-6">
			';
				if(isset($dataH->vcStatus)){
				echo'	
				<div class="form-group">
					<label for="Status" class="col-sm-4 control-label" style="height:20px">Status</label>
					<div class="col-sm-8" style="height:45px">
					<table style="width:100%">
						<tr>
						<td><input type="text" class="form-control" id="Status" name="Status" readonly="true" data-toggle="tooltip" data-placement="top" value="Open" title="Status" disabled="true">
						</td>
						</tr>
					</table>
					</div>
				</div>
				';
				}
				
				if(isset($dataH->dtDate)){
				echo'
				<div class="form-group">
					<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
					<div class="col-sm-8" style="height:45px">
					<div class="input-group date">
						<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control" id="DocDate" name="DocDate" data-toggle="tooltip" data-placement="top" value="'.date('m/d/Y',strtotime($dataH->dtDate)).'" title="Document Date" disabled="true">
					</div>
					</div>
				</div>
				';
				}
				if(isset($dataH->vcType)){
					echo'
				<div class="form-group">
					<label for="Type" class="col-sm-4 control-label" style="height:20px">Type</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="Type" name="Type" placeholder="Type" data-toggle="tooltip" data-placement="top" title="Type" value="'.$dataH->vcType.'" disabled="true">
					</div>
				</div>
				';
				}
				if(isset($dataH->dtDelDate)){
				echo'
				<div class="form-group">
					<label class="col-sm-4 control-label" style="height:20px">Del. Date</label>
					<div class="col-sm-8" style="height:45px">
					<div class="input-group date">
						<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control" id="DelDate" name="DelDate" data-toggle="tooltip" data-placement="top" value="'.date('m/d/Y',strtotime($dataH->dtDelDate)).'" title="Delivery Date" disabled="true">
					</div>
					</div>
				</div>
				';
				}
				if(isset($dataH->dtDueDate)){
				echo'
				<div class="form-group">
					<label class="col-sm-4 control-label" style="height:20px">Due. Date</label>
					<div class="col-sm-8" style="height:45px">
					<div class="input-group date">
						<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control" id="DueDate" name="DueDate" data-toggle="tooltip" data-placement="top" value="'.date('m/d/Y',strtotime($dataH->dtDueDate)).'" title="Due Date" disabled="true">
					</div>
					</div>
				</div>
				';
				}
				if(isset($dataH->vcGLCode)){
					echo'
				<div class="form-group">
					<label for="GLCode" class="col-sm-4 control-label" style="height:20px">Account</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="GLCode" name="GLCode" placeholder="GLCode" data-toggle="tooltip" data-placement="top" title="GLCode" value="'.$dataH->vcGLCode.'" disabled="true">
					</div>
				</div>
				';
				}
				if(isset($dataH->vcSalesName)){
				echo'
				<div class="form-group">
					<label for="SalesEmp" class="col-sm-4 control-label" style="height:20px">Sales Employee</label>
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="SalesEmp" name="SalesEmp" placeholder="Sales Employee" data-toggle="tooltip" data-placement="top" value="'.$dataH->vcSalesName.'" title="Sales Employee" disabled="true">
					</div>
				</div>
				';
				}

				echo '
			</div>
		</div>
		';

		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
		';
		if(isset($dataDH->vcItemCode)){
			echo '<th>Code</th>';
		}
		if(isset($dataDH->vcItemName)){
			echo '<th>Name</th>';
  		}
		if(isset($dataDH->intQty)){
			echo '<th>Qty</th>';
  		}
		if(isset($dataDH->intOpenQty)){
			echo '<th>Open Qty</th>';
  		}
		if(isset($dataDH->vcUoM)){
			echo '<th>UoM</th>';
  		}
		if(isset($dataDH->intPrice)){
			echo '<th>Price</th>';
  		}
		if(isset($dataDH->intDiscPer)){
			echo '<th>Disc</th>';
  		}
		if(isset($dataDH->intLineTotal)){
			echo '<th>Line Total</th>';
  		}
		if(isset($dataDH->vcLocation)){
			echo '<th>Whs</th>';
  		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
		foreach($dataD->result() as $d){
			echo '<tr>';
			if(isset($d->vcItemCode)){
			echo'<td>'.$d->vcItemCode.'</td>';
			}
			if(isset($d->vcItemName)){
				echo'<td>'.$d->vcItemName.'</td>';
			}
			if(isset($d->intQty)){
				echo'<td align="right">'.number_format($d->intQty,2).'</td>';
			}
			if(isset($d->intOpenQty)){
				echo'<td align="right">'.number_format($d->intOpenQty,2).'</td>';
			}
			if(isset($d->vcUoM)){
				echo'<td>'.$d->vcUoM.'</td>';
			}
			if(isset($d->intPrice)){
				echo'<td align="right">'.number_format($d->intPrice,0).'</td>';
			}
			if(isset($d->intDiscPer)){
				echo'<td align="right">'.number_format($d->intDiscPer,2).' %</td>';
			}
			if(isset($d->intLineTotal)){
				echo'<td align="right">'.number_format($d->intLineTotal,0).'</td>';
			}
			if(isset($d->vcLocation)){
				echo'<td>'.$d->vcLocation.'</td>';
			}
			echo '</tr>';
		}
		echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"ordering": false
			});
		  });
		  
		 
		</script>';
	}
	function execute()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocType'] = isset($_POST['DocType'])?$_POST['DocType']:''; // get the requested page
		
		$this->db->trans_begin();
		if($data['DocType']=='OUTPAY')
		{
			$getstatusdoc=$this->m_outpay->GetHeaderByDocNum($data['DocNum']); // ambil status document
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='C')
				{
					echo "You can only cancel documents outgoing payment with closed status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$idHeader=$getstatusdoc->intID;
					$this->m_outpay->closeall($idHeader); // close document hOutpay
					$header = $this->m_outpay->GetHeaderByHeaderID($idHeader);
					$detail = $this->m_outpay->GetDetailByHeaderID($idHeader);
					
					foreach($detail->result() as $d)
					{
						$dintApplied = $d->intApplied;
						$basetype=$d->vcBaseType."X";
						
						$cf 	= $this->m_setting->getValueByCode('ven_group_cf');
						$idcf	= $this->m_group_cf->GetIDByCode($cf);
						$this->m_wallet->addMutation($header->intWallet,$header->dtDate,$basetype,$dintApplied,$d->vcDocNum,$header->vcDocNum,$idcf);// add mutation wallet
						$this->m_wallet->updateBalance($header->intWallet,$dintApplied); // change wallet balance
						
						$intBaseRef = $d->intBaseRef;
						$vcBaseType = $d->vcBaseType;
						
						
						if($vcBaseType=='AP')
						{
							$dintApplied = $d->intApplied;
							$this->m_ap->rollback($intBaseRef, $dintApplied); // rollback ar
						}
						else if($vcBaseType=='APCM')
						{
							$dintApplied = $d->intApplied*-1;
							$this->m_apcm->rollback($intBaseRef, $dintApplied); // rollback apcm
						}
						else if($vcBaseType=='ARCM')
						{
							$dintApplied = $d->intApplied;
							$this->m_arcm->rollback($intBaseRef, $dintApplied); // rollback arcm
						}
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='INPAY')
		{
			$getstatusdoc=$this->m_inpay->GetHeaderByDocNum($data['DocNum']); // ambil status document
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='C')
				{
					echo "You can only cancel documents incoming payment with closed status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$idHeader=$getstatusdoc->intID;
					$this->m_inpay->closeall($idHeader); // close document hOutpay
					$header = $this->m_inpay->GetHeaderByHeaderID($idHeader);
					$detail = $this->m_inpay->GetDetailByHeaderID($idHeader);
					
					foreach($detail->result() as $d)
					{
						$dintAppliedminus = $d->intApplied*-1;
						$basetype=$d->vcBaseType."X";
						
						$cf 	= $this->m_setting->getValueByCode('cus_group_cf');
						$idcf	= $this->m_group_cf->GetIDByCode($cf);
						$this->m_wallet->addMutation($header->intWallet,$header->dtDate,$basetype,$dintAppliedminus,$d->vcDocNum,$header->vcDocNum,$idcf);// add mutation wallet
						$this->m_wallet->updateBalance($header->intWallet,$dintAppliedminus); // change wallet balance
						
						$intBaseRef = $d->intBaseRef;
						$vcBaseType = $d->vcBaseType;
						
						
						if($vcBaseType=='AR')
						{
							$dintApplied = $d->intApplied;
							$this->m_ar->rollback($intBaseRef, $dintApplied); // rollback ar
						}
						else if($vcBaseType=='ARCM')
						{
							$dintApplied = $d->intApplied*-1;
							$this->m_arcm->rollback($intBaseRef, $dintApplied); // rollback arcm
						}
						else if($vcBaseType=='APCM')
						{
							$dintApplied = $d->intApplied;
							$this->m_apcm->rollback($intBaseRef, $dintApplied); // rollback apcm
						}
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='SR')
		{
			$getstatusdoc=$this->m_sr->GetHeaderByDocNum($data['DocNum']);
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents A/P Invoice with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_sr->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_sr->cancelall($getstatusdoc->intID);//cancel SR
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_sr->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='SRX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='DN')
						{
							//jika baseref GRPO hanya update open qty pada GRPO dan set GRPO to open
							$this->m_dn->updatestatusH($ld->intBaseRef,'O');
							$this->m_dn->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
						}
						$qtymin=$ld->intQtyInv*-1;
						$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
						$idmutasi = $this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'SRX',$ld->dtDate,$ld->vcDocNum);//add mutation
						// batch
						$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'SR');
						foreach($listbatch->result() as $lb)
						{
							$this->m_batch->BatchCancelGR('SRX',$lb,$ld,$idmutasi,$ld->intLocation);
						}
						// end batch
							
						//start detail jurnal
						$actinv = $this->m_item_category->GetAccountHppByItemCode($ld->vcItemCode);
						$creacc = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$creacc;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($daJurnal['GLCodeJEX']);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
					}
					echo 1;
				}
			}
		
		}
		else if($data['DocType']=='APCM')
		{
			$getstatusdoc=$this->m_apcm->GetHeaderByDocNum($data['DocNum']);
			$data['Service'] = $getstatusdoc->intService;
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O' or $getstatusdoc->intApplied>0)
				{
					echo "You can only cancel documents A/P Credit Memo with open status / 0 Applied";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_apcm->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_apcm->cancelall($getstatusdoc->intID);//cancel AP
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_apcm->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='APCMX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='PR')
						{
							//jika baseref PR hanya update open qty pada PR dan set PR to open
							$this->m_pr->updatestatusH($ld->intBaseRef,'O');
							$this->m_pr->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
							
						}
						else if($ld->vcBaseType=='AP')
						{
							
							
						}
						else
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$qtymin=$ld->intQtyInv*-1;
								$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'APCMX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else // jika tipe service
							{
								// tidak terjadi apa-apa
							}
						}
						//start jurnal Apcm
						$data['BPCode'] = $this->m_apcm->GetHeaderByDocNum($headDocnum)->vcBPCode;
						if($ld->intService==1)// jika service
						{
							$debact = $getstatusdoc->vcGLCode;
							$creact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
							
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineTotal;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
						else
						{
							$debact = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							$creact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
							
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineTotal;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
						//start jurnal selisih inventory dengan invoice
						if($ld->intLineTotal>$ld->intLineCost)
						{
							$deb = $this->m_coa_setting->GetValue('sel_ret');
							$cre = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							
							$valsel = $ld->intLineTotal-$ld->intLineCost;
						}
						else
						{
							$deb = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							$cre = $this->m_coa_setting->GetValue('sel_ret');
							
							$valsel = $ld->intLineCost-$ld->intLineTotal;
						}
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$deb;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($deb);
							$daJurnal['GLCodeJEX']=$cre;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($cre);
							$daJurnal['ValueJE']=$valsel;
							$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end jurnal selisih inventory dengan invoices
						//end jurnal Apcm
						
						if($ld->vcBaseType!='PR' and $data['Service']==0)
						{
							//start detail jurnal
							$actinv = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							$acthut_yg_blm_tgih = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
								$daJurnal['GLCodeJE']=$acthut_yg_blm_tgih;
								$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($acthut_yg_blm_tgih);
								$daJurnal['GLCodeJEX']=$actinv;
								$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($actinv);
								$daJurnal['ValueJE']=$ld->intLineCost;
								$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
							//end detail jurnal
							
							
							
							$this->m_stock->updateStock($ld->intItem,$ld->intQtyInv,$ld->intLocation);//update stok menambah/mengurangi di gudang
							$this->m_stock->addMutation($ld->intItem,$ld->intQtyInv,$ld->intCost,$ld->intLocation,'APCMX',$data['DocDate'],$headDocnum);//add mutation
						}
					}
					//start jurnal APCM (Diskon)
					if($getstatusdoc->intDisc>0)
					{
						$debact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
						$creact = $this->m_coa_setting->GetValue('sel_ret'); // diskon dianggap biaya selisih retur
										
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$getstatusdoc->intDisc;
						$val_min=$daJurnal['ValueJE']*-1;
										
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal APCM (Diskon)
					
					//start jurnal APCM (Ongkir)
					if($getstatusdoc->intFreight>0)
					{
						$debact = $this->m_coa_setting->GetValue('biaya_ongkir');
						$creact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
										
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$getstatusdoc->intFreight;
						$val_min=$daJurnal['ValueJE']*-1;
										
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='AP')
		{
			$getstatusdoc=$this->m_ap->GetHeaderByDocNum($data['DocNum']);
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O' or $getstatusdoc->intApplied>0)
				{
					echo "You can only cancel documents A/P Invoice with open status / 0 Applied";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_ap->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_ap->cancelall($getstatusdoc->intID);//cancel AP
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_ap->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='APX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
				
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='GRPO')
						{
							//jika baseref GRPO hanya update open qty pada GRPO dan set GRPO to open
							$this->m_grpo->updatestatusH($ld->intBaseRef,'O');
							$this->m_grpo->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
							
						}
						else if($ld->vcBaseType=='PO')
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$this->m_po->updatestatusH($ld->intBaseRef,'O');
								$this->m_po->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
								
								$qtymin=$ld->intQtyInv*-1;
								$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'APX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else// jika tipe service
							{
								$this->m_po->updatestatusH($ld->intBaseRef,'O');
								$this->m_po->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
							}
						}
						else // tanpa referenci
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$qtymin=$ld->intQtyInv*-1;
								$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'APX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else // jika tipe service
							{
								// tidak terjadi apa-apa
							}
						}
						//start jurnal Ap
						$data['BPCode'] = $this->m_ap->GetHeaderByDocNum($headDocnum)->vcBPCode;
						if($ld->intService==1)// jika service
						{
							$debact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
							$creact = $getstatusdoc->vcGLCode;
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}				
						else
						{
							$debact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
							$creact = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
						//end jurnal Ap
						
						if($ld->vcBaseType!='GRPO' and $ld->intService==0)
						{
							//start detail jurnal inventory jika base on dokumen bukan GRPO (karena harus menambah stock)
							$actinv = $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
							$acthut_yg_blm_tgih = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$acthut_yg_blm_tgih;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthut_yg_blm_tgih);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
						
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
							//end detail jurnal
						
						}
					}
					
					//start jurnal AP (Ongkir)
					if($getstatusdoc->intFreight>0)
					{
						$debact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
						$creact = $this->m_coa_setting->GetValue('biaya_ongkir');
										
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$getstatusdoc->intFreight;
						$val_min=$daJurnal['ValueJE']*-1;
										
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal AP (Ongkir)
					
					
					//start jurnal AP (Tax)
					if($getstatusdoc->intTax>0 or $getstatusdoc->intTax<0)
					{
						$listcoatax = $this->m_tax->GetCoaTaxByRate($getstatusdoc->intTaxPer);
						foreach($listcoatax->result() as $lct)
						{
							$valcoatax 	= $lct->intRate/$getstatusdoc->intTaxPer*$getstatusdoc->intTax;
							if($valcoatax>0)
							{
								$debact 	= $this->m_bp_category->GetApActByBPCode($data['BPCode']);
								$creact 	= $lct->vcGLAP;
							}
							else
							{
								$creact 	= $this->m_bp_category->GetApActByBPCode($data['BPCode']);
								$debact 	= $lct->vcGLAP;
								$valcoatax	= $valcoatax*-1;
							}
							//$debact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
							//$creact = $this->m_coa_setting->GetValue('ppn_ap');
											
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
											
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$valcoatax;
							$val_min=$daJurnal['ValueJE']*-1;
											
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
					}
					//end jurnal AP (Tax)
					
					echo 1;
				}
			}
		}
		else if(($data['DocType']=='GRPO'))
		{
			$getstatusdoc=$this->m_grpo->GetHeaderByDocNum($data['DocNum']);
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents GRPO with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_grpo->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_grpo->cancelall($getstatusdoc->intID);//cancel DN
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_grpo->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='GRPOX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					foreach($listdata->result() as $ld)
					{
						if($ld->vcBaseType=='PO')
						{
							//jika baseref PO hanya update open qty pada PO dan set PO to open
							$this->m_po->updatestatusH($ld->intBaseRef,'O');
							$this->m_po->reOpenDetail($ld->intBaseRef,$ld->intItem,'',$ld->intQty,$ld->intQtyInv);
						}
						$qtymin=$ld->intQtyInv*-1;
						$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
						$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
						$idmutasi = $this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'GRPOX',$ld->dtDate,$ld->vcDocNum);//add mutation

						// batch
						$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'GRPO');
						foreach($listbatch->result() as $lb)
						{
							$this->m_batch->BatchCancelGR('GRPOX',$lb,$ld,$idmutasi,$ld->intLocation);
						}
						// end batch

						$data['BPCode'] = $this->m_grpo->GetHeaderByDocNum($headDocnum)->vcBPCode;
						
						//start detail jurnal
						$actinv 			= $this->m_coa_setting->GetValue('hut_yg_blm_tgh');
						$acthut_yg_blm_tgih = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$acthut_yg_blm_tgih;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthut_yg_blm_tgih);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
						
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='DN')
		{
			$getstatusdoc=$this->m_dn->GetHeaderByDocNum($data['DocNum']);
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents Delivery Note with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_dn->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_dn->cancelall($getstatusdoc->intID);//cancel DN
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_dn->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='DNX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					foreach($listdata->result() as $ld)
					{
						if($ld->vcBaseType=='SO')
						{
							//jika baseref SO hanya update open qty pada SO dan set SO to open
							$this->m_so->updatestatusH($ld->intBaseRef,'O');
							$this->m_so->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
						}
						if($ld->vcBaseType=='DO')
						{
							//jika baseref DO hanya update open qty pada DO dan set DO to open
							$this->m_do->updatestatusH($ld->intBaseRef,'O');
							$this->m_do->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
						}
						$qtyplus=$ld->intQtyInv*1;
						$this->m_stock->updateStock($ld->intItem,$qtyplus,$ld->intLocation);//update stok menambah/mengurangi di gudang
						$this->m_stock->updateCost($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation);//update cost
						$idmutasi = $this->m_stock->addMutation($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation,'DNX',$ld->dtDate,$ld->vcDocNum);//add mutation

						// batch
						$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'DN');
						foreach($listbatch->result() as $lb)
						{
							$this->m_batch->BatchCancelGI('DNX',$lb,$ld,$idmutasi,$ld->intLocation);
						}
						// end batch

						$data['BPCode'] = $this->m_dn->GetHeaderByDocNum($headDocnum)->vcBPCode;
						//start detail jurnal
						
						$actinv = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
						$data['dnconfirm']=$this->m_setting->getValueByCode('dn_confirm'); //cek apakah ada settingan perlu konfirmasi
						$data['coa_inv_transit']=$this->m_setting->getValueByCode('coa_inv_transit'); // ambil account konfirmasi
						
						if($data['dnconfirm']=='Y')
						{
							if($getstatusdoc->intConf==0)
							{
								$acthpp = $data['coa_inv_transit'];
							}
							else
							{
								$acthpp = $this->m_item_category->GetAccountHppByItemCode($ld->vcItemCode);
							}
						}
						else
						{
							$acthpp = $this->m_item_category->GetAccountHppByItemCode($ld->vcItemCode);
						}
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$acthpp;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='AD')
		{
			$getstatusdoc=$this->m_adjustment->GetHeaderByDocNum($data['DocNum']);
			
			if($getstatusdoc->vcType=='GR'){
				$cekminus = "";
				$detl = $this->m_adjustment->GetDetailByHeaderID($getstatusdoc->intID);
				foreach($detl->result() as $dtl)
				{
					$qcekmin=$this->m_stock->cekMinusStock($dtl->intItem,$dtl->intQty,$dtl->intLocation,$getstatusdoc->dtDate);
					if($qcekmin==0)
					{
						$cekminus="Insufficient stock for item ".$dtl->vcItemName."";
						break;
					}
				}
				
			}else{
				$cekminus = "";
			}
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}else if($cekminus!=""){
				
				echo $cekminus;
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents Adjustment with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_adjustment->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_adjustment->cancelall($getstatusdoc->intID);//cancel AD
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_adjustment->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='ADX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					foreach($listdata->result() as $ld)
					{
						$qty=$ld->intQty*-1;
						
						$this->m_stock->updateStock($ld->intItem,$qty,$ld->intLocation);//update stok menambah/mengurangi di gudang
						if($getstatusdoc->vcType=='GR')
						{
							$this->m_stock->updateCost($ld->intItem,$qty,$ld->intCost,$ld->intLocation);//update cost
						}
						$idmutasi = $this->m_stock->addMutation($ld->intItem,$qty,$ld->intCost,$ld->intLocation,'ADX',$ld->dtDate,$ld->vcDocNum);//add mutation

						// batch
						$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'AD');
						foreach($listbatch->result() as $lb)
						{
							if($getstatusdoc->vcType=='GR')
							{
								$this->m_batch->BatchCancelGR('ADX',$lb,$ld,$idmutasi,$ld->intLocation);
							}else
							{
								$this->m_batch->BatchCancelGI('ADX',$lb,$ld,$idmutasi,$ld->intLocation);
							}
						}
						// end batch
						
						//start detail jurnal
						$actinv = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						if($qty<0)
						{
							$daJurnal['ValueJE']=$ld->intCost*$qty*-1;
						}
						else
						{
							$daJurnal['ValueJE']=$ld->intCost*$qty;
						}
						$val_min=$daJurnal['ValueJE']*-1;
						
						if($getstatusdoc->vcType=='GI')
						{
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$getstatusdoc->vcGLCode;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($getstatusdoc->vcGLCode);
						}
						else if($getstatusdoc->vcType=='GR')
						{
							$daJurnal['GLCodeJE']=$getstatusdoc->vcGLCode;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($getstatusdoc->vcGLCode);
							$daJurnal['GLCodeJEX']=$actinv;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($actinv);
						}
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						//end detail jurnal
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='AR')
		{
			$getstatusdoc=$this->m_ar->GetHeaderByDocNum($data['DocNum']);
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O' or $getstatusdoc->intApplied>0)
				{
					echo "You can only cancel documents A/R Invoice with open status / 0 Applied";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_ar->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_ar->cancelall($getstatusdoc->intID);//cancel AR
					
					//proses jurnal
					$headDocnum=$data['DocNum'];
					$data['DocDate'] =  $getstatusdoc->dtDate;
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$headDocnum);
					
					$dataJurnal['Plan']=$this->m_ar->GetPlanByDocNum($headDocnum);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='ARX';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='DN')
						{
							//jika baseref DN hanya update open qty pada DN dan set DN to open
							$this->m_dn->updatestatusH($ld->intBaseRef,'O');
							$this->m_dn->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
							
						}
						else if($ld->vcBaseType=='SO' or $ld->vcBaseType=='SQ')
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$this->m_so->updatestatusH($ld->intBaseRef,'O');
								$this->m_so->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
								
								$qtyplus=$ld->intQtyInv;
								$this->m_stock->updateStock($ld->intItem,$qtyplus,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation);//update cost
								$idmutasi = $this->m_stock->addMutation($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation,'ARX',$ld->dtDate,$ld->vcDocNum);//add mutation
								// batch
								$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'AR');
								foreach($listbatch->result() as $lb)
								{
									$this->m_batch->BatchCancelGI('ARX',$lb,$ld,$idmutasi,$ld->intLocation);
								}
								// end batch
							}
							else// jika tipe service
							{
								$this->m_so->updatestatusH($ld->intBaseRef,'O');
								$this->m_so->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
							}
						}
						else // tanpa referenci
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								//echo "Tanpa Referesni";
								$qtyplus=$ld->intQtyInv;
								$this->m_stock->updateStock($ld->intItem,$qtyplus,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation);//update cost
								$idmutasi = $this->m_stock->addMutation($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation,'ARX',$ld->dtDate,$ld->vcDocNum);//add mutation
								// batch
								$listbatch = $this->m_batch->GetBatchByintHID2($ld->intID,'AR');
								foreach($listbatch->result() as $lb)
								{
									$this->m_batch->BatchCancelGI('ARX',$lb,$ld,$idmutasi,$ld->intLocation);
								}
								// end batch
							}
							else // jika tipe service
							{
								// tidak terjadi apa-apa
							}
						}
						//start jurnal AR
						$data['BPCode'] = $this->m_ar->GetHeaderByDocNum($headDocnum)->vcBPCode;
						if($ld->intService==1)// jika service
						{
							//$debact = $this->m_coa_setting->GetValue('pen_jasa');
							$debact = $getstatusdoc->vcAccount;
							$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineTotal;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}		
						else
						{
							$debact = $this->m_item_category->GetAccountRevByItemCode($ld->vcItemCode);
							$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$ld->intLineTotal;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
						//end jurnal AR
						if($ld->vcBaseType!='DN' and $ld->intService==0)
						{
							//start detail jurnal inventory jika base on dokumen bukan DN (karena harus menambah stock)
							$actinv = $this->m_item_category->GetAccountByItemCode($ld->vcItemCode);
							$acthpp = $this->m_item_category->GetAccountHppByItemCode($ld->vcItemCode);
							
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
						
							$daJurnal['GLCodeJE']=$actinv;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
							$daJurnal['GLCodeJEX']=$acthpp;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
							$daJurnal['ValueJE']=$ld->intLineCost;
							$val_min=$daJurnal['ValueJE']*-1;
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
							//end detail jurnal
							
						}
					}
					// start jurnal DD
					$list = $this->m_ar->getDD($getstatusdoc->intID);
					foreach($list->result() as $l)
					{
						// create jurnal for DD
						$debact = $l->vcAccountCode;
						$creactdebact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
								
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
								
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$l->intValue;
						$val_min=$daJurnal['ValueJE']*-1;
								
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//start jurnal AR (Diskon)
					if($getstatusdoc->intDisc>0)
					{
						$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						$creact = $this->m_coa_setting->GetValue('pot_disc');
										
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$getstatusdoc->intDisc;
						$val_min=$daJurnal['ValueJE']*-1;
										
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal AR (Diskon)
					
					//start jurnal AR (Ongkir)
					if($getstatusdoc->intFreight>0)
					{
						$debact = $this->m_coa_setting->GetValue('pen_ongkir');
						$creact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
										
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
										
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$getstatusdoc->intFreight;
						$val_min=$daJurnal['ValueJE']*-1;
										
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end jurnal AR (Ongkir)
					
					//start jurnal Ar (Tax)
					if($getstatusdoc->intTax>0 or $getstatusdoc->intTax<0)
					{
						$listcoatax = $this->m_tax->GetCoaTaxByRate($getstatusdoc->intTaxPer);
						foreach($listcoatax->result() as $lct)
						{
							$valcoatax 	= $lct->intRate/$getstatusdoc->intTaxPer*$getstatusdoc->intTax;
							if($valcoatax>0)
							{
								$debact 	= $lct->vcGLAR;
								$creact 	= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
							}
							else
							{
								$creact 	= $lct->vcGLAR;
								$debact 	= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
								$valcoatax 	= $valcoatax*-1;
							}
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
											
							$daJurnal['GLCodeJE']=$debact;
							$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
							$daJurnal['GLCodeJEX']=$creact;
							$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
							$daJurnal['ValueJE']=$valcoatax;
							$val_min=$daJurnal['ValueJE']*-1;
											
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						}
					}
					//end jurnal AR (Tax)
					echo 1;
				}
			}
		}
		else
		{
			echo "You Cannot Cancel This Document Type";
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
}
