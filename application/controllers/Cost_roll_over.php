<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost_roll_over extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_rout','',TRUE);
		$this->load->model('m_stock','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['Whs']=isset($_GET['Whs'])?$_GET['Whs']:$data['defaultwhs']; // get the requested page
			
			$data['typetoolbar']='CRO';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Cost Roll Over',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			if(isset($_GET['execute']))
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['Whs']);
				$data['cru']=$this->m_bom->GetAllCRUByLoc($data['Whs']);
				$data['cru2']=$this->m_rout->GetAllCRUByLoc($data['Whs']);
			}
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function execute()
	{
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'excute',$_POST['Whs']);
		$cru=$this->m_bom->GetAllCRUByLoc($_POST['Whs']);
		$cru2=$this->m_rout->GetAllCRUByLoc($_POST['Whs']);
		foreach($cru->result() as $a)
		{
			$index=$a->intID;
			if(isset($_POST['DCek'.$index]))
			{
				
				//update BOM
				$data['id']=$a->intBOM;
				$data['Whs'] = $a->intLocation;
				$data['FGQty'] = $a->BOMQty;
				$data['Remarks'] = $a->BOMRemarks;
				$data['Rev'] = $a->vcRef;
				
				$this->m_bom->editH($data);
				
				$detailBOM=$this->m_bom->GetDetailByHeaderID($a->intBOM);
				
				foreach($detailBOM->result() as $dbm)
				{
					
					$item=$dbm->intItem;
					$detailCRU=$this->m_bom->GetDetailCRUByBomIDandItem($a->intBOM,$item);
					
					$da['intID']=$dbm->intID;
					$da['intHID']=$dbm->intHID;
					$da['itemcodeBOM']=$dbm->vcItemCode;
					$da['itemnameBOM']=$dbm->vcItemName;
					$da['qtyBOM']=$dbm->intQty;
					$da['uomBOM']=$dbm->vcUoM;
					$da['sourceBOM']=$dbm->vcSourceCost;
					$da['costBOM']=$detailCRU->intCost;
					$da['autoBOM']=$dbm->intAutoIssue;
					$detail=$this->m_bom->editD($da);
				}
				$this->m_bom->updateCost($data['id'],$a->intCost);
				// del CRU
				$this->m_bom->delCRU($index);
			}
		}
		foreach($cru2->result() as $a)
		{
			$index=$a->intID;
			if(isset($_POST['DCek'.$index]))
			{
				//update ROUT
				$data['id']=$a->intROUT;
				$data['Whs'] = $a->intLocation;
				$data['FGQty'] = $a->ROUTQty;
				$data['Remarks'] = $a->ROUTRemarks;
				$data['Rev'] = $a->vcRef;
				
				$this->m_rout->editH($data);
				$detailROUT=$this->m_rout->GetDetailByHeaderID($a->intROUT);
				
				foreach($detailROUT->result() as $drt)
				{
					$activity=$drt->intActivity;
					$detailCRU2=$this->m_rout->GetDetailCRUByRoutIDandActivity($a->intROUT,$activity);
					
					$da['intID']=$dbm->intID;
					$da['intHID']=$dbm->intHID;
					$da['activitycodeROUT']=$drt->vcActivityCode;
					$da['activitynameROUT']=$drt->vcActivityName;
					$da['qtyROUT']=$drt->intQty;
					$da['uomROUT']=$drt->vcUoM;
					$da['sourceROUT']=$drt->vcSourceCost;
					$da['costROUT']=$detailCRU->intCost;
					$da['autoROUT']=$drt->intAutoIssue;
					$detail=$this->m_rout->editD($da);
				}
				$this->m_rout->updateCost($data['id'],$a->intCost);
				// del CRU
				$this->m_rout->delCRU($index);
			}
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?execute=true&Whs='.$_POST['Whs'].'&error=true';
		}else{
			$this->db->trans_commit();
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?execute=true&Whs='.$_POST['Whs'].'&success=true';
		}
		
		
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	public function detailbom()
	{
		$bom=$this->m_bom->GetDetailByHeaderID($_GET['id']);
		$cru=$this->m_bom->GetDetailCRUByBomID($_GET['id']);

		$rout=$this->m_rout->GetDetailByHeaderID($_GET['id2']);
		$cru2=$this->m_rout->GetDetailCRUByRoutID($_GET['id2']);
		echo '
		<div class="row">
		 <div class="col-sm-6">
		 <b><i><u>BOM Detail</u></i></b><br>
		  <table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th align="right">Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($bom->result() as $b)
		 {
			  echo'
			  <tr>
				<td>'.$b->vcItemCode.'</td>
				<td>'.$b->vcItemName.'</td>
				<td>'.$b->intQty.'</td>
                <td>'.$b->vcUoM.'</td>
				<td>'.$b->intCost.'</td>
			  </tr>
			  
			  ';
		 }  
		 echo '
			  </tbody>
			</table>
		 </div>
		 
		 <div class="col-sm-6">
		   <b><i><u>Cost Roll Up Detail</u></i></b><br>
		  <table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th>Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($cru->result() as $c)
		 {
			  echo'
			  <tr>
				<td>'.$c->vcItemCode.'</td>
				<td>'.$c->vcItemName.'</td>
				<td>'.$c->intQty.'</td>
                <td>'.$c->vcUoM.'</td>
				<td>'.$c->intCost.'</td>
			  </tr>
			  
			  ';
		 }  
		 
		echo '
			  </tbody>
			</table>
		 </div>
		</div>
		';

		echo '
		<div class="row">
		 <div class="col-sm-6">
		 <b><i><u>ROUT Detail</u></i></b><br>
		  <table id="example4" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th>Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($rout->result() as $b)
		 {
			  echo'
			  <tr>
				<td>'.$b->vcActivityCode.'</td>
				<td>'.$b->vcActivityName.'</td>
				<td align="right">'.$b->intQty.'</td>
                <td>'.$b->vcUoM.'</td>
				<td align="right">'.number_format($b->intCost,2).'</td>
			  </tr>
			  
			  ';
		 }  
		 echo '
			  </tbody>
			</table>
		 </div>
		 
		 <div class="col-sm-6">
		   <b><i><u>Cost Roll Up Detail</u></i></b><br>
		  <table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th>Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($cru2->result() as $c)
		 {
			  echo'
			  <tr>
				<td>'.$c->vcActivityCode.'</td>
				<td>'.$c->vcActivityName.'</td>
				<td align="right">'.$c->intQty.'</td>
                <td>'.$c->vcUoM.'</td>
				<td align="right">'.number_format($c->intCost,2).'</td>
			  </tr>
			  
			  ';
		 }  
		 
		echo '
			  </tbody>
			</table>
		 </div>
		</div>
		';
		
	}
}
