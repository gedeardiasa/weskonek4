<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dn extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_do','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_jurnal','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_udf','',TRUE);
		$this->load->model('m_block','',TRUE);
		$this->load->model('m_batch','',TRUE);

		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			
			$data['typetoolbar']='DN';
			$data['typeudf']='DN';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('delivery note',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_dn->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccessNotService($_SESSION['IDPOS']);
			$data['listso']=$this->m_so->GetOpenDataWithPlanAccessNotService($_SESSION['IDPOS']);
			$data['listdo']=$this->m_do->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['backdate']=$this->m_docnum->GetBackdate('DN');
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['uservalue']=$this->m_user->getByID($_SESSION['IDPOS'])->intValue;
			$data['dnconfirm']=$this->m_setting->getValueByCode('dn_confirm');
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_dn->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			$responce->CityH = $r->vcCity;
			$responce->CountryH = $r->vcCountry;
			$responce->AddressH = $r->vcAddress;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			
			$responce->CityH ='';
			$responce->CountryH = '';
			$responce->AddressH = '';
			
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSQ()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			$responce->CityH = $r->vcCity;
			$responce->CountryH = $r->vcCountry;
			$responce->AddressH = $r->vcAddress;
			
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	function getDataHeaderSO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			$responce->CityH = $r->vcCity;
			$responce->CountryH = $r->vcCountry;
			$responce->AddressH = $r->vcAddress;
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderDO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_do->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			$responce->CityH = $r->vcCity;
			$responce->CountryH = $r->vcCountry;
			$responce->AddressH = $r->vcAddress;
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			
			$r=$this->m_dn->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idDN'][$j]=$d->intID;
				$_SESSION['idBaseRefDN'][$j]=$d->intBaseRef;
				$_SESSION['BaseRefDN'][$j]=$d->vcBaseType;
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusDN'][$j]='O';
				}
				else
				{
					$_SESSION['statusDN'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemDN']=$j;
		}
	}
	function loaddetailsq()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemDN']=0;
		unset($_SESSION['itemcodeDN']);
		unset($_SESSION['idDN']);
		unset($_SESSION['idBaseRefDN']);
		unset($_SESSION['itemnameDN']);
		unset($_SESSION['qtyDN']);
		unset($_SESSION['qtyOpenDN']);
		unset($_SESSION['uomDN']);
		unset($_SESSION['priceDN']);
		unset($_SESSION['discDN']);
		unset($_SESSION['whsDN']);
		unset($_SESSION['statusDN']);
		
		$r=$this->m_sq->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefDN'][$j]=$d->intHID;
				$_SESSION['BaseRefDN'][$j]='SQ';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				$_SESSION['statusDN'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemDN']=$j;
	}
	function loaddetailso()
	{
		$id=$_POST['id'];
		$_SESSION['totitemDN']=0;
		unset($_SESSION['itemcodeDN']);
		unset($_SESSION['idDN']);
		unset($_SESSION['idBaseRefDN']);
		unset($_SESSION['itemnameDN']);
		unset($_SESSION['qtyDN']);
		unset($_SESSION['qtyOpenDN']);
		unset($_SESSION['uomDN']);
		unset($_SESSION['priceDN']);
		unset($_SESSION['discDN']);
		unset($_SESSION['whsDN']);
		unset($_SESSION['statusDN']);
		
		$r=$this->m_so->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefDN'][$j]=$d->intHID;
				$_SESSION['BaseRefDN'][$j]='SO';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				$_SESSION['statusDN'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemDN']=$j;
	}
	function loaddetaildo()
	{
		$id=$_POST['id'];
		$_SESSION['totitemDN']=0;
		unset($_SESSION['itemcodeDN']);
		unset($_SESSION['idDN']);
		unset($_SESSION['idBaseRefDN']);
		unset($_SESSION['itemnameDN']);
		unset($_SESSION['qtyDN']);
		unset($_SESSION['qtyOpenDN']);
		unset($_SESSION['uomDN']);
		unset($_SESSION['priceDN']);
		unset($_SESSION['discDN']);
		unset($_SESSION['whsDN']);
		unset($_SESSION['statusDN']);
		
		$r=$this->m_do->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefDN'][$j]=$d->intHID;
				$_SESSION['BaseRefDN'][$j]='DO';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				$_SESSION['statusDN'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemDN']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!='' and $_SESSION['itemcodeDN'][$j]!=null)
			{
				$total=$total+(($_SESSION['qtyDN'][$j]*$_SESSION['priceDN'][$j])-($_SESSION['discDN'][$j]/100*($_SESSION['qtyDN'][$j]*$_SESSION['priceDN'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if(isset($_SESSION['itemcodeDN'][$j]))
			{
				if($_SESSION['itemcodeDN'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$hasil='';
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeDN'][$j]);
				if($_SESSION['uomDN'][$j]==1)// konversi uom
				{
					$da['qtyinvDN']=$_SESSION['qtyDN'][$j];
				}
				else if($_SESSION['uomDN'][$j]==2)
				{
					$da['qtyinvDN']=$this->m_item->convert_qty($item,$_SESSION['qtyDN'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomDN'][$j]==3)
				{
					$da['qtyinvDN']=$this->m_item->convert_qty($item,$_SESSION['qtyDN'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvDN'],$_SESSION['whsDN'][$j],$data['DocDate']);
				if($res==1)
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeDN'][$j].' - '.$_SESSION['itemnameDN'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsDN'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSQ($id,$item,$qty,$qtyinv)
	{
		$this->m_sq->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseSO($id,$item,$qty,$qtyinv)
	{
		$this->m_so->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseDO($id,$item,$qty,$qtyinv)
	{
		$this->m_do->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['CityH'] = isset($_POST['CityH'])?$_POST['CityH']:''; // get the requested page
		$data['CountryH'] = isset($_POST['CountryH'])?$_POST['CountryH']:''; // get the requested page
		$data['AddressH'] = isset($_POST['AddressH'])?$_POST['AddressH']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$batch = json_decode($_POST['batch']); // batch
		$this->db->trans_begin();
		
		$head=$this->m_dn->insertH($data);
		
		//proses jurnal
		$headDocnum=$this->m_dn->GetHeaderByHeaderID($head)->vcDocNum;
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		for($j=0;$j<$_SESSION['totitemDN'];$j++)// proses mencari data locationa
		{
			if($_SESSION['whsDN'][$j]!="" or $_SESSION['whsDN'][$j]!=null)
			{
				$data['Whs']=$_SESSION['whsDN'][$j];
			}
		}
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']=$data['Remarks'];
		$dataJurnal['RefType']='DN';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		
		//$this->m_bp->updateBDO($data['BPId'],'intDelivery',$data['DocTotal']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemDN'];$j++)// save detail
			{
				if($_SESSION['itemcodeDN'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDN'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameDN'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsDN'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeDN']=$_SESSION['itemcodeDN'][$j];
					$da['itemnameDN']=$_SESSION['itemnameDN'][$j];
					$da['qtyDN']=$_SESSION['qtyDN'][$j];
					$da['whsDN']=$_SESSION['whsDN'][$j];
					$da['whsNameDN']=$whs;
					
					if($_SESSION['uomDN'][$j]==1)
					{
						$da['uomDN']=$vcUoM->vcUoM;
						$da['qtyinvDN']=$da['qtyDN'];
					}
					else if($_SESSION['uomDN'][$j]==2)
					{
						$da['uomDN']=$vcUoM->vcSlsUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intSlsUoM',1);
					}
					else if($_SESSION['uomDN'][$j]==3)
					{
						$da['uomDN']=$vcUoM->vcPurUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intPurUoM',1);
					}
					
					if(isset($_SESSION['idBaseRefDN'][$j]))
					{
						$da['idBaseRefDN']=$_SESSION['idBaseRefDN'][$j];
						$da['BaseRefDN']=$_SESSION['BaseRefDN'][$j];
					}
					else
					{
						$da['idBaseRefDN']=0;
						$da['BaseRefDN']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefDN']=='SQ')
					{
						$this->cekcloseSQ($da['idBaseRefDN'], $da['itemID'], $da['qtyDN'], $da['qtyinvDN']);
					}
					else if($da['BaseRefDN']=='SO')
					{
						$this->cekcloseSO($da['idBaseRefDN'], $da['itemID'], $da['qtyDN'], $da['qtyinvDN']);
					}
					else if($da['BaseRefDN']=='DO')
					{
						$this->cekcloseDO($da['idBaseRefDN'], $da['itemID'], $da['qtyDN'], $da['qtyinvDN']);
					}
					
					$da['uomtypeDN']=$_SESSION['uomDN'][$j];
					$da['uominvDN']=$vcUoM->vcUoM;
					$da['costDN']=$this->m_stock->GetCostItem($item,$da['whsDN']);
					
					$da['priceDN']= $_SESSION['priceDN'][$j];
					$da['discperDN'] = $_SESSION['discDN'][$j];
					$da['discDN'] = $_SESSION['discDN'][$j]/100*$da['priceDN'];
					$da['priceafterDN']=(100-$_SESSION['discDN'][$j])/100*$da['priceDN'];
					$da['linetotalDN']= $da['priceafterDN']*$da['qtyDN'];
					$da['linecostDN']=$da['costDN']*$da['qtyinvDN'];
					
					
					$detail=$this->m_dn->insertD($da);
					
					//start detail jurnal
					$actinv = $this->m_item_category->GetAccountByItemCode($da['itemcodeDN']);
					$data['dnconfirm']=$this->m_setting->getValueByCode('dn_confirm');
					$data['coa_inv_transit']=$this->m_setting->getValueByCode('coa_inv_transit');
					if($data['dnconfirm']=='Y')
					{
						$acthpp = $data['coa_inv_transit'];
					}
					else
					{
						$acthpp = $this->m_item_category->GetAccountHppByItemCode($da['itemcodeDN']);
					}
					$daJurnal['intHID']=$headJurnal;
					$daJurnal['DCJE']='D';
					
						$daJurnal['GLCodeJE']=$acthpp;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($daJurnal['GLCodeJE']);
						$daJurnal['GLCodeJEX']=$actinv;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($actinv);
						$daJurnal['ValueJE']=$da['linecostDN'];
						$val_min=$daJurnal['ValueJE']*-1;
					
					$detailJurnal=$this->m_jurnal->insertD($daJurnal);
					$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
					$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					//end detail jurnal
					
					$da['qtyinvDN']=$da['qtyinvDN']*-1;
					$this->m_stock->updateStock($item,$da['qtyinvDN'],$da['whsDN']);//update stok menambah/mengurangi di gudang
					$idmutasi = $this->m_stock->addMutation($item,$da['qtyinvDN'],$da['costDN'],$da['whsDN'],'DN',$data['DocDate'],$headDocnum);//add mutation
					//start batch
					
					$this->m_batch->BatchProcessingGI('DN',$item,$detail,$idmutasi,$batch,$da['whsDN']);
					
					//endbatch
				}
			}
			
		}
		$cekblock = $this->m_block->cekblock($head,'DN');
			
		if($cekblock=='')
		{
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			
			$this->db->trans_complete();
			echo $headDocnum;
		}
		else
		{
			echo $cekblock;
		}
		
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['CityH'] = isset($_POST['CityH'])?$_POST['CityH']:''; // get the requested page
		$data['CountryH'] = isset($_POST['CountryH'])?$_POST['CountryH']:''; // get the requested page
		$data['AddressH'] = isset($_POST['AddressH'])?$_POST['AddressH']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_dn->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
		$data['dnconfirm']=$this->m_setting->getValueByCode('dn_confirm');
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_dn->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table hover">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
		';
		if($data['dnconfirm']=='Y')
		{
			echo '<th>Confirmed</th>';
		}
		echo '
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatusName.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
			';
			if($data['dnconfirm']=='Y')
			{
				if($d->intConf==0)
				{
					$confmed='No';
				}
				else
				{
					$confmed='Yes';
				}
				echo '<td>'.$confmed.'</td>';
			}
			echo '
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_dn->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemDN'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]==$code)
			{
				$_SESSION['qtyDN'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenDN'][$j]=$_POST['detailQty'];
				$_SESSION['uomDN'][$j]=$_POST['detailUoM'];
				$_SESSION['priceDN'][$j]=$_POST['detailPrice'];
				$_SESSION['discDN'][$j]=$_POST['detailDisc'];
				$_SESSION['whsDN'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefDN'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeDN'][$i]=$code;
				$_SESSION['itemnameDN'][$i]=$_POST['detailItem'];
				$_SESSION['qtyDN'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenDN'][$i]=$_POST['detailQty'];
				$_SESSION['uomDN'][$i]=$_POST['detailUoM'];
				$_SESSION['priceDN'][$i]=$_POST['detailPrice'];
				$_SESSION['discDN'][$i]=$_POST['detailDisc'];
				$_SESSION['whsDN'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefDN'][$j]='';
				$_SESSION['statusDN'][$i]='O';
				$_SESSION['totitemDN']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeDN'][$j]="";
				$_SESSION['itemnameDN'][$j]="";
				$_SESSION['qtyDN'][$j]="";
				$_SESSION['qtyOpenDN'][$j]="";
				$_SESSION['uomDN'][$j]="";
				$_SESSION['priceDN'][$j]="";
				$_SESSION['discDN'][$j]="";
				$_SESSION['whsDN'][$j]="";
				$_SESSION['BaseRefDN'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		$data['uservalue']=$this->m_user->getByID($_SESSION['IDPOS'])->intValue;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Whs.</th>
		';
		if($data['uservalue']==1)
		{
			echo '
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  
			';
		}
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemDN'];$j++)
			{
				if($_SESSION['itemnameDN'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDN'][$j]);
					$cekbatch = $this->m_item->cekbatch($item);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameDN'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsDN'][$j]);
					
					if($_SESSION['uomDN'][$j]==1)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomDN'][$j]==2)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomDN'][$j]==3)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					
					if($_SESSION['discDN'][$j]=='')
					{
						$_SESSION['discDN'][$j]=0;
					}
					
					if($_SESSION['statusDN'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discDN'][$j])/100)*$_SESSION['priceDN'][$j]*$_SESSION['qtyDN'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeDN'][$j].'</td>
						<td>'.$_SESSION['itemnameDN'][$j].'</td>
					';
					if($cekbatch==1)
					{
						echo '
							<td align="right"><a href="#" onclick="changebatch(\''.$item.'\',\''.$_SESSION["qtyDN"][$j].'\',\''.$_SESSION["whsDN"][$j].'\')">'.number_format($_SESSION['qtyDN'][$j],'2').'</a></td>
						';
					}
					else
					{
						echo '
						<td align="right">'.number_format($_SESSION['qtyDN'][$j],'2').'</td>
						';
					}
						
					echo '
						<td align="right">'.number_format($_SESSION['qtyOpenDN'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td>'.$whs.'</td>
						';
					if($data['uservalue']==1)
					{
						echo '
							<td align="right">'.number_format($_SESSION['priceDN'][$j],'2').'</td>
							<td align="right">'.number_format($_SESSION['discDN'][$j],'2').' %</td>
							<td align="right">'.number_format($lineTotal,'2').'</td>
							
						';
					}
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusDN'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnameDN"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodeDN"][$j]).'\',\''.$_SESSION["qtyDN"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomDN"][$j]).'\',\''.$_SESSION["priceDN"][$j].'\',\''.$_SESSION["discDN"][$j].'\',\''.str_replace("'","\'",$_SESSION["whsDN"][$j]).'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodeDN"][$j]).'\',\''.$_SESSION["qtyDN"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function GetSumItem()
	{
		$data['DNNumber'] = isset($_POST['DNNumber'])?$_POST['DNNumber']:''; // get the requested page
		echo $this->m_dn->GetSumItem($data['DNNumber']);
	}
	function CekQtyConf()
	{
		$data['DNNumber'] = isset($_POST['DNNumber'])?$_POST['DNNumber']:''; // get the requested page
		//echo $data['DNNumber'];
		$dnconf = $this->m_dn->GetDataNotConfByDoc($data['DNNumber']);
		$data['idinput'] = isset($_POST['idinput'])?$_POST['idinput']:''; // get the requested page
		$data['i'] = isset($_POST['i'])?$_POST['i']:''; // get the requested page
		$list=$this->m_dn->GetDetailByHeaderID($dnconf->intID);
		$key=0;
		foreach($list->result() as $l)
		{
			if($key==$data['i'])
			{
				$qtyasli = $l->intOpenQtyInv;
				if($data['idinput']>$qtyasli)
				{
					echo 0;
				}
				else
				{
					echo 1;
				}
			}
			$key++;
		}
		
	}
	function SaveConf()
	{
		$data['DNNumber'] = isset($_POST['DNNumber'])?$_POST['DNNumber']:''; // get the requested page
		$ditm = isset($_POST['ditm'])?$_POST['ditm']:''; // get the requested page
		
		$dnconf = $this->m_dn->GetDataNotConfByDoc($data['DNNumber']);
		$list=$this->m_dn->GetDetailByHeaderID($dnconf->intID);
		$this->db->trans_begin();
		$this->m_dn->UpdateConf($dnconf->intID,1); // update field conf on db
		$data['header'] = $this->m_jurnal->GetHeaderJurnalByRefDocNum($data['DNNumber'],'DN'); //get header jurnal
		$i=0;
		foreach($list->result() as $l)
		{
			if($ditm[$i]>-1) // jika ada retur
			{
					$item=$l->intItem;
					$vcUoM=$this->m_item->GetUoMAllByName($l->vcItemName);
					$whs=$l->vcLocation;
					
					$da['intHID']=$dnconf->intID;
					$da['intID']=$l->intID;
					$da['itemID']=$item;
					$da['itemcodeDN']=$l->vcItemCode;
					$da['itemnameDN']=$l->vcItemName;
					$da['qtyDN']=$l->intQty-$ditm[$i];
					$da['qtyDNR']=$ditm[$i];
					$da['whsDN']=$l->intLocation;
					$da['whsNameDN']=$whs;
					
					if($l->intUoMType==1)
					{
						$da['uomDN']=$vcUoM->vcUoM;
						$da['qtyinvDN']=$da['qtyDN'];
						$da['qtyinvDNR']=$da['qtyDNR'];
					}
					else if($l->intUoMType==2)
					{
						$da['uomDN']=$vcUoM->vcSlsUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intSlsUoM',1);
						$da['qtyinvDNR']=$this->m_item->convert_qty($item,$da['qtyDNR'],'intSlsUoM',1);
					}
					else if($l->intUoMType==3)
					{
						$da['uomDN']=$vcUoM->vcPurUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intPurUoM',1);
						$da['qtyinvDNR']=$this->m_item->convert_qty($item,$da['qtyDNR'],'intPurUoM',1);
					}
					
					$da['uomtypeDN']=$l->intUoMType;
					$da['uominvDN']=$vcUoM->vcUoM;
					$da['costDN']=$l->intCost;
					
					$da['priceDN']= $l->intPrice;
					$da['discperDN'] = $l->intDiscPer;
					$da['discDN'] = $l->intDisc;
					$da['priceafterDN']=$l->intPriceAfterDisc;
					$da['linetotalDN']= $da['priceafterDN']*$da['qtyDN'];
					$da['linecostDN']=$da['costDN']*$da['qtyinvDN'];
					
					$da['linetotalDNR']= $da['priceafterDN']*$da['qtyDNR'];
					$da['linecostDNR']=$da['costDN']*$da['qtyinvDNR'];
					
					$detail=$this->m_dn->editD($da);
					
					//start detail jurnal barang retur
					$actinv = $this->m_item_category->GetAccountByItemCode($da['itemcodeDN']);
					$data['coa_inv_transit']=$this->m_setting->getValueByCode('coa_inv_transit');
					
					$acthpp = $data['coa_inv_transit'];
					
					$daJurnal['intHID']=$data['header']->intID;
					$daJurnal['DCJE']='D';
					
						$daJurnal['GLCodeJE']=$actinv;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
						$daJurnal['GLCodeJEX']=$acthpp;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
						$daJurnal['ValueJE']=$da['linecostDNR'];
						$val_min=$daJurnal['ValueJE']*-1;
					
					if($daJurnal['ValueJE']!=0)
					{
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end detail jurnal
					
					//start detail jurnal barang yg fix di kirim
					$actinv = $this->m_item_category->GetAccountHppByItemCode($da['itemcodeDN']);					
					$acthpp = $data['coa_inv_transit'];
					
					$daJurnal['intHID']=$data['header']->intID;
					$daJurnal['DCJE']='D';
					
						$daJurnal['GLCodeJE']=$actinv;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
						$daJurnal['GLCodeJEX']=$acthpp;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($acthpp);
						$daJurnal['ValueJE']=$da['linecostDN'];
						$val_min=$daJurnal['ValueJE']*-1;
					if($daJurnal['ValueJE']!=0)
					{
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					}
					//end detail jurnal
					
					$da['qtyinvDN']=$da['qtyinvDNR'];
					if($da['qtyinvDN']!=0)
					{
						$this->m_stock->updateStock($item,$da['qtyinvDN'],$da['whsDN']);//update stok menambah/mengurangi di gudang
						$this->m_stock->addMutation($item,$da['qtyinvDN'],$da['costDN'],$da['whsDN'],'DNR',$dnconf->dtDate,$data['DNNumber']);//add mutation
					}
				
			}
			$i++;
			$i++;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			echo 0;
			$this->db->trans_rollback();
		}else{
			echo $dnconf->intID;
			$this->db->trans_commit();
		}
	}
	function lisdnconf()
	{
		$data['DNNumber'] = isset($_GET['DNNumber'])?$_GET['DNNumber']:''; // get the requested page
		$dnconf = $this->m_dn->GetDataNotConfByDoc($data['DNNumber']);
		if($dnconf==null)
		{
			echo "Data Not Found";
		}
		else
		{
			$list=$this->m_dn->GetDetailByHeaderID($dnconf->intID);
			echo '
			<br>
				<table id="example4" class="table table-striped dt-responsive jambo_table" style="width:100%">
					<thead>
					<tr>
					  <th>Code</th>
					  <th>Name</th>
					  <th>Qty</th>
					  <th>Return Qty</th>
					  <th>UoM</th>
					  <th>Whs.</th>
					</tr>
					</thead>
					<tbody>'
			;
			$key=0;
			foreach($list->result() as $l)
			{
				echo '
					<tr>
					  <td>'.$l->vcItemCode.'</td>
					  <td>'.$l->vcItemName.'</td>
					  <td>'.$l->intOpenQtyInv.'</td>
					  <td><input type="number" class="form-control" id="dnitem'.$key.'" value="0" name="dnitem'.$key.'"></td>
					  <td>'.$l->vcUoMInv.'</td>
					  <td>'.$l->vcLocation.'.</td>
					</tr>
				';
				$key++;
			}
			echo '
				
				</tbody>
			</table>
			';
			echo '
			
			<script>
			  $(function () {
				$("#example4").DataTable({
					"oLanguage": {
					  "sSearch": "Search:"
					},
					\'iDisplayLength\': 10,
					//"sPaginationType": "full_numbers",
					"dom": \'T<"clear">lfrtip\',
					"tableTools": {
					  "sSwfPath": ""
					},
					dom: \'Blfrtip\',
					"aaSorting": [],
					buttons: [
					   {
						   extend: \'pdf\',
						   footer: false,
					   },
					   {
						   extend: \'csv\',
						   footer: false
						  
					   },
					   {
						   extend: \'excel\',
						   footer: false
					   }         
					]  
				});
			  });
			  
			 
			</script>';
			
			
		}
	}
}
