<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_batch extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_period','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_disc','',TRUE);
		$this->load->model('m_batch','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->library('message');
        $this->load->database();
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function get_new_batch_number()
	{
		echo $this->m_docnum->GetLastDocNum('mbatch');
	}
	function isnewbatchnumber()
	{
		if($_POST['BatchIn']==$this->m_docnum->GetLastDocNum('mbatch'))
		{
			echo 1;
		}else{
			echo 0;
		}
	}
	function getbatchfromheader()
	{
		$intHID 	= $this->m_batch->GetintHIDDoc($_POST['doc'],$_POST['DocNum'],$_POST['item']);
		if($intHID!=null)
		{
			$b 			= $this->m_batch->GetBatchByintHID($intHID,$_POST['doc']);
			$intBatch 	= $b->intBatch;
			$batch		= $this->m_batch->GetDocnumByID($intBatch);
			echo $batch;
		}
		echo '';
	}
	function getbatchfromheader2()
	{
		$intHID 	= $this->m_batch->GetintHIDDoc($_POST['doc'],$_POST['DocNum'],$_POST['item']);
		if($intHID!=null)
		{
			$b 			= $this->m_batch->GetBatchByintHID2($intHID,$_POST['doc']);
			foreach($b->result() as $bb)
			{
				echo $bb->intBatch."|".$bb->intQty.",";
			}
		}
		echo '';
	}
	function getbatchnumberbyid()
	{
		echo $this->m_batch->GetDocnumByID($_POST['batchid']);
	}
	function getbatchremarksbyid()
	{
		echo $this->m_batch->GetRemarksByID($_POST['idBatch']);
	}
	function getbatchremarksbydocnum()
	{
		$id = $this->m_batch->GetIdByDocNum($_POST['DocNum']);
		if($id!=0){
			echo $this->m_batch->GetRemarksByID($id);
		}else{
			echo '';
		}
		
	}
	function getbatchdatebyid()
	{
		echo $this->m_batch->GetDateByID($_POST['idBatch']);
	}
	function showmodallist()
	{
		$list = $this->m_batch->GetBatchByItemAndLoc($_GET['item'],$_GET['location']);
		echo '
		<br>
			<table id="batchlistmodal" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Batch Number</th>
				  <th align="right">Stock</th>
				  <th>Remarks</th>
                  <th>Date</th>
				</tr>
				</thead>
				<tbody>
		';
		foreach($list->result() as $l){
			echo '
				<tr onclick="insertBatch(\''.$l->vcDocNum.'\',\''.$_GET['dest'].'\');">
					<td>'.$l->vcDocNum.'</td>
					<td>'.number_format($l->intStock,2).'</td>
					<td>'.$l->vcRemarks.'</td>
					<td>'.$l->dtInsertTime.'</td>
				</tr>
			';
		}
		echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
			<script>
			  $(function () {
				$("#batchlistmodal").DataTable({
					"oLanguage": {
					  "sSearch": "Search:"
					},
					\'iDisplayLength\': 10,
					//"sPaginationType": "full_numbers",
					"dom": \'T<"clear">lfrtip\',
					"tableTools": {
					  "sSwfPath": ""
					},
					dom: \'Blfrtip\',
					"aaSorting": [],
					buttons: [
					   {
						   extend: \'pdf\',
						   footer: false,
					   },
					   {
						   extend: \'csv\',
						   footer: false
						  
					   },
					   {
						   extend: \'excel\',
						   footer: false
					   }         
					]  
				});
			  });
			  
			 
			</script>';
	}
	function getavailablebatchmin()
	{
		$list = $this->m_batch->GetBatchByItemAndLoc($_GET['item'],$_GET['Whs']);
		echo '
		<div class="row">
			<div class="col-sm-2">
			Batch Number<br>
			<select id="ABatch" name="ABatch" class="form-control select2" style="width: 100%;" data-toggle="tooltip" data-placement="top" title="Batch" onchange="getbatchdetail()">';
			foreach($list->result() as $l)
			{
				
				echo '<option value="'.$l->intID.'" >'.$l->vcDocNum.'</option>';
			}		
			echo '</select>
			</div>
			<div class="col-sm-2">
			Stock<br>
			<input type="text" class="form-control" id="StockBatch" name="StockBatch" placeholder="Stock" data-toggle="tooltip" data-placement="top" title="Stock" readonly="true">
			</div>
			<div class="col-sm-3">
			Remarks<br>
			<input type="text" class="form-control" id="RemarksBatch" name="RemarksBatch" placeholder="Remarks" data-toggle="tooltip" data-placement="top" title="Remarks" readonly="true">
			</div>
			<div class="col-sm-2">
			Date<br>
			<input type="text" class="form-control" id="DateBatch" name="DateBatch" placeholder="Date" data-toggle="tooltip" data-placement="top" title="Date" readonly="true">
			</div>
			<div class="col-sm-2">
			Qty<br>
			<input type="text" class="form-control" id="VBatch" name="VBatch" placeholder="Qty" data-toggle="tooltip" data-placement="top" title="Qty">
			</div>
			<div class="col-sm-1">
			Action<br>
			<button type="button" class="btn btn-primary" id="buttonadddetailbatchin" onclick="addDetailBatchIn()">Add</button>
			</div>';
		
		echo '
			</div>
		</div>';
	}
	function cekbatch()
	{
		echo $this->m_batch->CekBatch($_POST['BatchIn'],$_POST['ItemBatchIn'],$_POST['Whs']);
	}
	function cekavailablebatch()
	{
		if($_POST['batchtype']=='in')
		{
			$batch = json_decode($_POST['batch']);
			for($j=0;$j<$_SESSION['totitem'.$_POST['doc'].''];$j++)
			{
				$hasil[$j]	= 0;
				
				if($_SESSION['itemcode'.$_POST['doc'].''][$j]!='')
				{
					$item		= $this->m_item->GetIDByCode($_SESSION['itemcode'.$_POST['doc'].''][$j]);
					$cekbatch	= $this->m_item->cekbatch($item);
					if($cekbatch==0)
					{
						$hasil[$j]	= 1;
					}
					else
					{
						if(isset($_SESSION['BaseRefAR'][$j]))
						{
							if($_POST['doc']=='AR' and $_SESSION['BaseRefAR'][$j]=='DN') // jika ar dan copy dari DN maka tak perlu maintain batch
							{
								$hasil[$j]	= 1;
							}
						}
						if(isset($_SESSION['BaseRefARCM'][$j]))
						{
							if($_POST['doc']=='ARCM' and $_SESSION['BaseRefARCM'][$j]=='SR') // jika ar dan copy dari DN maka tak perlu maintain batch
							{
								$hasil[$j]	= 1;
							}
						}
						if(isset($_SESSION['BaseRefAP'][$j]))
						{
							if($_POST['doc']=='AP' and $_SESSION['BaseRefAP'][$j]=='GRPO') // jika ar dan copy dari DN maka tak perlu maintain batch
							{
								$hasil[$j]	= 1;
							}
						}
						if(isset($_SESSION['BaseRefAPCM'][$j]))
						{
							if($_POST['doc']=='APCM' and $_SESSION['BaseRefAPCM'][$j]=='PR') // jika ar dan copy dari DN maka tak perlu maintain batch
							{
								$hasil[$j]	= 1;
							}
						}
						if($_POST['doc']=='IP' and $_SESSION['qtyIP'][$j]<$_SESSION['qtybeforeIP'][$j])
						{// jika IP dan qty lbh kecil dari sebelumnya maka biarkan di cek di bagian out. Jadi ijinkanlah kali ini
							$hasil[$j]	= 1;
						}
						for($i=0;$i<count($batch);$i++)
						{
							if($item==$batch[$i][0])
							{
								$hasil[$j]	= 1;
							}
						}
					}
				}
				else
				{
					$hasil[$j]	= 1;
				}
			}
			$result		= 1 ;
			for($d=0;$d<count($hasil);$d++)
			{
				if($hasil[$d]==0)
				{
					$result = 0;
				}
			}
			echo $result;
		}
		else if($_POST['batchtype']=='out')
		{
			$batch = json_decode($_POST['batch']);
			for($j=0;$j<$_SESSION['totitem'.$_POST['doc'].''];$j++)
			{
				$hasil[$j]	= 0;
				
				if($_SESSION['itemcode'.$_POST['doc'].''][$j]!='')
				{
					$item		= $this->m_item->GetIDByCode($_SESSION['itemcode'.$_POST['doc'].''][$j]);
					$cekbatch	= $this->m_item->cekbatch($item);
					if($cekbatch==0) // jika item tidak maintain batch maka ijinkan
					{
						$hasil[$j]	= 1;
					}
					else
					{
						$totalqty = 0;
						for($i=0;$i<count($batch);$i++)
						{
							if($item==$batch[$i][0])// jika item maintain batch dan sdh ada batchnya ($batch[$i][0] berisi batch item tersebut)
							{
								if($_POST['doc']=='IT'){ // khusus IT qty disimpan diarray index 3
									$totalqty = $totalqty+(int) $batch[$i][3];
								}else{
									$totalqty = $totalqty+(int) $batch[$i][1];
								}
							}
						}
						if($_POST['doc']=='IP') // jika IP maka yg di cek adalah selisih qty
						{
							$qtyitem = $_SESSION['qtybefore'.$_POST['doc'].''][$j]-$_SESSION['qty'.$_POST['doc'].''][$j];
						}else
						{
							$qtyitem = $_SESSION['qty'.$_POST['doc'].''][$j];
						}
						
						if($totalqty==$qtyitem) // qty batch harus sama dengan qty detailnya
						{
							$hasil[$j]	= 1;
						}
						else
						{
							if($_POST['doc']=='AR' and $_SESSION['BaseRefAR'][$j]=='DN') // jika ar dan copy dari DN maka tak perlu maintain batch
							{
								$hasil[$j]	= 1;
							}else{
								$hasil[$j]	= 0;
							}
							if($_POST['doc']=='IP' and $_SESSION['qtyIP'][$j]>$_SESSION['qtybeforeIP'][$j])
							{// jika IP dan qty lbh besar dari sebelumnya maka biarkan di cek di bagian in. Jadi ijinkanlah kali ini
								$hasil[$j]	= 1;
							}
						}
					}
				}
				else
				{
					$hasil[$j]	= 1;
				}
			}
			$result		= 1 ;
			for($d=0;$d<count($hasil);$d++)
			{
				if($hasil[$d]==0)
				{
					$result = 0;
				}
			}
			echo $result;
		}
	}
	function getbatchstockbyid()
	{
		echo $this->m_batch->getbatchstockbyid($_POST['idBatch']);
	}
	function getbatchstockbydocnumlocation()
	{
		echo $this->m_batch->getbatchstockbydocnumlocation($_POST['Batch'],$_POST['Whs']);
	}
	function getitemnoneedbatch()
	{
		$noneedbatch = 0;
		for($j=0;$j<$_SESSION['totitem'.$_POST['doc'].''];$j++)
		{
			if($_SESSION['itemname'.$_POST['doc'].''][$j]==$_POST['detailItem'])
			{
				if($_POST['doc']=='AR'){
					if($_SESSION['BaseRefAR'][$j]=='DN')
					{
						$noneedbatch = 1;
					}
				}
				if($_POST['doc']=='ARCM'){
					if($_SESSION['BaseRefARCM'][$j]=='SR')
					{
						$noneedbatch = 1;
					}
				}
				if($_POST['doc']=='AP'){
					if($_SESSION['BaseRefAP'][$j]=='GRPO')
					{
						$noneedbatch = 1;
					}
				}
				if($_POST['doc']=='APCM'){
					if($_SESSION['BaseRefAPCM'][$j]=='PR')
					{
						$noneedbatch = 1;
					}
				}
			}
		}
		echo $noneedbatch;
	}
	function getitemisbatch()
	{
		$code		= $this->m_item->GetCodeByName($_POST['detailItem']);
		$item		= $this->m_item->GetIDByCode($code);
		$cekbatch	= $this->m_item->cekbatch($item);
		if($cekbatch==0)
		{
			echo 0;
		}else{
			echo $item;
		}
	}
}
