<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_function extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_period','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_disc','',TRUE);
		$this->load->model('m_batch','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->library('message');
        $this->load->database();
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function getstring()
	{
		echo $this->m_docnum->GetString($_POST['id'],$_POST['table'],$_POST['field']);
		
	}
	public function runcronjob()
	{
		$this->m_period->cekperiod($_POST['DocDate']);
	}
	public function cekpostperiod()
	{
		$result=$this->m_period->cekperiod($_POST['DocDate']);
		echo $result;
	}
	function loaddisc()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$whs=$this->m_location->GetIDByName($_POST['WhsName']);
		echo $this->m_disc->GetDiscByItemAndWhs($item,$whs);
	}
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function GetPriceBPItem()
	{
		$idbp = $this->m_bp->getIDByName($_POST['BPName']);
		$iditem= $this->m_item->GetIDByName($_POST['detailItem']);
		$data=$this->m_price->getpricebybpanditem($idbp,$iditem);
		echo $data;
	}
	
	function GetIDItem()
	{
		echo $this->m_item->GetIDByName($_POST['detailItem']);
	}
	function GetIDItemByCode()
	{
		echo $this->m_item->GetIDByCode($_POST['detailItem']);
	}
	function CekUoM()
	{
		echo $this->m_item->CekUoM($_GET['UoM']);
	}
	function TesSession()
	{
		/*unset($_SESSION['totitemBatch']);
		$_SESSION['totitemBatch'][1] = 'A';
		$_SESSION['totitemBatch']["XXXX"] = 'B';
		$keys = array_keys($_SESSION['totitemBatch']);
		$key = $keys[1];
		echo sizeof($_SESSION['totitemBatch']);*/
		//echo $_SESSION['QtyBatch'][23];
		//$long =sizeof($_SESSION['totitemBatch']);
		//$keys = array_keys($_SESSION['totitemBatch']);
		//$key = $keys[0];
		//echo $long;
		echo $_SESSION['QtyBatch'][25][0];
		//echo $_SESSION['totitemBatch'][24];
		
	}
	
	function showsession()
	{
		echo $_SESSION['DocNumBatch'][1]."|";
		echo $_SESSION['QtyBatch'][1]."|";
	}
	function loadadrsbybpcode()
	{
		$bpcode=$_POST['bpcode'];
		$type=$_POST['type'];
		$r=$this->m_bp->GetAddressByCode($bpcode);
		$responce=new stdClass();
		if($type=='bill' and $bpcode!='')
		{
			$responce->CityH = $r->vcBillToCity;
			$responce->CountryH = $r->vcBillToCountry;
			$responce->AddressH = $r->vcBillToAddress;
		}
		else if($type=='ship' and $bpcode!='')
		{
			$responce->CityH = $r->vcShipToCity;
			$responce->CountryH = $r->vcShipToCountry;
			$responce->AddressH = $r->vcShipToAddress;
		}
		echo json_encode($responce);
	}
	function loadadrsdata()
	{
			echo '
			<div class="form-group">
				<label class="col-sm-4 control-label" style="height:20px">City</label>
			';
			
				echo'	
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="CityH" name="CityH" maxlength="100">
					</div>
				';
			echo '
			</div>
			';
			
			echo '
			<div class="form-group">
				<label class="col-sm-4 control-label" style="height:20px">Country</label>
			';
			
				echo'	
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="CountryH" name="CountryH" maxlength="100">
					</div>
				';
			echo '
			</div>
			';
			
			echo '
			<div class="form-group">
				<label class="col-sm-4 control-label" style="height:20px">Address</label>
			';
				echo'	
					<div class="col-sm-8">
					<textarea class="form-control" rows="3" id="AddressH" name="AddressH"
						  data-toggle="tooltip" data-placement="top" title="Address"
					></textarea>
					</div>
				';
			echo '
			</div>
			';
	}
}
