<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_udf extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_udf','',TRUE);
		
		$this->load->library('message');
        $this->load->database();
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function loadhtml()
	{
		$data['doc'] = isset($_GET['doc'])?$_GET['doc']:''; // get the requested page
		
		$udf = $this->m_udf->GetDataByDoc($data['doc']);
		
		foreach($udf->result() as $u)
		{
			
			echo '
			<div class="form-group">
				<label class="col-sm-4 control-label" style="height:20px">'.$u->vcName.'</label>
			';
			if($u->vcType=='text')
			{
				echo'	
					<div class="col-sm-8" style="height:45px">
					<input type="text" class="form-control" id="'.$u->vcCode.'" name="'.$u->vcCode.'" maxlength="'.$u->intLength.'">
					</div>
				';
			}
			else if($u->vcType=='number')
			{
				echo'	
					<div class="col-sm-8" style="height:45px">
					<input type="number" class="form-control" id="'.$u->vcCode.'" name="'.$u->vcCode.'" maxlength="'.$u->intLength.'">
					</div>
				';
			}
			else if($u->vcType=='textarea')
			{
				echo'	
					<div class="col-sm-8">
					<textarea class="form-control" rows="'.$u->intLength.'" id="'.$u->vcCode.'" name="'.$u->vcCode.'"
						  data-toggle="tooltip" data-placement="top" title="'.$u->vcName.'"
					></textarea>
					</div>
				';
			}
			echo '
			</div>
			';
		}
	}
	function saveudf()
	{
		$data['Doc'] = isset($_GET['Doc'])?$_GET['Doc']:''; // get the requested page
		$data['ID'] = isset($_GET['ID'])?$_GET['ID']:''; // get the requested page
		$data['Val'] = isset($_GET['Val'])?$_GET['Val']:''; // get the requested page
		$this->m_udf->SaveUdf($data);
	}
	function editudf()
	{
		$data['Doc'] = isset($_GET['Doc'])?$_GET['Doc']:''; // get the requested page
		$data['ID'] = isset($_GET['ID'])?$_GET['ID']:''; // get the requested page
		$data['Val'] = isset($_GET['Val'])?$_GET['Val']:''; // get the requested page
		$this->m_udf->EditUdf($data);
	}
	function loadudf()
	{
		$data['Doc'] = isset($_GET['Doc'])?$_GET['Doc']:''; // get the requested page
		$data['ID'] = isset($_GET['ID'])?$_GET['ID']:''; // get the requested page
		echo $this->m_udf->LoadUdf($data);
	}
}
