<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_inpay','',TRUE);
		$this->load->model('m_outpay','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_group_cf','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_udf','',TRUE);
		
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_jurnal','',TRUE);

		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			
			$data['datatype'] = isset($_GET['data'])?$_GET['data']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Import',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function emptysession()
	{
		unset($_SESSION['totitemIMPORT']);
		unset($_SESSION['messageIMPORT']);
		
		//variant
		unset($_SESSION['Code']);
		unset($_SESSION['Name']);
		unset($_SESSION['Price']);
		//end item
		
		//item
		unset($_SESSION['Category']);
		unset($_SESSION['Code']);
		unset($_SESSION['Name']);
		unset($_SESSION['Price']);
		unset($_SESSION['Tax']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['UoM']);
		unset($_SESSION['PurUoM']);
		unset($_SESSION['intPurUoM']);
		unset($_SESSION['SlsUoM']);
		unset($_SESSION['intSlsUoM']);
		unset($_SESSION['intSalesItem']);
		unset($_SESSION['intPurchaseItem']);
		unset($_SESSION['intSalesWithoutQty']);
		unset($_SESSION['Barcode']);
		unset($_SESSION['intBatch']);
		unset($_SESSION['intService']);
		//end item
		
		//ib
		unset($_SESSION['Date']);
		unset($_SESSION['ItemCode']);
		unset($_SESSION['Type']);
		unset($_SESSION['Location']);
		unset($_SESSION['Qty']);
		unset($_SESSION['Cost']);
		unset($_SESSION['Account']);
		//endib
		
		
		//price
		unset($_SESSION['Category']);
		unset($_SESSION['ItemCode']);
		unset($_SESSION['Price']);
		//endprice
		
		//Inpay
		unset($_SESSION['ARNumber']);
		unset($_SESSION['DocDateInpay']);
		unset($_SESSION['WalletInpay']);
		//endinpay
		
		//Outpay
		unset($_SESSION['APNumber']);
		unset($_SESSION['DocDateOutpay']);
		unset($_SESSION['WalletOutpay']);
		//endoutpay
		
		//bp
		unset($_SESSION['Type']);
		unset($_SESSION['Category']);
		unset($_SESSION['Code']);
		unset($_SESSION['Name']);
		unset($_SESSION['TaxNumber']);
		unset($_SESSION['PriceList']);
		unset($_SESSION['CreditLimit']);
		unset($_SESSION['PaymentTerm']);
		unset($_SESSION['Tax']);
		unset($_SESSION['Phone1']);
		unset($_SESSION['Phone2']);
		unset($_SESSION['Fax']);
		unset($_SESSION['Email']);
		unset($_SESSION['Website']);
		unset($_SESSION['ContactPerson']);
		unset($_SESSION['CityBillTo']);
		unset($_SESSION['CountryBillTo']);
		unset($_SESSION['AddressBillTo']);
		unset($_SESSION['CityShipTo']);
		unset($_SESSION['CountryShipTo']);
		unset($_SESSION['AddressShipTo']);
		
		//bp
		
		//ar and ap
		unset($_SESSION['BPCode']);
		unset($_SESSION['RefNum']);
		unset($_SESSION['DocDate']);
		unset($_SESSION['DelDate']);
		unset($_SESSION['SalesEmp']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['DocTotal']);
		unset($_SESSION['Location']);
		//ar and ap
		
		//bom
		unset($_SESSION['DocDate']);
		unset($_SESSION['FGItem']);
		unset($_SESSION['Whs']);
		unset($_SESSION['FGQty']);
		unset($_SESSION['Rev']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['itemcodeBOM']);
		unset($_SESSION['qtyBOM']);
		unset($_SESSION['autoBOM']);
		//bom
		
		//udf
		unset($_SESSION['DocType']);
		unset($_SESSION['UdfCode']);
		unset($_SESSION['Key']);
		unset($_SESSION['Value']);
		//udf
	}
	public function reloaddownload()
	{
		echo '<i class="fa fa-file-excel-o"></i><br>';
		if($_GET['Type']=='item')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_item_master_data.xls">download template item master data</a>';
		}
		if($_GET['Type']=='variant')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_item_variant_data.xls">download template variant data</a>';
		}
		if($_GET['Type']=='ib')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_adjustment.xls">download template initial balance stock</a>';
		}
		if($_GET['Type']=='price')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_price.xls">download template price list</a>';
		}
		if($_GET['Type']=='bp')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_bp.xls">download template business partner</a>';
		}
		if($_GET['Type']=='ar')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_ar_invo.xls">download template a/r invoice</a>';
		}
		if($_GET['Type']=='ap')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_ap_invo.xls">download template a/p invoice</a>';
		}
		if($_GET['Type']=='bom')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_bom.xls">download template BOM</a>';
		}
		if($_GET['Type']=='udf')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_udf.xls">download template UDF</a>';
		}
		if($_GET['Type']=='inpay')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_inpay.xls">download template Incoming Payment</a>';
		}
		if($_GET['Type']=='outpay')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_outpay.xls">download template Outgoing Payment</a>';
		}
	}
	public function upload()
	{
		if($_GET['type']=='variant')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['Id'] = $this->m_item->GetIDByCode($_SESSION['Code'][$i]);
				$data['VariantName']	= $_SESSION['Name'][$i];
				$data['VariantPrice']	= $_SESSION['Price'][$i];
				if($data['Id']==null)
				{
					$upload=0;
					$message=$message.'Item Not Found, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_item->addvariant($data);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=variant&finish=true';
		}
		if($_GET['type']=='item')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_item_category->GetIDByCode($data['Category']);
				if($data['Category']!=0)
				{
					$data['Code'] = $this->m_item->GetLastCode($data['Category']);
				}
				else
				{
					$data['Code']=0;
				}
				$data['Name'] =  $_SESSION['Name'][$i];
				$samename=$this->m_item->GetIDByName($data['Name']);
				$data['Price'] =  $_SESSION['Price'][$i];
				$data['Tax'] =  $_SESSION['Tax'][$i];
				$data['Tax'] = $this->m_tax->GetIDByCode($data['Tax']);
				$data['Remarks'] =  $_SESSION['Remarks'][$i];
				$data['UoM'] =  $_SESSION['UoM'][$i];
				$data['PurUoM'] =  $_SESSION['PurUoM'][$i];
				$data['intPurUoM'] =  $_SESSION['intPurUoM'][$i];
				
				$data['SlsUoM'] =  $_SESSION['SlsUoM'][$i];
				$data['intSlsUoM'] =  $_SESSION['intSlsUoM'][$i];
				
				//setting
				$data['intSalesItem'] =  $_SESSION['intSalesItem'][$i];
				$data['intPurchaseItem'] =  $_SESSION['intPurchaseItem'][$i];
				$data['intSalesWithoutQty'] =  $_SESSION['intSalesWithoutQty'][$i];
				$data['Barcode'] =  $_SESSION['Barcode'][$i];
				$data['intBatch'] =  $_SESSION['intBatch'][$i];
				$data['intService'] =  $_SESSION['intService'][$i];
				$data['imgData']=null;
				
				
				if($data['Category']==0)
				{
					$upload=0;
					$message=$message.'Item Category Not Found, ';
				}
				if (is_numeric($data['Price'])==false)
				{
					$upload=0;
					$message=$message.'The price is only allowed with the format number, ';
				}
				if($data['Tax']==0)
				{
					$upload=0;
					$message=$message.'Tax Not Found, ';
				}
				if (is_numeric($data['intPurUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intPurUoM is only allowed with the format number, ';
				}
				if (is_numeric($data['intPurUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intPurUoM is only allowed with the format number, ';
				}
				if (is_numeric($data['intSlsUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intSlsUoM is only allowed with the format number, ';
				}
				if ($data['intSalesItem']!=0 and $data['intSalesItem']!=1)
				{
					$upload=0;
					$message=$message.'The intSalesItem is only allowed 0 or 1, ';
				}
				if ($data['intPurchaseItem']!=0 and $data['intPurchaseItem']!=1)
				{
					$upload=0;
					$message=$message.'The intPurchaseItem is only allowed 0 or 1, ';
				}
				if ($data['intSalesWithoutQty']!=0 and $data['intSalesWithoutQty']!=1)
				{
					$upload=0;
					$message=$message.'The intSalesWithoutQty is only allowed 0 or 1, ';
				}
				if ($samename!=null)
				{
					$upload=0;
					$message=$message.'You already have a master data with this name, ';
				}
				
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_item->insert($data);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=item&finish=true';
		}
		if($_GET['type']=='ib')
		{
			$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAdjustment');
			$data['DocDate'] = $_SESSION['Date'][0];
			$data['RefNum'] = 'Initial Balance';
			$data['Whs'] = $this->m_location->GetIDByName($_SESSION['Location'][0]);
			$data['Type'] = $_SESSION['Type'][0];
			$data['Account'] = $_SESSION['Account'][0];
			$data['Remarks'] = 'Initial Balance';
			$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
			if($data['Whs']!=null and ( $data['Type']=='GR' or $data['Type']=='GI') and DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE)
			{
				$this->db->trans_begin();
				$head=$this->m_adjustment->insertH($data);
				//proses jurnal
				$headDocnum=$this->m_adjustment->GetHeaderByHeaderID($head)->vcDocNum;
				$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
				$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
				$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
				$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
				$dataJurnal['Remarks']='';
				$dataJurnal['RefType']='AD';
				$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
				
				if($head!=0)
				{
					$itemuplod=0;
					for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
					{
						$message='';
						$upload=1;
						
						if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
						{
						  // it's a date
						}
						else
						{
							$upload=0;
							$message=$message.'Your Date Format is Wrong, ';
						}
						$item=$this->m_item->GetIDByCode($_SESSION['ItemCode'][$i]);
						if($item==null)
						{
							$upload=0;
							$message=$message.'Item Code Not Found, ';
						}
						if($_SESSION['Type'][$i]!='GR' and $_SESSION['Type'][$i]!='GI')
						{
							$upload=0;
							$message=$message.'Unknown Type, ';
						}
						$dwhs=$this->m_location->GetIDByName($_SESSION['Location'][$i]);
						if($dwhs==null)
						{
							$upload=0;
							$message=$message.'Location Not Found, ';
						}
						if(is_numeric($_SESSION['Qty'][$i])==false)
						{
							$upload=0;
							$message=$message.'The Qty is only allowed with the format number, ';
						}
						if(is_numeric($_SESSION['Cost'][$i])==false)
						{
							$upload=0;
							$message=$message.'The Cost is only allowed with the format number, ';
						}
						if($upload==1)	
						{
							$cek=1;
							$item=$this->m_item->GetIDByCode($_SESSION['ItemCode'][$i]);
							$da['intHID']=$head;
							$da['itemcodeAD']=$_SESSION['ItemCode'][$i];
							$da['itemnameAD']=$this->m_item->GetNameByCode($_SESSION['ItemCode'][$i]);
							$da['Whs']=$data['Whs'];
							if($data['Type']=='GR')
							{
								$da['qtyAD']=$_SESSION['Qty'][$i];
							}
							else if($data['Type']=='GI')
							{
								$da['qtyAD']=$_SESSION['Qty'][$i]*-1;
							}
							$da['uomAD']=$this->m_item->GetUoMByID($item);
							$da['costAD']=$_SESSION['Cost'][$i];
							$detail=$this->m_adjustment->insertD($da);
							
							//start detail jurnal
							$actinv = $this->m_item_category->GetAccountByItemCode($da['itemcodeAD']);
							$daJurnal['intHID']=$headJurnal;
							$daJurnal['DCJE']='D';
							if($da['qtyAD']<0)
							{
								$daJurnal['ValueJE']=$da['costAD']*$da['qtyAD']*-1;
							}
							else
							{
								$daJurnal['ValueJE']=$da['costAD']*$da['qtyAD'];
							}
							$val_min=$daJurnal['ValueJE']*-1;
							
							if($data['Type']=='GR')
							{
								$daJurnal['GLCodeJE']=$actinv;
								$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($actinv);
								$daJurnal['GLCodeJEX']=$data['Account'];
								$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($data['Account']);
							}
							else if($data['Type']=='GI')
							{
								$daJurnal['GLCodeJE']=$data['Account'];
								$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($data['Account']);
								$daJurnal['GLCodeJEX']=$actinv;
								$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($actinv);
							}
							
							$detailJurnal=$this->m_jurnal->insertD($daJurnal);
							$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
							$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
							//end detail jurnal
							
							$itemuplod++;
							$this->m_stock->updateStock($item,$da['qtyAD'],$data['Whs']);//update stok menambah/mengurangi di gudang
							if($data['Type']=='GR')
							{
								$this->m_stock->updateCost($item,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
							}
							$this->m_stock->addMutation($item,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data['DocDate'],$data['DocNum']);//add mutation
							$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
						}
						else
						{
							$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
						}
					}
				}
				if($itemuplod==0)
				{
					$this->m_adjustment->deleteH($head);
				}
				
				$this->db->trans_complete();
			
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib&finish=true';
			}
			else
			{
				$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib&error=true';
			}
		}
		if($_GET['type']=='price')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_price->GetIDByName($data['Category']);
				
				$data['ItemCode'] = $_SESSION['ItemCode'][$i];
				$data['Item'] = $this->m_item->GetIDByCode($data['ItemCode']);
				
				$data['Price'] = $_SESSION['Price'][$i];
				
				if($data['Category']==null)
				{
					$upload=0;
					$message=$message.'Price Name Not Found, ';
				}
				if($data['Item']==null)
				{
					$upload=0;
					$message=$message.'Item Not Found, ';
				}
				if (is_numeric($data['Price'])==false)
				{
					$upload=0;
					$message=$message.'Price is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_price->editByItem($data['Category'],$data['Item'],$data['Price']);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=price&finish=true';
		}
		if($_GET['type']=='bp')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				//general
				$data['Type'] = $_SESSION['Type'][$i];
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_bp_category->GetIDByName($data['Category']);
				$data['Code'] = $this->m_bp->GetLastCode($data['Type']);
				$data['Name'] = $_SESSION['Name'][$i];
				$data['TaxNumber'] = $_SESSION['TaxNumber'][$i];
				$data['PriceList'] = $_SESSION['PriceList'][$i];
				$data['PriceList'] = $this->m_price->GetIDByName($data['PriceList']);
				$data['CreditLimit'] = $_SESSION['CreditLimit'][$i];
				$data['PaymentTerm'] = $_SESSION['PaymentTerm'][$i];
				$data['Tax'] = $_SESSION['Tax'][$i];
				$data['Tax'] = $this->m_tax->GetIDByCode($data['Tax']);
				//contact
				$data['Phone1'] = $_SESSION['Phone1'][$i];
				$data['Phone2'] = $_SESSION['Phone2'][$i];
				$data['Fax'] = $_SESSION['Fax'][$i];
				$data['Email'] = $_SESSION['Email'][$i];
				$data['Website'] = $_SESSION['Website'][$i];
				$data['ContactPerson'] = $_SESSION['ContactPerson'][$i];
				
				//address
				$data['CityBillTo'] = $_SESSION['CityBillTo'][$i];
				$data['CountryBillTo'] = $_SESSION['CountryBillTo'][$i];
				$data['AddressBillTo'] = $_SESSION['AddressBillTo'][$i];
				$data['CityShipTo'] = $_SESSION['CityShipTo'][$i];
				$data['CountryShipTo'] = $_SESSION['CountryShipTo'][$i];
				$data['AddressShipTo'] = $_SESSION['AddressShipTo'][$i];
				
				if($data['Type']!='C' and $data['Type']!='S')
				{
					$upload=0;
					$message=$message.'Type is only allowed C or S, ';
				}
				if($data['Category']==null)
				{
					$upload=0;
					$message=$message.'Category Not Found, ';
				}
				if($data['PriceList']==null)
				{
					$upload=0;
					$message=$message.'Price Not Found, ';
				}
				if($data['Tax']==0)
				{
					$upload=0;
					$message=$message.'Tax Not Found, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_bp->insert($data);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bp&finish=true';
		}
		if($_GET['type']=='outpay')
		{
			
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				if($upload==1)
				{
					$headDocnum		= $_SESSION['APNumber'][$i];
					$head			= $this->m_ap->GetHeaderByDocNum($headDocnum)->intID;
					$DocDate		= $_SESSION['DocDateOutpay'][$i];
					$Wallet			= $_SESSION['WalletOutpay'][$i];
					
					$_SESSION['totitemOUTPAY']=0;
					unset($_SESSION['docnumOUTPAY']);
					unset($_SESSION['idOUTPAY']);
					unset($_SESSION['idBaseRefOUTPAY']);
					unset($_SESSION['checkOUTPAY']);
					
					$j=0;
					$_SESSION['idOUTPAY'][$j]=0;
					$_SESSION['HIDOUTPAY'][$j]=0;
					$_SESSION['idBaseRefOUTPAY'][$j]=$head;
					$_SESSION['BaseRefOUTPAY'][$j]='AP';
					$_SESSION['docnumOUTPAY'][$j]=$headDocnum;
					$_SESSION['DocTotalOUTPAY'][$j]=$this->m_ap->GetHeaderByHeaderID($head)->intDocTotal;
					$_SESSION['AppliedOUTPAY'][$j]=$this->m_ap->GetHeaderByHeaderID($head)->intApplied;
					
					$j++;
					$_SESSION['totitemOUTPAY']=$j;
					
					//
					$data['DocNum'] = '';
					$data['BPCode'] = $this->m_ap->GetHeaderByHeaderID($head)->vcBPCode;
					$data['BPName'] = $this->m_ap->GetHeaderByHeaderID($head)->vcBPName;
					$data['DocDate'] = date('Y-m-d',strtotime($DocDate));
					$data['RefNum'] = '';
					$data['Remarks'] = '';
					$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
					
					$data['DocTotalBefore'] = $this->m_ap->GetHeaderByHeaderID($head)->intDocTotal;
					$data['AppliedAmount'] = $this->m_ap->GetHeaderByHeaderID($head)->intBalance;
					$data['BalanceDue'] = $data['DocTotalBefore'] - $data['AppliedAmount'];
					$data['Wallet'] = $this->m_wallet->GetIDByCode($Wallet);
					
					$sisa=$data['AppliedAmount'];
					
					$head=$this->m_outpay->insertH($data);
					
					$headDocnum=$this->m_outpay->GetHeaderByHeaderID($head)->vcDocNum;
					//proses jurnal
					$dataJurnal['Plan']=$this->m_wallet->GetPlanByID($data['Wallet']);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=date('Y-m-d');
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='OUTPAY';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					if($head!=0)
					{
						for($j=0;$j<$_SESSION['totitemOUTPAY'];$j++)// save detail
						{
							if($_SESSION['docnumOUTPAY'][$j]!="")
							{
								$da['intHID']=$head;
								$da['intBaseRef']=$_SESSION['idBaseRefOUTPAY'][$j];
								$da['vcBaseType']=$_SESSION['BaseRefOUTPAY'][$j];
								$da['vcDocNum']=$_SESSION['docnumOUTPAY'][$j];
								$da['intDocTotal']=$_SESSION['DocTotalOUTPAY'][$j];
								$da['intBalance']=$_SESSION['DocTotalOUTPAY'][$j]-$_SESSION['AppliedOUTPAY'][$j];
									
								if($da['intBalance']<=$sisa)
								{
									$da['intApplied']=$da['intBalance'];
									$sisa=$sisa-$da['intBalance'];
								}
								else
								{
									if($sisa<=0)
									{
										$da['intApplied']=0;
									}
									else
									{
										$da['intApplied']=$sisa;
									}
									$sisa=$sisa-$da['intBalance'];
								}
								$detail=$this->m_outpay->insertD($da);
															
								//jurnal entry
								if($_SESSION['BaseRefOUTPAY'][$j]=='AP')
								{
									$data['BPCode'] = $this->m_ap->GetHeaderByHeaderID($_SESSION['idBaseRefOUTPAY'][$j])->vcBPCode;
									$debact 		= $this->m_bp_category->GetApActByBPCode($data['BPCode']);
									$creact 		= $this->m_wallet->GetGLByID($data['Wallet']);
									$daJurnal['ValueJE']=$da['intApplied'];
								}
										
								$daJurnal['intHID']=$headJurnal;
								$daJurnal['DCJE']='D';
												
								$daJurnal['GLCodeJE']=$debact;
								$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
								$daJurnal['GLCodeJEX']=$creact;
								$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
								$val_min=$daJurnal['ValueJE']*-1;
								$detailJurnal=$this->m_jurnal->insertD($daJurnal);
								$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
								$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
								//end jurnal entry
								
								$da['intAppliedminus']=$da['intApplied']*-1;
								
								$cf 	= $this->m_setting->getValueByCode('ven_group_cf');
								$idcf	= $this->m_group_cf->GetIDByCode($cf);
								
								$mutremarks = "Payment AP-".$da['vcDocNum']."";
								$this->m_wallet->addMutation($data['Wallet'],$data['DocDate'],$da['vcBaseType'],$da['intAppliedminus'],$da['vcDocNum'],$headDocnum,$idcf,$mutremarks);// add mutation wallet
								$this->m_wallet->updateBalance($data['Wallet'],$da['intAppliedminus']); // change wallet balance
								$da['intAppliedminus']=$da['intApplied']*-1;
								if($da['vcBaseType']=='AP')
								{
									$this->m_ap->cekclose($da['intBaseRef'],$da['intApplied']); // set ar to close
								}
								
							}
						}
					}
			
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=outpay&finish=true';
		}
		if($_GET['type']=='inpay')
		{
			
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				
				if($upload==1)
				{
					
					$headDocnum		= $_SESSION['ARNumber'][$i];
					$head			= $this->m_ar->GetHeaderByDocNum($headDocnum)->intID;
					$DocDate		= $_SESSION['DocDateInpay'][$i];
					$Wallet			= $_SESSION['WalletInpay'][$i];
					
					$_SESSION['totitemINPAY']=0;
					unset($_SESSION['docnumINPAY']);
					unset($_SESSION['idINPAY']);
					unset($_SESSION['idBaseRefINPAY']);
					unset($_SESSION['checkINPAY']);
					
					$j=0;
					$_SESSION['idINPAY'][$j]=0;
					$_SESSION['HIDINPAY'][$j]=0;
					$_SESSION['idBaseRefINPAY'][$j]=$head;
					$_SESSION['BaseRefINPAY'][$j]='AR';
					$_SESSION['docnumINPAY'][$j]=$headDocnum;
					$_SESSION['DocTotalINPAY'][$j]=$this->m_ar->GetHeaderByHeaderID($head)->intDocTotal;
					$_SESSION['AppliedINPAY'][$j]=$this->m_ar->GetHeaderByHeaderID($head)->intApplied;
					
					$j++;
					$_SESSION['totitemINPAY']=$j;
				
					//
					$data['DocNum'] = '';
					$data['BPCode'] = $this->m_ar->GetHeaderByHeaderID($head)->vcBPCode;
					$data['BPName'] = $this->m_ar->GetHeaderByHeaderID($head)->vcBPName;
					$data['DocDate'] = date('Y-m-d',strtotime($DocDate));
					$data['RefNum'] = '';
					$data['Remarks'] = '';
					$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
					
					$data['DocTotalBefore'] = $this->m_ar->GetHeaderByHeaderID($head)->intDocTotal;
					$data['AppliedAmount'] = $this->m_ar->GetHeaderByHeaderID($head)->intBalance;
					$data['BalanceDue'] = $data['DocTotalBefore'] - $data['AppliedAmount'];
					$data['Wallet'] = $this->m_wallet->GetIDByCode($Wallet);
					
					$sisa=$data['AppliedAmount'];
					
					$head=$this->m_inpay->insertH($data);
					
					$headDocnum=$this->m_inpay->GetHeaderByHeaderID($head)->vcDocNum;
					//proses jurnal
				
					$dataJurnal['Plan']=$this->m_wallet->GetPlanByID($data['Wallet']);//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=date('Y-m-d');
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='INPAY';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					if($head!=0)
					{
						for($j=0;$j<$_SESSION['totitemINPAY'];$j++)// save detail
						{
							if($_SESSION['docnumINPAY'][$j]!="")
							{
								$da['intHID']=$head;
								$da['intBaseRef']=$_SESSION['idBaseRefINPAY'][$j];
								$da['vcBaseType']=$_SESSION['BaseRefINPAY'][$j];
								$da['vcDocNum']=$_SESSION['docnumINPAY'][$j];
								$da['intDocTotal']=$_SESSION['DocTotalINPAY'][$j];
								$da['intBalance']=$_SESSION['DocTotalINPAY'][$j]-$_SESSION['AppliedINPAY'][$j];
									
								if($da['intBalance']<=$sisa)
								{
									$da['intApplied']=$da['intBalance'];
									$sisa=$sisa-$da['intBalance'];
								}
								else
								{
									if($sisa<=0)
									{
										$da['intApplied']=0;
									}
									else
									{
										$da['intApplied']=$sisa;
									}
									$sisa=$sisa-$da['intBalance'];
								}
								$detail=$this->m_inpay->insertD($da);
														
								//jurnal entry
								if($_SESSION['BaseRefINPAY'][$j]=='AR')
								{
									$data['BPCode'] = $this->m_ar->GetHeaderByHeaderID($_SESSION['idBaseRefINPAY'][$j])->vcBPCode;
									$debact 		= $this->m_wallet->GetGLByID($data['Wallet']);
									$creact 		= $this->m_bp_category->GetArActByBPCode($data['BPCode']);
									$daJurnal['ValueJE']=$da['intApplied'];
								}
										
								$daJurnal['intHID']=$headJurnal;
								$daJurnal['DCJE']='D';
												
								$daJurnal['GLCodeJE']=$debact;
								$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
								$daJurnal['GLCodeJEX']=$creact;
								$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
								$val_min=$daJurnal['ValueJE']*-1;
								$detailJurnal=$this->m_jurnal->insertD($daJurnal);
								$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
								$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
								//end jurnal entry
								
								$cf 	= $this->m_setting->getValueByCode('cus_group_cf');
								$idcf	= $this->m_group_cf->GetIDByCode($cf);
								
								$mutremarks = "Payment AR-".$da['vcDocNum']."";
								$this->m_wallet->addMutation($data['Wallet'],$data['DocDate'],$da['vcBaseType'],$da['intApplied'],$da['vcDocNum'],$headDocnum,$idcf,$mutremarks);// add mutation wallet
								$this->m_wallet->updateBalance($data['Wallet'],$da['intApplied']); // change wallet balance
								$da['intAppliedminus']=$da['intApplied']*-1;
								if($da['vcBaseType']=='AR')
								{
									$this->m_ar->cekclose($da['intBaseRef'],$da['intApplied']); // set ar to close
								}
							}
							
						}
						
					}
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=inpay&finish=true';
		}
		if($_GET['type']=='ar')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAR');
				$data['BPCode'] = $_SESSION['BPCode'][$i];
				$data['BPName'] = $this->m_bp->getBPName($data['BPCode']);
				$data['RefNum'] = $_SESSION['RefNum'][$i];
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DelDate'] = $_SESSION['DelDate'][$i];
				$data['SalesEmp'] = $_SESSION['SalesEmp'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Service'] = 1;
				
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
				
				$data['DocTotalBefore'] = $_SESSION['DocTotal'][$i];
				$data['DiscPer'] = 0;
				$data['Disc'] = 0;
				$data['Freight'] = 0;
				$data['TaxPer'] = 0;
				$data['Tax'] = 0;
				$data['DocTotal'] = $_SESSION['DocTotal'][$i];
				$data['AmmountTendered'] = 0;
				$data['Change'] = 0;
				$data['PaymentNote'] = '';
				$data['PaymentCode'] = '';
				$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
				
				if($data['BPName']=='')
				{
					$upload=0;
					$message=$message.'BP Code Not Found, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Doc. Date Format is Wrong, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DelDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Del. Date Format is Wrong, ';
				}
				$dwhs=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
				if($dwhs==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['DocTotal'][$i])==false)
				{
					$upload=0;
					$message=$message.'The Doc. Total is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$head=$this->m_ar->insertH($data);
					
					//proses jurnal
					$headDocnum=$this->m_ar->GetHeaderByHeaderID($head)->vcDocNum;
					$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($this->m_location->GetIDByCode($_SESSION['Location'][0]));//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='AR';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					$da['intHID']=$head;
					$da['itemID']=0;
					$da['itemcodeAR']='';
					$da['itemnameAR']=$data['Remarks'];
					$da['qtyAR']=1;
					$da['whsAR']=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
					$da['whsNameAR']=$this->m_location->GetNameByID($da['whsAR']);
					$da['uomAR']   = 'Invoice';
					$da['qtyinvAR']=1;
					$da['idBaseRefAR']=0;
					$da['BaseRefAR']='';
					$da['uominvAR']='Invoice';
					$da['uomtypeAR'] = 0;
					$da['costAR']=0;
					
					$da['priceAR']= $data['DocTotal'];
					$da['discperAR'] = 0;
					$da['discAR'] = 0;
					$da['priceafterAR']=$data['DocTotal'];
					$da['linetotalAR']= $data['DocTotal'];
					$da['linecostAR']=0;
					
					$detail=$this->m_ar->insertD($da);
					
						$debact = $this->m_bp_category->GetArActByBPCode($data['BPCode']);
						$creact = $_SESSION['Account'][$i];
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linetotalAR'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
						
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$this->db->trans_complete();
		
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ar&finish=true';
		}
		if($_GET['type']=='ap')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAP');
				$data['BPCode'] = $_SESSION['BPCode'][$i];
				$data['BPName'] = $this->m_bp->getBPName($data['BPCode']);
				$data['RefNum'] = $_SESSION['RefNum'][$i];
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DelDate'] = $_SESSION['DelDate'][$i];
				$data['SalesEmp'] = $_SESSION['SalesEmp'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Service'] = 1;
				
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
				
				$data['DocTotalBefore'] = $_SESSION['DocTotal'][$i];
				$data['DiscPer'] = 0;
				$data['Disc'] = 0;
				$data['Freight'] = 0;
				$data['TaxPer'] = 0;
				$data['Tax'] = 0;
				$data['DocTotal'] = $_SESSION['DocTotal'][$i];
				$data['AmmountTendered'] = 0;
				$data['Change'] = 0;
				$data['PaymentNote'] = '';
				$data['PaymentCode'] = '';
				$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
				
				if($data['BPName']=='')
				{
					$upload=0;
					$message=$message.'BP Code Not Found, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Doc. Date Format is Wrong, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DelDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Del. Date Format is Wrong, ';
				}
				$dwhs=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
				if($dwhs==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['DocTotal'][$i])==false)
				{
					$upload=0;
					$message=$message.'The Doc. Total is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$head=$this->m_ap->insertH($data);
					
					//proses jurnal
					$headDocnum=$this->m_ap->GetHeaderByHeaderID($head)->vcDocNum;
					
					$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($this->m_location->GetIDByCode($_SESSION['Location'][$i]));//Ambil id plan
					$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
					$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
					$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
					$dataJurnal['Remarks']='';
					$dataJurnal['RefType']='AP';
					$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
					
					$da['intHID']=$head;
					$da['itemID']=0;
					$da['itemcodeAP']='';
					$da['itemnameAP']=$data['Remarks'];
					$da['qtyAP']=1;
					$da['whsAP']=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
					$da['whsNameAP']=$this->m_location->GetNameByID($da['whsAP']);
					$da['uomAP']   = 'Invoice';
					$da['qtyinvAP']=1;
					$da['idBaseRefAP']=0;
					$da['BaseRefAP']='';
					$da['uominvAP']='Invoice';
					$da['uomtypeAP'] = 0;
					$da['costAP']=$data['DocTotal'];
					
					$da['priceAP']= $data['DocTotal'];
					$da['discperAP'] = 0;
					$da['discAP'] = 0;
					$da['priceafterAP']=$data['DocTotal'];
					$da['linetotalAP']= $data['DocTotal'];
					$da['linecostAP']=$data['DocTotal'];
					
					$detail=$this->m_ap->insertD($da);
					
						$debact = $_SESSION['Account'][$i];;
						$creact = $this->m_bp_category->GetApActByBPCode($data['BPCode']);
						
						$daJurnal['intHID']=$headJurnal;
						$daJurnal['DCJE']='D';
						
						$daJurnal['GLCodeJE']=$debact;
						$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
						$daJurnal['GLCodeJEX']=$creact;
						$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
						$daJurnal['ValueJE']=$da['linecostAP'];
						$val_min=$daJurnal['ValueJE']*-1;
						
						$detailJurnal=$this->m_jurnal->insertD($daJurnal);
						$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
						$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$this->db->trans_complete();
		
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ap&finish=true';
		}
		if($_GET['type']=='udf')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				
				$iddoc 	= $this->m_docnum->getIDByCode($_SESSION['DocType'][$i]);
				$idudf	= $this->m_udf->CekAvailableUdf($iddoc,$_SESSION['UdfCode'][$i]);
				
				if($iddoc=='')
				{
					$upload=0;
					$message=$message.'Doc. Type Not Found, ';
				}
				if($idudf==0)
				{
					$upload=0;
					$message=$message.'Udf Not Found, ';
				}
				if($_SESSION['Key'][$i]=="")
				{
					$upload=0;
					$message=$message.'Key Cannot Empty, ';
				}
				if($_SESSION['Key'][$i]=="")
				{
					$upload=0;
					$message=$message.'Key Cannot Empty, ';
				}
				if($upload==0)
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
				else
				{
					$this->db->trans_begin();
					$cekdup = $this->m_udf->CekDuplicateUdf($idudf,$_SESSION['Key'][$i]);
					if($cekdup==1)
					{
						$data['Doc'] = $_SESSION['Key'][$i];
						$data['ID'] = $idudf;
						$data['Val'] = $_SESSION['Value'][$i];
						$this->m_udf->EditUdf($data);
						$upload=1;
					}
					else
					{
						$data['Doc'] = $_SESSION['Key'][$i];
						$data['ID'] = $idudf;
						$data['Val'] = $_SESSION['Value'][$i];
						$this->m_udf->SaveUdf($data);
						$upload=1;
					}
					if($upload==1)
					{
						$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
					}
					else
					{
						$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
					}
					$this->db->trans_complete();
			
					if($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=udf&finish=true';
		}
		if($_GET['type']=='bom')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hBOM');
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['FGItem'] = $this->m_item->GetNameByCode($_SESSION['FGItem'][$i]);
				$data['Whs'] = $this->m_location->GetIDByCode($_SESSION['Whs'][$i]);
				$data['FGQty'] = $_SESSION['FGQty'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Rev'] = $_SESSION['Rev'][$i];
				$data['FGCost'] = 0;
				$_SESSION['messageIMPORT'][$i]='';
				
				$data['FGItemID'] = $this->m_item->GetIDByName($data['FGItem']);
				$result=$this->m_bom->cekrev($data);
				
				if($result==0)
				{
					$upload=0;
					$message=$message.'Bom Version Already exist, ';
				}
				if($data['FGItem']==null)
				{
					$upload=0;
					$message=$message.'FG Item Not Found, ';
				}
				if($data['Whs']==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['FGQty'][$i])==false)
				{
					$upload=0;
					$message=$message.'FG Qty is only allowed with the format number, ';
				}
				
				if($upload==0)
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
				else
				{
					$head=0;
					$this->db->trans_begin();
					if($_SESSION['FGItem'][$i]!='')
					{
						$head=$this->m_bom->insertH($data);
					}
					if($head!=0)
					{
						$rollback=0;
						for($j=$i;$j<=$_SESSION['totitemIMPORT'];$j++)
						{
							$detail=0;
							if(isset($_SESSION['FGItem'][$j]))
							{
								if($j==$i or $_SESSION['FGItem'][$j]=='')
								{
									
									
									$item=$this->m_item->GetIDByCode($_SESSION['itemcodeBOM'][$j]);
									if($item==null)
									{
										$rollback=1;
										$message=$message.'Component Item Not Found, ';
									}
									else if(is_numeric($_SESSION['qtyBOM'][$j])==false)
									{
										$upload=0;
										$message=$message.'Component Qty is only allowed with the format number, ';
									}
									else
									{
										$da['intHID']=$head;
										$da['itemcodeBOM']=$_SESSION['itemcodeBOM'][$j];
										$da['itemnameBOM']=$this->m_item->GetNameByCode($_SESSION['itemcodeBOM'][$j]);
										$da['qtyBOM']=$_SESSION['qtyBOM'][$j];
										$da['uomBOM']=$this->m_item->GetUoMByName($da['itemnameBOM']);
										$da['costBOM']=0;
										$da['autoBOM']=$_SESSION['autoBOM'][$j];
										$detail=$this->m_bom->insertD($da);
										
									}
								}
								if($j!=$i and $_SESSION['FGItem'][$j]!='')
								{
									break;
								}
							}
							if($detail==1)
							{
								$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-green">Success</small> ';
							}
							else
							{
								$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-red">Error</small> '.$message.'';
							}
						}
						$i=$j-1;
					}	
					$this->db->trans_complete();
			
					if($this->db->trans_status() === FALSE or $rollback==1)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
				}
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bom&finish=true';
		}
		 	echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
	}
	public function prosesadd()
	{
		$this->emptysession();
		
		if($_FILES['Image']['tmp_name']!='')
		{
			require_once APPPATH.'third_party/excel_reader/excel_reader.php';
			$data= new Spreadsheet_Excel_Reader($_FILES['Image']['tmp_name']);
			$baris=$data->rowcount();
			$_SESSION['totitemIMPORT']=0;
			$j=0;
			for($i=2;$i<=$baris;$i++)
			{
				if($_POST['Type']=='item')
				{
					$_SESSION['Category'][$j]=$data->val($i,1);
					$_SESSION['Code'][$j]=$data->val($i,2);
					$_SESSION['Name'][$j]=$data->val($i,3);
					$_SESSION['Price'][$j]=$data->val($i,4);
					$_SESSION['Tax'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['UoM'][$j]=$data->val($i,7);
					$_SESSION['PurUoM'][$j]=$data->val($i,8);
					$_SESSION['intPurUoM'][$j]=$data->val($i,9);
					$_SESSION['SlsUoM'][$j]=$data->val($i,10);
					$_SESSION['intSlsUoM'][$j]=$data->val($i,11);
					$_SESSION['intSalesItem'][$j]=$data->val($i,12);
					$_SESSION['intPurchaseItem'][$j]=$data->val($i,13);
					$_SESSION['intSalesWithoutQty'][$j]=$data->val($i,14);
					$_SESSION['Barcode'][$j]=$data->val($i,15);
					$_SESSION['intBatch'][$j]=$data->val($i,16);
					$_SESSION['intService'][$j]=$data->val($i,17);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=item';
				}
				if($_POST['Type']=='variant')
				{
					$_SESSION['Code'][$j]=$data->val($i,1);
					$_SESSION['Name'][$j]=$data->val($i,2);
					$_SESSION['Price'][$j]=$data->val($i,3);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=variant';
				}
				if($_POST['Type']=='inpay')
				{
					$_SESSION['ARNumber'][$j]=$data->val($i,1);
					$_SESSION['DocDateInpay'][$j]=$data->val($i,2);
					$_SESSION['WalletInpay'][$j]=$data->val($i,3);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=inpay';
				}
				if($_POST['Type']=='outpay')
				{
					$_SESSION['APNumber'][$j]=$data->val($i,1);
					$_SESSION['DocDateOutpay'][$j]=$data->val($i,2);
					$_SESSION['WalletOutpay'][$j]=$data->val($i,3);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=outpay';
				}
				if($_POST['Type']=='ib')
				{
					$_SESSION['Date'][$j]=$data->val($i,1);
					$_SESSION['ItemCode'][$j]=$data->val($i,2);
					$_SESSION['Type'][$j]=$data->val($i,3);
					$_SESSION['Location'][$j]=$data->val($i,4);
					$_SESSION['Qty'][$j]=$data->val($i,5);
					$_SESSION['Cost'][$j]=$data->val($i,6);
					$_SESSION['Account'][$j]=$data->val($i,7);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib';
				}
				if($_POST['Type']=='price')
				{
					$_SESSION['Category'][$j]=$data->val($i,1);
					$_SESSION['ItemCode'][$j]=$data->val($i,2);
					$_SESSION['Price'][$j]=$data->val($i,3);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=price';
				}
				if($_POST['Type']=='bp')
				{
					$_SESSION['Type'][$j]=$data->val($i,1);
					$_SESSION['Category'][$j]=$data->val($i,2);
					$_SESSION['Code'][$j]=$data->val($i,3);
					$_SESSION['Name'][$j]=$data->val($i,4);
					$_SESSION['TaxNumber'][$j]=$data->val($i,5);
					$_SESSION['PriceList'][$j]=$data->val($i,6);
					$_SESSION['CreditLimit'][$j]=$data->val($i,7);
					$_SESSION['PaymentTerm'][$j]=$data->val($i,8);
					$_SESSION['Tax'][$j]=$data->val($i,9);
					$_SESSION['Phone1'][$j]=$data->val($i,10);
					$_SESSION['Phone2'][$j]=$data->val($i,11);
					$_SESSION['Fax'][$j]=$data->val($i,12);
					$_SESSION['Email'][$j]=$data->val($i,13);
					$_SESSION['Website'][$j]=$data->val($i,14);
					
					$_SESSION['ContactPerson'][$j]=$data->val($i,15);
					$_SESSION['CityBillTo'][$j]=$data->val($i,16);
					$_SESSION['CountryBillTo'][$j]=$data->val($i,17);
					$_SESSION['AddressBillTo'][$j]=$data->val($i,18);
					$_SESSION['CityShipTo'][$j]=$data->val($i,19);
					$_SESSION['CountryShipTo'][$j]=$data->val($i,20);
					$_SESSION['AddressShipTo'][$j]=$data->val($i,21);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bp';
				}
				if($_POST['Type']=='ar')
				{
					$_SESSION['BPCode'][$j]=$data->val($i,1);
					$_SESSION['RefNum'][$j]=$data->val($i,2);
					$_SESSION['DocDate'][$j]=$data->val($i,3);
					$_SESSION['DelDate'][$j]=$data->val($i,4);
					$_SESSION['SalesEmp'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['DocTotal'][$j]=$data->val($i,7);
					$_SESSION['Location'][$j]=$data->val($i,8);
					$_SESSION['Account'][$j]=$data->val($i,9);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ar';
				}
				if($_POST['Type']=='ap')
				{
					$_SESSION['BPCode'][$j]=$data->val($i,1);
					$_SESSION['RefNum'][$j]=$data->val($i,2);
					$_SESSION['DocDate'][$j]=$data->val($i,3);
					$_SESSION['DelDate'][$j]=$data->val($i,4);
					$_SESSION['SalesEmp'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['DocTotal'][$j]=$data->val($i,7);
					$_SESSION['Location'][$j]=$data->val($i,8);
					$_SESSION['Account'][$j]=$data->val($i,9);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ap';
				}
				if($_POST['Type']=='bom')
				{
					$_SESSION['DocDate'][$j]=$data->val($i,1);
					$_SESSION['FGItem'][$j]=$data->val($i,2);
					$_SESSION['Whs'][$j]=$data->val($i,3);
					$_SESSION['FGQty'][$j]=$data->val($i,4);
					$_SESSION['Rev'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['itemcodeBOM'][$j]=$data->val($i,7);
					$_SESSION['qtyBOM'][$j]=$data->val($i,8);
					$_SESSION['autoBOM'][$j]=$data->val($i,9);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bom';
				}
				if($_POST['Type']=='udf')
				{
					$_SESSION['DocType'][$j]=$data->val($i,1);
					$_SESSION['UdfCode'][$j]=$data->val($i,2);
					$_SESSION['Key'][$j]=$data->val($i,3);
					$_SESSION['Value'][$j]=$data->val($i,4);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=udf';
				}
				$j++;
			}
			$_SESSION['totitemIMPORT']=$j;
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
		}
		else
		{
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=nofile';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
		}
	}
	
}
