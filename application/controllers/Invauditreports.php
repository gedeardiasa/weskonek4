<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invauditreports extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_mvt','',TRUE);
		$this->load->model('m_batch','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function recalculate()
	{
		$data['year'] = isset($_GET['year'])?$_GET['year']:''; // get the requested page
		$data['itemcode'] = isset($_GET['itemcode'])?$_GET['itemcode']:''; // get the requested page\
		$data['location'] = isset($_GET['location'])?$_GET['location']:''; // get the requested page\
		
		$this->m_stock->recalculate($data);
	}
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['ItemCode']=isset($_GET['ItemCode'])?$_GET['ItemCode']:''; // get the requested page
			$data['ItemName']=isset($_GET['ItemName'])?$_GET['ItemName']:''; // get the requested page
			$data['ItemGroup']=isset($_GET['ItemGroup'])?$_GET['ItemGroup']:0; // get the requested page
			$data['Mvt']=isset($_GET['Mvt'])?$_GET['Mvt']:0; // get the requested page
			
			$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
			$dt=explode("-",$data['daterange']);
			$data['from']=str_replace("/","-",$dt[0]);
			$data['until']=str_replace("/","-",$dt[1]);
			$data['yearC']=date('Y');
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('stock',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listgrp']=$this->m_item_category->GetAllData();
			$data['listmvt']=$this->m_mvt->GetAllData();
			$data['autoitem']=$this->m_item->GetAllData();
			$whs = 'and a.intLocation in (0,';
			
			foreach($data['listwhs']->result() as $a)
			{
				$index=$a->vcCode;
				if(isset($_GET["loc".$index]))
				{
					$data['sloc'][$index]=1;
					$whs = $whs."".$a->intID.",";
					
				}
			}
			$whs = rtrim($whs,", ");
			$whs =$whs.")";
			$data['whs']=$whs;
			
			$data['liststock']=$this->m_stock->GetDataAuditStockReport($data);
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterange']);
			$in=1;
			$cekkode='';
			foreach($data['liststock']->result() as $d) 
			{
				if($cekkode!=$d->ItemCode)
				{
					$data['allitem'][$in]=$d->ItemCode;
					$data['allitemname'][$in]=$d->ItemName;
					$data['allitemkey'][$in]=$in;
					$in++;
					$cekkode=$d->ItemCode;
				}
			}
			$data['in']=$in;
			$data['uservalue']=$this->m_user->getByID($_SESSION['IDPOS'])->intValue;
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function detailbatch($idmutation)
	{
		$listbatch=$this->m_batch->GetBatchMutationByIdMutation($idmutation);
		echo '<table id="exampledetailBatchMutation" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Qty</th>
				  <th>Remarks</th>
				  <th>Date</th>
                </tr>
                </thead>
                <tbody>';
				foreach($listbatch->result() as $d) 
				{
                echo '<tr>
                  <td>'.$d->vcDocNum.'</td>
				  <td align="right">'.number_format($d->intQty,'2').'</td>
				  <td>'.$d->vcRemarks.'</td>
				  <td>'.$d->dtInsertTime.'</td>
                </tr>
				';
				}
				echo '
                </tbody>

              </table>';
			  
			  echo '
		
		<script>
		  $(function () {
			$("#exampledetailBatchMutation").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	public function stockcard()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['ItemCode']=isset($_GET['ItemCode'])?$_GET['ItemCode']:''; // get the requested page
			$data['ItemName']=isset($_GET['ItemName'])?$_GET['ItemName']:''; // get the requested page
			$data['ItemGroup']=isset($_GET['ItemGroup'])?$_GET['ItemGroup']:0; // get the requested page
			
			$data['PeriodeMonth']=isset($_GET['PeriodeMonth'])?$_GET['PeriodeMonth']:date('n'); // get the requested page
			$data['PeriodeYear']=isset($_GET['PeriodeYear'])?$_GET['PeriodeYear']:date('Y'); // get the requested page
		
			$data['show']=isset($_GET['show'])?$_GET['show']:0; // get the requested page
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('stock',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['listgrp']=$this->m_item_category->GetAllData();
			$data['autoitem']=$this->m_item->GetAllData();
			$data['list']=$this->m_stock->GetStockCardHeader($data);
			
			$this->load->view($this->uri->segment(1).'/stockcard',$data);
		}
	}
	public function lisDetail()
	{
		$data['itemcode']=isset($_GET['itemcode'])?$_GET['itemcode']:''; // get the requested page
		
		$data['PeriodeMonth']=isset($_GET['PeriodeMonth'])?$_GET['PeriodeMonth']:date('n'); // get the requested page
		$data['PeriodeYear']=isset($_GET['PeriodeYear'])?$_GET['PeriodeYear']:date('Y'); // get the requested page
		$data['Periode'] = $data['PeriodeYear']."".$data['PeriodeMonth'];
		
		$data['list']=$this->m_stock->GetStockCardDetail($data);
		
		echo '
		<table id="example2" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
				  <th>Location</th>
                  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Category</th>
				  <th>UoM</th>
				  <th>Last Stock</th>
				  <th>Qty In</th>
				  <th>Qty Out</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcLocation.'</td>
				  <td>'.$d->vcItemCode.'</td>
				  <td>'.$d->vcItemName.'</td>
				  <td>'.$d->vcCategory.'</td>
				  <td>'.$d->vcUoM.'</td>
				  <td align="right">'.number_format($d->intLastStock,2).'</td>
				  <td align="right">'.number_format($d->QtyIn,2).'</td>
				  <td align="right">'.number_format($d->QtyOut,2).'</td>
				 
			';
			echo '
            </tr>
			';
		}
		
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example2").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				paging":   false,
				"ordering": false,
				"info":     false,
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
}
