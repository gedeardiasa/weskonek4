<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pp extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pp','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_udf','',TRUE);
		$this->load->model('m_block','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPP']=0;
			unset($_SESSION['itemcodePP']);
			unset($_SESSION['idPP']);
			unset($_SESSION['idBaseRefPP']);
			unset($_SESSION['itemnamePP']);
			unset($_SESSION['qtyPP']);
			unset($_SESSION['qtyOpenPP']);
			unset($_SESSION['uomPP']);
			unset($_SESSION['whsPP']);
			unset($_SESSION['statusPP']);
			
			$data['typetoolbar']='PP';
			$data['typeudf']='PP';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Purchase Request',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_pp->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['backdate']=$this->m_docnum->GetBackdate('PP');
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_pp->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hPP');
				
				
				$cekaproval = $this->m_docnum->CekAproval('PP');
				if($cekaproval==1)
				{
					$responce->Status = 'Pending';
					$responce->StatusCode = 'P';
				}
				else
				{
					$responce->Status = 'Open';
					$responce->StatusCode = 'O';
				}
			}
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hPP');
			$responce->RefNum = '';
			$responce->Service = '0';
			$cekaproval = $this->m_docnum->CekAproval('PP');
				if($cekaproval==1)
				{
					$responce->Status = 'Pending';
					$responce->StatusCode = 'P';
				}
				else
				{
					$responce->Status = 'Open';
					$responce->StatusCode = 'O';
				}
			
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemPP']=0;
			unset($_SESSION['itemcodePP']);
			unset($_SESSION['idPP']);
			unset($_SESSION['idBaseRefPP']);
			unset($_SESSION['itemnamePP']);
			unset($_SESSION['qtyPP']);
			unset($_SESSION['qtyOpenPP']);
			unset($_SESSION['uomPP']);
			unset($_SESSION['whsPP']);
			unset($_SESSION['statusPP']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemPP']=0;
			unset($_SESSION['itemcodePP']);
			unset($_SESSION['idPP']);
			unset($_SESSION['idBaseRefPP']);
			unset($_SESSION['itemnamePP']);
			unset($_SESSION['qtyPP']);
			unset($_SESSION['qtyOpenPP']);
			unset($_SESSION['uomPP']);
			unset($_SESSION['whsPP']);
			unset($_SESSION['statusPP']);
			
			$r=$this->m_pp->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idPP'][$j]=$d->intID;
				$_SESSION['BaseRefPP'][$j]='';
				$_SESSION['itemcodePP'][$j]=$d->vcItemCode;
				$_SESSION['itemnamePP'][$j]=$d->vcItemName;
				$_SESSION['qtyPP'][$j]=$d->intQty;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['qtyOpenPP'][$j]=$d->intQty;
					
				}
				else
				{
					$_SESSION['qtyOpenPP'][$j]=$d->intOpenQty;
				}
				
				if($d->intService==0)
				{
					$_SESSION['uomPP'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomPP'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['whsPP'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$cekaproval = $this->m_docnum->CekAproval('PP');
					if($cekaproval==1)
					{
						$_SESSION['statusPP'][$j]="P";
					}
					else
					{
						$_SESSION['statusPP'][$j]='O';
					}
					
				}
				else
				{
					$_SESSION['statusPP'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemPP']=$j;
		}
	}
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemPP'];$j++)
		{
			if(isset($_SESSION['itemcodePP'][$j]) or isset($_SESSION['itemnamePP'][$j]))
			{
				if($_SESSION['itemcodePP'][$j]!='' or $_SESSION['itemnamePP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
			
		$this->db->trans_begin();
		
		$head=$this->m_pp->insertH($data);
		$headDocnum=$this->m_pp->GetHeaderByHeaderID($head)->vcDocNum;
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPP'];$j++)// save detail
			{
				if($_SESSION['itemcodePP'][$j]!="" or $_SESSION['itemnamePP'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnamePP'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePP'][$j]);
					}
					else
					{
						$item=0;
					}
					$whs=$this->m_location->GetNameByID($_SESSION['whsPP'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodePP']=$_SESSION['itemcodePP'][$j];
					$da['itemnamePP']=$_SESSION['itemnamePP'][$j];
					$da['qtyPP']=$_SESSION['qtyPP'][$j];
					$da['whsPP']=$_SESSION['whsPP'][$j];
					$da['whsNamePP']=$whs;
					
					if($_SESSION['uomPP'][$j]==1 and $data['Service']==0)
					{
						$da['uomPP']=$vcUoM->vcUoM;
						$da['qtyinvPP']=$da['qtyPP'];
					}
					else if($_SESSION['uomPP'][$j]==2 and $data['Service']==0)
					{
						$da['uomPP']=$vcUoM->vcSlsUoM;
						$da['qtyinvPP']=$this->m_item->convert_qty($item,$da['qtyPP'],'intSlsUoM',1);
					}
					else if($_SESSION['uomPP'][$j]==3 and $data['Service']==0)
					{
						$da['uomPP']=$vcUoM->vcPurUoM;
						$da['qtyinvPP']=$this->m_item->convert_qty($item,$da['qtyPP'],'intPurUoM',1);
					}
					else
					{
						$da['uomPP']   = $_SESSION['uomPP'][$j];
						$da['qtyinvPP']= $da['qtyPP'];
					}
					
					if(isset($_SESSION['idBaseRefPP'][$j]))
					{
						$da['idBaseRefPP']=$_SESSION['idBaseRefPP'][$j];
						$da['BaseRefPP']=$_SESSION['BaseRefPP'][$j];
					}
					else
					{
						$da['idBaseRefPP']=0;
						$da['BaseRefPP']='';
					}
					
					
					$da['uomtypePP']=$_SESSION['uomPP'][$j];
					if($data['Service']==0)
					{
						$da['uominvPP']=$vcUoM->vcUoM;
					}
					else
					{
						$da['uominvPP'] = $da['uomPP'];
					}
					
					$detail=$this->m_pp->insertD($da);
				}
			}
			
		}
		$cekblock = $this->m_block->cekblock($head,'PP');
			
		if($cekblock=='')
		{
			$_SESSION['totitemPP']=0;
			unset($_SESSION['itemcodePP']);
			unset($_SESSION['idPP']);
			unset($_SESSION['idBaseRefPP']);
			unset($_SESSION['itemnamePP']);
			unset($_SESSION['qtyPP']);
			unset($_SESSION['qtyOpenPP']);
			unset($_SESSION['uomPP']);
			unset($_SESSION['whsPP']);
			unset($_SESSION['statusPP']);
			
			$this->db->trans_complete();
			echo $headDocnum;
		}
		else
		{
			echo $cekblock;
		}
		// $this->db->trans_complete();
		
		// if($this->db->trans_status() === FALSE)
		// {
			// $this->db->trans_rollback();
		// }else{
			// $this->db->trans_commit();
		// }
	}
	function prosesapv()
	{
		$data['id']=$_POST['idHeader'];
		$this->m_pp->apvH($data);
		echo 1;
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_pp->editH($data);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemPP'];$j++)// save detail
			{
				if($_SESSION['statusPP'][$j]=='O' or $_SESSION['statusPP'][$j]=='P')
				{
					$cek=1;
					
					if($_SESSION['itemcodePP'][$j]=='' and $_SESSION['itemnamePP'][$j]=='')//jika tidak ada item code / item name artinya detail ini dihapus
					{
						if(isset($_SESSION['idPP'][$j]))
						{
							$this->m_pp->deleteD($_SESSION['idPP'][$j]);
						}
					}
					else
					{
						if($data['Service']==0)
						{
							$item=$this->m_item->GetIDByName($_SESSION['itemnamePP'][$j]);
							$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePP'][$j]);
						}
						else
						{
							$item=0;
						}
						
						$whs=$this->m_location->GetNameByID($_SESSION['whsPP'][$j]);
						
						
						$da['intHID']=$data['id'];
						$da['itemID']=$item;
						$da['itemcodePP']=$_SESSION['itemcodePP'][$j];
						$da['itemnamePP']=$_SESSION['itemnamePP'][$j];
						$da['qtyPP']=$_SESSION['qtyPP'][$j];
						$da['whsPP']=$_SESSION['whsPP'][$j];
						$da['whsNamePP']=$whs;
						
						if($_SESSION['uomPP'][$j]==1 and $data['Service']==0)
						{
							$da['uomPP']=$vcUoM->vcUoM;
							$da['qtyinvPP']=$da['qtyPP'];
						}
						else if($_SESSION['uomPP'][$j]==2 and $data['Service']==0)
						{
							$da['uomPP']=$vcUoM->vcSlsUoM;
							$da['qtyinvPP']=$this->m_item->convert_qty($item,$da['qtyPP'],'intSlsUoM',1);
						}
						else if($_SESSION['uomPP'][$j]==3 and $data['Service']==0)
						{
							$da['uomPP']=$vcUoM->vcPurUoM;
							$da['qtyinvPP']=$this->m_item->convert_qty($item,$da['qtyPP'],'intPurUoM',1);
						}
						else
						{
							$da['uomPP']   = $_SESSION['uomPP'][$j];
							$da['qtyinvPP']= $da['qtyPP'];
						}
						$da['uomtypePP']=$_SESSION['uomPP'][$j];
						if($data['Service']==0)
						{
							$da['uominvPP']=$vcUoM->vcUoM;
						}
						else
						{
							$da['uominvPP'] = $da['uomPP'];
						}
						
						
						if(isset($_SESSION['idPP'][$j])) //jika ada session id artinya data detail dirubah
						{
							$da['intID']=$_SESSION['idPP'][$j];
							$detail=$this->m_pp->editD($da);
						}
						else //jika tidak artinya ini merupakan detail tambahan
						{
							$detail=$this->m_pp->insertD($da);
						}
						
					}
				}
			}
			$_SESSION['totitemPP']=0;
			unset($_SESSION['itemcodePP']);
			unset($_SESSION['idPP']);
			unset($_SESSION['idBaseRefPP']);
			unset($_SESSION['itemnamePP']);
			unset($_SESSION['qtyPP']);
			unset($_SESSION['qtyOpenPP']);
			unset($_SESSION['uomPP']);
			unset($_SESSION['whsPP']);
			unset($_SESSION['statusPP']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
		
		
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_pp->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table hover">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			$plan 				= $this->m_docnum->GetPlanIDByDoc('hPP','dPP',$d->vcDocNum);
			$data['userapv']	= $this->m_docnum->GetUserApv($_SESSION['UsernamePOS'],'PP',$plan);
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->vcStatusName.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
				
				if($data['userapv']==1 and $d->vcStatus=="P")
				{
					echo '&nbsp;&nbsp;<i class="fa fa-check '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialapv(\''.$d->intID.'\')"></i>';
				
				}
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_pp->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemPP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		$cekaproval = $this->m_docnum->CekAproval('PP');
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemPP'];$j++)
		{
			if($_SESSION['itemcodePP'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyPP'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenPP'][$j]=$_POST['detailQty'];
				$_SESSION['uomPP'][$j]=$_POST['detailUoM'];
				$_SESSION['whsPP'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefPP'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnamePP'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyPP'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenPP'][$j]=$_POST['detailQty'];
				$_SESSION['uomPP'][$j]=$_POST['detailUoMS'];
				$_SESSION['whsPP'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefPP'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodePP'][$i]=$code;
				$_SESSION['itemnamePP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyPP'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenPP'][$i]=$_POST['detailQty'];
				if($_POST['Service']==1)
				{
					$_SESSION['uomPP'][$i]=$_POST['detailUoMS'];
				}else
				{
					$_SESSION['uomPP'][$i]=$_POST['detailUoM'];
				}
				$_SESSION['whsPP'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefPP'][$i]='';
				if($cekaproval==1)
				{
					$_SESSION['statusPP'][$i]='P';
				}
				else
				{
					$_SESSION['statusPP'][$i]='O';
				}
				$_SESSION['totitemPP']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodePP'][$i]='';
					$_SESSION['itemnamePP'][$i]=$_POST['detailItem'];
					$_SESSION['qtyPP'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenPP'][$i]=$_POST['detailQty'];
					$_SESSION['uomPP'][$i]=$_POST['detailUoMS'];
					$_SESSION['whsPP'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefPP'][$i]='';
					if($cekaproval==1)
					{
						$_SESSION['statusPP'][$i]='P';
					}
					else
					{
						$_SESSION['statusPP'][$i]='O';
					}
					$_SESSION['totitemPP']++;
				}
				else
				{
					echo "false";
				}
				
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemPP'];$j++)
		{
			if($_SESSION['itemcodePP'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodePP'][$j]="";
				$_SESSION['itemnamePP'][$j]="";
				$_SESSION['qtyPP'][$j]="";
				$_SESSION['qtyOpenPP'][$j]="";
				$_SESSION['uomPP'][$j]="";
				$_SESSION['whsPP'][$j]="";
				$_SESSION['BaseRefPP'][$j]='';
			}
			if($_SESSION['itemnamePP'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodePP'][$j]="";
				$_SESSION['itemnamePP'][$j]="";
				$_SESSION['qtyPP'][$j]="";
				$_SESSION['qtyOpenPP'][$j]="";
				$_SESSION['uomPP'][$j]="";
				$_SESSION['whsPP'][$j]="";
				$_SESSION['BaseRefPP'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			$cekaproval = $this->m_docnum->CekAproval('PP');
			for($j=0;$j<$_SESSION['totitemPP'];$j++)
			{
				if($_SESSION['itemnamePP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePP'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnamePP'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsPP'][$j]);
					
					
					if($_SESSION['uomPP'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomPP'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomPP'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomPP'][$j];
					}
					
					if(($_SESSION['statusPP'][$j]=='O' and $cekaproval==0) or ($_SESSION['statusPP'][$j]=='P' and $cekaproval==1))
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodePP'][$j].'</td>
						<td>'.$_SESSION['itemnamePP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenPP'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						
						
						if(($_SESSION['statusPP'][$j]=='O' and $cekaproval==0 and $_SESSION['qtyPP'][$j]==$_SESSION['qtyOpenPP'][$j]) 
						or ($_SESSION['statusPP'][$j]=='P' and $cekaproval==1 and $_SESSION['qtyPP'][$j]==$_SESSION['qtyOpenPP'][$j]))
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnamePP"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodePP"][$j]).'\',\''.$_SESSION["qtyPP"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomPP"][$j]).'\',\''.str_replace("'","\'",$viewUoM).'\',\''.str_replace("'","\'",$_SESSION["whsPP"][$j]).'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodePP"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemnamePP"][$j]).'\',\''.$_SESSION["qtyPP"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
