<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rout extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_rout','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_activity','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemROUT']=0;
			unset($_SESSION['activitycodeROUT']);
			unset($_SESSION['activitynameROUT']);
			unset($_SESSION['qtyROUT']);
			unset($_SESSION['uomROUT']);
			unset($_SESSION['sourceROUT']);
			unset($_SESSION['costROUT']);
			unset($_SESSION['idROUT']);
			unset($_SESSION['autoROUT']);
			
			
			$data['typetoolbar']='ROUT';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Rout',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_rout->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoactivity']=$this->m_activity->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getActivityCode()
	{
		$data=$this->m_activity->GetCodeByName($_POST['detailActivity']);
		echo $data;
	}
	function getActivityName()
	{
		$data=$this->m_activity->GetNameByCode($_POST['detailActivityCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_rout->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hROUT');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->FGItem = $r->vcItemName;
			$responce->Whs = $r->intLocation;
			$responce->FGQty = $r->intQty;
			$responce->Remarks = $r->vcRemarks;
			$responce->vcRef = $r->vcRef;
			$responce->FGCost = $r->intCost;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hROUT');
			$responce->DocDate = date('m/d/Y');
			$responce->FGItem = '';
			$responce->Whs = '';
			$responce->FGQty = '';
			$responce->Remarks = '';
			$responce->vcRef = '';
			$responce->FGCost = '';
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadcost()
	{
		$data['detailActivity'] = isset($_POST['detailActivity'])?$_POST['detailActivity']:''; // get the requested page
		
		$activity=$this->m_activity->GetIDByName($data['detailActivity']);
		$cost=$this->m_activity->GetCostActivity($activity);
		
		echo $cost;
	}
	function loaduom()
	{
		$data['detailActivity'] = isset($_POST['detailActivity'])?$_POST['detailActivity']:''; // get the requested page
		$uom=$this->m_activity->GetUoMByName($data['detailActivity']);
		echo $uom;
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemROUT']=0;
			unset($_SESSION['idROUT']);
			unset($_SESSION['activitycodeROUT']);
			unset($_SESSION['activitynameROUT']);
			unset($_SESSION['qtyROUT']);
			unset($_SESSION['uomROUT']);
			unset($_SESSION['sourceROUT']);
			unset($_SESSION['costROUT']);
			unset($_SESSION['autoROUT']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemROUT']=0;
			unset($_SESSION['activitycodeROUT']);
			unset($_SESSION['activitynameROUT']);
			unset($_SESSION['qtyROUT']);
			unset($_SESSION['uomROUT']);
			unset($_SESSION['sourceROUT']);
			unset($_SESSION['costROUT']);
			unset($_SESSION['idROUT']);
			unset($_SESSION['autoROUT']);
			
			$r=$this->m_rout->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idROUT'][$j]=$d->intID;
				$_SESSION['activitycodeROUT'][$j]=$d->vcActivityCode;
				$_SESSION['activitynameROUT'][$j]=$d->vcActivityName;
				$_SESSION['qtyROUT'][$j]=$d->intQty;
				$_SESSION['sourceROUT'][$j]=$d->vcSourceCost;
				$_SESSION['uomROUT'][$j]=$d->vcUoM;
				$_SESSION['costROUT'][$j]=$d->intCost;
				$_SESSION['autoROUT'][$j]=$d->intAutoIssue;
				$j++;
			}
			$_SESSION['totitemROUT']=$j;
		}
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemROUT'];$j++)
		{
			if(isset($_SESSION['activitycodeROUT'][$j]))
			{
				if($_SESSION['activitycodeROUT'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekrev()
	{
		$data['FGItem'] = isset($_POST['FGItem'])?$_POST['FGItem']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		$data['FGItemID'] = $this->m_item->GetIDByName($data['FGItem']);
		$result=$this->m_rout->cekrev($data);
		echo $result;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['FGItem'] = isset($_POST['FGItem'])?$_POST['FGItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		$data['FGCost'] = isset($_POST['FGCost'])?$_POST['FGCost']:''; // get the requested page
		
		$this->db->trans_begin();
		
		$head=$this->m_rout->insertH($data);
		$headDocnum=$this->m_rout->GetHeaderByHeaderID($head)->vcDocNum;
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		$totalcost=0;
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemROUT'];$j++)// save detail
			{
				if($_SESSION['activitycodeROUT'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['activitynameROUT'][$j]);
					$da['intHID']=$head;
					$da['activitycodeROUT']=$_SESSION['activitycodeROUT'][$j];
					$da['activitynameROUT']=$_SESSION['activitynameROUT'][$j];
					$da['qtyROUT']=$_SESSION['qtyROUT'][$j];
					$da['uomROUT']=$_SESSION['uomROUT'][$j];
					$da['sourceROUT']=$_SESSION['sourceROUT'][$j];
					$da['costROUT']=$_SESSION['costROUT'][$j];
					$da['autoROUT']=$_SESSION['autoROUT'][$j];
					$detail=$this->m_rout->insertD($da);
					$totalcost=$totalcost+($da['qtyROUT']*$da['costROUT']);
					
				}
			}
			$this->m_rout->updateCost($head,$totalcost);
			$_SESSION['totitemROUT']=0;
			unset($_SESSION['idROUT']);
			unset($_SESSION['activitycodeROUT']);
			unset($_SESSION['activitynameROUT']);
			unset($_SESSION['qtyROUT']);
			unset($_SESSION['uomROUT']);
			unset($_SESSION['sourceROUT']);
			unset($_SESSION['costROUT']);
			unset($_SESSION['autoROUT']);
			echo $headDocnum;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_rout->editH($data);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemROUT'];$j++)// save detail
			{
				if($_SESSION['activitycodeROUT'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
				{
					if(isset($_SESSION['idROUT'][$j]))
					{
						$this->m_rout->deleteD($_SESSION['idROUT'][$j]);
					}
				}
				else
				{
					$item=$this->m_activity->GetIDByName($_SESSION['activitynameROUT'][$j]);
					$da['intHID']=$data['id'];
					$da['activitycodeROUT']=$_SESSION['activitycodeROUT'][$j];
					$da['activitynameROUT']=$_SESSION['activitynameROUT'][$j];
					$da['qtyROUT']=$_SESSION['qtyROUT'][$j];
					$da['uomROUT']=$_SESSION['uomROUT'][$j];
					$da['sourceROUT']=$_SESSION['sourceROUT'][$j];
					$da['costROUT']=$_SESSION['costROUT'][$j];
					$da['autoROUT']=$_SESSION['autoROUT'][$j];
					if(isset($_SESSION['idROUT'][$j])) //jika ada session id artinya data detail dirubah
					{
						$da['intID']=$_SESSION['idROUT'][$j];
						$detail=$this->m_rout->editD($da);
					}
					else //jika tidak artinya ini merupakan detail tambahan
					{
						$detail=$this->m_rout->insertD($da);
					}
				}
				
			}
			$listdetail=$this->m_rout->GetDetailByHeaderID($data['id']);
			$totalcost=0;
			foreach($listdetail->result() as $ld)
			{
				$totalcost=$totalcost+($ld->intQty*$ld->intCost);
			}
			$this->m_rout->updateCost($data['id'],$totalcost);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemROUT'];
		$code=$this->m_activity->GetCodeByName($_POST['detailActivity']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemROUT'];$j++)
		{
			if($_SESSION['activitycodeROUT'][$j]==$code)
			{
				$_SESSION['qtyROUT'][$j]=$_POST['detailQty'];
				$_SESSION['sourceROUT'][$j]=$_POST['SourceCost'];
				$_SESSION['costROUT'][$j]=$_POST['detailCost'];
				$_SESSION['autoROUT'][$j]=$_POST['detailAuto'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_activity->GetUoMByName($_POST['detailActivity']);
				$_SESSION['activitycodeROUT'][$i]=$code;
				$_SESSION['activitynameROUT'][$i]=$_POST['detailActivity'];
				$_SESSION['qtyROUT'][$i]=$_POST['detailQty'];
				$_SESSION['sourceROUT'][$i]=$_POST['SourceCost'];
				$_SESSION['costROUT'][$i]=$_POST['detailCost'];
				$_SESSION['uomROUT'][$i]=$UoM;
				$_SESSION['autoROUT'][$i]=$_POST['detailAuto'];
				$_SESSION['totitemROUT']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemROUT'];$j++)
		{
			if($_SESSION['activitycodeROUT'][$j]==$_POST['code'])
			{
				$_SESSION['activitycodeROUT'][$j]="";
				$_SESSION['activitynameROUT'][$j]="";
				$_SESSION['qtyROUT'][$j]="";
				$_SESSION['uomROUT'][$j]="";
				$_SESSION['sourceROUT'][$j]="";
				$_SESSION['costROUT'][$j]="";
				$_SESSION['autoROUT'][$i]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
				  <th>Source</th>
				  <th>Cost</th>
				  <th>Issue Method</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemROUT'];$j++)
			{
				if($_SESSION['activitynameROUT'][$j]!="")
				{
					$item=$this->m_activity->GetIDByName($_SESSION['activitynameROUT'][$j]);
					
					$source = $_SESSION['sourceROUT'][$j];
					
					if($_SESSION["autoROUT"][$j]==1)
					{
						$autorout = 'Auto';
					}
					else
					{
						$autorout = 'Manual';
					}
					echo '
					<tr>
						<td>'.$_SESSION['activitycodeROUT'][$j].'</td>
						<td>'.$_SESSION['activitynameROUT'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyROUT'][$j],'2').'</td>
						<td>'.$_SESSION['uomROUT'][$j].'</td>
						<td>'.$source.'</td>
						<td align="right">'.number_format($_SESSION['costROUT'][$j],'2').'</td>
						<td>'.$autorout.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["activitynameROUT"][$j]).'\',\''.str_replace("'","\'",$_SESSION["activitycodeROUT"][$j]).'\',\''.$_SESSION["qtyROUT"][$j].'\',\''.$_SESSION["costROUT"][$j].'\',\''.$_SESSION["autoROUT"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomROUT"][$j]).'\',\''.str_replace("'","\'",$_SESSION["sourceROUT"][$j]).'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["activitycodeROUT"][$j]).'\',\''.$_SESSION["qtyROUT"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
