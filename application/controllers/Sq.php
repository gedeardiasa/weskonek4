<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sq extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_udf','',TRUE);
		$this->load->model('m_block','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemSQ']=0;
			unset($_SESSION['itemcodeSQ']);
			unset($_SESSION['idSQ']);
			unset($_SESSION['itemnameSQ']);
			unset($_SESSION['qtySQ']);
			unset($_SESSION['qtyOpenSQ']);
			unset($_SESSION['uomSQ']);
			unset($_SESSION['priceSQ']);
			unset($_SESSION['discSQ']);
			unset($_SESSION['whsSQ']);
			unset($_SESSION['statusSQ']);
			
			$data['typetoolbar']='SQ';
			$data['typeudf']='SQ';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('sales quotation',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_sq->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['backdate']=$this->m_docnum->GetBackdate('SQ');
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hSQ');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hSQ');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemSQ']=0;
			unset($_SESSION['itemcodeSQ']);
			unset($_SESSION['idSQ']);
			unset($_SESSION['itemnameSQ']);
			unset($_SESSION['qtySQ']);
			unset($_SESSION['qtyOpenSQ']);
			unset($_SESSION['uomSQ']);
			unset($_SESSION['priceSQ']);
			unset($_SESSION['discSQ']);
			unset($_SESSION['whsSQ']);
			unset($_SESSION['statusSQ']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemSQ']=0;
			unset($_SESSION['itemcodeSQ']);
			unset($_SESSION['idSQ']);
			unset($_SESSION['itemnameSQ']);
			unset($_SESSION['qtySQ']);
			unset($_SESSION['qtyOpenSQ']);
			unset($_SESSION['uomSQ']);
			unset($_SESSION['priceSQ']);
			unset($_SESSION['discSQ']);
			unset($_SESSION['whsSQ']);
			unset($_SESSION['statusSQ']);
			
			$r=$this->m_sq->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idSQ'][$j]=$d->intID;
				$_SESSION['BaseRefSQ'][$j]='';
				$_SESSION['itemcodeSQ'][$j]=$d->vcItemCode;
				$_SESSION['itemnameSQ'][$j]=$d->vcItemName;
				$_SESSION['qtySQ'][$j]=$d->intQty;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['qtyOpenSQ'][$j]=$d->intQty;
				}
				else
				{
					$_SESSION['qtyOpenSQ'][$j]=$d->intOpenQty;
				}
				
				if($d->intService==0)
				{
					$_SESSION['uomSQ'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomSQ'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceSQ'][$j]=$d->intPrice;
				$_SESSION['discSQ'][$j]=$d->intDiscPer;
				$_SESSION['whsSQ'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusSQ'][$j]='O';
				}
				else
				{
					$_SESSION['statusSQ'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemSQ']=$j;
		}
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemSQ'];$j++)
		{
			if(($_SESSION['itemcodeSQ'][$j]!='' and $_SESSION['itemcodeSQ'][$j]!=null) or ($_SESSION['itemnameSQ'][$j]!='' and $_SESSION['itemnameSQ'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtySQ'][$j]*$_SESSION['priceSQ'][$j])-($_SESSION['discSQ'][$j]/100*($_SESSION['qtySQ'][$j]*$_SESSION['priceSQ'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemSQ'];$j++)
		{
			if(isset($_SESSION['itemcodeSQ'][$j]) or isset($_SESSION['itemnameSQ'][$j]))
			{
				if($_SESSION['itemcodeSQ'][$j]!='' or $_SESSION['itemnameSQ'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$head=$this->m_sq->insertH($data);
		$headDocnum=$this->m_sq->GetHeaderByHeaderID($head)->vcDocNum;
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$headDocnum);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemSQ'];$j++)// save detail
			{
				if($_SESSION['itemcodeSQ'][$j]!="" or $_SESSION['itemnameSQ'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameSQ'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameSQ'][$j]);
					}
					else
					{
						$item=$_SESSION['itemnameSQ'][$j];
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsSQ'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeSQ']=$_SESSION['itemcodeSQ'][$j];
					$da['itemnameSQ']=$_SESSION['itemnameSQ'][$j];
					$da['qtySQ']=$_SESSION['qtySQ'][$j];
					$da['whsSQ']=$_SESSION['whsSQ'][$j];
					$da['whsNameSQ']=$whs;
					
					if($_SESSION['uomSQ'][$j]==1 and $data['Service']==0)
					{
						$da['uomSQ']=$vcUoM->vcUoM;
						$da['qtyinvSQ']=$da['qtySQ'];
					}
					else if($_SESSION['uomSQ'][$j]==2 and $data['Service']==0)
					{
						$da['uomSQ']=$vcUoM->vcSlsUoM;
						$da['qtyinvSQ']=$this->m_item->convert_qty($item,$da['qtySQ'],'intSlsUoM',1);
					}
					else if($_SESSION['uomSQ'][$j]==3 and $data['Service']==0)
					{
						$da['uomSQ']=$vcUoM->vcPurUoM;
						$da['qtyinvSQ']=$this->m_item->convert_qty($item,$da['qtySQ'],'intPurUoM',1);
					}
					else
					{
						$da['uomSQ']   = $_SESSION['uomSQ'][$j];
						$da['qtyinvSQ']= $da['qtySQ'];
					}
					$da['uomtypeSQ']=$_SESSION['uomSQ'][$j];
					if($data['Service']==0)
					{
						$da['uominvSQ']=$vcUoM->vcUoM;
					}
					else
					{
						$da['uominvSQ'] = $da['uomSQ'];
					}
					
					
					$da['priceSQ']= $_SESSION['priceSQ'][$j];
					$da['discperSQ'] = $_SESSION['discSQ'][$j];
					$da['discSQ'] = $_SESSION['discSQ'][$j]/100*$da['priceSQ'];
					$da['priceafterSQ']=(100-$_SESSION['discSQ'][$j])/100*$da['priceSQ'];
					$da['linetotalSQ']= $da['priceafterSQ']*$da['qtySQ'];
					$detail=$this->m_sq->insertD($da);
					
				}
			}
			
		}
		
		$cekblock = $this->m_block->cekblock($head,'SQ');
			
		if($cekblock=='')
		{
			$_SESSION['totitemSQ']=0;
			unset($_SESSION['itemcodeSQ']);
			unset($_SESSION['idSQ']);
			unset($_SESSION['itemnameSQ']);
			unset($_SESSION['qtySQ']);
			unset($_SESSION['qtyOpenSQ']);
			unset($_SESSION['uomSQ']);
			unset($_SESSION['priceSQ']);
			unset($_SESSION['discSQ']);
			unset($_SESSION['whsSQ']);
			unset($_SESSION['statusSQ']);
			
			$this->db->trans_complete();
			echo $headDocnum;
		}
		else
		{
			echo $cekblock;
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_sq->editH($data);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemSQ'];$j++)// save detail
			{
				if($_SESSION['statusSQ'][$j]=='O')
				{
					$cek=1;
					
					if($_SESSION['itemcodeSQ'][$j]=='' and $_SESSION['itemnameSQ'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
					{
						if(isset($_SESSION['idSQ'][$j]))
						{
							$this->m_sq->deleteD($_SESSION['idSQ'][$j]);
						}
						
					}
					else
					{
						if($data['Service']==0)
						{
							$item=$this->m_item->GetIDByName($_SESSION['itemnameSQ'][$j]);
							$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameSQ'][$j]);
						}
						else
						{
							$item=0;
						}
						
						$whs=$this->m_location->GetNameByID($_SESSION['whsSQ'][$j]);
						
						
						$da['intHID']=$data['id'];
						$da['itemID']=$item;
						$da['itemcodeSQ']=$_SESSION['itemcodeSQ'][$j];
						$da['itemnameSQ']=$_SESSION['itemnameSQ'][$j];
						$da['qtySQ']=$_SESSION['qtySQ'][$j];
						$da['whsSQ']=$_SESSION['whsSQ'][$j];
						$da['whsNameSQ']=$whs;
						
						if($_SESSION['uomSQ'][$j]==1 and $data['Service']==0)
						{
							$da['uomSQ']=$vcUoM->vcUoM;
							$da['qtyinvSQ']=$da['qtySQ'];
						}
						else if($_SESSION['uomSQ'][$j]==2 and $data['Service']==0)
						{
							$da['uomSQ']=$vcUoM->vcSlsUoM;
							$da['qtyinvSQ']=$this->m_item->convert_qty($item,$da['qtySQ'],'intSlsUoM',1);
						}
						else if($_SESSION['uomSQ'][$j]==3 and $data['Service']==0)
						{
							$da['uomSQ']=$vcUoM->vcPurUoM;
							$da['qtyinvSQ']=$this->m_item->convert_qty($item,$da['qtySQ'],'intPurUoM',1);
						}
						else
						{
							$da['uomSQ']   = $_SESSION['uomSQ'][$j];
							$da['qtyinvSQ']= $da['qtySQ'];
						}
						$da['uomtypeSQ']=$_SESSION['uomSQ'][$j];
						if($data['Service']==0)
						{
							$da['uominvSQ']=$vcUoM->vcUoM;
						}
						else
						{
							$da['uominvSQ'] = $da['uomSQ'];
						}
						
						
						$da['priceSQ']= $_SESSION['priceSQ'][$j];
						$da['discperSQ'] = $_SESSION['discSQ'][$j];
						$da['discSQ'] = $_SESSION['discSQ'][$j]/100*$da['priceSQ'];
						$da['priceafterSQ']=(100-$_SESSION['discSQ'][$j])/100*$da['priceSQ'];
						$da['linetotalSQ']= $da['priceafterSQ']*$da['qtySQ'];
						
						if(isset($_SESSION['idSQ'][$j])) //jika ada session id artinya data detail dirubah
						{
							$da['intID']=$_SESSION['idSQ'][$j];
							$detail=$this->m_sq->editD($da);
						}
						else //jika tidak artinya ini merupakan detail tambahan
						{
							$detail=$this->m_sq->insertD($da);
						}
						
					}
				}
			}
			$_SESSION['totitemSQ']=0;
			unset($_SESSION['itemcodeSQ']);
			unset($_SESSION['idSQ']);
			unset($_SESSION['itemnameSQ']);
			unset($_SESSION['qtySQ']);
			unset($_SESSION['qtyOpenSQ']);
			unset($_SESSION['uomSQ']);
			unset($_SESSION['priceSQ']);
			unset($_SESSION['discSQ']);
			unset($_SESSION['whsSQ']);
			unset($_SESSION['statusSQ']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_sq->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table hover">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
				  <th>Doc. Total</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatusName.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="right">'.number_format($d->intDocTotal,0).'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
			<tfoot>
            <tr>
                <th colspan="5" style="text-align:right">Total:</th>
                <th style="text-align:right; padding-right:9px"></th>
				<th></th>
            </tr>
        </tfoot>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"footerCallback": function ( row, data, start, end, display ) {
					var api = this.api(), data;
		 
					// Remove the formatting to get integer data for summation
					var intVal = function ( i ) {
						return typeof i === \'string\' ?
							i.replace(/[\$,]/g, \'\')*1 :
							typeof i === \'number\' ?
								i : 0;
					};
		 
					// Total over all pages
					total = api
						.column( 5 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
		 
					// Total over this page
					pageTotal = api
						.column( 5, { page: \'current\'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
					pageTotalI = pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 
					// Update footer
					$( api.column( 5 ).footer() ).html(
						\'\'+pageTotalI +\'\'
					);
				},
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_sq->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemSQ'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemSQ'];$j++)
		{
			if($_SESSION['itemcodeSQ'][$j]==$code and $code!=null)
			{
				$_SESSION['qtySQ'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenSQ'][$j]=$_POST['detailQty'];
				$_SESSION['uomSQ'][$j]=$_POST['detailUoM'];
				$_SESSION['priceSQ'][$j]=$_POST['detailPrice'];
				$_SESSION['discSQ'][$j]=$_POST['detailDisc'];
				$_SESSION['whsSQ'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefSQ'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameSQ'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtySQ'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenSQ'][$j]=$_POST['detailQty'];
				$_SESSION['uomSQ'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceSQ'][$j]=$_POST['detailPrice'];
				$_SESSION['discSQ'][$j]=$_POST['detailDisc'];
				$_SESSION['whsSQ'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefSQ'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeSQ'][$i]=$code;
				$_SESSION['itemnameSQ'][$i]=$_POST['detailItem'];
				$_SESSION['qtySQ'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenSQ'][$i]=$_POST['detailQty'];
				if($_POST['Service']==1)
				{
					$_SESSION['uomSQ'][$i]=$_POST['detailUoMS'];
				}else
				{
					$_SESSION['uomSQ'][$i]=$_POST['detailUoM'];
				}
				$_SESSION['priceSQ'][$i]=$_POST['detailPrice'];
				$_SESSION['discSQ'][$i]=$_POST['detailDisc'];
				$_SESSION['whsSQ'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefSQ'][$i]='';
				$_SESSION['statusSQ'][$i]='O';
				$_SESSION['totitemSQ']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeSQ'][$i]='';
					$_SESSION['itemnameSQ'][$i]=$_POST['detailItem'];
					$_SESSION['qtySQ'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenSQ'][$i]=$_POST['detailQty'];
					$_SESSION['uomSQ'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceSQ'][$i]=$_POST['detailPrice'];
					$_SESSION['discSQ'][$i]=$_POST['detailDisc'];
					$_SESSION['whsSQ'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefSQ'][$i]='';
					$_SESSION['statusSQ'][$i]='O';
					$_SESSION['totitemSQ']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemSQ'];$j++)
		{
			if($_SESSION['itemcodeSQ'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeSQ'][$j]="";
				$_SESSION['itemnameSQ'][$j]="";
				$_SESSION['qtySQ'][$j]="";
				$_SESSION['qtyOpenSQ'][$j]="";
				$_SESSION['uomSQ'][$j]="";
				$_SESSION['priceSQ'][$j]="";
				$_SESSION['discSQ'][$j]="";
				$_SESSION['whsSQ'][$j]="";
				$_SESSION['BaseRefSQ'][$j]='';
			}
			if($_SESSION['itemnameSQ'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeSQ'][$j]="";
				$_SESSION['itemnameSQ'][$j]="";
				$_SESSION['qtySQ'][$j]="";
				$_SESSION['qtyOpenSQ'][$j]="";
				$_SESSION['uomSQ'][$j]="";
				$_SESSION['priceSQ'][$j]="";
				$_SESSION['discSQ'][$j]="";
				$_SESSION['whsSQ'][$j]="";
				$_SESSION['BaseRefSQ'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemSQ'];$j++)
			{
				if($_SESSION['itemnameSQ'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameSQ'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameSQ'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsSQ'][$j]);
					
					if($_SESSION['uomSQ'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomSQ'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomSQ'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomSQ'][$j];
					}
					
					if($_SESSION['discSQ'][$j]=='')
					{
						$_SESSION['discSQ'][$j]=0;
					}
					
					if($_SESSION['statusSQ'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discSQ'][$j])/100)*$_SESSION['priceSQ'][$j]*$_SESSION['qtySQ'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeSQ'][$j].'</td>
						<td>'.$_SESSION['itemnameSQ'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtySQ'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenSQ'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceSQ'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discSQ'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusSQ'][$j]=='O' and $_SESSION['qtySQ'][$j]==$_SESSION['qtyOpenSQ'][$j])
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.str_replace("'","\'",$_SESSION["itemnameSQ"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemcodeSQ"][$j]).'\',\''.$_SESSION["qtySQ"][$j].'\',\''.str_replace("'","\'",$_SESSION["uomSQ"][$j]).'\',\''.str_replace("'","\'",$viewUoM).'\',\''.$_SESSION["priceSQ"][$j].'\',\''.$_SESSION["discSQ"][$j].'\',\''.str_replace("'","\'",$_SESSION["whsSQ"][$j]).'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.str_replace("'","\'",$_SESSION["itemcodeSQ"][$j]).'\',\''.str_replace("'","\'",$_SESSION["itemnameSQ"][$j]).'\',\''.$_SESSION["qtySQ"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
