<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_batch','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['ItemCode']=isset($_GET['ItemCode'])?$_GET['ItemCode']:''; // get the requested page
			$data['ItemName']=isset($_GET['ItemName'])?$_GET['ItemName']:''; // get the requested page
			$data['ItemGroup']=isset($_GET['ItemGroup'])?$_GET['ItemGroup']:0; // get the requested page
			
			$data['zeroqty']=isset($_GET['zeroqty'])?$_GET['zeroqty']:''; // get the requested page
			$data['Highlight']=isset($_GET['Highlight'])?$_GET['Highlight']:'NetQty'; // get the requested page
			$data['activeitem']=isset($_GET['activeitem'])?$_GET['activeitem']:''; // get the requested page
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('stock',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$data['listgrp']=$this->m_item_category->GetAllData();
			$data['autoitem']=$this->m_item->GetAllData();
			$whs = 'and a.intLocation in (0,';
			
			foreach($data['listwhs']->result() as $a)
			{
				$index=$a->vcCode;
				if(isset($_GET["loc".$index]) or ($a->intID==$data['defaultwhs'] and !isset($_GET['Highlight'])))
				{
					$data['sloc'][$index]=1;
					$whs = $whs."".$a->intID.",";
					
				}
			}
			$whs = rtrim($whs,", ");
			$whs =$whs.")";
			$data['whs']=$whs;
			
			if($data['zeroqty']=='')
			{
				$data['zero']='';
			}
			else
			{
				$data['zero']='and a.intStock>0';
			}
			if($data['activeitem']=='')
			{
				$data['active']=0;
			}
			else
			{
				$data['active']=1;
			}
			$data['liststock']=$this->m_stock->GetDataStockReport($data);
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['ItemGroup']);
			$data['uservalue']=$this->m_user->getByID($_SESSION['IDPOS'])->intValue;
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function activeornot()
	{
		$this->m_item->activeornot($_GET['item']);
	}
	public function detailbatch($item)
	{
		$listbatch=$this->m_batch->GetBatchByItemAndLoc($item,$_GET['loc']);
		echo '<table id="exampledetailBatchStock" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Stock</th>
				  <th>Remarks</th>
				  <th>Date</th>
                </tr>
                </thead>
                <tbody>';
				foreach($listbatch->result() as $d) 
				{
                echo '<tr>
                  <td>'.$d->vcDocNum.'</td>
				  <td align="right">'.number_format($d->intStock,'2').'</td>
				  <td>'.$d->vcRemarks.'</td>
				  <td>'.$d->dtInsertTime.'</td>
                </tr>
				';
				}
				echo '
                </tbody>

              </table>';
			  
			  echo '
		
		<script>
		  $(function () {
			$("#exampledetailBatchStock").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	public function detailopenso($item)
	{
		$listso=$this->m_so->GetOpenDataWithItemAndLoc($item,$_GET['loc']);
		echo '<table id="exampleOPENsSO" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>BP. Name</th>
				  <th>Open Qty</th>
                </tr>
                </thead>
                <tbody>';
				foreach($listso->result() as $d) 
				{
                echo '<tr>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->vcBPName.'</td>
				  <td>'.number_format($d->intOpenQty,'0').'</td>
                </tr>
				';
				}
				echo '
                </tbody>

              </table>';
			  
			  echo '
		
		<script>
		  $(function () {
			$("#exampleOPENsSO").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
}
