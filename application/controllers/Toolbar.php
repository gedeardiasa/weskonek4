<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toolbar extends CI_Controller {

	//public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_toolbar','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_report','',TRUE);
		$this->load->model('m_history','',TRUE);
		
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_do','',TRUE);
		$this->load->model('m_sr','',TRUE);
		
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_pr','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_apcm','',TRUE);
		$this->load->model('m_pp','',TRUE);
		
		$this->load->model('m_adjustment','',TRUE);
		
		$this->load->model('m_jurnal','',TRUE);
	
		$this->load->library('message');
        $this->load->database('default');
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	public function index()
	{	
		echo "Hahaha";
	
	}
	function jurnal($id)
	{
		$typex= $_GET['type']."X";
		$data['header'] = $this->m_jurnal->GetHeaderJurnalByRefDocNum($id,$_GET['type']);
		$data['list'] 	= $this->m_jurnal->GetDetailJurnalByRefDocNum($id,$_GET['type']);
		$data['headerx2'] = $this->m_jurnal->GetHeaderJurnalByRefDocNumR($id,$typex);
		
		if($data['header']!=null)
		{
			$this->load->view($this->uri->segment(1).'/jurnal/view',$data);
		}
		
	}
	function history($id=0)
	{
		if($id!=0)
		{
			$type = $_GET['type'];
			if($type=='SQ' or $type=='BOM' or $type=='DO' or $type=='PO' or $type=='PP' or $type=='PRO' or $type=='SO' or $type=='WO')
			{
				$id = $this->m_history->getdatabyquery("select intID as id from h$type where vcDocNum=$id"); // get data id header
			}
			else
			{
				$id = $id;
			}
			$data['list'] = $this->m_history->GetDataByDocAndKey($id,$_GET['type']);
			if($data['list']!=null)
			{
				$this->load->view($this->uri->segment(1).'/history/view',$data);
			}
		}
	}
	function defined_flow($doc)
	{
		$docbefore 	= null;
		$docafter	= null;
		if($doc=='SO')
		{
			$docbefore[0]		= 'SQ';
			
			$docafter[0]		= 'DO';
			$docafter[1]		= 'DN';
			$docafter[2]		= 'AR';
		}
		else if($doc=='SQ')
		{
			
			$docafter[0]		= 'SO';
			$docafter[1]		= 'DO';
			$docafter[2]		= 'DN';
			$docafter[3]		= 'AR';
		}
		else if($doc=='DN')
		{
			$docbefore[0]		= 'SQ';
			$docbefore[1]		= 'SO';
			$docbefore[2]		= 'DO';
			
			$docafter[0]		= 'AR';
			$docafter[1]		= 'SR';
		}
		else if($doc=='DO')
		{
			$docbefore[0]		= 'SQ';
			$docbefore[1]		= 'SO';
			
			$docafter[0]		= 'DN';
		}
		else if($doc=='AR')
		{
			$docbefore[0]		= 'SQ';
			$docbefore[1]		= 'SO';
			$docbefore[1]		= 'DN';
			
			$docafter[0]		= 'ARCM';
			$docafter[1]		= 'INPAY';
			$docafter[2]		= 'ARDP';
		}
		else if($doc=='SR')
		{
			$docbefore[0]		= 'DN';
			
			$docafter[0]		= 'ARCM';
		}
		else if($doc=='ARCM')
		{
			$docbefore[0]		= 'SR';
			$docbefore[1]		= 'AR';
			
			$docafter[0]		= 'INPAY';
			$docafter[1]		= 'OUTPAY';
		}
		else if($doc=='PP')
		{
			
			$docafter[0]		= 'PO';
		}
		else if($doc=='PO')
		{
			$docbefore[0]		= 'PR';
			
			$docafter[0]		= 'GRPO';
			$docafter[1]		= 'AP';
		}
		else if($doc=='GRPO')
		{
			$docbefore[0]		= 'PO';
			
			$docafter[0]		= 'AP';
			$docafter[1]		= 'PR';
		}
		else if($doc=='AP')
		{
			$docbefore[0]		= 'GRPO';
			$docbefore[1]		= 'PO';
			
			$docafter[0]		= 'APCM';
			$docafter[1]		= 'OUTPAY';
			$docafter[2]		= 'APDP';
		}
		else if($doc=='APCM')
		{
			$docbefore[0]		= 'PR';
			$docbefore[1]		= 'AP';
			
			$docafter[0]		= 'INPAY';
			$docafter[1]		= 'OUTPAY';
		}
		else if($doc=='PR')
		{
			$docbefore[0]		= 'GRPO';
			
			$docafter[0]		= 'APCM';
		}
		else
		{
			return null;
		}
		$d['docbefore']			= $docbefore;
		$d['docafter']			= $docafter;
		
		return $d;
	}
	function print_th_doc_before()
	{
		echo '
				
					<table id="exampleDocBefore" class="table table-striped dt-responsive jambo_table hover" style="width:100%; border:solid 1px">
						<thead>
						<tr>
						  <th></th>
						  <th>Doc. Number</th>
						  <th>Doc. Date</th>
						  <th>Doc. Type</th>
						</tr>
						</thead>
						<tbody>
				';
	}
	function print_th_doc_after()
	{
		echo '
				
					<table id="exampleDocAfter" class="table table-striped dt-responsive jambo_table hover" style="width:100%; border:solid 1px">
						<thead>
						<tr>
						  <th>Doc. Number</th>
						  <th>Doc. Date</th>
						  <th>Doc. Type</th>
						  <th></th>
						</tr>
						</thead>
						<tbody>
				';
	}
	function print_close_th()
	{
		echo "
					</tbody>
				</table><br>
				";
	}
	function print_data_flow($rs,$doc,$i,$tp)
	{
		if($tp=='aftr')
		{
			echo '
							<tr>
								<td><a href="'. base_url().'index.php/'.strtolower($doc[$i]).'?id='.$rs->intID.'" target="_blank">'.$rs->vcDocNum.'</a></td>
								<td>'.$rs->dtDate.'</td>
								<td>'.$this->m_docnum->GetNameByCode($doc[$i]).'</td>
							
							';
		}
		else if($tp=='bfr')
		{
			echo '
							
								<td><a href="'. base_url().'index.php/'.strtolower($doc[$i]).'?id='.$rs->intID.'" target="_blank">'.$rs->vcDocNum.'</a></td>
								<td>'.$rs->dtDate.'</td>
								<td>'.$this->m_docnum->GetNameByCode($doc[$i]).'</td>
							</tr>
							';
		}
	}
	function core_doc_flow($id,$type,$bfr,$aftr)
	{
		$dataflow		= $this->defined_flow($type);
		
		if($dataflow!=null)
		{
			$docbefore		= $dataflow['docbefore'];
			$docafter		= $dataflow['docafter'];
			if(isset($docbefore) and $bfr==1)
			{
				$this->print_th_doc_before();
				for($i=0;$i<count($docbefore);$i++)
				{
					$rslt = $this->m_docnum->GetDocRelationBefore($id,$docbefore[$i],$type);
					foreach($rslt->result() as $rs)
					{
						echo '<tr><td>';
						$this->core_doc_flow($rs->vcDocNum,$docbefore[$i],1,0);
						echo '</td>';
						$this->print_data_flow($rs,$docbefore,$i,'bfr');
						
					}
				}
				$this->print_close_th();
			}
			//echo '<hr>';
			if(isset($docafter) and $aftr==1)
			{
				$this->print_th_doc_after();
				for($i=0;$i<count($docafter);$i++)
				{
					$rslt = $this->m_docnum->GetDocRelationAfter($id,$docafter[$i],$type);
					foreach($rslt->result() as $rs)
					{
						$this->print_data_flow($rs,$docafter,$i,'aftr');
						echo '<td>';
						$this->core_doc_flow($rs->vcDocNum,$docafter[$i],0,1);
						echo '</td></tr>';
						
					}
				}
				$this->print_close_th();
			}
		}
	}
	function docflow($id)
	{
		$this->load->view('T1');
		$this->load->view('body');
		$this->core_doc_flow($id,$_GET['type'],1,1);
		$this->load->view('T2'); 
	}
	function createTree(&$list, $parent){
				$tree = array();
				foreach ($parent as $k=>$l){
					if(isset($list[$l['id']])){
						$l['children'] = $this->createTree($list, $list[$l['id']]);
					}
					$tree[] = $l;
				} 
				print_r($tree);
			}
	function prints($id)
	{
		$dbmaster=$this->load->database('master', TRUE);
		$qclient = $dbmaster->query("select a.vcSubDomain from mclient a 
		LEFT JOIN muser c on a.intID=c.intClient
		where c.vcUserID='$_SESSION[UsernamePOS]'");
		$rclient =$qclient->row();
		$subdomain = $rclient->vcSubDomain;
		if($_GET['type']=='POS')
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Print',$typemessage);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			
			$data['headerAR']=$this->m_ar->GetHeaderByDocNum($id);
			$data['profile']=$this->m_toolbar->GetProfil($data['headerAR']->intID);
			$data['profileH']=$this->m_toolbar->GetProfilHeader();
			
			
			$data['detailAR']=$this->m_ar->GetDetailByHeaderID($data['headerAR']->intID);
			$data['cashier']=$this->m_user->getByCode($data['headerAR']->vcUser);
			$data['docsetting']=$this->m_docnum->GetDocSetting('POS');
			if($customprint==0)
			{
				$this->load->view($this->uri->segment(1).'/print/pos',$data);
			}
			else
			{
				$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/pos',$data);
			}
		}
		if($_GET['type']=='ARCM')
		{
			$data['header']=$this->m_arcm->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_arcm->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('ARCM');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/arcm',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/arcm',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/arcm',$data);
					}
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PNL')
		{
			
			$explode = explode("|",$_GET['adtdata']);
			$data['plan']		=$explode[1];
			$data['daterange']	=$explode[0];
			$dt=explode("-",$data['daterange']);
			$data['from']=str_replace("/","-",$dt[0]);
			$data['until']=str_replace("/","-",$dt[1]);
			
			$data['list1']=$this->m_report->GetPNL1($data);
			$data['list2']=$this->m_report->GetPNL2($data);
			$data['list3']=$this->m_report->GetPNL3($data);
			$data['list4']=$this->m_report->GetPNL4($data);
			$data['list5']=$this->m_report->GetPNL5($data);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			$data['docsetting']=$this->m_docnum->GetDocSetting('PNL');
			if(isset($_GET['pdf']))
			{
				$this->load->view($this->uri->segment(1).'/pdf/pnl',$data);
			}
			else
			{
				if($customprint==0)
				{
					$this->load->view($this->uri->segment(1).'/print/pnl',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/pnl',$data);
				}
				
			}
		}
		else if($_GET['type']=='BALANCE')
		{
			
			$explode = explode("|",$_GET['adtdata']);
			$data['plan']		=$explode[1];
			$data['date']		=$explode[0];
			$data['Date']		=str_replace("/","-",$data['date']);
			
			
			$data['haveaccessallplan']=$this->m_plan->cekishaveallaccess($_SESSION['IDPOS']);
			$data['listplan']=$this->m_plan->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['list1']=$this->m_report->GetBalanceSheet1($data);
			$data['list2']=$this->m_report->GetBalanceSheet2($data);
			$data['list3']=$this->m_report->GetBalanceSheet3($data);
			$data['list4']=$this->m_report->GetBalanceSheet4($data);
			$data['list5']=$this->m_report->GetBalanceSheet5($data);
			
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			$data['docsetting']=$this->m_docnum->GetDocSetting('BALANCE');
			if(isset($_GET['pdf']))
			{
				$this->load->view($this->uri->segment(1).'/pdf/balance',$data);
			}
			else
			{
				if($customprint==0)
				{
					$this->load->view($this->uri->segment(1).'/print/balance',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/balance',$data);
				}
				
			}
		}
		else if($_GET['type']=='SQ')
		{
			$data['header']=$this->m_sq->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_sq->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SQ');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/sq',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/sq',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/sq',$data);
					}
					
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='AD')
		{
			$data['header']=$this->m_adjustment->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_adjustment->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('AD');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/ar',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/ad',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/ad',$data);
					}
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='AR')
		{
			$data['header']=$this->m_ar->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_ar->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('AR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/ar',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/ar',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/ar',$data);
					}
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='SO')
		{
			$data['header']=$this->m_so->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_so->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/so',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/so',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/so',$data);
					}
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='DN')
		{
			$data['header']=$this->m_dn->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_dn->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('DN');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/dn',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/dn',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/dn',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='DO')
		{
			$data['header']=$this->m_do->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_do->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('DO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/do',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/do',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/do',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='SR')
		{
			$data['header']=$this->m_sr->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_sr->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/sr',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/sr',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/sr',$data);
					}
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PO')
		{
			$data['header']=$this->m_po->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_po->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('PO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/po',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/po',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/po',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PR')
		{
			$data['header']=$this->m_pr->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_pr->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('PR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/pr',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/pr',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/pr',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='GRPO')
		{
			$data['header']=$this->m_grpo->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_grpo->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('GRPO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/grpo',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/grpo',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/grpo',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='AP')
		{
			$data['header']=$this->m_ap->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_ap->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('AP');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/ap',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/ap',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/ap',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='APCM')
		{
			$data['header']=$this->m_apcm->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_apcm->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('APCM');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/apcm',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/apcm',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/apcm',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PP')
		{
			$data['header']=$this->m_pp->GetHeaderByDocNum($id);
			$customprint =  $this->m_docnum->GetCustomPrintByCode($_GET['type']);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_pp->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('PP');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/pp',$data);
				}
				else
				{
					if($customprint==0)
					{
						$this->load->view($this->uri->segment(1).'/print/pp',$data);
					}
					else
					{
						$this->load->view($this->uri->segment(1).'/print/custom/'.$subdomain.'/pp',$data);
					}
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
	}
	function pdf($id)
	{
		if($_GET['type']=='ARCM')
		{
			$data['docsetting']=$this->m_docnum->GetDocSetting('ARCM');
			$this->load->library('Pdf');
			$pdf = new TCPDF('L', 'mm', array('236','140'), true, 'UTF-8', false);
			
			//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->SetFont('dejavusans', '', 9);

			$pdf->AddPage();

			$html = file_get_contents('https://app.weskonek.id/index.php/toolbar/prints/'.$id.'?type=ARCM&pdf=true');
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output('example_006.pdf', 'I');
		}
		else if($_GET['type']=='AR')
		{
			$data['docsetting']=$this->m_docnum->GetDocSetting('AR');
			$this->load->library('Pdf');
			$pdf = new TCPDF('L', 'mm', array('236','140'), true, 'UTF-8', false);
			
			//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->SetFont('dejavusans', '', 9);

			$pdf->AddPage();


			$html = file_get_contents('https://app.weskonek.id/index.php/toolbar/prints/'.$id.'?type=AR&pdf=true');
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output('example_006.pdf', 'I');
			
		}
	}
	function cekhttp()
	{
		$w = stream_get_wrappers();
		echo 'openssl: ',  extension_loaded  ('openssl') ? 'yes':'no', "\n";
		echo 'http wrapper: ', in_array('http', $w) ? 'yes':'no', "\n";
		echo 'https wrapper: ', in_array('https', $w) ? 'yes':'no', "\n";
		echo 'wrappers: ', var_export($w);
		
	}
	function next()
	{
		
		$id=$_POST['idHeader'];
		if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='AD')
		{
			$table='hAdjustment';
		}
		else if($_POST['type']=='IT')
		{
			$table='hIT';
		}
		else if($_POST['type']=='SO')
		{
			$table='hSO';
		}
		else if($_POST['type']=='DN')
		{
			$table='hDN';
		}
		else if($_POST['type']=='AR')
		{
			$table='hAR';
		}
		else if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='ARCM')
		{
			$table='hARCM';
		}
		else if($_POST['type']=='SR')
		{
			$table='hSR';
		}
		else if($_POST['type']=='PO')
		{
			$table='hPO';
		}
		else if($_POST['type']=='GRPO')
		{
			$table='hGRPO';
		}
		else if($_POST['type']=='AP')
		{
			$table='hAP';
		}
		else if($_POST['type']=='PR')
		{
			$table='hPR';
		}
		else if($_POST['type']=='APCM')
		{
			$table='hAPCM';
		}
		else if($_POST['type']=='INPAY')
		{
			$table='hINPAY';
		}
		else if($_POST['type']=='OUTPAY')
		{
			$table='hOUTPAY';
		}
		else if($_POST['type']=='REVALUATION')
		{
			$table='hRevaluation';
		}
		else if($_POST['type']=='PID')
		{
			$table='hPID';
		}
		else if($_POST['type']=='IP')
		{
			$table='hIP';
		}
		else if($_POST['type']=='DISC')
		{
			$table='hDiscount';
		}
		else if($_POST['type']=='BOM')
		{
			$table='hBOM';
		}
		else if($_POST['type']=='PRO')
		{
			$table='hPRO';
		}
		else if($_POST['type']=='JE')
		{
			$table='hJE';
		}
		else if($_POST['type']=='WO')
		{
			$table='hWO';
		}
		else if($_POST['type']=='PP')
		{
			$table='hPP';
		}
		else if($_POST['type']=='CAP')
		{
			$table='hCAP';
		}
		else if($_POST['type']=='DEP')
		{
			$table='hDEP';
		}
		else if($_POST['type']=='DEPR')
		{
			$table='hDEPR';
		}
		else if($_POST['type']=='RT')
		{
			$table='hRT';
		}
		else if($_POST['type']=='RV')
		{
			$table='hRV';
		}
		else if($_POST['type']=='AT')
		{
			$table='hAT';
		}
		else if($_POST['type']=='DO')
		{
			$table='hDO';
		}
		else if($_POST['type']=='ROUT')
		{
			$table='hROUT';
		}
		$nextid=$this->m_toolbar->GetNextId($id,$table);
		echo $nextid;
	}
	function previous()
	{
		$id=$_POST['idHeader'];
		if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='AD')
		{
			$table='hAdjustment';
		}
		else if($_POST['type']=='IT')
		{
			$table='hIT';
		}
		else if($_POST['type']=='SO')
		{
			$table='hSO';
		}
		else if($_POST['type']=='DN')
		{
			$table='hDN';
		}
		else if($_POST['type']=='AR')
		{
			$table='hAR';
		}
		else if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='ARCM')
		{
			$table='hARCM';
		}
		else if($_POST['type']=='SR')
		{
			$table='hSR';
		}
		else if($_POST['type']=='PO')
		{
			$table='hPO';
		}
		else if($_POST['type']=='GRPO')
		{
			$table='hGRPO';
		}
		else if($_POST['type']=='AP')
		{
			$table='hAP';
		}
		else if($_POST['type']=='PR')
		{
			$table='hPR';
		}
		else if($_POST['type']=='APCM')
		{
			$table='hAPCM';
		}
		else if($_POST['type']=='INPAY')
		{
			$table='hINPAY';
		}
		else if($_POST['type']=='OUTPAY')
		{
			$table='hOUTPAY';
		}
		else if($_POST['type']=='REVALUATION')
		{
			$table='hRevaluation';
		}
		else if($_POST['type']=='PID')
		{
			$table='hPID';
		}
		else if($_POST['type']=='IP')
		{
			$table='hIP';
		}
		else if($_POST['type']=='DISC')
		{
			$table='hDiscount';
		}
		else if($_POST['type']=='BOM')
		{
			$table='hBOM';
		}
		else if($_POST['type']=='PRO')
		{
			$table='hPRO';
		}
		else if($_POST['type']=='JE')
		{
			$table='hJE';
		}
		else if($_POST['type']=='WO')
		{
			$table='hWO';
		}
		else if($_POST['type']=='PP')
		{
			$table='hPP';
		}
		else if($_POST['type']=='CAP')
		{
			$table='hCAP';
		}
		else if($_POST['type']=='DEP')
		{
			$table='hDEP';
		}
		else if($_POST['type']=='DEPR')
		{
			$table='hDEPR';
		}
		else if($_POST['type']=='RT')
		{
			$table='hRT';
		}
		else if($_POST['type']=='RV')
		{
			$table='hRV';
		}
		else if($_POST['type']=='AT')
		{
			$table='hAT';
		}
		else if($_POST['type']=='DO')
		{
			$table='hDO';
		}
		else if($_POST['type']=='ROUT')
		{
			$table='hROUT';
		}
		$previousid=$this->m_toolbar->GetPreviousId($id,$table);
		echo $previousid;
	}
}
