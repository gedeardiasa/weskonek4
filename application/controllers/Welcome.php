<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_dashboard','',TRUE);
		$this->load->model('m_report','',TRUE);
		$this->load->model('m_manual_dep','',TRUE);
		$this->load->model('m_dep_run','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		
		$this->load->model('m_jurnal','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_report','',TRUE);
        $this->load->database();
		if(!isset($_SESSION['LangPOS']))
		{
			$_SESSION['LangPOS']='en';
		}
		if($_SESSION['LangPOS']=='en')
		{
			$this->lang->load("primary","english");
		}
		else if($_SESSION['LangPOS']=='id')
		{
			$this->lang->load("primary","indonesia");
		}
    }
	
	public function index()
	{	
		$this->authorization->cektoken();
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{	
			if(!isset($_SESSION[md5('posroot')]))
			{
				
				// fungsi cek depresiasi
				$daynow		= date('d');
				$datadep 	= $this->m_dep_run->GetDataToRun($daynow);
				$this->db->trans_begin();
				foreach($datadep->result() as $dd)
				{
					$vcDocNum 	= $dd->vcDocNum;
					$daterun	= date('Y-m')."-".$dd->intDateRun;
					$cekrun 	= $this->m_manual_dep->cekrun($vcDocNum,$daterun);
					if($cekrun==0)// buat depresiasi manual
					{
						$detaildep = $this->m_dep_run->GetDetail($dd->intID);
						
						$_SESSION['totitemDEP'] = 0;
						unset($_SESSION['itemcodeDEP']);
						unset($_SESSION['itemnameDEP']);
						unset($_SESSION['costDEP']);
						foreach($detaildep->result() as $dj)
						{
							$i=$_SESSION['totitemDEP'];
							$_SESSION['itemcodeDEP'][$i]=$dj->vcItemCode;
							$_SESSION['itemnameDEP'][$i]=$dj->vcItemName;
							$_SESSION['costDEP'][$i]=$dj->intCost;
							$_SESSION['totitemDEP']++;
						}
						
						$data['DocNum'] = $this->m_docnum->GetLastDocNum('hDEP');
						$data['DocDate'] = $daterun;
						$data['RefNum'] = $dd->vcDocNum;
						$data['Whs'] = $dd->intLocation;
						$data['Remarks'] = 'create from depreciation run (Automatic)'; // get the requested page
						
						
						$head=$this->m_manual_dep->insertH($data);
						
						//proses jurnal
						$headDocnum=$this->m_manual_dep->GetHeaderByHeaderID($head)->vcDocNum;
						$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data['Whs']);//Ambil id plan
						$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
						$dataJurnal['DocDate']=$data['DocDate']; // docdate jurnal = docdate transaksi
						$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
						$dataJurnal['Remarks']='';
						$dataJurnal['RefType']='DEP';
						$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
						if($head!=0)
						{
							for($j=0;$j<$_SESSION['totitemDEP'];$j++)// save detail
							{
								if($_SESSION['itemcodeDEP'][$j]!="")
								{
									$cek=1;
									$item=$this->m_item->GetIDByName($_SESSION['itemnameDEP'][$j]);
									$da['intHID']=$head;
									$da['itemcodeDEP']=$_SESSION['itemcodeDEP'][$j];
									$da['itemnameDEP']=$_SESSION['itemnameDEP'][$j];
									$da['Whs']=$data['Whs'];
									
									$da['costDEP']	= $_SESSION['costDEP'][$j];
									$linetotal 		= $da['costDEP'];
									$linetotalmin 	= $linetotal*-1;
									
									$detail=$this->m_manual_dep->insertD($da);
									
									//$this->m_item->UpdateAPC($item,$linetotal);
									$this->m_item->UpdateNetBook($item,$linetotalmin);
									$this->m_item->UpdateDep($item,$linetotal);
									
									//start detail jurnal
									$debact = $this->m_item->GetGLAsset($item)->vcGLDep;
									$creact = $this->m_item->GetGLAsset($item)->vcGLAPC;
									$daJurnal['intHID']=$headJurnal;
									$daJurnal['DCJE']='D';
									
									$daJurnal['ValueJE']=$linetotal;
									
									$val_min=$daJurnal['ValueJE']*-1;
									
									
										$daJurnal['GLCodeJE']=$debact;
										$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
										$daJurnal['GLCodeJEX']=$creact;
										$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
									
									
									$detailJurnal=$this->m_jurnal->insertD($daJurnal);
									$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
									$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
									//end detail jurnal
								}
								
							}
							
						}
						$_SESSION['totitemDEP']=0;
						unset($_SESSION['itemcodeDEP']);
						unset($_SESSION['itemnameDEP']);
						unset($_SESSION['costDEP']);
					}
				}
				$this->db->trans_complete();
				
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				
				$data['dashboard']=$this->m_dashboard->GetAccessDashboard($_SESSION);
				
				if($data['dashboard']->num_rows()>0)
				{
					$data['show']=1;
				}
				else{
					$data['show']=0;
				}
				$data['form']=$this->authorization->GetForm($_SESSION);
				$data['navigation']=$this->authorization->GetNavigation('user');
			}
			else
			{
				$data['root'] = true;
			}
			
			$this->load->view('dashboard',$data);
		}
		
	}
	function tesemail()
	{
		$this->load->library('mailer');
				
		$name[0]="Gede";
		$email[0]="gede.ardiasa99@gmail.com";
		$subject="Alert Management";
		$html = "Hahahaha";
		$this->mailer->sendemail2($name,$email,$subject,$html,"Weskonek",'');
	}
	public function search()
	{	
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{	
			$data['key'] = isset($_GET['key'])?$_GET['key']:''; // get the requested page
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['navigation']=$this->authorization->GetNavigation('user');
			$data['list']=$this->m_report->GetSearch($data['key']);
			$this->load->view('search',$data);
		}
	}
	function ceklogin()
	{
		$data['link']=$this->config->base_url().'';
		$cek=$this->authorization->login(str_replace(' ','',$_GET['Username']),$_GET['Password']);
		if($cek==1)
		{
			echo '1';
		}
		else if($cek==2)
		{
			echo '2';
		}
		else
		{
			echo '0';
		}
	}
	function coba()
	{
		$this->load->view('coba');
		
	}
	function coba2()
	{
		echo "Hahahahahaha";
	}
	function continuelogons()
	{
		$this->authorization->continuelogons();
	}
	public function logout()
	{
		$data['link']=$this->config->base_url().'';
		if(!isset($_SESSION[md5('posroot')]))
		{
			if(isset($_SESSION['IDPOS']))
			{
				$cek=$this->authorization->deleteToken($_SESSION['IDPOS']);
			}
		}
		unset($_SESSION['IDPOS']);
		unset($_SESSION['UsernamePOS']);
		unset($_SESSION['PasswordPOS']);
		unset($_SESSION['NamePOS']);
		unset($_SESSION['EmailPOS']);
		unset($_SESSION['LangPOS']);
		unset($_SESSION['DatePOS']);
		unset($_SESSION['tokenPOS']);
		unset($_SESSION['SHOWMODALITEMPOS']);
		$xx=md5('db_pos_setting');
		unset($_SESSION[$xx]);
		unset($_SESSION[md5('posroot')]);
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	public function profile()
	{
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{
			$id=$_SESSION['IDPOS'];
			if(!isset($_SESSION[md5('posroot')]))
			{
				$data['form']=$this->authorization->GetForm($_SESSION);
				$data['navigation']=$this->authorization->GetNavigation('welcome');
				$data['user']=$this->m_user->getByID($id);
			}
			else
			{
				$data['user']=$this->m_user->GetUserRoot();
			}
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view','');
			$this->load->view('profile',$data);
		}
	}
	function proseseditprofile()
	{
		$id=$_SESSION['IDPOS'];
		$data['id'] = $id; // get the requested page
		$data['UserID'] = isset($_POST['UserID'])?$_POST['UserID']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Phone'] = isset($_POST['Phone'])?$_POST['Phone']:''; // get the requested page
		$data['SetIcon'] = isset($_POST['SetIcon'])?$_POST['SetIcon']:''; // get the requested page
		if(!isset($_SESSION[md5('posroot')]))
		{
			if($_FILES['Image']['tmp_name']!='')
			{
				$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
			}
			else
			{
				$data['imgData']=null;
			}
		}
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
		$cek=$this->m_user->editProfile($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/welcome/profile?success_edit=true';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/welcome/profile?error_edit=true';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditprofilepass()
	{
		$id=$_SESSION['IDPOS'];
		$data['id'] = $id; // get the requested page
		$data['OldPassword'] = isset($_POST['OldPassword'])?$_POST['OldPassword']:''; // get the requested page
		$data['NewPassword'] = isset($_POST['NewPassword'])?$_POST['NewPassword']:''; // get the requested page
		$data['ConfirmPassword'] = isset($_POST['ConfirmPassword'])?$_POST['ConfirmPassword']:''; // get the requested page
		
		if($data['NewPassword']!=$data['ConfirmPassword'])
		{
			$data['link']=$this->config->base_url().'index.php/welcome/profile?error_confirmpass=true';
		}
		else
		{
			$cek=$this->m_user->cekpass($data);
			if($cek==1)
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
				$this->m_user->editpass($data);
				$data['link']=$this->config->base_url().'index.php/welcome/logout';
			}
			else{
				$data['link']=$this->config->base_url().'index.php/welcome/profile?error_oldpass=true';
			}
		}
		
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function getLogonsInformation()
	{
		$log=$this->authorization->getLogonsInformation($_SESSION['IDPOS']);
		echo '
		User '.$log->vcUserID.' is already logged on system<br>
		(Terminal '.$log->vcIP.'-'.$log->vcCompName.', since '.$log->dtInsertTime.')<br>
		Note : Continue this logon will end any other logons on system with same user
		';
	}
	function skiptutorial()
	{
		$this->authorization->skiptutorial();
	}
	function tessession()
	{
		$_SESSION['totitemBatch'][1] = 10;
		unset($_SESSION['totitemBatch']);
		echo $_SESSION['totitemBatch'][1];
	}
	function setmutationje()
	{
		$this->authorization->setmutationje();
	}
	function getcolumnandtable()
	{
		$table = $this->m_report->GetAllTable();
		foreach($table->result() as $t)
		{
			$column = $this->m_report->GetColumnByTable($t->TABLE_NAME);
			foreach($column->result() as $c)
			{
				echo $c->Field."<br>";
			}
		}
	}
	function asdf()
	{
		echo 'hahaha';
	}
	function shopee_api()
	{
		$data['tes'] = 0;
		$this->load->view('shopee',$data);
	}
	function CallAPI($method, $url, $data = false)
	{
		$curl = curl_init();

		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);

				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// Optional Authentication:
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "username:password");

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);

		curl_close($curl);

		return $result;
	}
	function changeremarks()
	{
		$this->m_coa->changeremarks();
		
	}
	function sp()
	{
		//$s	= $this->CallAPI('POST','https://partner.shopeemobile.com/api/v1/shop/get');
		//$s = file_get_contents('https://partner.shopeemobile.com/api/v1/shop/get');
		$post_fields = array(
					'partner_id' => 844369,
					'shopid' => 33512386,
					'timestamp' => 1583979170,
				);

		// encode json data
		$data = json_encode($post_fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://partner.shopeemobile.com/api/v1/shop/get");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		//curl_setopt($ch, CURLOPT_POSTFIELDS,
					//"shopid=33512386&partner_id=844369&timestamp=1583979170");
		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_USERPWD, "fd4ddfbb91c0bcbaccf65f0c6fa82b0b99b99da5cfb732d267893ea0e3a131f3");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: 62',
			'Authorization: 494947aec56fe1cb20f70babedf642ffcea85fbddb0b5d978bd627c202a082c0')
		);
		$server_output = curl_exec($ch);

		curl_close ($ch);

		echo $server_output;
		
		// request url
		//$baseurl='https://partner.shopeemobile.com/api/v1/shop/get';
		// $url='https://partner.shopeemobile.com/api/v1/shop/get';

		// // data to be posted
		// $post_fields = array(
					// 'partner_id' => 844369,
					// 'shopid' => 33512386,
					// 'timestamp' => 1583979170,
				// );

		// // encode json data
		// $data = json_encode($post_fields);

		// // initialize cURL
		// $ch = curl_init();
		
		// // set options
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// //curl_setopt($ch, CURLOPT_USERPWD, "$token:$password");
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// curl_setopt($ch, CURLOPT_POST, true);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			// 'Content-Type: application/json',
			// 'Content-Length: ' . strlen($data))
		// );

		// // make the request
		// $result = curl_exec($ch);

		// // decode the result
		// $json = json_decode($result, true);

		// // print the result
		// echo $result;

		// curl_close($ch);
	}
}
