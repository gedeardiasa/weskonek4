<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->library('message');
        $this->load->database();
		
    }
	
	public function index()
	{	
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{	
			if(!isset($_SESSION[md5('posroot')]))
			{
				$data['dashboard']=$this->m_dashboard->GetAccessDashboard($_SESSION);
				
				if($data['dashboard']->num_rows()>0)
				{
					$data['show']=1;
				}
				else{
					$data['show']=0;
				}
				$data['form']=$this->authorization->GetForm($_SESSION);
				$data['navigation']=$this->authorization->GetNavigation('user');
			}
			else
			{
				$data['root'] = true;
			}
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('client',$typemessage);
			
			$data['list']=$this->m_client->GetAllData();
			$this->load->view($this->uri->segment(1).'/'.$this->uri->segment(2).'/view',$data);
		}
	}
	function prosesaddedit()
	{
		switch ($_POST['type']) {
			case "add":
				
				$data['Domain'] = isset($_POST['Domain'])?$_POST['Domain']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['DB'] = isset($_POST['DB'])?$_POST['DB']:''; // get the requested page
				$cek=$this->m_client->insert($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?success=true&type=successadd';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?error=true&type=erroradd';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			case "edit":
				
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Domain'] = isset($_POST['Domain'])?$_POST['Domain']:''; // get the requested page
				$data['LastDomain'] = isset($_POST['LastDomain'])?$_POST['LastDomain']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['DB'] = isset($_POST['DB'])?$_POST['DB']:''; // get the requested page
				$cek=$this->m_client->edit($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?success=true&type=successedit';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?error=true&type=erroredit';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
	function prosesdelete()
	{	
		$id=$_POST['ID2'];
		$cek=$this->m_client->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
}
