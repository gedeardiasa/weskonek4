<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zpp001 extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_jurnal','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(3));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(3));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$subdomain   = $this->m_user->getSubDomainByvcUserId($_SESSION['UsernamePOS']);
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('wallet',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(3));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['listgl']=$this->m_coa->GetAllDataLevel5();
			
			$data['autoitem']	=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			$data['listwhs']	=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['DocDate']	= date('Y-m-d');
			$this->load->view('zprogram/'.$subdomain.'/'.$this->uri->segment(3).'/view',$data);
		}
	}
	function loadhpp()
	{
		$item	= $this->m_item->GetIDByName($_POST['ItemName']);
		$loc	= $_POST['Location'];
		$hpp	= $this->m_stock->GetCostItem($item,$loc);
		echo $hpp;
	}
	function cekdetailstok()
	{
		$item	= $this->m_item->GetIDByName($_POST['ItemName']);
		$loc	= $_POST['Location'];
		$stock	= $this->m_stock->GetStockItem($item,$loc);
		if($stock==0)
		{
			echo "X";
		}
		else
		{
			echo "Y";
		}
	}
	function prosesadd()
	{
		$data['ItemName'] 	= isset($_POST['ItemName'])?$_POST['ItemName']:''; // get the requested page
		$data['ItemName2'] 	= isset($_POST['ItemName2'])?$_POST['ItemName2']:''; // get the requested page
		$data['ItemName3'] 	= isset($_POST['ItemName3'])?$_POST['ItemName3']:''; // get the requested page
		
		$item				= $this->m_item->GetIDByName($_POST['ItemName']);
		
		$data['Val'] = isset($_POST['Val'])?$_POST['Val']:''; // get the requested page
		$data['Val2'] = isset($_POST['Val2'])?$_POST['Val2']:''; // get the requested page
		$data['Val3'] = isset($_POST['Val3'])?$_POST['Val3']:''; // get the requested page
		
		$data['Location'] = isset($_POST['Location'])?$_POST['Location']:''; // get the requested page
		$data['Acc'] = isset($_POST['Acc'])?$_POST['Acc']:''; // get the requested page
		
		$ItemCode2 		= $this->m_item->GetCodeByName($data['ItemName2']);
		$ItemCode3 		= $this->m_item->GetCodeByName($data['ItemName3']); 
		
		$itemacuan		= $this->m_item->getByID($item);
		
		$data2['Category'] = $itemacuan->intCategory;
		$data2['Code'] = $this->m_item->GetLastCode($data2['Category']);
		$data2['Barcode'] = "";
		
		$data2['Price'] = 0;
		$data2['Tax'] = $itemacuan->intTax;
		$data2['Remarks'] = "";
		$data2['UoM'] = $itemacuan->vcUoM;
		$data2['PurUoM'] = $itemacuan->vcPurUoM;
		$data2['intPurUoM'] = $itemacuan->intPurUoM;
		$data2['SlsUoM'] = $itemacuan->vcSlsUoM;
		$data2['intSlsUoM'] = $itemacuan->intSlsUoM;
		
		$data2['intSalesItem'] = $itemacuan->intSalesItem;
		$data2['intPurchaseItem'] = $itemacuan->intPurchaseItem;
		$data2['intSalesWithoutQty'] = $itemacuan->intSalesWithoutQty;
		
		$data2['imgData']=null;
		
		$this->db->trans_begin();
		if($ItemCode2==null) // jika itemcode tidak ada maka perlu create item baru
		{
			$data2['Name'] = $data['ItemName2'];
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(3),$this->uri->segment(4),'add',$data2['Code']);
			$cek=$this->m_item->insert($data2);
		}
		
		
		if($ItemCode3==null) // jika itemcode tidak ada maka perlu create item baru
		{
			
			$data2['Name'] = $data['ItemName3'];
			$data2['Code'] = $this->m_item->GetLastCode($data2['Category']);
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(3),$this->uri->segment(4),'add',$data2['Code']);
			$cek=$this->m_item->insert($data2);
		}
		
		
		$item2				= $this->m_item->GetIDByName($_POST['ItemName2']);
		$item3				= $this->m_item->GetIDByName($_POST['ItemName3']);
		// GR item 2
		$data3['DocNum'] = ""; // terbentuk sendiri di model
		$data3['DocDate'] = date('Y-m-d');
		$data3['Account'] = $data['Acc'];
		$data3['RefNum'] = "";
		$data3['Whs'] = $data['Location'];
		$data3['Type'] = "GR";
		$data3['Remarks'] = "Zpp001";
		
		$head3=$this->m_adjustment->insertH($data3);
		$headDocnum=$this->m_adjustment->GetHeaderByHeaderID($head3)->vcDocNum;
		//proses jurnal
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data3['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data3['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']='';
		$dataJurnal['RefType']='AD';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(3),$this->uri->segment(4),'add',$headDocnum);
		
			$da['intHID']		= $head3;
			$da['itemcodeAD']	= $this->m_item->GetCodeByName($data['ItemName2']);
			$da['itemnameAD']	= $data['ItemName2'];
			$da['Whs']			= $data['Location'];
			$da['qtyAD']		= 1;
			$da['uomAD']		= $this->m_item->GetUoMAllByName($data['ItemName2'])->vcUoM;
			$da['costAD']		= $data['Val2'];
			$detail3=$this->m_adjustment->insertD($da);
			
					//start detail jurnal
					$debact = $this->m_item_category->GetAccountByItemCode($da['itemcodeAD']);
					$creact = $data['Acc'];
						
					$daJurnal['intHID']=$headJurnal;
					$daJurnal['DCJE']='D';
						
					$daJurnal['GLCodeJE']=$debact;
					$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
					$daJurnal['GLCodeJEX']=$creact;
					$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
					$daJurnal['ValueJE']=$da['costAD']*$da['qtyAD'];
					$val_min=$daJurnal['ValueJE']*-1;
						
					$detailJurnal=$this->m_jurnal->insertD($daJurnal);
					$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
					$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					//end detail jurnal
			
			$this->m_stock->updateStock($item2,$da['qtyAD'],$da['Whs']);//update stok menambah/mengurangi di gudang
			if($data3['Type']=='GR')
			{
				$this->m_stock->updateCost($item2,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
			}
			$this->m_stock->addMutation($item2,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data3['DocDate'],$headDocnum);//add mutation
		//end gr item 2
		
		
		// GR item 3
		$data4['DocNum'] = ""; // terbentuk sendiri di model
		$data4['DocDate'] = date('Y-m-d');
		$data4['Account'] = $data['Acc'];
		$data4['RefNum'] = "";
		$data4['Whs'] = $data['Location'];
		$data4['Type'] = "GR";
		$data4['Remarks'] = "Zpp001";
		
		$head4=$this->m_adjustment->insertH($data4);
		$headDocnum=$this->m_adjustment->GetHeaderByHeaderID($head4)->vcDocNum;
		//proses jurnal
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data4['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data4['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']='';
		$dataJurnal['RefType']='AD';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(3),$this->uri->segment(4),'add',$headDocnum);
		
			$da['intHID']		= $head4;
			$da['itemcodeAD']	= $this->m_item->GetCodeByName($data['ItemName3']);
			$da['itemnameAD']	= $data['ItemName3'];
			$da['Whs']			= $data['Location'];
			$da['qtyAD']		= 1;
			$da['uomAD']		= $this->m_item->GetUoMAllByName($data['ItemName3'])->vcUoM;
			$da['costAD']		= $data['Val3'];
			$detail3=$this->m_adjustment->insertD($da);
					//start detail jurnal
					$debact = $this->m_item_category->GetAccountByItemCode($da['itemcodeAD']);
					$creact = $data['Acc'];
						
					$daJurnal['intHID']=$headJurnal;
					$daJurnal['DCJE']='D';
						
					$daJurnal['GLCodeJE']=$debact;
					$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
					$daJurnal['GLCodeJEX']=$creact;
					$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
					$daJurnal['ValueJE']=$da['costAD']*$da['qtyAD'];
					$val_min=$daJurnal['ValueJE']*-1;
						
					$detailJurnal=$this->m_jurnal->insertD($daJurnal);
					$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
					$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					//end detail jurnal
			
			$this->m_stock->updateStock($item3,$da['qtyAD'],$da['Whs']);//update stok menambah/mengurangi di gudang
			if($data4['Type']=='GR')
			{
				$this->m_stock->updateCost($item3,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
			}
			$this->m_stock->addMutation($item3,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data4['DocDate'],$headDocnum);//add mutation
		//end gr item 3
		
		// GI item 1
		$data5['DocNum'] = ""; // terbentuk sendiri di model
		$data5['DocDate'] = date('Y-m-d');
		$data5['Account'] = $data['Acc'];
		$data5['RefNum'] = "";
		$data5['Whs'] = $data['Location'];
		$data5['Type'] = "GI";
		$data5['Remarks'] = "Zpp001";
		
		$head5=$this->m_adjustment->insertH($data5);
		$headDocnum=$this->m_adjustment->GetHeaderByHeaderID($head5)->vcDocNum;
		//proses jurnal
		$dataJurnal['Plan']=$this->m_location->GetIDPlanByLocationId($data4['Whs']);//Ambil id plan
		$dataJurnal['DocNum']='';//docnum jurnal terbentuk sendiri saat di model
		$dataJurnal['DocDate']=$data4['DocDate']; // docdate jurnal = docdate transaksi
		$dataJurnal['RefNum']=$headDocnum; // refnumber jurnal adalah docnumber transaksi
		$dataJurnal['Remarks']='';
		$dataJurnal['RefType']='AD';
		$headJurnal=$this->m_jurnal->insertH($dataJurnal); // create jurnal
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(3),$this->uri->segment(4),'add',$headDocnum);
		
			$da['intHID']		= $head5;
			$da['itemcodeAD']	= $this->m_item->GetCodeByName($data['ItemName']);
			$da['itemnameAD']	= $data['ItemName'];
			$da['Whs']			= $data['Location'];
			$da['qtyAD']		= -1;
			$da['uomAD']		= $this->m_item->GetUoMAllByName($data['ItemName'])->vcUoM;
			$da['costAD']		= $data['Val'];
			$detail3=$this->m_adjustment->insertD($da);
					//start detail jurnal
					$debact = $data['Acc'];
					$creact = $this->m_item_category->GetAccountByItemCode($da['itemcodeAD']);
						
					$daJurnal['intHID']=$headJurnal;
					$daJurnal['DCJE']='D';
						
					$daJurnal['GLCodeJE']=$debact;
					$daJurnal['GLNameJE']=$this->m_coa->GetNameByCode($debact);
					$daJurnal['GLCodeJEX']=$creact;
					$daJurnal['GLNameJEX']=$this->m_coa->GetNameByCode($creact);
					$daJurnal['ValueJE']=$da['costAD']*$da['qtyAD']*-1;
					$val_min=$daJurnal['ValueJE']*-1;
						
					$detailJurnal=$this->m_jurnal->insertD($daJurnal);
					$this->m_coa->changebalance($daJurnal['GLCodeJE'],$daJurnal['ValueJE']);// update balance debits
					$this->m_coa->changebalance($daJurnal['GLCodeJEX'],$val_min);// update balance credits
					//end detail jurnal
			
			$this->m_stock->updateStock($item,$da['qtyAD'],$da['Whs']);//update stok menambah/mengurangi di gudang
			
			$this->m_stock->addMutation($item,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data5['DocDate'],$headDocnum);//add mutation
		//end GI Item 1
		
		
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
			echo 1;
		}
	}
}
