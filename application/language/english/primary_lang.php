<?php

/*
 * English language
 */

/*All*/
$lang['Exchange Rate'] = 'Exchange Rate';
$lang['Currency'] = 'Currency';
$lang['Add New Adjustment'] = 'Add New Adjustment';
$lang['Adjustment Detail'] = 'Adjustment Detail';
$lang['Success. You successfully process the data'] = 'Success. You successfully process the data';
$lang['Error. You failed to process the data'] = 'Error. You failed to process the data';
$lang['Doc. Number'] = 'Doc. Number';
$lang['Document Number'] = 'Document Number';
$lang['Doc. Date'] = 'Doc. Date';
$lang['Document Date'] = 'Document Date';
$lang['Ref. Number'] = 'Ref. Number';
$lang['Reference Number'] = 'Reference Number';
$lang['Cost Source'] = 'Cost Source';
$lang['Item Cost'] = 'Item Cost';
$lang['Warehouse'] = 'Warehouse';
$lang['Type'] = 'Type';
$lang['Goods Receipt'] = 'Goods Receipt';
$lang['Goods Issue'] = 'Goods Issue';
$lang['Trans. Type'] = 'Trans. Type';
$lang['Account'] = 'Account';
$lang['Detail'] = 'Detail';
$lang['Additional Data'] = 'Additional Data';
$lang['Form Detail'] = 'Form Detail';
$lang['Item Name'] = 'Item Name';
$lang['Type Item Name'] = 'Type Item Name';
$lang['Type Code'] = 'Type Code';
$lang['Item Code'] = 'Item Code';
$lang['Quantity'] = 'Quantity';
$lang['Qty'] = 'Qty';
$lang['Unit of Measure'] = 'Unit of Measure';
$lang['UoM'] = 'UoM';
$lang['Add'] = 'Add';
$lang['Update'] = 'Update';
$lang['Cancel'] = 'Cancel';
$lang['Remarks'] = 'Remarks';
$lang['Save'] = 'Save';
$lang['Add New'] = 'Add New';
$lang['Reload'] = 'Reload';
$lang['Success'] = 'Success';
$lang['Error'] = 'Error';
$lang['Please Fill Doc. Number'] = 'Please Fill Doc. Number';
$lang['Please Fill Warehouse'] = 'Please Fill Warehouse';
$lang['Please Fill Doc. Date'] = 'Please Fill Doc. Date';
$lang['Please Fill Account'] = 'Please Fill Account';
$lang['This Period is locked'] = 'This Period is locked';
$lang['Please Fill Detail Data'] = 'Please Fill Detail Data';
$lang['Insufficient stock'] = 'Insufficient stock';
$lang['Please Fill Item Name'] = 'Please Fill Item Name';
$lang['Please Fill Quantity'] = 'Please Fill Quantity';
$lang['Add New A/P Invoice'] = 'Add New A/P Invoice';
$lang['A/P Invoice Detail'] = 'A/P Invoice Detail';
$lang['Movement Type'] = 'Movement Type';
$lang['NOT SET'] = 'NOT SET';
$lang['Please Set Batch For Item With Batch Management and Make Sure The Batch Quantity is The Same as The Item Quantity'] = 'Please Set Batch For Item With Batch Management and Make Sure The Batch Quantity is The Same as The Item Quantity';
$lang['Administration'] = 'Administration';
$lang['User'] = 'User';
$lang['Form'] = 'Form';
$lang['Sub Form'] = 'Sub Form';
$lang['Inventory'] = 'Inventory';
$lang['Item Master'] = 'Item Master';
$lang['Price List'] = 'Price List';
$lang['Transfer Stock'] = 'Transfer Stock';
$lang['Adjustment Stock'] = 'Adjustment Stock';
$lang['Item Category'] = 'Item Category';
$lang['Sales'] = 'Sales';
$lang['Purchase'] = 'Purchase';
$lang['Business Partner'] = 'Business Partner';
$lang['Business Partner Master'] = 'Business Partner Master';
$lang['Business Partner Category'] = 'Business Partner Category';
$lang['Sales Order'] = 'Sales Order';
$lang['Delivery'] = 'Delivery';
$lang['Purchase Order'] = 'Purchase Order';
$lang['Reports'] = 'Reports';
$lang['Profit and Loss'] = 'Profit and Loss';
$lang['Sales Return'] = 'Sales Return';
$lang['Purchase Return'] = 'Purchase Return';
$lang['Sales Quotation'] = 'Sales Quotation';
$lang['Location'] = 'Location';
$lang['Goods Receipt PO'] = 'Goods Receipt PO';
$lang['Banking'] = 'Banking';
$lang['Wallet'] = 'Wallet';
$lang['Incoming Payment'] = 'Incoming Payment';
$lang['Outgoing Payment'] = 'Outgoing Payment';
$lang['Deposit and Withdrawal'] = 'Deposit and Withdrawal';
$lang['Inventory Audit Reports'] = 'Inventory Audit Reports';
$lang['Plan'] = 'Plan';
$lang['Tax Group'] = 'Tax Group';
$lang['Top Form'] = 'Top Form';
$lang['A/R Invoice'] = 'A/R Invoice';
$lang['A/P Invoice'] = 'A/P Invoice';
$lang['Table'] = 'Table';
$lang['A/P Credit Memo'] = 'A/P Credit Memo';
$lang['A/R Credit Memo'] = 'A/R Credit Memo';
$lang['Production'] = 'Production';
$lang['Bill Of Material'] = 'Bill Of Material';
$lang['Production Order'] = 'Production Order';
$lang['Issued For Production'] = 'Issued For Production';
$lang['Receipt From Production'] = 'Receipt From Production';
$lang['Dashboard'] = 'Dashboard';
$lang['Inventory Report'] = 'Inventory Report';
$lang['Sales Reports'] = 'Sales Reports';
$lang['Document Setting'] = 'Document Setting';
$lang['Cancel Document'] = 'Cancel Document';
$lang['Purchase Report'] = 'Purchase Report';
$lang['Payment Mutation Report'] = 'Payment Mutation Report';
$lang['Posting Periods'] = 'Posting Periods';
$lang['Open Item List'] = 'Open Item List';
$lang['Wallet Transfer'] = 'Wallet Transfer';
$lang['Revaluation'] = 'Revaluation';
$lang['Customer Aging Reports'] = 'Customer Aging Reports';
$lang['Vendor Aging Reports'] = 'Vendor Aging Reports';
$lang['Physical Inv. Document'] = 'Physical Inv. Document';
$lang['Inventory Posting'] = 'Inventory Posting';
$lang['Cost Roll Up'] = 'Cost Roll Up';
$lang['Cost Roll Over'] = 'Cost Roll Over';
$lang['Discount'] = 'Discount';
$lang['Import Data'] = 'Import Data';
$lang['Profile'] = 'Profile';
$lang['Journal'] = 'Journal';
$lang['Balance Sheet'] = 'Balance Sheet';
$lang['COA'] = 'COA';
$lang['COA Setting'] = 'COA Setting';
$lang['Finance'] = 'Finance';
$lang['A/R Down Payment'] = 'A/R Down Payment';
$lang['A/P Down Payment'] = 'A/P Down Payment';
$lang['Assets Capitalization'] = 'Assets Capitalization';
$lang['Assets Retirement'] = 'Assets Retirement';
$lang['Assets Transfer'] = 'Assets Transfer';
$lang['Manual Depreciation'] = 'Manual Depreciation';
$lang['Depreciation Run'] = 'Depreciation Run';
$lang['Asset Revaluation'] = 'Asset Revaluation';
$lang['Maintenance'] = 'Maintenance';
$lang['Work Order'] = 'Work Order';
$lang['Assets Master Data'] = 'Assets Master Data';
$lang['Assets Class'] = 'Assets Class';
$lang['Purchase Request'] = 'Purchase Request';
$lang['Document Group'] = 'Document Group';
$lang['Setting'] = 'Setting';
$lang['Group Cash Flow'] = 'Group Cash Flow';
$lang['Cash Flow Reports'] = 'Cash Flow Reports';
$lang['E-Faktur'] = 'E-Faktur';
$lang['User Defined Fields'] = 'User Defined Fields';
$lang['Reports Generator'] = 'Reports Generator';
$lang['Delivery Order'] = 'Delivery Order';
$lang['Movement Type'] = 'Movement Type';
$lang['Bank Master'] = 'Bank Master';
$lang['Blocking System'] = 'Blocking System';
$lang['New'] = 'New';
$lang['Duplicated'] = 'Duplicated';
$lang['Print'] = 'Print';
$lang['Send E-Mail'] = 'Send E-Mail';
$lang['Excel'] = 'Excel';
$lang['Word'] = 'Word';
$lang['Related Documents'] = 'Related Documents';
$lang['Data Change'] = 'Data Change';
$lang['Journal Entry'] = 'Journal Entry';
$lang['Previous'] = 'Previous';
$lang['Next'] = 'Next';
$lang['Refresh'] = 'Refresh';
$lang['Back'] = 'Back';
$lang['Toolbar type is not set'] = 'Toolbar type is not set';
$lang['Code'] = 'Code';
$lang['Name'] = 'Name';
$lang['City'] = 'City';
$lang['Warehouse List'] = 'Warehouse List';
$lang['BP. Name'] = 'BP. Name';
$lang['Date'] = 'Date';
$lang['Sales Return List'] = 'Sales Return List';
$lang['Reservation Category'] = 'Reservation Category';
$lang['Reservation'] = 'Reservation';
$lang['Master Activity'] = 'Master Activity';
$lang['Activity Rate'] = 'Activity Rate';
$lang['Routing'] = 'Routing';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';
$lang['XXXX'] = 'XXXX';