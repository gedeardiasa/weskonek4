<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); // This Prevents browsers from directly accessing this PHP file.

class Message{
        public function GetMessage($form,$type)
        {
			if($type=='successadd')
			{
				$message="You are successfully add ".$form." data";
			}
			else if($type=='erroradd')
			{
				$message="You are failed add ".$form." data";
			}
			else if($type=='successapply')
			{
				$message="You are successfully apply ".$form." data";
			}
			else if($type=='successedit')
			{
				$message="You are successfully edit ".$form." data";
			}
			else if($type=='erroredit')
			{
				$message="You are failed edit ".$form." data";
			}
			else if($type=='successdelete')
			{
				$message="You are successfully delete ".$form." data";
			}
			else if($type=='errordelete')
			{
				$message="You are failed delete ".$form." data";
			}
			else if($type=='successeditaccess')
			{
				$message="You are successfully edit ".$form." access data";
			}
			else if($type=='successeditpass')
			{
				$message="You are successfully edit ".$form." password data";
			}
			else if($type=='erroreditpass')
			{
				$message="Your old password is wrong";
			}
			else if($type=='erroreditpassconfirm')
			{
				$message="Your confirmation password is wrong";
			}
			else if($type=='erroreditpassconfirm')
			{
				$message="Your confirmation password is wrong";
			}
			else if($type=='errorduplicatecode')
			{
				$message="Your Code Already Exist";
			}
			else if($type=='nofile')
			{
				$message="Please Upload Your File";
			}
			else if($type=='emptybpcategory')
			{
				$message="Please Fill BP Category";
			}
			else if($type=='emptybpprice')
			{
				$message="Please Fill Price List";
			}
			else
			{
				$message=$type;
			}
			return $message;
		}
		
}