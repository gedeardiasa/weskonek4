<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authorization extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function insertactivity($session,$controller='',$function='',$activity='',$key='')
	{
		$now=date('Y-m-d H:i:s');
		$controller=str_replace("'","''",$controller);
		$function=str_replace("'","''",$function);
		$activity=str_replace("'","''",$activity);
		$key=str_replace("'","''",$key);
		
		$this->db->query("
		insert into museractivity (intUserID,vcUserID,vcUserName,vcController,vcFunction,vcActivity,vcKey,dtInsertTime)
		values
		('$session[IDPOS]','$session[UsernamePOS]','$session[NamePOS]','$controller','$function','$activity','$key','$now')
		");
	}
	function setdatabase($db)
	{
		$_SESSION[md5('db_pos_setting')]=md5($db);
	}
	function skiptutorial()
	{
		$this->db->query("update maccess set intSetup=1 where intUserID='$_SESSION[IDPOS]'");
	}
	function cektoken()
	{
		$now = date('Y-m-d H:i:s');
		$dbmaster=$this->load->database('master', TRUE);
		$qmaster=$dbmaster->query("select * from mclient where intDeleted=0");
		foreach($qmaster->result() as $qm)
		{
			$dbclient = $this->load->database($qm->vcSubDomain, TRUE);
			$q= $dbclient->query("delete from musertoken where DATE_ADD(dtInsertTime, INTERVAL 4 HOUR) < '$now'");
			
		}
	}
	function continuelogons()
	{
		$now=date('Y-m-d H:i:s');
		$iduser=$_SESSION['IDPOS'];
		$token=$_SESSION['tokenPOS'];
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
						
		$compname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$this->db->query("update musertoken set vcToken='$token', vcIP='$ipaddress', vcCompName='$compname',dtInsertTime='$now' where intUserID='$iduser'");
	}
	function ceklogin()
	{
		if(!isset($_SESSION['UsernamePOS']) or !isset($_SESSION['PasswordPOS']))
		{
			return false;
		}
		else
		{
			$user=$_SESSION['UsernamePOS'];
			$pass=$_SESSION['PasswordPOS'];
			if(!isset($_SESSION[md5('posroot')]))
			{
				$token=$_SESSION['tokenPOS'];
			}
			$dbmaster=$this->load->database('master', TRUE);
			$qmaster=$dbmaster->query("select * from madmin where vcUserID='$user' and vcPassword='$pass'");
			if($qmaster->num_rows()>0)
			{
				return true;
			}
			else
			{
					//echo $_SESSION[md5('db_pos_setting')];
					$db=$this->load->database('default', TRUE);
					$user=str_replace("'","''",$user);
					$pass=str_replace("'","''",$pass);
					$q=$this->db->query("select * from muser where vcUserID='$user' and vcPassword='$pass' and intDeleted=0");
					//echo "select * from muser where vcUserID='$user' and vcPassword='$pass'";
					if($q->num_rows()>0)
					{
						
						//cek token
						$qcektoken=$db->query("select * from musertoken where vcToken='$token'");
						if($qcektoken->num_rows()>0)
						{
							return true;
						}
						else
						{
							return false;
						}
						
					}else{
						
						return false;
					}
					return true;
					
			}
		}
	}
	function login($user,$pass)
	{
		$pass=md5(str_replace("'","''",$pass));
		$user=str_replace("'","''",$user);
		
		$db=$this->load->database('default', TRUE);
		
		$dbmaster=$this->load->database('master', TRUE);
		
		$qmasterceklogin=$dbmaster->query("select * from madmin where vcUserID='$user' and vcPassword='$pass'");
		if($qmasterceklogin->num_rows()>0)
		{
			$r=$qmasterceklogin->row();
					
			$_SESSION['IDPOS']=$r->intID;
			$_SESSION['UsernamePOS']=$r->vcUserID;
			$_SESSION['PasswordPOS']=$r->vcPassword;
			$_SESSION['NamePOS']=$r->vcName;
			$_SESSION['EmailPOS']=$r->vcEmail;
			$_SESSION['LangPOS']='en';
			$_SESSION['DatePOS']=$r->dtInsertTime;
			$_SESSION[md5('posroot')]=md5('posroot');
			unset($_SESSION[md5('db_pos_setting')]);
			return 1;
		}
		else
		{	
			$qmaster=$dbmaster->query("select a.*, b.vcSubDomain, b.vcDB from muser a LEFT JOIN mclient b on a.intClient=b.intID where vcUserID='$user' and b.intDeleted=0");
			if($qmaster->num_rows()>0)
			{
				$rmaster=$qmaster->row();
				
				$db=$this->load->database($rmaster->vcSubDomain, TRUE);
				
				$q=$db->query("select a.* from muser a where vcUserID='$user' and vcPassword='$pass' and intDeleted=0");
				if($q->num_rows()>0)
				{
					$r=$q->row();
					
					$_SESSION['IDPOS']=$r->intID;
					$_SESSION['UsernamePOS']=$r->vcUserID;
					$_SESSION['PasswordPOS']=$r->vcPassword;
					$_SESSION['NamePOS']=$r->vcName;
					$_SESSION['EmailPOS']=$r->vcEmail;
					$_SESSION['LangPOS']=$r->vcLang;
					$_SESSION['DatePOS']=$r->dtInsertTime;
					$_SESSION[md5('db_pos_setting')]=md5($rmaster->vcSubDomain);
					
					//cek token
					$length=15;
					$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$charactersLength = strlen($characters);
					$randomString = '';
					for ($i = 0; $i < $length; $i++) {
						$randomString .= $characters[rand(0, $charactersLength - 1)];
					}
					$_SESSION['tokenPOS']=$randomString;
					$qcektoken=$db->query("select * from musertoken where intUserID='$_SESSION[IDPOS]'");
					
					if($qcektoken->num_rows()==0)
					{
						//insert token
						$now=date('Y-m-d H:i:s');
						$ipaddress = '';
						if (getenv('HTTP_CLIENT_IP'))
							$ipaddress = getenv('HTTP_CLIENT_IP');
						else if(getenv('HTTP_X_FORWARDED_FOR'))
							$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
						else if(getenv('HTTP_X_FORWARDED'))
							$ipaddress = getenv('HTTP_X_FORWARDED');
						else if(getenv('HTTP_FORWARDED_FOR'))
							$ipaddress = getenv('HTTP_FORWARDED_FOR');
						else if(getenv('HTTP_FORWARDED'))
						   $ipaddress = getenv('HTTP_FORWARDED');
						else if(getenv('REMOTE_ADDR'))
							$ipaddress = getenv('REMOTE_ADDR');
						else
							$ipaddress = 'UNKNOWN';
						
						$compname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
						$db->query("insert into musertoken(intUserID, vcUserID, vcIP, vcCompName, vcToken, dtInsertTime) values
						('$_SESSION[IDPOS]','$_SESSION[UsernamePOS]','$ipaddress','$compname','$_SESSION[tokenPOS]','$now')");
						//access by session
						$q2=$db->query("select intItem from muser where intID='$_SESSION[IDPOS]'");
						$r2=$q2->row();
						$_SESSION['SHOWMODALITEMPOS'] = $r2->intItem;
		
						
						return 1;
					}else
					{
						return 2;
					}
				}
				else
				{
					//echo $rmaster->vcSubDomain;
					return 0;
				}
			}
			else
			{
				return 0;
			}
		
		}
	}
	function getLogonsInformation($id)
	{
		$user= $_SESSION['UsernamePOS'];
		$dbmaster=$this->load->database('master', TRUE);
		$qmaster=$dbmaster->query("select a.*, b.vcSubDomain, b.vcDB from muser a LEFT JOIN mclient b on a.intClient=b.intID where vcUserID='$user' and b.intDeleted=0");
		if($qmaster->num_rows()>0)
		{
			$rmaster=$qmaster->row();
			$db=$this->load->database($rmaster->vcSubDomain, TRUE);
		}

		$q=$db->query(" 
		select * from musertoken where intUserID='$id'
		");
		
		return $q->row();
		
	}
	function deleteToken($id)
	{
		$this->db->query(" 
		delete from musertoken where intUserID='$id'
		");
	}
	function GetForm($session)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		DISTINCT(d.`vcName`) AS vcFormName, d.`vcCode` AS vcFormCode, d.`intID` AS FormID, d.vcIcon, d.intID
		FROM maccess a LEFT JOIN muser b ON a.`intUserID`=b.`intID`
		LEFT JOIN (mform c LEFT JOIN mform d ON c.`intHeader`=d.`intID`) ON a.`intForm`=c.`intID`
		WHERE  c.`intDeleted`=0 AND b.intID='$session[IDPOS]' order by d.intID, c.intID asc
		");
		
		//echo $q;
		  return $q;
	}
	function GetNavigation($form)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.`vcName`, b.`vcName` AS vcHeader, b.vcIcon FROM mform a LEFT JOIN mform b ON a.`intHeader`=b.`intID`
		WHERE a.vcCode='$form'
		");
		
		if($q->num_rows()>0)
		{
			return $q->row();
		}
		else
		{
			$q=$this->db->query("SELECT a.`vcName`, a.`vcName` AS vcHeader, a.vcIcon FROM mtopform a
			WHERE a.vcCode='$form'
			");
			if($q->num_rows()>0)
			{
				return $q->row();
			}
			else
			{
				$dbmaster=$this->load->database('master', TRUE);
				$q = $dbmaster->query("select b.vcName as vcName, 'ZProgram' AS vcHeader, '<i class=\"fa fa-star\"></i>' as vcIcon  from mclient a LEFT JOIN mzprogram b on a.intID=b.intClient where a.intID='$_SESSION[IDPOS]' and b.vcCode='$form'");
				
				if($q->num_rows()>0)
				{
					return $q->row();
				}
				else
				{
					$q=$this->db->query("SELECT 'Welcome' as `vcName`, 'Administrator' AS vcHeader, '<i class=\"fa fa-gear\"></i>' as vcIcon 
					");
					return $q->row();
				}
			}
		}
	}
	function getcrudaccess($form)
	{
		$db=$this->load->database('default', TRUE);
		$intID=isset($_SESSION['IDPOS'])?$_SESSION['IDPOS']:''; // get the requested page
		$form=str_replace("'","''",$form);
		$q=$this->db->query("select a.* from maccess a left join mform b on a.intForm=b.intID
		where a.intUserID='$intID' and b.vcCode='$form'");
		
		
		
		if($q->num_rows()>0)
		{
			return $q->row();
		}
		else
		{
			$q=$this->db->query("select a.* from maccessz a 
			where a.intUserID='$intID' and a.vcZProgram='$form'");
			if($q->num_rows()>0)
			{
				return $q->row();
			}
		}
	}
	function cekform($form)
	{
		$db=$this->load->database('default', TRUE);
		$intID=isset($_SESSION['IDPOS'])?$_SESSION['IDPOS']:''; // get the requested page
		$form=str_replace("'","''",$form);
		$q=$this->db->query("select a.* from maccess a left join mform b on a.intForm=b.intID
		where a.intUserID='$intID' and b.vcCode='$form' and b.intDeleted=0");
		if($q->num_rows()==0)
		{
			// cek zprogram
			
			$q2   = $this->db->query("select a.* from maccessz a
					where a.intUserID='$intID' and a.vcZProgram='$form'");
			if($q2->num_rows()==0)
			{
				$data['link']=$this->config->base_url();
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
			else
			{
				
			}
		}
		else
		{
		}
	}
	function cektopform($form)
	{
		$db=$this->load->database('default', TRUE);
		$intID=isset($_SESSION['IDPOS'])?$_SESSION['IDPOS']:''; // get the requested page
		$form=str_replace("'","''",$form);
		$q=$this->db->query("select a.* from maccesstop a left join mtopform b on a.intForm=b.intID
		where a.intUserID='$intID' and b.vcCode='$form'");
		if($q->num_rows()==0)
		{
			
			$data['link']=$this->config->base_url();
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
		}
		else
		{
		}
	}
	function getformname($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mform where vcCode='$code'");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			$q2=$this->db->query("select vcName from mtopform where vcCode='$code'");
			if($q2->num_rows()>0)
			{
				$r2=$q2->row();
				return $r2->vcName;
			}
			else
			{
				return "";
			}
		}
	}
	function setmutationje()
	{
		// $db=$this->load->database('default', TRUE);
		// $q=$this->db->query("SELECT * FROM dMutationJE 
		// WHERE intValue<>0 and vcGLCode='71110199' ORDER BY vcGLCode, dtDate, dtInsertTime");
		
		// $lastvalue = 0;
		// foreach($q->result() as $r)
		// {
			// $id = $r->intID;
			// echo $r->vcGLCode."-".$r->intValue."<br>";
			// $this->db->query("update dMutationJE set intLastValue='$lastvalue' where intID='$id'");
			// $lastvalue = $lastvalue + $r->intValue;
		// }
		
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */