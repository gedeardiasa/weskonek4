<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_activity extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from mactivity a 
		where a.intDeleted=0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mactivity a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetNameByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mactivity where intID='$id'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetNameByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mactivity where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetCodeByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcCode from mactivity where vcName='$name'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return null;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mactivity where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mactivity where vcName='$name'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetUoMByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcUoM from mactivity where vcName='$name'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcUoM;
		}
		else
		{
			return '';
		}
	}
	function GetCostActivity($id)
	{
		$db=$this->load->database('default', TRUE);
		$month = date('n');
		$year = date('Y');
		$q=$this->db->query("select intCost from mactivityrate where intActivity='$id' and intYear='$year' and intMonth='$month'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intCost;
		}
		else
		{
			return 0;
		}
	}
	function getratebyactmthyear($activity,$year,$month)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mactivityrate where intActivity='$activity' and intYear='$year' and intMonth='$month'
		");
		$t = new stdClass();
		if($q->num_rows()>0)
		{
			$r=$q->row();
			$t->id = $r->intID;
			$t->cost = $r->intCost;
			return $t;
		}
		else
		{
			$t->id = 0;
			$t->cost = 0;
			return $t;
		}
	}
	function insertupdateplanrate($d){
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mactivityrate 
		where intActivity='$d[activity]' and 
		intYear='$d[year]' and 
		intMonth='$d[month]'
		");
		if($q->num_rows()>0){
			$r=$this->db->query("
			update mactivityrate set intCost='$d[cost]'
			where intActivity='$d[activity]' and 
			intYear='$d[year]' and 
			intMonth='$d[month]'
			");
		}else{
			$r=$this->db->query("insert into mactivityrate (intActivity,intYear,intMonth,intCost)
			values ('$d[activity]','$d[year]','$d[month]','$d[cost]')
			");
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mactivity (vcCode,vcName,vcUoM, vcGL)
		values ('$d[Code]','$d[Name]','$d[UoM]','$d[Gl]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mactivity';
		$his['doc']			= 'ACTIVITY';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$q=$this->db->query("update mactivity set vcCode='$d[Code]',vcName='$d[Name]',vcUoM='$d[UoM]',vcGL='$d[Gl]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mactivity set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */