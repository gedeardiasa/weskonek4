<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_adjustment extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAdjustment a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAdjustment a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAdjustment a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAdjustment a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM, c.vcDocNum, c.dtDate  from dAdjustment a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hAdjustment c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPlanByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		DISTINCT(d.`intID`) AS intPlan
		FROM hAdjustment a LEFT JOIN dAdjustment b ON a.`intID`=b.`intHID`
		LEFT JOIN mlocation c ON b.`intLocation`=c.`intID`
		LEFT JOIN mplan d ON d.`intID`=c.`intPlan`
		WHERE a.`vcDocNum`='$doc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return 0;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hAdjustment where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function deleteH($id)
	{
		$q=$this->db->query("delete from hAdjustment where intID='$id'
		");
		
	}
	function cancelall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hAdjustment set vcStatus='X' where intID='$id'
		");
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['Account']=str_replace("'","''",$d['Account']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$d['Mvt'] = isset($d['Mvt'])?$d['Mvt']:''; // get the requested page
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hAdjustment');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hAdjustment (vcDocNum,vcGLCode,dtDate,vcRef,vcType,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime,vcMvt)
		values ('$d[DocNum]','$d[Account]','$d[DocDate]','$d[RefNum]','$d[Type]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now','$d[Mvt]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeAD']=str_replace("'","''",$d['itemcodeAD']);
		$d['itemnameAD']=str_replace("'","''",$d['itemnameAD']);
		$d['uomAD']=str_replace("'","''",$d['uomAD']);
		$q=$this->db->query("insert into dAdjustment (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intQty, intCost)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeAD]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeAD]','$d[itemnameAD]','$d[qtyAD]','$d[costAD]')
		");
		if($q)
		{
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */