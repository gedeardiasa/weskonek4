<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_apcm extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hAPCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAPCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dAPCM a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hAPCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dAPCM a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dAPCM a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hAPCM c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPCM a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPCM a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetHeaderByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPCM a where a.intBP='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderOpenByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPCM a where a.intBP='$id' and a.vcStatus='O'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPlanByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		DISTINCT(d.`intID`) AS intPlan
		FROM hAPCM a LEFT JOIN dAPCM b ON a.`intID`=b.`intHID`
		LEFT JOIN mlocation c ON b.`intLocation`=c.`intID`
		LEFT JOIN mplan d ON d.`intID`=c.`intPlan`
		WHERE a.`vcDocNum`='$doc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return 0;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		$d['Account'] = isset($d['Account'])?$d['Account']:''; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//get payment term BP
		$this->load->model('m_bp', 'bp');
		$paymentterm = $this->bp->getPaymentTerm($d['BPCode']);
		$duedate = date('Y-m-d', strtotime($d['DocDate'] . ' +'.$paymentterm.' day'));
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hAPCM');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcBillToCity;
		$Country = $adrs->vcBillToCountry;
		$AddressH = $adrs->vcBillToAddress;
		
		$q=$this->db->query("insert into hAPCM (intBP,intService,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,dtDueDate, vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,intBalance,intAmmountTendered,intChange,vcPaymentNote,vcPaymentCode,vcRemarks,vcUser,dtInsertTime,vcCity,vcCountry,vcAddress ,vcGLCode )
		values ('$d[BPId]','$d[Service]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$duedate','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[DocTotal]','$d[AmmountTendered]','$d[Change]','$d[PaymentNote]','$d[PaymentCode]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH','$d[Account]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hAPCM set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dAPCM set vcStatus='C' where intHID='$id'
		");
	}
	function cancelall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hAPCM set vcStatus='X' where intID='$id'
		");
		$q2=$this->db->query("
		update dAPCM set vcStatus='X' where intHID='$id'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("
		
		update hAPCM set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeAPCM']=str_replace("'","''",$d['itemcodeAPCM']);
		$d['itemnameAPCM']=str_replace("'","''",$d['itemnameAPCM']);
		
		$d['uomAPCM']=str_replace("'","''",$d['uomAPCM']);
		$d['uominvAPCM']=str_replace("'","''",$d['uominvAPCM']);
		
		$q=$this->db->query("insert into dAPCM (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intCost, intLineCost, intLocation,vcLocation)
		values ('$d[intHID]','$d[idBaseRefAPCM]','$d[BaseRefAPCM]','$d[itemID]','$d[itemcodeAPCM]','$d[itemnameAPCM]','$d[qtyAPCM]','$d[qtyAPCM]','$d[uomAPCM]',
		'$d[uomtypeAPCM]','$d[qtyinvAPCM]','$d[qtyinvAPCM]','$d[uominvAPCM]','$d[priceAPCM]','$d[discperAPCM]','$d[discAPCM]','$d[priceafterAPCM]',
		'$d[linetotalAPCM]','$d[costAPCM]','$d[linecostAPCM]','$d[whsAPCM]','$d[whsNameAPCM]')
		");
		
		if($q)
		{
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeAPCM']=str_replace("'","''",$d['itemcodeAPCM']);
		$d['itemnameAPCM']=str_replace("'","''",$d['itemnameAPCM']);
		
		$d['uomAPCM']=str_replace("'","''",$d['uomAPCM']);
		$d['uominvAPCM']=str_replace("'","''",$d['uominvAPCM']);
		
		$q=$this->db->query("update dAPCM set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeAPCM]',
		vcItemName='$d[itemnameAPCM]',
		intQty='$d[qtyAPCM]',
		intOpenQty='$d[qtyAPCM]',
		vcUoM='$d[uomAPCM]',
		intUoMType='$d[uomtypeAPCM]',
		intQtyInv='$d[qtyinvAPCM]',
		intOpenQtyInv='$d[qtyinvAPCM]',
		vcUoMInv='$d[uominvAPCM]',
		intPrice='$d[priceAPCM]',
		intDiscPer='$d[discperAPCM]',
		intDisc='$d[discAPCM]',
		intPriceAfterDisc='$d[priceafterAPCM]',
		intLineTotal='$d[linetotalAPCM]',
		intLocation='$d[whsAPCM]',
		vcLocation='$d[whsNameAPCM]'
		where intID='$d[intID]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dAPCM where intID='$id'
		");
	}
	function cekclose($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPCM set intApplied=intApplied+$applied, intBalance=intBalance-$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hAPCM where intID='$id' and intBalance<=0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hAPCM set vcStatus='C' where intID='$id'
			");
			$this->db->query("update dAPCM set vcStatus='C' where intHID='$id'
			");
		}
	}
	function rollback($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPCM set intApplied=intApplied-$applied, intBalance=intBalance+$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hAPCM where intID='$id' and intBalance>0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hAPCM set vcStatus='O' where intID='$id'
			");
			$this->db->query("update dAPCM set vcStatus='O' where intHID='$id'
			");
		}
	}
	function cekclose2($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		$this->db->query("
		update dAPCM set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and intItem='$item'
		");// query update open qty APCM
		
		$this->db->query("
		update dAPCM set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dAPCM
		
		$cekdetailsq=$this->db->query("
		select intID from dAPCM where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dAPCM yang statusnya open
		{
			$this->db->query("
			update hAPCM set
			vcStatus='C'
			where intID='$id'
			");// maka update hAPCM status ke 'C' (Close)
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */