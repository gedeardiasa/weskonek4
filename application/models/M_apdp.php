<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_apdp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAPDP a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataByReport($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hAPDP a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where b.vcCode like '%$d[BPCode]%' and b.vcName like '%$d[BPName]%' and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderOpenByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPDP a where a.intBP='$id' and a.vcStatus='O'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPDP a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetDetailDataByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, c.`vcDocNum` AS APDocNum, c.intID as APID FROM dAPDP a
		LEFT JOIN hAPDP b ON a.intHID=b.`intID`
		LEFT JOIN hAP c ON a.`intBaseRef`=c.intID where b.vcDocNum='$doc'
		");
		
		return $q;
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hAPDP a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function createConfirm($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into dAPDP (intBaseRef,intHID,dtDate,intApplied,vcRemarks, vcUser,dtInsertTime)
		values ('$d[AP]','$d[APDP]','$d[date]','$d[Applied]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hAPDP');
		// end cek DocNum kembar
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into hAPDP (intWallet,intBP,vcBPCode,vcBPName,vcDocNum, dtDate,intDocTotal,intBalance,vcRemarks,dtInsertTime,vcUser)
		values ('$d[Wallet]','$d[BPId]','$d[BPCode]','$d[BPName]','$d[DocNum]','$d[DocDate]','$d[DocTotal]','$d[DocTotal]','$d[Remarks]','$now','$d[UserID]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function cancel($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPDP set vcStatus='X' where intID='$id'
		");
		
	}
	function cancelconfirm($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update dAPDP set vcStatus='X' where intID='$id'
		");
		
	}
	function cancelconfirmAPDP($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPDP set vcStatus='O', intBalance=intBalance+$val, intApplied=intApplied-$val where intID='$id'
		");
		
	}
	function cancelconfirmAP($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAP set vcStatus='O', intBalance=intBalance+$val, intApplied=intApplied-$val where intID='$id'
		");
		
	}
	function GetConfirmDetail($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum, b.intWallet, b.vcBPCode from dAPDP a 
		LEFT JOIN hAPDP b on a.intHID=b.intID
		where a.intID=".$id."
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r;
		}
		else
		{
			return null;
		}
	}
	function UpdateBalance($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPDP set intBalance=intBalance-$d[RetTotal] where vcDocNum='$d[DocNum]'
		");
		
	}
	function cekclose($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAPDP set intApplied=intApplied+$applied, intBalance=intBalance-$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hAPDP where intID='$id' and intBalance<=0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hAPDP set vcStatus='C' where intID='$id'
			");
			
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */