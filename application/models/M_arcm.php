<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_arcm extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hARCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hARCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dARCM a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hARCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dARCM a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hARCM a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dARCM a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,  c.intService from dARCM a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hARCM c on a.intHID=c.intID
		where a.intHID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARCM a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARCM a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetHeaderByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARCM a where a.intBP='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderOpenByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARCM a where a.intBP='$id' and a.vcStatus='O'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//get payment term BP
		$this->load->model('m_bp', 'bp');
		$paymentterm = $this->bp->getPaymentTerm($d['BPCode']);
		$duedate = date('Y-m-d', strtotime($d['DocDate'] . ' +'.$paymentterm.' day'));
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hARCM');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcBillToCity;
		$Country = $adrs->vcBillToCountry;
		$AddressH = $adrs->vcBillToAddress;
		
		$q=$this->db->query("insert into hARCM (intBP,intService,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate, dtDueDate, vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,intBalance,intAmmountTendered,intChange,vcPaymentNote,vcPaymentCode,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[BPId]','$d[Service]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$duedate','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[DocTotal]','$d[AmmountTendered]','$d[Change]','$d[PaymentNote]','$d[PaymentCode]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hARCM set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dARCM set vcStatus='C' where intHID='$id'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("
		
		update hARCM set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeARCM']=str_replace("'","''",$d['itemcodeARCM']);
		$d['itemnameARCM']=str_replace("'","''",$d['itemnameARCM']);
		
		$d['uomARCM']=str_replace("'","''",$d['uomARCM']);
		$d['uominvARCM']=str_replace("'","''",$d['uominvARCM']);
		
		$q=$this->db->query("insert into dARCM (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intCost, intLineCost, intLocation,vcLocation)
		values ('$d[intHID]','$d[idBaseRefARCM]','$d[BaseRefARCM]','$d[itemID]','$d[itemcodeARCM]','$d[itemnameARCM]','$d[qtyARCM]','$d[qtyARCM]','$d[uomARCM]',
		'$d[uomtypeARCM]','$d[qtyinvARCM]','$d[qtyinvARCM]','$d[uominvARCM]','$d[priceARCM]','$d[discperARCM]','$d[discARCM]','$d[priceafterARCM]',
		'$d[linetotalARCM]','$d[costARCM]','$d[linecostARCM]','$d[whsARCM]','$d[whsNameARCM]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeARCM']=str_replace("'","''",$d['itemcodeARCM']);
		$d['itemnameARCM']=str_replace("'","''",$d['itemnameARCM']);
		
		$d['uomARCM']=str_replace("'","''",$d['uomARCM']);
		$d['uominvARCM']=str_replace("'","''",$d['uominvARCM']);
		
		$q=$this->db->query("update dARCM set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeARCM]',
		vcItemName='$d[itemnameARCM]',
		intQty='$d[qtyARCM]',
		intOpenQty='$d[qtyARCM]',
		vcUoM='$d[uomARCM]',
		intUoMType='$d[uomtypeARCM]',
		intQtyInv='$d[qtyinvARCM]',
		intOpenQtyInv='$d[qtyinvARCM]',
		vcUoMInv='$d[uominvARCM]',
		intPrice='$d[priceARCM]',
		intDiscPer='$d[discperARCM]',
		intDisc='$d[discARCM]',
		intPriceAfterDisc='$d[priceafterARCM]',
		intLineTotal='$d[linetotalARCM]',
		intLocation='$d[whsARCM]',
		vcLocation='$d[whsNameARCM]'
		where intID='$d[intID]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dARCM where intID='$id'
		");
	}
	function cekclose($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARCM set intApplied=intApplied+$applied, intBalance=intBalance-$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hARCM where intID='$id' and intBalance<=0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hARCM set vcStatus='C' where intID='$id'
			");
			$this->db->query("update dARCM set vcStatus='C' where intHID='$id'
			");
		}
	}
	function rollback($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARCM set intApplied=intApplied-$applied, intBalance=intBalance+$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hARCM where intID='$id' and intBalance>0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hARCM set vcStatus='O' where intID='$id'
			");
			$this->db->query("update dARCM set vcStatus='O' where intHID='$id'
			");
		}
	}
	function cekclose2($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		$this->db->query("
		update dARCM set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and intItem='$item'
		");// query update open qty ARCM
		
		
		$this->db->query("
		update dARCM set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dARCM
		
		$cekdetailsq=$this->db->query("
		select intID from dARCM where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dARCM yang statusnya open
		{
			$this->db->query("
			update hARCM set
			vcStatus='C'
			where intID='$id'
			");// maka update dARCM status ke 'C' (Close)
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */