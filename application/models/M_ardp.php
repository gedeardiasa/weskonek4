<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_ardp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hARDP a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataByReport($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hARDP a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where b.vcCode like '%$d[BPCode]%' and b.vcName like '%$d[BPName]%' and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDataByBPCode($bp)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hARDP where vcBPCode='$bp' and intBalance>0 and vcStatus='O'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GeteBalanceByBPCode($bp)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT SUM(intBalance) AS Balance FROM hARDP WHERE vcBPCode='$bp' AND intBalance>0 and vcStatus='O'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->Balance;
		}
		else
		{
			return 0;
		}
	}
	function GetHeaderOpenByBPID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARDP a where a.intBP='$id' and a.vcStatus='O'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARDP a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetDetailDataByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, c.`vcDocNum` AS ARDocnum, c.intID as ARID FROM dARDP a
		LEFT JOIN hARDP b ON a.intHID=b.`intID`
		LEFT JOIN hAR c ON a.`intBaseRef`=c.intID where b.vcDocNum='$doc'
		");
		
		return $q;
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hARDP a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function createConfirm($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into dARDP (intBaseRef,intHID,dtDate,intApplied,vcRemarks, vcUser,dtInsertTime)
		values ('$d[AR]','$d[ARDP]','$d[date]','$d[Applied]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hARDP');
		// end cek DocNum kembar
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into hARDP (intWallet,intBP,vcBPCode,vcBPName,vcDocNum, dtDate,intDocTotal,intBalance,vcRemarks,dtInsertTime,vcUser)
		values ('$d[Wallet]','$d[BPId]','$d[BPCode]','$d[BPName]','$d[DocNum]','$d[DocDate]','$d[DocTotal]','$d[DocTotal]','$d[Remarks]','$now','$d[UserID]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function cancel($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARDP set vcStatus='X' where intID='$id'
		");
		
	}
	function cancelconfirm($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update dARDP set vcStatus='X' where intID='$id'
		");
		
	}
	function cancelconfirmARDP($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARDP set vcStatus='O', intBalance=intBalance+$val, intApplied=intApplied-$val where intID='$id'
		");
		
	}
	function cancelconfirmAR($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hAR set vcStatus='O', intBalance=intBalance+$val, intApplied=intApplied-$val where intID='$id'
		");
		
	}
	function GetConfirmDetail($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum, b.intWallet, b.vcBPCode from dARDP a 
		LEFT JOIN hARDP b on a.intHID=b.intID
		where a.intID=".$id."
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r;
		}
		else
		{
			return null;
		}
	}
	function UpdateBalance($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARDP set intBalance=intBalance-$d[RetTotal] where vcDocNum='$d[DocNum]'
		");
		
	}
	function cekclose($id,$applied)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hARDP set intApplied=intApplied+$applied, intBalance=intBalance-$applied
		where intID='$id'
		");
		
		$cek=$this->db->query("select intID from hARDP where intID='$id' and intBalance<=0
		");
		if($cek->num_rows()>0)
		{
			$this->db->query("update hARDP set vcStatus='C' where intID='$id'
			");
			
		}
	}
	
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */