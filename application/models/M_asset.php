<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asset extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mitem';
		$his['doc']			= 'ASSET';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mitem set intAssetClass='$d[AssetClass]'
				where intID='$d[id]'");
		
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function cekMinusNetBook($item,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select * from mitem where intID=$item and intNetBook>=$val
		");
		
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */