<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asset_class extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from massetclass a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from massetclass a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from massetclass where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->intID;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountAPCByItemCode($code) // get account APC
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcGLAPC 
		FROM massetclass a LEFT JOIN mitem b ON a.intID=b.intAsset
		WHERE b.vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcGLAPC;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountDepByItemCode($code) // get account Depreciation
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcGLDep 
		FROM massetclass a LEFT JOIN mitem b ON a.intID=b.intAsset
		WHERE b.vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcGLDep;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountRevByItemCode($code) // get account revaluation
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcGLReval 
		FROM massetclass a LEFT JOIN mitem b ON a.intID=b.intAsset
		WHERE b.vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcGLReval;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into massetclass (vcCode,vcName,dtInsertTime,vcGLAPC,vcGLDep,vcGLReval)
		values ('$d[Code]','$d[Name]','$now','$d[vcGLAPC]','$d[vcGLDep]','$d[vcGLReval]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'massetclass';
		$his['doc']			= 'ASSETCLASS';
		$his['key']			= "intID='$d[id]'";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$q=$this->db->query("update massetclass set vcName='$d[Name]', vcCode='$d[Code]', vcGLAPC='$d[vcGLAPC]', vcGLDep='$d[vcGLDep]', vcGLReval='$d[vcGLReval]'  where intID='$d[id]'");
		
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update massetclass set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */