<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bank extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mbank where intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Branch']=str_replace("'","''",$d['Branch']);
		$d['Code']=str_replace(" ","",$d['Code']);
		
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Branch']=str_replace('"','',$d['Branch']);
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mbank (vcName,vcCode, vcBranch)
		values ('$d[Name]','$d[Code]','$d[Branch]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Branch']=str_replace("'","''",$d['Branch']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Branch']=str_replace('"','',$d['Branch']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mbank';
		$his['doc']			= 'BANK';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mbank set vcName='$d[Name]', vcCode='$d[Code]', vcBranch='$d[Branch]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mbank set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */