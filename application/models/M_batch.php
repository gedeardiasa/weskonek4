<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_batch extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function BatchProcessingGR($type,$item,$detail,$idmutasi,$qty,$whs,$batch)
	{
		for($i=0;$i<count($batch);$i++)
		{
			
			if($item==$batch[$i][0])
			{
				if($type=='IT'){// jika IT maka qty ambil dari detail batch karena khsus IT GR bs lbh dari 2 item batch
					$qty = $batch[$i][3];
				}else{
					$qty = $qty;
				}
				$cekbatch = $batch[$i][2];
				$cekex=$this->db->query("
				select * from mbatch where vcDocNum='$cekbatch' and intLocation='$whs' and intItem='$item'
				");
				
				if($cekex->num_rows()>0)
				{
					$cekexresult = 0;
				}
				else
				{
					$cekexresult = 1;
				}
				if($batch[$i][2]=='' or $cekexresult==1) // jika batch ksong / batch yg di input tidak ada dalam system maka buat batch baru
				{
					if($type=='IT'){
						$btremarks = '';
					}else{
						$btremarks = $batch[$i][3];
					}
					if($type=='IT' and $cekexresult==1){// jika IT dan batch number di isi manual (kemungkinan batch number disamakan dengan gudang asal)
						$batch[$i][2] = $this->CreateBatch($item,$whs,$qty,$btremarks,$batch[$i][2]);// parameter ke 5 khsus untuk buat batch dengan isi nomer manual
					}else{
						$batch[$i][2] = $this->CreateBatch($item,$whs,$qty,$btremarks);
					}
				}
				else
				{
					$kl['DocNum'] = $batch[$i][2];
					$kl['Qty'] = $qty;
					$this->UpdateStock($kl,$whs);
					$batch[$i][2] = $this->GetIdByDocNumLoc($batch[$i][2],$whs);
				}
				$this->InsertD($type,$detail,$batch[$i][2],$qty); // create batch detail
				$this->addMutation($idmutasi,$batch[$i][2],$qty); // create batch mutation
			}
		}
	}
	function BatchProcessingGI($type,$item,$detail,$idmutasi,$batch,$loc=0)
	{
		for($i=0;$i<count($batch);$i++)
		{		
			if($item==$batch[$i][0])
			{
				if($type=='IT'){
					$kl['DocNum'] 	= $batch[$i][1];
					$kl['QtyPlus'] 	= $batch[$i][3];
					$kl['Qty'] 		= $batch[$i][3]*-1;
				}else{
					$kl['DocNum'] 	= $this->GetDocnumByID($batch[$i][2]);
					$kl['QtyPlus'] 	= $batch[$i][1];
					$kl['Qty'] 		= $batch[$i][1]*-1;
				}
				
				$this->UpdateStock($kl,$loc);
				
				if($type=='IT'){
					$this->InsertD($type,$detail,$this->GetIdByDocNumLoc($batch[$i][2],$loc),$kl['QtyPlus']); // create batch detail
					$this->addMutation($idmutasi,$this->GetIdByDocNumLoc($batch[$i][2],$loc),$kl['Qty']); // create batch mutation
				}else{
					$this->InsertD($type,$detail,$batch[$i][2],$kl['QtyPlus']); // create batch detail
					$this->addMutation($idmutasi,$batch[$i][2],$kl['Qty']); // create batch mutation
				}	
			}
		}
	}
	function BatchCancelGR($type,$lb,$ld,$idmutasi,$loc=0)
	{
		$kl['DocNum'] = $this->GetDocnumByID($lb->intBatch);
		$kl['Qty'] = $lb->intQty*-1;
		$this->UpdateStock($kl,$loc);

		$this->InsertD($type,$ld->intID,$lb->intBatch,$lb->intQty); // create batch detail
		$this->addMutation($idmutasi,$lb->intBatch,$kl['Qty']); // create batch mutation
	}
	function BatchCancelGI($type,$lb,$ld,$idmutasi,$loc=0)
	{
		$kl['DocNum'] = $this->GetDocnumByID($lb->intBatch);
		$kl['Qty'] = $lb->intQty;
		$this->UpdateStock($kl,$loc);

		$this->InsertD($type,$ld->intID,$lb->intBatch,$kl['Qty']); // create batch detail
		$this->addMutation($idmutasi,$lb->intBatch,$kl['Qty']); // create batch mutation
	}
	function CreateBatch($item,$loc,$qty,$remarks='',$docnum='')
	{
		$db=$this->load->database('default', TRUE);
		$this->load->model('m_docnum', 'docnum');
		if($docnum==''){
			$d['DocNum'] = $this->docnum->GetLastDocNum('mbatch');
		}else{
			$d['DocNum'] = $docnum;
		}
		
		$remarks=str_replace("'","''",$remarks);
		
		$now=date('Y-m-d H:i:s');
		$q=$this->db->query("insert into mbatch (vcDocNum,intLocation,intItem,intStock,dtInsertTime,vcRemarks) values ('$d[DocNum]','$loc','$item','$qty','$now','$remarks')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}		
	}
	function InsertD($doc,$hid,$batch,$qty)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("insert into dBatch (vcDoc,intHID,intBatch,intQty) values ('$doc','$hid','$batch','$qty')
		");
	}
	function addMutation($hid,$batch,$qty)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("insert into dMutationBatch (intHID,intBatch,intQty) values ('$hid','$batch','$qty')
		");
	}
	function CekBatch($doc,$item,$whs)
	{
		$db=$this->load->database('default', TRUE);
		
		// $this->load->model('m_item', 'item');
		// $iditem = $this->item->GetIDByName($item);
		
		$q=$this->db->query("
		select intID from mbatch where vcDocNum='$doc' and intItem='$item' and intLocation='$whs'
		");
		
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetBatchByintHID($intHID,$doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select * from dBatch where vcDoc='$doc' and intHID='$intHID'
		");
		return $q->row();
	}
	function GetBatchByintHID2($intHID,$doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select * from dBatch where vcDoc='$doc' and intHID='$intHID'
		");
		return $q;
	}
	function GetintHIDDoc($doc,$docnum,$itm)
	{
		$db=$this->load->database('default', TRUE);
		
		if($doc=='AD')
		{
			$doc='Adjustment';
		}
		$q=$this->db->query("
		select a.intID from d$doc a left join h$doc b on a.intHID=b.intID
		where b.vcDocNum='$docnum' and a.intItem='$itm'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function isnewbatchnumber($doc)
	{
		$cekex=$this->db->query("
		select * from mbatch where vcDocNum='$doc'
		");
				
		if($cekex->num_rows()>0)
		{
			$cekexresult = 0;
		}
		else
		{
			$cekexresult = 1;
		}
		return $cekexresult;
	}
	function GetItemByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intItem from mbatch where vcDocNum='$doc'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intItem;
		}
		else
		{
			return 0;
		}
	}
	function GetIdByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intID from mbatch where vcDocNum='$doc'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}else{
			return 0;
		}
		
	}
	function GetIdByDocNumLoc($doc,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intID from mbatch where vcDocNum='$doc' and intLocation='$loc'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}else{
			return 0;
		}
		
	}
	function GetDocnumByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select vcDocNum from mbatch where intID='$id'
		");
		$r=$q->row();
		return $r->vcDocNum;
	}
	function GetRemarksByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select vcRemarks from mbatch where intID='$id'
		");
		$r=$q->row();
		return $r->vcRemarks;
	}
	function GetDateByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select dtInsertTime from mbatch where intID='$id'
		");
		$r=$q->row();
		return $r->dtInsertTime;
	}
	function GetBatchMutationByIdMutation($idmutation)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		select
		a.*, b.vcDocNum, b.dtInsertTime, b.vcRemarks
		from dMutationBatch a
		left join mbatch b on a.intBatch=b.intID
		where a.intHID='$idmutation'
		");
		
		return $q;
	}
	function GetBatchByItemAndLoc($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		select * from mbatch where intItem='$item' and intLocation='$loc' and intStock>0
		");
		
		return $q;
		
	}
	function GetMutByDocTypeAndId($d)
	{
		$db=$this->load->database('default', TRUE);
	
		
		$q=$this->db->query("
		SELECT c.*, d.`vcDocNum` AS BatchNumber FROM $d[table] a
		LEFT JOIN dMutation b ON a.`vcDocNum`=b.`vcDoc` AND b.`vcType`='$d[type]'
		LEFT JOIN dMutationBatch c ON c.`intHID`=b.intID
		LEFT JOIN mbatch d ON c.`intBatch`=d.`intID`
		WHERE a.intID='$d[intID]' and d.intItem='$d[item]'
		");
		
		return $q;
		
	}
	function UpdateStock($d,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update mbatch set intStock=intStock + $d[Qty] where vcDocNum='$d[DocNum]' and intLocation='$loc'
		");
		
	}
	function getbatchstockbyid($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intStock from mbatch where intID='$id'
		");
		$r=$q->row();
		return $r->intStock;
	}
	function getbatchstockbydocnumlocation($docnum,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intStock from mbatch where vcDocNum='$docnum' and intLocation='$loc'
		");
		$r=$q->row();
		return $r->intStock;
	}
	
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */