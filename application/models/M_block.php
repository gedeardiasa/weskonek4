<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_block extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		select a.*,
		case when intActive=1 then 'Active' else 'Not Active' end as vcStatus
		from mblock a 
		where a.intDeleted=0
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mblock a
		where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function cekblock($id,$doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mblock a LEFT JOIN mdoc b on a.intDoc=b.intID where b.vcCode='$doc' and a.intActive=1 and a.intDeleted=0
		");
		//echo "select a.* from mblock a LEFT JOIN mdoc b on a.intDoc=b.intID where b.vcCode='$doc'";
		$ret = '';
		foreach($q->result() as $d)
		{
			$query = $d->vcQuery;
			$query = str_replace("{id}","'".$id."'",$query);
			//echo $query." | ";
			$ex = $this->db->query($query);
			if($ex->num_rows()>0)
			{
				$ret = $ret."".$d->vcAlert;
			}
		}
		return $ret;
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Query']=str_replace("'","''",$d['Query']);
		$d['Alert']=str_replace("'","''",$d['Alert']);
		
		if(stripos($d['Query'], 'insert') !== false)
		{
			$d['Query'] = 'Dont Use Insertt Query';
		}
		if(stripos($d['Query'], 'update') !== false)
		{
			$d['Query'] = 'Dont Use Updatee Query';
		}
		if(stripos($d['Query'], 'delete') !== false)
		{
			$d['Query'] = 'Dont Use Deletee Query';
		}
		
		$q=$this->db->query("
		update mblock set
		vcName='$d[Name]',
		intDoc='$d[Doc]',
		vcQuery='$d[Query]',
		vcAlert='$d[Alert]',
		intActive='$d[Active]'
		where intID='$d[id]'
		");
		
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Query']=str_replace("'","''",$d['Query']);
		$d['Alert']=str_replace("'","''",$d['Alert']);
		
		if(stripos($d['Query'], 'insert') !== false)
		{
			$d['Query'] = 'Dont Use Insertt Query';
		}
		if(stripos($d['Query'], 'update') !== false)
		{
			$d['Query'] = 'Dont Use Updatee Query';
		}
		if(stripos($d['Query'], 'delete') !== false)
		{
			$d['Query'] = 'Dont Use Deletee Query';
		}
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("insert into mblock (intDoc,vcName,vcQuery,vcAlert)
		select '$d[Doc]','$d[Name]','$d[Query]','$d[Alert]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mblock set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */