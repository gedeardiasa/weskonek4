<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bom extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	function CekBom($iduser,$item, $loc=0)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hBOM a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.intItem='$item' and a.intLocation='$loc'
		order by a.intID desc
		");
		
		
		if($q->num_rows()>0)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetDataByItemAndLoc($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hBOM a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		where a.intItem='$item' and a.intLocation='$loc'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hBOM a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessPROOnly($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hBOM a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.vcRef<>'999'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dBOM a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailCRUByBomID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dCRU a 
		LEFT JOIN hCRU b on a.intHID=b.intID
		where b.intBOM='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailCRUByBomIDandItem($id,$item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dCRU a 
		LEFT JOIN hCRU b on a.intHID=b.intID
		where b.intBOM='$id' and a.intItem='$item'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByLoc($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByLocJustCost($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intLocation='$loc' and vcRef='999'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function CekHeaderByItemAndLocJustCost($itm,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intLocation='$loc' and vcRef='999' and intItem='$itm'
		");
		
		if($q->num_rows()>0)
		{
		  return 1;
		}
		else
		{
			return null;
		}
	}
	function GetHeaderByItemAndLocJustCost($itm,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intLocation='$loc' and vcRef='999' and intItem='$itm'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetAllCRUByLoc($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum, b.vcRef, b.intLocation, b.vcLocation, b.vcItemCode, b.vcItemName, b.intQty, b.intID as BomID, 
		b.intQty as BOMQty, b.vcRemarks as BOMRemarks from hCRU a 
		LEFT JOIN hBOM b on a.intBOM=b.intID
		where b.intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetCostBomCostingByItemCode($item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intCost, intQty from hBOM where vcRef='999' and vcItemCode='$item'
		");
		
		if($q->num_rows()>0)
		{
			$r = $q->row();
			$hasil = $r->intCost/$r->intQty;
			return $hasil;
		}
		else
		{
			return 0;
		}
	}
	function cekrev($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hBOM where intItem='$d[FGItemID]' and vcRef='$d[Rev]'
		");
		
		if($q->num_rows()>0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	function insertCRU($d)
	{
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		$q=$this->db->query("insert into hCRU (intBOM,dtDate,intCostBefore,vcUser,dtInsertTime)
		values ('$d[intBOM]','$d[dtDate]','$d[intCostBefore]','$d[UserID]','$now')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertDCRU($d)
	{
		$q=$this->db->query("insert into dCRU (intHID,intItem,vcItemCode,vcItemName,intQty,vcUoM,intCost)
		values ('$d[intHID]','$d[intItem]','$d[vcItemCode]','$d[vcItemName]','$d[intQty]','$d[vcUoM]','$d[intCost]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['FGItem']=str_replace("'","''",$d['FGItem']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hBOM');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hBOM (vcDocNum,vcRef,dtDate,intLocation,vcLocation,intItem,vcItemCode,vcItemName,intQty,intCost,
		vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[Rev]','$d[DocDate]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),(select intID from mitem where vcName='$d[FGItem]'),(select vcCode from mitem where vcName='$d[FGItem]'),'$d[FGItem]',
		'$d[FGQty]','$d[FGCost]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeBOM']=str_replace("'","''",$d['itemcodeBOM']);
		$d['itemnameBOM']=str_replace("'","''",$d['itemnameBOM']);
		$d['uomBOM']=str_replace("'","''",$d['uomBOM']);
		$q=$this->db->query("insert into dBOM (intHID,intItem,vcItemCode,vcItemName,intQty,vcUoM,vcSourceCost,intCost,intAutoIssue)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeBOM]'),
		'$d[itemcodeBOM]','$d[itemnameBOM]','$d[qtyBOM]','$d[uomBOM]','$d[sourceBOM]','$d[costBOM]','$d[autoBOM]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeBOM']=str_replace("'","''",$d['itemcodeBOM']);
		$d['itemnameBOM']=str_replace("'","''",$d['itemnameBOM']);
		$d['uomBOM']=str_replace("'","''",$d['uomBOM']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dBOM';
		$his['doc']			= 'BOM';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dBOM where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodeBOM'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$q=$this->db->query("update dBOM set
		intItem=(select intID from mitem where vcCode='$d[itemcodeBOM]'),
		vcItemCode='$d[itemcodeBOM]',
		vcItemName='$d[itemnameBOM]',
		intQty='$d[qtyBOM]',
		vcUoM='$d[uomBOM]',
		vcSourceCost='$d[sourceBOM]',
		intCost='$d[costBOM]',
		intAutoIssue='$d[autoBOM]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hBOM';
		$his['doc']			= 'BOM';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hBOM set
		vcRef='$d[Rev]',
		intLocation='$d[Whs]',
		vcLocation=(select vcName from mlocation where intID='$d[Whs]'),
		intQty='$d[FGQty]',
		vcRemarks='$d[Remarks]'
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hBOM set intCost='$cost' where intID='$intID'");
	}
	function updatecostCRU($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hCRU set intCost='$cost' where intID='$intID'");
	}
	function emptyCRU()
	{
		$this->db->query("delete from dCRU");
		$this->db->query("delete from hCRU");
		
		$this->db->query("ALTER TABLE dCRU AUTO_INCREMENT = 1");
		$this->db->query("ALTER TABLE hCRU AUTO_INCREMENT = 1");
		
	}
	function delCRU($id)
	{
		$this->db->query("delete from dCRU where intHID='$id'");
		$this->db->query("delete from hCRU where intID='$id'");
		
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dBOM where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */