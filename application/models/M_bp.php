<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetLastCode($type)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
			SELECT MAX(RIGHT(vcCode,8))+1 as id FROM mbp
			where LEFT(vcCode,1)='$type'
		");
		
		$r=$q->row();
		if($r->id!=null)
		{
			$paddedNum = sprintf("%08d", $r->id);
			$result = $type."".$paddedNum;
		}
		else
		{
			$result=$type."00000001";
		}
		
		return $result;
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mbp a LEFT JOIN mbpcategory b on a.intCategory=b.intID where a.intDeleted=0 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getBPCode($BPName)
	{
		$db=$this->load->database('default', TRUE);
		$BPName=str_replace("'","''",$BPName);
		$q=$this->db->query("select vcCode as data from mbp where vcName='$BPName'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function GetAddressByCode($BPCode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcBillToAddress,vcBillToCity,vcBillToCountry,vcShipToAddress,vcShipToCity,vcShipToCountry from mbp where vcCode='$BPCode'
		");
		return $q->row();
	}
	function GetPaymentByBPId($ID)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode,b.vcName from mbppayment a 
		LEFT JOIN mbank b on a.intBank=b.intID
		where a.intBP='$ID'");
		return $q;
	}
	function getBPName($BPCode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName as data from mbp where vcCode='$BPCode'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function getBPTax($BPCode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT b.`intRate` as data FROM mbp a
		LEFT JOIN mtax b ON a.`intTax`=b.`intID`
		WHERE a.`vcCode`='$BPCode'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return 0;
		}
	}
	function getPaymentTerm($BPCode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intPaymentTerm as data from mbp where vcCode='$BPCode'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return 0;
		}
	}
	function getIDByCode($BPCode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID as data from mbp where vcCode='$BPCode'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function getIDByName($BPName)
	{
		$db=$this->load->database('default', TRUE);
		$BPName=str_replace("'","''",$BPName);
		$q=$this->db->query("select intID as data from mbp where vcName='$BPName'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function getNameByID($BPID)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName as data from mbp where intID='$BPID'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function GetBalance($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT SUM(a.intvalue) as intBalance, b.vcBPCode FROM (
	SELECT 
	b.`vcARAccount` AS Account, c.`intValue`, d.`vcDocNum`, d.`vcRefType`, d.`vcRef`, c.`vcGLCodeX` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcARAccount`=c.`vcGLCode`
	WHERE a.`vcCode`='$code'
	UNION ALL
	SELECT 
	b.`vcARAccount` AS Account, c.`intValue`*-1 AS intValue, d.`vcDocNum` , d.`vcRefType`, d.`vcRef`, c.`vcGLCode` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcARAccount`=c.`vcGLCodeX`
	WHERE a.`vcCode`='$code'
	
	UNION ALL
	SELECT 
	b.`vcAPAccount` AS Account, c.`intValue`, d.`vcDocNum`, d.`vcRefType`, d.`vcRef`, c.`vcGLCodeX` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcAPAccount`=c.`vcGLCode`
	WHERE a.`vcCode`='$code'
	UNION ALL
	SELECT 
	b.`vcAPAccount` AS Account, c.`intValue`*-1 AS intValue, d.`vcDocNum` , d.`vcRefType`, d.`vcRef` , c.`vcGLCode` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcAPAccount`=c.`vcGLCodeX`
	WHERE a.`vcCode`='$code'
) a
LEFT JOIN
(
	SELECT vcDocNum,vcBPCode, 'AR' AS TYPE FROM hAR WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'ARCM' AS TYPE FROM hARCM WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'ARDP' AS TYPE FROM hARDP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'AP' AS TYPE FROM hAP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'APCM' AS TYPE FROM hAPCM WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'APDP' AS TYPE FROM hAPDP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'INPAY' AS TYPE FROM hINPAY WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'OUTPAY' AS TYPE FROM hOUTPAY WHERE vcBPCode='$code'
) b ON a.vcRef = b.vcDocNum AND a.vcRefType = b.Type
WHERE b.vcBPCode= '$code'
GROUP BY b.vcBPCode
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intBalance;
		}
		else
		{
			return 0;
		}
	}
	function GetBalanceDetail($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, b.vcBPCode FROM (
	SELECT 
	b.`vcARAccount` AS Account, c.`intValue`, d.`vcDocNum`, d.`vcRefType`, d.`vcRef`, c.`vcGLCodeX` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcARAccount`=c.`vcGLCode`
	WHERE a.`vcCode`='$code'
	UNION ALL
	SELECT 
	b.`vcARAccount` AS Account, c.`intValue`*-1 AS intValue, d.`vcDocNum` , d.`vcRefType`, d.`vcRef`, c.`vcGLCode` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcARAccount`=c.`vcGLCodeX`
	WHERE a.`vcCode`='$code'
	
	UNION ALL
	SELECT 
	b.`vcAPAccount` AS Account, c.`intValue`, d.`vcDocNum`, d.`vcRefType`, d.`vcRef`, c.`vcGLCodeX` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcAPAccount`=c.`vcGLCode`
	WHERE a.`vcCode`='$code'
	UNION ALL
	SELECT 
	b.`vcAPAccount` AS Account, c.`intValue`*-1 AS intValue, d.`vcDocNum` , d.`vcRefType`, d.`vcRef` , c.`vcGLCode` AS OffsetAccount
	FROM mbp a
	LEFT JOIN mbpcategory b ON a.`intCategory`=b.intID
	LEFT JOIN (dJE c
	LEFT JOIN hJE d ON c.`intHID`=d.`intID`
	) ON b.`vcAPAccount`=c.`vcGLCodeX`
	WHERE a.`vcCode`='$code'
) a
LEFT JOIN
(
	SELECT vcDocNum,vcBPCode, 'AR' AS TYPE FROM hAR WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'ARCM' AS TYPE FROM hARCM WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'ARDP' AS TYPE FROM hARDP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'AP' AS TYPE FROM hAP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'APCM' AS TYPE FROM hAPCM WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'APDP' AS TYPE FROM hAPDP WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'INPAY' AS TYPE FROM hINPAY WHERE vcBPCode='$code'
	UNION ALL SELECT vcDocNum,vcBPCode, 'OUTPAY' AS TYPE FROM hOUTPAY WHERE vcBPCode='$code'
) b ON a.vcRef = b.vcDocNum AND a.vcRefType = b.Type
WHERE b.vcBPCode= '$code'
		");
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenSO($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		SUM(b.`intOpenQty`/b.intQty*b.`intLineTotal`) AS val
		FROM hSO a
		LEFT JOIN dSO b ON a.intID = b.`intHID`
		WHERE a.vcBPCode='$code' and b.vcStatus='O'
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();	
		  return $r->val;
		}
		else
		{
			return 0;
		}
	}
	function GetOpenAR($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		b.intLineTotal AS val
		FROM hAR a
		LEFT JOIN dAR b ON a.intID = b.`intHID`
		WHERE a.vcBPCode='$code'
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();	
		  return $r->val;
		}
		else
		{
			return 0;
		}
	}
	function GetAllDataCustomer()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory, c.intRate
		from mbp a LEFT JOIN mbpcategory b on a.intCategory=b.intID 
		LEFT JOIN mtax c on a.intTax=c.intRate
		where a.intDeleted=0 and (a.vcType='C' or a.vcType='X') order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataVendor()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mbp a LEFT JOIN mbpcategory b on a.intCategory=b.intID where a.intDeleted=0 and (a.vcType='S' or a.vcType='X') order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCategory()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from mbpcategory a where a.intDeleted=0 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCategoryByType($type)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from mbpcategory a where a.intDeleted=0 and ( vcType='$type' or  '$type'='X') order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllPriceList()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from mpricecategory a where a.intDeleted=0 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function addpayment($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['AccName']=str_replace("'","''",$d['AccName']);
		
		$q=$this->db->query("insert into mbppayment (intBP,intBank,vcAccNumber,vcAccName)
		select '$d[BPID]','$d[Bank]','$d[AccNumber]','$d[AccName]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function delpayment($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			delete from mbppayment where intID='$id'
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['TaxNumber']=str_replace("'","''",$d['TaxNumber']);
		$d['Phone1']=str_replace("'","''",$d['Phone1']);
		$d['Phone2']=str_replace("'","''",$d['Phone2']);
		$d['Fax']=str_replace("'","''",$d['Fax']);
		$d['Email']=str_replace("'","''",$d['Email']);
		$d['Website']=str_replace("'","''",$d['Website']);
		$d['ContactPerson']=str_replace("'","''",$d['ContactPerson']);
		$d['CityBillTo']=str_replace("'","''",$d['CityBillTo']);
		$d['CountryBillTo']=str_replace("'","''",$d['CountryBillTo']);
		$d['AddressBillTo']=str_replace("'","''",$d['AddressBillTo']);
		$d['CityShipTo']=str_replace("'","''",$d['CityShipTo']);
		$d['CountryShipTo']=str_replace("'","''",$d['CountryShipTo']);
		$d['AddressShipTo']=str_replace("'","''",$d['AddressShipTo']);
		
		$d['Name']=str_replace('"','',$d['Name']);
		$d['TaxNumber']=str_replace('"','',$d['TaxNumber']);
		$d['Phone1']=str_replace('"','',$d['Phone1']);
		$d['Phone2']=str_replace('"','',$d['Phone2']);
		$d['Fax']=str_replace('"','',$d['Fax']);
		$d['Email']=str_replace('"','',$d['Email']);
		$d['Website']=str_replace('"','',$d['Website']);
		$d['ContactPerson']=str_replace('"','',$d['ContactPerson']);
		$d['CityBillTo']=str_replace('"','',$d['CityBillTo']);
		$d['CountryBillTo']=str_replace('"','',$d['CountryBillTo']);
		$d['AddressBillTo']=str_replace('"','',$d['AddressBillTo']);
		$d['CityShipTo']=str_replace('"','',$d['CityShipTo']);
		$d['CountryShipTo']=str_replace('"','',$d['CountryShipTo']);
		$d['AddressShipTo']=str_replace('"','',$d['AddressShipTo']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("insert into mbp (vcType,intCategory,intPriceList,intPaymentTerm,intTax,vcCode,vcName,vcTaxNum,vcTelp1,vcTelp2,vcFax,   
		vcEmail,vcWebsite,vcContactPerson,vcBillToAddress,vcBillToCity,  
		vcBillToCountry,vcShipToAddress,vcShipToCity,vcShipToCountry,intCreditLimit,intDeleted,vcUser,dtInsertTime,intDefWallet)
		select '$d[Type]','$d[Category]','$d[PriceList]','$d[PaymentTerm]','$d[Tax]','$d[Code]','$d[Name]','$d[TaxNumber]',
		'$d[Phone1]','$d[Phone2]','$d[Fax]','$d[Email]','$d[Website]','$d[ContactPerson]',
		'$d[AddressBillTo]','$d[CityBillTo]','$d[CountryBillTo]','$d[AddressShipTo]','$d[CityShipTo]','$d[CountryBillTo]',
		'$d[CreditLimit]',0,
		'$d[UserID]','$now','$d[DefWallet]'
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory from mbp a LEFT JOIN mbpcategory b on a.intCategory=b.intID
		where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['TaxNumber']=str_replace("'","''",$d['TaxNumber']);
		$d['Phone1']=str_replace("'","''",$d['Phone1']);
		$d['Phone2']=str_replace("'","''",$d['Phone2']);
		$d['Fax']=str_replace("'","''",$d['Fax']);
		$d['Email']=str_replace("'","''",$d['Email']);
		$d['Website']=str_replace("'","''",$d['Website']);
		$d['ContactPerson']=str_replace("'","''",$d['ContactPerson']);
		$d['CityBillTo']=str_replace("'","''",$d['CityBillTo']);
		$d['CountryBillTo']=str_replace("'","''",$d['CountryBillTo']);
		$d['AddressBillTo']=str_replace("'","''",$d['AddressBillTo']);
		$d['CityShipTo']=str_replace("'","''",$d['CityShipTo']);
		$d['CountryShipTo']=str_replace("'","''",$d['CountryShipTo']);
		$d['AddressShipTo']=str_replace("'","''",$d['AddressShipTo']);
		
		$d['Name']=str_replace('"','',$d['Name']);
		$d['TaxNumber']=str_replace('"','',$d['TaxNumber']);
		$d['Phone1']=str_replace('"','',$d['Phone1']);
		$d['Phone2']=str_replace('"','',$d['Phone2']);
		$d['Fax']=str_replace('"','',$d['Fax']);
		$d['Email']=str_replace('"','',$d['Email']);
		$d['Website']=str_replace('"','',$d['Website']);
		$d['ContactPerson']=str_replace('"','',$d['ContactPerson']);
		$d['CityBillTo']=str_replace('"','',$d['CityBillTo']);
		$d['CountryBillTo']=str_replace('"','',$d['CountryBillTo']);
		$d['AddressBillTo']=str_replace('"','',$d['AddressBillTo']);
		$d['CityShipTo']=str_replace('"','',$d['CityShipTo']);
		$d['CountryShipTo']=str_replace('"','',$d['CountryShipTo']);
		$d['AddressShipTo']=str_replace('"','',$d['AddressShipTo']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mbp';
		$his['doc']			= 'BP';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		update mbp set
		vcType='$d[Type]',
		intCategory='$d[Category]',
		intPriceList='$d[PriceList]',
		intPaymentTerm='$d[PaymentTerm]',
		intTax='$d[Tax]',
		vcName='$d[Name]',
		vcTaxNum='$d[TaxNumber]',
		vcTelp1='$d[Phone1]',
		vcTelp2='$d[Phone2]',
		vcFax='$d[Fax]',
		vcEmail='$d[Email]',
		vcWebsite='$d[Website]',
		vcContactPerson='$d[ContactPerson]',
		vcBillToAddress='$d[AddressBillTo]',
		vcBillToCity='$d[CityBillTo]',
		vcBillToCountry='$d[CountryBillTo]',
		vcShipToAddress='$d[AddressShipTo]',
		vcShipToCity='$d[CityShipTo]',
		vcShipToCountry='$d[CountryShipTo]',
		intCreditLimit='$d[CreditLimit]',
		intDefWallet='$d[DefWallet]'
		where intID='$d[id]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if(isset($d['LastName']))
		{
			$d['LastName']=str_replace("'","''",$d['LastName']);
			if($d['LastName']!=$d['Name'])
			{
				$qgettable = $this->db->query("SELECT DISTINCT TABLE_NAME 
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE COLUMN_NAME IN ('vcBPName') ORDER BY TABLE_NAME ASC");
				
				foreach($qgettable->result() as $rrrr) 
				{
					$tablename = $rrrr->TABLE_NAME;
					$this->db->query("update $tablename set vcBPName='$d[Name]' where vcBPCode='$d[Code]'");
				}
			}
		}
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function updateBDO($id,$field,$value)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mbp set $field=$field+$value where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mbp set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */