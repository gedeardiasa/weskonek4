<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bp_category extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcType='C' then 'Customer'
		when a.vcType='S' then 'Supplier'
		else 'Unknown' end as vcTypeName
		from mbpcategory a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mbpcategory a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.intID from mbpcategory a where a.vcName='$name'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->intID;
		}
		else
		{
			return null;
		}
	}
	function GetArActByBPCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcARAccount 
		FROM mbpcategory a LEFT JOIN mbp b ON a.intID=b.intCategory
		WHERE b.vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcARAccount;
		}
		else
		{
			return null;
		}
	}
	function GetApActByBPCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcAPAccount 
		FROM mbpcategory a LEFT JOIN mbp b ON a.intID=b.intCategory
		WHERE b.vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcAPAccount;
		}
		else
		{
			return null;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mbpcategory (vcName,vcType,dtInsertTime,vcARAccount,vcAPAccount)
		values ('$d[Name]','$d[Type]','$now','$d[ARAct]','$d[APAct]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mbpcategory';
		$his['doc']			= 'BPCATEGROY';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		$q=$this->db->query("update mbpcategory set vcName='$d[Name]',vcType='$d[Type]',vcARAccount='$d[ARAct]',vcAPAccount='$d[APAct]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mbpcategory set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */