<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_client extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mclient
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetActiveData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mclient where intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Domain']=str_replace("'","''",$d['Domain']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['DB']=str_replace("'","''",$d['DB']);
		$master=$this->load->database('master', TRUE);
		
		$cek=$master->query("
		select * from mclient where vcSubDomain='$d[Domain]' or vcDB='$d[DB]'
		");
		
		if($cek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			$cr=$master->query("CREATE DATABASE $d[DB];
			");
			
			$q=$master->query("insert into mclient (vcSubDomain,vcName,vcDB)
			values ('$d[Domain]','$d[Name]','$d[DB]')
			");
			if($q)
			{
				
			  return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Domain']=str_replace("'","''",$d['Domain']);
		$d['LastDomain']=str_replace("'","''",$d['LastDomain']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['DB']=str_replace("'","''",$d['DB']);
		$master=$this->load->database('master', TRUE);
		
		$cek=$master->query("
		select * from mclient where vcSubDomain='$d[Domain]' and vcSubDomain!='$d[LastDomain]'
		");
		if($cek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			$q=$master->query("update mclient set vcName='$d[Name]', vcSubDomain='$d[Domain]' where intID='$d[id]'
			");
			if($q)
			{
			  return 1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	function delete($id)
	{
		$master=$this->load->database('master', TRUE);
		$q=$master->query("update mclient set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */