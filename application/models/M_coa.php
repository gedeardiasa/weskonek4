<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_coa extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllDataLevel2()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgl a where a.intDeleted=0 and a.intLevel=2 order by a.vcCode desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataLevel5()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgl a where a.intDeleted=0 and a.intLevel=5 order by a.vcCode desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataLevel5ByHead($head)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.* FROM mgl a WHERE a.intDeleted=0 AND a.intLevel=5
		AND LEFT(a.`vcCode`,1)=$head
		ORDER BY a.vcCode DESC
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgl a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetGL($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*,
		b.*
		FROM dMutationJE a
		LEFT JOIN hJE b ON a.`intHID`=b.`intID`
		where a.vcGLCode='$d[coa]' and a.dtDate>='$d[from]' and a.dtDate<='$d[until]' order by a.vcGLCode, a.dtDate, a.dtInsertTime, a.intID
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	
	function recalculate($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*,
		b.*, a.intID as id
		FROM dMutationJE a
		LEFT JOIN hJE b ON a.`intHID`=b.`intID`
		where a.vcGLCode='$d[glCode]' AND YEAR(a.dtDate)=$d[year]  order by a.vcGLCode, a.dtDate, a.dtInsertTime, a.intID
		");
		
		$balance = $this->db->query("select intBalance from mgl where vcCode='$d[glCode]'
		");
		$balanceresult = $balance->row()->intBalance;
		
		
		$idx=0;
		foreach($q->result() as $qq){
			$jj[$idx]['id']=$qq->id;
			$jj[$idx]['value']=$qq->intValue;
			$idx++;
		}
		
		for($i=$idx-1;$i>=0;$i--)
		{
			$valfix		= $jj[$i]['value'];
			$idfix		= $jj[$i]['id'];
			$this->db->query("update dMutationJE set intLastValue=$balanceresult - $valfix where vcGLCode='$d[glCode]' and intID=$idfix");
			$balanceresult = $balanceresult - $valfix;
		}
		echo 'Success';
	}
	function GetCodeByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcCode from mgl where vcName='$name'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return '';
		}
	}
	function GetCodeByName5($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcCode from mgl where vcName='$name' and intLevel=5
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return '';
		}
	}
	function GetCodeById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcCode from mgl where intID='$id'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return null;
		}
	}
	function GetNameByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mgl where vcCode='$code'
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetNameByCode5($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mgl where vcCode='$code' and intLevel=5
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Level']=str_replace("'","''",$d['Level']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Level']=str_replace('"','',$d['Level']);

		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mgl (vcCode,vcName,intLevel)
		values ('$d[Code]','$d[Name]','$d[Level]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Level']=str_replace("'","''",$d['Level']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Level']=str_replace('"','',$d['Level']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mgl';
		$his['doc']			= 'COA';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mgl set vcCode='$d[Code]',vcName='$d[Name]',intLevel='$d[Level]'
		where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mgl set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."'), vcCode=CONCAT(vcCode,'X') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function changebalance($code,$val)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("update mgl set intBalance=intBalance+$val where vcCode='$code'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function changeremarks()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hJE where vcRemarks=''");
		foreach($q->result() as $r)
		{
			echo $r->vcRef." - ".$r->vcRefType."<br>";
			$vcRefType = $r->vcRefType;
			$docnum = $r->vcRef;
			$idje = $r->intID;
			if($vcRefType=='AD')
			{
				$table = 'hAdjustment';
				$kk = 'normal';
			}
			else if($vcRefType=='DEPWITH')
			{
				$table = 'hWalletAdjustment';
				$kk = 'normal';
			}
			else if($vcRefType=='WT')
			{
				$table = 'hWalletTransfer';
				$kk = 'normal';
			}
			else if($vcRefType=='ADX')
			{
				$table = 'hAdjustment';
				$kk = 'cancel';
			}
			else if($vcRefType=='OUTPAYX')
			{
				$table = 'hOUTPAY';
				$kk = 'cancel';
			}
			else if($vcRefType=='INPAYX')
			{
				$table = 'hINPAY';
				$kk = 'cancel';
			}
			else if($vcRefType=='ARX')
			{
				$table = 'hAR';
				$kk = 'cancel';
			}
			else if($vcRefType=='APDPX')
			{
				$table = 'hAPDP';
				$kk = 'cancel';
			}
			else if($vcRefType=='DNX')
			{
				$table = 'hDN';
				$kk = 'cancel';
			}
			else if($vcRefType=='APDPC')
			{
				$table = 'hAPDP';
				$kk = 'normal';
			}
			else
			{
				$table = 'h'.$vcRefType.'';
				$kk = 'normal';
			}
			$grmks = $this->db->query("select vcRemarks from $table where vcDocNum='$docnum'");
			$rrmks = $grmks->row();
			if($kk=='normal')
			{
				$rmkkk = $rrmks->vcRemarks;
			}
			else
			{
				$rmkkk = "(Cancelation) ".$rrmks->vcRemarks;
			}
			$this->db->query("update hJE set vcRemarks='$rmkkk' where intID='$idje' and vcRemarks=''");
		}
	}
}

/* End of file validasi.php */
/* plan: ./application/models/validasi.php */