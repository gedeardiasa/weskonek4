<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_currency extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mcurrency a order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mcurrency a where a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace('"','',$d['Code']);
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mcurrency (vcName,vcCode)
		values ('$d[Name]','$d[Code]')
		");
		if($q)
		{
		  $id=$this->db->query("select LAST_INSERT_ID() as intID");
		  $rid=$id->row();
		  $idtin=$rid->intID;
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace('"','',$d['Code']);
		$q=$this->db->query("update mcurrency set vcName='$d[Name]', vcCode='$d[Code]' where intID='$d[id]'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */