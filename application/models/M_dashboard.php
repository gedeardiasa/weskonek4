<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dashboard extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mdashboard a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mdashboard a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetAccessDashboard($session)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode, b.vcName , b.intWidth
		from maccess_dashboard a LEFT JOIN mdashboard b on a.intDashboard=b.intID 
		where a.intUserID='$_SESSION[IDPOS]' order by a.intOrder
		");
		return $q;
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Width']=str_replace("'","''",$d['Width']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mdashboard (vcCode,vcName,intWidth,dtInsertTime, vcRemarks)
		values ('$d[Code]','$d[Name]','$d[Width]','$now','$d[Remarks]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$q=$this->db->query("update mdashboard set vcCode='$d[Code]', vcName='$d[Name]', intWidth='$d[Width]', vcRemarks='$d[Remarks]' where intID='$d[id]'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mdashboard set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */