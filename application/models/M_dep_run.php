<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dep_run extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDEPR a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		where vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDataToRun($day)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from hDEPR a where a.intDateRun<=$day and $day<=31 and a.vcStatus='O'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetail($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from dDEPR where intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDEPR a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDEPR a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dDEPR a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hDEPR where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function deleteH($id)
	{
		$q=$this->db->query("update hDEPR set vcStatus='X' where intID='$id'
		");
		
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['Account']=str_replace("'","''",$d['Account']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hDEPR');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hDEPR (vcDocNum,dtDate,intDateRun,vcRef,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]','$d[DateRun]','$d[RefNum]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeDEPR']=str_replace("'","''",$d['itemcodeDEPR']);
		$d['itemnameDEPR']=str_replace("'","''",$d['itemnameDEPR']);
		$q=$this->db->query("insert into dDEPR (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName, intCost)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeDEPR]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeDEPR]','$d[itemnameDEPR]','$d[costDEPR]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */