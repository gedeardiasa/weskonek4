<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_disc extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDiscount a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDiscount a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hDiscount a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dDiscount a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDiscByItemAndWhs($item,$whs)
	{
		$now=date('Y-m-d');
		$q=$this->db->query("select a.* from dDiscount a
		LEFT JOIN hDiscount b on a.intHID=b.intID
		where b.dtDate<='$now' and b.dtUntil>='$now' and a.intItem='$item' and b.intLocation='$whs'
		");
		
		if($q->num_rows()>0)
		{
			return $q->row()->intDisc;
		}
		else
		{
			return 0;
		}
		
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hDiscount where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hDiscount');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hDiscount (vcDocNum,dtDate,dtUntil,vcName,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]','$d[UntilDate]','$d[Name]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeDISC']=str_replace("'","''",$d['itemcodeDISC']);
		$d['itemnameDISC']=str_replace("'","''",$d['itemnameDISC']);
		$q=$this->db->query("insert into dDiscount (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intDisc)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeDISC]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeDISC]','$d[itemnameDISC]','$d[discDISC]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($docnum)
	{
		$this->db->trans_begin();
		$q=$this->db->query("select intID from hDiscount where vcDocNum='$docnum'
		");
		$intID=$q->row()->intID;
		
		$this->db->query("delete from dDiscount where intHID='$intID'
		");
		
		$this->db->query("delete from hDiscount where intID='$intID'
		");
		
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */