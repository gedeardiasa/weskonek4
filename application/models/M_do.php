<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_do extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hDO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hDO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dDO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hDO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dDO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hDO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dDO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,c.vcDocNum, c.dtDate from dDO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hDO c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPlanByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		DISTINCT(d.`intID`) AS intPlan
		FROM hDO a LEFT JOIN dDO b ON a.`intID`=b.`intHID`
		LEFT JOIN mlocation c ON b.`intLocation`=c.`intID`
		LEFT JOIN mplan d ON d.`intID`=c.`intPlan`
		WHERE a.`vcDocNum`='$doc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return 0;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hDO a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hDO a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['CityH']=str_replace("'","''",$d['CityH']);
		$d['CountryH']=str_replace("'","''",$d['CountryH']);
		$d['AddressH']=str_replace("'","''",$d['AddressH']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hDO');
		// end cek DocNum kembar
		
		/*$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcShipToCity;
		$Country = $adrs->vcShipToCountry;
		$AddressH = $adrs->vcShipToAddress;*/
		
		$q=$this->db->query("insert into hDO (intBP,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[BPId]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now',
		'$d[CityH]','$d[CountryH]','$d[AddressH]')
		");
		
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hDO set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dDO set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hDO set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update dDO set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
		");
	}
	function cekclose($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		if(is_numeric($item)==true)
		{
			$varitem = 'intItem';
		}
		else
		{
			$varitem = 'vcItemName';
		}
		$this->db->query("
		update dDO set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and $varitem='$item'
		");// query update open qty DO
		
		$this->db->query("
		update dDO set
		vcStatus='C'
		where intHID='$id' and $varitem='$item' and intOpenQty<=0
		");// query update status dDO
		
		$cekdetailsq=$this->db->query("
		select intID from dDO where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dDO yang statusnya open
		{
			$this->db->query("
			update hDO set
			vcStatus='C'
			where intID='$id'
			");// maka update hDO status ke 'C' (Close)
		}
	}
	function cancelall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hDO set vcStatus='X' where intID='$id'
		");
		$q2=$this->db->query("
		update dDO set vcStatus='X' where intHID='$id'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['CityH']=str_replace("'","''",$d['CityH']);
		$d['CountryH']=str_replace("'","''",$d['CountryH']);
		$d['AddressH']=str_replace("'","''",$d['AddressH']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hDO';
		$his['doc']			= 'DO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hDO set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		vcCity='$d[CityH]',
		vcCountry='$d[CountryH]',
		vcAddress='$d[AddressH]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeDO']=str_replace("'","''",$d['itemcodeDO']);
		$d['itemnameDO']=str_replace("'","''",$d['itemnameDO']);
		
		$d['uomDO']=str_replace("'","''",$d['uomDO']);
		$d['uominvDO']=str_replace("'","''",$d['uominvDO']);
		
		$q=$this->db->query("insert into dDO (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intCost, intLineCost, intLocation,vcLocation)
		values ('$d[intHID]','$d[idBaseRefDO]','$d[BaseRefDO]','$d[itemID]','$d[itemcodeDO]','$d[itemnameDO]','$d[qtyDO]','$d[qtyDO]','$d[uomDO]',
		'$d[uomtypeDO]','$d[qtyinvDO]','$d[qtyinvDO]','$d[uominvDO]','$d[priceDO]','$d[discperDO]','$d[discDO]','$d[priceafterDO]',
		'$d[linetotalDO]','$d[costDO]','$d[linecostDO]','$d[whsDO]','$d[whsNameDO]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeDO']=str_replace("'","''",$d['itemcodeDO']);
		$d['itemnameDO']=str_replace("'","''",$d['itemnameDO']);
		
		$d['uomDO']=str_replace("'","''",$d['uomDO']);
		$d['uominvDO']=str_replace("'","''",$d['uominvDO']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dDO';
		$his['doc']			= 'DO';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dDO where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodeDO'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dDO set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeDO]',
		vcItemName='$d[itemnameDO]',
		intQty='$d[qtyDO]',
		intOpenQty='$d[qtyDO]',
		vcUoM='$d[uomDO]',
		intUoMType='$d[uomtypeDO]',
		intQtyInv='$d[qtyinvDO]',
		intOpenQtyInv='$d[qtyinvDO]',
		vcUoMInv='$d[uominvDO]',
		intPrice='$d[priceDO]',
		intDiscPer='$d[discperDO]',
		intDisc='$d[discDO]',
		intPriceAfterDisc='$d[priceafterDO]',
		intLineTotal='$d[linetotalDO]',
		intLineCost='$d[linecostDO]',
		intLocation='$d[whsDO]',
		vcLocation='$d[whsNameDO]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dDO where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */