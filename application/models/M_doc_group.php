<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_doc_group extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcDocName from mdocgroup a
		LEFT JOIN mdoc b on a.intDoc=b.intID order by b.vcName, a.vcCode, a.vcUserID asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDistinctCode()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select distinct(vcCode) from mdocgroup
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDistinctName()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select distinct(vcName) as vcName from mdocgroup
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mdocgroup a where a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetCodeByName($val)
	{
		$db=$this->load->database('default', TRUE);
		
		$val=str_replace("'","''",$val);
		$q=$this->db->query("select vcCode from mdocgroup where vcName='$val'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return '';
		}
	}
	function GetNameByCode($val)
	{
		$db=$this->load->database('default', TRUE);
		
		$val=str_replace("'","''",$val);
		$q=$this->db->query("select vcName from mdocgroup where vcCode='$val'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return '';
		}
	}
	
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mdocgroup (intDoc,vcCode,vcName,vcUserID,intCreate,intUpdate,intDelete,intRead)
		values ('$d[Doc]','$d[Code]','$d[Name]','$d[User]','$d[Create]','$d[Update]','$d[Delete]','$d[Read]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mdocgroup';
		$his['doc']			= 'DOCGROUP';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mdocgroup set 
		intDoc='$d[Doc]',
		vcCode='$d[Code]',
		vcName='$d[Name]',
		vcUserID='$d[User]',
		intCreate='$d[Create]',
		intUpdate='$d[Update]',
		intDelete='$d[Delete]',
		intRead='$d[Read]'
		where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from mdocgroup where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */