<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_docnum extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$q=$this->db->query("
			select * from mdoc
		");
		
		return $q;
		
	}
	function GetString($id,$table,$field)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select $field as vcdata from $table where intID='$id';
		");
		$r=$q->row();
		return $r->vcdata;
		
	}
	function GetCustomPrintByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intCustomPrint from mdoc where vcCode='$code';
		");
		$r=$q->row();
		return $r->intCustomPrint;
		
	}
	function GetNameByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select vcName from mdoc where vcCode='$code';
		");
		$r=$q->row();
		return $r->vcName;
		
	}
	function GetDataH($tableH,$tableD,$docnum)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select
		b.*
		from $tableH b
		where b.vcDocNum='$docnum'
		");
		if($q){
			return $q->row();
		}
		else{
			return false;
		}
		
	}
	function GetDataDD($tableH,$tableD,$docnum)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select
		a.*
		from $tableD a
		left join $tableH b
		on a.intHID=b.intID
		where b.vcDocNum='$docnum'
		");
		return $q;
	}
	function GetDataDDHH($tableH,$tableD,$docnum)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select
		a.*
		from $tableD a
		left join $tableH b
		on a.intHID=b.intID
		where b.vcDocNum='$docnum'
		");
		return $q->row();
	}
	function getIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mdoc where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->intID;
		}
		else
		{
			return "";
		}
	}
	function GetOpenData($d,$iduser)
	{
		$q=$this->db->query("
			select a.*, b.vcDocNum, b.dtDate, b.vcBPName, b.dtDelDate
			from $d[DetailTable] a
			LEFT JOIN $d[HeaderTable] b on a.intHID=b.intID
			where  a.vcStatus='O' and b.vcStatus='O' and
			a.intID not in
			(
				SELECT a.intHID
				FROM $d[DetailTable] a
				LEFT JOIN 
				(
					mlocation b
					LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
				) 
				ON a.`intLocation`=b.intID
				WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
			)
		");
		
		return $q;
	}
	function GetDocGroupByDocCodeAndUser($doccode,$user)
	{
		$q=$this->db->query("
			SELECT DISTINCT(a.vcCode), a.`vcName` FROM mdocgroup a
			LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
			WHERE b.vcCode='$doccode' AND a.`vcUserID`='$user'
		");
		
		return $q;
	}
	function GetDocGroupByDocCodeAndUserCanEdit($doccode,$user)
	{
		$q=$this->db->query("
			SELECT DISTINCT(a.vcCode), a.`vcName` FROM mdocgroup a
			LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
			WHERE b.vcCode='$doccode' AND a.`vcUserID`='$user' and intUpdate=1
		");
		
		return $q;
	}
	function GetDefaultGroup($doccode,$user)
	{
		$q=$this->db->query("
			SELECT DISTINCT(a.vcCode), a.`vcName` FROM mdocgroup a
			LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
			WHERE b.vcCode='$doccode' AND a.`vcUserID`='$user' limit 0,1
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return "";
		}
	}
	function CekDocGroup($doccode,$code,$user,$tipe)
	{
		$q=$this->db->query("
			SELECT DISTINCT(a.vcCode), a.`vcName` FROM mdocgroup a
			LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
			WHERE b.vcCode='$doccode' AND a.`vcUserID`='$user' and a.vcCode='$code' and $tipe=1
		");
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetLastDocNum($table)
	{
		$db=$this->load->database('default', TRUE);
		$kop=substr(date('Y'),-2);
		$q=$this->db->query("
			SELECT MAX(cast(vcDocNum as decimal)) id FROM $table
			where LEFT(vcDocNum,2)='$kop'
		");
		
		$r=$q->row();
		if($r->id!=null)
		{
			$result = $r->id+1;
		}
		else
		{
			$result=$kop."0000001";
		}
		
		return $result;
	}
	function GetCustomLastDocNum($table,$doctype,$doctable)
	{
		$db=$this->load->database('default', TRUE);
		$docquery 	= $this->db->query("
			SELECT * from $doctable where intID=$doctype
		");
		if($docquery->num_rows()>0)
		{
			$doctype	= $docquery->row();
			
			$dockop		= $doctype->vcDoc;
			$dockop		= str_replace('{year}',date('Y'),$dockop);
			$dockop		= str_replace('{month}',date('m'),$dockop);
			
			$doclgt		= strlen($dockop)-4;
			
			$dockop2	= str_replace('{no}','',$dockop);
			
			$q=$this->db->query("
				SELECT MAX(CAST(CONCAT('99',LEFT(vcDocNumCustom,7)) AS DECIMAL)) id FROM $table
				where right(vcDocNumCustom,$doclgt)='$dockop2'
			");
			$r=$q->row();
			if($r->id!=null)
			{
				$qwerty = $r->id+1;
				$qwerty = ltrim($qwerty, '99');
				$result= str_replace('{no}',$qwerty,$dockop);
				
			}
			else
			{
				$result= str_replace('{no}','001',$dockop);
			}
		}
		else{
			$result = '';
		}
		return $result;
		
	}
	
	function GetDocSetting($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			select * from mdoc where vcCode='$doc'
		");
		return $q->row();
		
	}
	function GetAllDocType()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			select * from mdoc
		");
		return $q;
		
	}
	function GetUserApv($user,$doc, $plan=0)
	{
		$db=$this->load->database('default', TRUE);
		$qdoc = $this->db->query("
			select intID from mdoc where vcCode='$doc'
		");
		$rdoc = $qdoc->row();
		$iddoc = $rdoc->intID;
		
		$q=$this->db->query("
			select * from mapproval where intDoc='$iddoc' and intPlan='$plan' and vcUserApv='$user'
		");
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function GetDocRelationBefore($docnum,$type,$ghk)
	{
		$db=$this->load->database('default', TRUE);
		
		
		$q=$this->db->query("
		SELECT 
		b.`vcDocNum`, b.`dtDate`, b.intID
		FROM h$type b WHERE b.intID IN
		(
			SELECT a.intBaseRef FROM d$ghk a LEFT JOIN h$ghk b ON a.`intHID`=b.intID 
			WHERE b.`vcDocNum`='$docnum' AND a.vcBaseType='$type'
		)
		");
		return $q;
	}
	function GetDocRelationAfter($docnum,$type,$doc)
	{
		$db=$this->load->database('default', TRUE);
		
		
		$q=$this->db->query("
		SELECT 
		b.`vcDocNum`, b.`dtDate`, b.intID
		FROM d$type a
		LEFT JOIN h$type b ON a.intHID=b.`intID`
		WHERE a.vcBaseType='$doc' AND a.intBaseRef= (SELECT intID FROM h$doc WHERE vcDocnum='$docnum')
		GROUP BY b.`vcDocNum`
		");
		
		return $q;
	}
	function GetPlanIDByDoc($header,$detail,$docnum)
	{
		$db=$this->load->database('default', TRUE);
		
		
		$q=$this->db->query("
		SELECT 
		DISTINCT(d.intID) AS intPlan
		FROM
		$detail a LEFT JOIN $header b ON a.`intHID`=b.intID
		LEFT JOIN mlocation c ON a.intLocation=c.`intID`
		LEFT JOIN mplan d ON c.`intPlan`=d.intID
		WHERE b.vcDocNum='$docnum'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return 0;
		}
		
	}
	function CekAproval($doc)
	{
		$cekapproval 	= $this->db->query("select * from mdoc where vcCode='$doc'
		");
		$rapv			= $cekapproval->row();
		
		if($rapv->intApv==1) // jika butuh aproval maka status document pending
		{
			return 1;
		}
		else //jika tidak maka status document Open
		{
			return 0;
		}
		
	}
	function GetBackdate($doc)
	{
		$q 		= $this->db->query("select intBackDate from mdoc where vcCode='$doc'
		");
		$r		= $q->row();
		return $r->intBackDate;
	}
	function getUserApvByDocCode($doc)
	{
		$q 		= $this->db->query("
		select a.*, b.vcName as vcPlan from mapproval a 
		LEFT JOIN mplan b on a.intPlan=b.intID
		where a.intDoc='$doc'
		");
		return $q;
		
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mdoc';
		$his['doc']			= 'DOC';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
			update mdoc set vcName='$d[Name]', vcCode='$d[Code]', intWidth='$d[Width]', intHeight='$d[Height]', vcFontFamily='$d[Font]', intApv='$d[Apv]', vcUserApv='$d[UserApv]'
			, intBackDate='$d[Backdate]' , intCustomPrint='$d[CuP]'
			where intID='$d[id]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		return $q;
		
	}
	function adduser($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			insert into mapproval (intDoc, intPlan, vcUserApv)
			values ('$d[Code]','$d[Plan]','$d[UserApv]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function deluser($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			delete from mapproval where intID='$id'
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */