<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_form extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllForm()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcHeader, case when a.IsHeader=1 then 'Yes' when a.IsHeader=0 then 'No' end as vcIsHeader 
		from mform a left join mform b on a.intHeader=b.intID where a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mform a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetHeader()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mform a where a.intDeleted=0 and isHeader=1
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		if($d['Header']==0)
		{
			$isheader=1;
		}
		else{
			$isheader=0;
		}
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mform (vcName,vcCode,vcRemarks,intHeader,IsHeader)
		values ('$d[Name]','$d[Code]','$d[Remarks]','$d[Header]','$isheader')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		if($d['Header']==0)
		{
			$isheader=1;
		}
		else{
			$isheader=0;
		}
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$q=$this->db->query("update mform set vcName='$d[Name]', vcCode='$d[Code]', vcRemarks='$d[Remarks]', 
		intHeader='$d[Header]', isHeader='$isheader' where intID='$d[id]'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mform set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */