<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_group_cf extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllGroup()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcHeader, case when a.IsHeader=1 then 'Yes' when a.IsHeader=0 then 'No' end as vcIsHeader 
		from mgroupcashflow a left join mgroupcashflow b on a.intHeader=b.intID where a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeader()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgroupcashflow a where a.intDeleted=0 and isHeader=1
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetail()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgroupcashflow a where a.intDeleted=0 and isHeader=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mgroupcashflow where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		if($d['Header']==0)
		{
			$isheader=1;
		}
		else{
			$isheader=0;
		}
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mgroupcashflow (vcName,vcCode,intHeader,IsHeader)
		values ('$d[Name]','$d[Code]','$d[Header]','$isheader')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		if($d['Header']==0)
		{
			$isheader=1;
		}
		else{
			$isheader=0;
		}
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mgroupcashflow';
		$his['doc']			= 'GROUPCF';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		
		$q=$this->db->query("update mgroupcashflow set vcName='$d[Name]', vcCode='$d[Code]', 
		intHeader='$d[Header]', isHeader='$isheader' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mgroupcashflow set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mgroupcashflow a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */