<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_grpo extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hGRPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hGRPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dGRPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hGRPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dGRPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hGRPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dGRPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPlanByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		DISTINCT(d.`intID`) AS intPlan
		FROM hGRPO a LEFT JOIN dGRPO b ON a.`intID`=b.`intHID`
		LEFT JOIN mlocation c ON b.`intLocation`=c.`intID`
		LEFT JOIN mplan d ON d.`intID`=c.`intPlan`
		WHERE a.`vcDocNum`='$doc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return 0;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,c.vcDocNum, c.dtDate from dGRPO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hGRPO c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hGRPO a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hGRPO a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hGRPO');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcShipToCity;
		$Country = $adrs->vcShipToCountry;
		$AddressH = $adrs->vcShipToAddress;
		
		$q=$this->db->query("insert into hGRPO (intBP,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[BPId]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hGRPO set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dGRPO set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hGRPO set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update dGRPO set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
		");
	}
	function cekclose($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		$this->db->query("
		update dGRPO set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and intItem='$item'
		");// query update open qty GRPO
		
		$this->db->query("
		update dGRPO set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dGRPO
		
		$cekdetailsq=$this->db->query("
		select intID from dGRPO where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dGRPO yang statusnya open
		{
			$this->db->query("
			update hGRPO set
			vcStatus='C'
			where intID='$id'
			");// maka update hGRPO status ke 'C' (Close)
		}
	}
	function cancelall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hGRPO set vcStatus='X' where intID='$id'
		");
		$q2=$this->db->query("
		update dGRPO set vcStatus='X' where intHID='$id'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("
		
		update hGRPO set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeGRPO']=str_replace("'","''",$d['itemcodeGRPO']);
		$d['itemnameGRPO']=str_replace("'","''",$d['itemnameGRPO']);
		
		$d['uomGRPO']=str_replace("'","''",$d['uomGRPO']);
		$d['uominvGRPO']=str_replace("'","''",$d['uominvGRPO']);
		
		$q=$this->db->query("insert into dGRPO (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intCost, intLineCost, intLocation,vcLocation)
		values ('$d[intHID]','$d[idBaseRefGRPO]','$d[BaseRefGRPO]','$d[itemID]','$d[itemcodeGRPO]','$d[itemnameGRPO]','$d[qtyGRPO]','$d[qtyGRPO]','$d[uomGRPO]',
		'$d[uomtypeGRPO]','$d[qtyinvGRPO]','$d[qtyinvGRPO]','$d[uominvGRPO]','$d[priceGRPO]','$d[discperGRPO]','$d[discGRPO]','$d[priceafterGRPO]',
		'$d[linetotalGRPO]','$d[costGRPO]','$d[linecostGRPO]','$d[whsGRPO]','$d[whsNameGRPO]')
		");
		
		if($q)
		{
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeGRPO']=str_replace("'","''",$d['itemcodeGRPO']);
		$d['itemnameGRPO']=str_replace("'","''",$d['itemnameGRPO']);
		
		$d['uomGRPO']=str_replace("'","''",$d['uomGRPO']);
		$d['uominvGRPO']=str_replace("'","''",$d['uominvGRPO']);
		
		$q=$this->db->query("update dGRPO set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeGRPO]',
		vcItemName='$d[itemnameGRPO]',
		intQty='$d[qtyGRPO]',
		intOpenQty='$d[qtyGRPO]',
		vcUoM='$d[uomGRPO]',
		intUoMType='$d[uomtypeGRPO]',
		intQtyInv='$d[qtyinvGRPO]',
		intOpenQtyInv='$d[qtyinvGRPO]',
		vcUoMInv='$d[uominvGRPO]',
		intPrice='$d[priceGRPO]',
		intDiscPer='$d[discperGRPO]',
		intDisc='$d[discGRPO]',
		intPriceAfterDisc='$d[priceafterGRPO]',
		intLineTotal='$d[linetotalGRPO]',
		intLocation='$d[whsGRPO]',
		vcLocation='$d[whsNameGRPO]'
		where intID='$d[intID]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dGRPO where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */