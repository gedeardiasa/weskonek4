<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_history extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function getdatabyid($his)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from $his[table] where $his[key]
		");

		if($q->num_rows()>0)
		{
		  return $q->row_array();
		}
		else
		{
			return null;
		}
	}
	function getdatabyquery($query)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query($query);
		if($q->num_rows()>0)
		{
			$r	=	$q->row();
			return $r->id;
		}
		else
		{
			return 0;
		}
	}
	function getcolumn($table)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("SHOW COLUMNS FROM $table");
		return $q;
	}
	function GetDataByDocAndKey($key,$doc)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("select a.*, b.vcName as ColumnName from dHistory a 
		LEFT JOIN mcolumn b on a.vcField=b.vcCode
		where a.intIDKey='$key' and a.vcDoc='$doc'");
		return $q;
	}
	function inserthistory($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['vcBeforeValue']=str_replace("'","''",$d['vcBeforeValue']);
		$d['vcAfterValue']=str_replace("'","''",$d['vcAfterValue']);
		
		$q=$this->db->query("insert into dHistory (vcDoc,vcTable,vcField,intIDKey,vcDetailKey,vcBeforeValue,vcAfterValue,vcUserID,dtUpdateTime)
		values ('$d[vcDoc]','$d[vcTable]','$d[vcField]','$d[intIDKey]','$d[vcDetailKey]','$d[vcBeforeValue]','$d[vcAfterValue]','$d[vcUserID]',
		'$d[dtUpdateTime]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function createhistory($his,$databefore,$dataafter)
	{
		$now				= date('Y-m-d H:i:s');
		$column				= $this->history->getcolumn($his['table']); // get column table
		foreach($column->result() as $c) // looping column
		{
			if($dataafter[$c->Field]!=$databefore[$c->Field]) // cek kolom apakah sama, jika tidak sama maka ada perubahan dan dibuatkan history
			{
				if($c->Field!='blpImage' and $c->Field!='blpImageMin')
				{
					$chis['vcDoc']			= $his['doc'];
					$chis['vcTable']		= $his['table'];
					$chis['vcField']		= $c->Field;
					$chis['intIDKey']		= $his['id'];
					$chis['vcDetailKey']	= $his['detailkey'];
					$chis['vcBeforeValue']	= $databefore[$c->Field];
					$chis['vcAfterValue']	= $dataafter[$c->Field];
					$chis['vcUserID']		= $his['UserID'];
					$chis['dtUpdateTime']	= $now;
					
					$hisresult				= $this->inserthistory($chis); // buat history
				}
			}
		}
		
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */