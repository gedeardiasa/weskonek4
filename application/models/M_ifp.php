<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ifp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hIFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, d.vcDocNum as PRONum
		from hIFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		LEFT JOIN hPRO d on a.intPRO=d.intID
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, d.vcDocNum as PRONum
		from hIFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		LEFT JOIN hPRO d on a.intPRO=d.intID
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and a.dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dIFP a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailDataByPROID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		a.*, b.`intItem`, b.`vcItemCode`, b.`vcItemName`, b.`intQty`, b.`intCost`
		FROM hIFP a
		LEFT JOIN dIFP b ON a.intID=b.`intHID`
		WHERE a.`intPRO`=$id
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum as PRONum from hIFP a 
		LEFT JOIN hPRO b on a.intPRO=b.intID
		where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		//$db=$d['db'];
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hIFP');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hIFP (vcDocNum,intPRO,dtDate,vcRef,vcType,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[PRONum]','$d[DocDate]','$d[RefNum]','$d[Type]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		//db=$d['db'];
		$d['itemcodeIFP']=str_replace("'","''",$d['itemcodeIFP']);
		$d['itemnameIFP']=str_replace("'","''",$d['itemnameIFP']);
		$d['uomIFP']=str_replace("'","''",$d['uomIFP']);
		$q=$this->db->query("insert into dIFP (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intQty, intCost)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeIFP]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeIFP]','$d[itemnameIFP]','$d[qtyIFP]','$d[costIFP]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */