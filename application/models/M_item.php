<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_item extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetLastCode($cat)
	{
		$db=$this->load->database('default', TRUE);
		
		
		$qcatcode=$this->db->query("
			select vcCode from mitemcategory where intID='$cat'
		");
		$catcode=$qcatcode->row()->vcCode;
		
		// $q=$this->db->query("
			// SELECT MAX(RIGHT(vcCode,6))+1 as id FROM mitem
			// where intCategory='$cat'
		// ");
		
		$q=$this->db->query("
			SELECT MAX(RIGHT(vcCode,6))+1 as id FROM mitem
			where vcCode like '%$catcode%'
		");
		
		$r=$q->row();
		if($r->id!=null)
		{
			$paddedNum = sprintf("%06d", $r->id);
			$result = $catcode."".$paddedNum;
		}
		else
		{
			$result=$catcode."000001";
		}
		
		return $result;
	}
	function GetAllDataCodeName($code,$name,$group)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID where a.intDeleted=0 
		and (a.vcCode like '%$code%' and a.vcName like '%$name%') and  (a.intCategory='$group' or $group=0)
		order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID where a.intDeleted=0 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function CekUoM($UoM)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from muom where vcCode='$UoM'
		");
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetAllUoM()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from muom where intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataAsset()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory, c.vcName as vcAsset
		from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID 
		LEFT JOIN massetclass c on a.intAssetClass=c.intID
		where a.intDeleted=0 and intAsset=1 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataSls()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID where a.intDeleted=0 and a.intSalesItem=1 and a.intActive=1 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataPur()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory
		from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID where a.intDeleted=0 and a.intPurchaseItem=1 and a.intActive=1 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetNetBookValue($item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intNetBook from mitem where intID='$item'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intNetBook;
		}
		else
		{
			return 0;
		}
	}
	function GetListStock($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT b.*,
		c.`vcName` AS vcLocation, a.* FROM mstock a 
		LEFT JOIN mitem b ON a.`intItem`=b.`intID`
		LEFT JOIN mlocation c ON a.`intLocation`=c.intID
		WHERE a.intItem='$id' and b.intDeleted=0 and c.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function Isservicebyid($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intService from mitem where intID='$id'
		");
		$r=$q->row();
		return $r->intService;
	}
	function Isservicebycode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intService from mitem where vcCode='$code'
		");
		$r=$q->row();
		return $r->intService;
	}
	function GetGLAsset($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT b.* FROM mitem a LEFT JOIN massetclass b ON a.`intAssetClass`=b.`intID`
		WHERE a.`intID`='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetUoMAllByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcUoM, vcPurUoM, vcSlsUoM from mitem where vcName='$name'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r;
		}
		else
		{
			return null;
		}
	}
	function GetUoMAllById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcUoM, vcPurUoM, vcSlsUoM from mitem where intID='$id'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r;
		}
		else
		{
			return null;
		}
	}
	function convert_qty($item,$qty,$to,$from)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		(select $to from mitem where intID='$item')/(select $from from mitem where intID='$item')*$qty as hasil
		FROM mitem
		WHERE intID='$item'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->hasil;
		}
		else
		{
			return 0;
		}
	}
	function convert_price($item,$price,$from,$to)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		(select $to from mitem where intID='$item')/(select $from from mitem where intID='$item')*$price as hasil
		FROM mitem
		WHERE intID='$item'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->hasil;
		}
		else
		{
			return 0;
		}
	}
	function GetCodeByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcCode from mitem where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return null;
		}
	}
	function GetNameByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mitem where vcCode='$code' and intDeleted=0
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetNameByBarcode($barcode)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mitem where vcBarCode='$barcode' and intDeleted=0
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetNameById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mitem where intID='$id' and intDeleted=0
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetCodeByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcCode from mitem where intID='$id' and intDeleted=0
		");

		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcCode;
		}
		else
		{
			return null;
		}
	}
	function GetPriceByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intPrice from mitem where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPrice;
		}
		else
		{
			return null;
		}
	}
	function GetIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select intID from mitem where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mitem where vcCode='$code' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetVariantById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mitemvariant where intItem='$id'
		");
		return $q;
	}
	function GetUoMByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcUoM from mitem where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcUoM;
		}
		else
		{
			return null;
		}
	}
	function GetUoMByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcUoM from mitem where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcUoM;
		}
		else
		{
			return null;
		}
	}
	function GetSlsUoMByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcSlsUoM from mitem where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcSlsUoM;
		}
		else
		{
			return null;
		}
	}
	function GetintSlsUoMById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intSlsUoM from mitem where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intSlsUoM;
		}
		else
		{
			return null;
		}
	}
	function GetPurUoMByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select vcPurUoM from mitem where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcPurUoM;
		}
		else
		{
			return null;
		}
	}
	function GetAllCategory()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from mitemcategory a where a.intDeleted=0 order by vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetTopItemByQty($top)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		a.vcItemCode, a.`vcItemName`,
		SUM(a.`intQtyInv`) AS QtyInv,
		a.`vcUoMInv`, c.`blpImage`
		FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		LEFT JOIN mitem c ON a.`intItem`=c.intID
		WHERE MONTH(b.`dtDate`)=2 AND YEAR(b.`dtDate`)=2018
		GROUP BY a.vcItemCode
		ORDER BY SUM(a.`intQtyInv`) DESC
		LIMIT 0,$top
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenTranByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT SUM(a.OpenSOIDR) AS OpenSOIDR,
		SUM(a.OpenSOQty) AS OpenSOQty,
		SUM(a.OpenPOIDR) AS OpenPOIDR,
		SUM(a.OpenPOQty) AS OpenPOQty
		FROM
		(
		SELECT 
		IFNULL(SUM(a.`intOpenQty`/a.`intQty`*(`intLineTotal`- (b.`intDiscPer`/100*a.`intLineTotal`))),0) AS OpenSOIDR,
		0 AS OpenSOQty,
		0 AS OpenPOIDR,
		0 AS OpenPOQty
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.`intID`
		WHERE a.intItem=$id and a.vcStatus='O' and b.vcStatus='O'

		UNION ALL

		SELECT 
		0 AS OpenSOIDR,
		IFNULL(SUM(a.`intOpenQtyInv`),0) AS OpenSOQty,
		0 AS OpenPOIDR,
		0 AS OpenPOQty
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.`intID`
		WHERE a.intItem=$id and a.vcStatus='O' and b.vcStatus='O'

		UNION ALL

		SELECT 
		0 AS OpenSOIDR,
		0 AS OpenSOQty,
		IFNULL(SUM(a.`intOpenQty`/a.`intQty`*(`intLineTotal`- (b.`intDiscPer`/100*a.`intLineTotal`))),0) AS OpenPOIDR,
		0 AS OpenPOQty
		FROM dPO a
		LEFT JOIN hPO b ON a.`intHID`=b.`intID`
		WHERE a.intItem=$id and a.vcStatus='O' and b.vcStatus='O'

		UNION ALL

		SELECT 
		0 AS OpenSOIDR,
		0 AS OpenSOQty,
		0 AS OpenPOIDR,
		IFNULL(SUM(a.`intOpenQtyInv`),0) AS OpenPOQty
		FROM dPO a
		LEFT JOIN hPO b ON a.`intHID`=b.`intID`
		WHERE a.intItem=$id and a.vcStatus='O' and b.vcStatus='O'
		) a
		");
		if($q->num_rows()>0)
		{
		    return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function addvariant($d)
	{
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mitemvariant (intItem,vcName,intPrice)
		select '$d[Id]','$d[VariantName]','$d[VariantPrice]'
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function delvariant($id)
	{
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from mitemvariant where intID='$id'
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Price']=str_replace("'","''",$d['Price']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UoM']=str_replace("'","''",$d['UoM']);
		$d['PurUoM']=str_replace("'","''",$d['PurUoM']);
		$d['intPurUoM']=str_replace("'","''",$d['intPurUoM']);
		$d['SlsUoM']=str_replace("'","''",$d['SlsUoM']);
		$d['intSlsUoM']=str_replace("'","''",$d['intSlsUoM']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Price']=str_replace('"','',$d['Price']);
		$d['Remarks']=str_replace('"','',$d['Remarks']);
		$d['UoM']=str_replace('"','',$d['UoM']);
		$d['PurUoM']=str_replace('"','',$d['PurUoM']);
		$d['intPurUoM']=str_replace('"','',$d['intPurUoM']);
		$d['SlsUoM']=str_replace('"','',$d['SlsUoM']);
		$d['intSlsUoM']=str_replace('"','',$d['intSlsUoM']);
		
		$d['Name']=str_replace('&','and',$d['Name']);
		
		if(!isset($d['Barcode']))
		{
			$d['Barcode']='';
		}
		else
		{
			$d['Barcode']=str_replace("'","''",$d['Barcode']);
		}
		
		if(!isset($d['intBatch']))
		{
			$d['intBatch']=0;
		}
		else
		{
			$d['intBatch']=str_replace("'","''",$d['intBatch']);
		}
		
		if(!isset($d['intAsset']))
		{
			$d['intAsset']=0;
		}
		else
		{
			$d['intAsset']=str_replace("'","''",$d['intAsset']);
		}
		
		if(!isset($d['intService']))
		{
			$d['intService']=0;
		}
		else
		{
			$d['intService']=str_replace("'","''",$d['intService']);
		}
		
		//cek kode kembar
		$qcek=$this->db->query("select intID from mitem where vcCode='$d[Code]'");
		if($qcek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			if($d['imgData']!=null)// proses membuat thumnail
			{
				list($width, $height) = getimagesize($d['imgDataAsli']);
				
				$newwidth = 100;
				$newheight = 100;
				
				$thumb = imagecreatetruecolor($newwidth, $newheight);
				
				$ext = pathinfo($d['imgDataName'], PATHINFO_EXTENSION);
				if($ext=='jpeg' or $ext=='jpg')
				{
					$source = imagecreatefromjpeg($d['imgDataAsli']);
				}
				else if($ext=='png')
				{
					$source = imagecreatefrompng($d['imgDataAsli']);
				}
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				
				ob_start(); // Start capturing stdout.
				imagejpeg($thumb); // As though output to browser.
				$sBinaryThumbnail = ob_get_contents(); // the raw jpeg image data.
				ob_end_clean(); // Dump the stdout so it does not screw other output.
				
				$sBinaryThumbnail = addslashes($sBinaryThumbnail);
			}
			else
			{
				$sBinaryThumbnail=null;
			}
			
			
			$q=$this->db->query("insert into mitem (intCategory,vcCode,vcName,intPrice,intTax,vcUoM,vcPurUoM,intPurUoM,vcSlsUoM,intSlsUoM,vcRemarks,blpImage,blpImageMin,
			intSalesItem,intPurchaseItem,intSalesWithoutQty,dtInsertTime, vcBarCode, intAsset, intBatch,intService, vcUser)
			select '$d[Category]','$d[Code]','$d[Name]','$d[Price]','$d[Tax]','$d[UoM]','$d[PurUoM]','$d[intPurUoM]',
			'$d[SlsUoM]','$d[intSlsUoM]','$d[Remarks]','$d[imgData]','$sBinaryThumbnail','$d[intSalesItem]','$d[intPurchaseItem]','$d[intSalesWithoutQty]','$now','$d[Barcode]'
			,'$d[intAsset]' ,'$d[intBatch]' ,'$d[intService]' ,'$d[UserID]' 
			");
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			if($q)
			{
				$this->load->model('m_stock', 'stock');
				$this->stock->addItem($idtin);
				
				$this->load->model('m_price', 'price');
				$this->price->addItem($idtin);
				return $idtin;
			}
			else
			{
				return 0;
			}
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['LastCode']=str_replace("'","''",$d['LastCode']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Price']=str_replace("'","''",$d['Price']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UoM']=str_replace("'","''",$d['UoM']);
		$d['PurUoM']=str_replace("'","''",$d['PurUoM']);
		$d['intPurUoM']=str_replace("'","''",$d['intPurUoM']);
		$d['SlsUoM']=str_replace("'","''",$d['SlsUoM']);
		$d['intSlsUoM']=str_replace("'","''",$d['intSlsUoM']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Price']=str_replace('"','',$d['Price']);
		$d['Remarks']=str_replace('"','',$d['Remarks']);
		$d['UoM']=str_replace('"','',$d['UoM']);
		$d['PurUoM']=str_replace('"','',$d['PurUoM']);
		$d['intPurUoM']=str_replace('"','',$d['intPurUoM']);
		$d['SlsUoM']=str_replace('"','',$d['SlsUoM']);
		$d['intSlsUoM']=str_replace('"','',$d['intSlsUoM']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mitem';
		$his['doc']			= 'ITM';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		
        if(!isset($d['Barcode']))
		{
			$d['Barcode']='';
		}
		else
		{
			$d['Barcode']=str_replace("'","''",$d['Barcode']);
		}  
		
		//cek kode kembar
		$qcek=$this->db->query("select intID from mitem where vcCode='$d[Code]' and vcCode!='$d[LastCode]'");
		if($qcek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			if($d['imgData']!=null)
			{
				list($width, $height) = getimagesize($d['imgDataAsli']);
				
				$newwidth = 100;
				$newheight = 100;
				
				$thumb = imagecreatetruecolor($newwidth, $newheight);
				$ext = pathinfo($d['imgDataName'], PATHINFO_EXTENSION);
				
				if($ext=='jpeg' or $ext=='jpg')
				{
					$source = imagecreatefromjpeg($d['imgDataAsli']);
				}
				else if($ext=='png')
				{
					$source = imagecreatefrompng($d['imgDataAsli']);
				}
				
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				
				ob_start(); // Start capturing stdout.
				imagejpeg($thumb); // As though output to browser.
				$sBinaryThumbnail = ob_get_contents(); // the raw jpeg image data.
				ob_end_clean(); // Dump the stdout so it does not screw other output.
				
				$sBinaryThumbnail = addslashes($sBinaryThumbnail);
				
				$q=$this->db->query("update mitem set intCategory='$d[Category]', vcCode='$d[Code]', 
				vcName='$d[Name]', intPrice='$d[Price]', intTax='$d[Tax]', vcUoM='$d[UoM]',vcPurUoM='$d[PurUoM]', intPurUoM='$d[intPurUoM]',
				vcSlsUoM='$d[SlsUoM]', intSlsUoM='$d[intSlsUoM]',vcRemarks='$d[Remarks]'
				,intSalesItem='$d[intSalesItem]',intPurchaseItem='$d[intPurchaseItem]',intSalesWithoutQty='$d[intSalesWithoutQty]',
				blpImage='$d[imgData]',blpImageMin='$sBinaryThumbnail',intActive='$d[intActive]', vcBarCode='$d[Barcode]' where intID='$d[id]'");
			}
			else
			{
				$q=$this->db->query("update mitem set intCategory='$d[Category]', vcCode='$d[Code]', 
				vcName='$d[Name]', intPrice='$d[Price]', intTax='$d[Tax]', vcUoM='$d[UoM]',vcPurUoM='$d[PurUoM]', intPurUoM='$d[intPurUoM]',
				vcSlsUoM='$d[SlsUoM]', intSlsUoM='$d[intSlsUoM]',vcRemarks='$d[Remarks]',
				intSalesItem='$d[intSalesItem]',intPurchaseItem='$d[intPurchaseItem]',intSalesWithoutQty='$d[intSalesWithoutQty]',intActive='$d[intActive]', vcBarCode='$d[Barcode]'
				where intID='$d[id]'");
				
				$qcek2=$this->db->query("select * from mitem where intID='$d[id]'");
				$rcek=$qcek2->row();
				if($rcek->blpImage!=null)
				{
					$gmbrasli=$rcek->blpImage;
					
					$newwidth = 100;
					$newheight = 100;
					
					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$source = imagecreatefromstring($gmbrasli);
					$uri = 'data://application/octet-stream;base64,'  . base64_encode($gmbrasli);
					list($width, $height) = getimagesize($uri);
					imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					
					ob_start(); // Start capturing stdout.
					imagejpeg($thumb); // As though output to browser.
					$sBinaryThumbnail = ob_get_contents(); // the raw jpeg image data.
					ob_end_clean(); // Dump the stdout so it does not screw other output.
					
					$sBinaryThumbnail = addslashes($sBinaryThumbnail);
					$this->db->query("update mitem set blpImageMin='$sBinaryThumbnail'
					where intID='$d[id]'");
				}
			}
			if(isset($d['LastName']))
			{
				$d['LastName']=str_replace("'","''",$d['LastName']);
				if($d['LastName']!=$d['Name'])
				{
					$qgettable = $this->db->query("SELECT DISTINCT TABLE_NAME 
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE COLUMN_NAME IN ('vcItemName') ORDER BY TABLE_NAME ASC");
					
					foreach($qgettable->result() as $rrrr) 
					{
						$tablename = $rrrr->TABLE_NAME;
						if($tablename!='dAR2'){
							$this->db->query("update $tablename set vcItemName='$d[Name]' where vcItemCode='$d[Code]'");
						}
					}
				}
			}
			//HISTORY
			$dataafter			= $this->history->getdatabyid($his); // get data after
			$this->history->createhistory($his,$databefore,$dataafter); // create history
			//HISTORY
			if($q)
			{
			  return 1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcCategory from mitem a LEFT JOIN mitemcategory b on a.intCategory=b.intID
		where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mitem set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateAPC($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intAPC=intAPC + $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateNetBook($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intNetBook=intNetBook + $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateDep($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intOrdinaryDep=intOrdinaryDep + $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateRev($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intReval=intReval + $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateAPCX($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intAPC=intAPC - $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function UpdateNetBookX($id,$val)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mitem set intNetBook=intNetBook - $val where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function activeornot($code)
	{
		$db=$this->load->database('default', TRUE);
		$item=$this->db->query("select intActive from mitem where vcCode='$code'");
		
		$ritem=$item->row()->intActive;
		
		if($ritem==0)
		{
			$sitem=1;
		}
		if($ritem==1)
		{
			$sitem=0;
		}
		
		$q=$this->db->query("update mitem set intActive=$sitem where vcCode='$code'");
		
	}
	function cekbatch($id)
	{
		$db=$this->load->database('default', TRUE);
		$item=$this->db->query("select intBatch from mitem where intID='$id'");
		if($item->num_rows()>0)
		{
			$ritem=$item->row()->intBatch;
			return $ritem;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */