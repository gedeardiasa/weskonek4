<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_item_category extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mitemcategory a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataAsset()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
b.intID, b.`vcCode`, b.`vcName`
FROM mitem a
LEFT JOIN mitemcategory b ON a.`intCategory`=b.`intID`
WHERE a.`intAsset`=1
GROUP BY b.`intID`
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataSls()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mitemcategory a where a.intDeleted=0 
		and intID in
		(
			select intCategory from mitem where intSalesItem=1 and intDeleted=0 and intActive=1
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataPur()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mitemcategory a where a.intDeleted=0 
		and intID in
		(
			select intCategory from mitem where intPurchaseItem=1 and intDeleted=0 and intActive=1
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mitemcategory a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mitemcategory where vcCode='$code' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->intID;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountByItemCode($code) // get account inventory
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcInvAccount 
		FROM mitemcategory a LEFT JOIN mitem b ON a.intID=b.intCategory
		WHERE b.vcCode='$code' and a.intDeleted=0 and b.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcInvAccount;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountHppByItemCode($code) // get account inventory
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcHppAccount 
		FROM mitemcategory a LEFT JOIN mitem b ON a.intID=b.intCategory
		WHERE b.vcCode='$code' and a.intDeleted=0 and b.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcHppAccount;
		}
		else
		{
			return 0;
		}
	}
	function GetAccountRevByItemCode($code) // get account inventory
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcRevAccount 
		FROM mitemcategory a LEFT JOIN mitem b ON a.intID=b.intCategory
		WHERE b.vcCode='$code' and a.intDeleted=0 and b.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcRevAccount;
		}
		else
		{
			return 99;
		}
	}
	function GetAccountRetByItemCode($code) // get account inventory
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.vcRetAccount 
		FROM mitemcategory a LEFT JOIN mitem b ON a.intID=b.intCategory
		WHERE b.vcCode='$code' and a.intDeleted=0 and b.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->vcRetAccount;
		}
		else
		{
			return 0;
		}
	}
	
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("insert into mitemcategory (vcCode,vcName,dtInsertTime,vcInvAccount,vcHppAccount,vcRevAccount,vcRetAccount,
		vcDefBaseUoM,vcDefPurUoM,intDefPurNum,vcDefSlsUoM,intDefSlsNum,intDefSlsItem,intDefPurItem,intDefSalesWithoutQty,intDefAsset,intDefBatch)
		values ('$d[Code]','$d[Name]','$now','$d[AcctInv]','$d[AcctHpp]','$d[AcctRev]','$d[AcctRet]'
		,'$d[UoM]','$d[PurUoM]','$d[intPurUoM]','$d[SlsUoM]','$d[intSlsUoM]'
		,'$d[intSalesItem]','$d[intPurchaseItem]','$d[intSalesWithoutQty]','$d[intAsset]','$d[intBatch]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mitemcategory';
		$his['doc']			= 'ITMCATEGORY';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mitemcategory set vcName='$d[Name]', vcCode='$d[Code]', vcInvAccount='$d[AcctInv]', vcHppAccount='$d[AcctHpp]', 
		vcRevAccount='$d[AcctRev]', vcRetAccount='$d[AcctRet]'
		, vcDefBaseUoM='$d[UoM]'
		, vcDefPurUoM='$d[PurUoM]'
		, intDefPurNum='$d[intPurUoM]'
		, vcDefSlsUoM='$d[SlsUoM]'
		, intDefSlsNum='$d[intSlsUoM]'
		
		, intDefSlsItem='$d[intSalesItem]'
		, intDefPurItem='$d[intPurchaseItem]'
		, intDefSalesWithoutQty='$d[intSalesWithoutQty]'
		, intDefAsset='$d[intAsset]'
		, intDefBatch='$d[intBatch]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mitemcategory set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */