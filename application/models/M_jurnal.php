<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jurnal extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hJE a 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithDate($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select a.*
		from hJE a 
		where a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hJE where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dJE a 
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailJurnalByRefDocNum($docnum,$type)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("	SELECT 
		a.*, b.*
		FROM hJE a
		LEFT JOIN dJE b ON a.`intID`=b.`intHID`
		WHERE vcRef='$docnum' AND vcRefType='$type'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderJurnalByRefDocNum($docnum,$type)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("	SELECT 
		a.*
		FROM hJE a
		WHERE vcRef='$docnum' AND vcRefType='$type'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetHeaderJurnalByRefDocNumR($docnum,$type)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("	SELECT 
		a.*
		FROM hJE a
		WHERE vcRef='$docnum' AND vcRefType='$type'
		");
		return $q;
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hJE');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hJE (intPlan, vcDocNum,dtDate,vcRef,vcRefType,vcRemarks,vcUser,dtInsertTime)
		values ('$d[Plan]','$d[DocNum]','$d[DocDate]','$d[RefNum]','$d[RefType]',
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function deleteH($id)
	{
		$q=$this->db->query("delete from hJE where intID='$id'
		");
		
	}
	
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['GLCodeJE']=str_replace("'","''",$d['GLCodeJE']);
		$d['GLNameJE']=str_replace("'","''",$d['GLNameJE']);
		$d['GLCodeJEX']=str_replace("'","''",$d['GLCodeJEX']);
		$d['GLNameJEX']=str_replace("'","''",$d['GLNameJEX']);
		$q=$this->db->query("insert into dJE (intHID,vcGLCode,vcGLName,vcDC,vcGLCodeX,vcGLNameX,intValue)
		values ('$d[intHID]','$d[GLCodeJE]','$d[GLNameJE]','$d[DCJE]','$d[GLCodeJEX]','$d[GLNameJEX]','$d[ValueJE]')
		");
		
		$nowdt 	= date('Y-m-d');
		if($q)
		{
			// create mutation
			$qdtdate = $this->db->query("select dtDate from hJE where intID='$d[intHID]'
			");
			$rdtdate = $qdtdate->row();
			$dtdate	 = $rdtdate->dtDate;// date mutasi ambil dari date header jurnal
			
			if($dtdate<$nowdt) // jika backdate maka jam insert wajib di akhir jam pada hari tersebut
			{
				$now=date('Y-m-d 23:59:59');
			}
			else
			{
				$now=date('Y-m-d H:i:s');
			}
			
			$minvalue = $d['ValueJE']*-1;
			if($d['GLCodeJE']!='')
			{
				$qlb=$this->db->query("select intBalance from mgl where vcCode='$d[GLCodeJE]'
				");
				$rlb=$qlb->row();
				
				if($dtdate<$nowdt)// jika backdate maka last cost adalah cost terkahir dikurangi mutasi mulai tgl backdate sampai sekarang
				{
					$lastbalance = $rlb->intBalance;
					$qgetlast = $this->db->query("
					select SUM(intValue) as value 
					from dMutationJE where vcGLCode='$d[GLCodeJE]' and dtDate>'$dtdate'");
					if($qgetlast->num_rows()>0)
					{
						$rgetlast = $qgetlast->row();
						$hgetlast = $rgetlast->value;
					}
					else
					{
						$hgetlast = 0;
					}
					$lastbalance = $lastbalance - $hgetlast;
				}
				else
				{
					$lastbalance = $rlb->intBalance;
				}
				
				$this->db->query("insert into dMutationJE (dtDate,intHID,vcGLCode,intValue,intLastValue,dtInsertTime)
				values ('$dtdate','$d[intHID]','$d[GLCodeJE]','$d[ValueJE]','$lastbalance','$now')
				");
				//cek jika backdate maka ubah intLastValue tgl sesudahnya menjadi intLastValue + val
				if($dtdate<$nowdt)
				{
					$q7=$this->db->query("update dMutationJE set intLastValue=intLastValue+$d[ValueJE] where vcGLCode='$d[GLCodeJE]' and dtDate>'$dtdate'
					");
				}
				//
			}
			if($d['GLCodeJEX']!='')
			{
				$qlb=$this->db->query("select intBalance from mgl where vcCode='$d[GLCodeJEX]'
				");
				$rlb=$qlb->row();
				
				if($dtdate<$nowdt)// jika backdate maka last cost adalah cost terkahir dikurangi mutasi mulai tgl backdate sampai sekarang
				{
					$lastbalance = $rlb->intBalance;
					$qgetlast = $this->db->query("
					select SUM(intValue) as value 
					from dMutationJE where vcGLCode='$d[GLCodeJEX]' and dtDate>'$dtdate'");
					if($qgetlast->num_rows()>0)
					{
						$rgetlast = $qgetlast->row();
						$hgetlast = $rgetlast->value;
					}
					else
					{
						$hgetlast = 0;
					}
					$lastbalance = $lastbalance - $hgetlast;
				}
				else
				{
					$lastbalance = $rlb->intBalance;
				}
				
				$this->db->query("insert into dMutationJE (dtDate,intHID,vcGLCode,intValue,intLastValue,dtInsertTime)
				values ('$dtdate','$d[intHID]','$d[GLCodeJEX]','$minvalue','$lastbalance','$now')
				");
				//cek jika backdate maka ubah intLastValue tgl sesudahnya menjadi intLastValue + val
				if($dtdate<$nowdt)
				{
					$q7=$this->db->query("update dMutationJE set intLastValue=intLastValue+$minvalue where vcGLCode='$d[GLCodeJEX]' and dtDate>'$dtdate'
					");
				}
				//
			}
			
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function jurnalexternal($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hJE');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hJE (vcDocNum,dtDate,vcRef,vcRefType,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]','$d[RefNum]','$d[RefType]',
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		
		
		$d['DCJE']='D';
		
		$this->db->query("insert into dJE (intHID,vcGLCode,vcGLName,vcDC,vcGLCodeX,vcGLNameX,intValue)
		values ('$didtin','$d[GLCodeJE]','$d[GLNameJE]','$d[DCJE]','$d[GLCodeJEX]','$d[GLNameJEX]','$d[ValueJE]')
		");
		
			// create mutation
			$dtdate	 = $d['DocDate'];
			$minvalue = $d['ValueJE']*-1;
			$this->db->query("insert into dMutationJE (dtDate,intHID,vcGLCode,intValue,dtInsertTime)
			values ('$dtdate','$idtin','$d[GLCodeJE]','$d[ValueJE]','$now')
			");
			
			$this->db->query("insert into dMutationJE (dtDate,intHID,vcGLCode,intValue,dtInsertTime)
			values ('$dtdate','$idtin','$d[GLCodeJEX]','$minvalue','$now')
			");
		
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */