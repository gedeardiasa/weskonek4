<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_location extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcPlan from mlocation a left join mplan b on a.intPlan=b.intID where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, b.vcName AS vcPlan, c.`intUserID`
		FROM mlocation a 
		LEFT JOIN mplan b ON a.intPlan=b.intID
		LEFT JOIN maccessplan c ON a.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE a.intDeleted=0 AND c.`intUserID`='$iduser' ORDER BY a.intID DESC 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCountry()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mcountry a where a.intDeleted=0  order by a.vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mlocation a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function getByNameandAccessPlan($name,$iduser)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select a.* from mlocation a
		LEFT JOIN mplan b ON a.intPlan=b.intID
		LEFT JOIN maccessplan c ON a.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where a.intDeleted=0 and a.vcName='$name'  AND c.`intUserID`='$iduser'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return '';
		}
	}
	function GetIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select intID from mlocation where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mlocation where vcCode='$code' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function GetPlanIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$name=str_replace("'","''",$name);
		$q=$this->db->query("select intPlan from mlocation where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPlan;
		}
		else
		{
			return null;
		}
	}
	function GetNameByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mlocation where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetIDPlanByLocationId($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.intID as intID from mplan a
		LEFT JOIN mlocation b on a.intID=b.intPlan where b.intID='$id' and b.intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Address']=str_replace("'","''",$d['Address']);
		$d['City']=str_replace("'","''",$d['City']);
		$d['State']=str_replace("'","''",$d['State']);
		$d['Country']=str_replace("'","''",$d['Country']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Address']=str_replace('"','',$d['Address']);
		$d['City']=str_replace('"','',$d['City']);
		$d['State']=str_replace('"','',$d['State']);
		$d['Country']=str_replace('"','',$d['Country']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mlocation (vcCode,vcName,intPlan,vcAddress,vcCity,vcState,vcCountry,dtInsertTime)
		values ('$d[Code]','$d[Name]','$d[Plan]','$d[Address]','$d[City]','$d[State]','$d[Country]','$now')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
			$this->load->model('m_stock', 'stock');
			$this->stock->addLoc($idtin);
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mlocation';
		$his['doc']			= 'LOCATION';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Address']=str_replace("'","''",$d['Address']);
		$d['City']=str_replace("'","''",$d['City']);
		$d['State']=str_replace("'","''",$d['State']);
		$d['Country']=str_replace("'","''",$d['Country']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Address']=str_replace('"','',$d['Address']);
		$d['City']=str_replace('"','',$d['City']);
		$d['State']=str_replace('"','',$d['State']);
		$d['Country']=str_replace('"','',$d['Country']);
		$q=$this->db->query("update mlocation set vcCode='$d[Code]',vcName='$d[Name]',intPlan='$d[Plan]',vcAddress='$d[Address]'
		,vcCity='$d[City]',vcState='$d[State]',vcCountry='$d[Country]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mlocation set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."'), vcCode=CONCAT(vcCode,'X') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */