<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_outpay extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hOUTPAY a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intID
			FROM hOUTPAY a
			LEFT JOIN 
			(
				mwallet b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intWallet`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hOUTPAY a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hOUTPAY a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			
			return 99;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dOUTPAY a 
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hOUTPAY a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intID
			FROM hOUTPAY a
			LEFT JOIN 
			(
				mwallet b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intWallet`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDateDocInv($doc,$table)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select dtDate from $table where vcDocNum='$doc'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->dtDate;
		}
		else
		{
			return '';
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hOUTPAY');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hOUTPAY (intWallet,intBP,vcBPCode,vcBPName,vcDocNum,dtDate,vcRef,vcStatus,
		vcRemarks,intAmount,intApplied,intBalance,vcUser,dtInsertTime)
		values ('$d[Wallet]','$d[BPId]','$d[BPCode]','$d[BPName]','$d[DocNum]','$d[DocDate]',
		'$d[RefNum]','C','$d[Remarks]','$d[DocTotalBefore]','$d[AppliedAmount]','$d[BalanceDue]',
		'$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['vcDocNum']=str_replace("'","''",$d['vcDocNum']);
		$d['vcBaseType']=str_replace("'","''",$d['vcBaseType']);
		
		
		$q=$this->db->query("insert into dOUTPAY (intHID,intBaseRef,vcBaseType,vcDocNum,intDocTotal,intApplied)
		values ('$d[intHID]','$d[intBaseRef]','$d[vcBaseType]','$d[vcDocNum]','$d[intDocTotal]','$d[intApplied]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hOUTPAY set vcStatus='X' where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */