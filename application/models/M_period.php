<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_period extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select *, case when vcStatus='U' then 'Unlocked' when vcStatus='L' then 'Locked'
		else 'Unknown' end as StatusName
		from mperiod where intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mperiod where intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$this->db->trans_begin();
		
		$cekq=$this->db->query("select * from mperiod where intYear='$d[Year]'
		");
		
		if($cekq->num_rows()>0)
		{
			return 0;
		}
		else
		{
			for($i=1;$i<=12;$i++)
			{
				$q=$this->db->query("insert into mperiod (vcName,intYear,intMonth, vcStatus)
				values ('$d[Name]','$d[Year]','$i','L')
				");
			}
			$this->db->trans_complete();
			
			if($this->db->trans_status() === FALSE)
			{
				$ok=0;
				$this->db->trans_rollback();
			}else{
				$ok=1;
				$this->db->trans_commit();
			}
			
			if($ok==1)
			{
			  return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Status']=str_replace("'","''",$d['Status']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		
		$this->db->trans_begin();
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mperiod';
		$his['doc']			= 'PERIOD';
		$his['key']			= "intID=$d[ID]";
		$his['id']			= $d['ID'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mperiod set vcStatus='$d[Status]' where intID='$d[ID]'");
		$q2=$this->db->query("update mperiod set vcName='$d[Name]' where intYear='$d[Year]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$ok=0;
			$this->db->trans_rollback();
		}else{
			$ok=1;
			$this->db->trans_commit();
		}
		
		if($ok==1)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function cekperiod($date)
	{
		$year=date('Y',strtotime($date));
		$month=date('n',strtotime($date));
		$q=$this->db->query("select vcStatus from mperiod where intYear='$year' and intMonth='$month'");
		if($q->num_rows()>0)
		{
			$r=$q->row()->vcStatus;
			if($r=='U')
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */