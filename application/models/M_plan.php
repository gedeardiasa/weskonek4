<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_plan extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mplan a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT b.*, c.`intUserID`
		FROM mplan b
		LEFT JOIN maccessplan c ON b.`intID`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE b.intDeleted=0 AND c.`intUserID`='$iduser' ORDER BY b.intID DESC 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCountry()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mcountry a where a.intDeleted=0  order by a.vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllState()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mstate a where a.intDeleted=0  order by a.vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCity()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mcity a where a.intDeleted=0  order by a.vcName asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mplan a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mplan where vcName='$name' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	
	function cekishaveallaccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT intID FROM mplan WHERE intID NOT IN
		(
		SELECT intPlan FROM maccessplan WHERE intUserID=$iduser
		) AND intDeleted=0
		");
		if($q->num_rows()>0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Address']=str_replace("'","''",$d['Address']);
		$d['City']=str_replace("'","''",$d['City']);
		$d['State']=str_replace("'","''",$d['State']);
		$d['Country']=str_replace("'","''",$d['Country']);
		$d['Telp']=str_replace("'","''",$d['Telp']);
		$d['Fax']=str_replace("'","''",$d['Fax']);
		$d['Email']=str_replace("'","''",$d['Email']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Address']=str_replace('"','',$d['Address']);
		$d['City']=str_replace('"','',$d['City']);
		$d['State']=str_replace('"','',$d['State']);
		$d['Country']=str_replace('"','',$d['Country']);
		$d['Telp']=str_replace('"','',$d['Telp']);
		$d['Fax']=str_replace('"','',$d['Fax']);
		$d['Email']=str_replace('"','',$d['Email']);
		
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mplan (vcCode,vcName,vcAddress,vcCity,vcState,vcCountry,dtInsertTime,vcTelp,vcFax,vcEmail)
		values ('$d[Code]','$d[Name]','$d[Address]','$d[City]','$d[State]','$d[Country]','$now','$d[Telp]','$d[Fax]','$d[Email]')
		");
		//$id=$this->db->query("select LAST_INSERT_ID() as intID");
		//$rid=$id->row();
		//$idtin=$rid->intID;
		if($q)
		{
			//$this->load->model('m_stock', 'stock');
			//$this->stock->addLoc($idtin);
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Address']=str_replace("'","''",$d['Address']);
		$d['City']=str_replace("'","''",$d['City']);
		$d['State']=str_replace("'","''",$d['State']);
		$d['Country']=str_replace("'","''",$d['Country']);
		$d['Telp']=str_replace("'","''",$d['Telp']);
		$d['Fax']=str_replace("'","''",$d['Fax']);
		$d['Email']=str_replace("'","''",$d['Email']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$d['Address']=str_replace('"','',$d['Address']);
		$d['City']=str_replace('"','',$d['City']);
		$d['State']=str_replace('"','',$d['State']);
		$d['Country']=str_replace('"','',$d['Country']);
		$d['Telp']=str_replace('"','',$d['Telp']);
		$d['Fax']=str_replace('"','',$d['Fax']);
		$d['Email']=str_replace('"','',$d['Email']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mplan';
		$his['doc']			= 'PLAN';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		if($d['imgData']!=null)
		{
			$q=$this->db->query("update mplan set vcCode='$d[Code]',vcName='$d[Name]',vcAddress='$d[Address]'
			,vcCity='$d[City]',vcState='$d[State]',vcCountry='$d[Country]',
			vcTelp='$d[Telp]',
			vcFax='$d[Fax]',
			vcEmail='$d[Email]',
			blpImage='$d[imgData]'
			where intID='$d[id]'");
		}
		else
		{
			$q=$this->db->query("update mplan set vcCode='$d[Code]',vcName='$d[Name]',vcAddress='$d[Address]'
			,vcCity='$d[City]',vcState='$d[State]',vcCountry='$d[Country]',
			vcTelp='$d[Telp]',
			vcFax='$d[Fax]',
			vcEmail='$d[Email]'
			where intID='$d[id]'");
		}
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mplan set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* plan: ./application/models/validasi.php */