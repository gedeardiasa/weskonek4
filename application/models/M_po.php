<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_po extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDateAndDocNumRead($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]' and
		a.vcDocGroup in
		(
			SELECT DISTINCT(a.vcCode) FROM mdocgroup a
			LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
			LEFT JOIN muser c ON a.`vcUserID`=c.`vcUserID`
			WHERE b.vcCode='PO' AND c.`intID`='$iduser' AND a.`intRead`=1
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessNotService($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O' and a.intService=0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessAndTable($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName, c.vcName as TableName
		from hPO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN mtable c on a.intTable=c.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetWhsNameByHeader($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select distinct(vcLocation) as data from dPO where intHID='$id' limit 0,1
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->data;
		}
		else
		{
			return "";
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dPO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hPO c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPO a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPO a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		//cek approval
		$cekapproval 	= $this->db->query("select * from mdoc where vcCode='PO'
		");
		$rapv			= $cekapproval->row();
		
		if($rapv->intApv==1) // jika butuh aproval maka status document pending
		{
			$d['Status']='P';
		}
		else //jika tidak maka status document Open
		{
			$d['Status']='O';
		}
		//end cek approval
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hPO');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcBillToCity;
		$Country = $adrs->vcBillToCountry;
		$AddressH = $adrs->vcBillToAddress;
		
		$q=$this->db->query("insert into hPO (intTable,intBP,intService,vcBPCode,vcBPName,vcSalesName,vcDocNum,vcDocGroup,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[Table]','$d[BPId]','$d[Service]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocGroup]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','$d[Status]','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPO set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dPO set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPO set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$itemname,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		if($item!=0)// tipe item
		{
			$q=$this->db->query("
			update dPO set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
			");
		}
		else//tipe service
		{
			$q=$this->db->query("
			update dPO set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and vcItemName='$itemname'
			");

		}
	}
	function cekclose($id,$item,$qty,$qtyinv,$itemname='')
	{
		$db=$this->load->database('default', TRUE);
		
		if($item!=0)
		{
			$this->db->query("
			update dPO set
			intOpenQty=intOpenQty-$qty,
			intOpenQtyInv=intOpenQtyInv-$qtyinv
			where intHID='$id' and intItem='$item'
			");// query update open qty PO
		}
		else
		{
			$this->db->query("
			update dPO set
			intOpenQty=intOpenQty-$qty,
			intOpenQtyInv=intOpenQtyInv-$qtyinv
			where intHID='$id' and vcItemName='$itemname'
			");// query update open qty PO
		}
		
		$this->db->query("
		update dPO set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dPO
		
		$cekdetailsq=$this->db->query("
		select intID from dPO where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dPO yang statusnya open
		{
			$this->db->query("
			update hPO set
			vcStatus='C'
			where intID='$id'
			");// maka update hPO status ke 'C' (Close)
		}
	}
	function apvH($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		
		update hPO set
		vcStatus='O'
		where intID='$d[id]'
		
		");
		
		
		$q2=$this->db->query("
		
		update dPO set
		vcStatus='O'
		where intHID='$d[id]'
		
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPO';
		$his['doc']			= 'PO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hPO set
		intTable='$d[Table]',
		vcDocGroup='$d[DocGroup]',
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePO']=str_replace("'","''",$d['itemcodePO']);
		$d['itemnamePO']=str_replace("'","''",$d['itemnamePO']);
		
		$d['uomPO']=str_replace("'","''",$d['uomPO']);
		$d['uominvPO']=str_replace("'","''",$d['uominvPO']);
		
		//cek approval
		$cekapproval 	= $this->db->query("select * from mdoc where vcCode='PP'
		");
		$rapv			= $cekapproval->row();
		
		if($rapv->intApv==1) // jika butuh aproval maka status document pending
		{
			$d['Status']='P';
		}
		else //jika tidak maka status document Open
		{
			$d['Status']='O';
		}
		//end cek approval
		
		
		$q=$this->db->query("insert into dPO (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intLocation,vcLocation,vcStatus)
		values ('$d[intHID]','$d[idBaseRefPO]','$d[BaseRefPO]','$d[itemID]','$d[itemcodePO]','$d[itemnamePO]','$d[qtyPO]','$d[qtyPO]','$d[uomPO]',
		'$d[uomtypePO]','$d[qtyinvPO]','$d[qtyinvPO]','$d[uominvPO]','$d[pricePO]','$d[discperPO]','$d[discPO]','$d[priceafterPO]',
		'$d[linetotalPO]','$d[whsPO]','$d[whsNamePO]','$d[Status]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePO']=str_replace("'","''",$d['itemcodePO']);
		$d['itemnamePO']=str_replace("'","''",$d['itemnamePO']);
		
		$d['uomPO']=str_replace("'","''",$d['uomPO']);
		$d['uominvPO']=str_replace("'","''",$d['uominvPO']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dPO';
		$his['doc']			= 'PO';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dPO where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodePO'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dPO set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodePO]',
		vcItemName='$d[itemnamePO]',
		intQty='$d[qtyPO]',
		intOpenQty='$d[qtyPO]',
		vcUoM='$d[uomPO]',
		intUoMType='$d[uomtypePO]',
		intQtyInv='$d[qtyinvPO]',
		intOpenQtyInv='$d[qtyinvPO]',
		vcUoMInv='$d[uominvPO]',
		intPrice='$d[pricePO]',
		intDiscPer='$d[discperPO]',
		intDisc='$d[discPO]',
		intPriceAfterDisc='$d[priceafterPO]',
		intLineTotal='$d[linetotalPO]',
		intLocation='$d[whsPO]',
		vcLocation='$d[whsNamePO]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dPO where intID='$id'
		");
	}
	function deleteD2($id,$notdelitem)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dPO where intHID='$id' and intItem not in $notdelitem
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */