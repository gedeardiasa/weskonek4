<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hPP a
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPendingDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='P'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessNotService($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O' and a.intService=0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessAndTable($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hPP a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPP a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetWhsNameByHeader($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select distinct(vcLocation) as data from dPP where intHID='$id' limit 0,1
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->data;
		}
		else
		{
			return "";
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dPP a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hPP c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPP a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPP a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		//cek approval
		$cekapproval 	= $this->db->query("select * from mdoc where vcCode='PP'
		");
		$rapv			= $cekapproval->row();
		
		if($rapv->intApv==1) // jika butuh aproval maka status document pending
		{
			$d['Status']='P';
		}
		else //jika tidak maka status document Open
		{
			$d['Status']='O';
		}
		//end cek approval
		
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hPP');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hPP (intService,vcDocNum,dtDate,  
		dtDelDate,vcRef,vcStatus,
		vcRemarks,vcUser,dtInsertTime  )
		values ('$d[Service]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Status]',
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPP set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dPP set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPP set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$itemname,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		if($item!=0)// tipe item
		{
			$q=$this->db->query("
			update dPP set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
			");
		}
		else//tipe service
		{
			$q=$this->db->query("
			update dPP set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and vcItemName='$itemname'
			");

		}
	}
	function cekclose($id,$item,$qty,$qtyinv,$itemname='')
	{
		$db=$this->load->database('default', TRUE);
		
		if($item!=0)
		{
			$this->db->query("
			update dPP set
			intOpenQty=intOpenQty-$qty,
			intOpenQtyInv=intOpenQtyInv-$qtyinv
			where intHID='$id' and intItem='$item'
			");// query update open qty PO
		}
		else
		{
			$this->db->query("
			update dPP set
			intOpenQty=intOpenQty-$qty,
			intOpenQtyInv=intOpenQtyInv-$qtyinv
			where intHID='$id' and vcItemName='$itemname'
			");// query update open qty PO
		}
		
		$this->db->query("
		update dPP set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dPP
		
		$cekdetailsq=$this->db->query("
		select intID from dPP where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dPP yang statusnya open
		{
			$this->db->query("
			update hPP set
			vcStatus='C'
			where intID='$id'
			");// maka update hPP status ke 'C' (Close)
		}
	}
	function apvH($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		
		update hPP set
		vcStatus='O'
		where intID='$d[id]'
		
		");
		
		$q2=$this->db->query("
		
		update dPP set
		vcStatus='O'
		where intHID='$d[id]'
		
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPP';
		$his['doc']			= 'PP';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("
		
		update hPP set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcRemarks='$d[Remarks]'
		
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePP']=str_replace("'","''",$d['itemcodePP']);
		$d['itemnamePP']=str_replace("'","''",$d['itemnamePP']);
		
		$d['uomPP']=str_replace("'","''",$d['uomPP']);
		$d['uominvPP']=str_replace("'","''",$d['uominvPP']);
		
		//cek approval
		$cekapproval 	= $this->db->query("select * from mdoc where vcCode='PP'
		");
		$rapv			= $cekapproval->row();
		
		if($rapv->intApv==1) // jika butuh aproval maka status document pending
		{
			$d['Status']='P';
		}
		else //jika tidak maka status document Open
		{
			$d['Status']='O';
		}
		//end cek approval
		
		$q=$this->db->query("insert into dPP (intHID,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv, intLocation,vcLocation, vcStatus)
		values ('$d[intHID]','$d[itemID]','$d[itemcodePP]','$d[itemnamePP]','$d[qtyPP]','$d[qtyPP]','$d[uomPP]',
		'$d[uomtypePP]','$d[qtyinvPP]','$d[qtyinvPP]','$d[uominvPP]','$d[whsPP]','$d[whsNamePP]','$d[Status]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePP']=str_replace("'","''",$d['itemcodePP']);
		$d['itemnamePP']=str_replace("'","''",$d['itemnamePP']);
		
		$d['uomPP']=str_replace("'","''",$d['uomPP']);
		$d['uominvPP']=str_replace("'","''",$d['uominvPP']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dPP';
		$his['doc']			= 'PP';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dPP where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodePP'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dPP set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodePP]',
		vcItemName='$d[itemnamePP]',
		intQty='$d[qtyPP]',
		intOpenQty='$d[qtyPP]',
		vcUoM='$d[uomPP]',
		intUoMType='$d[uomtypePP]',
		intQtyInv='$d[qtyinvPP]',
		intOpenQtyInv='$d[qtyinvPP]',
		vcUoMInv='$d[uominvPP]',
		intLocation='$d[whsPP]',
		vcLocation='$d[whsNamePP]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dPP where intID='$id'
		");
	}
	function deleteD2($id,$notdelitem)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dPP where intHID='$id' and intItem not in $notdelitem
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */