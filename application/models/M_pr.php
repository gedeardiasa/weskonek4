<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pr extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPR a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hPR a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPR a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPR a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPR a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hPR a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dPR a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dPR a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPR a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hPR a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hPR');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcShipToCity;
		$Country = $adrs->vcShipToCountry;
		$AddressH = $adrs->vcShipToAddress;
		
		$q=$this->db->query("insert into hPR (intBP,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[BPId]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPR set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dPR set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hPR set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update dPR set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
		");
	}
	function cekclose($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		$this->db->query("
		update dPR set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and intItem='$item'
		");// query update open qty PR
		
		$this->db->query("
		update dPR set
		vcStatus='C'
		where intHID='$id' and intItem='$item' and intOpenQty<=0
		");// query update status dPR
		
		$cekdetailsq=$this->db->query("
		select intID from dPR where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dPR yang statusnya open
		{
			$this->db->query("
			update hPR set
			vcStatus='C'
			where intID='$id'
			");// maka update hPR status ke 'C' (Close)
		}
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("
		
		update hPR set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePR']=str_replace("'","''",$d['itemcodePR']);
		$d['itemnamePR']=str_replace("'","''",$d['itemnamePR']);
		
		$d['uomPR']=str_replace("'","''",$d['uomPR']);
		$d['uominvPR']=str_replace("'","''",$d['uominvPR']);
		
		$q=$this->db->query("insert into dPR (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intCost, intLineCost, intLocation,vcLocation)
		values ('$d[intHID]','$d[idBaseRefPR]','$d[BaseRefPR]','$d[itemID]','$d[itemcodePR]','$d[itemnamePR]','$d[qtyPR]','$d[qtyPR]','$d[uomPR]',
		'$d[uomtypePR]','$d[qtyinvPR]','$d[qtyinvPR]','$d[uominvPR]','$d[pricePR]','$d[discperPR]','$d[discPR]','$d[priceafterPR]',
		'$d[linetotalPR]','$d[costPR]','$d[linecostPR]','$d[whsPR]','$d[whsNamePR]')
		");
		
		if($q)
		{
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePR']=str_replace("'","''",$d['itemcodePR']);
		$d['itemnamePR']=str_replace("'","''",$d['itemnamePR']);
		
		$d['uomPR']=str_replace("'","''",$d['uomPR']);
		$d['uominvPR']=str_replace("'","''",$d['uominvPR']);
		
		$q=$this->db->query("update dPR set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodePR]',
		vcItemName='$d[itemnamePR]',
		intQty='$d[qtyPR]',
		intOpenQty='$d[qtyPR]',
		vcUoM='$d[uomPR]',
		intUoMType='$d[uomtypePR]',
		intQtyInv='$d[qtyinvPR]',
		intOpenQtyInv='$d[qtyinvPR]',
		vcUoMInv='$d[uominvPR]',
		intPrice='$d[pricePR]',
		intDiscPer='$d[discperPR]',
		intDisc='$d[discPR]',
		intPriceAfterDisc='$d[priceafterPR]',
		intLineTotal='$d[linetotalPR]',
		intLocation='$d[whsPR]',
		vcLocation='$d[whsNamePR]'
		where intID='$d[intID]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dPR where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */