<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_price extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mpricecategory a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getDetailPrice($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode, b.vcName, b.vcSlsUoM from mprice a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intCategory='$id' and b.intDeleted=0
		order by b.intSalesItem desc, intPrice, b.vcCode asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPriceByItemAndCategory($item,$category)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intPrice from mprice where intItem='$item' and intCategory='$category'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intPrice;
		}
		else
		{
			return 0;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mpricecategory a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByName($name)
	{
		$q=$this->db->query("select intID from mpricecategory where vcName='$name'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function getpricebybpanditem($bp,$item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT c.intPrice
		FROM mbp a
		LEFT JOIN mpricecategory b ON a.`intPriceList`=b.intID
		LEFT JOIN mprice c ON b.`intID`=c.`intCategory`
		WHERE a.intID='$bp' AND c.intItem='$item'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			if($r->intPrice!=0)
			{
				return $r->intPrice;
			}
			else
			{
				$cekdefaultprice=$this->db->query("select intPrice from mitem where intID='$item'
				");
				$rdefaultprice=$cekdefaultprice->row();
				return $rdefaultprice->intPrice;
			}
		}
		else
		{
			$cekdefaultprice=$this->db->query("select intPrice from mitem where intID='$item'
			");
			
			$rdefaultprice=$cekdefaultprice->row();
			return $rdefaultprice->intPrice;
		}
	}
	function getAccessAllPlan()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT * FROM mplan WHERE intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function addnewaccessplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from mpriceplan where intPrice='$id' and intPlan='$plan'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into mpriceplan (intPrice,intPlan)
			values ('$id','$plan')
			");
		}
		return 1;
	}
	function dellastaccessbyidplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from mpriceplan where intPrice='$id' and intPlan='$plan'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function getAccessPlan($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.PlanID ,
		CASE WHEN b.PlanID>0 THEN 'checked=checked' ELSE '' END AS OpenForm
		
		FROM mplan a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS PlanID
			FROM mpricecategory a LEFT JOIN 
			(mpriceplan b LEFT JOIN mplan c ON b.`intPlan`=c.`intID`) ON a.`intID`=b.`intPrice`
			WHERE a.intID='$id'
		) b ON a.`intID`=b.PlanID
		WHERE a.intDeleted=0
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mpricecategory (vcName,dtInsertTime)
		values ('$d[Name]','$now')
		");
		if($q)
		{
		  $id=$this->db->query("select LAST_INSERT_ID() as intID");
		  $rid=$id->row();
		  $idtin=$rid->intID;
		  $this->addPrice($idtin);
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function addItem($iditem)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mpricecategory where intDeleted=0
		");
		
		foreach($q->result() as $c)
		{
			$idprice=$c->intID;
			$in=$this->db->query("insert into mprice (intCategory,intItem) values ('$idprice','$iditem')
			");
		}
	}
	function addPrice($idprice)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mitem where intDeleted=0
		");
		
		foreach($q->result() as $c)
		{
			$iditem=$c->intID;
			$in=$this->db->query("insert into mprice (intCategory,intItem) values ('$idprice','$iditem')
			");
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mpricecategory';
		$his['doc']			= 'PRICE';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mpricecategory set vcName='$d[Name]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($id,$price)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mprice';
		$his['doc']			= 'PRICE';
		$his['key']			= "intID=$id";
		$his['id']			= $this->history->getdatabyquery("select intCategory as id from mprice where intID=$id"); // get data id header
		$his['detailkey']	=$this->history->getdatabyquery("select a.vcCode as id from mprice b left join mitem a on b.intItem=a.intID where b.intID=$id"); // get data id header
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mprice set intPrice='$price' where intID='$id'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function editByItem($category,$item,$price)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("update mprice set intPrice='$price' where intItem='$item' and intCategory='$category'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mpricecategory set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */