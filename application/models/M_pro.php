<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pro extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, 
		case when a.intStatus=0 then 'Planned' when a.intStatus=1 then 'Release' when a.intStatus=2 then 'Closed' else 'Unknown' end as vcStatus
		from hPRO a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, 
		case when a.intStatus=0 then 'Planned' when a.intStatus=1 then 'Release' when a.intStatus=2 then 'Closed' else 'Unknown' end as vcStatus
		from hPRO a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and a.dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessRelease($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, 
		case when a.intStatus=0 then 'Planned' when a.intStatus=1 then 'Release' when a.intStatus=2 then 'Closed' else 'Unknown' end as vcStatus
		from hPRO a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.intStatus=1
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dPRO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM as FGUoM from hPRO a
		left join mitem b on a.intItem= b.intID
		 where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetIDByDocNum($docnum)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from hPRO where vcDocNum='$docnum'
		");
		
		
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->intID;
		}
		else
		{
			return 0;
		}
	}
	function GetPRO2byproandactivity($pro,$activity)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select *
		from dPRO2 where intHID='$pro' and intActivity='$activity'
		");
		
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r;
		}
		else
		{
			$r=$q->row();
			return $r;
		}
	}
	function GetDatabysonumberanditem($so,$item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from hPRO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.vcSONumber='$so' and a.intItem='$item'
		");
		
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r;
		}
		else
		{
			$r=$q->row();
			return $r;
		}
	}
	function CekNumPro($DocNum)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from hPRO where vcDocNum='$DocNum'
		");
		
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['FGItem']=str_replace("'","''",$d['FGItem']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);

		if(!isset($d['vcROUT'])){
			$d['vcROUT'] = '';
		}
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hPRO');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hPRO (vcDocNum,vcRef,dtDate,intLocation,vcLocation,intItem,vcItemCode,vcItemName,intQty,intCost,
		vcRemarks,vcUser,dtInsertTime,intPlannedQty,dtReqDate,vcSONumber,vcROUT)
		values ('$d[DocNum]','$d[Rev]','$d[DocDate]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),(select intID from mitem where vcName='$d[FGItem]'),(select vcCode from mitem where vcName='$d[FGItem]'),'$d[FGItem]',
		'$d[FGQty]','$d[FGCost]','$d[Remarks]','$d[UserID]','$now','$d[PlannedQty]','$d[ReqDate]','$d[SONumber]','$d[vcROUT]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD2($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['vcActivityCode']=str_replace("'","''",$d['vcActivityCode']);
		$d['vcActivityName']=str_replace("'","''",$d['vcActivityName']);
		$d['vcUoM']=str_replace("'","''",$d['vcUoM']);
		$q=$this->db->query("insert into dPRO2 (intHID,intActivity,vcActivityCode,vcActivityName,intQty,vcUoM,intAutoIssue)
		values ('$d[intHID]','$d[intActivity]',
		'$d[vcActivityCode]','$d[vcActivityName]','$d[intQty]','$d[vcUoM]','$d[intAutoIssue]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}

	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePRO']=str_replace("'","''",$d['itemcodePRO']);
		$d['itemnamePRO']=str_replace("'","''",$d['itemnamePRO']);
		$d['uomPRO']=str_replace("'","''",$d['uomPRO']);
		$q=$this->db->query("insert into dPRO (intHID,intItem,vcItemCode,vcItemName,intQty,intPlannedQty,vcUoM,intCost,intAutoIssue)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodePRO]'),
		'$d[itemcodePRO]','$d[itemnamePRO]','$d[qtyPRO]','$d[plannedqtyPRO]','$d[uomPRO]','$d[costPRO]','$d[autoPRO]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function updateplancost($proid,$activityid,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update dPRO2 set intPlanCost=intPlanCost+'$cost' where intHID='$proid' and intActivity='$activityid'
		");
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodePRO']=str_replace("'","''",$d['itemcodePRO']);
		$d['itemnamePRO']=str_replace("'","''",$d['itemnamePRO']);
		$d['uomPRO']=str_replace("'","''",$d['uomPRO']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dPRO where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodePRO'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dPRO set
		intItem=(select intID from mitem where vcCode='$d[itemcodePRO]'),
		vcItemCode='$d[itemcodePRO]',
		vcItemName='$d[itemnamePRO]',
		intQty='$d[qtyPRO]',
		intPlannedQty='$d[plannedqtyPRO]',
		vcUoM='$d[uomPRO]',
		intCost='$d[costPRO]',
		intAutoIssue='$d[autoPRO]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editStatus($id,$status)
	{
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$id";
		$his['id']			= $id;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		update hPRO set
		intStatus='$status'
		where intID='$id'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hPRO set
		vcRef='$d[Rev]',
		intLocation='$d[Whs]',
		vcLocation=(select vcName from mlocation where intID='$d[Whs]'),
		intQty='$d[FGQty]',
		intPlannedQty='$d[PlannedQty]',
		vcSONumber='$d[SONumber]',
		intStatus='$d[Status]',
		dtReqDate='$d[ReqDate]',
		vcRemarks='$d[Remarks]'
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function release($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		$now=date('Y-m-d H:i:s');
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		update hPRO set
		dtReleaseDate='$now',
		vcUserRelease='$d[UserID]'
		where intID='$d[id]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function close($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		$now=date('Y-m-d H:i:s');
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		update hPRO set
		dtCloseDate='$now',
		vcUserClose='$d[UserID]'
		where intID='$d[id]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intCost='$cost' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateComponentCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intComponentCost=intComponentCost+'$cost' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateProductCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intProductCost=intProductCost+'$cost' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateRejectCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intRejectCost=intRejectCost+'$cost' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateIssueQty($intID,$item,$qty)
	{
		$qty=$qty*-1;
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intHID='$intID' and intItem='$item'";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dPRO where intHID='$intID' and intItem='$item'"); // get data id header
		$his['detailkey']	= $this->history->getdatabyquery("select vcItemCode as id from dPRO where intHID='$intID' and intItem='$item'");
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dPRO set intIssueQty=intIssueQty+'$qty' where intHID='$intID' and intItem='$item'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateActualQty($intID,$qty)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intActualQty=intActualQty+'$qty' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function updateRejectQty($intID,$qty)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$intID";
		$his['id']			= $intID;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update hPRO set intRejectQty=intRejectQty+'$qty' where intID='$intID'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hPRO';
		$his['doc']			= 'PRO';
		$his['key']			= "intID=$id";
		$his['id']			= $id;
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("delete from dPRO where intID='$id'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */