<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pur_report extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	/*
	
	jenis transaksi barang
	
	IT=inventory transfer
	AD=Adjustment
	DN=Delivery
	GO=Grpo/Kedatangan Barang
	RE=Retur Penjualan
	RP=Retur Pembelian
	
	
	*/
	function GetRepIA($d)
	{
		if($d['RefI']=='GRPO')
		{
			$tabH='hGRPO';
			$tabD='dGRPO';
		}
		else if($d['RefI']=='AP')
		{
			$tabH='hAP';
			$tabD='dAP';
		}
		else if($d['RefI']=='APCM')
		{
			$tabH='hAPCM';
			$tabD='dAPCM';
		}
		else if($d['RefI']=='PR')
		{
			$tabH='hPR';
			$tabD='dPR';
		}
		else if($d['RefI']=='PP')
		{
			$tabH='hPP';
			$tabD='dPP';
		}
		
		$db=$this->load->database('default', TRUE);
		
		if($d['RefI']!='PP')
		{
			$q=$this->db->query("SELECT 
			a.`vcItemCode`, a.`vcItemName`, SUM(a.`intQtyInv`) AS intQtyInv,
			a.`vcUoMInv`, SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) AS intLineTotal, SUM(a.`intLineCost`) AS intLineCost,
			SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`) AS intGrossProfit,
			(SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`))/SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)))*100 AS intGrossPercent
			FROM $tabD a
			LEFT JOIN $tabH b ON a.`intHID`=b.intID
			LEFT JOIN (mitem c 
				LEFT JOIN mitemcategory d on c.intCategory=d.intID
			)
			on a.intItem=c.intID
			LEFT JOIN mlocation e on a.intLocation=e.intID
			where c.intDeleted=0 and a.vcItemCode like '%$d[ItemCodeI]%' and a.vcItemName like '%$d[ItemNameI]%' and
			(d.intID='$d[ItemGroupI]' or $d[ItemGroupI]=0) and
			(e.intPlan='$d[PlanI]' or $d[PlanI]=0) and
			b.dtDate>='$d[fromI]' and b.dtDate<='$d[untilI]'
			and (a.vcStatus='$d[StatusI]' or '$d[StatusI]'='A')
			GROUP BY a.`vcItemCode`, a.`vcItemName`, a.`vcUoMInv`
			");
		}
		else
		{
			$q=$this->db->query("SELECT 
			a.`vcItemCode`, a.`vcItemName`, SUM(a.`intQtyInv`) AS intQtyInv,
			a.`vcUoMInv`, 0  AS intLineTotal, 0 AS intLineCost,
			0 AS intGrossProfit,
			0 AS intGrossPercent
			FROM $tabD a
			LEFT JOIN $tabH b ON a.`intHID`=b.intID
			LEFT JOIN (mitem c 
				LEFT JOIN mitemcategory d on c.intCategory=d.intID
			)
			on a.intItem=c.intID
			LEFT JOIN mlocation e on a.intLocation=e.intID
			where c.intDeleted=0 and a.vcItemCode like '%$d[ItemCodeI]%' and a.vcItemName like '%$d[ItemNameI]%' and
			(d.intID='$d[ItemGroupI]' or $d[ItemGroupI]=0) and
			(e.intPlan='$d[PlanI]' or $d[PlanI]=0) and
			b.dtDate>='$d[fromI]' and b.dtDate<='$d[untilI]'
			and ( a.vcStatus='$d[StatusI]' or '$d[StatusI]'='A' )
			GROUP BY a.`vcItemCode`, a.`vcItemName`, a.`vcUoMInv`
			");

		}
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetRepCA($d)
	{
		if($d['RefC']=='GRPO')
		{
			$tabH='hGRPO';
			$tabD='dGRPO';
		}
		else if($d['RefC']=='AP')
		{
			$tabH='hAP';
			$tabD='dAP';
		}
		else if($d['RefC']=='APCM')
		{
			$tabH='hAPCM';
			$tabD='dAPCM';
		}
		else if($d['RefC']=='PR')
		{
			$tabH='hPR';
			$tabD='dPR';
		}
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		b.`vcBPCode`, b.`vcBPName`, SUM(a.`intQtyInv`) AS intQtyInv,
		a.`vcUoMInv`, SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) AS intLineTotal, SUM(a.`intLineCost`) AS intLineCost,
		SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`) AS intGrossProfit,
		(SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`))/SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)))*100 AS intGrossPercent
		FROM $tabD a
		LEFT JOIN $tabH b ON a.`intHID`=b.intID
		LEFT JOIN (mitem c 
			LEFT JOIN mitemcategory d on c.intCategory=d.intID
		)
		on a.intItem=c.intID
		LEFT JOIN (mbp e 
			LEFT JOIN mbpcategory f on e.intCategory=f.intID
		)on b.intBP=e.intID
		LEFT JOIN mlocation g on a.intLocation=g.intID
		where c.intDeleted=0 and e.intDeleted=0 and b.vcBPCode like '%$d[BPCodeC]%' and b.vcBPName like '%$d[BPNameC]%' and
		(f.intID='$d[BPGroupC]' or $d[BPGroupC]=0) and
		(g.intPlan='$d[PlanC]' or $d[PlanC]=0) and
		b.dtDate>='$d[fromC]' and b.dtDate<='$d[untilC]'
		and b.vcStatus!='X'
		GROUP BY b.`vcBPCode`, b.`vcBPName`, a.`vcUoMInv`
		");

		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetRepG($d)
	{
		if($d['RefG']=='GRPO')
		{
			$tabH='hGRPO';
			$tabD='dGRPO';
		}
		else if($d['RefG']=='AP')
		{
			$tabH='hAP';
			$tabD='dAP';
		}
		else if($d['RefG']=='APCM')
		{
			$tabH='hAPCM';
			$tabD='dAPCM';
		}
		else if($d['RefG']=='PR')
		{
			$tabH='hPR';
			$tabD='dPR';
		}
		else if($d['RefG']=='PP')
		{
			$tabH='hPP';
			$tabD='dPP';
		}
		
		if($d['RefG']!='PP')
		{
			$db=$this->load->database('default', TRUE);
			$q=$this->db->query("
			SELECT 
			b.vcDocNum,
			a.`vcItemCode`, a.`vcItemName`,
			b.`vcBPCode`, b.`vcBPName`,a.intQtyInv,
			a.`vcUoMInv`, a.intLineTotal-(b.intDiscPer/100*a.intLineTotal) as intLineTotal, a.intLineCost,
			(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) - a.`intLineCost` AS intGrossProfit,
			((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) - a.`intLineCost`)/(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))*100 AS intGrossPercent
			FROM $tabD a
			LEFT JOIN $tabH b ON a.`intHID`=b.intID
			LEFT JOIN (mitem c 
				LEFT JOIN mitemcategory d on c.intCategory=d.intID
			)
			on a.intItem=c.intID
			LEFT JOIN (mbp e 
				LEFT JOIN mbpcategory f on e.intCategory=f.intID
			)on b.intBP=e.intID
			LEFT JOIN mlocation g on a.intLocation=g.intID
			where 
			c.intDeleted=0 and a.vcItemCode like '%$d[ItemCodeG]%' and a.vcItemName like '%$d[ItemNameG]%' and
			e.intDeleted=0 and b.vcBPCode like '%$d[BPCodeG]%' and b.vcBPName like '%$d[BPNameG]%' and
			(f.intID='$d[BPGroupG]' or $d[BPGroupG]=0) and
			(d.intID='$d[ItemGroupG]' or $d[ItemGroupG]=0) and
			(g.intPlan='$d[PlanG]' or $d[PlanG]=0) and
			b.dtDate>='$d[fromG]' and b.dtDate<='$d[untilG]'
			and (a.vcStatus='$d[StatusG]' or '$d[StatusG]'='A')
			
			");
		}
		else
		{
			$db=$this->load->database('default', TRUE);
			$q=$this->db->query("
			SELECT 
			b.vcDocNum,
			a.`vcItemCode`, a.`vcItemName`, a.`vcUoMInv`,
			'Nof Defined' as vcBPCode, 'Nof Defined' as vcBPName,a.intQtyInv,
			0 as intLineTotal, 0 as intLineCost,
			0 AS intGrossProfit,
			0 AS intGrossPercent
			FROM $tabD a
			LEFT JOIN $tabH b ON a.`intHID`=b.intID
			LEFT JOIN (mitem c 
				LEFT JOIN mitemcategory d on c.intCategory=d.intID
			)
			on a.intItem=c.intID
			
			LEFT JOIN mlocation g on a.intLocation=g.intID
			where 
			c.intDeleted=0 and a.vcItemCode like '%$d[ItemCodeG]%' and a.vcItemName like '%$d[ItemNameG]%' and
			(d.intID='$d[ItemGroupG]' or $d[ItemGroupG]=0) and
			(g.intPlan='$d[PlanG]' or $d[PlanG]=0) and
			b.dtDate>='$d[fromG]' and b.dtDate<='$d[untilG]'
			and (a.vcStatus='$d[StatusG]' or '$d[StatusG]'='A')
			
			");
		}
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */