<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_report extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllTable()
	{
		$db=$this->load->database('default', TRUE);
		$dbmaster=$this->load->database('master', TRUE);
		$qm=$dbmaster->query("SELECT * FROM mclient where intDeleted=0");
		foreach($qm->result() as $rm)
		{
			$subdomain=md5($rm->vcSubDomain);
			if($_SESSION[md5('db_pos_setting')]==$subdomain)
			{
				$dbnamefromdatabase=$rm->vcDB;
			}
		}
		
		$q=$this->db->query("SELECT TABLE_NAME, LEFT(TABLE_NAME,1) AS TYPE
		FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE' AND TABLE_SCHEMA='$dbnamefromdatabase'");
		return $q;
	}
	function GetColumnByTable($table)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("SHOW COLUMNS FROM $table");
		return $q;
	}
	function GetColumnNameByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("select vcName from mcolumn where vcCode='$code'");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return '';
		}
	}
	function GetAllColumn()
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("select * from mcolumn where intDeleted=0");
		return $q;
	}
	function GetDataByQuery($query,$d)
	{
		$db=$this->load->database('default', TRUE);
		$this->db->db_debug = FALSE;
		$q=$this->db->query($query);
		if(!$q)
		{
			return null;
		}
		else
		{
			return $q;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mreport where vcCode='$code'");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return 0;
		}
	}
	function GetAccessReport($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select b.vcName,b.intID,b.vcUserID from maccess_report a
		LEFT JOIN muser b on a.intUserID=b.intID
		where a.intReport='$id'
		");
		return $q;
	}
	function CekAccess($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select intID from maccess_report where intReport='$id' and intUserID='$_SESSION[IDPOS]'
		");
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetSearch($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT
		a.vcName AS vcKey,
		a.intID AS intID,
		'item/detail/' AS vcUrl,
		'Item Master Data' AS vcType
		FROM mitem a where vcName like '%$d%' or vcCode like '%$d%'
		
		union all
		select
		a.vcName as vcKey,
		a.intID as intID,
		'user/detail/' AS vcUrl,
		'User' AS vcType
		FROM muser a where vcName like '%$d%' or vcEmail like '%$d%' or vcUserID like '%$d%'
		
		union all
		select
		a.vcName as vcKey,
		a.intID as intID,
		'bp/detail/' AS vcUrl,
		'Business Patner' AS vcType
		FROM mbp a where vcName like '%$d%' or vcCode like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'so?id=' AS vcUrl,
		'Sales Order' AS vcType
		FROM hSO a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'dn?id=' AS vcUrl,
		'Delivery Note' AS vcType
		FROM hDN a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'sr?id=' AS vcUrl,
		'Sales Return' AS vcType
		FROM hSR a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'sq?id=' AS vcUrl,
		'Sales Quotation' AS vcType
		FROM hSQ a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'ar?id=' AS vcUrl,
		'A/R Invoice' AS vcType
		FROM hAR a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'arcm?id=' AS vcUrl,
		'A/R Credit Memo' AS vcType
		FROM hARCM a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'disc?id=' AS vcUrl,
		'Discount' AS vcType
		FROM hDiscount a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'po?id=' AS vcUrl,
		'Purchase Order' AS vcType
		FROM hPO a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'pr?id=' AS vcUrl,
		'Purchase Return' AS vcType
		FROM hPR a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'grpo?id=' AS vcUrl,
		'Good Receipt Purchase Order' AS vcType
		FROM hGRPO a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'ap?id=' AS vcUrl,
		'A/P Invoice' AS vcType
		FROM hAP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'apcm?id=' AS vcUrl,
		'A/P Credit Memo' AS vcType
		FROM hAPCM a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'pp?id=' AS vcUrl,
		'Purchase Request' AS vcType
		FROM hPP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'inpay?id=' AS vcUrl,
		'Incoming Payment' AS vcType
		FROM hINPAY a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'outpay?id=' AS vcUrl,
		'Outgoing Payment Payment' AS vcType
		FROM hOUTPAY a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'transfer?id=' AS vcUrl,
		'Inventory Transfer' AS vcType
		FROM hIT a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'adjustment?id=' AS vcUrl,
		'Adjustment Stock' AS vcType
		FROM hAdjustment a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'revaluation?id=' AS vcUrl,
		'Revaluation Item' AS vcType
		FROM hRevaluation a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'pid?id=' AS vcUrl,
		'Physical Inventory Document' AS vcType
		FROM hPID a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'inv_posting?id=' AS vcUrl,
		'Inventory Posting' AS vcType
		FROM hIP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'bom?id=' AS vcUrl,
		'Bill of Material' AS vcType
		FROM hBOM a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'pro?id=' AS vcUrl,
		'Production Order' AS vcType
		FROM hPRO a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'ifp?id=' AS vcUrl,
		'Issue For Production' AS vcType
		FROM hIFP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'rfp?id=' AS vcUrl,
		'Receipt From Production' AS vcType
		FROM hRFP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'wo?id=' AS vcUrl,
		'Work Order' AS vcType
		FROM hWO a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'jurnal?id=' AS vcUrl,
		'Jurnal Entry' AS vcType
		FROM hJE a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'capital?id=' AS vcUrl,
		'Capitalization Asset' AS vcType
		FROM hCAP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'retire?id=' AS vcUrl,
		'Retirement Asset' AS vcType
		FROM hRT a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'asset_transfer?id=' AS vcUrl,
		'Asset Transfer' AS vcType
		FROM hAT a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'manual_dep?id=' AS vcUrl,
		'Manual Depreciation' AS vcType
		FROM hDEP a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'dep_run?id=' AS vcUrl,
		'Running Depreciation' AS vcType
		FROM hDEPR a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		union all
		select
		a.vcDocNum as vcKey,
		a.intID as intID,
		'asset_reval?id=' AS vcUrl,
		'Asset Revaluation' AS vcType
		FROM hRV a where vcDocNum like '%$d%' or vcRemarks like '%$d%'
		
		");
		
		
		return $q;
	}
	function GetPNL($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		#Profit By AR Item
SELECT 
SUM(a.`intLineTotal`-(b.`intDiscPer`/100*a.`intLineTotal`)) AS val, 'Penjualan Barang' AS Account
FROM dAR a
LEFT JOIN hAR b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]' AND b.`intService`=0 
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAR a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Profit By AR Service
UNION ALL
SELECT 
IFNULL(SUM(a.`intLineTotal`-(b.`intDiscPer`/100*a.`intLineTotal`)),0) AS val, 'Pendapatan Jasa' AS Account
FROM dAR a
LEFT JOIN hAR b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]' AND b.`intService`=1
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAR a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Pendapatan Ongkir
UNION ALL
SELECT
SUM(b.`intFreight`) AS val, 'Pendapatan Ongkos Kirim' AS Account
FROM hAR b
LEFT JOIN 
(
	SELECT intLocation, intHID FROM dAR GROUP BY intHID
) a ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]'
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAR a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'

)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Pendapatan Pajak
UNION ALL
SELECT
SUM(b.`intTax`) AS val, 'Pajak Masukan' AS Account
FROM dAR a
LEFT JOIN hAR b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]'
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAR a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#COGS
UNION ALL
SELECT
SUM(a.`intLineCost`)*-1 AS val, 'HPP Barang' AS Account
FROM dAR a
LEFT JOIN hAR b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]' AND b.`intService`=0 
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAR a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Retur Penjualan
UNION ALL
SELECT 
IFNULL(SUM(a.`intLineTotal`-(b.`intDiscPer`/100*a.`intLineTotal`))*-1,0) AS val, 'Retur Penjualan Barang' AS Account
FROM dARCM a
LEFT JOIN hARCM b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]' AND b.`intService`=0 
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dARCM a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Varian GI
UNION ALL
SELECT
SUM(a.`intQty`*a.`intCost`) AS val, 'Variance GI' AS Account
FROM dAdjustment a
LEFT JOIN hAdjustment b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]'
AND (c.`intPlan`=$d[plan] OR $d[plan]=0) AND b.vcType='GI'
AND b.intID NOT IN(SELECT a.intHID FROM dAdjustment a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Varian IP
UNION ALL
SELECT
SUM(a.`intCost`* (a.intQty-intQtyBefore)) AS val, 'Variance IP (Inventory Posting)' AS Account
FROM dIP a
LEFT JOIN hIP b ON a.intHID=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]'
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dIP a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')

#Biaya Service AP
UNION ALL
SELECT 
IFNULL(SUM(a.`intLineTotal`-(b.`intDiscPer`/100*a.`intLineTotal`))*-1,0) AS val, 'Biaya Service' AS Account
FROM dAP a
LEFT JOIN hAP b ON a.`intHID`=b.`intID`
LEFT JOIN mlocation c ON a.`intLocation`=c.intID
WHERE b.`vcStatus`!='X' AND b.`dtDate`>='$d[from]' AND b.`dtDate`<='$d[until]' AND b.`intService`=1 
AND (c.`intPlan`=$d[plan] OR $d[plan]=0)
AND b.intID NOT IN(SELECT a.intHID FROM dAP a LEFT JOIN 
(mlocation b LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$userid'
)ON a.`intLocation`=b.intID WHERE IFNULL(c.`intUserID`,0)<>'$userid')


		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPNL1($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, 
		CASE WHEN a.GL=4 THEN a.Val*-1
		WHEN a.GL=5 THEN a.Val*-1
		WHEN a.GL=6 THEN a.Val*-1
		WHEN a.GL=7 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, SUM(b.`intValue`) AS Val, c.`vcName` FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.`intHID`
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,1)=c.vcCode
			WHERE (a.`intPlan`=$d[plan] or $d[plan]=0)
			
			AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
			AND (LEFT(b.`vcGLCode`,1)=4 OR LEFT(b.`vcGLCode`,1)=5
			OR LEFT(b.`vcGLCode`,1)=6 OR LEFT(b.`vcGLCode`,1)=7)
			GROUP BY LEFT(b.`vcGLCode`,1)
		) a

		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPNL2($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		
		SELECT a.GL, a.GL2, 
		CASE WHEN a.GL=4 THEN a.Val*-1
		WHEN a.GL=5 THEN a.Val*-1
		WHEN a.GL=6 THEN a.Val*-1
		WHEN a.GL=7 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2 ,SUM(b.`intValue`) AS Val, c.`vcName` FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.`intHID`
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,2)=c.vcCode
			WHERE (a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
			AND (LEFT(b.`vcGLCode`,1)=4 OR LEFT(b.`vcGLCode`,1)=5
			OR LEFT(b.`vcGLCode`,1)=6 OR LEFT(b.`vcGLCode`,1)=7)
			GROUP BY LEFT(b.`vcGLCode`,2)
		) a

		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPNL3($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		
		SELECT a.GL, a.GL2,a.GL4, 
		CASE WHEN a.GL=4 THEN a.Val*-1
		WHEN a.GL=5 THEN a.Val*-1
		WHEN a.GL=6 THEN a.Val*-1
		WHEN a.GL=7 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2 ,
			LEFT(b.`vcGLCode`,4) AS GL4 ,
			SUM(b.`intValue`) AS Val, c.`vcName` FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.`intHID`
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,4)=c.vcCode
			WHERE (a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
			AND (LEFT(b.`vcGLCode`,1)=4 OR LEFT(b.`vcGLCode`,1)=5
			OR LEFT(b.`vcGLCode`,1)=6 OR LEFT(b.`vcGLCode`,1)=7)
			GROUP BY LEFT(b.`vcGLCode`,4)
		) a

		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPNL4($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2,a.GL4,a.GL6, 
		CASE WHEN a.GL=4 THEN a.Val*-1
		WHEN a.GL=5 THEN a.Val*-1
		WHEN a.GL=6 THEN a.Val*-1
		WHEN a.GL=7 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2 ,
			LEFT(b.`vcGLCode`,4) AS GL4 ,LEFT(b.`vcGLCode`,6) AS GL6 ,
			SUM(b.`intValue`) AS Val, c.`vcName` FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.`intHID`
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,6)=c.vcCode
			WHERE (a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
			AND (LEFT(b.`vcGLCode`,1)=4 OR LEFT(b.`vcGLCode`,1)=5
			OR LEFT(b.`vcGLCode`,1)=6 OR LEFT(b.`vcGLCode`,1)=7)
			GROUP BY LEFT(b.`vcGLCode`,6)
		) a

		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetPNL5($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2,a.GL4,a.GL6, a.GL8, 
		CASE WHEN a.GL=4 THEN a.Val*-1
		WHEN a.GL=5 THEN a.Val*-1
		WHEN a.GL=6 THEN a.Val*-1
		WHEN a.GL=7 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2 ,
			LEFT(b.`vcGLCode`,4) AS GL4 ,LEFT(b.`vcGLCode`,6) AS GL6 ,LEFT(b.`vcGLCode`,8) AS GL8 ,
			SUM(b.`intValue`) AS Val, c.`vcName` FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.`intHID`
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,8)=c.vcCode
			WHERE (a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
			AND (LEFT(b.`vcGLCode`,1)=4 OR LEFT(b.`vcGLCode`,1)=5
			OR LEFT(b.`vcGLCode`,1)=6 OR LEFT(b.`vcGLCode`,1)=7)
			GROUP BY LEFT(b.`vcGLCode`,8)
		) a

		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataByUser()
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		select * from mreport 
		where (vcUserID='$_SESSION[UsernamePOS]' or intID in
		(
			select intReport as intID from maccess_report where intUserID='$_SESSION[IDPOS]'
		)) and intDeleted=0
		order by intID desc 
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetBalanceSheet1($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, 
		CASE WHEN a.GL=1 THEN a.Val
		WHEN a.GL=2 THEN a.Val*-1
		WHEN a.GL=3 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, SUM(b.`intValue`) AS Val, c.`vcName` 
			FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.intHID
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,1)=c.vcCode
			WHERE 
			(a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`<='$d[Date]' AND
			(LEFT(b.`vcGLCode`,1)=1  OR LEFT(b.`vcGLCode`,1)=2 OR   LEFT(b.`vcGLCode`,1)=3)
			GROUP BY LEFT(b.`vcGLCode`,1)

		) a
		
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetBalanceSheet2($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2, 
		CASE WHEN a.GL=1 THEN a.Val
		WHEN a.GL=2 THEN a.Val*-1
		WHEN a.GL=3 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2, SUM(b.`intValue`) AS Val, c.`vcName` 
			FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.intHID
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,2)=c.vcCode
			WHERE 
			(a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`<='$d[Date]' AND
			(LEFT(b.`vcGLCode`,1)=1  OR LEFT(b.`vcGLCode`,1)=2 OR   LEFT(b.`vcGLCode`,1)=3)
			GROUP BY LEFT(b.`vcGLCode`,2)

		) a
		
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetBalanceSheet3($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2,  a.GL4, 
		CASE WHEN a.GL=1 THEN a.Val
		WHEN a.GL=2 THEN a.Val*-1
		WHEN a.GL=3 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2,LEFT(b.`vcGLCode`,4) AS GL4, SUM(b.`intValue`) AS Val, c.`vcName` 
			FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.intHID
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,4)=c.vcCode
			WHERE 
			(a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`<='$d[Date]' AND
			(LEFT(b.`vcGLCode`,1)=1  OR LEFT(b.`vcGLCode`,1)=2 OR   LEFT(b.`vcGLCode`,1)=3)
			GROUP BY LEFT(b.`vcGLCode`,4)

		) a
		
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetBalanceSheet4($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2,  a.GL4, a.GL6, 
		CASE WHEN a.GL=1 THEN a.Val
		WHEN a.GL=2 THEN a.Val*-1
		WHEN a.GL=3 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2,LEFT(b.`vcGLCode`,4) AS GL4, LEFT(b.`vcGLCode`,6) AS GL6, SUM(b.`intValue`) AS Val, c.`vcName` 
			FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.intHID
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,6)=c.vcCode
			WHERE 
			(a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`<='$d[Date]' AND
			(LEFT(b.`vcGLCode`,1)=1  OR LEFT(b.`vcGLCode`,1)=2 OR   LEFT(b.`vcGLCode`,1)=3)
			GROUP BY LEFT(b.`vcGLCode`,6)

		) a
		
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetBalanceSheet5($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		
		SELECT a.GL, a.GL2,  a.GL4, a.GL6,  a.GL8, 
		CASE WHEN a.GL=1 THEN a.Val
		WHEN a.GL=2 THEN a.Val*-1
		WHEN a.GL=3 THEN a.Val*-1
		ELSE 0 END AS Val,
		a.vcName  FROM
		(
			SELECT LEFT(b.`vcGLCode`,1) AS GL, LEFT(b.`vcGLCode`,2) AS GL2,LEFT(b.`vcGLCode`,4) AS GL4, LEFT(b.`vcGLCode`,6) AS GL6, LEFT(b.`vcGLCode`,8) AS GL8, 
			SUM(b.`intValue`) AS Val, c.`vcName` 
			FROM hJE a
			LEFT JOIN dMutationJE b ON a.intID=b.intHID
			LEFT JOIN mgl c ON LEFT(b.`vcGLCode`,8)=c.vcCode
			WHERE 
			(a.`intPlan`=$d[plan] or $d[plan]=0)
			AND a.`dtDate`<='$d[Date]' AND
			(LEFT(b.`vcGLCode`,1)=1  OR LEFT(b.`vcGLCode`,1)=2 OR   LEFT(b.`vcGLCode`,1)=3)
			GROUP BY LEFT(b.`vcGLCode`,8)

		) a
		
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAgingCustomer($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		SELECT 
		a.*, 0 as intBalanceCM, 'ar' as type
		FROM hAR a
		WHERE a.`intBalance`>0 and a.vcBPCode like '%$d[BPCode]%' and a.vcBPName like '%$d[BPName]%' and a.vcStatus<>'X'
		union all
		SELECT 
		a.*, a.intBalance*-1 as intBalanceCM, 'arcm' as type
		FROM hARCM a
		WHERE a.`intBalance`>0 and a.vcBPCode like '%$d[BPCode]%' and a.vcBPName like '%$d[BPName]%' and a.vcStatus<>'X'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAgingVendor($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		SELECT 
		a.*, 0 as intBalanceCM, 'ap' as type
		FROM hAP a
		WHERE a.`intBalance`>0 and a.vcBPCode like '%$d[BPCode]%' and a.vcBPName like '%$d[BPName]%' and a.vcStatus<>'X'
		union all
		SELECT 
		a.*, a.intBalance*-1 as intBalanceCM, 'apcm' as type
		FROM hAPCM a
		WHERE a.`intBalance`>0 and a.vcBPCode like '%$d[BPCode]%' and a.vcBPName like '%$d[BPName]%' and a.vcStatus<>'X'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetCashFlowReport($d)
	{
		$db=$this->load->database('default', TRUE);
		$userid=$_SESSION['IDPOS'];
		$q=$this->db->query("
		SELECT 
SUM(a.intCost) AS intCost, 
CASE WHEN b.vcName!='' THEN b.vcName ELSE 'Unknown' END AS GroupName, 
CASE WHEN b.`vcCode`!='' THEN b.vcCode ELSE 'Unknown' END AS GroupCode, b.intHeader
FROM dWalletMutation a
LEFT JOIN mgroupcashflow b ON a.`intCF`=b.`intID`
LEFT JOIN mwallet c ON a.intWallet=c.intID
LEFT JOIN mplan d ON c.intPlan=d.intID
WHERE (c.`intPlan`=$d[plan] or $d[plan]=0)
			
AND a.`dtDate`>='$d[from]' AND a.`dtDate`<='$d[until]'
GROUP BY b.vcCode

		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mreport a
		where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Query']=str_replace("'","''",$d['Query']);
		
		if(stripos($d['Query'], 'insert') !== false)
		{
			$d['Query'] = 'Dont Use Insertt Query';
		}
		if(stripos($d['Query'], 'update') !== false)
		{
			$d['Query'] = 'Dont Use Updatee Query';
		}
		if(stripos($d['Query'], 'delete') !== false)
		{
			$d['Query'] = 'Dont Use Deletee Query';
		}
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$q=$this->db->query("insert into mreport (vcCode,vcName,vcQuery,vcUserID,dtInsertTime)
		select '$d[Code]','$d[Name]','$d[Query]',
		'$d[UserID]','$now'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Query']=str_replace("'","''",$d['Query']);
		
		if(stripos($d['Query'], 'insert') !== false)
		{
			$d['Query'] = 'Dont Use Insertt Query';
		}
		if(stripos($d['Query'], 'update') !== false)
		{
			$d['Query'] = 'Dont Use Updatee Query';
		}
		if(stripos($d['Query'], 'delete') !== false)
		{
			$d['Query'] = 'Dont Use Deletee Query';
		}
		
		$q=$this->db->query("
		update mreport set
		vcCode='$d[Code]',
		vcName='$d[Name]',
		vcQuery='$d[Query]'
		where intID='$d[id]'
		");
		
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mreport set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function adduser($d)
	{
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			insert into maccess_report (intUserID, intReport)
			values ('$d[User]','$d[Report]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deluser($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
			delete from maccess_report where intID='$id'
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function tescron()
	{
		$this->db->query("
			update mwallet set vcName='Kas X' where intID=1
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */