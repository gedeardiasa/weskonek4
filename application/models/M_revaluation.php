<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_revaluation extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hRevaluation a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hRevaluation a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hRevaluation a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dRevaluation a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hRevaluation where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['Account']=str_replace("'","''",$d['Account']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hRevaluation');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hRevaluation (vcDocNum,vcGLCode,dtDate,vcRef,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[Account]','$d[DocDate]','$d[RefNum]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeRV']=str_replace("'","''",$d['itemcodeRV']);
		$d['itemnameRV']=str_replace("'","''",$d['itemnameRV']);
		$d['uomRV']=str_replace("'","''",$d['uomRV']);
		$q=$this->db->query("insert into dRevaluation (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intCostBefore, intCost)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeRV]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeRV]','$d[itemnameRV]','$d[costbeforeRV]','$d[costRV]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */