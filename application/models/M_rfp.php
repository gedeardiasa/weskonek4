<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rfp extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hRFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, d.vcDocNum as PRONum
		from hRFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		LEFT JOIN hPRO d on a.intPRO=d.intID
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation, d.vcDocNum as PRONum
		from hRFP a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		LEFT JOIN hPRO d on a.intPRO=d.intID
		where c.intUserID='$iduser' and a.dtDate>='$d[from]' and a.dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dRFP a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum as PRONum from hRFP a 
		LEFT JOIN hPRO b on a.intPRO=b.intID
		where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetDetailDataByPROID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		a.*, b.`intItem`, b.`vcItemCode`, b.`vcItemName`, b.`intQty`, b.`intCost`
		FROM hRFP a
		LEFT JOIN dRFP b ON a.intID=b.`intHID`
		WHERE a.`intPRO`=$id
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		//$db=$d['db'];
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hRFP');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hRFP (vcDocNum,intPRO,dtDate,vcRef,vcType,intLocation,vcLocation,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[PRONum]','$d[DocDate]','$d[RefNum]','$d[Type]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		//db=$d['db'];
		$d['itemcodeRFP']=str_replace("'","''",$d['itemcodeRFP']);
		$d['itemnameRFP']=str_replace("'","''",$d['itemnameRFP']);
		$d['uomRFP']=str_replace("'","''",$d['uomRFP']);
		if(!$d['costroutRFP']){
			$d['costroutRFP']=0;
		}
		$q=$this->db->query("insert into dRFP (intHID,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intQty, intCost, intCostRout)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeRFP]'),
		'$d[Whs]',(select vcName from mlocation where intID='$d[Whs]'),
		'$d[itemcodeRFP]','$d[itemnameRFP]','$d[qtyRFP]','$d[costRFP]','$d[costroutRFP]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */