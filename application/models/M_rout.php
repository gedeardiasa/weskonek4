<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rout extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	function GetDataByItemAndLoc($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hROUT a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		where a.intItem='$item' and a.intLocation='$loc'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetDataByItemAndLoc2($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hROUT a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		where a.intItem='$item' and a.intLocation='$loc'
		order by a.intID desc
		");
		return $q;
	}
	function GetIdByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from hROUT where vcDocNum='$doc'
		");
		if($q){
			$r=$q->row();
			return $r->intID;
		}else{
			return 0;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hROUT a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessPROOnly($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocation
		from hROUT a 
		LEFT JOIN mlocation b on a.intLocation=b.intID
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		where c.intUserID='$iduser' and a.vcRef<>'999'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailCRUByRoutIDandActivity($id,$activity)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dCRU2 a 
		LEFT JOIN hCRU2 b on a.intHID=b.intID
		where b.intROUT='$id' and a.intActivity='$activity'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dROUT a 
		LEFT JOIN mactivity b on a.intActivity=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailCRUByRoutID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from dCRU2 a 
		LEFT JOIN hCRU2 b on a.intHID=b.intID
		where b.intROUT='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetCostByDoc($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intCost from hROUT where vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  $r = $q->row();
		  return $r->intCost;
		}
		else
		{
			return 0;
		}
	}
	function GetHeaderByLoc($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllCRUByLoc($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcDocNum, b.vcRef, b.intLocation, b.vcLocation, b.vcItemCode, b.vcItemName, b.intQty, b.intID as RoutID, 
		b.intQty as ROUTQty, b.vcRemarks as ROUTRemarks from hCRU2 a 
		LEFT JOIN hROUT b on a.intROUT=b.intID
		where b.intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByLocJustCost($loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intLocation='$loc' and vcRef='999'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function CekHeaderByItemAndLocJustCost($itm,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intLocation='$loc' and vcRef='999' and intItem='$itm'
		");
		
		if($q->num_rows()>0)
		{
		  return 1;
		}
		else
		{
			return null;
		}
	}
	function GetHeaderByItemAndLocJustCost($itm,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intLocation='$loc' and vcRef='999' and intItem='$itm'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function cekrev($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hROUT where intItem='$d[FGItemID]' and vcRef='$d[Rev]'
		");
		
		if($q->num_rows()>0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['FGItem']=str_replace("'","''",$d['FGItem']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hROUT');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hROUT (vcDocNum,vcRef,dtDate,intLocation,vcLocation,intItem,vcItemCode,vcItemName,intQty,intCost,
		vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[Rev]','$d[DocDate]','$d[Whs]',
		(select vcName from mlocation where intID='$d[Whs]'),(select intID from mitem where vcName='$d[FGItem]'),(select vcCode from mitem where vcName='$d[FGItem]'),'$d[FGItem]',
		'$d[FGQty]','$d[FGCost]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['activitycodeROUT']=str_replace("'","''",$d['activitycodeROUT']);
		$d['activitynameROUT']=str_replace("'","''",$d['activitynameROUT']);
		$d['uomROUT']=str_replace("'","''",$d['uomROUT']);
		$q=$this->db->query("insert into dROUT (intHID,intActivity,vcActivityCode,vcActivityName,intQty,vcUoM,vcSourceCost,intCost,intAutoIssue)
		values ('$d[intHID]',(select intID from mactivity where vcCode='$d[activitycodeROUT]'),
		'$d[activitycodeROUT]','$d[activitynameROUT]','$d[qtyROUT]','$d[uomROUT]','$d[sourceROUT]','$d[costROUT]','$d[autoROUT]')
		");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['activitycodeROUT']=str_replace("'","''",$d['activitycodeROUT']);
		$d['activitynameROUT']=str_replace("'","''",$d['activitynameROUT']);
		$d['uomROUT']=str_replace("'","''",$d['uomROUT']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dROUT';
		$his['doc']			= 'ROUT';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dROUT where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['activitycodeROUT'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$q=$this->db->query("update dROUT set
		intActivity=(select intID from mactivity where vcCode='$d[activitycodeROUT]'),
		vcActivityCode='$d[activitycodeROUT]',
		vcActivityName='$d[activitynameROUT]',
		intQty='$d[qtyROUT]',
		vcUoM='$d[uomROUT]',
		vcSourceCost='$d[sourceROUT]',
		intCost='$d[costROUT]',
		intAutoIssue='$d[autoROUT]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hROUT';
		$his['doc']			= 'ROUT';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hROUT set
		vcRef='$d[Rev]',
		intLocation='$d[Whs]',
		vcLocation=(select vcName from mlocation where intID='$d[Whs]'),
		intQty='$d[FGQty]',
		vcRemarks='$d[Remarks]'
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertCRU($d)
	{
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		$q=$this->db->query("insert into hCRU2 (intROUT,dtDate,intCostBefore,vcUser,dtInsertTime)
		values ('$d[intROUT]','$d[dtDate]','$d[intCostBefore]','$d[UserID]','$now')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertDCRU($d)
	{
		$q=$this->db->query("insert into dCRU2 (intHID,intActivity,vcActivityCode,vcActivityName,intQty,vcUoM,intCost)
		values ('$d[intHID]','$d[intActivity]','$d[vcActivityCode]','$d[vcActivityName]','$d[intQty]','$d[vcUoM]','$d[intCost]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function updateCost($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hROUT set intCost='$cost' where intID='$intID'");
	}
	function updatecostCRU($intID,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hCRU2 set intCost='$cost' where intID='$intID'");
	}
	function emptyCRU()
	{
		$this->db->query("delete from dCRU2");
		$this->db->query("delete from hCRU2");
		
		$this->db->query("ALTER TABLE dCRU2 AUTO_INCREMENT = 1");
		$this->db->query("ALTER TABLE hCRU2 AUTO_INCREMENT = 1");
		
	}
	function delCRU($id)
	{
		$this->db->query("delete from dCRU2 where intHID='$id'");
		$this->db->query("delete from hCRU2 where intID='$id'");
		
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dROUT where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */