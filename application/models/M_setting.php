<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_setting extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function getValueByCode($Code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcValue as data from msetting where vcCode='$Code'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->data;
		}
		else
		{
			return '';
		}
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from msetting
		");
		
		return $q;
	}
	function GetAllLang()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mlanguage where intDeleted=0
		");
		
		return $q;
	}
	function Update($code,$value)
	{
		$db=$this->load->database('default', TRUE);
		$value=str_replace("'","''",$value);
		$q=$db->query("update msetting set vcValue='$value' where vcCode='$code'");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */