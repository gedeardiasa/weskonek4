<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sls_report extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	/*
	
	jenis transaksi barang
	
	IT=inventory transfer
	AD=Adjustment
	DN=Delivery
	GO=Grpo/Kedatangan Barang
	RE=Retur Penjualan
	RP=Retur Pembelian
	
	
	*/
	function GetRepIA($d)
	{
		if($d['RefI']=='DN')
		{
			$tabH='hDN';
			$tabD='dDN';
		}
		else if($d['RefI']=='AR')
		{
			$tabH='hAR';
			$tabD='dAR';
		}
		else if($d['RefI']=='ARCM')
		{
			$tabH='hARCM';
			$tabD='dARCM';
		}
		else if($d['RefI']=='SR')
		{
			$tabH='hSR';
			$tabD='dSR';
		}
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		a.`vcItemCode`, a.`vcItemName`, SUM(a.`intQtyInv`) AS intQtyInv,
		a.`vcUoMInv`, SUM(a.intLineTotal) AS intLineTotal, SUM(a.`intLineCost`) AS intLineCost,
		SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`) AS intGrossProfit,
		(SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`))/SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)))*100 AS intGrossPercent
		FROM $tabD a
		LEFT JOIN $tabH b ON a.`intHID`=b.intID
		LEFT JOIN (mitem c 
			LEFT JOIN mitemcategory d on c.intCategory=d.intID
		)
		on a.intItem=c.intID
		LEFT JOIN mlocation e on a.intLocation=e.intID
		where a.vcItemCode like '%$d[ItemCodeI]%' and a.vcItemName like '%$d[ItemNameI]%' and
		(d.intID='$d[ItemGroupI]' or $d[ItemGroupI]=0) and
		(e.intPlan='$d[PlanI]' or $d[PlanI]=0) and
		b.dtDate>='$d[fromI]' and b.dtDate<='$d[untilI]'
		and b.vcStatus!='X'
		GROUP BY a.`vcItemCode`, a.`vcItemName`, a.`vcUoMInv`
		");

		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetRepCA($d)
	{
		if($d['RefC']=='DN')
		{
			$tabH='hDN';
			$tabD='dDN';
		}
		else if($d['RefC']=='AR')
		{
			$tabH='hAR';
			$tabD='dAR';
		}
		else if($d['RefC']=='ARCM')
		{
			$tabH='hARCM';
			$tabD='dARCM';
		}
		else if($d['RefC']=='SR')
		{
			$tabH='hSR';
			$tabD='dSR';
		}
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		b.`vcBPCode`, b.`vcBPName`, SUM(a.`intQtyInv`) AS intQtyInv,
		a.`vcUoMInv`, SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) AS intLineTotal, SUM(a.`intLineCost`) AS intLineCost,
		SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`) AS intGrossProfit,
		(SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))) - SUM(a.`intLineCost`))/SUM((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)))*100 AS intGrossPercent
		FROM $tabD a
		LEFT JOIN $tabH b ON a.`intHID`=b.intID
		LEFT JOIN (mitem c 
			LEFT JOIN mitemcategory d on c.intCategory=d.intID
		)
		on a.intItem=c.intID
		LEFT JOIN (mbp e 
			LEFT JOIN mbpcategory f on e.intCategory=f.intID
		)on b.intBP=e.intID
		LEFT JOIN mlocation g on a.intLocation=g.intID
		where e.intDeleted=0 and b.vcBPCode like '%$d[BPCodeC]%' and b.vcBPName like '%$d[BPNameC]%' and
		(f.intID='$d[BPGroupC]' or $d[BPGroupC]=0) and
		(g.intPlan='$d[PlanC]' or $d[PlanC]=0) and
		b.dtDate>='$d[fromC]' and b.dtDate<='$d[untilC]'
		and b.vcStatus!='X'
		GROUP BY b.`vcBPCode`, b.`vcBPName`, a.`vcUoMInv`
		");

		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetRepG($d)
	{
		if($d['RefG']=='DN')
		{
			$tabH='hDN';
			$tabD='dDN';
		}
		else if($d['RefG']=='AR')
		{
			$tabH='hAR';
			$tabD='dAR';
		}
		else if($d['RefG']=='ARCM')
		{
			$tabH='hARCM';
			$tabD='dARCM';
		}
		else if($d['RefG']=='SR')
		{
			$tabH='hSR';
			$tabD='dSR';
		}
		
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT 
		b.vcDocNum,
		a.`vcItemCode`, a.`vcItemName`,
		b.`vcBPCode`, b.`vcBPName`,a.intQtyInv,
		a.`vcUoMInv`, a.intLineTotal-(b.intDiscPer/100*a.intLineTotal) as intLineTotal, a.intLineCost,
		(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) - a.`intLineCost` AS intGrossProfit,
		((a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) - a.`intLineCost`)/(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal))*100 AS intGrossPercent
		FROM $tabD a
		LEFT JOIN $tabH b ON a.`intHID`=b.intID
		LEFT JOIN (mitem c 
			LEFT JOIN mitemcategory d on c.intCategory=d.intID
		)
		on a.intItem=c.intID
		LEFT JOIN (mbp e 
			LEFT JOIN mbpcategory f on e.intCategory=f.intID
		)on b.intBP=e.intID
		LEFT JOIN mlocation g on a.intLocation=g.intID
		where 
		a.vcItemCode like '%$d[ItemCodeG]%' and a.vcItemName like '%$d[ItemNameG]%' and
		e.intDeleted=0 and b.vcBPCode like '%$d[BPCodeG]%' and b.vcBPName like '%$d[BPNameG]%' and
		(f.intID='$d[BPGroupG]' or $d[BPGroupG]=0) and
		(d.intID='$d[ItemGroupG]' or $d[ItemGroupG]=0) and
		(g.intPlan='$d[PlanG]' or $d[PlanG]=0) and
		b.dtDate>='$d[fromG]' and b.dtDate<='$d[untilG]'
		and b.vcStatus!='X'
		
		");

		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */