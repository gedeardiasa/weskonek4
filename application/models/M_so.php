<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_so extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		h.abc,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN
		(
			SELECT 
			SUM(a.abc) AS abc, a.vcDocNum, a.vcBPName
			FROM
			(
			SELECT
			a.intHID, CASE WHEN a.intOpenQty<=a.intLoadQty THEN 0
			ELSE 1 END AS abc, b.vcDocNum, b.vcBPName
			FROM
			dSO a
			LEFT JOIN hSO b ON a.intHID=b.intID
			WHERE b.vcStatus='O' AND a.vcStatus='O'
			) a
			GROUP BY a.intHID
		) h on a.vcDocNum=h.vcDocNum
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetLocaByIDSO($idso)
	{
		$q=$this->db->query("SELECT 
		intLocation
		FROM dSO WHERE intHID='$idso'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->intLocation;
		}
		else
		{
			return 0;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithItemAndLoc($item,$loc)
	{
		$q=$this->db->query("SELECT 
		b.`vcDocNum`, b.`vcBPName`, a.intOpenQty
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.intID
		WHERE a.`vcStatus`='O' AND a.`intItem`=$item AND a.`intLocation`=$loc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName, c.vcName as TableName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN mtable c on a.intTable=c.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessNotService($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName, c.vcName as TableName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN mtable c on a.intTable=c.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O' and a.intService=0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName, c.vcName as TableName, d.vcLocation,
		d.vcItemCode, d.vcItemName, d.intQty, d.intOpenQty, d.intQtyInv, d.intOpenQtyInv,
		e.blpImage, e.blpImageMin, d.vcUoM, d.intItem, d.intLoadQty, d.intCreated
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN mtable c on a.intTable=c.intID
		LEFT JOIN dSO d on a.intID=d.intHID
		LEFT JOIN mitem e on d.intItem=e.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessAndTable($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName, c.vcName as TableName
		from hSO a 
		LEFT JOIN mbp b on a.intBP=b.intID
		LEFT JOIN mtable c on a.intTable=c.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O' and a.intTable<>0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetWhsNameByHeader($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select distinct(vcLocation) as data from dSO where intHID='$id' limit 0,1
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->data;
		}
		else
		{
			return "";
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dSO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hSO c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderIDandItem($id,$item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dSO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hSO c on a.intHID=c.intID
		where a.intHID='$id' and a.intItem='$item'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hSO a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hSO a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hSO');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcBillToCity;
		$Country = $adrs->vcBillToCountry;
		$AddressH = $adrs->vcBillToAddress;
		
		$q=$this->db->query("insert into hSO (intTable,intBP,intService,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime,vcCity,vcCountry,vcAddress  )
		values ('$d[Table]','$d[BPId]','$d[Service]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now','$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hSO set vcStatus='C' where intID='$id'
		");
		$q2=$this->db->query("
		update dSO set vcStatus='C' where intHID='$id'
		");
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hSO set vcStatus='$status' where intID='$id'
		");
	}
	function reOpenDetail($intHID,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update dSO set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
		");
	}
	function cekclose($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		if(is_numeric($item)==true)
		{
			$varitem = 'intItem';
		}
		else
		{
			$varitem = 'vcItemName';
		}
		$this->db->query("
		update dSO set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and $varitem='$item'
		");// query update open qty SO
		
		
		$this->db->query("
		update dSO set
		vcStatus='C'
		where intHID='$id' and $varitem='$item' and intOpenQty<=0
		");// query update status dSO
		
		$cekdetailsq=$this->db->query("
		select intID from dSO where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dSO yang statusnya open
		{
			$this->db->query("
			update hSO set
			vcStatus='C'
			where intID='$id'
			");// maka update hSO status ke 'C' (Close)
			
			/*$lastdata=$this->GetHeaderByHeaderID($id);
			$data['DocTotalBP']=$lastdata->intDocTotal*-1;
			$data['BPId']=$lastdata->intBP;
			$this->load->model('m_bp', 'bp');
			$this->bp->updateBDO($data['BPId'],'intOrder',$data['DocTotalBP']);*/
		}
	}
	function updateLoadDetail($id,$item)
	{
		$q=$this->db->query("
		update dSO set intLoadQty=intQty where intHID='$id' and intItem='$item'
		");
	}
	function updateCreated($id,$item)
	{
		$q=$this->db->query("
		update dSO set intCreated=1 where intHID='$id' and intItem='$item'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hSO';
		$his['doc']			= 'SO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hSO set
		intTable='$d[Table]',
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeSO']=str_replace("'","''",$d['itemcodeSO']);
		$d['itemnameSO']=str_replace("'","''",$d['itemnameSO']);
		
		$d['uomSO']=str_replace("'","''",$d['uomSO']);
		$d['uominvSO']=str_replace("'","''",$d['uominvSO']);
		
		if(!isset($d['qtyLoadSO']))
		{
			$d['qtyLoadSO']=0;
		}
		
		
		//cek existing itemcode, kalau ada maka detail SO diubah bukan di tambah
		$cekdouble=$this->db->query("select * from dSO where intHID='$d[intHID]' and vcItemCode='$d[itemcodeSO]'
		");
		if($cekdouble->num_rows()>0)
		{
			$q=$this->db->query("update dSO set
			intItem='$d[itemID]',
			vcBaseType='$d[BaseRefSO]',
			vcItemCode='$d[itemcodeSO]',
			vcItemName='$d[itemnameSO]',
			intQty='$d[qtySO]',
			intOpenQty='$d[qtySO]',
			vcUoM='$d[uomSO]',
			intUoMType='$d[uomtypeSO]',
			intQtyInv='$d[qtyinvSO]',
			intOpenQtyInv='$d[qtyinvSO]',
			vcUoMInv='$d[uominvSO]',
			intPrice='$d[priceSO]',
			intDiscPer='$d[discperSO]',
			intDisc='$d[discSO]',
			intPriceAfterDisc='$d[priceafterSO]',
			intLineTotal='$d[linetotalSO]',
			intLocation='$d[whsSO]',
			vcLocation='$d[whsNameSO]'
			where intHID='$d[intHID]' and vcItemCode='$d[itemcodeSO]'
			");
		}
		else{
			$q=$this->db->query("insert into dSO (intHID,intBaseRef,vcBaseType,intItem,vcItemCode,vcItemName,intQty,intOpenQty,intLoadQty,vcUoM,
			intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intLocation,vcLocation)
			values ('$d[intHID]','$d[idBaseRefSO]','$d[BaseRefSO]','$d[itemID]','$d[itemcodeSO]','$d[itemnameSO]','$d[qtySO]','$d[qtySO]','$d[qtyLoadSO]','$d[uomSO]',
			'$d[uomtypeSO]','$d[qtyinvSO]','$d[qtyinvSO]','$d[uominvSO]','$d[priceSO]','$d[discperSO]','$d[discSO]','$d[priceafterSO]',
			'$d[linetotalSO]','$d[whsSO]','$d[whsNameSO]')
			");
		}
		
		
		
		if($q)
		{
			return $q;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeSO']=str_replace("'","''",$d['itemcodeSO']);
		$d['itemnameSO']=str_replace("'","''",$d['itemnameSO']);
		
		$d['uomSO']=str_replace("'","''",$d['uomSO']);
		$d['uominvSO']=str_replace("'","''",$d['uominvSO']);
		
		
		if(!isset($d['qtyLoadSO']))
		{
			$d['qtyLoadSO']=0;
			$edit="";
		}
		else
		{
			$edit="intLoadQty='$d[qtyLoadSO]',";
		}
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dSO';
		$his['doc']			= 'SO';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dSO where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodeSO'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dSO set
		intItem='$d[itemID]',
		vcBaseType='$d[BaseRefSO]',
		vcItemCode='$d[itemcodeSO]',
		vcItemName='$d[itemnameSO]',
		intQty='$d[qtySO]',
		intOpenQty='$d[qtySO]',
		".$edit."
		vcUoM='$d[uomSO]',
		intUoMType='$d[uomtypeSO]',
		intQtyInv='$d[qtyinvSO]',
		intOpenQtyInv='$d[qtyinvSO]',
		vcUoMInv='$d[uominvSO]',
		intPrice='$d[priceSO]',
		intDiscPer='$d[discperSO]',
		intDisc='$d[discSO]',
		intPriceAfterDisc='$d[priceafterSO]',
		intLineTotal='$d[linetotalSO]',
		intLocation='$d[whsSO]',
		vcLocation='$d[whsNameSO]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return $q;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dSO where intID='$id'
		");
	}
	function deleteD2($id,$notdelitem)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dSO where intHID='$id' and intItem not in $notdelitem
		");
	}
	
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */