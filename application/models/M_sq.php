<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sq extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSQ a 
		LEFT JOIN mbp b on a.intBP=b.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		from hSQ a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSQ a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSQ a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSQ a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSQ a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSQ a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccessNotService($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcCode as BPCode, b.vcName as BPName
		from hSQ a 
		LEFT JOIN mbp b on a.intBP=b.intID
		where a.intID not in
		(
			SELECT a.intHID
			FROM dSQ a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O' and a.intService=0
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.intService from dSQ a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hSQ c on a.intHID=c.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hSQ a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hSQ a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return null;
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['BPCode']=str_replace("'","''",$d['BPCode']);
		$d['BPName']=str_replace("'","''",$d['BPName']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['Service'] = isset($d['Service'])?$d['Service']:0; // get the requested page
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hSQ');
		// end cek DocNum kembar
		
		$this->load->model('m_bp', 'bp');
		$adrs = $this->bp->GetAddressByCode($d['BPCode']);
		$CityH = $adrs->vcBillToCity;
		$Country = $adrs->vcBillToCountry;
		$AddressH = $adrs->vcBillToAddress;
		
		$q=$this->db->query("insert into hSQ (intBP,intService,vcBPCode,vcBPName,vcSalesName,vcDocNum,dtDate,  
		dtDelDate,vcRef,intFreight,intTaxPer,intTax,vcStatus,intDiscPer,intDisc,  
		intDocTotalBefore,intDocTotal,vcRemarks,vcUser,dtInsertTime ,vcCity,vcCountry,vcAddress )
		values ('$d[BPId]','$d[Service]','$d[BPCode]','$d[BPName]','$d[SalesEmp]','$d[DocNum]','$d[DocDate]',
		'$d[DelDate]','$d[RefNum]','$d[Freight]','$d[TaxPer]','$d[Tax]','O','$d[DiscPer]','$d[Disc]',
		'$d[DocTotalBefore]','$d[DocTotal]','$d[Remarks]','$d[UserID]','$now',
		'$CityH','$Country','$AddressH')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function updatestatusH($id,$status)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		update hSQ set vcStatus='$status' where intID='$id'
		");
		
	}
	function reOpenDetail($intHID,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		update dSQ set vcStatus='O', intOpenQty=intOpenQty+'$qty', intOpenQtyInv=intOpenQtyInv+'$qtyinv' where intHID='$intHID' and intItem='$item'
		");
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		update hSQ set vcStatus='C' where intID='$id'
		");

		$q2=$this->db->query("
		update dSQ set vcStatus='C' where intHID='$id'
		");
	}
	function cekclose($id,$item,$qty,$qtyinv)
	{
		$db=$this->load->database('default', TRUE);
		
		if(is_numeric($item)==true)
		{
			$varitem = 'intItem';
		}
		else
		{
			$varitem = 'vcItemName';
		}
		$this->db->query("
		update dSQ set
		intOpenQty=intOpenQty-$qty,
		intOpenQtyInv=intOpenQtyInv-$qtyinv
		where intHID='$id' and $varitem='$item'
		");// query update open qty SQ
		
		$this->db->query("
		update dSQ set
		vcStatus='C'
		where intHID='$id' and $varitem='$item' and intOpenQty<=0
		");// query update status dSQ
		
		$cekdetailsq=$this->db->query("
		select intID from dSQ where intHID='$id' and vcStatus='O'
		");
		
		if($cekdetailsq->num_rows()==0)// jika tidak ada dSQ yang statusnya open
		{
			$this->db->query("
			update hSQ set
			vcStatus='C'
			where intID='$id'
			");// maka update hSQ status ke 'C' (Close)
		}
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['DelDate']=str_replace("'","''",$d['DelDate']);
		$d['SalesEmp']=str_replace("'","''",$d['SalesEmp']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hSQ';
		$his['doc']			= 'SQ';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$q=$this->db->query("
		
		update hSQ set
		vcRef='$d[RefNum]',
		dtDate='$d[DocDate]',
		dtDelDate='$d[DelDate]',
		vcSalesName='$d[SalesEmp]',
		vcRemarks='$d[Remarks]',
		intFreight='$d[Freight]',
		intTaxPer='$d[TaxPer]',
		intTax='$d[Tax]',
		intDiscPer='$d[DiscPer]',
		intDisc='$d[Disc]',
		intDocTotalBefore='$d[DocTotalBefore]',
		intDocTotal='$d[DocTotal]'
		
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeSQ']=str_replace("'","''",$d['itemcodeSQ']);
		$d['itemnameSQ']=str_replace("'","''",$d['itemnameSQ']);
		
		$d['uomSQ']=str_replace("'","''",$d['uomSQ']);
		$d['uominvSQ']=str_replace("'","''",$d['uominvSQ']);
		
		$q=$this->db->query("insert into dSQ (intHID,intItem,vcItemCode,vcItemName,intQty,intOpenQty,vcUoM,
		intUoMType,intQtyInv,intOpenQtyInv,vcUoMInv,intPrice,intDiscPer,intDisc,intPriceAfterDisc,intLineTotal, intLocation,vcLocation)
		values ('$d[intHID]','$d[itemID]','$d[itemcodeSQ]','$d[itemnameSQ]','$d[qtySQ]','$d[qtySQ]','$d[uomSQ]',
		'$d[uomtypeSQ]','$d[qtyinvSQ]','$d[qtyinvSQ]','$d[uominvSQ]','$d[priceSQ]','$d[discperSQ]','$d[discSQ]','$d[priceafterSQ]',
		'$d[linetotalSQ]','$d[whsSQ]','$d[whsNameSQ]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeSQ']=str_replace("'","''",$d['itemcodeSQ']);
		$d['itemnameSQ']=str_replace("'","''",$d['itemnameSQ']);
		
		$d['uomSQ']=str_replace("'","''",$d['uomSQ']);
		$d['uominvSQ']=str_replace("'","''",$d['uominvSQ']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dSQ';
		$his['doc']			= 'SQ';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dSQ where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemcodeSQ'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		
		$q=$this->db->query("update dSQ set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeSQ]',
		vcItemName='$d[itemnameSQ]',
		intQty='$d[qtySQ]',
		intOpenQty='$d[qtySQ]',
		vcUoM='$d[uomSQ]',
		intUoMType='$d[uomtypeSQ]',
		intQtyInv='$d[qtyinvSQ]',
		intOpenQtyInv='$d[qtyinvSQ]',
		vcUoMInv='$d[uominvSQ]',
		intPrice='$d[priceSQ]',
		intDiscPer='$d[discperSQ]',
		intDisc='$d[discSQ]',
		intPriceAfterDisc='$d[priceafterSQ]',
		intLineTotal='$d[linetotalSQ]',
		intLocation='$d[whsSQ]',
		vcLocation='$d[whsNameSQ]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dSQ where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */