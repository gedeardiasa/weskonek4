<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_stock extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	
	/*
	
	jenis transaksi barang
	
	IT=inventory transfer
	AD=Adjustment
	DN=Delivery
	GO=Grpo/Kedatangan Barang
	RE=Retur Penjualan
	RP=Retur Pembelian
	
	
	*/
	function GetCostItem($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intHPP from mstock where intItem='$item' and intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intHPP;
		}
		else
		{
			return 0;
		}
	}
	function GetCostAverage($item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT IFNULL(AVG(intHPP),0) AS intHPP FROM mstock WHERE intItem='$item' AND intStock>0 
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intHPP;
		}
		else
		{
			return 0;
		}
	}
	function GetCostMax($item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT IFNULL(MAX(intHPP),0) AS intHPP FROM mstock WHERE intItem='$item' AND intStock>0 
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intHPP;
		}
		else
		{
			return 0;
		}
	}
	function GetCostMin($item)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT IFNULL(MIN(intHPP),0) AS intHPP FROM mstock WHERE intItem='$item' AND intStock>0 
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intHPP;
		}
		else
		{
			return 0;
		}
	}
	function GetStockItem($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intStock from mstock where intItem='$item' and intLocation='$loc'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intStock;
		}
		else
		{
			return 0;
		}
	}
	function GetStockItemJustToShow($item,$loc)
	{
		$db=$this->load->database('default', TRUE);
		
		$qcekset = $this->db->query("select vcValue from msetting where vcCode='stock_check_so'
		"); // cek apakan menghitung stock perlu dikurangi Open SO
		$rcekset = $qcekset->row();
		$hcekset = $rcekset->vcValue;
		
		if($hcekset==1)
		{
			$q=$this->db->query("
			SELECT a.intStock-IFNULL(d.intOpenQtyInv,0) as intStock FROM mstock a
			LEFT JOIN
			(
				SELECT intItem, intLocation, SUM(intOpenQtyInv) AS intOpenQtyInv FROM dSO
				WHERE vcStatus='O'
				GROUP BY intItem, intLocation
			) d ON a.intItem=d.intItem AND a.intLocation=d.intLocation
			WHERE a.intItem='$item' AND a.intLocation='$loc'
			
			");
		}
		else
		{
			$q=$this->db->query("select intStock from mstock where intItem='$item' and intLocation='$loc'
			");
		}
		
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intStock;
		}
		else
		{
			return 0;
		}
	}
	function GetMutUp($item,$loc,$dt)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select SUM(intQty) as intQty from dMutation where intItem='$item' and intLocation='$loc' and dtPost>'$dt'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intQty;
		}
		else
		{
			return 0;
		}
	}
	function GetDataStockReport($data)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select a.*, b.vcCode as ItemCode, b.vcName as ItemName, b1.vcName as GroupName,
		c.vcCode as LocationCode, c.vcName as LocationName,
		b.vcUoM, b.vcSlsUoM, b.vcPurUoM, b.intPurUoM, b.intSlsUoM, d.intOpenQtyInv as OpenSO
		from mstock a
		LEFT JOIN (mitem b
		LEFT JOIN mitemcategory b1 on b.intCategory=b1.intID
		) on a.intItem=b.intID
		LEFT JOIN mlocation c on a.intLocation = c.intID
		LEFT JOIN
		(
			SELECT intItem, intLocation, SUM(intOpenQtyInv) AS intOpenQtyInv FROM dSO
			where vcStatus='O'
			GROUP BY intItem, intLocation
		) d on a.intItem=d.intItem and a.intLocation=d.intLocation
		where b.intDeleted = 0 and b.vcCode like '%$data[ItemCode]%' and b.vcName like '%$data[ItemName]%' and  (b1.intID='$data[ItemGroup]' or $data[ItemGroup]=0)
		and (b.intActive='$data[active]' or b.intActive='$data[active]')
		$data[whs] $data[zero];
		");
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDataAuditStockReport($data)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select a.*, b.vcCode as ItemCode, b.vcName as ItemName, b1.vcName as GroupName,
		c.vcCode as LocationCode, c.vcName as LocationName,
		b.vcUoM, b.vcSlsUoM, b.vcPurUoM, b.intPurUoM, b.intSlsUoM
		from dMutation a
		LEFT JOIN (mitem b
		LEFT JOIN mitemcategory b1 on b.intCategory=b1.intID
		) on a.intItem=b.intID
		LEFT JOIN mlocation c on a.intLocation = c.intID
		where b.vcCode like '%$data[ItemCode]%' and b.vcName like '%$data[ItemName]%' and  (b1.intID='$data[ItemGroup]' or $data[ItemGroup]=0)
		and a.dtPost>='$data[from]' and a.dtPost<='$data[until]' and (a.vcMvt='$data[Mvt]' or '$data[Mvt]'='X')
		$data[whs] order by a.intItem asc, a.intLocation asc, a.dtPost asc, a.dtDate asc;
		");
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetStockCardHeader($data)
	{
		$db=$this->load->database('default', TRUE);
		// $q=$this->db->query("
		// SELECT ff.Period, ff.vcItemName, ff.vcItemCode, 
		// ff.vcCategory, ff.vcUoM, ff.intLastStock,
		// SUM(ff.QtyIn) AS QtyIn,
		// SUM(ff.QtyOut) AS QtyOut FROM
		// (
		// SELECT a.vcLocation, CONCAT(YEAR(a.`dtPost`),MONTH(a.dtPost)) AS Period, 
		// a.`vcItemName`, a.`intLastStock`, a.`intQty`, a.vcItemCode, c.vcName AS vcCategory,
		// b.vcUoM,
		// CASE WHEN a.intQty<0 THEN a.intQty
		// ELSE 0 END AS QtyOut,
		// CASE WHEN a.intQty>0 THEN a.intQty
		// ELSE 0 END AS QtyIn
		// FROM dMutation a
		// LEFT JOIN mitem b ON a.intItem=b.intID
		// LEFT JOIN mitemcategory c ON b.intCategory=c.intID
		// WHERE a.vcItemCode like '%$data[ItemCode]%' and a.vcItemName like '%$data[ItemName]%' and  (c.intID='$data[ItemGroup]' or $data[ItemGroup]=0)
		// ORDER BY a.`vcLocation`, a.`dtPost`, a.dtDate
		// ) ff
		// where ff.period='$data[Periode]'
		// GROUP BY ff.vcItemName, ff.Period
		// ");
		
		$period = $data['PeriodeYear']."-".$data['PeriodeMonth']."-1";
		$q=$this->db->query("
		SELECT 
		b.`vcCode`, b.vcName, b.vcUoM,
		c.`vcName` AS vcCategory,
		d.intLastStock,
		IFNULL(d.QtyIn,0) AS QtyIn,IFNULL(d.QtyOut,0) AS QtyOut,
		e.intStock,
		IFNULL(f.intOpenQtyInv,0) AS OpenSO,
		e.intStock-IFNULL(f.intOpenQtyInv,0) AS StockAvailable
		FROM mitem b
		LEFT JOIN mitemcategory c ON b.`intCategory`=c.`intID`
		LEFT JOIN 
		(
			SELECT 
			a.intItem, b.intStock - SUM(a.intQty) AS intLastStock, SUM(a.intQty) AS CekInOut,
			SUM(CASE WHEN a.intQty>0 THEN a.intQty ELSE 0 END ) AS QtyIn,
			SUM(CASE WHEN a.intQty<0 THEN a.intQty ELSE 0 END ) AS QtyOut
			FROM dMutation a
			INNER JOIN (
				SELECT a1.intItem, SUM(a1.`intStock`) AS intStock FROM mstock a1
				GROUP BY a1.`intItem`
			) b ON a.`intItem`=b.intItem
			WHERE dtPost>='$period'
			GROUP BY a.`intItem`
		) d ON b.`intID`=d.intItem
		LEFT JOIN 
		(
			SELECT a1.intItem, SUM(a1.`intStock`) AS intStock FROM mstock a1
			GROUP BY a1.`intItem`
		) e ON b.`intID`=e.intItem
		LEFT JOIN
		(			
			SELECT intItem, SUM(intOpenQtyInv) AS intOpenQtyInv FROM dSO
			WHERE vcStatus='O'
			GROUP BY intItem
		) f ON b.intID=f.intItem
		WHERE b.vcCode like '%$data[ItemCode]%' and b.vcName like '%$data[ItemName]%' and  (c.intID='$data[ItemGroup]' or $data[ItemGroup]=0)
				");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetStockCardDetail($data)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT ff.vcLocation, ff.Period, ff.vcItemName, ff.vcItemCode, 
		ff.vcCategory, ff.vcUoM, ff.intLastStock,
		SUM(ff.QtyIn) AS QtyIn,
		SUM(ff.QtyOut) AS QtyOut FROM
		(
		SELECT a.vcLocation, CONCAT(YEAR(a.`dtPost`),MONTH(a.dtPost)) AS Period, 
		a.`vcItemName`, a.`intLastStock`, a.`intQty`, a.vcItemCode, c.vcName AS vcCategory,
		b.vcUoM,
		CASE WHEN a.intQty<0 THEN a.intQty
		ELSE 0 END AS QtyOut,
		CASE WHEN a.intQty>0 THEN a.intQty
		ELSE 0 END AS QtyIn
		FROM dMutation a
		LEFT JOIN mitem b ON a.intItem=b.intID
		LEFT JOIN mitemcategory c ON b.intCategory=c.intID
		WHERE a.vcItemCode ='$data[itemcode]'
		ORDER BY a.`vcLocation`, a.`dtPost`, a.dtDate
		) ff
		where ff.period='$data[Periode]'
		GROUP BY ff.vcLocation, ff.vcItemName, ff.Period
		");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function cekMinusStock($item,$qty,$loc,$date='')
	{
		$db=$this->load->database('default', TRUE);
		
		if($date=='')
		{
			$date=date('Y-m-d');
		}
		
		// hitung mutasi stock setelah tgl terpilih (biasanya untuk backdate)
		$hsummut = 0;
		$qsummut = $this->db->query("
		select IFNULL(SUM(intQty),0) as qty 
		from dMutation where intItem='$item' and intLocation='$loc' and dtPost>'$date'");
		if($qsummut->num_rows()>0)
		{
			$rsummut = $qsummut->row();
			$hsummut = $rsummut->qty;
		}
		
		$qcekset = $this->db->query("select vcValue from msetting where vcCode='stock_check_so'
		"); // cek apakan menghitung stock perlu dikurangi Open SO
		$rcekset = $qcekset->row();
		$hcekset = $rcekset->vcValue;
		//echo 'XXX';
		if($hcekset==1)
		{
			$q=$this->db->query("
			SELECT a.*, d.intOpenQtyInv FROM mstock a
			LEFT JOIN
			(
				SELECT intItem, intLocation, SUM(intOpenQtyInv) AS intOpenQtyInv FROM dSO
				WHERE vcStatus='O'
				GROUP BY intItem, intLocation
			) d ON a.intItem=d.intItem AND a.intLocation=d.intLocation
			WHERE a.intItem='$item' AND a.intLocation='$loc' AND intStock-IFNULL(d.intOpenQtyInv,0)-$hsummut>='$qty'
			
			");
		}
		else
		{
			$q=$this->db->query("select * from mstock where intItem='$item' and intLocation='$loc' and intStock-$hsummut>='$qty'
			");
			/*echo "select * from mstock where intItem='$item' and intLocation='$loc' and intStock-$hsummut>='$qty'
			";*/
		}
		
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function updateStock($item,$qty,$loc,$crtbatch=1)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mstock set intStock=intStock+$qty where intItem='$item' and intLocation='$loc'
		");
		
		$q=$this->db->query("update mstock set intStock2=intStock where intItem='$item' and intLocation='$loc'
		");
		// pembulatan
		$stock	= $this->GetStockItem($item,$loc);
		$stock	= round($stock,2);
		$q=$this->db->query("update mstock set intStock=$stock where intItem='$item' and intLocation='$loc'
		");
		
		// $q=$this->db->query("update mstock set intStock=FORMAT(intStock, 2) where intItem='$item' and intLocation='$loc'
		// ");
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function emptyStock($item,$loc,$crtbatch=1)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mstock set intStock=0 where intItem='$item' and intLocation='$loc'
		");
		
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function updateCost($item,$qty,$cost,$loc)
	{
		$db=$this->load->database('default', TRUE);
		
		$g=$this->db->query("select * from mstock where intItem='$item' and intLocation='$loc'
		");
		$r=$g->row();
		
		//rumus menghitung moving average
		$laststock=$r->intStock-$qty;
		$lastprice=$laststock*$r->intHPP;
		
		$addprice=$qty*$cost;
		$finalprice=$lastprice+$addprice;
		if($finalprice==0)
		{
			$finalpriceitem=0;
		}
		else
		{
			$finalpriceitem=$finalprice/$r->intStock;
		}	
		$q=$this->db->query("update mstock set intHPP='$finalpriceitem' where intItem='$item' and intLocation='$loc'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function recalculate($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*,
		a.intID as id
		FROM dMutation a
		where a.vcItemCode='$d[itemcode]' AND a.vcLocation='$d[location]' AND YEAR(a.dtDate)=$d[year]
		order by a.intItem asc, a.intLocation asc, a.dtPost asc, a.dtDate asc
		");
		
		$balance = $this->db->query("select intStock from mstock where intItem=(select intID from mitem where vcCode='$d[itemcode]') 
		and intLocation=(select intID from mlocation where vcName='$d[location]')
		");
		$balanceresult = $balance->row()->intStock;
		
		
		$idx=0;
		foreach($q->result() as $qq){
			$jj[$idx]['id']=$qq->id;
			$jj[$idx]['value']=$qq->intQty;
			$idx++;
		}
		
		for($i=$idx-1;$i>=0;$i--)
		{
			$valfix		= $jj[$i]['value'];
			$idfix		= $jj[$i]['id'];
			$this->db->query("update dMutation set intLastStock=$balanceresult - $valfix where vcItemCode='$d[itemcode]' AND vcLocation='$d[location]' and intID=$idfix");
			$balanceresult = $balanceresult - $valfix;
		}
		echo 'Success';
	}
	function reval($item,$cost,$loc)
	{
		$q=$this->db->query("update mstock set intHPP='$cost' where intItem='$item' and intLocation='$loc'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function addMutation($item,$qty,$cost,$loc,$type, $date='',$doc='', $mvt='')
	{
		$nowdt = date('Y-m-d');
		if($date=='')
		{
			$date=date('Y-m-d');
		}
		
		if($date<$nowdt) // jika backdate maka jam insert wajib di akhir jam pada hari tersebut
		{
			$now=date('Y-m-d 23:59:59');
		}
		else
		{
			$now=date('Y-m-d H:i:s');
		}
		
		$db=$this->load->database('default', TRUE);
		$itm=$this->db->query("select vcCode, vcName from mitem where intID='$item'
		");
		$ritem=$itm->row();
		$vcItemCode=$ritem->vcCode;
		$vcItemName=$ritem->vcName;
		
		$vcItemName=str_replace("'","''",$vcItemName);
		$vcItemCode=str_replace("'","''",$vcItemCode);
		
		$qls=$this->db->query("select intStock from mstock where intItem='$item' and intLocation='$loc'
		");
		$rls=$qls->row();
		if($date<$nowdt)// jika backdate maka last stock adalah stock terkahir dikurangi mutasi mulai tgl backdate sampai sekarang
		{
			$laststock = $rls->intStock-$qty;
			$qgetlast = $this->db->query("
			select SUM(intQty) as qty 
			from dMutation where intItem='$item' and intLocation='$loc' and dtPost>'$date'");
			if($qgetlast->num_rows()>0)
			{
				$rgetlast = $qgetlast->row();
				$hgetlast = $rgetlast->qty;
			}
			else
			{
				$hgetlast = 0;
			}
			$laststock = $laststock - $hgetlast;
		}
		else
		{
			$laststock = $rls->intStock-$qty;
		}
		$q=$this->db->query("insert into dMutation (vcType,intItem,intLocation,vcLocation,vcItemCode,vcItemName,intQty,intCost, dtDate, dtPost, vcDoc, intLastStock, vcMvt)
		values ('$type','$item','$loc',(select vcName from mlocation where intID='$loc'),'$vcItemCode','$vcItemName','$qty','$cost','$now','$date','$doc','$laststock','$mvt')
		");
		//cek jika backdate maka ubah intLastStock tgl sesudahnya menjadi intLastStock + Qty
		if($date<$nowdt)
		{
			$q7=$this->db->query("update dMutation set intLastStock=intLastStock+$qty where intItem='$item' and intLocation='$loc' and dtPost>'$date'
			");
		}
		//
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		return $idtin;
		
	}
	function addItem($iditem)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mlocation where intDeleted=0
		");
		
		foreach($q->result() as $c)
		{
			$idloc=$c->intID;
			$in=$this->db->query("insert into mstock (intLocation,intItem) values ('$idloc','$iditem')
			");
		}
	}
	function addLoc($idloc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mitem where intDeleted=0
		");
		
		foreach($q->result() as $c)
		{
			$iditem=$c->intID;
			$in=$this->db->query("insert into mstock (intLocation,intItem) values ('$idloc','$iditem')
			");
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */