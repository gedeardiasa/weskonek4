<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sub_form extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllForm()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcHeader from msubform a LEFT JOIN mform b on a.intHeader=b.intID where a.intDeleted=0  and b.intDeleted=0 order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from msubform a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetHeader()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mform a where a.intDeleted=0 and isHeader=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into msubform (vcName,vcCode,vcRemarks,intHeader)
		values ('$d[Name]','$d[Code]','$d[Remarks]','$d[Header]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$q=$this->db->query("update msubform set vcName='$d[Name]', vcCode='$d[Code]', vcRemarks='$d[Remarks]', 
		intHeader='$d[Header]' where intID='$d[id]'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update msubform set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */