<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_table extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mtable a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mtable a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	
	function getAccessAllPlan()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT * FROM mplan WHERE intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function addnewaccessplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from mtableplan where intTable='$id' and intPlan='$plan'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into mtableplan (intTable,intPlan)
			values ('$id','$plan')
			");
		}
		return 1;
	}
	function dellastaccessbyidplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from mtableplan where intTable='$id' and intPlan='$plan'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function getAccessPlan($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.PlanID ,
		CASE WHEN b.PlanID>0 THEN 'checked=checked' ELSE '' END AS OpenForm
		
		FROM mplan a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS PlanID
			FROM mtable a LEFT JOIN 
			(mtableplan b LEFT JOIN mplan c ON b.`intPlan`=c.`intID`) ON a.`intID`=b.`intTable`
			WHERE a.intID='$id'
		) b ON a.`intID`=b.PlanID
		WHERE a.intDeleted=0
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, c.`intUserID` FROM mtable a
		LEFT JOIN mtableplan b ON a.`intID`=b.`intTable`
		LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE c.`intUserID`='$iduser'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mtable (vcName,dtInsertTime)
		values ('$d[Name]','$now')
		");
		if($q)
		{
		  $id=$this->db->query("select LAST_INSERT_ID() as intID");
		  $rid=$id->row();
		  $idtin=$rid->intID;
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Name']=str_replace('"','',$d['Name']);
		$q=$this->db->query("update mtable set vcName='$d[Name]' where intID='$d[id]'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mtable set intDeleted=1, vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */