<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tax extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.intActive=1 then 'Yes' else 'No' end as vcActive
		from mtax a where a.intDeleted=0  order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetActiveData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mtax a where a.intDeleted=0  and intActive=1 order by a.intID asc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetTaxCoaById($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mtaxcoa where intTax='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mtax a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mtax where vcCode='$code'
		");
		if($q->num_rows()>0)
		{
		  return $q->row()->intID;
		}
		else
		{
			return 0;
		}
	}
	function GetCoaTaxByRate($rate)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT 
		a.*
		FROM mtaxcoa a
		LEFT JOIN mtax b ON a.`intTax`=b.`intID`
		WHERE b.`intRate`='$rate'
		");
		if($q->num_rows()>0)
		{
		    return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$q=$this->db->query("insert into mtax (vcCode,vcName,intRate,intActive,intDeleted)
		values ('$d[Code]','$d[Name]','$d[Rate]','1','0')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mtaxcoa (intTax,vcGlAP,vcGlAR,intRate)
		values ('$d[intTax]','$d[GLAPTax]','$d[GLARTax]','$d[RateTax]')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function updaterate($id,$rate)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mtax set intRate='$rate' where intID='$id'");
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mtax';
		$his['doc']			= 'TAX';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mtax set 
		vcCode='$d[Code]',
		vcName='$d[Name]',
		intRate='$d[Rate]',
		intActive='$d[Active]'
		
		where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mtax set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from mtaxcoa where intTax='$id'");
		if($q)
		{
		    return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */