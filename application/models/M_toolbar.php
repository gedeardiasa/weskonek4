<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_toolbar extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetNextId($id,$table)
	{
		$db=$this->load->database('default', TRUE);
		
		$loop=1;
		$cekidnext=$id+1;
		$result=0;
		
		$getmaxid=$this->db->query("select max(intID) as maxID from $table
		");
		$rmaxid=$getmaxid->row();
		$maxid=$rmaxid->maxID;
		while($loop==1 and $cekidnext<=$maxid)
		{
			$q=$this->db->query("select intID from $table where intID='$cekidnext'
			");
			if($q->num_rows()>0)
			{
				$h=$q->row();
				$result=$h->intID;
				$loop=0;
			}
			$cekidnext++;
		}
		return $result;
	}
	function GetPreviousId($id,$table)
	{
		$db=$this->load->database('default', TRUE);
		
		$getmaxid=$this->db->query("select max(intID) as maxID from $table
		");
		$rmaxid=$getmaxid->row();
		$maxid=$rmaxid->maxID;
		
		$loop=1;
		if($id!=0)
		{
			$cekidnext=$id-1;
		}
		else
		{
			$cekidnext = $maxid;
		}
		$result=0;
		
		while($loop==1 and $cekidnext>0)
		{
			$q=$this->db->query("select intID from $table where intID='$cekidnext'
			");
			if($q->num_rows()>0)
			{
				$h=$q->row();
				$result=$h->intID;
				$loop=0;
			}
			$cekidnext--;
		}
		return $result;
	}
	function GetProfilHeader()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT DISTINCT(c1.vcName) AS vcName1, c1.*  FROM 
		mprofile c1 where intID=1
		");
		
		return $q->row();
	}
	function GetProfil($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT DISTINCT(c1.vcName) AS vcName1, c1.*  FROM 
		dAR a
		LEFT JOIN hAR b ON a.intHID=b.`intID`
		LEFT JOIN (mlocation c
			LEFT JOIN mplan c1 ON c.`intPlan`=c1.intID
		) ON a.`intLocation`=c.intID
		WHERE b.intID='$id'
		LIMIT 1
		");
		
		return $q->row();
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */