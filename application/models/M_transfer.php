<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_transfer extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocationFrom, c.vcName as vcLocationTo
		from hIT a 
		LEFT JOIN mlocation b on a.intLocationFrom=b.intID
		LEFT JOIN mlocation c on a.intLocationTo=c.intID
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocationFrom, c.vcName as vcLocationTo
		from hIT a 
		LEFT JOIN mlocation b on a.intLocationFrom=b.intID
		LEFT JOIN mlocation c on a.intLocationTo=c.intID
		LEFT JOIN maccessplan d ON b.`intPlan`=d.`intPlan` AND d.`intUserID`='$iduser'
		LEFT JOIN maccessplan e ON c.`intPlan`=e.`intPlan` AND e.`intUserID`='$iduser'
		where d.intUserID='$iduser' and e.intUserID='$iduser'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcLocationFrom, c.vcName as vcLocationTo
		from hIT a 
		LEFT JOIN mlocation b on a.intLocationFrom=b.intID
		LEFT JOIN mlocation c on a.intLocationTo=c.intID
		LEFT JOIN maccessplan d ON b.`intPlan`=d.`intPlan` AND d.`intUserID`='$iduser'
		LEFT JOIN maccessplan e ON c.`intPlan`=e.`intPlan` AND e.`intUserID`='$iduser'
		where d.intUserID='$iduser' and e.intUserID='$iduser' and a.dtDate>='$d[from]' and a.dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcUoM from dIT a 
		LEFT JOIN mitem b on a.intItem=b.intID
		where a.intHID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hIT where intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hIT');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hIT (vcDocNum,dtDate,vcRef,intLocationFrom,vcLocationFrom,intLocationTo,vcLocationTo,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]','$d[RefNum]','$d[FromWhs]',
		(select vcName from mlocation where intID='$d[FromWhs]'),
		'$d[ToWhs]',
		(select vcName from mlocation where intID='$d[ToWhs]'),
		'$d[Remarks]','$d[UserID]','$now')
		");
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeIT']=str_replace("'","''",$d['itemcodeIT']);
		$d['itemnameIT']=str_replace("'","''",$d['itemnameIT']);
		$d['uomIT']=str_replace("'","''",$d['uomIT']);
		$q=$this->db->query("insert into dIT (intHID,intItem,intLocationFrom,vcLocationFrom,intLocationTo,vcLocationTo,vcItemCode,vcItemName,intQty)
		values ('$d[intHID]',(select intID from mitem where vcCode='$d[itemcodeIT]'),
		'$d[FromWhs]',
		(select vcName from mlocation where intID='$d[FromWhs]'),
		'$d[ToWhs]',
		(select vcName from mlocation where intID='$d[ToWhs]'),
		'$d[itemcodeIT]','$d[itemnameIT]','$d[qtyIT]')
		");
		if($q)
		{
			$id=$this->db->query("select LAST_INSERT_ID() as intID");
			$rid=$id->row();
			$idtin=$rid->intID;
			return $idtin;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */