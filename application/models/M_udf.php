<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_udf extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcDoc
		from mudf a 
		LEFT JOIN mdoc b on a.intDoc=b.intID
		where a.intDeleted=0  
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDataByDoc($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcDoc
		from mudf a 
		LEFT JOIN mdoc b on a.intDoc=b.intID
		where a.intDeleted=0 and b.vcCode='$doc'
		order by a.intID desc
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getUDF2byName($name)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mudf2 where vcDoc='$name'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getdataudf2($udfid,$docnum,$intHID)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from dudf2 where intUdf='$udfid' and vcDocNum='$docnum' and intHID='$intHID'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function CekAvailableUdf($iddoc,$code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mudf where intDoc='$iddoc' and vcCode='$code'
		");
		
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return 0;
		}
	}
	function SaveUdf($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$now=date('Y-m-d H:i:s');
		$d['Val']=str_replace("'","''",$d['Val']);
		$d['Val']=str_replace('"','',$d['Val']);
		
		$q=$this->db->query("insert into dudf (intUdf,vcDocNum,vcValue,dtInsertTime)
			values('$d[ID]','$d[Doc]','$d[Val]','$now')
		");
	}
	function insertudf2($intUdf,$vcDocNum,$intHID,$vcValue)
	{
		$db=$this->load->database('default', TRUE);
		
		$now=date('Y-m-d H:i:s');
		$vcValue=str_replace("'","''",$vcValue);
		$vcValue=str_replace('"','',$vcValue);
		
		$q=$this->db->query("insert into dudf2 (intUdf,vcDocNum,intHID, vcValue,dtInsertTime)
			values('$intUdf','$vcDocNum','$intHID','$vcValue','$now')
		");
	}
	function EditUdf($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$now=date('Y-m-d H:i:s');
		$d['Val']=str_replace("'","''",$d['Val']);
		$d['Val']=str_replace('"','',$d['Val']);
		
		$q=$this->db->query("update dudf set vcValue='$d[Val]' where intUdf='$d[ID]' and vcDocNum='$d[Doc]'
		");
	}
	function LoadUdf($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("select vcValue from dudf where intUdf='$d[ID]' and vcDocNum='$d[Doc]'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcValue;
		}
		else
		{
			return "";
		}
	}
	function GetNameByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mudf where intID='$id'
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function CekDuplicateUdf($idudf,$doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from dudf where intUdf='$idudf' and vcDocNum='$doc'
		");
		if($q->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mudf (intDoc,vcCode,vcName,vcType, intLength)
		values ('$d[Doc]','$d[Code]','$d[Name]','$d[Type]','$d[Panjang]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mudf';
		$his['doc']			= 'UDF';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update mudf set intDoc='$d[Doc]',vcCode='$d[Code]',vcName='$d[Name]',vcType='$d[Type]', intLength='$d[Panjang]' 
		where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;
		$q=$this->db->query("update mudf set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */