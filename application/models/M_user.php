<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllUser()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from muser a where a.intDeleted=0 order by intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['UserID']=str_replace("'","''",$d['UserID']);
		$d['Email']=str_replace("'","''",$d['Email']);
		$d['Password']=str_replace("'","''",$d['Password']);
		$d['Password']=md5($d['Password']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$q=$this->db->query("insert into muser (vcUserID,vcPassword,vcName,vcEmail,blpImage,dtInsertTime)
		select '$d[UserID]','$d[Password]','$d[Name]','$d[Email]','$d[imgData]','$now'
		
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function editwallet($id,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update muser set vcWallet='$d' where intID='$id'");
		
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Email']=str_replace("'","''",$d['Email']);
		$d['UserID']=str_replace("'","''",$d['UserID']);
		$d['Phone']=str_replace("'","''",$d['Phone']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'muser';
		$his['doc']			= 'USER';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		if($d['imgData']!=null)
		{
			$q=$this->db->query("update muser set vcName='$d[Name]', vcEmail='$d[Email]', 
			vcUserID='$d[UserID]', vcPhone='$d[Phone]',
			blpImage='$d[imgData]' where intID='$d[id]'");
		}
		else
		{
			$q=$this->db->query("update muser set vcName='$d[Name]', vcEmail='$d[Email]', 
			vcUserID='$d[UserID]', vcPhone='$d[Phone]' where intID='$d[id]'");
		}
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function editProfile($d)
	{
		$db=$this->load->database('default', TRUE);
		$master=$this->load->database('master', TRUE);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'muser';
		$his['doc']			= 'USER';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		if(!isset($_SESSION[md5('posroot')]))
		{
			$d['Name']=str_replace("'","''",$d['Name']);
			$d['Email']=str_replace("'","''",$d['Email']);
			$d['UserID']=str_replace("'","''",$d['UserID']);
			$d['Phone']=str_replace("'","''",$d['Phone']);
			if($d['imgData']!=null)
			{
				$q=$this->db->query("update muser set vcName='$d[Name]', vcEmail='$d[Email]', 
				vcUserID='$d[UserID]', vcPhone='$d[Phone]', vcIcon='$d[SetIcon]',
				blpImage='$d[imgData]' where intID='$d[id]'");
			}
			else
			{
				$q=$this->db->query("update muser set vcName='$d[Name]', vcEmail='$d[Email]', vcIcon='$d[SetIcon]',
				vcUserID='$d[UserID]', vcPhone='$d[Phone]' where intID='$d[id]'");
			}
		}
		else
		{
			$d['Name']=str_replace("'","''",$d['Name']);
			$d['Email']=str_replace("'","''",$d['Email']);
			$d['UserID']=str_replace("'","''",$d['UserID']);
			
			$q=$master->query("update madmin set vcName='$d[Name]', vcEmail='$d[Email]', 
			vcUserID='$d[UserID]' where intID='$d[id]'");
			
		}
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function editsetting($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'muser';
		$his['doc']			= 'USER';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update muser set intDefaultLoc='$d[DefaultLoc]', intDefaultBP='$d[DefaultBP]', intDefaultWallet='$d[DefaultWallet]', intValue='$d[SetValue]'
		, intProfit='$d[SetProfit]', intItem='$d[SetItem]', vcIcon='$d[SetIcon]' , vcLang='$d[Language]'
		where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function GetActivityByDate($d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from museractivity where intUserID='$d[iduser]' and dtInsertTime>='$d[from]' and dtInsertTime<='$d[until]'
		");
		if($q->num_rows()>0)
		{
		    return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from muser a 
		where a.intID='$id' and a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function getIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from muser where vcUserID='$code' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  $r=$q->row();
		  return $r->intID;
		}
		else
		{
			return "";
		}
	}
	function GetUserRoot()
	{
		$db=$this->load->database('master', TRUE);
		$q=$db->query("select a.* from madmin a 
		where a.intID='1'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function getByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from muser a 
		where a.vcUserID='$code' and a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function getDefaultWallet($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intDefaultWallet from muser a 
		where a.intID='$id' and a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intDefaultWallet;
		}
		else
		{
			return 0;
		}
	}
	function getValueUser($key,$id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select $key as data from muser where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
		   $r=$q->row();
		   return $r->data;
		}
		else
		{
			return '';
		}
	}
	function cekpass($d)
	{
		$db=$this->load->database('default', TRUE);
		$master=$this->load->database('master', TRUE);
		if(!isset($_SESSION[md5('posroot')]))
		{
			$q=$this->db->query("select vcPassword from  muser where intID='$d[id]' and intDeleted=0
			");
		}
		else
		{
			$q=$master->query("select vcPassword from  madmin where intID='1'
			");
		}
		$r=$q->row();
		if(md5($d['OldPassword'])==$r->vcPassword)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editpass($d)
	{
		$db=$this->load->database('default', TRUE);
		$master=$this->load->database('master', TRUE);
		$pass=md5($d['NewPassword']);
		if(!isset($_SESSION[md5('posroot')]))
		{
			$q=$this->db->query("update muser set vcPassword='$pass' where intID='$d[id]' and intDeleted=0
			");
		}
		else
		{
			$q=$master->query("update madmin set vcPassword='$pass' where intID='1'
			");
		}
		
	}
	function getAccessUserHead($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.FormID ,CASE WHEN a.IsHeader=1 THEN 'Yes' ELSE 'No' END AS IsHeaderName,
		CASE WHEN b.FormID>0 THEN 'checked=checked' ELSE '' END AS OpenForm,
		c.vcName as HeaderName
		FROM mform a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS FormID
			FROM muser a LEFT JOIN 
			(maccess b LEFT JOIN mform c ON b.`intForm`=c.`intID`) ON a.`intID`=b.`intUserID`
			where a.intID='$id'
		) b ON a.`intID`=b.FormID
		LEFT JOIN mform c on a.intHeader=c.intID
		WHERE a.intDeleted=0 and a.IsHeader=1
		ORDER by a.intID
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessUserPlan($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.PlanID ,
		CASE WHEN b.PlanID>0 THEN 'checked=checked' ELSE '' END AS OpenForm
		
		FROM mplan a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS PlanID
			FROM muser a LEFT JOIN 
			(maccessplan b LEFT JOIN mplan c ON b.`intPlan`=c.`intID`) ON a.`intID`=b.`intUserID`
			WHERE a.intID='$id'
		) b ON a.`intID`=b.PlanID
		WHERE a.intDeleted=0
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessUserTop($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.FormID ,
		CASE WHEN b.FormID>0 THEN 'checked=checked' ELSE '' END AS OpenForm
		
		FROM mtopform a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS FormID
			FROM muser a LEFT JOIN 
			(maccesstop b LEFT JOIN mtopform c ON b.`intForm`=c.`intID`) ON a.`intID`=b.`intUserID`
			WHERE a.intID='$id'
		) b ON a.`intID`=b.FormID
		WHERE a.intDeleted=0
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessUser($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.FormID ,CASE WHEN a.IsHeader=1 THEN 'Yes' ELSE 'No' END AS IsHeaderName,
		CASE WHEN b.FormID>0 THEN 'checked=checked' ELSE '' END AS OpenForm,
		CASE WHEN b.FormID>0 THEN '' ELSE 'disabled=true' END AS DisabledForm,
		CASE WHEN b.intCreate=1 THEN 'checked=checked' ELSE '' END AS OpenFormC,
		CASE WHEN b.intRead=1 THEN 'checked=checked' ELSE '' END AS OpenFormR,
		CASE WHEN b.intUpdate=1 THEN 'checked=checked' ELSE '' END AS OpenFormU,
		CASE WHEN b.intDelete=1 THEN 'checked=checked' ELSE '' END AS OpenFormD,
		CASE WHEN b.intExport=1 THEN 'checked=checked' ELSE '' END AS OpenFormE,
		c.vcName as HeaderName
		FROM mform a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS FormID,b.intCreate, b.intRead, b.intUpdate, b.intDelete, b.intExport
			FROM muser a LEFT JOIN 
			(maccess b LEFT JOIN mform c ON b.`intForm`=c.`intID`) ON a.`intID`=b.`intUserID`
			where a.intID='$id'
		) b ON a.`intID`=b.FormID
		LEFT JOIN mform c on a.intHeader=c.intID
		WHERE a.intDeleted=0 and a.IsHeader=0
		ORDER by a.intID
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessAllWithoutHeader()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT *, CASE WHEN IsHeader=1 THEN 'Yes' ELSE 'No' END AS IsHeaderName FROM mform WHERE intDeleted=0 and isHeader=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessAllPlan()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT * FROM mplan WHERE intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessAllTop()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT * FROM mtopform WHERE intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getAccessUserDashboard($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.DashboardID , b.intOrder,
		CASE WHEN b.DashboardID>0 THEN 'checked=checked' ELSE '' END AS OpenDashboard		
		FROM mdashboard a
		LEFT JOIN
		(
			SELECT 
			c.`intID` AS DashboardID, b.intOrder
			FROM muser a LEFT JOIN 
			(maccess_dashboard b LEFT JOIN mdashboard c ON b.`intDashboard`=c.`intID`) ON a.`intID`=b.`intUserID`
			where a.intID='$id'
		) b ON a.`intID`=b.DashboardID
		WHERE a.intDeleted=0
		 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function dellastaccessdashboard($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from maccess_dashboard where intUserID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function getAccessAllDashboard()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT * FROM mdashboard WHERE intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getSubDomainByvcUserId($user)
	{
		$dbmaster=$this->load->database('master', TRUE);
		$q=$dbmaster->query("
		select b.vcSubDomain from muser a left join mclient b on a.intClient=b.intID where a.vcUserID='$user'
		");
		if($q->num_rows()>0)
		{
			$r = $q->row();
			return $r->vcSubDomain;
		}
		else
		{
			return '';
		}
	}
	function addnewaccessdashboard($id,$dash,$order)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into maccess_dashboard (intUserID,intDashboard,intOrder)
		values ('$id','$dash','$order')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function addnewaccess($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from maccess where intUserID='$id' and intForm='$form'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into maccess (intUserID,intForm)
			values ('$id','$form')
			");
		}
		return 1;
	}
	function editcrud($id,$form,$crud)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("update maccess set
		intCreate='$crud[C]',
		intRead='$crud[R]',
		intUpdate='$crud[U]',
		intDelete='$crud[D]',
		intExport='$crud[E]'
		where intUserID='$id' and intForm='$form'");
		return 1;
	}
	function addnewaccessplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from maccessplan where intUserID='$id' and intPlan='$plan'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into maccessplan (intUserID,intPlan)
			values ('$id','$plan')
			");
		}
		return 1;
	}
	function addnewaccesstop($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from maccesstop where intUserID='$id' and intForm='$form'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into maccesstop (intUserID,intForm)
			values ('$id','$form')
			");
		}
		return 1;
	}
	function dellastaccessbyid($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from maccess where intUserID='$id' and intForm='$form'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function dellastaccessbyidplan($id,$plan)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from maccessplan where intUserID='$id' and intPlan='$plan'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function dellastaccessbyidtop($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from maccesstop where intUserID='$id' and intForm='$form'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function getAccessAllWithoutHeaderSub()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.vcCode as HCode FROM msubform a 
		LEFT JOIN mform b on a.intHeader=b.intID
		WHERE a.intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function addnewaccessSub($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$cek=$this->db->query("select * from maccesssub where intUserID='$id' and intForm='$form'");
		if($cek->num_rows()==0)
		{
			$q=$this->db->query("insert into maccesssub (intUserID,intForm)
			values ('$id','$form')
			");
		}
		return 1;
	}
	function dellastaccessbyidSub($id,$form)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from maccesssub where intUserID='$id' and intForm='$form'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update muser set intDeleted=1 where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	
	//ROOT MODEL
	function RootGetAllUser()
	{
		$master=$this->load->database('master', TRUE);
		$q=$master->query("select a.*, b.vcSubDomain, b.vcName, b.vcDB
		from muser a LEFT JOIN mclient b on a.intClient = b.intID
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function Rootinsert($d)
	{
		$master=$this->load->database('master', TRUE);
		$now=date('Y-m-d');
		$d['User']=str_replace("'","''",$d['User']);
		$d['Email']=str_replace("'","''",$d['Email']);
		
		$cek=$master->query("
		select * from muser where vcUserID='$d[User]' or vcEmail='$d[Email]'
		");
		
		if($cek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			
			$q=$master->query("insert into muser (intClient,vcUserID,vcEmail)
			values ('$d[Client]','$d[User]','$d[Email]')
			");
			if($q)
			{
				$client=$master->query("select * from mclient where intID='$d[Client]'
				");
				$rclinet=$client->row();
				$dbclient=$this->load->database($rclinet->vcSubDomain, TRUE);
				
				$defpassword=md5('12345');
				$q=$dbclient->query("insert into muser (vcUserID,vcPassword,vcName,vcEmail,dtInsertTime)
				select '$d[User]','$defpassword','$d[User]','$d[Email]','$now'
				
				");
				$qgetlastusers=$dbclient->query("select LAST_INSERT_ID() as intID");
				$rgetlastusers=$qgetlastusers->row();
				$iduserok=$rgetlastusers->intID;
				if($d['Admin']=='on')
				{
					
					$dbclient->query("insert into maccess (intUserID,intForm,intCreate,intRead,intUpdate,intDelete)
					values ('$iduserok','2','1','1','1','1')
					
					"); // menambahkan access khusus muser saja jika tipe user admin agar bisa merubah access nya sendiri
				}
				if($q)
				{
				  return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	
	}
	function Rootedit($d)
	{
		$master=$this->load->database('master', TRUE);
		$now=date('Y-m-d');
		$d['User']=str_replace("'","''",$d['User']);
		$d['LastUser']=str_replace("'","''",$d['LastUser']);
		$d['Email']=str_replace("'","''",$d['Email']);
		
		$cek=$master->query("
		select * from muser where (vcUserID='$d[User]' and vcUserID!='$d[LastUser]') or (vcEmail='$d[Email]' and vcEmail!='$d[LastEmail]')
		");
		
		if($cek->num_rows()>0)
		{
			return 0;
		}
		else
		{
			
			$q=$master->query("update muser set vcUserID='$d[User]', vcEmail='$d[Email]' where intID='$d[id]'
			");
			if($q)
			{
				$client=$master->query("select * from mclient where intID='$d[Client]'
				");
				$rclinet=$client->row();
				$dbclient=$this->load->database($rclinet->vcSubDomain, TRUE);
				
				$defpassword=md5('12345');
				$q=$dbclient->query("update muser set vcUserID='$d[User]', vcEmail='$d[Email]' where vcUserID='$d[LastUser]'
				");
				if($q)
				{
				  return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
	
	}
	function Rootdelete($id)
	{
		$master=$this->load->database('master', TRUE);
		$q=$master->query("update muser set intDeleted='1' where intID='$id'
		");
		$userid=$master->query("select vcUserID from muser where intID='$id'
		");
		$ruserid=$userid->row()->vcUserID;
		
		$client=$master->query("select a.* from mclient a 
		LEFT JOIN muser b on a.intID=b.intClient
		where b.intID='$id'
		");
		
		
		$rclinet=$client->row();
		$dbclient=$this->load->database($rclinet->vcSubDomain, TRUE);
		
		$q=$dbclient->query("update muser set intDeleted=1 where vcUserID='$ruserid'
		");
		
		return 1;
	}
	
	
	
	//END ROOT MODEL
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */