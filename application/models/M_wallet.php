<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_wallet extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, b.vcName as vcPlan
		from mwallet a 
		LEFT JOIN mplan b on a.intPlan=b.intID
		where a.intDeleted=0  
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("SELECT a.*, b.vcName AS vcPlan, c.`intUserID`
		FROM mwallet a 
		LEFT JOIN mplan b ON a.intPlan=b.intID
		LEFT JOIN maccessplan c ON a.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE a.intDeleted=0 AND c.`intUserID`='$iduser' ORDER BY a.intID DESC 
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessandWalletAccess($iduser)
	{
		// NOTE
		
		// empty field mean open all
		
		
		$db=$this->load->database('default', TRUE);
		if($this->db->query("select vcWallet from muser where intID='$iduser'")->num_rows()>0){
			
			$rtyu = $this->db->query("select vcWallet from muser where intID='$iduser'")->row()->vcWallet;
		}else{
			
			$rtyu = "";
		}
		if($rtyu==""){
			$q=$this->db->query("SELECT a.*, b.vcName AS vcPlan, c.`intUserID`
			FROM mwallet a 
			LEFT JOIN mplan b ON a.intPlan=b.intID
			LEFT JOIN maccessplan c ON a.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			WHERE a.intDeleted=0 AND c.`intUserID`='$iduser' 
			ORDER BY a.intID DESC 
			");
		}else{
			$q=$this->db->query("SELECT a.*, b.vcName AS vcPlan, c.`intUserID`
			FROM mwallet a 
			LEFT JOIN mplan b ON a.intPlan=b.intID
			LEFT JOIN maccessplan c ON a.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			WHERE a.intDeleted=0 AND c.`intUserID`='$iduser' 
			and 
			(	(
					a.intID in
					(
						".$rtyu."
					)
				)
			)
			ORDER BY a.intID DESC 
			");
		}
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWalletAdjustmentWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.vcName AS vcPlan, c.`intUserID`,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		FROM hWalletAdjustment a 
		LEFT JOIN mwallet a1 on a.intWallet=a1.intID
		LEFT JOIN mplan b ON a1.intPlan=b.intID
		LEFT JOIN maccessplan c ON a1.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE a1.intDeleted=0 AND c.`intUserID`='$iduser' ORDER BY a.intID DESC 
		");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataTransactionWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.vcName AS vcPlan, c.`intUserID`, a1.vcName as vcWalletName
		FROM hWalletTransaction a 
		LEFT JOIN mwallet a1 on a.intWallet=a1.intID
		LEFT JOIN mplan b ON a1.intPlan=b.intID
		LEFT JOIN maccessplan c ON a1.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		WHERE a1.intDeleted=0 AND c.`intUserID`='$iduser' ORDER BY a.intID DESC 
		");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWalletTransferPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		SELECT a.*, b.vcName AS vcPlan, c.`intUserID`
		FROM hWalletTransfer a 
		LEFT JOIN mwallet a1 on a.intWalletFrom=a1.intID
		LEFT JOIN mwallet a2 on a.intWalletTo=a2.intID
		LEFT JOIN mplan b ON a1.intPlan=b.intID
		LEFT JOIN mplan b2 ON a2.intPlan=b2.intID
		LEFT JOIN maccessplan c ON a1.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
		LEFT JOIN maccessplan c2 ON a2.`intPlan`=c2.`intPlan` AND c2.`intUserID`='$iduser'
		WHERE a1.intDeleted=0 AND a2.intDeleted=0 AND c.`intUserID`='$iduser' AND c2.`intUserID`='$iduser' ORDER BY a.intID DESC 
		");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDataMutation($data)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		select a.*, b.vcName as WalletName, c.vcName as DocName, 
		IFNULL(ar.vcBPName,'') as vcBPName
		from dWalletMutation a
		LEFT JOIN mwallet b on a.intWallet=b.intID
		LEFT JOIN (
		SELECT 
		CONCAT(vcCode,'X') AS vcCode,
		CONCAT('Cancel ', vcName) AS vcName
		FROM mdoc
		UNION ALL
		SELECT 
		vcCode,vcName
		FROM mdoc
		) c on a.vcType=c.vcCode
		LEFT JOIN
		hAR ar on ar.vcDocNum=a.vcDoc and a.vcType='AR'
		where a.dtDate>='$data[from]' and a.dtDate<='$data[until]' and IFNULL(ar.vcBPName,'') like '%$data[bpname]%' and IFNULL(a.vcRemarks,'') like '%$data[remarks]%'
		$data[walletq] order by a.intWallet asc, a.dtDate asc, a.dtInsert asc;
		");
		
		if($q->num_rows()>0)
		{
			return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllByPlan($plan)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from mwallet where intPlan='$plan' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function getByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.* from mwallet a where a.intDeleted=0 and a.intID='$id'
		");
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return 0;
		}
	}
	function GetGLByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcGL from mwallet where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
		    return $r->vcGL;
		}
		else
		{
			return null;
		}
	}
	function GetPlanByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intPlan from mwallet where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
		    return $r->intPlan;
		}
		else
		{
			return null;
		}
	}
	function GetHeaderDepWithByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select * from hWalletAdjustment where intID='$id'
		");
		if($q->num_rows()>0)
		{
			return $q->row();
		}
		else
		{
			return null;
		}
	}
	function GetNameByID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select vcName from mwallet where intID='$id' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->vcName;
		}
		else
		{
			return null;
		}
	}
	function GetIDByCode($code)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select intID from mwallet where vcCode='$code' and intDeleted=0
		");
		if($q->num_rows()>0)
		{
			$r=$q->row();
			return $r->intID;
		}
		else
		{
			return null;
		}
	}
	function insert($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$now=date('Y-m-d H:i:s');
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("insert into mwallet (vcCode,vcName,intPlan,dtInsertTime, vcGL)
		values ('$d[Code]','$d[Name]','$d[Plan]','$now','$d[Gl]')
		");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function edit($d)
	{
		$db=$this->load->database('default', TRUE);
		
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'mwallet';
		$his['doc']			= 'WALLET';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$d['Code']=str_replace("'","''",$d['Code']);
		$d['Code']=str_replace(" ","",$d['Code']);
		$d['Name']=str_replace("'","''",$d['Name']);
		$d['Code']=str_replace('"','',$d['Code']);
		$d['Name']=str_replace('"','',$d['Name']);
		$q=$this->db->query("update mwallet set vcCode='$d[Code]',vcName='$d[Name]',intPlan='$d[Plan]',vcGL='$d[Gl]' where intID='$d[id]'");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
	}
	function canceladjustment($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update hWalletAdjustment set vcStatus='X' where intID='$id'");
		if($q)
		{
		  return 1;
		}
		else
		{
			return 0;
		}
		
	}
	function delete($id)
	{
		$db=$this->load->database('default', TRUE);
		$now = time();
		$deletedstring = "(deleted)".$now;

		$cek=$this->db->query("select a.* from mwallet a where a.intDeleted=0 and a.intID='$id'
		");
		$cekr = $cek->row();
		if($cekr->intBalance==0){
			$q=$this->db->query("update mwallet set intDeleted=1, vcCode=CONCAT(vcCode,'".$deletedstring."'), vcName=CONCAT(vcName,'".$deletedstring."') where intID='$id'");
			if($q)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}else{
			return 0;
		}
		
	}
	function addMutation($wallet,$date,$type,$cost,$doc='',$docpay='', $cf=0,$remarks='')
	{
		$nowdt = date('Y-m-d');
		if($date=='')
		{
			$date=date('Y-m-d');
		}
		
		if($date<$nowdt) // jika backdate maka jam insert wajib di akhir jam pada hari tersebut
		{
			$now=date('Y-m-d 23:59:59');
		}
		else
		{
			$now=date('Y-m-d H:i:s');
		}
		
		$db=$this->load->database('default', TRUE);
		
		$qlb=$this->db->query("select intBalance from mwallet where intID='$wallet'
		");
		$rlb=$qlb->row();
		
		if($date<$nowdt)// jika backdate maka last cost adalah cost terkahir dikurangi mutasi mulai tgl backdate sampai sekarang
		{
			$lastbalance = $rlb->intBalance;
			$qgetlast = $this->db->query("
			select SUM(intCost) as cost 
			from dWalletMutation where intWallet='$wallet' and dtDate>'$date'");
			if($qgetlast->num_rows()>0)
			{
				$rgetlast = $qgetlast->row();
				$hgetlast = $rgetlast->cost;
			}
			else
			{
				$hgetlast = 0;
			}
			$lastbalance = $lastbalance - $hgetlast;
		}
		else
		{
			$lastbalance = $rlb->intBalance;
		}
		
		//$lastbalance = $rlb->intBalance;
		
		$q=$this->db->query("insert into dWalletMutation (intWallet,dtDate,vcType,vcDoc,vcDocPay,intCost,intCF,vcRemarks, intLastCost, dtInsert)
		values ('$wallet','$date','$type','$doc','$docpay','$cost','$cf','$remarks','$lastbalance','$now')
		");
		//cek jika backdate maka ubah intLastStock tgl sesudahnya menjadi intLastStock + Qty
		if($date<$nowdt)
		{
			$q7=$this->db->query("update dWalletMutation set intLastCost=intLastCost+$cost where intWallet='$wallet' and dtDate>'$date'
			");
		}
		//
	}
	
	function insertDeporWith($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		if(!isset($d['Group'])){
			$d['Group'] = '';
		}
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['WalletName']=str_replace("'","''",$d['WalletName']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		$d['Group']=str_replace("'","''",$d['Group']);
		
		$q=$this->db->query("
		insert into hWalletAdjustment (vcDocNum, vcAccount, dtDate,intWallet, vcWalletName, vcType, intValue, vcRemarks, vcUser, dtInsertTime, vcGroupCode)
		values ('$d[DocNum]','$d[Account]','$d[DocDate]','$d[Wallet]','$d[WalletName]','$d[Type]','$d[Value]','$d[Remarks]','$d[UserID]','$now','$d[Group]')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function insertTrans($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$q=$this->db->query("
		insert into hWalletTransaction (vcName, vcAccount,intWallet, vcType, intValue, intGroup, vcRemarks, vcUser, dtInsertTime)
		values ('$d[Name]','$d[Account]','$d[Wallet]','$d[Type]','$d[Value]','$d[Group]','$d[Remarks]','$d[UserID]','$now')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editTrans($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$q=$this->db->query("
		update hWalletTransaction set 
		vcName='$d[Name]',
		vcAccount='$d[Account]',
		intWallet='$d[Wallet]',
		vcType='$d[Type]',
		intValue='$d[Value]',
		intGroup='$d[Group]',
		vcRemarks='$d[Remarks]'
		where intID='$d[idHeader]'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function insertTransfer($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['WalletFromName']=str_replace("'","''",$d['WalletFromName']);
		$d['WalletToName']=str_replace("'","''",$d['WalletToName']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		$q=$this->db->query("
		insert into hWalletTransfer (vcDocNum, dtDate,intWalletFrom, vcWalletNameFrom, intWalletTo, vcWalletNameTo, intValue, vcRemarks, vcUser, dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]','$d[WalletFrom]','$d[WalletFromName]','$d[WalletTo]','$d[WalletToName]','$d[Value]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		  return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function updateBalance($wallet,$cost)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("update mwallet set intBalance=intBalance+$cost where intID='$wallet'
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */