<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_wo extends CI_Model{
	
	function __construct(){
	parent::__construct();
		
	}
	function GetAllData()
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hWO a 
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccessAndDate($iduser,$d)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hWO a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dWO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser' and a.vcItemCode!=''
		)
		and a.dtDate>='$d[from]' and dtDate<='$d[until]'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetAllDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hWO a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dWO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		)
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetOpenDataWithPlanAccess($iduser)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*
		from hWO a 
		where a.intID not in
		(
			SELECT a.intHID
			FROM dWO a
			LEFT JOIN 
			(
				mlocation b
				LEFT JOIN maccessplan c ON b.`intPlan`=c.`intPlan` AND c.`intUserID`='$iduser'
			) 
			ON a.`intLocation`=b.intID
			WHERE IFNULL(c.`intUserID`,0)<>'$iduser'
		) and a.vcStatus='O'
		order by a.intID desc
		");
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetDetailByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*, c.vcDocNum, c.dtDate from dWO a 
		LEFT JOIN mitem b on a.intItem=b.intID
		LEFT JOIN hWO c on a.intHID=c.intID
		where a.intHID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q;
		}
		else
		{
			return $q;
		}
	}
	function GetHeaderByHeaderID($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hWO a where a.intID='$id'
		");
		
		if($q->num_rows()>0)
		{
		  return $q->row();
		}
		else
		{
			return $q->row();
		}
	}
	function GetHeaderByDocNum($doc)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("select a.*,
		case when a.vcStatus='O' then 'Open'
		when a.vcStatus='C' then 'Closed'
		when a.vcStatus='X' then 'Cancel'
		when a.vcStatus='D' then 'Draft'
		when a.vcStatus='P' then 'Pending'
		else '' end as vcStatusName
		
		from hWO a where a.vcDocNum='$doc'
		");
		
		if($q->num_rows()>0)
		{
		    return $q->row();
		}
		else
		{
			return 99;
		}
	}
	
	function insertH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		
		
		$d['UserID']=str_replace("'","''",$_SESSION['UsernamePOS']);
		
		//cek DocNum kembar
		$this->load->model('m_docnum', 'docnum');
		$d['DocNum'] = $this->docnum->GetLastDocNum('hWO');
		// end cek DocNum kembar
		
		$q=$this->db->query("insert into hWO (vcDocNum,dtDate,  
		vcRef,vcStatus,
		intDocTotal,vcRemarks,vcUser,dtInsertTime)
		values ('$d[DocNum]','$d[DocDate]',
		'$d[RefNum]','O',
		'$d[DocTotal]','$d[Remarks]','$d[UserID]','$now')
		");
		
		$id=$this->db->query("select LAST_INSERT_ID() as intID");
		$rid=$id->row();
		$idtin=$rid->intID;
		if($q)
		{
		    return $idtin;
		}
		else
		{
			return 0;
		}
	}
	function closeall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hWO set vcStatus='C' where intID='$id'
		");
		
		$q=$this->db->query("
		update dWO set vcStatus='C' where intHID='$id'
		");
	}
	function openall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hWO set vcStatus='O' where intID='$id'
		");
		
		$q=$this->db->query("
		update dWO set vcStatus='O' where intHID='$id'
		");
	}
	function cancelall($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("
		update hWO set vcStatus='X' where intID='$id'
		");
		
		$q=$this->db->query("
		update dWO set vcStatus='X' where intHID='$id'
		");
	}
	function editH($d)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d H:i:s');
		
		$d['DocNum']=str_replace("'","''",$d['DocNum']);
		$d['RefNum']=str_replace("'","''",$d['RefNum']);
		$d['DocDate']=str_replace("'","''",$d['DocDate']);
		$d['Remarks']=str_replace("'","''",$d['Remarks']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'hWO';
		$his['doc']			= 'WO';
		$his['key']			= "intID=$d[id]";
		$his['id']			= $d['id'];
		$his['detailkey']	= '';
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("
		
		update hWO set
		vcRef='$d[RefNum]',
		vcRemarks='$d[Remarks]',
		intDocTotal='$d[DocTotal]'
		where intID='$d[id]'
		
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
	}
	function UpdateType($id,$type,$gl,$asset)
	{
		$db=$this->load->database('default', TRUE);
		$now=date('Y-m-d');
		
		if($type=='asset')
		{
			$q=$this->db->query("
			update hWO set
			vcType='$type',
			dtConf='$now',
			vcAsset='$asset'
			where intID='$id'
			");
		}
		else if($type=='cost')
		{
			$q=$this->db->query("
			update hWO set
			vcType='$type',
			dtConf='$now',
			vcGLCost='$gl'			
			where intID='$id'
			");

		}
	}
	function UpdateTypeX($id)
	{
		$db=$this->load->database('default', TRUE);
		
		$q=$this->db->query("
		update hWO set
		vcType='',
		dtConf='1970-01-01',
		vcGLCost='',
		vcAsset=''
		where intID='$id'
		");
	}
	function insertD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeWO']=str_replace("'","''",$d['itemcodeWO']);
		$d['itemnameWO']=str_replace("'","''",$d['itemnameWO']);
		
		$d['uomWO']=str_replace("'","''",$d['uomWO']);
		
		$q=$this->db->query("insert into dWO (intHID,intItem,vcItemCode,vcItemName,intQty,vcUoM,
		intPrice,intLineTotal, intLocation,vcLocation,intWallet,vcWallet)
		values ('$d[intHID]','$d[itemID]','$d[itemcodeWO]','$d[itemnameWO]','$d[qtyWO]','$d[uomWO]',
		'$d[costWO]',
		'$d[linecostWO]','$d[whsWO]','$d[whsNameWO]','$d[walletWO]','$d[walletNameWO]')
		");
		
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function editD($d)
	{
		$db=$this->load->database('default', TRUE);
		$d['itemcodeWO']=str_replace("'","''",$d['itemcodeWO']);
		$d['itemnameWO']=str_replace("'","''",$d['itemnameWO']);
		
		$d['uomWO']=str_replace("'","''",$d['uomWO']);
		//HISTORY
		$this->load->model('m_history', 'history'); //load model history
		//define history
		$his['table'] 		= 'dWO';
		$his['doc']			= 'WO';
		$his['key']			= "intID=$d[intID]";
		$his['id']			= $this->history->getdatabyquery("select intHID as id from dWO where intID=$d[intID]"); // get data id header
		$his['detailkey']	= $d['itemnameWO'];
		$his['UserID']		= str_replace("'","''",$_SESSION['UsernamePOS']);
		$databefore			= $this->history->getdatabyid($his); // get data before
		//HISTORY
		$q=$this->db->query("update dWO set
		intItem='$d[itemID]',
		vcItemCode='$d[itemcodeWO]',
		vcItemName='$d[itemnameWO]',
		intQty='$d[qtyWO]',
		vcUoM='$d[uomWO]',
		intPrice='$d[costWO]',
		intLineTotal='$d[linecostWO]',
		intLocation='$d[whsWO]',
		vcLocation='$d[whsNameWO]',
		intWallet='$d[walletWO]',
		vcWallet='$d[walletNameWO]'
		where intID='$d[intID]'
		");
		//HISTORY
		$dataafter			= $this->history->getdatabyid($his); // get data after
		$this->history->createhistory($his,$databefore,$dataafter); // create history
		//HISTORY
		if($q)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function deleteD($id)
	{
		$db=$this->load->database('default', TRUE);
		$q=$this->db->query("delete from dWO where intID='$id'
		");
	}
}

/* End of file validasi.php */
/* Location: ./application/models/validasi.php */