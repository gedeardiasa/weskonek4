<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>
.modal-dialog {
  width: 90%;
 
}

</style>
<div class="wrapper">   
   
  <div class="modal fade" id="modal-add-edit">
  <div id="printarea_toolbar">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodaladd">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title"><?php echo $this->lang->line("Add New Adjustment");?></h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			<div class="box-header with-border" id="tittlemodaledit">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title"><?php echo $this->lang->line("Adjustment Detail");?></h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
              <div class="box-body">
			  <?php $this->load->view('loadingbar2'); ?>
				<div class="row">
					<div class="alert alert-success alert-dismissible" id="successalert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-check"></i><span id="successalertttext"><?php echo $this->lang->line("Success. You successfully process the data");?></span>
					</div>
					<div class="alert alert-danger  alert-dismissible" id="dangeralert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-ban"></i><?php echo $this->lang->line("Error. You failed to process the data");?>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Doc. Number");?></label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" readonly="true"  data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Document Number");?>">
							<input type="hidden" id="idHeader" name="idHeader">
							<input type="hidden" id="IdToolbar" name="IdToolbar">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Doc. Date");?></label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" placeholder="Doc. Date"  data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Document Date");?>">
							</div>
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Ref. Number");?></label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number"  data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Reference Number");?>">
							</div>
						</div>
						<div class="form-group" id="formgroupsourcecode">
						  <label for="Account" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Cost Source");?></label>
							<div class="col-sm-8" style="height:45px">
							 <select id="SourceCost" name="SourceCost" class="form-control select2" style="width: 100%; height:35px" data-toggle="tooltip" title="<?php echo $this->lang->line("Cost Source");?>" onchange="loadcost()">
							 <option value="0"><?php echo $this->lang->line("Item Cost");?></option>
							  <?php 
							  foreach($listprice->result() as $h) 
							  {
							  ?>	
							  <option value="<?php echo $h->intID;?>" <?php if($h->vcName==$defaultsource){ echo 'selected';}?>><?php echo $h->vcName;?></option>
							  <?php 
							  }
							  ?>
							</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label for="Whs" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Warehouse");?></label>
							<div class="col-sm-8" style="height:45px">
							<select id="Whs" name="Whs"class="form-control select2" onchange="pilihwhs()" style="width: 100%; height:35px" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Warehouse");?>">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($d->intID==$defaultwhs){ echo 'selected';}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
						   </select>
						   </div>
						</div>
						<div class="form-group">
						  <label for="Type" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Type");?></label>
							<div class="col-sm-8" style="height:45px">
							<select id="Type" name="Type" class="form-control select2"  data-toggle="tooltip" style="width: 100%; height:35px" data-placement="top" title="<?php echo $this->lang->line("Trans. Type");?>" onchange="showhidecost()">
								<option value="GR" ><?php echo $this->lang->line("Goods Receipt");?> (+)</option>
								<option value="GI" ><?php echo $this->lang->line("Goods Issue");?> (-)</option>
						   </select>
						   </div>
						</div>
						<div class="form-group">
						  <label for="Account" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Account");?></label>
							<div class="col-sm-8" style="height:45px">
							 <select id="Account" name="Account" class="form-control select2" style="width: 100%; height:35px" data-toggle="tooltip" title="<?php echo $this->lang->line("Account");?>">
							  <?php 
							  foreach($listgl->result() as $h) 
							  {
							  ?>	
							  <option value="<?php echo $h->vcCode;?>" <?php if($h->vcCode==$defaultgl){ echo 'selected';}?>><?php echo $h->vcCode."-".$h->vcName;?></option>
							  <?php 
							  }
							  ?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="Mvt" class="col-sm-4 control-label" style="height:20px"><?php echo $this->lang->line("Movement Type");?></label>
							<div class="col-sm-8" style="height:45px">
							 <select id="Mvt" name="Mvt" class="form-control select2" style="width: 100%; height:35px" data-toggle="tooltip" title="<?php echo $this->lang->line("Movement Type");?>">
							  <option value="">--<?php echo $this->lang->line("NOT SET");?>--</option>
							  <?php 
							  foreach($listmvt->result() as $h) 
							  {
							  ?>	
							  <option value="<?php echo $h->vcCode;?>"><?php echo $h->vcCode."-".$h->vcName;?></option>
							  <?php 
							  }
							  ?>
							</select>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#detaildata" data-toggle="tab"><?php echo $this->lang->line("Detail");?></a></li>
					  <li><a href="#adtdata" data-toggle="tab"><?php echo $this->lang->line("Additional Data");?></a></li>
					</ul>
				<div class="tab-content">
				  <div class="active tab-pane" id="detaildata">
					<div class="" id="form-adddetail">
					<b><i><?php echo $this->lang->line("Form Detail");?></i></b><br>
						<div class="col-sm-5" style="padding-left:0px;padding-bottom:0px">
							<div class="input-group">
							  <div class="input-group-addon" data-toggle="modal" data-target="#modal-item">
								<i class="fa fa-cubes" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control inputenter" id="detailItem" name="detailItem" maxlength="100" required="true" 
							  onchange="isiitem()" onpaste="isiitem()" placeholder="<?php echo $this->lang->line("Type Item Name");?>" data-toggle="tooltip" data-placement="top" 
							  title="<?php echo $this->lang->line("Item Name");?>">
							</div>
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" class="form-control inputenter" id="detailItemCode" name="detailItemCode" maxlength="50" required="true" 
							  onchange="isiitemcode()" onpaste="isiitemcode()" placeholder="<?php echo $this->lang->line("Type Code");?>" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Item Code");?>">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="detailQty" name="detailQty" onkeypress="isiqty()" onkeyup="isiqty()" 
							   data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Quantity");?>" maxlength="15" required="true" 
							  placeholder="<?php echo $this->lang->line("Qty");?>">
						</div>
						<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" step="any" class="form-control inputenter" id="detailUoM" name="detailUoM" readonly="true"
							   data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Unit of Measure");?>" maxlength="15" required="true" 
							  placeholder="<?php echo $this->lang->line("UoM");?>">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="detailCost" name="detailCost" maxlength="15" required="true"
							   data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Item Cost");?>"
							  placeholder="<?php echo $this->lang->line("Item Cost");?>">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<button type="button" class="btn btn-primary" id="buttonadddetail" onclick="addDetail()"><?php echo $this->lang->line("Add");?></button>
							<button type="button" class="btn btn-primary" id="buttoneditdetail" onclick="addDetail()"><?php echo $this->lang->line("Update");?></button>
							<button type="button" class="btn btn-default" id="buttoncanceldetail" onclick="cancelDetail()"><?php echo $this->lang->line("Cancel");?></button>
						</div>
					</div>
					<div id="mix_detail"></div>
				  </div>
				  <div class="tab-pane" id="adtdata">
					<div class="row">
						<div class="col-md-6">
						  <div id="mix_adtdata"></div>
						</div>
						<div class="col-md-6">
						
						</div>
					 </div>
				  </div>
				</div>
				</div>

				
				<div class="form-group">
                  <label><?php echo $this->lang->line("Remarks");?></label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="<?php echo $this->lang->line("Remarks");?> ..."  data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line("Remarks");?>"></textarea>
                </div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel"><?php echo $this->lang->line("Cancel");?></button>
		   <div class="btn-group" id="buttoncopyfrom">
                  <button type="button" class="btn btn-default">Copy From</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu dropup" role="menu">
					<li onclick="copyfrom('SO')"><a href="#">Sales Order</a></li>
                  </ul>
           </div>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  id="buttonsave"><?php echo $this->lang->line("Save");?></button>
         </div>
       </div>
     </div>
   </div>
   </div>
   <?php $this->load->view('batchplus'); ?>
   <?php $this->load->view('batchmin'); ?>
   <?php $this->load->view('batchjs'); ?>
   <script>
   
   function changebatch(item,detailQty) // fungsi ini tidak bisa digeneralkan
   {
	   var Type = $("#Type").val();
	   document.getElementById("BatchIn").value 	= '';
	   if(Type=='GR')
	   {
		   load_existing_batch(item);//load existing batch
		   load_batch_from_header(item);
		   document.getElementById("ItemBatchIn").value 	= item;
		   document.getElementById("QtyBatchIn").value 		= detailQty;
		   call_modal_plus_batch();
	   }
	   else if(Type=='GI')
	   {
		   document.getElementById("totalbatchmin").value = '';
		   load_available_batch_min(item,detailQty);
		   call_modal_min_batch();
		   reloaddetailBatchIn(item,detailQty);
	   }
   }
   </script>
  <?php $this->load->view('selectitem'); ?>
  <?php $this->load->view('selectso'); ?>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<?php $this->load->view('loadingbar2'); ?>
				<div class="col-sm-2" style="padding-left:0px;margin-left:0px">
                  <button type="button" style="width:100%" class="btn btn-dark" data-toggle="modal" data-target="#modal-add-edit" onclick="initialadd()" 
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?> disabled>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo $this->lang->line("Add New");?></button>
				</div>
				<div class="col-sm-7" style="padding-left:0px;margin-left:0px">
				&nbsp;
				</div>
				<div class="col-sm-3" style="padding-left:0px;margin-right:0px">
				<table align="center" style="width:100%">
					<tr>
						<td><input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff"></td>
						<td style="padding-left:5px" align="right"><button type="submit" class="btn btn-primary" onclick="loaddetailheader()"><?php echo $this->lang->line("Reload");?></button>
						</td>
					</tr>
				</table>
				</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong><?php echo $this->lang->line("Success");?>!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong><?php echo $this->lang->line("Error");?>!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<div id="mix_header"></div>
				<div class="loader"></div>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  <?php } ?>
  <?php $this->load->view('ctrludf'); ?>
<script>
  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode == 13) {
        addDetail();
    }
  });
  
  var xxx = document.getElementById("successalertttext").textContent;
  /*
  
	FUNGSI COPY FROM
  
  */
  function copyfrom(val)
  {
	 if(val=='SO')
	  {
		  $('#modal-so').modal('show');
	  }
  }
  function insertSO(id) //fungsi saat pilih item dengan modal
  {
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetailso?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				$('#modal-so').modal('hide');
				loadheaderso(id);
				
			}
		});
	  
  }
  /*
  
	FUNGSI LOAD
  
  */
  function pilihwhs()
  {
	  loadcost();
  }
  function isiqty()
  {
	  //loadcost();
  }
  function isiitem() // fungsi saat ketik field item
  {
	   var detailItem = $("#detailItem").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/getItemCode?", 
			data: "detailItem="+detailItem+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItemCode").value = data;
				loadcost();
				loaduom();
			},
		});
	  
  }
  function isiitemcode() // fungsi saat ketik field item
  {
	  var detailItemCode = $("#detailItemCode").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/getItemName?", 
			data: "detailItemCode="+detailItemCode+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItem").value = data;
				loadcost();
				loaduom();
			},
	  });
	  
  }
  function showhidecost()
  {
	  emptyformdetail();
	  var Type = $("#Type").val();
	  if(Type=='GI')
	  {
		  document.getElementById('detailCost').readOnly = true;
		  $("#formgroupsourcecode").hide();
	  }
	  else if(Type=='GR')
	  {
		  document.getElementById('detailCost').readOnly = false;
		  $("#formgroupsourcecode").show();
	  }
  }
  function cancelselectitem()
  {
	  //$("#modal-add-edit").css({"overflow:auto"});
  }
  function loadcost()
  {
	  var Type = $("#Type").val();
	  var detailItem = $("#detailItem").val();
	  var Whs = $("#Whs").val();
	  var SourceCost = $("#SourceCost").val();
	  if(Type=='GI')
	  {
		  var SourceCost2=0;
	  }
	  else
	  {
		  SourceCost2 = SourceCost;
	  }
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loadcost?", 
			data: "detailItem="+detailItem+"&Whs="+Whs+"&SourceCost="+SourceCost2+"", 
			cache: true, 
			success: function(data){ 
				document.getElementById("detailCost").value = data;
			},
			async: false
		});
  }
  function loaduom()
  {
	  var detailItem = $("#detailItem").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaduom?", 
			data: "detailItem="+detailItem+"", 
			cache: true, 
			success: function(data){ 
				document.getElementById("detailUoM").value = data;
			},
			async: false
		});
  }
  function loadheader(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				loadudf(obj.DocNum);
				if(dup==1)
				{
					document.getElementById("idHeader").value = 0;
				}
				else
				{
					document.getElementById("idHeader").value = obj.idHeader;
				}
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("Account").value = obj.Account;
				document.getElementById("Mvt").value = obj.Mvt;
				document.getElementById("IdToolbar").value = obj.DocNum;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				document.getElementById("RefNum").value = obj.RefNum;
				if(id==0)
				{
					$("#Whs").val(<?php echo $defaultwhs;?>);
					$("#Type").val($("#Type option:first").val());
				}
				else
				{
					document.getElementById("Whs").value = obj.Whs;
					document.getElementById("Type").value = obj.Type;
				}
				document.getElementById("Remarks").value = obj.Remarks;
				showhidecost();
				<?php
				if($backdate==0)
				{
				?>
				$('#DocDate').attr("disabled", true);
				<?php
				}
				?>
				
			}
		});
		
  }
  function loadheaderso(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderSO?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				loadudf(obj.DocNum);
				if(dup==1)
				{
					document.getElementById("idHeader").value = 0;
				}
				else
				{
					document.getElementById("idHeader").value = obj.idHeader;
				}
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("Account").value = obj.Account;
				document.getElementById("Mvt").value = obj.Mvt;
				document.getElementById("IdToolbar").value = obj.DocNum;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				document.getElementById("RefNum").value = obj.RefNum;
				
					document.getElementById("Whs").value = obj.Whs;
					document.getElementById("Type").value = obj.Type;
					
				document.getElementById("Remarks").value = obj.Remarks;
				$('#Whs').attr("disabled", true);
				$('#Type').attr("disabled", true);
				showhidecost();
				<?php
				if($backdate==0)
				{
				?>
				$('#DocDate').attr("disabled", true);
				<?php
				}
				?>
				
			}
		});
		
  }
  function loaddetailheader()
  {
	  $(".loadingbar2").show();
	  var daterange = $("#daterange").val();
	  daterange = daterange.split(' ').join('');
	  $('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'', function(){
		$(".loadingbar2").hide();
	  });   
  }
  function initialadd()
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#Whs').attr("disabled", false);
	  $('#Type').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DocNum').attr("disabled", false);
	  $('#Account').attr("disabled", false);
	  $('#Mvt').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				loadheader(0,0);
				showhidecost();
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
  }
  function initialedit(id)
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").hide();
	  $("#buttonsave").hide();
	  $("#tittlemodaladd").hide();
	  $("#tittlemodaledit").show();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $('#Remarks').attr("disabled", true);
	  $('#Whs').attr("disabled", true);
	  $('#Type').attr("disabled", true);
	  $('#RefNum').attr("disabled", true);
	  $('#DocDate').attr("disabled", true);
	  $('#DocNum').attr("disabled", true);
	  $('#Account').attr("disabled", true);
	  $('#Mvt').attr("disabled", true);
	  
	  //load detail ambil dari database dan masukkan ke session
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				loadheader(id,0);
				showhidecost();
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');

			}
		});
  }
  function initialduplicate(id)
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#Whs').attr("disabled", false);
	  $('#Type').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DocNum').attr("disabled", false);
	  $('#Account').attr("disabled", false);
	  $('#Mvt').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				loadheader(id,1);
				showhidecost();
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
  }
  /*
  
	HEADER FUNCTION
  
  */
  function addHeader()
  {
		$(".loadertransaction").show();
		var DocNum = $("#DocNum").val();
		var Account = $("#Account").val();
		var Mvt = $("#Mvt").val();
		var DocDate = $("#DocDate").val();
		var RefNum = $("#RefNum").val();
		var Whs = $("#Whs").val();
		var Type = $("#Type").val();
		var Remarks = $("#Remarks").val();
		
		var detail=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		
		var minus=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekminusH?", 
			data: "Whs="+Whs+"&DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				minus=data;
			},
			async: false
		});
		
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});
		
		var isbatch=0;
		if(Type=='GR')
		{
			batchtype= 'in';
		}
		else
		{
			batchtype= 'out';
		}
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/cekavailablebatch?", 
			data: "batch="+JSON.stringify(batch)+"&batchtype="+batchtype+"&doc=AD", 
			cache: true, 
			success: function(data){ 
				isbatch=data;
			},
			async: false
		});
		
		
		if(DocNum=='')
		{
			alert('<?php echo $this->lang->line("Please Fill Doc. Number");?>');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(Whs=='' || Whs==null || Whs=='0')
		{
			alert('<?php echo $this->lang->line("Please Fill Warehouse");?>');
			$("#Whs").focus();
			$(".loadertransaction").hide();
		}
		else if(isbatch!=1)
		{
			alert('<?php echo $this->lang->line("Please Set Batch For Item With Batch Management and Make Sure The Batch Quantity is The Same as The Item Quantity");?>');
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('<?php echo $this->lang->line("Please Fill Doc. Date");?>');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(Account==null)
		{
			alert('<?php echo $this->lang->line("Please Fill Account");?>');
			$("#Account").focus();
			$(".loadertransaction").hide();
		}
		else if(period!=1)
		{
			alert('<?php echo $this->lang->line("This Period is locked");?>');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('<?php echo $this->lang->line("Please Fill Detail Data");?>');
			$(".loadertransaction").hide();
		}
	
		else if(minus!=1 && Type=='GI')
		{
			alert('<?php echo $this->lang->line("Insufficient stock");?> ('+minus+')');
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd", 
				data: "DocNum="+DocNum+"&Account="+Account+"&Mvt="+Mvt+"&DocDate="+DocDate+"&RefNum="+RefNum+"&Whs="+Whs+
				"&Remarks="+Remarks+"&Type="+Type+"&batch="+JSON.stringify(batch)+"",  
				cache: false, 
				success: function(msg){ 
					if(msg.length==9)
					{
						saveudf(msg);
						document.getElementById("successalertttext").innerHTML = xxx + " (" + msg + ")";
						initialadd();
						loaddetailheader();
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
					showhidecost();
				} 
			}); 
		}
  }
  /*
  
	DETAIL FUNCTION
  
  */
  function insertItem(item)
  {
	  document.getElementById("detailItem").value = item;
	  $('#modal-item').modal('hide');
	  loadcost();
	  $("#detailQty").focus();
	  isiitem();
	  loaduom();
  }
  function emptyformdetail()
  {
	document.getElementById("detailItem").value = "";
	document.getElementById("detailItemCode").value = "";
	document.getElementById("detailQty").value = "";
	document.getElementById("detailUoM").value = "";
	document.getElementById("detailCost").value = "";
	$("#buttonadddetail").show();
	$("#buttoneditdetail").hide();
	$("#buttoncanceldetail").hide();
	EnableDisableItem(false);
	batch = [];
  }
  function EnableDisableItem(val)
  {
	  $('#detailItem').attr("disabled", val);
	  $('#detailItemCode').attr("disabled", val);
  }
  function addDetail()
  {
		$(".loadertransaction").show();
		EnableDisableItem(false);
		var Whs = $("#Whs").val();
		var Type = $("#Type").val();
	    var detailItem = $("#detailItem").val();
		var detailQty = $("#detailQty").val();
		var detailCost = $("#detailCost").val();
		var DocDate = $("#DocDate").val();
		
		var minus=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekminusD?", 
			data: "detailItem="+detailItem+"&detailQty="+detailQty+"&Whs="+Whs+"&DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				//alert(data);
				minus=data;
			},
			async: false
		});
		if(detailItem=='')
		{
			alert('<?php echo $this->lang->line("Please Fill Item Name");?>');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else if(detailQty=='' || detailQty==0 || detailQty<0)
		{
			alert('<?php echo $this->lang->line("Please Fill Quantity");?>');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		else if(minus==0 && Type=='GI')
		{
			alert('<?php echo $this->lang->line("Insufficient stock");?>');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/addDetail", 
				data: "detailItem="+detailItem+"&detailQty="+detailQty+"&detailCost="+detailCost+"",  
				cache: false, 
				success: function(msg){ 
					if(msg!='false')
					{
						$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
						document.getElementById("detailItem").value = "";
						document.getElementById("detailItemCode").value = "";
						document.getElementById("detailQty").value = "";
						document.getElementById("detailUoM").value = "";
						document.getElementById("detailCost").value = "";
						$("#buttonadddetail").show();
						$("#buttoneditdetail").hide();
						$("#buttoncanceldetail").hide();
						document.getElementById("detailItem").focus();
						// batch
						var isbatch  = 0;
						$.ajax({ 
							type: "POST",
							url: "<?php echo base_url(); ?>index.php/general_batch/getitemisbatch?", 
							data: "detailItem="+detailItem+"", 
							cache: true, 
							success: function(data){ 
								isbatch = parseInt(data);
							},
							async: false
						});
						if(isbatch!=0)
						{
							changebatch(isbatch,detailQty);
						}
						//end batch
					}
					else
					{
						alert('Error');
					}
					$(".loadertransaction").hide();
					cekDetail();
				} 
			}); 
		}
  }
  function delDetail(code, qty)
  {
	EnableDisableItem(false);
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/delDetail", 
		data: "code="+code+"&qty="+qty+"",  
		cache: false, 
		success: function(msg){ 
			$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
			document.getElementById("detailItem").value = "";
			document.getElementById("detailItemCode").value = "";
			document.getElementById("detailQty").value = "";
			document.getElementById("detailUoM").value = "";
			document.getElementById("detailCost").value = "";
			$("#buttonadddetail").show();
			$("#buttoneditdetail").hide();
			$("#buttoncanceldetail").hide();
			cekDetail();
			
		} 
	}); 
  }
  function cekDetail()
  {
	  var detail=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		if(detail!=0)
		{
			$('#Whs').attr("disabled", true);
			$('#Type').attr("disabled", true);
		}
		else
		{
			$('#Whs').attr("disabled", false);
			$('#Type').attr("disabled", false);
		}
  }
  function editDetail(item, code, qty, cost,uom)
  {
		document.getElementById("detailItem").value = item;
		document.getElementById("detailItemCode").value = code;
		document.getElementById("detailQty").value = qty;
		document.getElementById("detailUoM").value = uom;
		document.getElementById("detailCost").value = cost;
		$("#buttonadddetail").hide();
		$("#buttoneditdetail").show();
		$("#buttoncanceldetail").show();
		$("#detailQty").focus();
		EnableDisableItem(true);
  }
  function cancelDetail()
  {
		document.getElementById("detailItem").value = "";
		document.getElementById("detailItemCode").value = "";
		document.getElementById("detailQty").value = "";
		document.getElementById("detailUoM").value = "";
		document.getElementById("detailCost").value = "";
		$("#buttonadddetail").show();
		$("#buttoneditdetail").hide();
		$("#buttoncanceldetail").hide();
		EnableDisableItem(false);
  }
  $(function () {
	  
	
	$(".loadertransaction").hide();
	
	$(document).bind('keydown', function(e) {// fungsi shortcut
		if($('#modal-add-edit').hasClass('in')==true)
		{
			  if(e.ctrlKey && (e.which == 83)) { // Ctrl + s
				e.preventDefault();
				if($('#buttonsave').is(":visible")==true)
				{					
					addHeader();
				}
				else if($('#buttonedit').is(":visible")==true)
				{					
					editHeader();
				}
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 39)) { // Ctrl + right
				e.preventDefault();
				toolbar_next();
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 37)) { // Ctrl + left
				e.preventDefault();
				toolbar_previous();
				return false;
			  }
			  else if(e.which == 27) { // Esc
				e.preventDefault();
				$('#modal-add-edit').modal('hide');
				return false;
			  }
		}
		else
		{
			if(e.which == 78) {
				//$('#modal-add-edit').modal('show');
				//initialadd();
			}
		}
	});
  
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
		   'This Year': [moment().startOf('year'), moment().endOf('year')],
		   'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        }
    });
	document.getElementById("daterange").value = lastWeekT1+' - '+todayT1;
	
	//fungsi khusus multiple modal agar saat modal-2 di close modal-1 tetap bisa scroll
	$('.modal').on('hidden.bs.modal', function (e) {
		if($('.modal').hasClass('in')) {
		$('body').addClass('modal-open');
		}    
	});
	//end 
	$('#DocDate').datepicker({
      autoclose: true
    });
	//fungsi load ajax
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'');
	$('#mix_adtdata').load('<?php echo $this->config->base_url()?>index.php/general_udf/loadhtml?doc=AD');
	//end
	$('#detailItem').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
			alert('Selected ');
		}
	});
	$('#detailItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	$("#exampleSO").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
    
   
  });
  
 
</script>
</body>
</html>
