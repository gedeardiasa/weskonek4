<script>
//function batch
var batch = [];
   var dataSet;
   function call_modal_plus_batch()
   {
	   $('#modal-batch-plus').modal('show');
   }
   function call_modal_min_batch()
   {
	   
	   $('#modal-batch-min').modal('show');
	   $('#mix_detailbatchmin').DataTable( {
			data: dataSet,
			columns: [
				{ title: "DocNum" },
				{ title: "Qty" },
				{ title: "Control" }
			],
			"columnDefs": [
			  { className: "text-right", "targets": [1,2,3]}
			]
		} );
   }
   function reloaddetailBatchIn(itembatchmin,totalbatchmin)
   {
	   var DocNum = $("#DocNum").val();
	   var isloadfromheader = '';
	   $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchfromheader2?", 
			data: "DocNum="+DocNum+"&item="+itembatchmin+"&doc=<?php echo $typetoolbar;?>", 
			cache: true, 
			success: function(data){
				isloadfromheader =data;
			},
			async: false
		});
		
		if(isloadfromheader!='') // jika data load dari db maka hidden detail input
		{
			batch = [];
			$("#mix_availablebatchmin").hide();
			var res = isloadfromheader.split(",");
			for(var p=0;p<res.length-1;p++)
			{
				var tyu = res[p].split("|");
				batch.push([itembatchmin, tyu[1],tyu[0]]);
			}
		}
		else
		{
			$("#mix_availablebatchmin").show();
		}
	   // cek total detail yg sdh di input
		var ttdt = 0;
		
		var oTable = $('#mix_detailbatchmin').dataTable();
		oTable.fnClearTable();
		for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][0]==itembatchmin)
			{
				ttdt = parseFloat(ttdt) + parseFloat(batch[i][1]);
				
				var batchnumber = '';
				$.ajax({ 
					type: "POST",
					url: "<?php echo base_url(); ?>index.php/general_batch/getbatchnumberbyid?", 
					data: "batchid="+batch[i][2]+"", 
					cache: true, 
					success: function(data){
						batchnumber = data;
					},
					async: false
				});
				if(isloadfromheader!='') // jika data load dari db maka hidden detail input
				{
					$('#mix_detailbatchmin').dataTable().fnAddData( [
						batchnumber,
						batch[i][1],
						'']
					);
				}
				else
				{
					$('#mix_detailbatchmin').dataTable().fnAddData( [
						batchnumber,
						batch[i][1],
						'<div id="controldetail"><a onclick="delDetailBatchIn(\''+batch[i][2]+'\',\''+batch[i][0]+'\')"><i class="fa fa-trash" aria-hidden="true"></i></a></div>']
					);
				}
			}
		}
			
		if(parseFloat(ttdt)!=parseFloat(totalbatchmin) && isloadfromheader=='')
		{
			$("#alertbatchmin").show();
			$("#okbatchmin").hide();
			$("#buttonsavebatchout").hide();
		}
		else
		{
			$("#alertbatchmin").hide();
			$("#okbatchmin").show();
			if(isloadfromheader!='')
			{
				$("#okbatchmin").hide();
				$("#buttonsavebatchout").hide();
				batch = [];
			}else
			{
				$("#okbatchmin").show();
				$("#buttonsavebatchout").show();
			}
		}
   }
   function addDetailBatchIn()
   {
	   var itembatchmin 	= $("#itembatchmin").val();
	   var VBatch 			= $("#VBatch").val();
	   var ABatch 			= $("#ABatch").val();
	   var totalbatchmin 	= $("#totalbatchmin").val();

	   var batchstock		= 0;
	   $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchstockbyid?", 
			data: "idBatch="+ABatch+"", 
			cache: true, 
			success: function(data){
				batchstock = data;
			},
			async: false
		});
		if(parseFloat(batchstock)<parseFloat(VBatch)) // jika stok batch kurang maka alert error
		{
			alert('Stok Tidak Cukup');	
			return;
		}
	   
	   var Modif		= false;
	   
	   for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][0]==itembatchmin && batch[i][2]==ABatch)
			{
				Modif = true;
				batch[i][0]	= itembatchmin;
				batch[i][1]	= VBatch;
				batch[i][2]	= ABatch;
			}
		}
		if(Modif==false)
		{
			 batch.push([itembatchmin, VBatch,ABatch]);
		}
		reloaddetailBatchIn(itembatchmin,totalbatchmin);
		$("#VBatch").val("");

   }
   function delDetailBatchIn(doc,itm)
   {
	   var totalbatchmin 	= $("#totalbatchmin").val();
	   for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][2]==doc)
			{
				var indexdel = i;
			}
		}
		batch.splice(indexdel, 1);
		reloaddetailBatchIn(itm,totalbatchmin);
   }
   function savebacthin()
   {
	   var ItemBatchIn 			= $("#ItemBatchIn").val();
	   var QtyBatchIn 			= $("#QtyBatchIn").val();
	   var BatchIn 				= $("#BatchIn").val();
	   var BatchInRemarks 		= $("#BatchInRemarks").val();
	   var Whs 					= $("#Whs").val();
	   var WhsCheck = document.getElementById("Whs");
	   if(WhsCheck=== null){ // jika whs dari header tidak ada maka ambil whs dari detail
			var Whs = $("#WhsBatch").val();
	   }
	   var Modif				= false;
	   
	   var batchada		= 0;
	   $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/cekbatch?", 
			data: "Whs="+Whs+"&BatchIn="+BatchIn+"&ItemBatchIn="+ItemBatchIn+"", 
			cache: true, 
			success: function(data){
				batchada = data;
			},
			async: false
		});
		
		var isnewbatchnumber = 0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/isnewbatchnumber?", 
			data: "BatchIn="+BatchIn+"", 
			cache: true, 
			success: function(data){
				isnewbatchnumber = data;
			},
			async: false
		});
		if(batchada==1 || BatchIn=='' || isnewbatchnumber==1 || isnewbatchnumber=='1')
		{
		   for(var i = 0; i < batch.length; i++)
		   {
			   if(batch[i][0]==ItemBatchIn)
			   {
				   Modif = true;
				   batch[i][0]	= ItemBatchIn;
				   batch[i][1]	= QtyBatchIn;
				   batch[i][2]	= BatchIn;
				   batch[i][3]	= BatchInRemarks;
			   }
		   }
		   if(Modif==false)
		   {
				batch.push([ItemBatchIn, QtyBatchIn,BatchIn,BatchInRemarks]);
		   }
		   $('#modal-batch-plus').modal('hide');
		   $("#BatchIn").val("");
		   $("#BatchInRemarks").val("");
		}
		else
		{
			alert('Wrong Batch Number');
		}
	   
   }
   function load_existing_batch(item)
   {
		$('#BatchIn').attr("disabled", false);
		$("#buttonsavebatchin").show();
		$('#fillemptybatchplus').show();
	    for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][0]==item)
			{
				document.getElementById("BatchIn").value 	= batch[i][2];
			}
		}
   }
   function load_batch_from_header(itm)
   {
	   var DocNum = $("#DocNum").val();

	   // by default load new batch number first
	   $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/get_new_batch_number?", 
			data: "", 
			cache: true, 
			success: function(data){
				if(data!='')
				{
					document.getElementById("BatchIn").value 	= data;
				}
			},
			async: false
		});

		// then try get batch from data header
	   $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchfromheader?", 
			data: "DocNum="+DocNum+"&item="+itm+"&doc=<?php echo $typetoolbar;?>", 
			cache: true, 
			success: function(data){
				if(data!='')
				{
					$('#BatchIn').attr("disabled", true);
					$('#BatchInRemarks').attr("disabled", true);
					$('#fillemptybatchplus').hide();
					$("#buttonsavebatchin").hide();
					document.getElementById("BatchIn").value 	= data;
				}
			},
			async: false
		});
		var BatchIn = $("#BatchIn").val();
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchremarksbydocnum?", 
			data: "DocNum="+BatchIn+"", 
			cache: true, 
			success: function(data2){
				if(data2!='')
				{
					document.getElementById("BatchInRemarks").value 	= data2;
				}
			},
			async: false
		});
   }
   function load_available_batch_min(item,detailQty, WhsD = 0)
   {
	   	if(WhsD==0)
		{
			var Whs 			= $("#Whs").val();
		}else
		{
			var Whs = WhsD;
		}
		document.getElementById("totalbatchmin").value 	= detailQty;
		document.getElementById("itembatchmin").value 	= item;
		$("#buttonsavebatchout").hide();
		
		$('#mix_availablebatchmin').load('<?php echo $this->config->base_url()?>index.php/general_batch/getavailablebatchmin?Whs='+Whs+'&item='+item+'',function(){
			getbatchdetail();
		});    
   }   
   function getbatchdetail(){
		var ABatch 			= $("#ABatch").val();
		var stock			= 0;
		var remarks			= "";
		var date			= "";
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchstockbyid?", 
			data: "idBatch="+ABatch+"", 
			cache: true, 
			success: function(data){
				stock = data;
			},
			async: false
		});
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchremarksbyid?", 
			data: "idBatch="+ABatch+"", 
			cache: true, 
			success: function(data){
				remarks = data;
			},
			async: false
		});
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchdatebyid?", 
			data: "idBatch="+ABatch+"", 
			cache: true, 
			success: function(data){
				date = data;
			},
			async: false
		});
		$("#StockBatch").val(stock);
		$("#RemarksBatch").val(remarks);
		$("#DateBatch").val(date);
   }

</script>

