<div class="modal fade" id="modal-batch-min">
     <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodalbatchmin">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Set Batch</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <br><br>
			</div>
				<div class="box-body">
				<br><br>
				<div class="alert alert-danger alert-dismissible" id="alertbatchmin">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i><span id="alertbatchmintext">Make sure the batch quantity is the same as the item quantity!!</span>
				</div>


				<div class="alert alert-success alert-dismissible" id="okbatchmin">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i><span id="okbatchmintext">OK. Now quantity is same</span>
				</div>
			
				Total qty = <input type="text" style="background-color:yellow" id="totalbatchmin" name ="totalbatchmin" readonly="true">
				<hr>
				<input type="hidden" id="itembatchmin" name ="itembatchmin">
					<div id="mix_availablebatchmin"></div>
					<hr>
					<table id="mix_detailbatchmin" class="table table-striped dt-responsive jambo_table hover" width="100%"></table>
				</div>
            </div>
		</div>
		</div>
		 <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Back</button>
		 </div>
       </div>
	     
     </div>
  </div>
 