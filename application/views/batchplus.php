<div class="modal fade" id="modal-batch-plus">
     <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodalbatchplus">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Set Batch</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <br><br>
			</div>
				<div class="box-body">
					<div class="form-group">
					  <label for="BatchIn">Batch Number</label> <span id="fillemptybatchplus"></span>
					  <input type="hidden" id="ItemBatchIn" name="ItemBatchIn">
					  <input type="hidden" id="QtyBatchIn" name="QtyBatchIn">
					  <input type="hidden" id="WhsBatch" name="WhsBatch">
					  <input type="text" class="form-control" id="BatchIn" name="BatchIn" maxlength="25" required="true" placeholder="Enter Batch">
					</div>
					<div class="form-group">
					  <label for="BatchInRemarks">Remarks</label>
					  <input type="text" class="form-control" id="BatchInRemarks" name="BatchInRemarks" maxlength="250" required="true" placeholder="Enter Remarks">
					</div>
				</div>
            </div>
		</div>
		</div>
		 <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Back</button>
		   <button type="button" class="btn btn-primary" onclick="savebacthin()"  id="buttonsavebatchin"><?php echo $this->lang->line("Save");?></button>
		 </div>
       </div>
	     
     </div>
  </div>