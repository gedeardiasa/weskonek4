<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
		<div class="box">
            <div class="box-body">
			<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong>
                </div>
				
				<?php } ?>
            <form id="demo-form2" method="GET" action="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="Whs" class="col-sm-4 control-label" style="height:20px">Location</label>
							<div class="col-sm-8" style="height:45px">
							<input type="hidden" name="execute" id="execute" value="true">
							<select id="Whs" name="Whs" class="form-control select2" style="width: 100%;" data-toggle="tooltip" data-placement="top" title="Location">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($Whs==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Show</button>
                      </div>
                    </div>
				</div>
			</form>
			<hr>
				
				<?php if(isset($_GET['execute'])){?>
				<form id="demo-form2" method="POST" action="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/execute">
				<input type="Whs" name="Whs" hidden="true" value="<?php echo $Whs;?>">
				<table id="example1" class="table table-striped dt-responsive jambo_table hover">
					<thead>
					<tr>
					  <th><input type="checkbox" name="AllCek" id="AllCek" onclick="cekall()"></th>
					  <th>Item Code</th>
					  <th>Item Name</th>
					  <th>
					  <div class="col-sm-2">Bom</div>
					  <div class="col-sm-2">BOM Qty</div>
					  <div class="col-sm-2">BOM Cost Before</div>
					  <div class="col-sm-2">BOM Cost</div>
					  <div class="col-sm-2">BOM Cost/UoM</div>
					  </th>
					  <th>
					  <div class="col-sm-2">Rout</div>
					  <div class="col-sm-2">Rout Qty</div>
					  <div class="col-sm-2">Rout Cost Before</div>
					  <div class="col-sm-2">Rout Cost</div>
					  <div class="col-sm-2">Rout Cost/UoM</div>
					  </th>
					  <th>FINAL Cost/UoM</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($cru->result() as $cr){?>
						<?php foreach($cru2->result() as $cr2){
						if($cr2->vcItemCode==$cr->vcItemCode){	  
							$rtvcDocNum = $cr2->vcDocNum;
							$rtintQty = $cr2->intQty;
							$rtintCostBefore = $cr2->intCostBefore;
							$rtintCost = $cr2->intCost;
							$rtid		= $cr2->RoutID;
						}else{
							$rtvcDocNum ='';
							$rtintQty = 0;
							$rtintCostBefore = 0;
							$rtintCost = 0;
							$rtid = 0;
						}
					  	?>
					<tr>
					  <td><input class="detailcek" type="checkbox" name="DCek<?php echo $cr->intID; ?>" id="DCek<?php echo $cr->intID; ?>"></td>
					  <td><?php echo $cr->vcItemCode; ?></td>
					  <td><?php echo $cr->vcItemName; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')">
					  <div class="col-sm-2"><?php echo $cr->vcDocNum; ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($cr->intQty,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($cr->intCostBefore,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($cr->intCost,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($cr->intCost/$cr->intQty,'0'); ?></div>
					  </td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')">
					  <div class="col-sm-2"><?php echo $rtvcDocNum; ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($rtintQty,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($rtintCostBefore,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($rtintCost,'0'); ?></div>
					  <div class="col-sm-2" align="right"><?php echo number_format($rtintCost/$rtintQty,'0'); ?></div>
					  </td>
					  <td><?php echo number_format(($cr->intCost/$cr->intQty)+($rtintCost/$rtintQty),'0'); ?></td>
					  <?php
					  } 
					  ?>
					</tr>
					<?php }?>
					
					</tbody>

				</table>
				<button type="submit" class="btn btn-primary">Execute</button>
				</form>
				<?php } ?>
			</div>
		</div>
				
	</div>
    </section>
    <!-- /.content -->
	<div class="modal fade" id="modal-detail">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail</h3>
            </div>
            
              <div class="box-body">
				<div id="detailbom"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>
  </div>
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  function showdetail(id,id2)
  {
	  $('#detailbom').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/detailbom?id='+id+'&id2='+id2+'');
	  $('#modal-detail').modal('show');
  }
  function cekall()
  {
	  var val =  document.getElementById("AllCek").checked;
	  if(val==true)
	  {
		  $('.detailcek').prop('checked', true); //true to check else false uncheck
	  }
	  else
	  {
		  $('.detailcek').prop('checked', false); //true to check else false uncheck
	  }
	  
  }
  $(function () {
	
   
  });
  
 
</script>
</body>
</html>
