<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
		<div class="box">
            <div class="box-body">
				<form id="demo-form2" method="GET" action="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="Whs" class="col-sm-4 control-label" style="height:20px">Location</label>
							<div class="col-sm-8" style="height:45px">
							<input type="hidden" name="execute" id="execute" value="true">
							<select id="Whs" name="Whs" class="form-control select2" style="width: 100%;" data-toggle="tooltip" data-placement="top" title="Location">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($Whs==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Execute</button>
                      </div>
                    </div>
				</div>
				</form>
				
				<hr>
				
				<?php if(isset($_GET['execute'])){?>
				<table id="example1" class="table table-striped dt-responsive jambo_table hover">
					<thead>
					<tr>
					  <!--<th>Version</th>-->
					  <!--<th>Location</th>-->
					  <th>Item Code</th>
					  <th>Item Name</th>
					  <th>Bom</th>
					  <th>BOM Qty</th>
					  <th>BOM Cost Before</th>
					  <th>BOM Cost</th>
					  <th>BOM Cost/UoM</th>
					  <th>ROUT</th>
					  <th>ROUT Qty</th>
					  <th>ROUT Cost Before</th>
					  <th>ROUT Cost</th>
					  <th>ROUT Cost/UoM</th>
					  <th>FINAL Cost/UoM</th>
					  <th></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($cru->result() as $cr){?>
						<?php foreach($cru2->result() as $cr2){
						if($cr2->vcItemCode==$cr->vcItemCode){	  
							$rtvcDocNum = $cr2->vcDocNum;
							$rtintQty = $cr2->intQty;
							$rtintCostBefore = $cr2->intCostBefore;
							$rtintCost = $cr2->intCost;
							$rtid		= $cr2->RoutID;
						}else{
							$rtvcDocNum ='';
							$rtintQty = 0;
							$rtintCostBefore = 0;
							$rtintCost = 0;
							$rtid = 0;
						}
					  	?>
					<tr>
					  <!--<td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcRef; ?></td>-->
					  <!--<td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcLocation; ?></td>-->
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')"><?php echo $cr->vcItemCode; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')"><?php echo $cr->vcItemName; ?></td>
					  <td><?php echo $cr->vcDocNum; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($cr->intQty,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($cr->intCostBefore,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($cr->intCost,'0'); ?></td>
					  <td align="right"><?php echo number_format($cr->intCost/$cr->intQty,'0'); ?></td>
					  <td><?php echo $rtvcDocNum; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($rtintQty,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($rtintCostBefore,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>','<?php echo $rtid; ?>')" align="right"><?php echo number_format($rtintCost,'0'); ?></td>
					  <td align="right"><?php echo number_format($rtintCost/$rtintQty,'0'); ?></td>
					  <td align="right"><?php echo number_format(($cr->intCost/$cr->intQty)+($rtintCost/$rtintQty),'0'); ?></td>
					  <td></td>
					  <?php
					  } 
					  ?>
					</tr>
					<?php }?>
					
					</tbody>

				</table>
				
				<?php } ?>
			</div>
		</div>
    </section>
    <!-- /.content -->
	<div class="modal fade" id="modal-detail">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail</h3>
            </div>
            
              <div class="box-body">
				<div id="detailbom"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  <?php } ?>
<script>
	
  function showdetail(id,id2)
  {
	  $('#detailbom').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/detailbom?id='+id+'&id2='+id2+'');
	  $('#modal-detail').modal('show');
  }
	
  $(function () {
	$("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
