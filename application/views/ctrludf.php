<script>
// UDF
function saveudf(doc)
{
	<?php
	$ludf = $this->m_udf->GetDataByDoc($typeudf);
	foreach($ludf->result() as $rudf)
	{
	?>
	var idudf = <?php echo $rudf->intID;?>;
	var valudf = $("#<?php echo $rudf->vcCode;?>").val();
	$.ajax({ 
		type: "GET",
		url: "<?php echo base_url(); ?>index.php/general_udf/saveudf?", 
		data: "Doc="+doc+"&ID="+idudf+"&Val="+valudf+"", 
		cache: true, 
		success: function(data){ 
			document.getElementById("<?php echo $rudf->vcCode;?>").value = "";
		},
		async: false
	});
	<?php
	}
	?>
}
function editudf(doc)
{
	<?php
	$ludf = $this->m_udf->GetDataByDoc($typeudf);
	foreach($ludf->result() as $rudf)
	{
	?>
	var idudf = <?php echo $rudf->intID;?>;
	var valudf = $("#<?php echo $rudf->vcCode;?>").val();
	$.ajax({ 
		type: "GET",
		url: "<?php echo base_url(); ?>index.php/general_udf/editudf?", 
		data: "Doc="+doc+"&ID="+idudf+"&Val="+valudf+"", 
		cache: true, 
		success: function(data){ 
			//document.getElementById("<?php echo $rudf->vcCode;?>").value = "";
		},
		async: false
	});
	<?php
	}
	?>
}
function loadudf(doc)
{
	<?php
	$ludf = $this->m_udf->GetDataByDoc($typeudf);
	foreach($ludf->result() as $rudf)
	{
	?>
	var idudf = <?php echo $rudf->intID;?>;
	$.ajax({ 
		type: "GET",
		url: "<?php echo base_url(); ?>index.php/general_udf/loadudf?", 
		data: "Doc="+doc+"&ID="+idudf+"", 
		cache: true, 
		success: function(data){ 
			//alert("<?php echo $rudf->vcCode;?>");
			document.getElementById("<?php echo $rudf->vcCode;?>").value = data;
			//alert('xxxxxx');
		},
		async: false
	});
	<?php
	}
	?>
	
}
//END UDF
</script>