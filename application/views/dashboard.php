<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
    <!--<script src="<?php echo base_url(); ?>asset/hc/highcharts.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/exporting.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/highcharts-more.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/solid-gauge.js"></script>-->
</head>
  <?php $this->load->view('bodycollapse'); ?>

<div class="wrapper">

  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	
        <?php 
		if(!isset($_SESSION[md5('posroot')]))
		{
			if($show==1)
			{
				foreach($dashboard->result() as $d) 
				{
				?>
				<?php  
				$data['cart']=$d;
				$this->load->view('dashboard/chart/'.$d->vcCode.'',$data); ?>
				
				<?php 
				}
			}
		}
		?>
   
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
</body>
</html>