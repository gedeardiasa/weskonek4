<?php 
		
		$year=date('Y');
		$month=date('m');
		$db=$this->load->database('default', TRUE);
		
		$qinvval=$db->query("
		SELECT 
		c.`vcName`, SUM(a.intStock*a.`intHPP`) AS valueItem
		FROM mstock a
		LEFT JOIN mitem b ON a.`intItem`=b.`intID`
		LEFT JOIN mitemcategory c ON b.`intCategory`=c.intID
		GROUP BY c.`vcName`
		ORDER BY SUM(a.intStock*a.`intHPP`) DESC

		");
		$d['Date']= date('Y-m-d');
		$d['plan'] = 0;
		$balancesheet = $this->m_report->GetBalanceSheet1($d);
		$kep1 = 0;
		foreach($balancesheet->result() as $x)
		{
			if($x->GL=='1')
			{
				$kep1 = $x->Val;
			}
			
		}

?>			
	<div class="row">
        <div class="col-md-12">
			<div class="box-footer">
              <div class="row">

				<div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo 'IDR '.number_format($kep1,'2');?></h5>
                    <span class="description-text">TOTAL ASSET</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                
              </div>
              <!-- /.row -->
            </div>
		</div>
	</div>
	<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Inventory Value</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php
				
				foreach($qinvval->result() as $ivl)
				{
					if($ivl->valueItem>0)
					{
				?>
                <li><a href="#"><?php echo $ivl->vcName; ?> <span class="pull-right text-green"><?php echo number_format($ivl->valueItem,0); ?></span></a>
                </li>
				<?php
					}
				}
				?>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->