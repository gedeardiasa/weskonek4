<?php 
		
		$year=date('Y');
		$month=date('m');
		$db=$this->load->database('default', TRUE);
		$qtopitem=$db->query("
		SELECT 
		a.vcItemCode, a.`vcItemName`,
		SUM(a.`intQtyInv`) AS QtyInv,
		a.`vcUoMInv`, c.`blpImage`
		FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		LEFT JOIN mitem c ON a.`intItem`=c.intID
		WHERE MONTH(b.`dtDate`)=$month AND YEAR(b.`dtDate`)=$year
		GROUP BY a.vcItemCode
		ORDER BY SUM(a.`intQtyInv`) DESC
		LIMIT 0,4
		");
		
		
		
		$qtotalinvvalue=$db->query("
		select 
		SUM(a.intStock*a.intHPP) as value
		from mstock a
		");
		$datainvvalue=$qtotalinvvalue->row();
		
		$qcost=$db->query("
		SELECT
		SUM(a.cost) AS cost, a.intMonth
		FROM
		(
		# ambil cost dari AR/Penjualan
		SELECT SUM(a.intLineCost) AS cost, MONTH(b.dtDate) AS intMonth FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.dtDate)=$year and b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		
		UNION ALL
		# ambil cost AP dengan tipe service
		SELECT SUM(a.intLineCost) AS cost,MONTH(b.dtDate) AS intMonth
		FROM dAP a
		LEFT JOIN hAP b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.`dtDate`)=$year AND b.vcStatus!='X' AND b.intService=1
		
		UNION ALL
		# ambil cost GI sebagai varian stock
		select SUM(a.intQty*a.intCost*-1) as cost, MONTH(b.dtDate) AS intMonth 
		from dAdjustment a
		LEFt JOIN hAdjustment b on a.intHID=b.intID
		where YEAR(b.`dtDate`)=$year and b.vcType='GI'
		
		UNION ALL
		# ambil cost IP/ Inventory Posting sebagai varian stock
		select SUM(a.intCost) as cost, MONTH(b.dtDate) AS intMonth 
		from dIP a
		LEFT JOIN hIP b on a.intHID=b.intID
		where a.intQty<a.intQtyBefore and YEAR(b.`dtDate`)=$year
		
		) a
		GROUP BY a.intMonth
		");
		
		$qrevenue=$db->query("
		
		# revenue di dapat dari line total AR yang sudah dikurangi Disc Header
		# revenue juga di dapat dari ongkir AR (masuk pendapatan lain2)
		SELECT
		SUM(a.revenue) AS revenue, a.intMonth
		FROM
		(
		SELECT SUM(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) AS revenue, MONTH(b.dtDate) AS intMonth FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.dtDate)=$year and b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		
		union all
		SELECT SUM(b.intFreight) AS revenue, MONTH(b.dtDate) AS intMonth FROM
		hAR b
		WHERE YEAR(b.dtDate)=$year AND b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		) a
		GROUP BY a.intMonth
		");
		$thismonth=date('m');
		$lastmonth=date('m')-1;
		
		$totalrevenue=0;
		$revthismonth=0;
		$revlastmonth=0;
		foreach($qrevenue->result() as $rev)
		{
			if($rev->intMonth==$thismonth)
			{
				$revthismonth=$rev->revenue;
			}
			if($rev->intMonth==$lastmonth)
			{
				$revlastmonth=$rev->revenue;
			}
			$totalrevenue=$totalrevenue+$rev->revenue;
		}
		if($thismonth>1)
		{
			if($revlastmonth==0)
			{
				$uprev=0;
			}
			else
			{
				$uprev=($revthismonth-$revlastmonth) / $revlastmonth *100;
			}
		}
		else
		{
			$uprev=0;
		}
		
		$totalcost=0;
		$costthismonth=0;
		$costlastmonth=0;
		foreach($qcost->result() as $rev)
		{
			if($rev->intMonth==$thismonth)
			{
				$costthismonth=$rev->cost;
			}
			if($rev->intMonth==$lastmonth)
			{
				$costlastmonth=$rev->cost;
			}
			$totalcost=$totalcost+$rev->cost;
		}
		if($thismonth>1)
		{
			if($costlastmonth==0)
			{
				$upcost=0;
			}
			else
			{
				$upcost=($costthismonth-$costlastmonth) / $costlastmonth *100;
			}
		}
		else
		{
			$upcost=0;
		}
		
		
		$profitthismonth=$revthismonth-$costthismonth;
		$profitlastmonth=$revlastmonth-$costlastmonth;
		if($thismonth>1)
		{
			if($profitlastmonth==0)
			{
				$upprofit=0;
			}
			else
			{	
				$upprofit=($profitthismonth-$profitlastmonth) / $profitlastmonth *100;
			}
		}
		else
		{
			$upprofit=0;
		}
		
		
?>	
	
	<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Recap Report</h3>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Sales: <?php echo date('Y');?></strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                   <div id="container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                 <!-- /.box-header -->
				 <div class="box box-primary">
					<div class="box-header with-border">
					  <h3 class="box-title">TOP Items (This Month)</h3>

					  
					</div>
					<div class="box-body">
					  
					  <ul class="products-list product-list-in-box">
						<?php foreach($qtopitem->result() as $ti){?>
						<li class="item">
						  <div class="product-img">
							<?php if($ti->blpImage!=null){ ?>
							<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $ti->blpImage ).'" class="img-circle" alt="Product Image"/>'?>
							<?php }else{ ?>
							<?php echo '<img src="'.base_url().'data/general/dist/img/box.png" class="img-circle" alt="Product Image"/>'?>
							<?php }?>
						  </div>
						  <div class="product-info">
							<a href="javascript:void(0)" class="product-title"><?php echo $ti->vcItemCode;?>
							  <span class="label label-warning pull-right"><?php echo $ti->QtyInv." ".$ti->vcUoMInv."";?></span></a>
							<span class="product-description">
								  <?php echo $ti->vcItemName;?>
								</span>
						  </div>
						</li>
						<?php }?>
					  </ul>
					</div>
					</div>
					<!-- /.box-body -->
					
					<!-- /.box-footer -->
				  </div>
				  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-<?php if($uprev>=0){ echo 'green';}else{echo 'red';}?>"><i class="fa fa-caret-<?php if($uprev>=0){ echo 'up';}else{echo 'down';}?>"></i> <?php echo number_format($uprev,'0');?>%</span>
                    <h5 class="description-header"><?php echo 'IDR '.number_format($revthismonth,'2');?></h5>
                    <span class="description-text">REVENUE THIS MONTH</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-<?php if($upcost>=0){ echo 'red';}else{echo 'green';}?>"><i class="fa fa-caret-<?php if($upcost>=0){ echo 'up';}else{echo 'down';}?>"></i> <?php echo number_format($upcost,'0');?>%</span>
                    <h5 class="description-header"><?php echo 'IDR '.number_format($costthismonth,'2');?></h5>
                    <span class="description-text">COST THIS MONTH</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-<?php if($upprofit>=0){ echo 'green';}else{echo 'red';}?>"><i class="fa fa-caret-<?php if($upprofit>=0){ echo 'up';}else{echo 'down';}?>"></i> <?php echo number_format($upprofit,'0');?>%</span>
                    <h5 class="description-header"><?php echo 'IDR '.number_format($profitthismonth,'2');?></h5>
                    <span class="description-text">PROFIT THIS MONTH</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-red"><br></span>
                    <h5 class="description-header"><?php echo 'IDR '.number_format($datainvvalue->value,'2');?></h5>
                    <span class="description-text">Inventory Value</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>

Highcharts.chart('container', {

	chart: {
        type: 'area'
    },
    title: {
        text: 'Revenue vs Cost'
    },

  
    yAxis: {
        title: {
            text: 'IDR'
        }
    },
	xAxis: {
            categories: [<?php for($i=1;$i<=12;$i++){ $dateObj   = DateTime::createFromFormat('!m', $i);
									$monthName = $dateObj->format('F'); echo "'".$monthName."',";}?>]
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
            series: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
    },

    series: [{
        name: 'Revenue',
		//color: 'green',
        data: [
		<?php
		
		for($i=1;$i<=12;$i++)
		{
			$kk=0;
			foreach($qrevenue->result() as $rev)
			{
				if($rev->intMonth==$i)
				{
					$kk=$rev->revenue;
					break;
				}
			}
			echo $kk.",";
		}
		?>
		]
    }, {
        name: 'Cost',
		//color: 'red',
        data: [
		<?php
		for($i=1;$i<=12;$i++)
		{
			$kk=0;
			foreach($qcost->result() as $rev)
			{
				if($rev->intMonth==$i)
				{
					$kk=$rev->cost;
					break;
				}
			}
			echo $kk.",";
		}
		?>
		]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
  

</script>