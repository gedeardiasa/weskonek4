<?php 
	$db=$this->load->database('default', TRUE);
	if(!isset($_SESSION[md5('posroot')]))
	{
		$q=$db->query("select blpImage from muser where intID='$_SESSION[IDPOS]'");
		$r=$q->row();
		$imageprofile=$r->blpImage;
	}
	else
	{
		$imageprofile=null;
	}
?> 

<style>


</style>
<header class="main-header">	
	
	<script>
	
	function logOut() 
	{
		window.location.href = "<?php echo base_url(); ?>index.php/welcome/logout";
	}
	
	</script>
	<!-- Logo -->
    <a href="<?php //echo base_url(); ?>#" class="logo">
      <span class="logo-mini"><b>W</b>K</span>
      <span class="logo-lg"><b>WesKonek</b> .id</span>
    </a>
	
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="height:10px">
	
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		<!--<li class="dropdown messages-menu" data-toggle="tooltip" data-placement="bottom" title="Search...">
		<form action="<?php echo base_url() ?>index.php/welcome/search/" method="get">
		<input type="text" class="form-control" id="key" name="key" placeholder="Search..." style="margin-top:8px; width:70%">
		</form>
		</li>-->
		<?php
		if(!isset($_SESSION[md5('posroot')]))
		{	
			$qtopform=$db->query("SELECT 
			c.`vcName` AS vcFormName, c.`vcCode` AS vcFormCode, c.`intID` AS FormID, c.vcIcon, c.intID
			FROM maccesstop a LEFT JOIN muser b ON a.`intUserID`=b.`intID`
			LEFT JOIN mtopform c ON a.`intForm`=c.`intID`
			WHERE  c.`intDeleted`=0 AND b.intID='$_SESSION[IDPOS]' ORDER BY c.intID desc
			");
		}
		else
		{
			$qtopform=null;
		}
		
		if(!isset($_SESSION[md5('posroot')]))
		{
			foreach($qtopform->result() as $tfrm)
			{
		?>
		
          <li class="dropdown messages-menu" data-toggle="tooltip" data-placement="bottom" title="<?php echo $tfrm->vcFormName;?>">
            <a align="center" href="<?php echo base_url(); ?>index.php/<?php echo $tfrm->vcFormCode?>" onclick="menuloading()">
              <?php echo $tfrm->vcIcon;?>
            </a>
			
		  </li>
		<?php
			}
		}
		?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			
              <?php if($imageprofile!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $imageprofile ).'" class="user-image" alt="User Image"/>';}else{ ?>
              <img src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="user-image" alt="User Image">
			  <?php } ?>
              <span class="hidden-xs"><?php echo $_SESSION['NamePOS']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if($imageprofile!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $imageprofile ).'" alt="..." class="img-circle" alt="User Image"/>';}else{ ?>
                <img src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="img-circle" alt="User Image">
			    <?php } ?>

                <p>
                  <?php echo $_SESSION['NamePOS']; ?>
                  <small>Member since <?php echo date('F, Y',strtotime($_SESSION['DatePOS'])); ?></small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>index.php/welcome/profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" onclick="logOut()" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
	<div class="progress progress-sm active loadingbar3" style="display:none;padding:0px;margin:0px">
           <div class="progress-bar progress-bar-primary  progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
           <span class="sr-only">20% Complete</span>
           </div>
    </div>
</header>