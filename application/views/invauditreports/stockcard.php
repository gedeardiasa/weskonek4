<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>
  <?php $this->load->view('treeok');?>
<div class="wrapper">

  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
			<b><i>
			<a href="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/">
			Inventory Audit Report
			</a> | 
			<a href="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/stockcard">
			<u>Stock Card</u>
			</a>
			</i></b>
			<hr>
				<form id="demo-form2" method="GET" action="">
				<div class="row">
				
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="ItemCode" class="col-sm-4 control-label" style="height:20px">Item Code</label>
							<div class="col-sm-8" style="height:45px">
							<input type="hidden" class="form-control" id="show" name="show" value="1">
							<input type="text" class="form-control" value="<?php echo $ItemCode;?>" id="ItemCode" name="ItemCode" data-toggle="tooltip" data-placement="top" title="Item Code">
							</div>
						</div>
						<div class="form-group">
						  <label for="ItemName" class="col-sm-4 control-label" style="height:20px">Item Name</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="<?php echo $ItemName;?>" id="ItemName" name="ItemName" data-toggle="tooltip" data-placement="top" title="Item Name">
							</div>
						</div>
						
						
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="ItemGroup" class="col-sm-4 control-label" style="height:20px">Item Group</label>
							<div class="col-sm-8" style="height:45px">
							<select id="ItemGroup" name="ItemGroup" class="form-control select2" style="width: 100%; height:35px" data-toggle="tooltip" data-placement="top" title="Item Group">
								<option value="0" >All</option>
								<?php 
								foreach($listgrp->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($ItemGroup==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="Periode" class="col-sm-4 control-label" style="height:20px">Periode</label>
							<div class="col-sm-2" style="height:45px">
							<select class="" id="PeriodeMonth" name="PeriodeMonth" style="width: 100%; height:35px" title="Month" data-toggle="tooltip" data-placement="top" >
							<?php for($i=1;$i<=12;$i++){?>
								<option value="<?php echo $i;?>" <?php if($i==$PeriodeMonth){echo 'selected="true"';}?>><?php echo $i;?></option>
							<?php }?>
							</select>
							</div>
							<div class="col-sm-4" style="height:45px">
							<select class="" id="PeriodeYear" name="PeriodeYear" style="width: 100%; height:35px" title="Year" data-toggle="tooltip" data-placement="top" >
							<?php for($i=2010;$i<=2050;$i++){?>
								<option value="<?php echo $i;?>" <?php if($i==$PeriodeYear){echo 'selected="true"';}?>><?php echo $i;?></option>
							<?php }?>
							</select>
							</div>
						</div>
					</div>
					
					
				
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                      </div>
                    </div>
				</div>
				</form>
				<hr>
				<table id="example1" class="table table-striped dt-responsive jambo_table hover">
                <thead>
                <tr>
				  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Category</th>
				  <th>UoM</th>
				  <th>Stock Start Periode</th>
				  <th>Qty In</th>
				  <th>Qty Out</th>
				  <th>Stock End Periode</th>
				  <th>Stock Now</th>
				  <th>Open SO</th>
				  <th>Available Stock</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				if($show==1)
				{
					foreach($list->result() as $d) 
					{
					?>
					<tr onclick="showdetail('<?php echo $d->vcCode; ?>')">
					  <td><?php echo $d->vcCode; ?></td>
					  <td><?php echo $d->vcName; ?></td>
					  <td><?php echo $d->vcCategory; ?></td>
					  <td><?php echo $d->vcUoM; ?></td>
					  <td align="right"><?php echo number_format($d->intLastStock,2); ?></td>
					  <td align="right"><?php echo number_format($d->QtyIn,2); ?></td>
					  <td align="right"><?php echo number_format($d->QtyOut,2); ?></td>
					  <td align="right"><?php echo number_format($d->intLastStock+$d->QtyIn+$d->QtyOut,2); ?></td>
					  <td align="right"><?php echo number_format($d->intStock,2); ?></td>
					  <td align="right"><?php echo number_format($d->OpenSO,2); ?></td>
					  <td align="right"><?php echo number_format($d->StockAvailable,2); ?></td>
					</tr>
				<?php 
					}
				}
				?>
                </tbody>
              </table>
			  <hr>
			  <b><u><i>Detail</i></u></b>
			  <div id="mix_detail"></div>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  <?php } ?>
<script>
	
	function showdetail(itemcode)
	{
		var PeriodeMonth = $("#PeriodeMonth").val();
		var PeriodYear = $("#PeriodYear").val();
		
		$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?itemcode='+itemcode+'&PeriodeMonth='+PeriodeMonth+'&PeriodYear='+PeriodYear+'');
	}



  $(function () {
	//fungsi typeahead
	$('#ItemName').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
