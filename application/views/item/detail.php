<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
  <?php $this->load->view('toolbar'); ?>


    <script src="https://yandex.st/highlightjs/7.3/highlight.min.js"></script>
</head>
  <?php $this->load->view('body'); ?>
<style>
.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">
<div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesdelete/<?php echo $item->intID; ?>"><button type="button" class="btn btn-danger">Delete</button></a>
         </div>
       </div>
     </div>
</div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>


    <!-- Main content -->
    <section class="content">
	<div class="row">
	<form role="form" method="POST" enctype="multipart/form-data"
		action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit/<?php echo $item->intID;?>">
        <div class="col-md-3">
		<!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
			  <div class="image-upload" align="center">
			  <label for="Image">
			  <?php if($item->blpImage!=null){echo '<img id="blah" src="data:image/jpeg;base64,'.base64_encode( $item->blpImage ).'" alt="User profile picture" class="profile-user-img img-responsive img-circle"/>';}else{ ?>
              <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/upload.png" class="profile-user-img img-responsive img-circle" alt="User profile picture">
			  <?php } ?>
			  </label>
			  <input type="file" id="Image" name="Image" onchange="readURL(this);" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
			  </div>
              <h3 class="profile-username text-center"><?php echo $item->vcName;?></h3>

              <p class="text-muted text-center"><?php echo $item->vcCode;?></p>
			  <div id="html-content-holder"><center><?php echo bar128(stripslashes($item->vcBarCode));?></center></div>
			  <center><a id="btn-Convert-Html2Image" href="#">Download</a></center>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Open SO</b> <a class="pull-right">IDR <?php echo number_format($opentrans->OpenSOIDR,'2');?></a>
                </li>
                <li class="list-group-item">
                  <b>Open SO (Qty)</b> <a class="pull-right"><?php echo number_format($opentrans->OpenSOQty,'2');?></a>
                </li>
                <li class="list-group-item">
                  <b>Open PO (IDR)</b> <a class="pull-right">IDR <?php echo number_format($opentrans->OpenPOIDR,'2');?></a>
                </li>
				<li class="list-group-item">
                  <b>Open PO (Qty)</b> <a class="pull-right"><?php echo number_format($opentrans->OpenPOQty,'2');?></a>
                </li>
              </ul>
			
              <button data-toggle="modal" type="button" data-target="#modal-delete" class="btn btn-danger btn-block" <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?>><b>Delete</b></button>
			  <button type="button" onclick="toolbar_history()"  class="btn btn-default btn-block" ><b>Show Data Change</b></button>
            </div>
          </div>
		
		</div>
		
		<div class="col-md-9">
          <div class="nav-tabs-custom">
		  
            <ul class="nav nav-tabs">
              <li class="active"><a href="#itemdata" data-toggle="tab">Item Data</a></li>
              <li><a href="#setting" data-toggle="tab">Setting</a></li>
			  <li><a href="#stock" data-toggle="tab">Stock</a></li>
			  <li><a href="#variant" data-toggle="tab">Variant</a></li>
			  <li><a href="#adtdata" data-toggle="tab">Additional Data</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="itemdata">
                <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
					
					<div class="col-md-12">
					  <div class="col-md-6">
						<div class="form-group">
						  <label for="Code">Code</label>
						  <input type="hidden" id="IdToolbar" name="IdToolbar" value="<?php echo $item->intID;?>">
						  <input type="hidden" class="form-control" id="LastCode" name="LastCode" value="<?php echo $item->vcCode;?>">
						  <input type="hidden" class="form-control" id="LastName" name="LastName" value="<?php echo $item->vcName;?>">
						  <input type="text" class="form-control" id="Code" name="Code" readonly="true" value="<?php echo $item->vcCode;?>" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> maxlength="25" required="true" placeholder="Enter Code">
						</div>
						<div class="form-group">
						  <label for="Name">Name</label>
						  <input type="text" class="form-control" id="Name" name="Name" value="<?php echo $item->vcName;?>" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> maxlength="75" required="true" placeholder="Enter Name">
						</div>
						<div class="form-group">
						  <label for="Price">Default Price</label>
						  <input type="number" step="any" class="form-control" id="Price" name="Price" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->intPrice;?>" maxlength="25" required="true" placeholder="Enter Price">
						</div>
						<div class="form-group">
						  <label for="Tax">Tax Group</label>
							<select id="Tax" name="Tax" class="form-control select2" style="width: 100%; height:35px" >
							<?php 
							foreach($listtax->result() as $d) 
							{
							?>	
							<option value="<?php echo $d->intID;?>" <?php if($item->intTax==$d->intID){ echo "selected";}?>><?php echo $d->vcName;?></option>
							<?php 
							}
							?>
							</select>
						</div>
						<div class="form-group">
						  <label for="Remarks">Remarks</label>
						  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> placeholder="Remarks ..."><?php echo $item->vcRemarks;?></textarea>
						</div>
						
					  </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label for="Category">Category</label>
						  <select id="Category" name="Category" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> class="form-control select2" style="width: 100%; height:35px">
								<?php 
								foreach($listcategory->result() as $d) 
								{
									if($item->intCategory==$d->intID){
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($item->intCategory==$d->intID){ echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
									}
								}
								?>
						   </select>
						</div>
						<div class="form-group">
						  <label for="Barcode">Barcode</label>
						  <input type="text" class="form-control" id="Barcode" name="Barcode" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->vcBarCode;?>" maxlength="50" placeholder="Enter Barcode">
						</div>
						<div class="form-group">
						  <label for="UoM">Inventory UoM</label>
						  <input type="text" class="form-control " id="UoM" name="UoM" maxlength="25" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->vcUoM;?>" required="true" placeholder="Enter Inventory UoM">
						</div>
						<div class="form-group">
						  <label for="PurUoM" style="padding-left:0px" class="col-sm-8 control-label">Purchase UoM</label>
						  <label for="intPurUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
						  <div class="col-sm-8" style="padding-left:0px">
						  <input type="text" class="form-control" id="PurUoM" name="PurUoM" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->vcPurUoM;?>" maxlength="75" required="true" placeholder="Enter Purchase UoM">
						  </div>
						 
						  <div class="col-sm-4" style="padding-left:0px">
						  <input type="text" class="form-control" id="intPurUoM" name="intPurUoM" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->intPurUoM;?>" maxlength="15" required="true" placeholder="Item Per Purchase Unit">
						  </div>
						</div>
						<div class="form-group">
						  <label for="SlsUoM" style="padding-left:0px" class="col-sm-8 control-label">Sales UoM</label>
						  <label for="intSlsUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
						  <div class="col-sm-8" style="padding-left:0px">
						  <input type="text" class="form-control" id="SlsUoM" name="SlsUoM" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $item->vcSlsUoM;?>" maxlength="25" required="true" placeholder="Sales UoM">
						  </div>
						  <div class="col-sm-4" style="padding-left:0px">
						  <input type="text" step="any" class="form-control" id="intSlsUoM" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="intSlsUoM" value="<?php echo $item->intSlsUoM;?>" maxlength="15" required="true" placeholder="Item Per Sales Unit">
						  </div>
						</div>
						
						
					  </div>
					  
				  </div>
				  
				  
                </div>
              </div>
			  <div class="tab-pane" id="setting">
                <div class="box-body">
					<table class="table table-striped dt-responsive jambo_table tree">
						<tr>
							<td style="padding:5px">Sales Item?</td>
							<td style="padding:5px"><input type="checkbox" id="intSalesItem" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="intSalesItem" class="minimal" <?php if($item->intSalesItem==1){ echo "checked=checked";}?>></td>
						</tr>
						<tr>
							<td style="padding:5px">Purchase Item?</td>
							<td style="padding:5px"><input type="checkbox" id="intPurchaseItem" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="intPurchaseItem" class="minimal" <?php if($item->intPurchaseItem==1){ echo "checked=checked";}?>></td>
						</tr>
						<tr>
							<td style="padding:5px">Allowed to sell goods even though the stock is not available?</td>
							<td style="padding:5px"><input type="checkbox" id="intSalesWithoutQty" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="intSalesWithoutQty" class="minimal" <?php if($item->intSalesWithoutQty==1){ echo "checked=checked";}?>></td>
						</tr>
						<tr>
							<td style="padding:5px">Active Item?</td>
							<td style="padding:5px"><input type="checkbox" id="intActive" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="intActive" class="minimal" <?php if($item->intActive==1){ echo "checked=checked";}?>></td>
						</tr>
						<tr>
							<td style="padding:5px">Is Asset?</td>
							<td style="padding:5px"><input type="checkbox" id="intAsset" disabled="true" name="intAsset" class="minimal" <?php if($item->intAsset==1){ echo "checked=checked";}?>></td>
						</tr>
						<tr>
							<td style="padding:5px">Batch Management?</td>
							<td style="padding:5px"><input type="checkbox" id="intBatch" disabled="true" name="intBatch" class="minimal" <?php if($item->intBatch==1){ echo "checked=checked";}?> readonly="true" ></td>
						</tr>
						<tr>
								<td style="padding:5px">Is Service?</td>
								<td style="padding:5px"><input type="checkbox" id="intService" name="intService" disabled="true" class="minimal"  <?php if($item->intService==1){ echo "checked=checked";}?> readonly="true" ></td>
							</tr>
					</table>
				</div>
			  </div>
			  <div class="tab-pane" id="stock">
                <div class="box-body">
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Location</th>
					  <th>Stock</th>
					  <th>UoM</th>
					  <th>Item Cost</th>
					 
					</tr>
					</thead>
					<tbody>
					<?php 
					foreach($liststock->result() as $d) 
					{
					?>
					<tr>

					  <td><?php echo $d->vcLocation; ?></td>
					  <td align="right"><?php echo number_format($d->intStock,'2'); ?></td>
					  <td><?php echo $item->vcUoM;?></td>
					  <td align="right"><?php echo number_format($d->intHPP,'2'); ?></td>
					</tr>
					<?php 
					}
					?>
					</tbody>

					</table>
				</div>
			  </div>
			  <div class="tab-pane" id="variant">
                <div class="box-body">
					<div class="" id="form-adddetail">
					<b><i><?php echo $this->lang->line("Form Detail");?></i></b><br>
						
						<div class="col-sm-5" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" class="form-control inputenter" id="VariantName" name="VariantName" maxlength="50" 
							  onchange="isiitemcode()" onpaste="isiitemcode()" placeholder="Variant Name" data-toggle="tooltip" data-placement="top" 
							  title="Variant Name">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="VariantPrice" name="VariantPrice"
							   data-toggle="tooltip" data-placement="top" title="Price" maxlength="15" 
							  placeholder="Price">
						</div>
						
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<button type="button" class="btn btn-primary" id="buttonaddvariant" onclick="addVariant()"><?php echo $this->lang->line("Add");?></button>
						</div>
					</div>
					<div id="mix_variant"></div>
				</div>
			  </div>
			  <div class="tab-pane" id="adtdata">
				<div class="row">
					<div class="col-md-6">
						<?php
						$udf = $this->m_udf->GetDataByDoc('ITM');
		
						foreach($udf->result() as $u)
						{
							
							echo '
							<div class="form-group">
								<label class="col-sm-4 control-label" style="height:20px">'.$u->vcName.'</label>
							';
							if($u->vcType=='text')
							{
								echo'	
									<div class="col-sm-8" style="height:45px">
									<input type="text" class="form-control" id="'.$u->vcCode.'" name="'.$u->vcCode.'" maxlength="'.$u->intLength.'">
									</div>
								';
							}
							else if($u->vcType=='number')
							{
								echo'	
									<div class="col-sm-8" style="height:45px">
									<input type="number" class="form-control" id="'.$u->vcCode.'" name="'.$u->vcCode.'" maxlength="'.$u->intLength.'">
									</div>
								';
							}
							else if($u->vcType=='textarea')
							{
								echo'	
									<div class="col-sm-8">
									<textarea class="form-control" rows="'.$u->intLength.'" id="'.$u->vcCode.'" name="'.$u->vcCode.'"
										  data-toggle="tooltip" data-placement="top" title="'.$u->vcName.'"
									></textarea>
									</div>
								';
							}
							echo '
							</div>
							';
						}
						?>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			  </div>
			  <?php if($crudaccess->intUpdate==1) { ?>
			  <button type="submit" class="btn btn-primary" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>
			  <?php } ?>
            </div>
			
			
          </div>
		  
        </div>
		
		</form>
	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<!--<script src="<?php echo base_url(); ?>asset/htmlcanvas/html2canvas.min.js"></script>-->
<script>
$(document).ready(function(){

var element = $("#html-content-holder"); // global variable
var getCanvas; // global variable
 
	$("#btn-Convert-Html2Image").on('click', function () {
		html2canvas(element, {
         onrendered: function (canvas) {
                //$("#previewImage").append(canvas);
                getCanvas = canvas;
             }
         });	
		var imgageData = getCanvas.toDataURL("image/png");
		// Now browser starts downloading it instead of just showing it
		var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
		$("#btn-Convert-Html2Image").attr("download", "barcode<?php echo $item->vcCode;?>.png").attr("href", newData);
	});
});
</script>
<?php $this->load->view('ctrludf'); ?>
<script>

	function addVariant()
	{
		var VariantName = $("#VariantName").val();
		var VariantPrice = $("#VariantPrice").val();
		
		
		if(VariantName=='')
		{
			alert('Please Fill Variant Name');
			$("#VariantName").focus();
		}
		else if(VariantPrice=='' || VariantPrice==0 || VariantPrice=='0')
		{
			alert('Please Fill Variant Price');
			$("#VariantPrice").focus();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/addvariant", 
				data: "VariantName="+VariantName+"&VariantPrice="+VariantPrice+"&Code=<?php echo $item->vcCode;?>",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						document.getElementById("VariantPrice").value = "";
						document.getElementById("VariantName").value = "";
						$('#mix_variant').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisVariant?Code=<?php echo $item->vcCode;?>');
					}
					else
					{
						alert(msg);
					}
				} 
			}); 
		}
	}
	function delVariant(id)
	{
		$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/delvariant", 
				data: "id="+id+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						$('#mix_variant').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisVariant?Code=<?php echo $item->vcCode;?>');
					}
					else
					{
						alert(msg);
					}
				} 
			}); 
		
	}
function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}

$(function () {
	
	//$('#mix_adtdata').load('<?php echo $this->config->base_url()?>index.php/general_udf/loadhtml?doc=ITM');
	loadudf('<?php echo $item->vcCode;?>');
	$('#mix_variant').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisVariant?Code=<?php echo $item->vcCode;?>');
	
	$('#UoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#PurUoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#SlsUoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
   
  });
</script>

</body>
</html>
