<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">

  <div class="modal fade" id="modal-add">
  <form role="form" method="POST" enctype="multipart/form-data"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd/">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Item</h3>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#general_tab" data-toggle="tab">General</a></li>
						<li><a href="#setting_tab" data-toggle="tab">Setting</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="general_tab">
							<div class="row">
							  <div class="col-md-12">
							    <div class="form-group" align="center">
								  <div class="image-upload">
									<label for="Image">
										<img id="blah"  class="profile-user-img img-responsive img-circle" alt="User profile picture" src="<?php echo base_url(); ?>data/general/dist/img/upload.png"/>
									</label>

									<input type="file" id="Image" name="Image" onchange="readURL(this);"><br>
									Upload Image
								  </div>
								  <!--<label for="exampleInputFile">Image</label><br>
								  <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/box.png" width="35px" height="35px" class="img-circle" alt="User Image">
								  <input type="file" id="Image" name="Image" onchange="readURL(this);">-->
								</div>
							  </div>
							  <div class="col-md-6">
								
								<div class="form-group">
								  <label for="Category">Category</label>
								  <select id="Category" name="Category" class="form-control select2" style="width: 100%; height:35px" onchange="reloadcategory()">
										<?php 
										foreach($listcategory->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
								<div class="form-group">
								  <label for="Code">Code</label>
								  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code" readonly="true">
								</div>
								<div class="form-group">
								  <label for="Name">Name</label>
								  <input type="text" class="form-control" id="Name" name="Name" maxlength="75" required="true" placeholder="Enter Name">
								</div>
								<div class="form-group">
								  <label for="Price">Default Price</label>
								  <input type="number" step="any" class="form-control" id="Price" name="Price" maxlength="25" placeholder="Enter Price">
								</div>
								
								<div class="form-group">
								  <label for="Remarks">Remarks</label>
								  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..."></textarea>
								</div>
								
							  </div>
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Tax">Tax Group</label>
								  <select id="Tax" name="Tax" class="form-control select2" style="width: 100%; height:35px" >
										<?php 
										foreach($listtax->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
								<div class="form-group">
								  <label for="Barcode">Barcode</label>
								  <input type="text" class="form-control" id="Barcode" name="Barcode" maxlength="50" placeholder="Enter Barcode">
								</div>
								<div class="form-group">
								  <label for="UoM">Inventory UoM</label>
								  <input type="text" class="form-control " id="UoM" name="UoM" maxlength="25" required="true" placeholder="Enter Inventory UoM">
								</div>
								<div class="form-group">
								  <label for="PurUoM" style="padding-left:0px" class="col-sm-8 control-label">Purchase UoM</label>
								  <label for="intPurUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
								  
								  <div class="col-sm-8" style="padding-left:0px">
								  <input type="text" class="form-control" id="PurUoM" name="PurUoM" maxlength="75" required="true" placeholder="Enter Purchase UoM">
								  </div>
								  <div class="col-sm-4" style="padding-left:0px">
								  <input type="number" step="any" class="form-control" id="intPurUoM" name="intPurUoM" value="1" maxlength="15" required="true" placeholder="Item Per Purchase Unit">
								  </div>
								</div>
								<div class="form-group">
								  <label for="SlsUoM" style="padding-left:0px" class="col-sm-8 control-label">Sales UoM</label>
								  <label for="intSlsUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
								  
								  <div class="col-sm-8" style="padding-left:0px">
								  <input type="text" class="form-control" id="SlsUoM" name="SlsUoM" maxlength="25" required="true" placeholder="Sales UoM">
								  </div>
								  <div class="col-sm-4" style="padding-left:0px">
								  <input type="number" step="any" class="form-control" id="intSlsUoM" name="intSlsUoM" value="1" maxlength="15" required="true" placeholder="Item Per Sales Unit">
								  </div>
								</div>
								
							  </div>
							</div>
						</div>
						<div class="tab-pane" id="setting_tab">
						  <table class="table table-striped dt-responsive jambo_table hover tree">
							<tr>
								<td style="padding:5px">Sales Item?</td>
								<td style="padding:5px"><input type="checkbox" id="intSalesItem" name="intSalesItem" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Purchase Item?</td>
								<td style="padding:5px"><input type="checkbox" id="intPurchaseItem" name="intPurchaseItem" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Allowed to sell goods even though the stock is not available?</td>
								<td style="padding:5px"><input type="checkbox" id="intSalesWithoutQty" name="intSalesWithoutQty" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Is Assets?</td>
								<td style="padding:5px"><input type="checkbox" id="intAsset" name="intAsset" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Batch Management?</td>
								<td style="padding:5px"><input type="checkbox" id="intBatch" name="intBatch" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Is Service?</td>
								<td style="padding:5px"><input type="checkbox" id="intService" name="intService" class="minimal"></td>
							</tr>
						  </table>
							  
							  
							  
							
						</div>
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<form method="GET" >
			<div class="col-md-12">
				  <div class="col-md-2">
				    <?php if($crudaccess->intCreate==1) {?>
					  <button type="button" class="btn btn-dark" style="widht:100%" data-toggle="modal" data-target="#modal-add"
					  >
					  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
					<?php } ?>
				  </div>
				  <div class="col-md-2">
					  <input type="hidden" class="form-control" id="show" name="show" value="1">
					  <input type="text" class="form-control" id="filtercode" name="filtercode" data-toggle="tooltip" data-placement="top" value="<?php echo $filtercode;?>" maxlength="25" placeholder="Enter Code" title="Item Code">
				  </div>
				  <div class="col-md-4">
					  <input type="text" class="form-control" id="filtername" name="filtername" data-toggle="tooltip" data-placement="top" value="<?php echo $filtername;?>" maxlength="75" placeholder="Enter Name" title="Item Name">
				  </div>
				   <div class="col-md-2">
					  <select id="filtergroup" name="filtergroup" class="form-control select2" style="width: 100%;" data-toggle="tooltip" data-placement="top" title="Item Group">
						<option value="0" >All</option>
						<?php 
						foreach($listgrp->result() as $d) 
						{
						?>	
						<option value="<?php echo $d->intID;?>" <?php if($filtergroup==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
						<?php 
						}
						?>
					  </select>
				  </div>
				  <div class="col-md-2">
					  <button type="submit" class="btn btn-primary">Search</button>
				  </div>
			</div>
			</form>
            </div>
			
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table hover">
                <thead>
                <tr>
				  <th>Name</th>
				  <th>Code</th>
                  <th>Category</th>
				  <th>Price</th>
				  <th>Barcode</th>
				  <th>Tax</th>
				  <th>UoM</th>
				  <th>Pur. UoM</th>
				  <th>Numerator Pur. UoM</th>
				  <th>Sls. UoM</th>
				  <th>Numerator Sls. UoM</th>
				  <th>Remarks</th>
				  <th>Sales Item?</th>
				  <th>Purchase Item?</th>
				  <th>Sales Without Qty?</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				if($show==1)
				{
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td>
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <?php echo $d->vcName; ?>
				  </a>
				  <?php }else{ ?>
				  <?php echo $d->vcName; ?>
				  <?php } ?>
				  </td>
                  <td><?php echo $d->vcCode; ?></td>
				  <td><?php echo $d->vcCategory; ?></td>
				  <td align="right"><?php echo "IDR ".number_format($d->intPrice,'0'); ?></td>
				  
				  <td><?php echo $d->vcBarCode; ?></td>
				  <td><?php echo $d->intTax; ?></td>
				  <td><?php echo $d->vcUoM; ?></td>
				  <td><?php echo $d->vcPurUoM; ?></td>
				  <td align="right"><?php echo number_format($d->intPurUoM,'2'); ?></td>
				  <td><?php echo $d->vcSlsUoM; ?></td>
				  <td align="right"><?php echo number_format($d->intSlsUoM,'2'); ?></td>
				  <td><?php echo $d->vcRemarks; ?></td>
				  <td align="right"><?php echo $d->intSalesItem; ?></td>
				  <td align="right"><?php echo $d->intPurchaseItem; ?></td>
				  <td align="right"><?php echo $d->intSalesWithoutQty; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true"></i></a>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  <?php } ?>
  <script>
  
  
  function setdefaultvalue()
  {
	  var Category = $("#Category").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataDefault?", 
			data: "Category="+Category+"",
			cache: true, 
			success: function(data){
				var obj = JSON.parse(data);
				
				if(obj.intDefPurNum==0){
					obj.intDefPurNum=1;
				}
				if(obj.intDefSlsNum==0){
					obj.intDefSlsNum=1;
				}
				document.getElementById("UoM").value = obj.vcDefBaseUoM;
				document.getElementById("PurUoM").value = obj.vcDefPurUoM;
				document.getElementById("intPurUoM").value = obj.intDefPurNum;
				document.getElementById("SlsUoM").value = obj.vcDefSlsUoM;
				document.getElementById("intSlsUoM").value = obj.intDefSlsNum;
				
				if(obj.intDefSlsItem==1){document.getElementById("intSalesItem").checked = true;}else{document.getElementById("intSalesItem").checked = false;}
				if(obj.intDefPurItem==1){document.getElementById("intPurchaseItem").checked = true;}else{document.getElementById("intPurchaseItem").checked = false;}
				if(obj.intDefSalesWithoutQty==1){document.getElementById("intSalesWithoutQty").checked = true;}else{document.getElementById("intSalesWithoutQty").checked = false;}
				if(obj.intDefAsset==1){document.getElementById("intAsset").checked = true;}else{document.getElementById("intAsset").checked = false;}
				if(obj.intDefBatch==1){document.getElementById("intBatch").checked = true;}else{document.getElementById("intBatch").checked = false;}
			},
			async: false
	  });
  }
  $(function () {
	  	
	reloadcategory();
	setdefaultvalue();
	$('#filtername').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#filtercode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#UoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#PurUoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#SlsUoM').typeahead({
		source: [
			<?php foreach($listuom->result() as $uo)
			{
			?>
		  '<?php echo str_replace("'","\'",$uo->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
  
  function reloadcategory()
  {
	  var Category = $("#Category").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "Category="+Category+"",
			cache: true, 
			success: function(data){
				document.getElementById("Code").value = data;
				document.getElementById("Barcode").value = data;
			},
			async: false
	  });
	  setdefaultvalue();
  }
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}
 
</script>
</body>
</html>
