<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Weskonek.id | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>data/general/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>data/general/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>data/general/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>

#loadingall {
    position:fixed;
    top: 50%;
    left: 50%;
    z-axis:1000 !important;
}

.loadingbar2 {
    position:fixed;
    top: 50%;
    left: 50%;
    z-axis:1000 !important;
}
</style>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<div class="modal fade" id="modal-multiplelogin">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Information for Multiple Logons</h3>
            </div>
            
              <div class="box-body">
			  <div id="multipleinformation"></div>
			  </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="terminate()">Terminate</button>
		   <button type="button" class="btn btn-primary" id="editbutton" onclick="continues()">Continue</button>
         </div>
       </div>
     </div>
</div>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
   <b>WesKonek </b>.id
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <?php $this->load->view('loadingbar2'); ?>
    <form action="javascript:ceklogin();" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="Username" id="Username" placeholder="Username" value="<?php echo $UserName;?>" autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="Password" id="Password" placeholder="Password" value="<?php echo $Password;?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!--<div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!--<div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>-->
    <!-- /.social-auth-links -->

    <!--<a href="<?php echo base_url(); ?>index.php/welcome/forgot/">I forgot my password</a><br>-->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3.1.1 -->
<script src="<?php echo base_url(); ?>data/general/plugins/jQuery/jquery-3.1.1.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>data/general/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>data/general/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  function terminate()
  {
	  location.reload();
  }
  function continues()
  {
	  $.ajax({ 
			type: "GET",
			url: "<?php echo $this->config->base_url()?>index.php/welcome/continuelogons", 
			data: "",  
			cache: false, 
			success: function(msg){ 
				location.reload();
			} 
		}); 
  }
  function ceklogin()
  {
	$(".loadingbar2").show();
	$(':button').prop('disabled', true);
	var Username = $("#Username").val();
	var Password = $("#Password").val();
	
		$.ajax({ 
			type: "GET",
			url: "<?php echo $this->config->base_url()?>index.php/welcome/ceklogin", 
			data: "Username="+Username+"&Password="+Password+"",  
			cache: false, 
			success: function(msg){ 
				if(msg==1)
				{
					$(".loadingbar2").hide();
					$(':button').prop('disabled', false);
					location.reload();
				}
				else if(msg==2)
				{
					$(".loadingbar2").hide();
					$(':button').prop('disabled', false);
					$.ajax({ 
						type: "GET",
						url: "<?php echo $this->config->base_url()?>index.php/welcome/getLogonsInformation", 
						data: "",  
						cache: false, 
						success: function(msg){ 
							var div = document.getElementById('multipleinformation');
							div.innerHTML = msg;
						} 
					}); 
					$('#modal-multiplelogin').modal('show');
					//window.location.href = "<?php echo $this->config->base_url()?>index.php/welcome/logout";
				}
				else
				{
					alert("Wrong Username or Password");
					location.reload();
				}
				
			} 
		}); 
	
  }
</script>
</body>
</html>
