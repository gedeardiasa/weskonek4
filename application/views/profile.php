<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
  


    <script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
</head>
  <?php $this->load->view('body'); ?>
<style>
.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">

  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>


    <!-- Main content -->
    <section class="content">
	<div class="row">
	<form role="form" method="POST" enctype="multipart/form-data"
				action="<?php echo base_url(); ?>index.php/welcome/proseseditprofile/">
        <div class="col-md-3">
		<!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
			  <?php 
			  if(!isset($_SESSION[md5('posroot')]))
			  {
			  ?>
			  <div class="image-upload" align="center">
			  <label for="Image">
			  	
			  <?php if($user->blpImage!=null){echo '<img id="blah" src="data:image/jpeg;base64,'.base64_encode( $user->blpImage ).'" style="width:100px;height:100px" alt="User profile picture" class="profile-user-img img-responsive img-circle"/>';}else{ ?>
              <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="profile-user-img img-responsive img-circle" alt="User profile picture">
			  <?php } ?>
			  </label>
			  <input type="file" id="Image" name="Image" onchange="readURL(this);">
			  </div>
			  <?php 
			  } 
			  else
			  {
			  ?>
			  <div class="image-upload" align="center">
			  <label for="Image">
			  <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="profile-user-img img-responsive img-circle" alt="User profile picture">
			  </label>
			  </div>
			  <?php
			  }
			  ?>
              <h3 class="profile-username text-center"><?php echo $user->vcName;?></h3>

              <p class="text-muted text-center"><?php echo $user->vcEmail;?></p>

              <!--<ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>-->

            </div>
          </div>
		
		</div>
		<div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#userdata" data-toggle="tab">User Data</a></li>
              <li><a href="#password" data-toggle="tab">Change Password</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="userdata">
                <div class="box-body">
				<?php if(isset($_GET['success_pass'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> You are successfully change password.
                </div>
				<?php }?>
				<?php if(isset($_GET['error_oldpass'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> Your old password is wrong.
                </div>
				<?php }?>
				<?php if(isset($_GET['error_confirmpass'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> Your confirm password is wrong.
                </div>
				<?php }?>
				<?php if(isset($_GET['success_edit'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> You are successfully change user data.
                </div>
				<?php }?>
				<?php if(isset($_GET['error_edit'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> You are failed to change user data.
                </div>
				<?php }?>
				
					<div class="form-group">
					  <label for="UserID">User ID</label>
					  <input type="text" class="form-control" id="UserID" name="UserID" readonly="true" maxlength="25" value="<?php echo $user->vcUserID;?>" required="true" placeholder="Enter user id" autofocus>
					</div>
					<div class="form-group">
					  <label for="Email">Email address</label>
					  <input type="email" class="form-control" id="Email" name="Email" readonly="true" maxlength="50" value="<?php echo $user->vcEmail;?>" required="true"  placeholder="Enter email">
					</div>
					<div class="form-group">
					  <label for="Name">Name</label>
					  <input type="text" class="form-control" id="Name" name="Name" required="true"  value="<?php echo $user->vcName;?>" maxlength="100" placeholder="Enter name">
					</div>
					<?php 
					  if(!isset($_SESSION[md5('posroot')]))
					  {
					  ?>
					<div class="form-group">
					  <label for="Name">Phone</label>
					  <input type="text" class="form-control" id="Phone" name="Phone" required="true"  value="<?php echo $user->vcPhone;?>" maxlength="50" placeholder="Enter phone">
					</div>
					<div class="form-group">
					  <label for="SetIcon">Icon Size</label>
					  <select class="form-control" id="SetIcon" name="SetIcon">
					  <option value="" <?php if($user->vcIcon==''){ echo 'selected';}?>>Small</option>
					  <option value="fa-2x" <?php if($user->vcIcon=='fa-2x'){ echo 'selected';}?>>Medium</option>
					  </select>
					</div>
					<?php
					  }
					  
					?>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</form>
                </div>
              </div>
              <div class="tab-pane" id="password">
                <div class="box-body">
				<form role="form" method="POST"
				action="<?php echo base_url(); ?>index.php/welcome/proseseditprofilepass/">
					<div class="form-group">
					  <label for="OldPassword">Old Password</label>
					  <input type="password" class="form-control" id="OldPassword" name="OldPassword" required="true"  maxlength="50" placeholder="Password">
					</div>
					<div class="form-group">
					  <label for="NewPassword">New Password</label>
					  <input type="password" class="form-control" id="NewPassword" name="NewPassword" required="true"  maxlength="50" placeholder="Password">
					</div>
					
					<div class="form-group">
					  <label for="ConfirmPassword">Confirm Password</label>
					  <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" required="true"  maxlength="50" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-primary">Save changes</button>
				
                </div>
              </div>
			 
            </div>
          </div>
        </div>
		</form>
	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>

function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}
</script>
</body>
</html>
