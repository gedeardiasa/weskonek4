<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
    <!--<script src="<?php echo base_url(); ?>asset/hc/highcharts.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/exporting.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/highcharts-more.js"></script>
	<script src="<?php echo base_url(); ?>asset/hc/solid-gauge.js"></script>-->
</head>
  <?php $this->load->view('bodycollapse'); ?>

<div class="wrapper">

  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Search Result
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Search Result</a></li>
        <li class="active"></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<div class="box">
       <div class="box-body">
	     <table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Key</th>
				  <th>Type</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
            </thead>
            <tbody>
			<?php
			foreach($list->result() as $d) 
				{
				?>
                <tr>
				  <td>
				  <?php echo $d->vcKey;?>
				  </td>
                  <td>
				  <?php echo $d->vcType;?>
				  </td>
				  <td>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $d->vcUrl;?><?php echo $d->intID;?>" target="_blank">
				  <i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true"></i></a>
				  </td>
                </tr>
				<?php 
				}
				?>
            </tbody>
         </table>
	   </div>
	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  
 <script>
  $(function () {
	  
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
 
  
 
</script>
</body>
</html>