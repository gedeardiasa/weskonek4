<div class="modal fade" id="modal-batch">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Batch List</h3>
            </div>
            
              <div class="box-body">
              <div id="selectbatchlist"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>