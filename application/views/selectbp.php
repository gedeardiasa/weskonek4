<div class="modal fade" id="modal-bp">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">BP List</h3>
            </div>
            
              <div class="box-body">
				<table id="example3" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
                <thead>
                <tr>
                  <th>Code</th>
				  <th>Name</th>
				  <th>Category</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($autobp->result() as $d) 
				{
				?>
                <tr onclick="insertBP('<?php echo $d->vcCode; ?>','<?php echo str_replace("'","\'",$d->vcName);?>');">
                  <td><?php echo $d->vcCode; ?></td>
                  <td><?php echo $d->vcName; ?></td>
				  <td><?php echo $d->vcCategory; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectbp()">Cancel</button>
         </div>
       </div>
     </div>
   </div>