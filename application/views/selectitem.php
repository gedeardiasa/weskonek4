<?php
if(!isset($_SESSION['SHOWMODALITEMPOS']))
{
	$_SESSION['SHOWMODALITEMPOS']=0;
	
}
if($_SESSION['SHOWMODALITEMPOS']==1)
{
?>
<div class="modal fade" id="modal-item">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
			  <div class="col-sm-6">
              <h3 class="box-title">Item List</h3>
			  </div>
			  <div class="col-sm-6" align="right">
			  <input align="right" type="text" name="searchitem" id="searchitem" onchange="searchitem()" onkeyup="searchitem()" placeholder="search">
			  </div>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					<?php 
					$x=0;
					foreach($autoitemcategory->result() as $au)
					{
					?>
					<li id="detailcategory<?php echo $au->vcCode;?>"<?php if($x==0){ echo 'class="active"';}?>><a href="#<?php echo $au->intID;?>selectitem" data-toggle="tab"><?php echo $au->vcName;?></a></li>
					<?php
					$x++;
					}
					?>
					</ul>
					<div class="tab-content">
						<?php 
						$y=0;
						foreach($autoitemcategory->result() as $au)
						{
						?>
						<div class="<?php if($y==0){ echo 'active';}?> tab-pane" id="<?php echo $au->intID;?>selectitem">
							<div class="box-body">
							<?php foreach($autoitem->result() as $ai)
							{
								if($ai->intCategory==$au->intID)
								{
							?>
								<div class="col-sm-2" name="detailitem<?php echo $ai->vcCode;?>" id="detailitem<?php echo $ai->vcCode;?>" onclick="insertItem('<?php echo str_replace("'","\'",$ai->vcName);?>')">
									<div align="center"><?php echo substr($ai->vcName,0,20);?>
									</div>
									<?php if($ai->blpImageMin!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $ai->blpImageMin ).'" style="width:125px;height:85px" class="col-sm-12 img-circle" alt="Item Image"/>';}else{ ?>
									<img src="<?php echo base_url(); ?>data/general/dist/img/box.png" class="col-sm-12 img-circle" style="width:125px;height:85px" alt="Item Image">
									<?php } ?>
									
									
								</div>
							<?php
								}
							}
							?>
							</div>
						</div>
						<?php
						$y++;
						}
						?>
						
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <script>
   function searchitem()
   {
		var cari = $("#searchitem").val();
		<?php 
		foreach($autoitem->result() as $ai)
		{
		?>
		var key = "<?php echo $ai->vcName;?>";
		key		= key.toLowerCase();
		var n = key.search(cari);
		
		if(n>0 || cari=="")
		{
			$("#detailitem<?php echo $ai->vcCode;?>").show();
		}
		else
		{
			$("#detailitem<?php echo $ai->vcCode;?>").hide();
		}
		<?php
		}
		?>
		
		
		
		<?php
		foreach($autoitemcategory->result() as $au)
		{?>
			var sow = 0;
			<?php
			foreach($autoitem->result() as $ai)
			{
				if($ai->intCategory==$au->intID)
				{
		?>
					var key = "<?php echo $ai->vcName;?>";
					key		= key.toLowerCase();
					var n = key.search(cari);
					if(n>0 || cari=="")
					{
						sow = 1;
					}
		<?php
				}
			}
		?>
			if(sow==1)
			{
				$("#detailcategory<?php echo $au->vcCode;?>").show();	
			}
			else
			{
				$("#detailcategory<?php echo $au->vcCode;?>").hide();
			}
		<?php
		}	
		?>
   }
   
   
   </script>
<?php
}
else
{
?>
<div class="modal fade" id="modal-item">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
           
            
              <div class="box-body">
			  <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                This Menu Disable For You. Please Contact Admin
              </div>
				
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>


<?php
}
?>	