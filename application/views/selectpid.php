<div class="modal fade" id="modal-pid">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">PID List</h3>
            </div>
            
              <div class="box-body">
				<table id="exampleSO" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Date</th>
				  <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listpid->result() as $d) 
				{
				?>
                <tr onclick="insertPID('<?php echo $d->intID; ?>');">
                  <td><?php echo $d->vcDocNum; ?></td>
				  <td><?php echo date('d F Y',strtotime($d->dtDate)); ?></td>
				  <td><?php echo $d->vcRemarks; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>