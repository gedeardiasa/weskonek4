<div class="modal fade" id="modal-pp">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">PR List</h3>
            </div>
            
              <div class="box-body">
				<table id="examplePO" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Date</th>
				  <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listpp->result() as $d) 
				{
				?>
                <tr onclick="insertPP('<?php echo $d->intID; ?>');">
                  <td><?php echo $d->vcDocNum; ?></td>
				  <td><?php echo date('d F Y',strtotime($d->dtDate)); ?></td>
				  <td><?php echo $d->vcRemarks; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>