<div class="modal fade" id="modal-pro">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">PRO List</h3>
            </div>
            
              <div class="box-body">
				<table id="examplePRO" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Rev</th>
				  <th>Status</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listpro->result() as $d) 
				{
				?>
                <tr onclick="insertPRO('<?php echo $d->intID; ?>');">
                  <td><?php echo $d->vcDocNum; ?></td>
                  <td><?php echo $d->vcItemCode; ?></td>
				  <td><?php echo $d->vcItemName; ?></td>
				  <td><?php echo $d->vcRef; ?></td>
				  <td><?php echo $d->vcStatus; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>