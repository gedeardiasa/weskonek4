<div class="modal fade" id="modal-sr">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->lang->line('Sales Return List');?></h3>
            </div>
            
              <div class="box-body">
				<table id="exampleSR" class="table table-striped dt-responsive jambo_table hover" style="width:100%">
                <thead>
                <tr>
                  <th><?php echo $this->lang->line('Doc. Number');?></th>
				  <th><?php echo $this->lang->line('BP. Name');?></th>
				  <th><?php echo $this->lang->line('Date');?></th>
				  <th><?php echo $this->lang->line('Remarks');?></th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listsr->result() as $d) 
				{
				?>
                <tr onclick="insertSR('<?php echo $d->intID; ?>');">
                  <td><?php echo $d->vcDocNum; ?></td>
                  <td><?php echo $d->vcBPName; ?></td>
				  <td><?php echo date('d F Y',strtotime($d->dtDate)); ?></td>
				  <td><?php echo $d->vcRemarks; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo $this->lang->line('Cancel');?></button>
         </div>
       </div>
     </div>
   </div>