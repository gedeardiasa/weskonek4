<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>

<div class="wrapper">

  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>
	<div class="modal fade" id="modal-detailopenso">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">SO List</h3>
            </div>
            
              <div class="box-body">
				<div id="listopenso"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>

   <div class="modal fade" id="modal-detailbatch">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Batch List</h3>
            </div>
            
              <div class="box-body">
				<div id="listbatch"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form id="demo-form2" method="GET" action="">
				<div class="row">
				
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="ItemCode" class="col-sm-4 control-label" style="height:20px">Item Code</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="<?php echo $ItemCode;?>" id="ItemCode" name="ItemCode" data-toggle="tooltip" data-placement="top" title="Item Code">
							</div>
						</div>
						<div class="form-group">
						  <label for="ItemName" class="col-sm-4 control-label" style="height:20px">Item Name</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="<?php echo $ItemName;?>" id="ItemName" name="ItemName" data-toggle="tooltip" data-placement="top" title="Item Name">
							</div>
						</div>
						<div class="form-group">
						  <label for="ItemGroup" class="col-sm-4 control-label" style="height:20px">Item Group</label>
							<div class="col-sm-8" style="height:45px">
							<select id="ItemGroup" name="ItemGroup" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Group">
								<option value="0" >All</option>
								<?php 
								foreach($listgrp->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($ItemGroup==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="Setting" class="col-sm-4 control-label" style="height:20px">Highlight Field</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Highlight" name="Highlight" class="form-control" data-toggle="tooltip" data-placement="top" title="Highlight">
								<option value="NetQty" <?php if($Highlight=='NetQty'){ echo 'selected';}?>>Net Qty</option>
								<option value="StockQty" <?php if($Highlight=='StockQty'){ echo 'selected';}?>>Stock Qty</option>
								<?php if($uservalue==1){?><option value="Value" <?php if($Highlight=='Value'){ echo 'selected';}?>>Value</option><?php } ?>
								<?php if($uservalue==1){?><option value="TotalValue" <?php if($Highlight=='TotalValue'){ echo 'selected';}?>>Total Value</option><?php } ?>
								<?php if($uservalue==1){?><option value="TotalNetValue" <?php if($Highlight=='TotalNetValue'){ echo 'selected';}?>>Total Net Value</option><?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="Setting" class="col-sm-4 control-label" style="height:20px">Setting</label>
							<div class="col-sm-8" style="height:45px">
							<input type="checkbox" name="zeroqty" id="zeroqty"
							<?php if($zeroqty=='on'){echo 'checked=checked';}?>
							>No Zero Stock Lines <?php echo $zeroqty;?>
							</div>
						</div>
						
						<div class="form-group">
						  <label for="Setting" class="col-sm-4 control-label" style="height:20px">Active Item?</label>
							<div class="col-sm-8" style="height:45px">
							<input type="checkbox" name="activeitem" id="activeitem"
							<?php if($activeitem=='on'){echo 'checked=checked';}?>>Active/Not
							</div>
						</div>
						
					</div>
					<div class="col-sm-6">
					<label for="Location" class="col-sm-12 control-label" style="height:20px">Location</label>
					<?php 
					foreach($listwhs->result() as $d) 
					{
					?>
					<div class="col-sm-4">
					<input type="checkbox" name="loc<?php echo $d->vcCode;?>" id="loc<?php echo $d->vcCode;?>"
					<?php if(isset($sloc[$d->vcCode])){echo 'checked=checked';}?>
					><?php echo $d->vcCode;?>-<?php echo $d->vcName;?>
					</div>
					<?php
					}
					?>
					</div>
					
					
				
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </div>
				</div>
				</form>
				<hr>
				<?php
				if($Highlight=='NetQty')
				{
					$HighlightField='Qty - Open SO';
				}
				else if($Highlight=='StockQty')
				{
					$HighlightField='Qty';
				}
				else if($Highlight=='Value')
				{
					$HighlightField='Value';
				}
				else if($Highlight=='TotalValue')
				{
					$HighlightField='Total Value';
				}
				else if($Highlight=='TotalNetValue')
				{
					$HighlightField='Total Net Value';
				}
				else
				{
					$HighlightField='Unknown';
				}
				?>
				<table id="example1" class="table table-striped dt-responsive jambo_table hover">
                <thead>
                <tr>
				  <th>Item Group</th>
				  <th>Item Name</th>
				  <th><?php echo $HighlightField;?></th>
                  <th>Item Code</th>
				  <th>Location</th>
                  <th>Qty</th>
				  <th>UoM</th>
				  <th>Qty (Pur)</th>
				  <th>UoM (Pur)</th>
				  <th>Qty (Sls)</th>
				  <th>UoM (Sls)</th>
				  <th>Open SO</th>
				  <th>Qty - Open SO</th>
                  <?php if($uservalue==1){?>
                  <th>Value</th>
				  <th>Total Value</th>
				  <?php } ?>
				  <!--<th>Set Active/Not</th>-->
                </tr>
                </thead>
                <tbody>
				<?php 
				$HighlightTotal=0;
				foreach($liststock->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->GroupName; ?></td>
				  <td><?php echo $d->ItemName; ?></td>
				  <td  align="right" style="background-color:green; color:white">
				  <?php 
					if($Highlight=='NetQty')
					{
						$HighlightValue=number_format($d->intStock-$d->OpenSO,'2');
						$kjhgn=$d->intStock-$d->OpenSO;
					}
					else if($Highlight=='StockQty')
					{
						$HighlightValue=number_format($d->intStock,'2');
						$kjhgn=$d->intStock;
					}
					else if($Highlight=='Value' and $uservalue==1)
					{
						$HighlightValue=number_format($d->intHPP,'2');
						$kjhgn=$d->intHPP;
					}
					else if($Highlight=='TotalValue' and $uservalue==1)
					{
						$HighlightValue=number_format($d->intHPP*$d->intStock,'2');
						$kjhgn=$d->intHPP-$d->intStock;
					}
					else if($Highlight=='TotalNetValue' and $uservalue==1)
					{
						$HighlightValue=number_format($d->intHPP*($d->intStock-$d->OpenSO),'2');
						$kjhgn=$d->intHPP*($d->intStock-$d->OpenSO);
					}
					else
					{
						$HighlightValue=0;
					}
				    $HighlightTotal=$HighlightTotal+$kjhgn;
				  echo $HighlightValue; 
				  
				  ?>
				  
				  </td>
				  <td><?php echo $d->ItemCode; ?></td>
				  
				  <td><?php echo $d->LocationCode."-".$d->LocationName; ?></td>
				  <?php
					$this->load->model('m_batch');
					$this->load->model('m_item');
					$cekbatch = $this->m_item->cekbatch($d->intItem);
					if($cekbatch==1){
						?>
						<td><a href="#" onclick="showdetailBatch('<?php echo $d->intItem; ?>','<?php echo $d->intLocation; ?>')"><?php echo number_format($d->intStock,'2'); ?></a></td>
						<?php
					}else{
						?>
						<td><?php echo number_format($d->intStock,'2'); ?></td>
						<?php
					}
				  ?>
				  
				  <td><?php echo $d->vcUoM; ?></td>
				  <td align="right"><?php echo number_format((1/$d->intPurUoM)*$d->intStock,'2'); ?></td>
				  <td><?php echo $d->vcPurUoM; ?></td>
				  <td align="right"><?php echo number_format((1/$d->intSlsUoM)*$d->intStock,'2'); ?></td>
				  <td><?php echo $d->vcSlsUoM; ?></td>
				  <td align="right"><a href="#" onclick="showdetailOpenSO('<?php echo $d->intItem; ?>','<?php echo $d->intLocation; ?>')"><?php echo number_format($d->OpenSO,'2'); ?></a></td>
				  <td align="right"><?php echo number_format($d->intStock-$d->OpenSO,'2'); ?></td>
				  <?php if($uservalue==1){?>
				  <td align="right"><?php echo number_format($d->intHPP,'0'); ?></td>
				  <td align="right"><?php echo number_format($d->intHPP*$d->intStock,'0'); ?></td>
				   <?php } ?>
				  <!--<td> <button type="button" class="btn btn-primary" onclick="activeornot('<?php echo $d->ItemCode; ?>')">Set</button></td>-->
                </tr>
				<?php 
				}
				?>
                </tbody>
				<!--<tfoot>
                <tr>
				  <th>Total</th>
				  <th></th>
				  <th align="right"><?php echo number_format($HighlightTotal,'2');?></th>
                  <th></th>
				  <th></th>
                  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
                  <?php if($uservalue==1){?>
                  <th></th>
				  <th></th>
				  <?php } ?>
				  <th></th>
                </tr>
                </tfoot>-->
              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  
  <?php } ?>
  <script>
   function showdetailOpenSO(item,loc)
   {
	   $("#modal-detailopenso").modal('show');
	   $('#listopenso').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/detailopenso/'+item+'?loc='+loc+'');
   }

   function showdetailBatch(item,loc)
   {
	   $("#modal-detailbatch").modal('show');
	   $('#listbatch').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/detailbatch/'+item+'?loc='+loc+'');
   }

  function activeornot(item)
  {
	  $.ajax({ 
			type: "GET",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/activeornot?", 
			data: "item="+item+"",
			cache: true, 
			success: function(data){
				alert('Success');
			},
			async: false
	  });
  }

  $(function () {
	//fungsi typeahead
	$('#ItemName').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
