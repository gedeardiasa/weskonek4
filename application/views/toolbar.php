<?php if($this->uri->segment(1)!='asset' and $this->uri->segment(1)!='bp' and $this->uri->segment(1)!='item' and $this->uri->segment(1)!='user'){?>
<div class="col-sm-12 hidden-xs" align="right">

	<?php if($this->uri->segment(1)!='dep_with' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'
	 and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	 and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category' 
	 and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	 and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	 and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet' and $this->uri->segment(1)!='apdp'
	 and $this->uri->segment(1)!='ardp'
	 and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-file-o fa-2x" aria-hidden="true" onclick="initialadd()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('New');?>"></i>
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='dep_with' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category'
	and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet' and $this->uri->segment(1)!='apdp'
	and $this->uri->segment(1)!='ardp'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-files-o fa-2x" aria-hidden="true" onclick="toolbar_duplicate()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Duplicated');?>"></i>
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master' and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group' and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category' and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-print fa-2x" aria-hidden="true" onclick="toolbar_print()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Print');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master' and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group' and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category' and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-envelope-o fa-2x" aria-hidden="true" onclick="toolbar_mail()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Send E-Mail');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master' and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group' and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category' and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-file-excel-o fa-2x" aria-hidden="true" onclick="toolbar_excel()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Excel');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master' and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group' and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category' and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period'
	and $this->uri->segment(1)!='plan' and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax'
	and $this->uri->segment(1)!='udf' and $this->uri->segment(1)!='wallet'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-file-word-o fa-2x" aria-hidden="true" onclick="toolbar_word()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Word');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)=='sq' or $this->uri->segment(1)=='so' or $this->uri->segment(1)=='dn'  or $this->uri->segment(1)=='do2' 
	 or $this->uri->segment(1)=='ar'  or $this->uri->segment(1)=='sr'  or $this->uri->segment(1)=='arcm'
	 or $this->uri->segment(1)=='pp' or $this->uri->segment(1)=='po' or $this->uri->segment(1)=='grpo'
	 or $this->uri->segment(1)=='ap' or $this->uri->segment(1)=='apcm' or $this->uri->segment(1)=='pr'){?>
	<i class="fa fa-link fa-2x" aria-hidden="true" onclick="toolbar_docflow()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Related Documents');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)=='sq' or $this->uri->segment(1)=='asset_class' or $this->uri->segment(1)=='bank_master' or $this->uri->segment(1)=='bom'
	 or $this->uri->segment(1)=='bp' or $this->uri->segment(1)=='bp_category' or $this->uri->segment(1)=='coa' or $this->uri->segment(1)=='do2'
	 or $this->uri->segment(1)=='doc_group' or $this->uri->segment(1)=='doc_set' or $this->uri->segment(1)=='group_cf' 
	 or $this->uri->segment(1)=='item_category' or $this->uri->segment(1)=='location' or $this->uri->segment(1)=='mvt'
	 or $this->uri->segment(1)=='post_period' or $this->uri->segment(1)=='plan' or $this->uri->segment(1)=='po' or $this->uri->segment(1)=='pp'
	 or $this->uri->segment(1)=='price' or $this->uri->segment(1)=='pro' or $this->uri->segment(1)=='profile' or $this->uri->segment(1)=='so'
	 or $this->uri->segment(1)=='tax' or $this->uri->segment(1)=='udf'
	 or $this->uri->segment(1)=='wallet' or $this->uri->segment(1)=='wo'){?>
	<i class="fa fa-history fa-2x" aria-hidden="true" onclick="toolbar_history()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Data Change');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<!--<i class="fa fa-file-pdf-o fa-2x" aria-hidden="true" onclick="toolbar_pdf()" data-toggle="tooltip" data-placement="top" title="PDF"></i>&nbsp;&nbsp;-->
	
	
	<?php if($this->uri->segment(1)!='jurnal' and $this->uri->segment(1)!='so' and $this->uri->segment(1)!='pid' and $this->uri->segment(1)!='sq'
	 and $this->uri->segment(1)!='po' and $this->uri->segment(1)!='pp' and $this->uri->segment(1)!='bom' and $this->uri->segment(1)!='disc' 
	 and $this->uri->segment(1)!='dep_run' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'
	 and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	 and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category'
	 and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period' and $this->uri->segment(1)!='plan'
	 and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax' and $this->uri->segment(1)!='udf'
	 and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet'
	 and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-balance-scale fa-2x" aria-hidden="true" onclick="toolbar_je()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Journal Entry');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='dep_with' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category'
	and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period' and $this->uri->segment(1)!='plan'
	and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax' and $this->uri->segment(1)!='udf'
	and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet' and $this->uri->segment(1)!='apdp'
	and $this->uri->segment(1)!='ardp'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-arrow-left fa-2x" aria-hidden="true" onclick="toolbar_previous()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Previous');?>"></i>
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='dep_with' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category'
	and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period' and $this->uri->segment(1)!='plan'
	and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax' and $this->uri->segment(1)!='udf'
	and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet' and $this->uri->segment(1)!='apdp'
	and $this->uri->segment(1)!='ardp'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-arrow-right fa-2x" aria-hidden="true" onclick="toolbar_next()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Next');?>"></i>
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='dep_with' and $this->uri->segment(1)!='pnl' and $this->uri->segment(1)!='asset_class' and $this->uri->segment(1)!='bank_master'and $this->uri->segment(1)!='bp_category' and $this->uri->segment(1)!='coa' and $this->uri->segment(1)!='doc_group'
	and $this->uri->segment(1)!='doc_set' and $this->uri->segment(1)!='group_cf' and $this->uri->segment(1)!='item_category'
	and $this->uri->segment(1)!='location' and $this->uri->segment(1)!='mvt' and $this->uri->segment(1)!='post_period' and $this->uri->segment(1)!='plan'
	and $this->uri->segment(1)!='price' and $this->uri->segment(1)!='profile' and $this->uri->segment(1)!='tax' and $this->uri->segment(1)!='udf'
	and $this->uri->segment(1)!='wallet' and $this->uri->segment(1)!='balance_sheet' and $this->uri->segment(1)!='apdp'
	and $this->uri->segment(1)!='ardp'
	and $this->uri->segment(1)!='activity'){?>
	<i class="fa fa-refresh fa-2x" aria-hidden="true" onclick="toolbar_reload()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Refresh');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
	<?php if($this->uri->segment(1)!='pnl'){?>
	<i align="right"class="fa fa-minus fa-2x" aria-hidden="true" onclick="toolbar_cancel()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Back');?>"></i>&nbsp;&nbsp;
	<?php } ?>
	
	
</div>


<div class="col-sm-12 hidden-sm hidden-md hidden-lg" align="right">
	<i class="fa fa-file-o fa-2x" aria-hidden="true" onclick="initialadd()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('New');?>"></i>
	<i class="fa fa-files-o fa-2x" aria-hidden="true" onclick="toolbar_duplicate()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Duplicated');?>"></i>
	<i class="fa fa-arrow-left fa-2x" aria-hidden="true" onclick="toolbar_previous()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Previous');?>"></i>
	<i class="fa fa-arrow-right fa-2x" aria-hidden="true" onclick="toolbar_next()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Next');?>"></i>
	<i class="fa fa-refresh fa-2x" aria-hidden="true" onclick="toolbar_reload()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Refresh');?>"></i>&nbsp;&nbsp;
	<i align="right"class="fa fa-minus fa-2x" aria-hidden="true" onclick="toolbar_cancel()" data-toggle="tooltip" data-placement="top" title="<?php echo $this->lang->line('Back');?>"></i>&nbsp;&nbsp;
</div>
<?php } ?>

<script>
function toolbar_duplicate() 
{				
	var idHeader = $("#idHeader").val();
	if(idHeader==0)
	{
		//alert('xxx');
	}
	else
	{
		initialduplicate(idHeader);
	}
	
}
function toolbar_docflow() 
{
	var IdToolbar=$("#IdToolbar").val();
	
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/docflow/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_docflow', 'width=980,height=600,left=200,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
	
}
function toolbar_history() 
{
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/history/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_history', 'width=1180,height=600,left=100,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
	
}
function toolbar_print() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php 
	if($this->uri->segment(1)=='pnl')
	{
		$adttoldata = $daterange."|".$plan."";
	}
	else if($this->uri->segment(1)=='balance_sheet')
	{
		$adttoldata = $Date."|".$plan."";
	}
	else
	{
		$adttoldata = '';
	}
	?>
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/prints/'+IdToolbar+'?type=<?php echo $typetoolbar ?>&adtdata=<?php echo $adttoldata; ?>', 'toolbar_print', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
}
function toolbar_je() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ 
	if($typetoolbar=='REVALUATION')$typetoolbar='RV';
	?>
	window.open('<?php echo base_url() ?>index.php/toolbar/jurnal/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_jurnal', 'width=1180,height=600,left=100,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
}
function toolbar_mail() 
{
	alert('Send Mail');
}
function toolbar_excel() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php 
	if($this->uri->segment(1)=='pnl')
	{
		$adttoldata = $daterange."|".$plan."";
	}
	else if($this->uri->segment(1)=='balance_sheet')
	{
		$adttoldata = $Date."|".$plan."";
	}
	else
	{
		$adttoldata = '';
	}
	?>
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/prints/'+IdToolbar+'?type=<?php echo $typetoolbar ?>&excel=true&adtdata=<?php echo $adttoldata; ?>', 'toolbar_excel', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
}
function toolbar_word() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php 
	if($this->uri->segment(1)=='pnl')
	{
		$adttoldata = $daterange."|".$plan."";
	}
	else if($this->uri->segment(1)=='balance_sheet')
	{
		$adttoldata = $Date."|".$plan."";
	}
	else
	{
		$adttoldata = '';
	}
	?>
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/prints/'+IdToolbar+'?type=<?php echo $typetoolbar ?>&word=true&adtdata=<?php echo $adttoldata; ?>', 'toolbar_word', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
}
function toolbar_pdf() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php 
	if($this->uri->segment(1)=='pnl')
	{
		$adttoldata = $daterange."|".$plan."";
	}
	else if($this->uri->segment(1)=='balance_sheet')
	{
		$adttoldata = $Date."|".$plan."";
	}
	else
	{
		$adttoldata = '';
	}
	?>
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/pdf/'+IdToolbar+'?type=<?php echo $typetoolbar ?>&adtdata=<?php echo $adttoldata; ?>', 'toolbar_pdf', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('<?php echo $this->lang->line('Toolbar type is not set');?>');
	<?php } ?>
}
function toolbar_previous() 
{
	var idHeader = $("#idHeader").val();
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/toolbar/previous", 
		data: "idHeader="+idHeader+"&type=<?php echo $typetoolbar ?>",  
		cache: false, 
		success: function(data){ 

			if(data!=0 && data!='0')
			{
				initialedit(data);
			}
		} 
	}); 
}
function toolbar_next() 
{
	var idHeader = $("#idHeader").val();
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/toolbar/next", 
		data: "idHeader="+idHeader+"&type=<?php echo $typetoolbar ?>",  
		cache: false, 
		success: function(data){ 
			if(data!=0 && data!='0')
			{
				initialedit(data);
			}
		} 
	}); 
}
function toolbar_reload() 
{
	var idHeader = $("#idHeader").val();
	if(idHeader==0)
	{
		initialadd();
	}
	else
	{
		initialedit(idHeader);
	}
	
}
function toolbar_cancel() 
{
	$("#modal-add-edit").modal('hide');
	$("#modal-addedit").modal('hide');
}

</script>