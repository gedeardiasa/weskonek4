<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>


  
  
  
	<div class="box">
           
            <div class="box-body">
				<hr>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Doc</th>
				  <th>Table</th>
				  <th>Field</th>
				  <th>Field (Desc.)</th>
                  <th>Header Key</th>
                  <th>Detail Key</th>
				  <th>Value Before</th>
				  <th>Value After</th>
				  <th>User</th>
				  <th>Date</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcDoc; ?></td>
				  <td><?php echo $d->vcTable; ?></td>
				  <td><?php echo $d->vcField; ?></td>
				  <td><?php echo $d->ColumnName; ?></td>
				  <td><?php echo $d->intIDKey; ?></td>
				  <td><?php echo $d->vcDetailKey; ?></td>
				  <td><?php echo $d->vcBeforeValue; ?></td>
				  <td><?php echo $d->vcAfterValue; ?></td>
				  <td><?php echo $d->vcUserID; ?></td>
				  <td><?php echo $d->dtUpdateTime; ?></td>
                </tr>
				
				<?php 
				}
				?>
                </tbody>

              </table>
			 
			</div>
			<hr>
			
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	  
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
 
 
</script>
</body>
</html>
