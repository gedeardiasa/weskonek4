<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>


  
  
  
	<div class="box">
           
            <div class="box-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" readonly="true" value="<?php echo $header->vcDocNum;?>" data-toggle="tooltip" data-placement="top" title="Document Number">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" value="<?php echo date('m/d/Y',strtotime($header->dtDate));?>"readonly="true" placeholder="Doc. Date"  data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label for="RefType" class="col-sm-4 control-label" style="height:20px">Ref. Type</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefType" name="RefType" placeholder="Ref. Type" value="<?php echo $header->vcRefType;?>" readonly="true" data-toggle="tooltip" data-placement="top" title="Reference Type">
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" value="<?php echo $header->vcRef;?>" readonly="true" data-toggle="tooltip" data-placement="top" title="Reference Number">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>GL Code</th>
				  <th>GL Name</th>
				  <th>Debit</th>
                  <th>Credit</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcGLCode; ?></td>
				  <td><?php echo $d->vcGLName; ?></td>
				  <td><?php echo "IDR ".number_format($d->intValue,'0'); ?></td>
				  <td></td>
                </tr>
				<tr>
                  <td><?php echo $d->vcGLCodeX; ?></td>
				  <td><?php echo $d->vcGLNameX; ?></td>
				  <td></td>
				  <td><?php echo "IDR ".number_format($d->intValue,'0'); ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			  <div class="form-group">
                  <label>Remarks</label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..." readonly="true" data-toggle="tooltip" data-placement="top" title="Remarks"><?php echo $header->vcRemarks;?>
				  </textarea>
                </div>
			</div>
			<hr>
			
			
			<h3><i><u>Cancellation Journal</u></i></h3>
			
			<?php foreach($headerx2->result() as $headerx){
				
				$listx= $this->m_jurnal->GetDetailByHeaderID($headerx->intID);
			?>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" readonly="true" value="<?php echo $headerx->vcDocNum;?>" data-toggle="tooltip" data-placement="top" title="Document Number">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" value="<?php echo date('m/d/Y',strtotime($headerx->dtDate));?>"readonly="true" placeholder="Doc. Date"  data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label for="RefType" class="col-sm-4 control-label" style="height:20px">Ref. Type</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefType" name="RefType" placeholder="Ref. Type" value="<?php echo $headerx->vcRefType;?>" readonly="true" data-toggle="tooltip" data-placement="top" title="Reference Type">
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" value="<?php echo $headerx->vcRef;?>" readonly="true" data-toggle="tooltip" data-placement="top" title="Reference Number">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>GL Code</th>
				  <th>GL Name</th>
				  <th>Debit</th>
                  <th>Credit</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listx->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcGLCode; ?></td>
				  <td><?php echo $d->vcGLName; ?></td>
				  <td><?php echo "IDR ".number_format($d->intValue,'0'); ?></td>
				  <td></td>
                </tr>
				<tr>
                  <td><?php echo $d->vcGLCodeX; ?></td>
				  <td><?php echo $d->vcGLNameX; ?></td>
				  <td></td>
				  <td><?php echo "IDR ".number_format($d->intValue,'0'); ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			  <div class="form-group">
                  <label>Remarks</label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..." readonly="true" data-toggle="tooltip" data-placement="top" title="Remarks"><?php echo $headerx->vcRemarks;?>
				  </textarea>
                </div>
			</div>
			<?php } ?>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	  
	reloadcategory();
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
 
 
</script>
</body>
</html>
