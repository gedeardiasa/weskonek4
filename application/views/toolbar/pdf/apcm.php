<style>

body {
    font-family: <?php echo $docsetting->vcFontFamily;?>;
}
</style>
<!--<body onload="window.print()">-->
<body>
<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm; height:<?php echo $docsetting->intHeight;?>cm">
<table align="left" style="width:100%">
<tr>
	<td valign="top" align="left" style="width:20%">
		<?php if($profile->blpImage!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $profile->blpImage ).'" width="210cm" height="110cm" />';}else{ ?>
        <img src="<?php echo base_url(); ?>data/general/dist/img/box.png" width="25px" height="25px" alt="User profile picture">
		<?php } ?>
	</td>
	<td valign="top" align="left" style="width:30%">
	<h3 style="margin-bottom:0px"><?php echo $company;?></h3>
	<?php echo $profile->vcAddress;?><br>
	<?php echo $profile->vcCity;?>, <?php echo $profile->vcState;?>, <?php echo $profile->vcCountry;?><br>
	Telp <?php echo $profile->vcTelp;?>, Fax <?php echo $profile->vcFax;?>
	</td>
	<td style="width:2%"></td>
	<td valign="top" align="left" style="width:9%">
	<b>Vendor</b><br>
	<b>Alamat</b>
	</td>
	<td valign="top" align="left" style="width:39%">
	: <?php echo $header->vcBPName;?><br>
	: <?php echo $header->vcAddress;?>,<?php echo $header->vcCity;?>
	
	</td>	
</tr>
</table>


<h1 align="left">NOTA RETUR</h1>

<table cellpadding="0px" cellspacing="0px" align="left" style="width:100%; padding-left:4px">
	<tr>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Nama Barang</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Jumlah</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Satuan</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Harga</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Diskon</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Sub Total</th>
	  
	  <!--<th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Nama Barang</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Jumlah</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Satuan</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Harga</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Diskon</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Sub Total</th>-->
	</tr>
	
	
	<?php 
	$no=1;
	foreach($detail->result() as $dd){?>
	<tr>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $no;?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dd->vcItemName;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo number_format($dd->intQty,'2');?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dd->vcUoM;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo number_format($dd->intPrice,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo number_format($dd->intDisc,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-right:solid; border-width: 1px"><?php echo number_format($dd->intLineTotal,'0');?></td>
	  
	  
	  <!--<td align="middle" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $no;?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $dd->vcItemName;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intQty,'2');?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $dd->vcUoM;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intPrice,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intDisc,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intLineTotal,'0');?></td>-->
	</tr>
	<?php $no++;}?>
	<tr>
	  <td valign="top "colspan="5"  rowspan="5" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px">
	  Keterangan :<br>
	  <?php echo $header->vcRemarks;?>
	  </td>
	  <td cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px">Sub Total</td>
	  <td align="right" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px"><?php echo number_format($header->intDocTotalBefore,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px">Disc</td>
	  <td align="right" cellpadding="0px" cellspacing="0px"><?php echo number_format($header->intDisc,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px">Freight</td>
	  <td align="right" cellpadding="0px" cellspacing="0px"><?php echo number_format($header->intFreight,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px">Tax</td>
	  <td align="right" cellpadding="0px" cellspacing="0px"><?php echo number_format($header->intTax,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px">Total</td>
	  <td align="right" cellpadding="0px" cellspacing="0px"><?php echo number_format($header->intDocTotal,'0');?></td>
	</tr>
</table>
</div>
</body>



