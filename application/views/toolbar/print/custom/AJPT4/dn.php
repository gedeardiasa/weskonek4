<?php
if(isset($_GET['excel'])){
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=DN.xls");
}
if(isset($_GET['word'])){
	header("Content-type: application/vnd-ms-word");
	header("Content-Disposition: attachment; filename=DN.doc");
}

$db=$this->load->database('default', TRUE);
$docnum= $header->vcDocNum;
$qsopir=$db->query("
SELECT 
c.vcValue
FROM mudf a
LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
LEFT JOIN dudf c ON c.`intUdf`=a.`intID`
WHERE b.`vcCode`='DN' AND a.`vcCode`='sopir' AND c.`vcDocNum`='$docnum'
");
$rsopir=$qsopir->row();
$sopir=$rsopir->vcValue;

$qnopol=$db->query("
SELECT 
c.vcValue
FROM mudf a
LEFT JOIN mdoc b ON a.`intDoc`=b.`intID`
LEFT JOIN dudf c ON c.`intUdf`=a.`intID`
WHERE b.`vcCode`='DN' AND a.`vcCode`='nopol' AND c.`vcDocNum`='$docnum'
");
$rnopol=$qnopol->row();
$nopol=$rnopol->vcValue;
?>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin-top :0;
	margin-right :15mm;
	margin-bottom:0;
	margin-left:0;
}
</style>
<style>

body {
    font-family: <?php echo $docsetting->vcFontFamily;?>;
}
</style>
<!--<body onload="window.print()">-->
<body>
<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm; height:<?php echo $docsetting->intHeight;?>cm">
<table align="left" style="width:100%">
<tr>
	<td valign="top" align="left" style="width:20%">
		<?php if($profile->blpImage!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $profile->blpImage ).'" width="210cm" height="110cm" />';}else{ ?>
        <img src="<?php echo base_url(); ?>data/general/dist/img/box.png" width="25px" height="25px" alt="User profile picture">
		<?php } ?>
	</td>
	<td valign="top" align="left" style="width:30%">
	<h3 style="margin-bottom:0px"><?php echo $company;?></h3>
	<?php echo $profile->vcAddress;?><br>
	<?php echo $profile->vcCity;?>, <?php echo $profile->vcState;?>, <?php echo $profile->vcCountry;?><br>
	Telp <?php echo $profile->vcTelp;?>, Fax <?php echo $profile->vcFax;?>
	</td>
	<td style="width:2%"></td>
	<td valign="top" align="left" style="width:9%">
	<b>Customer</b><br>
	<b>Address</b>
	</td>
	<td valign="top" align="left" style="width:39%">
	: <?php echo $header->vcBPName;?><br>
	: <?php echo $header->vcAddress;?>,<?php echo $header->vcCity;?><br>
	
	</td>	
</tr>
</table>


<h2 align="left">Delivery Note <?php echo $header->vcDocNum;?></h2>
<table style="width:100%">
<tr>
<td align="left">Nopol : <?php echo $nopol;?>, Sopir : <?php echo $sopir;?></td>
<td align="right">Tanggal : <?php echo date('d F Y',strtotime($header->dtDate));?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
</table>
<table cellpadding="0px" cellspacing="0px" align="left" style="width:100%; padding-left:4px">
	<tr>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Item Name</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Qty</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">UoM</th>
	  
	  <!--<th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Nama Barang</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Jumlah</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Satuan</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Harga</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Diskon</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Sub Total</th>-->
	</tr>
	
	
	<?php 
	$no=1;
	foreach($detail->result() as $dd){?>
	<tr>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $no;?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dd->vcItemName;?></td>
	  <td align="center" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo number_format($dd->intQty,'0');?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-left:solid; border-right:solid; border-width: 1px"><?php echo $dd->vcUoM;?></td>
	  
	  
	  <!--<td align="middle" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $no;?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $dd->vcItemName;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intQty,'2');?></td>
	  <td cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo $dd->vcUoM;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intPrice,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intDisc,'0');?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-style:solid; border-width: 1px"><?php echo number_format($dd->intLineTotal,'0');?></td>-->
	</tr>
	<?php $no++;}?>
	<tr>
	  <td valign="top "colspan="5"  rowspan="5" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px">
	  Remarks :<br>
	  <?php echo $header->vcRemarks;?>
	  </td>
	  
	</tr>
</table>
<table style="width:100%">
<tr>
<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penerima</td>
<td align="right">Pengirim&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
<td style="height:100px">---------------------</td>
<td align="right" style="height:100px">---------------------</td>
</tr>
</table>
</div>
</body>



