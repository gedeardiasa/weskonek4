<?php
if(isset($_GET['excel'])){
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=AR.xls");
}
if(isset($_GET['word'])){
	header("Content-type: application/vnd-ms-word");
	header("Content-Disposition: attachment; filename=AR.doc");
}
?>

<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
	margin-top :5mm;
	margin-right :0;
	margin-bottom:0;
	margin-left:0;
}

body {
    font-family: <?php echo $docsetting->vcFontFamily;?>;
}
</style>
<!--<body onload="window.print()">-->
<body style="font-family:arial;font-size:11px">
<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm; height:<?php echo $docsetting->intHeight;?>cm">
<table align="left" style="width:100%">
<tr>
	<td valign="top" align="left" style="width:20%">
		<?php if($profile->blpImage!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $profile->blpImage ).'" width="110cm" height="80cm" />';}else{ ?>
        <img src="<?php echo base_url(); ?>data/general/dist/img/box.png" width="25px" height="25px" alt="User profile picture">
		<?php } ?>
	</td>
	<td valign="top" align="left" style="width:45%">
	<center><b><h5 style="margin-bottom:0px;color:#ff4b4b"><?php echo $company;?><br>
	<span style="margin-bottom:0px;color:#4267ac">INTERNATIONAL FREIGHT FORWARDING<br>
JASA PENGURUSAN TRANSPORTASI</span>
</h4></b>
	</center>
	<div style="margin-bottom:0px;color:#4267ac; font-size:12px">
	<?php echo $profile->vcAddress;?><br>
	</div>
	<div style="margin-bottom:0px;color:#4267ac; font-size:12px">
	EMAIL :kjas.indonesia@gmail.com
	</div>
	</td>
	<td style="width:7%;"></td>
	<td valign="top" align="left" style="width:28%;font-size:12px">
	Kepada Yth<br>
	<?php echo $header->vcBPName;?><br>
	Ongkos Angkut
	<?php
	$db=$this->load->database('default', TRUE);
		$tujuanquery=$this->db->query("
		
		select a.* from dudf a
		left join mudf b on a.intUdf=b.intID
		where a.vcDocNum=$header->vcDocNum
		");
		echo $tujuanquery->row()->vcValue;
	?>
	</td>
</tr>
</table>


<table style="width:100%">
<tr>
<td align="left"></td>
<td align="right"></td>
</tr>
</table>
<h4 align="left" style="margin-left:10px">No. Invoice : <?php echo $header->vcDocNumCustom;?></h4>
<table cellpadding="0px" cellspacing="0px" align="left" style="width:100%; padding-left:4px; font-size:12px">
	<tr>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Tanggal</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Truck</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-left:solid; border-top:solid; border-bottom:solid; border-width: 1px">Keterangan</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Jumlah</th>
	  
	  <!--<th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">No</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Nama Barang</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Jumlah</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Satuan</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Harga</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Diskon</th>
	  <th cellpadding="0px" cellspacing="0px" style="border-style:solid; border-width: 1px">Sub Total</th>-->
	</tr>
	
	
	<?php 
	$no=1;
	foreach($detail->result() as $dd){
	$dtgl=$this->db->query("
		
		select
		a.*
		from dudf2 a
		left join mudf2 b on a.intUdf=b.intID
		where b.vcCode='del_date' and a.vcDocNum=$header->vcDocNum and a.intHID=$dd->intID
		");
		
	$dnopol=$this->db->query("
		
		select
		a.*
		from dudf2 a
		left join mudf2 b on a.intUdf=b.intID
		where b.vcCode='nopol' and a.vcDocNum=$header->vcDocNum and a.intHID=$dd->intID
		");
		
	$dremarks=$this->db->query("
		
		select
		a.*
		from dudf2 a
		left join mudf2 b on a.intUdf=b.intID
		where b.vcCode='remarks' and a.vcDocNum=$header->vcDocNum and a.intHID=$dd->intID
		");
	?>
	<tr>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $no;?></td>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dtgl->row()->vcValue;?></td>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dnopol->row()->vcValue;?></td>
	   <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dremarks->row()->vcValue;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-right:solid; border-width: 1px"><?php echo number_format($dd->intLineTotal,'0');?></td>
	 
	</tr>
	<?php $no++;}
	
	$dDD=$this->db->query("select
	a.*
	from dAR2 a
	left join hAR b on a.intHID=b.intID
	where b.intID=$header->intID
		");
		
	foreach($dDD->result() as $dd){
	?>
	<tr>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $no;?></td>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"></td>
	  <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"></td>
	   <td align="middle" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-width: 1px"><?php echo $dd->vcItemName;?></td>
	  <td align="right" cellpadding="2px" cellspacing="2px" style="border-left:solid; border-right:solid; border-width: 1px"><?php echo number_format($dd->intValue,'0');?></td>
	 
	</tr>
	
	<?php $no++;} ?>
	<tr>
	  <td valign="top "colspan="3"  rowspan="5" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px">
	 
	  </td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px" style="border-top: solid; border-left:solid; border-width:1px;">Tax</td>
	  <td align="right" cellpadding="0px" cellspacing="0px" style="border-top: solid; border-left:solid;border-right:solid; border-width:1px; "><?php echo number_format($header->intTax,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px" style="border-top: solid; border-left:solid; border-width:1px;">Down Payment</td>
	  <td align="right" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-left:solid;border-right:solid;border-width: 1px"><?php echo number_format($header->intApplied,'0');?></td>
	</tr>
	<tr>
	  <td cellpadding="0px" cellspacing="0px"  style="border-top:solid; border-width: 1px">Total</td>
	  <td align="right" cellpadding="0px" cellspacing="0px" style="border-top:solid; border-width: 1px"><?php echo number_format($header->intBalance,'0');?></td>
	</tr>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br>
<table>
	<tr>
		<td style="width:35%;border:solid;border-width:1px;font-size:12px">
		Pembayaran harap di transfer ke:<br>
<ul style="margin:18px;padding:0px">
<li>No.0149789391 (Bank BNI KC Tanjung Perak Surabaya) atas nama PT.Keluarga Jaya Abadi Sentosa</li>
<li>No.513 3233333 (Bank BCA KCP Perak Timur) atas nama Keluarga Jaya Abdi Sento</li>
</ul>
		</td>
		<td style="width:35%">
		</td>
		<td style="width:30%" align="middle">
		Surabaya, <?php echo date('d F Y',strtotime($header->dtDate));?><br><br><br><br>
		
		Mochammad Afifuddin<br>
Direktur

		</td>
	</tr>

</table>
</div>
</body>



