<!DOCTYPE html>
<html>
<head>
</head>
<link rel="stylesheet" href="<?php echo base_url(); ?>data/general/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<?php $this->load->view('body'); ?>


<script>
function printDiv(divName) {
	var printContents = document.getElementById(divName).innerHTML;
			 var originalContents = document.body.innerHTML;

			 document.body.innerHTML = printContents;

			 window.print();

			 document.body.innerHTML = originalContents;
		}

</script>
<script language="VBScript">
// THIS VB SCRIP REMOVES THE PRINT DIALOG BOX AND PRINTS TO YOUR DEFAULT PRINTER
Sub window_onunload()
On Error Resume Next
Set WB = nothing
On Error Goto 0
End Sub

Sub Print()
OLECMDID_PRINT = 6
OLECMDEXECOPT_DONTPROMPTUSER = 2
OLECMDEXECOPT_PROMPTUSER = 1


On Error Resume Next

If DA Then
call WB.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,1)

Else
call WB.IOleCommandTarget.Exec(OLECMDID_PRINT ,OLECMDEXECOPT_DONTPROMPTUSER,"","","")

End If

If Err.Number <> 0 Then
If DA Then 
Alert("Nothing Printed :" & err.number & " : " & err.description)
Else
HandleError()
End if
End If
On Error Goto 0
End Sub

If DA Then
wbvers="8856F961-340A-11D0-A96B-00C04FD705A2"
Else
wbvers="EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B"
End If

document.write "<object ID=""WB"" WIDTH=0 HEIGHT=0 CLASSID=""CLSID:"
document.write wbvers & """> </object>"
</script>
<body onload="printDiv('printableArea')">
<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm">
	<!--<i class="fa fa-print fa-2x" aria-hidden="true" onclick="printDiv('printableArea')" data-toggle="tooltip" data-placement="top" title="Print"></i>&nbsp;&nbsp;
	<i class="fa fa-envelope-o fa-2x" aria-hidden="true" onclick="toolbar_mail()" data-toggle="tooltip" data-placement="top" title="Send Mail"></i>&nbsp;&nbsp;
	<i class="fa fa-file-pdf-o fa-2x" aria-hidden="true" onclick="toolbar_pdf()" data-toggle="tooltip" data-placement="top" title="PDF"></i>&nbsp;&nbsp;-->
	<img id="blah" src="data:image/jpeg;base64,<?php echo base64_encode($profile->blpImage);?>" 
	style="width:100px;height:100px"
	class="profile-user-img img-responsive img-circle" alt="User profile picture">
<br>
	<div id="printableArea" align="center" style="width:<?php echo $docsetting->intWidth;?>cm;font-family:time news roman">
	<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm;font-family:time news roman">
	<!--<?php echo $profileH->vcName;?><br>
	Cabang <?php echo $profile->vcName;?><br>
	<?php echo $profile->vcAddress;?><br>
	<?php echo $profile->vcCity;?>, <?php echo $profile->vcState;?>, <?php echo $profile->vcCountry;?>-->
	
		
		<div style="width:<?php echo $docsetting->intWidth;?>cm;font-family:time news roman;font-size:12px;border-top: 1px solid black;">Invoice:<?php echo $headerAR->vcDocNum;?>,&nbsp;&nbsp; Chasier:<?php echo $cashier->vcName;?><br>Time:<?php echo $headerAR->dtInsertTime;?>
		</div>
		<table align="center" style="width:<?php echo $docsetting->intWidth;?>cm;font-family:time news roman;
	font-size:12px;">
		<?php foreach ($detailAR->result() as $dAR){?>
		  <tr>
			<td valign="top"><?php echo $dAR->vcItemName;?> <?php if($dAR->intDisc>0){echo "<br>Disc. ".number_format($dAR->intDisc,'0');}?></td>
			<td valign="top" align="right"><?php echo number_format($dAR->intQty,'2');?></td>
			<td valign="top" align="right"><?php echo number_format($dAR->intPrice,'0');?></td>
			<td valign="top" align="right"><?php echo number_format($dAR->intLineTotal,'0');?></td>
		  </tr>
		<?php } ?>
		  <tr style="border-top: 1px solid black;">
			<td colspan="3">Total</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intDocTotalBefore,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="2">Disc</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intDiscPer,'2');?> %</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intDisc,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="3">Total (After Disc.)</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intDocTotalBefore-$headerAR->intDisc,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="2">Tax</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intTaxPer,'2');?> %</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intTax,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="3">Freight</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intFreight,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="3">Grand Total</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intDocTotal,'0');?></td>
		  </tr>
		  <tr style="border-top: 1px solid black;">
			<td colspan="3">Cash</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intAmmountTendered,'0');?></td>
		  </tr>
		  <tr>
			<td colspan="3">Change</td>
			<td valign="top" align="right"><?php echo number_format($headerAR->intChange,'0');?></td>
		  </tr>
		</table>
		<br>
		<?php echo $profile->vcAddress;?>,
		<?php echo $profile->vcCity;?> 082229189987
		<br>
	Thank You
	</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>data/general/plugins/jQuery/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>asset/jquery/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>data/general/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>