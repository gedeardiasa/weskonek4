<?php
if(isset($_GET['excel'])){
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=PNL.xls");
}
if(isset($_GET['word'])){
	header("Content-type: application/vnd-ms-word");
	header("Content-Disposition: attachment; filename=PNL.doc");
}
?>

<style>

body {
    font-family: <?php echo $docsetting->vcFontFamily;?>;
}
</style>
<body onload="window.print()">
<!--<body>-->
<div align="center" style="width:<?php echo $docsetting->intWidth;?>cm; height:<?php echo $docsetting->intHeight;?>cm">

					<table  class="table table-striped dt-responsive jambo_table tree">
						<thead>
						<tr id="trtableaccessgeneral">
						  <th>Account</th>
						  <th>Balance</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$profit=0;
						$val=1;
						foreach($list1->result() as $d) 
						{
							
							if($d->GL==6)
							{?>
							<tr>
							<td><b>Gross Profit</b></td>
							<td align="right" style=" border-top: 1px solid;"><b><?php echo number_format($profit,'0'); ?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<?php	
							}
							if($d->GL==4)
							{
								$profit= $d->Val;
							}
							else
							{
								$profit = $profit + $d->Val;
							}
						?>
						<tr class="treegrid-<?php echo $d->GL; ?>">
						  <td><?php echo $d->GL; ?> - <?php echo $d->vcName; ?></td>
						  <td align="right"><?php echo number_format($d->Val,'0'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
							<?php
							foreach($list2->result() as $d2) 
							{
								if($d->GL==$d2->GL)
								{
							?>
								<tr class="treegrid-<?php echo $d2->GL2; ?> treegrid-parent-<?php echo $d->GL; ?>">
									<td><?php echo $d2->GL2; ?> - <?php echo $d2->vcName; ?></td>
									<td align="right"><?php echo number_format($d2->Val,'0'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								
								
								<?php
								foreach($list3->result() as $d3) 
								{
									if($d2->GL2==$d3->GL2)
									{
								?>
									<tr class="treegrid-<?php echo $d3->GL4; ?> treegrid-parent-<?php echo $d2->GL2; ?>">
										<td><?php echo $d3->GL4; ?> - <?php echo $d3->vcName; ?></td>
										<td align="right"><?php echo number_format($d3->Val,'0'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									</tr>
									
									<?php
									foreach($list4->result() as $d4) 
									{
										if($d3->GL4==$d4->GL4)
										{
									?>
										<tr class="treegrid-<?php echo $d4->GL6; ?> treegrid-parent-<?php echo $d3->GL4; ?>">
											<td><?php echo $d4->GL6; ?> - <?php echo $d4->vcName; ?></td>
											<td align="right"><?php echo number_format($d4->Val,'0'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										</tr>
										<?php
										foreach($list5->result() as $d5) 
										{
											if($d4->GL6==$d5->GL6)
											{
										?>
											<tr class="treegrid-<?php echo $d5->GL8; ?> treegrid-parent-<?php echo $d4->GL6; ?>">
												<td>
												
												<?php echo $d5->GL8; ?> - <?php echo $d5->vcName; ?></td>
												<td align="right"><?php echo number_format($d5->Val,'0'); ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
											</tr>
										<?php
											}
										}
										?>
										
										
									<?php
										}
									}
									?>
									
									
									
									
									
								<?php
									}
								}
								?>
								
							<?php
								}
							}
							?>
						<?php 
						$val++;
						}
						?>
						</tbody>
						<tfoot>
						<tr>
						  <td><b>Profit</b></td>
						  <td align="right" style="border-top: 1px solid;"><b><?php echo number_format($profit,'0'); ?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						</tfoot>
					</table>
</div>
</body>



