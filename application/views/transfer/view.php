<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>
.modal-dialog {
  width: 90%;
 
}

</style>
<div class="wrapper">   
   
  <div class="modal fade" id="modal-add-edit">
  <div id="printme">
  </div>
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border" id="tittlemodaladd">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Add New Transfer</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			<div class="box-header with-border" id="tittlemodaledit">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Detail Transfer</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			  
              <div class="box-body">
				<?php $this->load->view('loadingbar2'); ?>
				<div class="row">
					<div class="alert alert-success alert-dismissible" id="successalert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-check"></i><span id="successalertttext">Success. You successfully process the data</span>
					</div>
					<div class="alert alert-danger  alert-dismissible" id="dangeralert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-ban"></i>Error. You failed to process the data
					</div>
					<div class="col-sm-6">
					
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" readonly="true" data-toggle="tooltip" data-placement="top" title="Document Number">
							<input type="hidden" id="idHeader" name="idHeader">
							<input type="hidden" id="IdToolbar" name="IdToolbar">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" placeholder="Doc. Date" data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" data-toggle="tooltip" data-placement="top" title="Reference Number">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="FromWhs" class="col-sm-4 control-label" style="height:20px">From Warehouse</label>
							<div class="col-sm-8" style="height:45px">
							<select id="FromWhs" name="FromWhs"class="form-control" data-toggle="tooltip" data-placement="top" title="From Warehouse">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
						   </select>
						   </div>
						</div>
						<div class="form-group">
						  <label for="ToWhs" class="col-sm-4 control-label" style="height:20px">To Warehouse</label>
							<div class="col-sm-8" style="height:45px">
							<select id="ToWhs" name="ToWhs" class="form-control" data-toggle="tooltip" data-placement="top" title="To Warehouse">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
						   </select>
						   </div>
						</div>
					</div>
				</div>
				<hr>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#detaildata" data-toggle="tab">Detail</a></li>
					  <li><a href="#adtdata" data-toggle="tab">Additional Data</a></li>
					</ul>
				<div class="tab-content">
				  <div class="active tab-pane" id="detaildata">
					<div class="" id="form-adddetail">
					<b><i>Form Detail</i></b><br>
						<div class="col-sm-5" style="padding-left:0px;padding-bottom:10px">
							<div class="input-group">
							  <div class="input-group-addon" data-toggle="modal" data-target="#modal-item">
								<i class="fa fa-cubes" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control inputenter" id="detailItem" name="detailItem" maxlength="100" required="true" 
							  onchange="isiitem()" onpaste="isiitem()" placeholder="Select Item Name" data-toggle="tooltip" data-placement="top" title="Item Name">
							</div>
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" class="form-control inputenter" id="detailItemCode" name="detailItemCode" maxlength="50" required="true" 
							  onchange="isiitemcode()" onpaste="isiitemcode()" placeholder="Select Code" data-toggle="tooltip" data-placement="top" title="Item Code">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="detailQty" name="detailQty" maxlength="15" required="true" 
							  data-toggle="tooltip" data-placement="top" title="Quantity" placeholder="Qty">
						</div>
						<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" step="any" class="form-control inputenter" id="detailUoM" readonly="true" name="detailUoM" maxlength="15" required="true" 
							  data-toggle="tooltip" data-placement="top" title="Unit of Measure" placeholder="UoM">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<button type="button" class="btn btn-primary" id="buttonadddetail" onclick="addDetail()">Add</button>
							<button type="button" class="btn btn-primary" id="buttoneditdetail" onclick="addDetail()">Update</button>
							<button type="button" class="btn btn-default" id="buttoncanceldetail" onclick="cancelDetail()">Cancel</button>
						</div>
					</div>
					<div id="mix_detail"></div>
				  </div>
				  <div class="tab-pane" id="adtdata">
					<div class="row">
						<div class="col-md-6">
						  <div id="mix_adtdata"></div>
						</div>
						<div class="col-md-6">
						
						</div>
					 </div>
				  </div>
				</div>
				</div>

				
				<div class="form-group">
                  <label>Remarks</label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..." data-toggle="tooltip" data-placement="top" title="Remarks"></textarea>
                </div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel" onclick="closeModal()">Cancel</button>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  id="buttonsave">Save changes</button>
         </div>
       </div>
     </div>
   </div>
   <div class="modal fade" id="modal-batch-double">
     <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodalbatchdouble">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Set Batch</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <br><br>
			</div>
				<div class="box-body">
				<br><br>
				<div class="alert alert-danger alert-dismissible" id="alertbatchmin">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i><span id="alertbatchmintext">Make sure the batch quantity is the same as the item quantity!!</span>
				</div>


				<div class="alert alert-success alert-dismissible" id="okbatchmin">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i><span id="okbatchmintext">OK. Now quantity is same</span>
				</div>

				Total qty = <input type="text" style="background-color:yellow" id="totalbatchmin" name ="totalbatchmin" readonly="true"><br><br>
					<div class="row">
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<input type="hidden" id="ItemBatchIn" name="ItemBatchIn">
							<input type="hidden" id="QtyBatchIn" name="QtyBatchIn">
							<div class="input-group">
							  <div class="input-group-addon">
								<i class="fa fa-cubes" aria-hidden="true" onclick="showbatchlistfrom()"></i>
							  </div>
							  <input type="text" class="form-control inputenter" id="Batchfrom" name="Batchfrom" maxlength="50" required="true" 
							   placeholder="Select Batch Number From" data-toggle="tooltip" data-placement="top" title="Batch Number From">
							</div>
							
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<div class="input-group">
							  <div class="input-group-addon">
								<i class="fa fa-cubes" aria-hidden="true" onclick="showbatchlistto()"></i>
							  </div>
							  <input type="text" class="form-control inputenter" id="Batchto" name="Batchto" maxlength="50" required="true" 
							  placeholder="Select Batch Number To" data-toggle="tooltip" data-placement="top" title="Batch Number To">
							</div>
							
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<input type="number" step="any" class="form-control inputenter" id="BatchQty" name="BatchQty" maxlength="15" required="true" 
							data-toggle="tooltip" data-placement="top" title="Quantity" placeholder="Qty">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<button type="button" class="btn btn-primary" id="buttonaddbatchdouble" onclick="addbatchdouble()"><?php echo $this->lang->line("Add");?></button>
						</div>
					</div>
					<hr>
					<table id="mix_detailbatchmin" class="table table-striped dt-responsive jambo_table hover" width="100%"></table>
				</div>
            </div>
		</div>
		</div>
		 <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Back</button>
		 </div>
       </div>
	     
     </div>
  </div>
   <?php $this->load->view('batchjs'); ?>
   	<script>
	function insertBatch(docnum,dest){
		if(dest=='from'){
			$("#Batchfrom").val(docnum);
		}else{
			$("#Batchto").val(docnum);
		}
		$('#modal-batch').modal('hide');
	}
	function showbatchlistfrom(){
		$('#modal-batch').modal('show');
		var item = $("#ItemBatchIn").val();
		var loc  = $("#FromWhs").val();
		$('#selectbatchlist').load('<?php echo $this->config->base_url()?>index.php/general_batch/showmodallist?item='+item+'&location='+loc+'&dest=from');
	}
	function showbatchlistto(){
		$('#modal-batch').modal('show');
		var item = $("#ItemBatchIn").val();
		var loc  = $("#ToWhs").val();
		$('#selectbatchlist').load('<?php echo $this->config->base_url()?>index.php/general_batch/showmodallist?item='+item+'&location='+loc+'&dest=to');
	}
	function addbatchdouble(){
		var Batchfrom = $("#Batchfrom").val();
		var Batchto = $("#Batchto").val();
		var BatchQty = $("#BatchQty").val();

		var ItemBatchIn = $("#ItemBatchIn").val();

		var FromWhs = $("#FromWhs").val();
		var ToWHs = $("#ToWHs").val();

		var batchfromada		= 0;
	   	$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/cekbatch?", 
			data: "Whs="+FromWhs+"&BatchIn="+Batchfrom+"&ItemBatchIn="+ItemBatchIn+"", 
			cache: true, 
			success: function(data){
				batchfromada = data;
			},
			async: false
		});

		var batchtoada		= 0;
	   	$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/cekbatch?", 
			data: "Whs="+ToWhs+"&BatchIn="+Batchto+"&ItemBatchIn="+ItemBatchIn+"", 
			cache: true, 
			success: function(data){
				batchtoada = data;
			},
			async: false
		});
		var isnewbatchnumber = 0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/isnewbatchnumber?", 
			data: "BatchIn="+Batchto+"", 
			cache: true, 
			success: function(data){
				isnewbatchnumber = data;
			},
			async: false
		});
		var batchstock		= 0;
	   	$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/getbatchstockbydocnumlocation?", 
			data: "Batch="+Batchfrom+"&Whs="+FromWhs+"", 
			cache: true, 
			success: function(data){
				batchstock = data;
			},
			async: false
		});
		if(parseFloat(batchstock)<parseFloat(BatchQty)) // jika stok batch kurang maka alert error
		{
			alert('Stok Tidak Cukup');	
			return;
		}else if(Batchfrom=='')//jika batch from ksong
		{
			alert('Batch From cannot empty');	
			return;
		}
		else if(batchfromada==0)//jika batch from tidak ada dalam database
		{
			alert('Wrong Batch From');	
			return;
		}
		else if(batchtoada==0 && Batchto!='' && isnewbatchnumber==0 && Batchfrom!=Batchto)//jika batch to tidak ada dalam database
		{
			alert('Wrong Batch To');	
			return;
		}
		else if(BatchQty=='')// jika qty di isi kosong
		{
			alert('Qty cannot empty');	
			return;
		}
		batch.push([ItemBatchIn, Batchfrom, Batchto, BatchQty]);

		$("#Batchfrom").val('');
		$("#Batchto").val('');
		$("#BatchQty").val('');
		reloadbatchdetail(ItemBatchIn,document.getElementById("totalbatchmin").value);
	}
	function delDetailBatchIn(item,from,to,qty)
   	{
	   var totalbatchmin 	= $("#totalbatchmin").val();
	   for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][0]==item && batch[i][1]==from && batch[i][2]==to && batch[i][3]==qty)
			{
				var indexdel = i;
			}
		}
		batch.splice(indexdel, 1);
		reloadbatchdetail(item,totalbatchmin);
   	}
	function reloadbatchdetail(itembatchmin,totalbatchmin)
	{	
		var DocNum = $("#DocNum").val();
	   	var isloadfromheader = '';

		// cek total detail yg sdh di input
		var ttdt = 0;

		var oTable = $('#mix_detailbatchmin').dataTable();
		oTable.fnClearTable();
		for(var i = 0; i < batch.length; i++)
		{
			if(batch[i][0]==itembatchmin)
			{
				ttdt = parseFloat(ttdt) + parseFloat(batch[i][3]);

				if(batch[i][2]==''){
					BatchTT = 'New Batch Number';
				}else{
					BatchTT = batch[i][2];
				}
				$('#mix_detailbatchmin').dataTable().fnAddData( [
					batch[i][1],
					BatchTT,
					batch[i][3],
					'<div id="controldetail"><a onclick="delDetailBatchIn(\''+batch[i][0]+'\',\''+batch[i][1]+'\',\''+batch[i][2]+'\',\''+batch[i][3]+'\')"><i class="fa fa-trash" aria-hidden="true"></i></a></div>']
				);
			}
		}
		if(parseFloat(ttdt)!=parseFloat(totalbatchmin) && isloadfromheader=='')
		{
			$("#alertbatchmin").show();
			$("#okbatchmin").hide();
			$("#buttonsavebatchout").hide();
		}
		else
		{
			$("#alertbatchmin").hide();
			$("#okbatchmin").show();
			if(isloadfromheader!='')
			{
				$("#okbatchmin").hide();
				$("#buttonsavebatchout").hide();
				batch = [];
			}else
			{
				$("#okbatchmin").show();
				$("#buttonsavebatchout").show();
			}
		}
	}
	function changebatch(item,detailQty) // fungsi ini tidak bisa digeneralkan
   	{
		document.getElementById("ItemBatchIn").value 	= item;
		document.getElementById("QtyBatchIn").value 	= detailQty;
		document.getElementById("totalbatchmin").value 	= detailQty;
		$('#modal-batch-double').modal('show');
		$('#mix_detailbatchmin').DataTable( {
			data: dataSet,
			columns: [
				{ title: "Batch From" },
				{ title: "Batch To" },
				{ title: "Qty" },
				{ title: "Control" }
			],
			"columnDefs": [
			  { className: "text-right", "targets": [1,2,3,4]}
			]
		} );
		reloadbatchdetail(item,detailQty);
	}
	</script>
  <?php $this->load->view('selectbatch'); ?>
  <?php $this->load->view('selectitem'); ?>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<?php $this->load->view('loadingbar2'); ?>
				<div class="col-sm-2" style="padding-left:0px;margin-left:0px">
                  <button type="button" style="width:100%" class="btn btn-dark" data-toggle="modal" data-target="#modal-add-edit" onclick="initialadd()" 
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?> disabled>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
				</div>
				<div class="col-sm-7" style="padding-left:0px;margin-left:0px">
				&nbsp;
				</div>
				<div class="col-sm-3" style="padding-left:0px;margin-right:0px">
				<table align="center" style="width:100%">
					<tr>
						<td><input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff"></td>
						<td style="padding-left:5px" align="right"><button type="submit" class="btn btn-primary" onclick="loaddetailheader()">Reload</button>
						</td>
					</tr>
				</table>
				</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<div id="mix_header"></div>
				<div class="loader"></div>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php if($crudaccess->intExport==1){?>
  <script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
  <?php } ?>
  <?php $this->load->view('ctrludf'); ?>
</body>
<script>

  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode == 13) {
        addDetail();
    }
  });
  
  var xxx = document.getElementById("successalertttext").textContent;
  
  function closeModal()
  {
	  $('#modal-add-edit').modal('hide');
  }
  
  /*
  
	FUNGSI LOAD
  
  */
  function loadheader(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				loadudf(obj.DocNum);
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				document.getElementById("RefNum").value = obj.RefNum;
				if(id==0)
				{
					$("#FromWhs").val(<?php echo $defaultwhs;?>);
					$("#ToWhs").val(<?php echo $defaultwhs;?>);
				}
				else
				{
					document.getElementById("FromWhs").value = obj.FromWhs;
					document.getElementById("ToWhs").value = obj.ToWhs;
				}
				document.getElementById("Remarks").value = obj.Remarks;
				<?php
				if($backdate==0)
				{
				?>
				$('#DocDate').attr("disabled", true);
				<?php
				}
				?>
			}
		});
		
  }
  function loaddetailheader()
  {
	  $(".loadingbar2").show();
	  var daterange = $("#daterange").val();
	  daterange = daterange.split(' ').join('');
	  $('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'', function(){
		$(".loadingbar2").hide();
	  });   
  }
  function loaduom()
  {
	  var detailItem = $("#detailItem").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaduom?", 
			data: "detailItem="+detailItem+"", 
			cache: true, 
			success: function(data){ 
				document.getElementById("detailUoM").value = data;
			},
			async: false
		});
  }
  function isiitem() // fungsi saat ketik field item
  {
	   var detailItem = $("#detailItem").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemCode?", 
			data: "detailItem="+detailItem+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItemCode").value = data;
				loaduom();
			},
		});
	  
  }
  function isiitemcode() // fungsi saat ketik field item
  {
	  var detailItemCode = $("#detailItemCode").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemName?", 
			data: "detailItemCode="+detailItemCode+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItem").value = data;
				loaduom();
			},
	  });
	  
  }
  function initialadd()
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#ToWhs').attr("disabled", false);
	  $('#FromWhs').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DocNum').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				 loadheader(0,0);
				 //load html untuk menampilkan detail dari session
				 $('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
		
  }
  function initialedit(id)
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").hide();
	  $("#buttonsave").hide();
	  $("#tittlemodaladd").hide();
	  $("#tittlemodaledit").show();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $('#Remarks').attr("disabled", true);
	  $('#ToWhs').attr("disabled", true);
	  $('#FromWhs').attr("disabled", true);
	  $('#RefNum').attr("disabled", true);
	  $('#DocDate').attr("disabled", true);
	  $('#DocNum').attr("disabled", true);
	  
	  //load detail ambil dari database dan masukkan ke session
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				loadheader(id,0);
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');
			}
		});
	  
  }
  function initialduplicate(id)
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#ToWhs').attr("disabled", false);
	  $('#FromWhs').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DocNum').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				 loadheader(id,1);
				 //load html untuk menampilkan detail dari session
				 $('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
		
  }
  
  
  /*
  
	HEADER FUNCTION
  
  */
  function addHeader()
  {
		$(".loadertransaction").show();
		var DocNum = $("#DocNum").val();
		var DocDate = $("#DocDate").val();
		var RefNum = $("#RefNum").val();
		var FromWhs = $("#FromWhs").val();
		var ToWhs = $("#ToWhs").val();
		var Remarks = $("#Remarks").val();
		var detail=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		var minus=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekminusH?", 
			data: "FromWhs="+FromWhs+"&DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				minus=data;
			},
			async: false
		});
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});

		batchtype= 'out';
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_batch/cekavailablebatch?", 
			data: "batch="+JSON.stringify(batch)+"&batchtype="+batchtype+"&doc=IT", 
			cache: true, 
			success: function(data){ 
				isbatch=data;
			},
			async: false
		});

		if(DocNum=='')
		{
			alert('Please Fill Doc. Number');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(period!=1)
		{
			alert('This Period is locked');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(isbatch!=1)
		{
			alert('<?php echo $this->lang->line("Please Set Batch For Item With Batch Management and Make Sure The Batch Quantity is The Same as The Item Quantity");?>');
			$(".loadertransaction").hide();
		}
		else if(FromWhs==ToWhs)
		{
			alert('The warehouse of origin and the destination warehouse may not be the same');
			$("#FromWhs").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$(".loadertransaction").hide();
		}
		else if(minus!=1)
		{
			alert('Insufficient stock ('+minus+')');
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd", 
				data: "DocNum="+DocNum+"&DocDate="+DocDate+"&RefNum="+RefNum+"&FromWhs="+FromWhs+
				"&ToWhs="+ToWhs+"&Remarks="+Remarks+"&batch="+JSON.stringify(batch)+"",  
				cache: false, 
				success: function(msg){ 
					if(msg.length==9)
					{
						saveudf(msg);
						document.getElementById("successalertttext").innerHTML = xxx + " (" + msg + ")";
						initialadd();
						loaddetailheader();
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  
  
  /*
  
	DETAIL FUNCTION
  
  */
  function insertItem(item)
  {
	  document.getElementById("detailItem").value = item;
	  $('#modal-item').modal('hide');
	  $("#detailQty").focus();
	  isiitem();
  }
  function emptyformdetail()
  {
	document.getElementById("detailItem").value = "";
	document.getElementById("detailItemCode").value = "";
	document.getElementById("detailQty").value = "";
	document.getElementById("detailUoM").value = "";
	batch = [];
  }
  function EnableDisableItem(val)
  {
	  $('#detailItem').attr("disabled", val);
	  $('#detailItemCode').attr("disabled", val);
  }
  function addDetail()
  {
		$(".loadertransaction").show();
		EnableDisableItem(false);
		var FromWhs = $("#FromWhs").val();
		var ToWhs = $("#ToWhs").val();
	    var detailItem = $("#detailItem").val();
		var detailQty = $("#detailQty").val();
		var DocDate = $("#DocDate").val();
		var minus=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekminusD?", 
			data: "detailItem="+detailItem+"&detailQty="+detailQty+"&FromWhs="+FromWhs+"&DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				minus=data;
			},
			async: false
		});
		if(detailItem=='')
		{
			alert('Please Fill Item Name');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else if(FromWhs==ToWhs)
		{
			alert('Destination Whs Cannot be same');
			$("#ToWhs").focus();
			$(".loadertransaction").hide();
		}
		else if(detailQty=='' || detailQty==0)
		{
			alert('Please Fill Quantity');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		else if(minus==0)
		{
			alert('Insufficient stock');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/addDetail", 
				data: "detailItem="+detailItem+"&detailQty="+detailQty+"",  
				cache: false, 
				success: function(msg){ 
					if(msg!='false')
					{
						$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
						document.getElementById("detailItem").value = "";
						document.getElementById("detailItemCode").value = "";
						document.getElementById("detailQty").value = "";
						document.getElementById("detailUoM").value = "";
						$("#buttonadddetail").show();
						$("#buttoneditdetail").hide();
						$("#buttoncanceldetail").hide();
						document.getElementById("detailItem").focus();
						// batch
						var isbatch  = 0;
						$.ajax({ 
							type: "POST",
							url: "<?php echo base_url(); ?>index.php/general_batch/getitemisbatch?", 
							data: "detailItem="+detailItem+"", 
							cache: true, 
							success: function(data){ 
								isbatch = parseInt(data);
							},
							async: false
						});
						if(isbatch!=0)
						{
							changebatch(isbatch,detailQty);
						}
						//end batch
					}
					else
					{
						alert('Error');
					}
					$(".loadertransaction").hide();
				} 
			}); 
			cekDetail();
		}
  }
  function delDetail(code, qty)
  {
	EnableDisableItem(false);
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/delDetail", 
		data: "code="+code+"&qty="+qty+"",  
		cache: false, 
		success: function(msg){ 
			$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
			document.getElementById("detailItem").value = "";
			document.getElementById("detailItemCode").value = "";
			document.getElementById("detailQty").value = "";
			document.getElementById("detailUoM").value = "";
			$("#buttonadddetail").show();
			$("#buttoneditdetail").hide();
			$("#buttoncanceldetail").hide();
		} 
	}); 
	cekDetail();
  }
  function cekDetail()
  {
	  var detail=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		if(detail!=0)
		{
			$('#FromWhs').attr("disabled", true);
			$('#ToWhs').attr("disabled", true);
		}
		else
		{
			$('#FromWhs').attr("disabled", false);
			$('#ToWhs').attr("disabled", false);
		}
  }
  function editDetail(item, code, qty, uom)
  {
		document.getElementById("detailItem").value = item;
		document.getElementById("detailItemCode").value = code;
		document.getElementById("detailQty").value = qty;
		document.getElementById("detailUoM").value = uom;
		$("#buttonadddetail").hide();
		$("#buttoneditdetail").show();
		$("#buttoncanceldetail").show();
		$("#detailQty").focus();
		EnableDisableItem(true);
  }
  function cancelDetail()
  {
		document.getElementById("detailItem").value = "";
		document.getElementById("detailItemCode").value = "";
		document.getElementById("detailQty").value = "";
		document.getElementById("detailUoM").value = "";
		$("#buttonadddetail").show();
		$("#buttoneditdetail").hide();
		$("#buttoncanceldetail").hide();
		EnableDisableItem(false);
  }
  $(function () {
	 
	$(".loadertransaction").hide();
	$(document).bind('keydown', function(e) {// fungsi shortcut
		if($('#modal-add-edit').hasClass('in')==true)
		{
			  if(e.ctrlKey && (e.which == 83)) { // Ctrl + s
				e.preventDefault();
				if($('#buttonsave').is(":visible")==true)
				{					
					addHeader();
				}
				else if($('#buttonedit').is(":visible")==true)
				{					
					editHeader();
				}
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 39)) { // Ctrl + right
				e.preventDefault();
				toolbar_next();
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 37)) { // Ctrl + left
				e.preventDefault();
				toolbar_previous();
				return false;
			  }
			  else if(e.which == 27) { // Esc
				e.preventDefault();
				$('#modal-add-edit').modal('hide');
				return false;
			  }
		}
		else
		{
			if(e.which == 78) {
				//$('#modal-add-edit').modal('show');
				//initialadd();
			}
		}
	});
	
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
		   'This Year': [moment().startOf('year'), moment().endOf('year')],
		   'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
        }
    });
	document.getElementById("daterange").value = lastWeekT1+' - '+todayT1;
	//fungsi khusus multiple modal agar saat modal-2 di close modal-1 tetap bisa scroll
	$('.modal').on('hidden.bs.modal', function (e) {
		if($('.modal').hasClass('in')) {
		$('body').addClass('modal-open');
		}    
	});
	//end 
	
	
	$('#DocDate').datepicker({
      autoclose: true
    });
	
	//fungsi load ajax
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'');
	$('#mix_adtdata').load('<?php echo $this->config->base_url()?>index.php/general_udf/loadhtml?doc=IT');
	//end
	
	$('#detailItem').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		]
	});
	
	$('#detailItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
   
  });
  
 
</script>
</html>
