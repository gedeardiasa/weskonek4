<link rel="stylesheet" href="<?php echo base_url(); ?>asset/tree/css/jquery.treegrid.css">

<!-- jQuery 3.1.1 -->
<script src="<?php echo base_url(); ?>data/general/plugins/jQuery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/tree/js/jquery.treegrid.js"></script>
<script type="text/javascript">
$(document).ready(function() {
		$('.tree').treegrid();
});
</script>



<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>data/general/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>data/general/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>data/general/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>data/general/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>data/general/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>data/general/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>data/general/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>data/general/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>data/general/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>data/general/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>data/general/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>asset/respon/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-bs/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-fixedheader/js/dataTables.fixedHeader.js"></script>
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/buttons.dataTables.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/jquery.dataTables.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/select.dataTables.css" type="text/css" />


<script type="text/javascript" src="<?php echo base_url(); ?>asset/cdn/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/dataTables.buttons.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>