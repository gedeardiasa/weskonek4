	<div class="modal fade" id="modal-tutorial-bp">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">BP Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur BP (Business Partner). BP (Business Partner) adalah menu untuk mengelola partner usaha anda baik itu customer atapun suplier.
				</p>
				Untuk melakukan perubahan data BP (Business Partner) anda bisa masuk menu <b>Business Partner-Business Partner Master</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menubp.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat Business Partner baru.
				Isikan data-data yang ada.<br>
				berikut adalah rincian isian pada data master data :<br>
				<li>Code (Kode BP. Terisi otomatis dan tidak bisa dirubah)</li>
				<li>Type (Jenis BP. Isikan Both jika BP bisa menjadi customer ataupun suplier)</li>
				<li>Name (Nama BP)</li>
				<li>Category (Kategori BP)</li>
				<li>Price List (Ini merupakan settingan harga agar saat memilih BP partner harga terisi otomatis sesuai price list yang BP miliki)</li>
				<li>Federal Tax ID (NPWP / Nomer Pokok Wajib Pajak)</li>
				<li>Credit Limit (Batas pemberian hutang pada BP)</li>
				<li>Payment Term (Batas jatuh tempo nota/invoice pada BP)</li>
				<li>Tax Group (Settingan default apakah BP ini dikenakan pajak atau tidak)</li>
				<li>Bill To (Alamat tujuan pengiriman nota/invoice)</li>
				<li>Ship To (Alamat tujuan pengiriman barang)</li>
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				<p align="justify">
				Untuk merubah data BP (Business Partner) yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data BP (Business Partner) yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/bp";
	
}
</script>