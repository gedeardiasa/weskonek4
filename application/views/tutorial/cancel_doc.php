	<div class="modal fade" id="modal-tutorial-cancel_doc">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cancel Doc. Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang Cancel Doc. Beberapa dokumen yang sudah anda input dapat dilakukan cancel. Menu inilah yang digunakan untuk
				melakukan cancel pada dokumen yang sudah anda buat jika dirasa dokumen tersebut ada kesalahan inputan.
				Untuk melakukan cancel dokumen anda bisa masuk menu <b>Administration-Cancel Document</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menucancel.png"></center>
				Isikan Jenis dokumen dan juga nomer dokumennya. Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/execute.png">.
				Maka status dokumen tersebut akan berubah menjadi "Cancelled" (X)
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/cancel_doc";
	
}
</script>