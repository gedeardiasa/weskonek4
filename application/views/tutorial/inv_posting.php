	<div class="modal fade" id="modal-tutorial-inv_posting">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inv. Posting Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu Inv. Posting. Menu ini digunakan untuk merubah stok barang sesuai dengan fisik.
				Untuk melakukan perubahan stok anda bisa masuk menu <b>Inventory-Inv. Posting</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menupid.png"></center>
				Berikut adalah tahap-tahap untuk mengubah stok :
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"></li>
				<li>Isikan "Doc. Date" sesuai tanggal barang dicatat</li>
				<li>Field Ref. Number merupakan field untuk mengisikan nomer referensi perubahan stok (kosongi jika tidak ada)</li>
				<li>Isikan gudang barang yang akan di ubah stoknya pada field "Warehouse"</li>
				<li>Anda bisa mengetikkan nama barang yang akan di adjustment pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectitemname.png"></li>
				<li>Atau jika anda sudah hafal kode barang anda bisa mengetikkan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectcode.png"></li>
				<li>Untuk menampilkan semua barang yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalitem.png">, lalu klik pada item yang dikehendaki</li>
				<li>Stok yang terdapat pada sistem akan muncul pada field "Qty Before"</li>
				<li>Isikan stok yang sesuai dengan kondisi fisik di lapangan pada field "Qty"</li>
				<li>Setelah itu tekan "Enter" / klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/add.png"></li>
				<li>Item yang sudah anda pilih akan muncul di bawah</li>
				<li>Untuk mengubah data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/iconedit.png"></li>
				<li>Untuk menghapus data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/icondelete.png"></li>
				<li>Jika data dirasa sudah benar klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"></li>
				
				
				Anda juga bisa mengubah stok melalui referensi dari dokumen PID. Caranya adalah klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/copyfrom.png">
				pilih "Physical Inv. Document" lalu pilih nomer dokumen PID yang ada. Secara otomatis pencatatan stok pada dokumen PID akan tercopy otomatis pada dokumen Inventory Posting
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/inv_posting";
	
}
</script>