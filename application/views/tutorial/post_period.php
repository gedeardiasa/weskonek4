	<div class="modal fade" id="modal-tutorial-post_period">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Posting Period Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur posting period.
				Menu ini digunakan untuk mengatur agar pembuatan dokumen tidak salah masuk periode. Misal jika saat ini adalah bulan februari maka posting period yang di buka adalah
				bulan februari saja sedangkan untuk bulan lainnya di tutup, sehingga tidak ada kesalahan saat memilih tanggal dokumen masuk ke bulan yang di tutup.</p>
				Untuk melakukan perubahan posting period anda bisa masuk menu <b>Administrator-Posting Periods</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menupostingperiod.png"></center>
				<p align="justify">Jika ini adalah pertama kalinya anda menggunakan aplikasi ini ataupun memasuki tahun baru and perlu membuat periode terlebih dahulu<br>
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> Isikan nama periode yang anda inginkan (Umumnya nama periode adalah nama tahun)
				setelah itu pilih tahun periode dan klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				
				<p align="justify">
				Jika periode telah di buat anda perlu untuk membuka periode pada bulan saat ini. Misal saat ini adalah bulan Februari maka
				
				klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data bulan Februari, lalu klik tanda 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Pada kolom status pilih Unlocked untuk membuka periode / Locked untuk menutup periode. Setelah itu klik 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/post_period";
	
}
</script>