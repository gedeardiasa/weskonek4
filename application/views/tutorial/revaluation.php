	<div class="modal fade" id="modal-tutorial-revaluation">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Revaluation Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu Revaluation. Menu ini digunakan untuk merubah hpp (Harga Pokok Penjualan) barang.
				Untuk merubah hpp barang anda bisa masuk menu <b>Inventory-Revaluation</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menureval.png"></center>
				Berikut adalah tahap-tahap untuk merubah hpp barang :
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"></li>
				<li>Isikan "Doc. Date" sesuai tanggal saat ini</li>
				<li>Pilih gudang yang akan di ubah hppnya pada field "Warehouse" (HPP menempel pada item dan gudang. Tiap item bisa berbeda hpp nya pada gudang yang lainnya)</li>
				<li>Field Ref. Number merupakan field untuk mengisikan nomer referensi revaluation (kosongi jika tidak ada)</li>
				<li>Anda bisa mengetikkan nama barang yang akan di ubah hppnya pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectitemname.png"></li>
				<li>Atau jika anda sudah hafal kode barang anda bisa mengetikkan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectcode.png"></li>
				<li>Untuk menampilkan semua barang yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalitem.png">, lalu klik pada item yang dikehendaki</li>
				<li>Isikan HPP baru pada field "Item Cost"</li>
				<li>Setelah itu tekan "Enter" / klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/add.png"></li>
				<li>Item yang sudah anda pilih akan muncul di bawah</li>
				<li>Untuk mengubah data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/iconedit.png"></li>
				<li>Untuk menghapus data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/icondelete.png"></li>
				<li>Jika data dirasa sudah benar klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"></li>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/revaluation";
	
}
</script>