/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.1.73 : Database - pos_start
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pos_start` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pos_start`;

/*Table structure for table `dAP` */

DROP TABLE IF EXISTS `dAP`;

CREATE TABLE `dAP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dAP` (`intHID`),
  KEY `fk_intLocation_dAP` (`intLocation`),
  CONSTRAINT `fk_intHID_dAP` FOREIGN KEY (`intHID`) REFERENCES `hAP` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dAP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dAP` */

/*Table structure for table `dAPCM` */

DROP TABLE IF EXISTS `dAPCM`;

CREATE TABLE `dAPCM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dAPCM` (`intHID`),
  KEY `fk_intLocation_dAPCM` (`intLocation`),
  CONSTRAINT `fk_intHID_dAPCM` FOREIGN KEY (`intHID`) REFERENCES `hAPCM` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dAPCM` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dAPCM` */

/*Table structure for table `dAR` */

DROP TABLE IF EXISTS `dAR`;

CREATE TABLE `dAR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dAR` (`intHID`),
  KEY `fk_intLocation_dAR` (`intLocation`),
  CONSTRAINT `fk_intHID_dAR` FOREIGN KEY (`intHID`) REFERENCES `hAR` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dAR` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dAR` */

/*Table structure for table `dARCM` */

DROP TABLE IF EXISTS `dARCM`;

CREATE TABLE `dARCM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dARCM` (`intHID`),
  KEY `fk_intLocation_dARCM` (`intLocation`),
  CONSTRAINT `fk_intHID_dARCM` FOREIGN KEY (`intHID`) REFERENCES `hARCM` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dARCM` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dARCM` */

/*Table structure for table `dAdjustment` */

DROP TABLE IF EXISTS `dAdjustment`;

CREATE TABLE `dAdjustment` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dAdjustment` (`intHID`),
  KEY `fk_intItem_dAdjustment` (`intItem`),
  KEY `fk_intLocation_dAdjustment` (`intLocation`),
  CONSTRAINT `fk_intHID_dAdjustment` FOREIGN KEY (`intHID`) REFERENCES `hAdjustment` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dAdjustment` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dAdjustment` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dAdjustment` */

/*Table structure for table `dBOM` */

DROP TABLE IF EXISTS `dBOM`;

CREATE TABLE `dBOM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) NOT NULL,
  `intItem` int(11) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `vcUoM` varchar(50) NOT NULL,
  `intCost` double NOT NULL,
  `intAutoIssue` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dBOM` (`intHID`),
  KEY `fk_intItem_dBOM` (`intItem`),
  CONSTRAINT `fk_intHID_dBOM` FOREIGN KEY (`intHID`) REFERENCES `hBOM` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dBOM` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dBOM` */

/*Table structure for table `dCRU` */

DROP TABLE IF EXISTS `dCRU`;

CREATE TABLE `dCRU` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) NOT NULL,
  `intItem` int(11) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `vcUoM` varchar(50) NOT NULL,
  `intCost` double NOT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dCRU` (`intHID`),
  KEY `fk_intItem_dCRU` (`intItem`),
  CONSTRAINT `fk_intHID_dCRU` FOREIGN KEY (`intHID`) REFERENCES `hCRU` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dCRU` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dCRU` */

/*Table structure for table `dDN` */

DROP TABLE IF EXISTS `dDN`;

CREATE TABLE `dDN` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dDN` (`intHID`),
  KEY `fk_intItem_dDN` (`intItem`),
  KEY `fk_intLocation_dDN` (`intLocation`),
  CONSTRAINT `fk_intHID_dDN` FOREIGN KEY (`intHID`) REFERENCES `hDN` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dDN` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dDN` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dDN` */

/*Table structure for table `dDiscount` */

DROP TABLE IF EXISTS `dDiscount`;

CREATE TABLE `dDiscount` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) NOT NULL,
  `intItem` int(11) NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intDisc` int(11) NOT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dDiscount` (`intHID`),
  KEY `fk_intItem_dDiscount` (`intItem`),
  KEY `fk_intLocation_dDiscount` (`intLocation`),
  CONSTRAINT `fk_intHID_dDiscount` FOREIGN KEY (`intHID`) REFERENCES `hDiscount` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dDiscount` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dDiscount` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dDiscount` */

/*Table structure for table `dGRPO` */

DROP TABLE IF EXISTS `dGRPO`;

CREATE TABLE `dGRPO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dGRPO` (`intHID`),
  KEY `fk_intItem_dGRPO` (`intItem`),
  KEY `fk_intLocation_dGRPO` (`intLocation`),
  CONSTRAINT `fk_intHID_dGRPO` FOREIGN KEY (`intHID`) REFERENCES `hGRPO` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dGRPO` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dGRPO` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dGRPO` */

/*Table structure for table `dIFP` */

DROP TABLE IF EXISTS `dIFP`;

CREATE TABLE `dIFP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dIFP` (`intHID`),
  KEY `fk_intItem_dIFP` (`intItem`),
  KEY `fk_intLocation_dIFP` (`intLocation`),
  CONSTRAINT `fk_intHID_dIFP` FOREIGN KEY (`intHID`) REFERENCES `hIFP` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dIFP` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dIFP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dIFP` */

/*Table structure for table `dINPAY` */

DROP TABLE IF EXISTS `dINPAY`;

CREATE TABLE `dINPAY` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `vcDocNum` varchar(50) NOT NULL,
  `intDocTotal` double NOT NULL DEFAULT '0',
  `intApplied` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dAR` (`intHID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dINPAY` */

/*Table structure for table `dIP` */

DROP TABLE IF EXISTS `dIP`;

CREATE TABLE `dIP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQtyBefore` double NOT NULL DEFAULT '0',
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dIP` (`intHID`),
  KEY `fk_intItem_dIP` (`intItem`),
  KEY `fk_intLocation_dIP` (`intLocation`),
  CONSTRAINT `fk_intHID_dIP` FOREIGN KEY (`intHID`) REFERENCES `hIP` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dIP` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dIP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dIP` */

/*Table structure for table `dIT` */

DROP TABLE IF EXISTS `dIT`;

CREATE TABLE `dIT` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocationFrom` int(11) DEFAULT NULL,
  `vcLocationFrom` varchar(250) NOT NULL,
  `intLocationTo` int(11) DEFAULT NULL,
  `vcLocationTo` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dIT` (`intHID`),
  KEY `fk_intItem_dIT` (`intItem`),
  KEY `fk_intLocationFrom` (`intLocationFrom`),
  KEY `fk_intLocationTo` (`intLocationTo`),
  CONSTRAINT `fk_intHID_dIT` FOREIGN KEY (`intHID`) REFERENCES `hIT` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dIT` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocationFrom` FOREIGN KEY (`intLocationFrom`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocationTo` FOREIGN KEY (`intLocationTo`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dIT` */

/*Table structure for table `dMutation` */

DROP TABLE IF EXISTS `dMutation`;

CREATE TABLE `dMutation` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `dtDate` date DEFAULT NULL,
  `dtPost` date DEFAULT NULL,
  `vcType` varchar(25) NOT NULL,
  `vcDoc` varchar(50) NOT NULL,
  `intItem` int(11) NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcItemCode` varchar(50) NOT NULL,
  `vcItemName` varchar(100) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intItem_dMutation` (`intItem`),
  KEY `fk_intLocation_dMutation` (`intLocation`),
  CONSTRAINT `fk_intItem_dMutation` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`),
  CONSTRAINT `fk_intLocation_dMutation` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dMutation` */

/*Table structure for table `dOUTPAY` */

DROP TABLE IF EXISTS `dOUTPAY`;

CREATE TABLE `dOUTPAY` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `vcDocNum` varchar(50) NOT NULL,
  `intDocTotal` double NOT NULL DEFAULT '0',
  `intApplied` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dAP` (`intHID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dOUTPAY` */

/*Table structure for table `dPID` */

DROP TABLE IF EXISTS `dPID`;

CREATE TABLE `dPID` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQtyBefore` double NOT NULL DEFAULT '0',
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dPID` (`intHID`),
  KEY `fk_intItem_dPID` (`intItem`),
  KEY `fk_intLocation_dPID` (`intLocation`),
  CONSTRAINT `fk_intHID_dPID` FOREIGN KEY (`intHID`) REFERENCES `hPID` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dPID` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dPID` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dPID` */

/*Table structure for table `dPO` */

DROP TABLE IF EXISTS `dPO`;

CREATE TABLE `dPO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dPO` (`intHID`),
  KEY `fk_intLocation_dPO` (`intLocation`),
  CONSTRAINT `fk_intHID_dPO` FOREIGN KEY (`intHID`) REFERENCES `hPO` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dPO` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dPO` */

/*Table structure for table `dPR` */

DROP TABLE IF EXISTS `dPR`;

CREATE TABLE `dPR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dPR` (`intHID`),
  KEY `fk_intItem_dPR` (`intItem`),
  KEY `fk_intLocation_dPR` (`intLocation`),
  CONSTRAINT `fk_intHID_dPR` FOREIGN KEY (`intHID`) REFERENCES `hPR` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dPR` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dPR` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dPR` */

/*Table structure for table `dPRO` */

DROP TABLE IF EXISTS `dPRO`;

CREATE TABLE `dPRO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) NOT NULL,
  `intItem` int(11) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intPlannedQty` double NOT NULL,
  `intIssueQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intCost` double NOT NULL,
  `intAutoIssue` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dPRO` (`intHID`),
  KEY `fk_intItem_dPRO` (`intItem`),
  CONSTRAINT `fk_intHID_dPRO` FOREIGN KEY (`intHID`) REFERENCES `hPRO` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dPRO` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dPRO` */

/*Table structure for table `dRFP` */

DROP TABLE IF EXISTS `dRFP`;

CREATE TABLE `dRFP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dRFP` (`intHID`),
  KEY `fk_intItem_dRFP` (`intItem`),
  KEY `fk_intLocation_dRFP` (`intLocation`),
  CONSTRAINT `fk_intHID_dRFP` FOREIGN KEY (`intHID`) REFERENCES `hRFP` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dRFP` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dRFP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dRFP` */

/*Table structure for table `dRevaluation` */

DROP TABLE IF EXISTS `dRevaluation`;

CREATE TABLE `dRevaluation` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intCostBefore` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intHID_dRevaluation` (`intHID`),
  KEY `fk_intItem_dRevaluation` (`intItem`),
  KEY `fk_intLocation_dRevaluation` (`intLocation`),
  CONSTRAINT `fk_intHID_dRevaluation` FOREIGN KEY (`intHID`) REFERENCES `hRevaluation` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dRevaluation` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dRevaluation` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dRevaluation` */

/*Table structure for table `dSO` */

DROP TABLE IF EXISTS `dSO`;

CREATE TABLE `dSO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `intLoadQty` double NOT NULL DEFAULT '0',
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intCreated` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dSO` (`intHID`),
  KEY `fk_intLocation_dSO` (`intLocation`),
  CONSTRAINT `fk_intHID_dSO` FOREIGN KEY (`intHID`) REFERENCES `hSO` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dSO` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dSO` */

/*Table structure for table `dSQ` */

DROP TABLE IF EXISTS `dSQ`;

CREATE TABLE `dSQ` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL DEFAULT '0',
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dSQ` (`intHID`),
  KEY `fk_intLocation_dSQ` (`intLocation`),
  CONSTRAINT `fk_intHID_dSQ` FOREIGN KEY (`intHID`) REFERENCES `hSQ` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dSQ` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dSQ` */

/*Table structure for table `dSR` */

DROP TABLE IF EXISTS `dSR`;

CREATE TABLE `dSR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intHID` int(11) DEFAULT NULL,
  `intBaseRef` int(11) NOT NULL DEFAULT '0',
  `vcBaseType` varchar(50) NOT NULL,
  `intItem` int(11) DEFAULT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intOpenQty` double NOT NULL,
  `vcUoM` varchar(50) NOT NULL,
  `intUoMType` int(11) NOT NULL DEFAULT '1',
  `intQtyInv` double NOT NULL,
  `intOpenQtyInv` double NOT NULL,
  `vcUoMInv` varchar(50) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `intDiscPer` double NOT NULL,
  `intDisc` double NOT NULL,
  `intPriceAfterDisc` double NOT NULL,
  `intLineTotal` double NOT NULL,
  `intCost` double NOT NULL,
  `intLineCost` double NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`intID`,`intPrice`),
  KEY `fk_intHID_dSR` (`intHID`),
  KEY `fk_intItem_dSR` (`intItem`),
  KEY `fk_intLocation_dSR` (`intLocation`),
  CONSTRAINT `fk_intHID_dSR` FOREIGN KEY (`intHID`) REFERENCES `hSR` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_dSR` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_dSR` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dSR` */

/*Table structure for table `dWalletMutation` */

DROP TABLE IF EXISTS `dWalletMutation`;

CREATE TABLE `dWalletMutation` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intWallet` int(11) NOT NULL,
  `dtDate` date DEFAULT NULL,
  `vcDoc` varchar(30) NOT NULL,
  `vcDocPay` varchar(30) NOT NULL,
  `vcType` varchar(25) NOT NULL,
  `intCost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intWallet_dWalletMutation` (`intWallet`),
  CONSTRAINT `fk_intWallet_dWalletMutation` FOREIGN KEY (`intWallet`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dWalletMutation` */

/*Table structure for table `hAP` */

DROP TABLE IF EXISTS `hAP`;

CREATE TABLE `hAP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `dtDueDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `intAmmountTendered` double NOT NULL DEFAULT '0',
  `intChange` double NOT NULL DEFAULT '0',
  `vcPaymentNote` varchar(100) NOT NULL,
  `vcPaymentCode` varchar(100) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hAP` (`vcDocNum`),
  KEY `fk_intBP_hAP` (`intBP`),
  CONSTRAINT `fk_intBP_hAP` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hAP` */

/*Table structure for table `hAPCM` */

DROP TABLE IF EXISTS `hAPCM`;

CREATE TABLE `hAPCM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `dtDueDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `intAmmountTendered` double NOT NULL DEFAULT '0',
  `intChange` double NOT NULL DEFAULT '0',
  `vcPaymentNote` varchar(100) NOT NULL,
  `vcPaymentCode` varchar(100) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hAPCM` (`vcDocNum`),
  KEY `fk_intBP_hAPCM` (`intBP`),
  CONSTRAINT `fk_intBP_hAPCM` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hAPCM` */

/*Table structure for table `hAR` */

DROP TABLE IF EXISTS `hAR`;

CREATE TABLE `hAR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `dtDueDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `intAmmountTendered` double NOT NULL DEFAULT '0',
  `intChange` double NOT NULL DEFAULT '0',
  `vcPaymentNote` varchar(100) NOT NULL,
  `vcPaymentCode` varchar(100) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hAR` (`vcDocNum`),
  KEY `fk_intBP_hAR` (`intBP`),
  CONSTRAINT `fk_intBP_hAR` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hAR` */

/*Table structure for table `hARCM` */

DROP TABLE IF EXISTS `hARCM`;

CREATE TABLE `hARCM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `dtDueDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `intAmmountTendered` double NOT NULL DEFAULT '0',
  `intChange` double NOT NULL DEFAULT '0',
  `vcPaymentNote` varchar(100) NOT NULL,
  `vcPaymentCode` varchar(100) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hARCM` (`vcDocNum`),
  KEY `fk_intBP_hARCM` (`intBP`),
  CONSTRAINT `fk_intBP_hARCM` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hARCM` */

/*Table structure for table `hAdjustment` */

DROP TABLE IF EXISTS `hAdjustment`;

CREATE TABLE `hAdjustment` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `vcType` varchar(25) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hAdjustment` (`vcDocNum`),
  KEY `fk_intLocation_hAdjustment` (`intLocation`),
  CONSTRAINT `fk_intLocation_hAdjustment` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hAdjustment` */

/*Table structure for table `hBOM` */

DROP TABLE IF EXISTS `hBOM`;

CREATE TABLE `hBOM` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcType` varchar(3) NOT NULL DEFAULT 'PRO' COMMENT 'PRO or SLS',
  `vcRef` varchar(25) NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `intItem` int(11) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL,
  `intCost` double NOT NULL,
  `vcSONumber` varchar(100) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  `vcRemarks` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hBOM` (`vcDocNum`),
  KEY `fk_intItem_hBOM` (`intItem`),
  KEY `fk_intLocation_hBOM` (`intLocation`),
  CONSTRAINT `fk_intItem_hBOM` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_hBOM` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hBOM` */

/*Table structure for table `hCRU` */

DROP TABLE IF EXISTS `hCRU`;

CREATE TABLE `hCRU` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBOM` int(11) NOT NULL,
  `dtDate` date NOT NULL,
  `intCostBefore` double NOT NULL,
  `intCost` double NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intBOM_hCRU` (`intBOM`),
  CONSTRAINT `fk_intBOM_hCRU` FOREIGN KEY (`intBOM`) REFERENCES `hBOM` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hCRU` */

/*Table structure for table `hDN` */

DROP TABLE IF EXISTS `hDN`;

CREATE TABLE `hDN` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hDN` (`vcDocNum`),
  KEY `fk_intBP_hDN` (`intBP`),
  CONSTRAINT `fk_intBP_hDN` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hDN` */

/*Table structure for table `hDiscount` */

DROP TABLE IF EXISTS `hDiscount`;

CREATE TABLE `hDiscount` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `dtDate` date NOT NULL,
  `dtUntil` date NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hDiscount` (`vcDocNum`),
  KEY `fk_intLocation_hDiscount` (`intLocation`),
  CONSTRAINT `fk_intLocation_hDiscount` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hDiscount` */

/*Table structure for table `hGRPO` */

DROP TABLE IF EXISTS `hGRPO`;

CREATE TABLE `hGRPO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hGRPO` (`vcDocNum`),
  KEY `fk_intBP_hGRPO` (`intBP`),
  CONSTRAINT `fk_intBP_hGRPO` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hGRPO` */

/*Table structure for table `hIFP` */

DROP TABLE IF EXISTS `hIFP`;

CREATE TABLE `hIFP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPRO` int(11) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `vcType` varchar(25) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hIFP` (`vcDocNum`),
  KEY `fk_intLocation_hIFP` (`intLocation`),
  KEY `fk_intPRO_hIFP` (`intPRO`),
  CONSTRAINT `fk_intLocation_hIFP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intPRO_hIFP` FOREIGN KEY (`intPRO`) REFERENCES `hPRO` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hIFP` */

/*Table structure for table `hINPAY` */

DROP TABLE IF EXISTS `hINPAY`;

CREATE TABLE `hINPAY` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intWallet` int(11) NOT NULL,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `vcRemarks` varchar(250) NOT NULL,
  `intAmount` double NOT NULL DEFAULT '0',
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hINPAY` (`vcDocNum`),
  KEY `fk_intBP_hINPAY` (`intBP`),
  KEY `fk_intWallet_hINPAY` (`intWallet`),
  CONSTRAINT `fk_intBP_hINPAY` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intWallet_hINPAY` FOREIGN KEY (`intWallet`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hINPAY` */

/*Table structure for table `hIP` */

DROP TABLE IF EXISTS `hIP`;

CREATE TABLE `hIP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_IP` (`vcDocNum`),
  KEY `fk_intLocation_hIP` (`intLocation`),
  CONSTRAINT `fk_intLocation_hIP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hIP` */

/*Table structure for table `hIT` */

DROP TABLE IF EXISTS `hIT`;

CREATE TABLE `hIT` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intLocationFrom` int(11) DEFAULT NULL,
  `vcLocationFrom` varchar(250) NOT NULL DEFAULT '',
  `intLocationTo` int(11) DEFAULT NULL,
  `vcLocationTo` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hIT` (`vcDocNum`),
  KEY `fk_intLocationFrom_hIT` (`intLocationFrom`),
  KEY `fk_intLocationTo_hIT` (`intLocationTo`),
  CONSTRAINT `fk_intLocationFrom_hIT` FOREIGN KEY (`intLocationFrom`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocationTo_hIT` FOREIGN KEY (`intLocationTo`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hIT` */

/*Table structure for table `hOUTPAY` */

DROP TABLE IF EXISTS `hOUTPAY`;

CREATE TABLE `hOUTPAY` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intWallet` int(11) NOT NULL,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `vcRemarks` varchar(250) NOT NULL,
  `intAmount` double NOT NULL DEFAULT '0',
  `intApplied` double NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL DEFAULT '0',
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hOUTPAY` (`vcDocNum`),
  KEY `fk_intBP_hOUTPAY` (`intBP`),
  KEY `fk_intWallet_hOUTPAY` (`intWallet`),
  CONSTRAINT `fk_intBP_hOUTPAY` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intWallet_hOUTPAY` FOREIGN KEY (`intWallet`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hOUTPAY` */

/*Table structure for table `hPID` */

DROP TABLE IF EXISTS `hPID`;

CREATE TABLE `hPID` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `vcStatus` varchar(1) NOT NULL DEFAULT 'O',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_PID` (`vcDocNum`),
  KEY `fk_intLocation_hPID` (`intLocation`),
  CONSTRAINT `fk_intLocation_hPID` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hPID` */

/*Table structure for table `hPO` */

DROP TABLE IF EXISTS `hPO`;

CREATE TABLE `hPO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intTable` int(11) NOT NULL DEFAULT '0',
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hPO` (`vcDocNum`),
  KEY `fk_intBP_hPO` (`intBP`),
  CONSTRAINT `fk_intBP_hPO` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hPO` */

/*Table structure for table `hPR` */

DROP TABLE IF EXISTS `hPR`;

CREATE TABLE `hPR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hPR` (`vcDocNum`),
  KEY `fk_intBP_hPR` (`intBP`),
  CONSTRAINT `fk_intBP_hPR` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hPR` */

/*Table structure for table `hPRO` */

DROP TABLE IF EXISTS `hPRO`;

CREATE TABLE `hPRO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtReqDate` date NOT NULL,
  `dtReleaseDate` date NOT NULL,
  `vcUserRelease` varchar(100) NOT NULL,
  `dtCloseDate` date NOT NULL,
  `vcUserClose` varchar(100) NOT NULL,
  `vcRef` varchar(25) NOT NULL,
  `intLocation` int(11) NOT NULL,
  `vcLocation` varchar(250) NOT NULL,
  `intItem` int(11) NOT NULL,
  `vcItemCode` varchar(100) NOT NULL,
  `vcItemName` varchar(250) NOT NULL,
  `intQty` double NOT NULL DEFAULT '0',
  `intPlannedQty` double NOT NULL DEFAULT '0',
  `intActualQty` double NOT NULL DEFAULT '0',
  `intRejectQty` double NOT NULL DEFAULT '0',
  `intCost` double NOT NULL,
  `intComponentCost` double NOT NULL DEFAULT '0',
  `intProductCost` double NOT NULL DEFAULT '0',
  `intRejectCost` double NOT NULL DEFAULT '0',
  `intVariance` double NOT NULL DEFAULT '0',
  `vcSONumber` varchar(100) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `intStatus` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hPRO` (`vcDocNum`),
  KEY `fk_intItem_hPRO` (`intItem`),
  KEY `fk_intLocation_hPRO` (`intLocation`),
  CONSTRAINT `fk_intItem_hPRO` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intLocation_hPRO` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hPRO` */

/*Table structure for table `hRFP` */

DROP TABLE IF EXISTS `hRFP`;

CREATE TABLE `hRFP` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPRO` int(11) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `vcType` varchar(25) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hRFP` (`vcDocNum`),
  KEY `fk_intLocation_hRFP` (`intLocation`),
  KEY `fk_intPRO_hRFP` (`intPRO`),
  CONSTRAINT `fk_intLocation_hRFP` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intPRO_hRFP` FOREIGN KEY (`intPRO`) REFERENCES `hPRO` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hRFP` */

/*Table structure for table `hRevaluation` */

DROP TABLE IF EXISTS `hRevaluation`;

CREATE TABLE `hRevaluation` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intLocation` int(11) DEFAULT NULL,
  `vcLocation` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_Revaluation` (`vcDocNum`),
  KEY `fk_intLocation_hRevaluation` (`intLocation`),
  CONSTRAINT `fk_intLocation_hRevaluation` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hRevaluation` */

/*Table structure for table `hSO` */

DROP TABLE IF EXISTS `hSO`;

CREATE TABLE `hSO` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intTable` int(11) NOT NULL DEFAULT '0',
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hSO` (`vcDocNum`),
  KEY `fk_intBP_hSO` (`intBP`),
  CONSTRAINT `fk_intBP_hSO` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hSO` */

/*Table structure for table `hSQ` */

DROP TABLE IF EXISTS `hSQ`;

CREATE TABLE `hSQ` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `intService` int(11) NOT NULL DEFAULT '0',
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hSQ` (`vcDocNum`),
  KEY `fk_intBP_hSQ` (`intBP`),
  CONSTRAINT `fk_intBP_hSQ` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hSQ` */

/*Table structure for table `hSR` */

DROP TABLE IF EXISTS `hSR`;

CREATE TABLE `hSR` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intBP` int(11) NOT NULL,
  `vcBPCode` varchar(50) NOT NULL,
  `vcBPName` varchar(250) NOT NULL,
  `vcSalesName` varchar(250) NOT NULL,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `dtDelDate` datetime NOT NULL,
  `vcRef` varchar(100) NOT NULL,
  `intFreight` double NOT NULL,
  `intTaxPer` double NOT NULL DEFAULT '0',
  `intTax` double NOT NULL DEFAULT '0',
  `vcStatus` varchar(5) NOT NULL DEFAULT 'O',
  `intDiscPer` double NOT NULL DEFAULT '0',
  `intDisc` double NOT NULL DEFAULT '0',
  `intDocTotalBefore` double NOT NULL,
  `intDocTotal` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `vcAddress` varchar(250) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hSR` (`vcDocNum`),
  KEY `fk_intBP_hSR` (`intBP`),
  CONSTRAINT `fk_intBP_hSR` FOREIGN KEY (`intBP`) REFERENCES `mbp` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hSR` */

/*Table structure for table `hWalletAdjustment` */

DROP TABLE IF EXISTS `hWalletAdjustment`;

CREATE TABLE `hWalletAdjustment` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `intWallet` int(11) NOT NULL,
  `vcWalletName` varchar(100) NOT NULL,
  `vcType` varchar(25) NOT NULL,
  `intValue` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hWalletAdjustment` (`vcDocNum`),
  KEY `fk_intWallet_hWalletAdjustment` (`intWallet`),
  CONSTRAINT `fk_intWallet_hWalletAdjustment` FOREIGN KEY (`intWallet`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hWalletAdjustment` */

/*Table structure for table `hWalletTransfer` */

DROP TABLE IF EXISTS `hWalletTransfer`;

CREATE TABLE `hWalletTransfer` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcDocNum` varchar(100) NOT NULL,
  `dtDate` date NOT NULL,
  `intWalletFrom` int(11) NOT NULL,
  `intWalletTo` int(11) NOT NULL,
  `vcWalletNameFrom` varchar(100) NOT NULL,
  `vcWalletNameTo` varchar(100) NOT NULL,
  `intValue` double NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `vcUser` varchar(250) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcDocNum_hWalletTransfer` (`vcDocNum`),
  KEY `fk_intWalletFrom_hWalletTransfer` (`intWalletFrom`),
  KEY `fk_intWalletTo_hWalletTransfer` (`intWalletTo`),
  CONSTRAINT `fk_intWalletFrom_hWalletTransfer` FOREIGN KEY (`intWalletFrom`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intWalletTo_hWalletTransfer` FOREIGN KEY (`intWalletTo`) REFERENCES `mwallet` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hWalletTransfer` */

/*Table structure for table `maccess` */

DROP TABLE IF EXISTS `maccess`;

CREATE TABLE `maccess` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `intForm` int(11) DEFAULT NULL,
  `intCreate` tinyint(4) NOT NULL DEFAULT '0',
  `intRead` tinyint(4) NOT NULL DEFAULT '0',
  `intUpdate` tinyint(4) NOT NULL DEFAULT '0',
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  `intOrder` int(11) NOT NULL DEFAULT '0',
  `intSetup` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `maccess_userid` (`intUserID`),
  KEY `maccess_formid` (`intForm`),
  CONSTRAINT `maccess_formid` FOREIGN KEY (`intForm`) REFERENCES `mform` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `maccess_userid` FOREIGN KEY (`intUserID`) REFERENCES `muser` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

/*Data for the table `maccess` */

insert  into `maccess`(`intID`,`intUserID`,`intForm`,`intCreate`,`intRead`,`intUpdate`,`intDelete`,`intOrder`,`intSetup`) values (1,1,2,1,1,1,1,0,1),(16,1,3,1,1,1,1,0,1),(17,1,4,1,1,1,1,0,1),(18,1,40,1,1,1,1,0,1),(19,1,7,1,1,1,1,0,1),(20,1,8,1,1,1,1,0,1),(21,1,9,1,1,1,1,0,1),(22,1,10,1,1,1,1,0,1),(23,1,11,1,1,1,1,0,1),(24,1,15,1,1,1,1,0,1),(25,1,16,1,1,1,1,0,1),(26,1,17,1,1,1,1,0,1),(27,1,18,1,1,1,1,0,1),(28,1,19,1,1,1,1,0,1),(29,1,23,1,1,1,1,0,1),(30,1,25,1,1,1,1,0,1),(31,1,26,1,1,1,1,0,1),(32,1,28,1,1,1,1,0,1),(33,1,29,1,1,1,1,0,1),(34,1,30,1,1,1,1,0,1),(35,1,33,1,1,1,1,0,1),(36,1,35,1,1,1,1,0,1),(37,1,36,1,1,1,1,0,1),(38,1,37,1,1,1,1,0,1),(39,1,39,1,1,1,1,0,1),(40,1,41,1,1,1,1,0,1),(41,1,42,1,1,1,1,0,1),(42,1,43,1,1,1,1,0,1),(43,1,44,1,1,1,1,0,1),(44,1,45,1,1,1,1,0,1),(45,1,46,1,1,1,1,0,1),(46,1,47,1,1,1,1,0,1),(47,1,49,1,1,1,1,0,1),(48,1,50,1,1,1,1,0,1),(49,1,51,1,1,1,1,0,1),(50,1,52,1,1,1,1,0,1),(51,1,53,1,1,1,1,0,1),(52,1,54,1,1,1,1,0,1),(53,1,57,1,1,1,1,0,1),(54,1,58,1,1,1,1,0,1),(55,1,59,1,1,1,1,0,1),(56,1,61,1,1,1,1,0,1),(57,1,63,1,1,1,1,0,1),(58,1,64,1,1,1,1,0,1),(59,1,65,1,1,1,1,0,1),(60,1,66,1,1,1,1,0,1),(61,1,67,1,1,1,1,0,1),(62,1,68,1,1,1,1,0,1),(63,1,69,1,1,1,1,0,1),(64,1,70,1,1,1,1,0,1),(65,1,71,1,1,1,1,0,1),(66,1,72,1,1,1,1,0,1),(67,1,73,1,1,1,1,0,1),(68,1,74,1,1,1,1,0,1),(69,1,75,1,1,1,1,0,1),(70,1,76,1,1,1,1,0,1),(107,1,77,1,1,1,1,0,1),(108,1,78,1,1,1,1,0,1),(109,1,79,1,1,1,1,0,1),(110,1,80,1,1,1,1,0,1);

/*Table structure for table `maccess_dashboard` */

DROP TABLE IF EXISTS `maccess_dashboard`;

CREATE TABLE `maccess_dashboard` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) NOT NULL,
  `intDashboard` int(11) NOT NULL,
  `intOrder` int(11) NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `maccess_dashboard` */

insert  into `maccess_dashboard`(`intID`,`intUserID`,`intDashboard`,`intOrder`) values (48,1,3,1),(47,1,2,2),(46,1,1,3);

/*Table structure for table `maccessplan` */

DROP TABLE IF EXISTS `maccessplan`;

CREATE TABLE `maccessplan` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `intPlan` int(11) DEFAULT NULL,
  `intOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intUserID_maccessplan` (`intUserID`),
  KEY `fk_intPlan_maccessplan` (`intPlan`),
  CONSTRAINT `fk_intPlan_maccessplan` FOREIGN KEY (`intPlan`) REFERENCES `mplan` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intUserID_maccessplan` FOREIGN KEY (`intUserID`) REFERENCES `muser` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `maccessplan` */

insert  into `maccessplan`(`intID`,`intUserID`,`intPlan`,`intOrder`) values (1,1,1,0);

/*Table structure for table `maccesssub` */

DROP TABLE IF EXISTS `maccesssub`;

CREATE TABLE `maccesssub` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `intForm` int(11) DEFAULT NULL,
  PRIMARY KEY (`intID`),
  KEY `maccesssub_muser` (`intUserID`),
  KEY `maccesssub_mform` (`intForm`),
  CONSTRAINT `maccesssub_mform` FOREIGN KEY (`intForm`) REFERENCES `msubform` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `maccesssub_muser` FOREIGN KEY (`intUserID`) REFERENCES `muser` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `maccesssub` */

/*Table structure for table `maccesstop` */

DROP TABLE IF EXISTS `maccesstop`;

CREATE TABLE `maccesstop` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `intForm` int(11) DEFAULT NULL,
  `intOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `maccesstop_userid` (`intUserID`),
  KEY `maccesstop_formid` (`intForm`),
  CONSTRAINT `maccesstop_formid` FOREIGN KEY (`intForm`) REFERENCES `mtopform` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `maccesstop_userid` FOREIGN KEY (`intUserID`) REFERENCES `muser` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `maccesstop` */

insert  into `maccesstop`(`intID`,`intUserID`,`intForm`,`intOrder`) values (1,1,1,0),(2,1,2,0),(3,1,3,0);

/*Table structure for table `mbp` */

DROP TABLE IF EXISTS `mbp`;

CREATE TABLE `mbp` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intCategory` int(11) NOT NULL,
  `intPriceList` int(11) NOT NULL,
  `intPaymentTerm` int(11) NOT NULL,
  `intTax` int(11) NOT NULL,
  `vcType` varchar(5) NOT NULL DEFAULT 'C',
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `vcTaxNum` varchar(100) NOT NULL,
  `vcTelp1` varchar(50) NOT NULL,
  `vcTelp2` varchar(50) NOT NULL,
  `vcFax` varchar(50) NOT NULL,
  `vcEmail` varchar(100) NOT NULL,
  `vcWebsite` varchar(100) NOT NULL,
  `vcContactPerson` varchar(100) NOT NULL,
  `vcBillToAddress` varchar(250) NOT NULL,
  `vcBillToCity` varchar(100) NOT NULL,
  `vcBillToCountry` varchar(100) NOT NULL,
  `vcShipToAddress` varchar(250) NOT NULL,
  `vcShipToCity` varchar(100) NOT NULL,
  `vcShipToCountry` varchar(100) NOT NULL,
  `intCreditLimit` int(11) NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `intBalance` double NOT NULL,
  `intDelivery` double NOT NULL,
  `intOrder` double NOT NULL,
  `vcUser` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `fk_vcCode_mbp` (`vcCode`),
  KEY `fk_intCategory_mbp` (`intCategory`),
  KEY `fk_intPriceList_mbp` (`intPriceList`),
  KEY `fk_intTax_mbp` (`intTax`),
  CONSTRAINT `fk_intCategory_mbp` FOREIGN KEY (`intCategory`) REFERENCES `mbpcategory` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intPriceList_mbp` FOREIGN KEY (`intPriceList`) REFERENCES `mpricecategory` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intTax_mbp` FOREIGN KEY (`intTax`) REFERENCES `mtax` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mbp` */

/*Table structure for table `mbpcategory` */

DROP TABLE IF EXISTS `mbpcategory`;

CREATE TABLE `mbpcategory` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcType` varchar(15) NOT NULL DEFAULT 'C',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mbpcategory` */

/*Table structure for table `mcountry` */

DROP TABLE IF EXISTS `mcountry`;

CREATE TABLE `mcountry` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=latin1;

/*Data for the table `mcountry` */

insert  into `mcountry`(`intID`,`vcName`,`intDeleted`) values (2,'Afghanistan',0),(3,'Albania',0),(4,'Algeria',0),(5,'Andorra',0),(6,'Angola',0),(7,'Antigua and Barbuda',0),(8,'Argentina',0),(9,'Armenia',0),(10,'Australia',0),(11,'Austria',0),(12,'Azerbaijan',0),(13,'Bahamas',0),(14,'Bahrain',0),(15,'Bangladesh',0),(16,'Barbados',0),(17,'Belarus',0),(18,'Belgium',0),(19,'Belize',0),(20,'Benin',0),(21,'Bhutan',0),(22,'Bolivia',0),(23,'Bosnia and Herzegovina',0),(24,'Botswana',0),(25,'Brazil',0),(26,'Brunei Darussalam',0),(27,'Bulgaria',0),(28,'Burkina Faso',0),(29,'Burundi',0),(30,'Cabo Verde',0),(31,'Cambodia',0),(32,'Cameroon',0),(33,'Canada',0),(34,'Central African Republic',0),(35,'Chad',0),(36,'Chile',0),(37,'China',0),(38,'Colombia',0),(39,'Comoros',0),(40,'Congo',0),(41,'Costa Rica',0),(42,'Côte d\'Ivoire',0),(43,'Croatia',0),(44,'Cuba',0),(45,'Cyprus',0),(46,'Czech Republic',0),(47,'Democratic People\'s Republic of Korea (North Korea)',0),(48,'Democratic Republic of the Cong',0),(49,'Denmark',0),(50,'Djibouti',0),(51,'Dominica',0),(52,'Dominican Republic',0),(53,'Ecuador',0),(54,'Egypt',0),(55,'El Salvador',0),(56,'Equatorial Guinea',0),(57,'Eritrea',0),(58,'Estonia',0),(59,'Ethiopia',0),(60,'Fiji',0),(61,'Finland',0),(62,'France',0),(63,'Gabon',0),(64,'Gambia',0),(65,'Georgia',0),(66,'Germany',0),(67,'Ghana',0),(68,'Greece',0),(69,'Grenada',0),(70,'Guatemala',0),(71,'Guinea',0),(72,'Guinea-Bissau',0),(73,'Guyana',0),(74,'Haiti',0),(75,'Honduras',0),(76,'Hungary',0),(77,'Iceland',0),(78,'India',0),(79,'Indonesia',0),(80,'Iran',0),(81,'Iraq',0),(82,'Ireland',0),(83,'Israel',0),(84,'Italy',0),(85,'Jamaica',0),(86,'Japan',0),(87,'Jordan',0),(88,'Kazakhstan',0),(89,'Kenya',0),(90,'Kiribati',0),(91,'Kuwait',0),(92,'Kyrgyzstan',0),(93,'Lao People\'s Democratic Republic (Laos)',0),(94,'Latvia',0),(95,'Lebanon',0),(96,'Lesotho',0),(97,'Liberia',0),(98,'Libya',0),(99,'Liechtenstein',0),(100,'Lithuania',0),(101,'Luxembourg',0),(102,'Macedonia',0),(103,'Madagascar',0),(104,'Malawi',0),(105,'Malaysia',0),(106,'Maldives',0),(107,'Mali',0),(108,'Malta',0),(109,'Marshall Islands',0),(110,'Mauritania',0),(111,'Mauritius',0),(112,'Mexico',0),(113,'Micronesia (Federated States of)',0),(114,'Monaco',0),(115,'Mongolia',0),(116,'Montenegro',0),(117,'Morocco',0),(118,'Mozambique',0),(119,'Myanmar',0),(120,'Namibia',0),(121,'Nauru',0),(122,'Nepal',0),(123,'Netherlands',0),(124,'New Zealand',0),(125,'Nicaragua',0),(126,'Niger',0),(127,'Nigeria',0),(128,'Norway',0),(129,'Oman',0),(130,'Pakistan',0),(131,'Palau',0),(132,'Panama',0),(133,'Papua New Guinea',0),(134,'Paraguay',0),(135,'Peru',0),(136,'Philippines',0),(137,'Poland',0),(138,'Portugal',0),(139,'Qatar',0),(140,'Republic of Korea (South Korea)',0),(141,'Republic of Moldova',0),(142,'Romania',0),(143,'Russian Federation',0),(144,'Rwanda',0),(145,'Saint Kitts and Nevis',0),(146,'Saint Lucia',0),(147,'Saint Vincent and the Grenadines',0),(148,'Samoa',0),(149,'San Marino',0),(150,'Sao Tome and Principe',0),(151,'Saudi Arabia',0),(152,'Senegal',0),(153,'Serbia',0),(154,'Seychelles',0),(155,'Sierra Leone',0),(156,'Singapore',0),(157,'Slovakia',0),(158,'Slovenia',0),(159,'Solomon Islands',0),(160,'Somalia',0),(161,'South Africa',0),(162,'South Sudan',0),(163,'Spain',0),(164,'Sri Lanka',0),(165,'Sudan',0),(166,'Suriname',0),(167,'Swaziland',0),(168,'Sweden',0),(169,'Switzerland',0),(170,'Syrian Arab Republic',0),(171,'Tajikistan',0),(172,'Thailand',0),(173,'Timor-Leste',0),(174,'Togo',0),(175,'Tonga',0),(176,'Trinidad and Tobago',0),(177,'Tunisia',0),(178,'Turkey',0),(179,'Turkmenistan',0),(180,'Tuvalu',0),(181,'Uganda',0),(182,'Ukraine',0),(183,'United Arab Emirates',0),(184,'United Kingdom of Great Britain and Northern Ireland',0),(185,'United Republic of Tanzania',0),(186,'United States of America',0),(187,'Uruguay',0),(188,'Uzbekistan',0),(189,'Vanuatu',0),(190,'Venezuela',0),(191,'Vietnam',0),(192,'Yemen',0),(193,'Zambia',0),(194,'Zimbabwe',0);

/*Table structure for table `mdashboard` */

DROP TABLE IF EXISTS `mdashboard`;

CREATE TABLE `mdashboard` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(50) NOT NULL,
  `vcName` varchar(500) NOT NULL,
  `vcType` varchar(100) NOT NULL,
  `vcQuery` mediumtext NOT NULL,
  `intWidth` int(11) NOT NULL,
  `intDeleted` int(11) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL DEFAULT 'as',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mdashboard` */

insert  into `mdashboard`(`intID`,`vcCode`,`vcName`,`vcType`,`vcQuery`,`intWidth`,`intDeleted`,`vcRemarks`,`dtInsertTime`) values (1,'FOOTERDASH1','FOOTERDASH1','','',100,0,'','2019-08-08 13:43:38'),(2,'SALESREPORT','SALESREPORT','','',100,0,'','2019-08-08 13:43:52'),(3,'TOPDASH1','TOPDASH1','','',100,0,'','2019-08-08 13:44:04');

/*Table structure for table `mdoc` */

DROP TABLE IF EXISTS `mdoc`;

CREATE TABLE `mdoc` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(15) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intWidth` double NOT NULL DEFAULT '0',
  `intHeight` double NOT NULL DEFAULT '0',
  `vcFontFamily` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `mdoc` */

insert  into `mdoc`(`intID`,`vcCode`,`vcName`,`intWidth`,`intHeight`,`vcFontFamily`) values (1,'PO','Purchase Order',21.4,14,'arial'),(2,'GRPO','Good Receipt From Purchase Order',21.6,14,'arial'),(3,'AP','A/P Invoice',21.6,14,'arial'),(4,'PR','Purchase Return',21.6,14,'arial'),(5,'APCM','A/P Credit Memo',21.6,14,'arial'),(6,'SQ','Sales Quotation',21.6,14,'arial'),(7,'SO','Sales Order',21.6,14,'arial'),(8,'DN','Delivery Note',21.6,14,'arial'),(9,'AR','A/R Invoice',21.6,14,'arial'),(10,'SR','Sales Return',21.6,14,'arial'),(11,'ARCM','A/R Credit Memo',21.6,14,'arial'),(12,'AD','Adjustment',0,0,''),(13,'IT','Inventory Transfer',0,0,''),(14,'IFP','Issue For Production',0,0,''),(15,'RFP','Receipt From Production',0,0,''),(16,'INPAY','Incoming Payment',0,0,''),(17,'OUTPAY','Outgoing Payment',0,0,''),(18,'POS','Point of Sale',7.2,0,''),(19,'DW','Deposit or Withdrawal',0,0,''),(20,'WT','Wallet Transfer',0,0,''),(21,'POP','Point of Purchase',0,0,''),(22,'REVALUATION','Revaluation',0,0,''),(23,'PID','PID',0,0,''),(24,'IP','Inventory Posting',0,0,'');

/*Table structure for table `mform` */

DROP TABLE IF EXISTS `mform`;

CREATE TABLE `mform` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcCode` varchar(250) NOT NULL DEFAULT '',
  `vcIcon` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL DEFAULT '',
  `intHeader` int(11) NOT NULL DEFAULT '0',
  `IsHeader` tinyint(4) NOT NULL DEFAULT '0',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  `intSort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

/*Data for the table `mform` */

insert  into `mform`(`intID`,`vcName`,`vcCode`,`vcIcon`,`vcRemarks`,`intHeader`,`IsHeader`,`intDeleted`,`dtInsertTime`,`intSort`) values (1,'Administration','admin','<i class=\"fa fa-gear\"></i>','',0,1,0,'0000-00-00 00:00:00',99),(2,'User','user','','',1,0,0,'0000-00-00 00:00:00',0),(3,'Form','form','','',1,0,0,'0000-00-00 00:00:00',12),(4,'Sub Form','sub_form','','',1,0,0,'0000-00-00 00:00:00',13),(6,'Inventory','inventory','<i class=\"fa fa-cubes\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(7,'Item Master','item','','-',6,0,0,'0000-00-00 00:00:00',7),(8,'Price List','price','','-',6,0,0,'0000-00-00 00:00:00',8),(9,'Transfer Stock','transfer','','-',6,0,0,'0000-00-00 00:00:00',19),(10,'Adjustment Stock','adjustment','','-',6,0,0,'0000-00-00 00:00:00',20),(11,'Item Category','item_category','','-',6,0,0,'0000-00-00 00:00:00',5),(12,'Sales','sales','<i class=\"fa fa-line-chart\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(13,'Purchase','purchase','<i class=\"fa fa-shopping-basket\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(14,'Business Partner','business_partner','<i class=\"fa fa-users\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(15,'Business Partner Master','bp','','',14,0,0,'0000-00-00 00:00:00',10),(16,'Business Partner Category','bp_category','','',14,0,0,'0000-00-00 00:00:00',9),(17,'Sales Order','so','','',12,0,0,'0000-00-00 00:00:00',28),(18,'Delivery','dn','','',12,0,0,'0000-00-00 00:00:00',29),(19,'Purchase Order','po','','',13,0,0,'0000-00-00 00:00:00',34),(22,'Reports','report','<i class=\"fa fa-area-chart\" aria-hidden=\"true\"></i>','',0,1,0,'0000-00-00 00:00:00',99),(23,'Profit and Loss','pnl','','',22,0,0,'0000-00-00 00:00:00',46),(25,'Sales Return','sr','','-',12,0,0,'0000-00-00 00:00:00',31),(26,'Purchase Return','pr','','-',13,0,0,'0000-00-00 00:00:00',37),(28,'Sales Quotation','sq','','-',12,0,0,'0000-00-00 00:00:00',27),(29,'Location','location','','-',6,0,0,'0000-00-00 00:00:00',4),(30,'Good Receipt PO','grpo','','-',13,0,0,'0000-00-00 00:00:00',35),(32,'Banking','bank','<i class=\"fa fa-university\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(33,'Wallet','wallet','','-',32,0,0,'0000-00-00 00:00:00',11),(35,'Incoming Payment','inpay','','',32,0,0,'0000-00-00 00:00:00',40),(36,'Outgoing Payment','outpay','','',32,0,0,'0000-00-00 00:00:00',41),(37,'Deposit and Withdrawal','dep_with','','',32,0,0,'0000-00-00 00:00:00',42),(39,'Inventory Audit Reports','invauditreports','','',6,0,0,'0000-00-00 00:00:00',21),(40,'Plan','plan','','-',1,0,0,'0000-00-00 00:00:00',1),(41,'Tax Group','tax','','-',1,0,0,'0000-00-00 00:00:00',6),(42,'Top Form','top_form','','-',1,0,0,'0000-00-00 00:00:00',14),(43,'A/R Invoice','ar','','-',12,0,0,'0000-00-00 00:00:00',30),(44,'A/P Invoice','ap','','-',13,0,0,'0000-00-00 00:00:00',36),(45,'Table','table','','-',1,0,0,'0000-00-00 00:00:00',3),(46,'A/P Credit Memo','apcm','','',13,0,0,'0000-00-00 00:00:00',38),(47,'A/R Credit Memo','arcm','','',12,0,0,'0000-00-00 00:00:00',32),(48,'Production','proh','<i class=\"fa fa-industry\" aria-hidden=\"true\"></i>','-',0,1,0,'0000-00-00 00:00:00',99),(49,'Bill Of Material','bom','','',48,0,0,'0000-00-00 00:00:00',49),(50,'Production Order','pro','','-',48,0,0,'0000-00-00 00:00:00',50),(51,'Issue For Production','ifp','','',48,0,0,'0000-00-00 00:00:00',51),(52,'Receipt From Production','rfp','','',48,0,0,'0000-00-00 00:00:00',52),(53,'Dashboard','dashboard','','',1,0,0,'0000-00-00 00:00:00',15),(54,'Inventory Report','stock','','',6,0,0,'0000-00-00 00:00:00',22),(57,'Sales Reports','sls_report','','',12,0,0,'0000-00-00 00:00:00',33),(58,'Document Setting','doc_set','','',1,0,0,'0000-00-00 00:00:00',16),(59,'Cancel Document','cancel_doc','','',1,0,0,'0000-00-00 00:00:00',17),(61,'Purchase Report','pur_report','','',13,0,0,'0000-00-00 00:00:00',39),(63,'Payment Mutation Report','pay_mutation','','',32,0,0,'0000-00-00 00:00:00',44),(64,'Posting Periods','post_period','','',1,0,0,'0000-00-00 00:00:00',2),(65,'Open Item List','open_item','','',22,0,0,'0000-00-00 00:00:00',45),(66,'Wallet Transfer','wallet_transfer','','',32,0,0,'0000-00-00 00:00:00',43),(67,'Revaluation','revaluation','','',6,0,0,'0000-00-00 00:00:00',23),(68,'Customer Aging Reports','cus_aging','','',22,0,0,'0000-00-00 00:00:00',47),(69,'Vendor Aging Reports','ven_aging','','',22,0,0,'0000-00-00 00:00:00',48),(70,'Physical Inv. Document','pid','','',6,0,0,'0000-00-00 00:00:00',24),(71,'Inventory Posting','inv_posting','','',6,0,0,'0000-00-00 00:00:00',25),(72,'Cost Roll Up','cost_roll_up','','',48,0,0,'0000-00-00 00:00:00',53),(73,'Cost Roll Over','cost_roll_over','','',48,0,0,'0000-00-00 00:00:00',54),(74,'Discount','disc','','',12,0,0,'0000-00-00 00:00:00',26),(75,'Import Data','import','','',1,0,0,'0000-00-00 00:00:00',18),(76,'Profile','profile','','',1,0,0,'0000-00-00 00:00:00',-1),(77,'Jurnal','jurnal','','',81,0,0,'0000-00-00 00:00:00',55),(78,'Balance Sheet','balance_sheet','','',81,0,0,'0000-00-00 00:00:00',56),(79,'COA','coa','','',81,0,0,'0000-00-00 00:00:00',57),(80,'COA Setting','coa_setting','','',81,0,0,'0000-00-00 00:00:00',58),(81,'Finance','finance','<i class=\"fa fa-balance-scale\" aria-hidden=\"true\"></i>','',0,1,0,'0000-00-00 00:00:00',99);

/*Table structure for table `mgl` */

DROP TABLE IF EXISTS `mgl`;

CREATE TABLE `mgl` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intLevel` int(11) DEFAULT NULL,
  `vcCode` varchar(15) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intBalance` double DEFAULT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mgl` */

insert  into `mgl`(`intID`,`intLevel`,`vcCode`,`vcName`,`intBalance`,`intDeleted`) values (1,2,'1000000','Asset',NULL,0);

/*Table structure for table `mitem` */

DROP TABLE IF EXISTS `mitem`;

CREATE TABLE `mitem` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intCategory` int(11) NOT NULL,
  `intTax` int(11) NOT NULL,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `vcBarCode` varchar(200) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  `vcUoM` varchar(50) NOT NULL,
  `vcPurUoM` varchar(50) NOT NULL,
  `intPurUoM` double NOT NULL DEFAULT '1',
  `vcSlsUoM` varchar(50) NOT NULL,
  `intSlsUoM` double NOT NULL DEFAULT '1',
  `intHPP` double NOT NULL DEFAULT '0',
  `vcRemarks` varchar(250) NOT NULL,
  `blpImage` longblob NOT NULL,
  `blpImageMin` longblob NOT NULL,
  `intSalesItem` tinyint(4) NOT NULL DEFAULT '0',
  `intPurchaseItem` tinyint(4) NOT NULL DEFAULT '0',
  `intSalesWithoutQty` tinyint(4) NOT NULL DEFAULT '0',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `intActive` tinyint(4) NOT NULL DEFAULT '1',
  `vcUser` varchar(100) NOT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcCode_Item` (`vcCode`),
  KEY `fk_intCategory_mitem` (`intCategory`),
  KEY `fk_intTax_mitem` (`intTax`),
  CONSTRAINT `fk_intCategory_mitem` FOREIGN KEY (`intCategory`) REFERENCES `mitemcategory` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intTax_mitem` FOREIGN KEY (`intTax`) REFERENCES `mtax` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mitem` */

/*Table structure for table `mitemcategory` */

DROP TABLE IF EXISTS `mitemcategory`;

CREATE TABLE `mitemcategory` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(10) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mitemcategory` */

/*Table structure for table `mlocation` */

DROP TABLE IF EXISTS `mlocation`;

CREATE TABLE `mlocation` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPlan` int(11) NOT NULL,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcAddress` varchar(150) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcState` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intPlan_mlocation` (`intPlan`),
  CONSTRAINT `fk_intPlan_mlocation` FOREIGN KEY (`intPlan`) REFERENCES `mplan` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mlocation` */

/*Table structure for table `mperiod` */

DROP TABLE IF EXISTS `mperiod`;

CREATE TABLE `mperiod` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intYear` int(11) NOT NULL,
  `intMonth` int(11) NOT NULL,
  `vcStatus` varchar(1) NOT NULL DEFAULT 'U',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `mperiod` */

insert  into `mperiod`(`intID`,`vcName`,`intYear`,`intMonth`,`vcStatus`,`intDeleted`) values (1,'2019',2019,1,'L',0),(2,'2019',2019,2,'L',0),(3,'2019',2019,3,'L',0),(4,'2019',2019,4,'L',0),(5,'2019',2019,5,'L',0),(6,'2019',2019,6,'L',0),(7,'2019',2019,7,'L',0),(8,'2019',2019,8,'L',0),(9,'2019',2019,9,'L',0),(10,'2019',2019,10,'L',0),(11,'2019',2019,11,'L',0),(12,'2019',2019,12,'L',0);

/*Table structure for table `mplan` */

DROP TABLE IF EXISTS `mplan`;

CREATE TABLE `mplan` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcAddress` varchar(150) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcState` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `vcTelp` varchar(30) NOT NULL,
  `vcFax` varchar(30) NOT NULL,
  `vcEmail` varchar(100) NOT NULL,
  `blpImage` longblob NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mplan` */

insert  into `mplan`(`intID`,`vcCode`,`vcName`,`vcAddress`,`vcCity`,`vcState`,`vcCountry`,`vcTelp`,`vcFax`,`vcEmail`,`blpImage`,`intDeleted`,`dtInsertTime`) values (1,'MFP','My First Plan','','Sidoarjo','JAWA TIMUR','Indonesia','','','','',0,'0000-00-00 00:00:00');

/*Table structure for table `mprice` */

DROP TABLE IF EXISTS `mprice`;

CREATE TABLE `mprice` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intCategory` int(11) NOT NULL,
  `intItem` int(11) NOT NULL,
  `intPrice` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intCategory_mprice` (`intCategory`),
  KEY `fk_intItem_mprice` (`intItem`),
  CONSTRAINT `fk_intCategory_mprice` FOREIGN KEY (`intCategory`) REFERENCES `mpricecategory` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intItem_mprice` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mprice` */

/*Table structure for table `mpricecategory` */

DROP TABLE IF EXISTS `mpricecategory`;

CREATE TABLE `mpricecategory` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mpricecategory` */

/*Table structure for table `mpriceplan` */

DROP TABLE IF EXISTS `mpriceplan`;

CREATE TABLE `mpriceplan` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPlan` int(11) DEFAULT NULL,
  `intPrice` int(11) DEFAULT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intPlan_mpriceplan` (`intPlan`),
  KEY `fk_intPrice_mpriceplan` (`intPrice`),
  CONSTRAINT `fk_intPlan_mpriceplan` FOREIGN KEY (`intPlan`) REFERENCES `mplan` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_intPrice_mpriceplan` FOREIGN KEY (`intPrice`) REFERENCES `mpricecategory` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mpriceplan` */

/*Table structure for table `mprofile` */

DROP TABLE IF EXISTS `mprofile`;

CREATE TABLE `mprofile` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcAddress` varchar(150) NOT NULL,
  `vcCity` varchar(100) NOT NULL,
  `vcState` varchar(100) NOT NULL,
  `vcCountry` varchar(100) NOT NULL,
  `vcTelp` varchar(30) NOT NULL,
  `vcFax` varchar(30) NOT NULL,
  `vcEmail` varchar(100) NOT NULL,
  `blpImage` longblob NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mprofile` */

insert  into `mprofile`(`intID`,`vcCode`,`vcName`,`vcAddress`,`vcCity`,`vcState`,`vcCountry`,`vcTelp`,`vcFax`,`vcEmail`,`blpImage`,`intDeleted`,`dtInsertTime`) values (1,'MP','My Profile','','Sidoarjo','JAWA TIMUR','Indonesia','','','','',0,'0000-00-00 00:00:00');

/*Table structure for table `msetting` */

DROP TABLE IF EXISTS `msetting`;

CREATE TABLE `msetting` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(200) NOT NULL,
  `vcValue` varchar(1000) NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `msetting` */

insert  into `msetting`(`intID`,`vcCode`,`vcName`,`vcValue`) values (1,'default_cust_for_pos','Default Customer On Pos Menu','Walk In Customer'),(2,'default_whs_for_pos','Default Warehouse On Pos Menu','Trosobo'),(3,'production_area','How do you call your production area','WKS'),(4,'company_name','Company Name','DYKEE.STORE'),(5,'currency','Currency','IDR');

/*Table structure for table `mstock` */

DROP TABLE IF EXISTS `mstock`;

CREATE TABLE `mstock` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intLocation` int(11) DEFAULT NULL,
  `intItem` int(11) DEFAULT NULL,
  `intStock` double NOT NULL DEFAULT '0',
  `intHPP` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`),
  KEY `fk_intLocation_mstock` (`intLocation`),
  KEY `fk_intItem_mstock` (`intItem`),
  CONSTRAINT `fk_intItem_mstock` FOREIGN KEY (`intItem`) REFERENCES `mitem` (`intID`) ON UPDATE CASCADE,
  CONSTRAINT `fl_intLoaction_mstock` FOREIGN KEY (`intLocation`) REFERENCES `mlocation` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mstock` */

/*Table structure for table `msubform` */

DROP TABLE IF EXISTS `msubform`;

CREATE TABLE `msubform` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcCode` varchar(250) NOT NULL DEFAULT '',
  `vcIcon` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL DEFAULT '',
  `intHeader` int(11) NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  KEY `msubform_form` (`intHeader`),
  CONSTRAINT `msubform_form` FOREIGN KEY (`intHeader`) REFERENCES `mform` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `msubform` */

/*Table structure for table `mtable` */

DROP TABLE IF EXISTS `mtable`;

CREATE TABLE `mtable` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mtable` */

/*Table structure for table `mtableplan` */

DROP TABLE IF EXISTS `mtableplan`;

CREATE TABLE `mtableplan` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPlan` int(11) DEFAULT NULL,
  `intTable` int(11) DEFAULT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intPlan_mtableplan` (`intPlan`),
  KEY `fk_intTable_mtableplan` (`intTable`),
  CONSTRAINT `fk_intPlan_mtableplan` FOREIGN KEY (`intPlan`) REFERENCES `mplan` (`intID`),
  CONSTRAINT `fk_intTable_mtableplan` FOREIGN KEY (`intTable`) REFERENCES `mtable` (`intID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mtableplan` */

/*Table structure for table `mtax` */

DROP TABLE IF EXISTS `mtax`;

CREATE TABLE `mtax` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcCode` varchar(100) NOT NULL,
  `vcName` varchar(200) NOT NULL,
  `intRate` double NOT NULL DEFAULT '0',
  `intActive` tinyint(4) NOT NULL DEFAULT '1',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mtax` */

insert  into `mtax`(`intID`,`vcCode`,`vcName`,`intRate`,`intActive`,`intDeleted`) values (1,'PPN0','Non PPN',0,1,0),(2,'PPN10','PPN 10%',10,1,0);

/*Table structure for table `mtopform` */

DROP TABLE IF EXISTS `mtopform`;

CREATE TABLE `mtopform` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcCode` varchar(250) NOT NULL DEFAULT '',
  `vcIcon` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL DEFAULT '',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  `intSort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mtopform` */

insert  into `mtopform`(`intID`,`vcName`,`vcCode`,`vcIcon`,`vcRemarks`,`intDeleted`,`dtInsertTime`,`intSort`) values (1,'Point of Sale','pos','<i class=\"fa fa-desktop\" aria-hidden=\"true\"></i>','-',0,'0000-00-00 00:00:00',0),(2,'Purchase of Goods','pop','<i class=\"fa fa-shopping-basket\" aria-hidden=\"true\"></i>','-',0,'0000-00-00 00:00:00',0),(3,'Kitchen or Production Area','protop','<i class=\"fa fa-industry\" aria-hidden=\"true\"></i>','',0,'0000-00-00 00:00:00',0);

/*Table structure for table `muser` */

DROP TABLE IF EXISTS `muser`;

CREATE TABLE `muser` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intDefaultLoc` int(11) NOT NULL DEFAULT '0',
  `intDefaultBP` int(11) NOT NULL DEFAULT '0',
  `intDefaultWallet` int(11) NOT NULL DEFAULT '0',
  `intValue` tinyint(4) NOT NULL DEFAULT '0',
  `intProfit` tinyint(4) NOT NULL DEFAULT '0',
  `vcIcon` varchar(10) NOT NULL,
  `vcUserID` varchar(100) NOT NULL,
  `vcPassword` varchar(500) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `vcEmail` varchar(250) NOT NULL,
  `vcPhone` varchar(100) NOT NULL,
  `vcGender` varchar(15) NOT NULL,
  `blpImage` mediumblob NOT NULL,
  `dtLastLogin` datetime NOT NULL,
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcUserID` (`vcUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `muser` */

insert  into `muser`(`intID`,`intDefaultLoc`,`intDefaultBP`,`intDefaultWallet`,`intValue`,`intProfit`,`vcIcon`,`vcUserID`,`vcPassword`,`vcName`,`vcEmail`,`vcPhone`,`vcGender`,`blpImage`,`dtLastLogin`,`intDeleted`,`dtInsertTime`) values (1,1,0,1,0,0,'','admin','21232F297A57A5A743894A0E4A801FC3','Gede Ardiasa','gede.ardiasa99@gmail.com','082234860986','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00');

/*Table structure for table `museractivity` */

DROP TABLE IF EXISTS `museractivity`;

CREATE TABLE `museractivity` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `vcUserID` varchar(100) DEFAULT NULL,
  `vcUserName` varchar(100) DEFAULT NULL,
  `vcController` varchar(50) DEFAULT NULL,
  `vcFunction` varchar(50) DEFAULT NULL,
  `vcActivity` varchar(100) DEFAULT NULL,
  `vcKey` varchar(100) DEFAULT NULL,
  `dtInsertTime` datetime DEFAULT NULL,
  PRIMARY KEY (`intID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `museractivity` */

/*Table structure for table `musertoken` */

DROP TABLE IF EXISTS `musertoken`;

CREATE TABLE `musertoken` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) DEFAULT NULL,
  `vcUserID` varchar(100) NOT NULL,
  `vcIP` varchar(100) DEFAULT NULL,
  `vcCompName` varchar(100) DEFAULT NULL,
  `vcToken` varchar(100) DEFAULT NULL,
  `dtInsertTime` datetime NOT NULL,
  PRIMARY KEY (`intID`),
  UNIQUE KEY `vcUserID` (`vcUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `musertoken` */

/*Table structure for table `mwallet` */

DROP TABLE IF EXISTS `mwallet`;

CREATE TABLE `mwallet` (
  `intID` int(11) NOT NULL AUTO_INCREMENT,
  `intPlan` int(11) NOT NULL,
  `vcCode` varchar(50) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `intBalance` double NOT NULL DEFAULT '0',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL,
  `vcGL` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`intID`),
  KEY `fk_intPlan_mwallet` (`intPlan`),
  CONSTRAINT `fk_intPlan_mwallet` FOREIGN KEY (`intPlan`) REFERENCES `mplan` (`intID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mwallet` */

insert  into `mwallet`(`intID`,`intPlan`,`vcCode`,`vcName`,`intBalance`,`intDeleted`,`dtInsertTime`,`vcGL`) values (1,1,'DMPT','Dompet',0,0,'2019-08-08 13:36:45','1000000');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
