-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Mar 2016 pada 04.16
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pgi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `maccess`
--

CREATE TABLE IF NOT EXISTS `maccess` (
`intID` int(11) NOT NULL,
  `intUserID` int(11) DEFAULT NULL,
  `intForm` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data untuk tabel `maccess`
--

INSERT INTO `maccess` (`intID`, `intUserID`, `intForm`) VALUES
(39, 1, 2),
(40, 1, 3),
(41, 1, 8),
(42, 1, 9),
(43, 1, 10),
(44, 1, 11),
(45, 1, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mdepartment`
--

CREATE TABLE IF NOT EXISTS `mdepartment` (
`intID` int(11) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `vcRemarks` varchar(250) NOT NULL,
  `intDeleted` tinyint(4) NOT NULL,
  `dtInsertTime` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `mdepartment`
--

INSERT INTO `mdepartment` (`intID`, `vcName`, `vcRemarks`, `intDeleted`, `dtInsertTime`) VALUES
(1, 'IT', '-', 0, '2016-03-17 09:09:32'),
(3, 'Purchasing', '-', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mform`
--

CREATE TABLE IF NOT EXISTS `mform` (
`intID` int(11) NOT NULL,
  `vcName` varchar(250) NOT NULL DEFAULT '',
  `vcCode` varchar(250) NOT NULL DEFAULT '',
  `vcRemarks` varchar(250) NOT NULL DEFAULT '',
  `intHeader` int(11) NOT NULL DEFAULT '0',
  `IsHeader` tinyint(4) NOT NULL DEFAULT '0',
  `intDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `dtInsertTime` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `mform`
--

INSERT INTO `mform` (`intID`, `vcName`, `vcCode`, `vcRemarks`, `intHeader`, `IsHeader`, `intDeleted`, `dtInsertTime`) VALUES
(1, 'Administration', 'admin', '-', 0, 1, 0, '2016-03-17 09:09:32'),
(2, 'User', 'user', '-', 1, 0, 0, '2016-03-17 09:09:32'),
(3, 'Form', 'form', '-', 1, 0, 0, '2016-03-17 09:09:32'),
(7, 'Report', 'report', '-', 0, 1, 0, '0000-00-00 00:00:00'),
(8, 'Inventory Stock', 'stock', '-', 7, 0, 0, '0000-00-00 00:00:00'),
(9, 'Department', 'department', '-', 1, 0, 0, '0000-00-00 00:00:00'),
(10, 'A/R Invoice', 'arinvoice', '-', 7, 0, 0, '0000-00-00 00:00:00'),
(11, 'A/P Invoice', 'apinvoice', '-', 7, 0, 0, '0000-00-00 00:00:00'),
(12, 'Sales Order', 'so', '-', 7, 0, 0, '0000-00-00 00:00:00'),
(13, 'Sales', 'sales', 'sales', 0, 1, 0, '0000-00-00 00:00:00'),
(14, 'testing', 'testing', 'Testing', 13, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `muser`
--

CREATE TABLE IF NOT EXISTS `muser` (
`intID` int(11) NOT NULL,
  `intMDepartmentID` int(11) NOT NULL,
  `vcUserID` varchar(100) NOT NULL,
  `vcPassword` varchar(500) NOT NULL,
  `vcName` varchar(250) NOT NULL,
  `vcEmail` varchar(250) NOT NULL,
  `vcAddress` varchar(500) NOT NULL,
  `vcPosition` varchar(100) NOT NULL,
  `intDeleted` tinyint(4) NOT NULL,
  `dtInsertTime` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `muser`
--

INSERT INTO `muser` (`intID`, `intMDepartmentID`, `vcUserID`, `vcPassword`, `vcName`, `vcEmail`, `vcAddress`, `vcPosition`, `intDeleted`, `dtInsertTime`) VALUES
(1, 1, 'Gede', 'Gede', 'Gede Ardiasa', 'kun_bero@yahoo.com', 'Jl. Saphire 4 no 13 Graha Bunder Asri', 'IT Staff', 0, '2016-03-17 09:10:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `maccess`
--
ALTER TABLE `maccess`
 ADD PRIMARY KEY (`intID`);

--
-- Indexes for table `mdepartment`
--
ALTER TABLE `mdepartment`
 ADD PRIMARY KEY (`intID`);

--
-- Indexes for table `mform`
--
ALTER TABLE `mform`
 ADD PRIMARY KEY (`intID`);

--
-- Indexes for table `muser`
--
ALTER TABLE `muser`
 ADD PRIMARY KEY (`intID`), ADD UNIQUE KEY `vcUserID` (`vcUserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `maccess`
--
ALTER TABLE `maccess`
MODIFY `intID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `mdepartment`
--
ALTER TABLE `mdepartment`
MODIFY `intID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mform`
--
ALTER TABLE `mform`
MODIFY `intID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `muser`
--
ALTER TABLE `muser`
MODIFY `intID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
