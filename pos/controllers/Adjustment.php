<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjustment extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_adjustment','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemAD']=0;
			unset($_SESSION['itemcodeAD']);
			unset($_SESSION['itemnameAD']);
			unset($_SESSION['qtyAD']);
			unset($_SESSION['uomAD']);
			unset($_SESSION['costAD']);
			
			$data['typetoolbar']='AD';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('adjustment',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_adjustment->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_adjustment->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hAdjustment');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->Whs = $r->intLocation;
			$responce->Type = $r->vcType;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
			
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAdjustment');
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->Whs = '';
			$responce->Type = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemAD']=0;
			unset($_SESSION['itemcodeAD']);
			unset($_SESSION['itemnameAD']);
			unset($_SESSION['qtyAD']);
			unset($_SESSION['uomAD']);
			unset($_SESSION['costAD']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemAD']=0;
			unset($_SESSION['itemcodeAD']);
			unset($_SESSION['itemnameAD']);
			unset($_SESSION['qtyAD']);
			unset($_SESSION['uomAD']);
			unset($_SESSION['costAD']);
			
			$r=$this->m_adjustment->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['itemcodeAD'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAD'][$j]=$d->vcItemName;
				$dintQty=(float)$d->intQty;
				if($dintQty<0)
				{
					$_SESSION['qtyAD'][$j]=$dintQty*-1;
				}
				else
				{
					$_SESSION['qtyAD'][$j]=$dintQty;
				}
				$_SESSION['uomAD'][$j]=$d->vcUoM;
				$_SESSION['costAD'][$j]=$d->intCost;
				$j++;
			}
			$_SESSION['totitemAD']=$j;
		}
	}
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		echo $cost;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekminusD()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$cek=$this->m_stock->cekMinusStock($item,$_POST['detailQty'],$_POST['Whs']);
		echo $cek;
	}
	function cekminusH()
	{
		$hasil=1;
		for($j=0;$j<$_SESSION['totitemAD'];$j++)
		{
			if(isset($_SESSION['itemcodeAD'][$j]))
			{
				if($_SESSION['itemcodeAD'][$j]!='')
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAD'][$j]);
					$cek=$this->m_stock->cekMinusStock($item,$_SESSION['qtyAD'][$j],$_POST['Whs']);
					if($cek==0)
					{
						$hasil=$_SESSION['itemnameAD'][$j];
						break;
					}
				}
			}
		}
		echo $hasil;
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemAD'];$j++)
		{
			if(isset($_SESSION['itemcodeAD'][$j]))
			{
				if($_SESSION['itemcodeAD'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	
	
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_adjustment->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemAD'];$j++)// save detail
			{
				if($_SESSION['itemcodeAD'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAD'][$j]);
					$da['intHID']=$head;
					$da['itemcodeAD']=$_SESSION['itemcodeAD'][$j];
					$da['itemnameAD']=$_SESSION['itemnameAD'][$j];
					$da['Whs']=$data['Whs'];
					if($data['Type']=='GR')
					{
						$da['qtyAD']=$_SESSION['qtyAD'][$j];
					}
					else if($data['Type']=='GI')
					{
						$da['qtyAD']=$_SESSION['qtyAD'][$j]*-1;
					}
					$da['uomAD']=$_SESSION['uomAD'][$j];
					$da['costAD']=$_SESSION['costAD'][$j];
					$detail=$this->m_adjustment->insertD($da);
					$this->m_stock->updateStock($item,$da['qtyAD'],$data['Whs']);//update stok menambah/mengurangi di gudang
					if($data['Type']=='GR')
					{
						$this->m_stock->updateCost($item,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
					}
					$this->m_stock->addMutation($item,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
			$_SESSION['totitemAD']=0;
			unset($_SESSION['itemcodeAD']);
			unset($_SESSION['itemnameAD']);
			unset($_SESSION['qtyAD']);
			unset($_SESSION['uomAD']);
			unset($_SESSION['costAD']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_adjustment->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>Whs</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemAD'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemAD'];$j++)
		{
			if($_SESSION['itemcodeAD'][$j]==$code)
			{
				$_SESSION['qtyAD'][$j]=$_POST['detailQty'];
				$_SESSION['costAD'][$j]=$_POST['detailCost'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeAD'][$i]=$code;
				$_SESSION['itemnameAD'][$i]=$_POST['detailItem'];
				$_SESSION['qtyAD'][$i]=$_POST['detailQty'];
				$_SESSION['costAD'][$i]=$_POST['detailCost'];
				$_SESSION['uomAD'][$i]=$UoM;
				$_SESSION['totitemAD']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemAD'];$j++)
		{
			if($_SESSION['itemcodeAD'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeAD'][$j]="";
				$_SESSION['itemnameAD'][$j]="";
				$_SESSION['qtyAD'][$j]="";
				$_SESSION['uomAD'][$j]="";
				$_SESSION['costAD'][$j]="";
			}
		}
	}
	
	
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
				  <th>Cost</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemAD'];$j++)
			{
				if($_SESSION['itemnameAD'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAD'][$j]);
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeAD'][$j].'</td>
						<td>'.$_SESSION['itemnameAD'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyAD'][$j],'2').'</td>
						<td>'.$_SESSION['uomAD'][$j].'</td>
						<td align="right">'.number_format($_SESSION['costAD'][$j],'2').'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameAD"][$j].'\',\''.$_SESSION["itemcodeAD"][$j].'\',\''.$_SESSION["qtyAD"][$j].'\',\''.$_SESSION["costAD"][$j].'\',\''.$_SESSION["uomAD"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeAD"][$j].'\',\''.$_SESSION["qtyAD"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
