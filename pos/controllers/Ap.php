<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AP extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemAP']=0;
			unset($_SESSION['itemcodeAP']);
			unset($_SESSION['idAP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['itemnameAP']);
			unset($_SESSION['qtyAP']);
			unset($_SESSION['qtyOpenAP']);
			unset($_SESSION['uomAP']);
			unset($_SESSION['priceAP']);
			unset($_SESSION['discAP']);
			unset($_SESSION['whsAP']);
			unset($_SESSION['statusAP']);
			
			$data['typetoolbar']='AP';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('A/P Invoice',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_ap->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listpo']=$this->m_po->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listgrpo']=$this->m_grpo->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			$data['autobp']=$this->m_bp->GetAllDataVendor();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('S');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ap->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hAP');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->DueDate = date('m/d/Y',strtotime($r->dtDueDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$responce->AppliedAmount = $r->intApplied;
			$responce->BalanceDue = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAP');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->DueDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			$responce->AppliedAmount = 0;
			$responce->BalanceDue = 0;
			
		}
		echo json_encode($responce);
	}
	
	function getDataHeaderPO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_po->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAP');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderGRPO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_grpo->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAP');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemAP']=0;
			unset($_SESSION['itemcodeAP']);
			unset($_SESSION['idAP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['itemnameAP']);
			unset($_SESSION['qtyAP']);
			unset($_SESSION['qtyOpenAP']);
			unset($_SESSION['uomAP']);
			unset($_SESSION['priceAP']);
			unset($_SESSION['discAP']);
			unset($_SESSION['whsAP']);
			unset($_SESSION['statusAP']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemAP']=0;
			unset($_SESSION['itemcodeAP']);
			unset($_SESSION['idAP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['itemnameAP']);
			unset($_SESSION['qtyAP']);
			unset($_SESSION['qtyOpenAP']);
			unset($_SESSION['uomAP']);
			unset($_SESSION['priceAP']);
			unset($_SESSION['discAP']);
			unset($_SESSION['whsAP']);
			unset($_SESSION['statusAP']);
			
			$r=$this->m_ap->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idAP'][$j]=$d->intID;
				$_SESSION['BaseRefAP'][$j]='';
				$_SESSION['itemcodeAP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAP'][$j]=$d->vcItemName;
				$_SESSION['qtyAP'][$j]=$d->intQty;
				$_SESSION['qtyOpenAP'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAP'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAP'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAP'][$j]=$d->intPrice;
				$_SESSION['discAP'][$j]=$d->intDiscPer;
				$_SESSION['whsAP'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusAP'][$j]='O';
				}
				else
				{
					$_SESSION['statusAP'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemAP']=$j;
		}
	}
	function loaddetailpo()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAP']=0;
		unset($_SESSION['itemcodeAP']);
		unset($_SESSION['idAP']);
		unset($_SESSION['idBaseRefAP']);
		unset($_SESSION['itemnameAP']);
		unset($_SESSION['qtyAP']);
		unset($_SESSION['qtyOpenAP']);
		unset($_SESSION['uomAP']);
		unset($_SESSION['priceAP']);
		unset($_SESSION['discAP']);
		unset($_SESSION['whsAP']);
		unset($_SESSION['statusAP']);
		
		$r=$this->m_po->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAP'][$j]=$d->intHID;
				$_SESSION['BaseRefAP'][$j]='PO';
				$_SESSION['itemcodeAP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAP'][$j]=$d->vcItemName;
				$_SESSION['qtyAP'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAP'][$j]=$d->intOpenQty;
				
				if($d->intService==0)
				{
					$_SESSION['uomAP'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAP'][$j]=$d->vcUoMInv;
				}
				$_SESSION['priceAP'][$j]=$d->intPrice;
				$_SESSION['discAP'][$j]=$d->intDiscPer;
				$_SESSION['whsAP'][$j]=$d->intLocation;
				$_SESSION['statusAP'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAP']=$j;
	}
	function loaddetailgrpo()
	{
		$id=$_POST['id'];
		$_SESSION['totitemAP']=0;
		unset($_SESSION['itemcodeAP']);
		unset($_SESSION['idAP']);
		unset($_SESSION['idBaseRefAP']);
		unset($_SESSION['itemnameAP']);
		unset($_SESSION['qtyAP']);
		unset($_SESSION['qtyOpenAP']);
		unset($_SESSION['uomAP']);
		unset($_SESSION['priceAP']);
		unset($_SESSION['discAP']);
		unset($_SESSION['whsAP']);
		unset($_SESSION['statusAP']);
		
		$r=$this->m_grpo->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAP'][$j]=$d->intHID;
				$_SESSION['BaseRefAP'][$j]='GRPO';
				$_SESSION['itemcodeAP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAP'][$j]=$d->vcItemName;
				$_SESSION['qtyAP'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAP'][$j]=$d->intOpenQty;
				$_SESSION['uomAP'][$j]=$d->intUoMType;
				$_SESSION['priceAP'][$j]=$d->intPrice;
				$_SESSION['discAP'][$j]=$d->intDiscPer;
				$_SESSION['whsAP'][$j]=$d->intLocation;
				$_SESSION['statusAP'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAP']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemAP'];$j++)
		{
			if(($_SESSION['itemcodeAP'][$j]!='' and $_SESSION['itemcodeAP'][$j]!=null) or ($_SESSION['itemnameAP'][$j]!='' and $_SESSION['itemnameAP'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyAP'][$j]*$_SESSION['priceAP'][$j])-($_SESSION['discAP'][$j]/100*($_SESSION['qtyAP'][$j]*$_SESSION['priceAP'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemAP'];$j++)
		{
			if(isset($_SESSION['itemcodeAP'][$j]) or isset($_SESSION['itemnameAP'][$j]))
			{
				if($_SESSION['itemcodeAP'][$j]!='' or $_SESSION['itemnameAP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		if($_POST['Service']==0)
		{
			$hasil='';
			for($j=0;$j<$_SESSION['totitemAP'];$j++)
			{
				if($_SESSION['itemcodeAP'][$j]!="")
				{
					$item=$this->m_item->GetIDByCode($_SESSION['itemcodeAP'][$j]);
					if($_SESSION['uomAP'][$j]==1)// konversi uom
					{
						$da['qtyinvAP']=$_SESSION['qtyAP'][$j];
					}
					else if($_SESSION['uomAP'][$j]==2)
					{
						$da['qtyinvAP']=$this->m_item->convert_qty($item,$_SESSION['qtyAP'][$j],'intSlsUoM',1);
					}
					else if($_SESSION['uomAP'][$j]==3)
					{
						$da['qtyinvAP']=$this->m_item->convert_qty($item,$_SESSION['qtyAP'][$j],'intPurUoM',1);
					}
					$res=$this->m_stock->cekMinusStock($item,$da['qtyinvAP'],$_SESSION['whsAP'][$j]);
					if($res==1 or $_SESSION['BaseRefAP'][$j]=='GRPO')
					{
						$hasil=$hasil;
					}
					else
					{
						$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeAP'][$j].' - '.$_SESSION['itemnameAP'][$j].') ';
					}
				}
			}
			if($hasil==''){$hasil=1;}
			echo $hasil;
		}
		else
		{
			echo 1;
		}
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemAP'];$j++)
		{
			if($_SESSION['itemcodeAP'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsAP'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekclosePO($id,$item, $itemname, $qty,$qtyinv)
	{
		$this->m_po->cekclose($id,$item,$qty,$qtyinv,$itemname);
	}
	function cekcloseGRPO($id,$item,$qty,$qtyinv)
	{
		$this->m_grpo->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		$data['AmmountTendered'] = 0;
		$data['Change'] = 0;
		$data['PaymentNote'] = '';
		$data['PaymentCode'] = '';
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_ap->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemAP'];$j++)// save detail
			{
				if($_SESSION['itemcodeAP'][$j]!="" or $_SESSION['itemnameAP'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameAP'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameAP'][$j]);
					}
					else
					{
						$item=0;
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsAP'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeAP']=$_SESSION['itemcodeAP'][$j];
					$da['itemnameAP']=$_SESSION['itemnameAP'][$j];
					$da['qtyAP']=$_SESSION['qtyAP'][$j];
					$da['whsAP']=$_SESSION['whsAP'][$j];
					$da['whsNameAP']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discAP'][$j])/100*$_SESSION['priceAP'][$j];
					$hargasetelahdiskon=$hargasetelahdiskon-($data['DiscPer']/100*$hargasetelahdiskon) ;// harga dikurangi discount header
					
					if($_SESSION['uomAP'][$j]==1 and $data['Service']==0)
					{
						$da['uomAP']=$vcUoM->vcUoM;
						$da['qtyinvAP']=$da['qtyAP'];
						$da['costAP']=$hargasetelahdiskon;// jika pilih inventory uom maka cost = harga setelah diskon
					}
					else if($_SESSION['uomAP'][$j]==2 and $data['Service']==0)
					{
						$da['uomAP']=$vcUoM->vcSlsUoM;
						$da['qtyinvAP']=$this->m_item->convert_qty($item,$da['qtyAP'],'intSlsUoM',1);
						$da['costAP'] = $this->m_item->convert_price($item,$hargasetelahdiskon,'intPurUoM','intSlsUoM');
					}
					else if($_SESSION['uomAP'][$j]==3 and $data['Service']==0)
					{
						$da['uomAP']=$vcUoM->vcPurUoM;
						$da['qtyinvAP']=$this->m_item->convert_qty($item,$da['qtyAP'],'intPurUoM',1);
						$da['costAP'] = $this->m_item->convert_price($item,$hargasetelahdiskon, 'intPurUoM' ,1);
					}
					else
					{
						$da['uomAP']   = $_SESSION['uomAP'][$j];
						$da['qtyinvAP']= $da['qtyAP'];
						$da['costAP'] = $hargasetelahdiskon;
					}
					
					if(isset($_SESSION['idBaseRefAP'][$j]))
					{
						$da['idBaseRefAP']=$_SESSION['idBaseRefAP'][$j];
						$da['BaseRefAP']=$_SESSION['BaseRefAP'][$j];
					}
					else
					{
						$da['idBaseRefAP']=0;
						$da['BaseRefAP']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefAP']=='PO')
					{
						$this->cekclosePO($da['idBaseRefAP'], $da['itemID'], $da['itemnameAP'], $da['qtyAP'], $da['qtyinvAP']);
					}
					else if($da['BaseRefAP']=='GRPO')
					{
						$this->cekcloseGRPO($da['idBaseRefAP'], $da['itemID'], $da['qtyAP'], $da['qtyinvAP']);
					}
					$da['uomtypeAP']=$_SESSION['uomAP'][$j];
					if($data['Service']==0)
					{
						$da['uominvAP']=$vcUoM->vcUoM;
					}
					else
					{
						$da['uominvAP'] = $da['uomAP'];
					}
					
					
					$da['priceAP']= $_SESSION['priceAP'][$j];
					$da['discperAP'] = $_SESSION['discAP'][$j];
					$da['discAP'] = $_SESSION['discAP'][$j]/100*$da['priceAP'];
					$da['priceafterAP']=(100-$_SESSION['discAP'][$j])/100*$da['priceAP'];
					$da['linetotalAP']= $da['priceafterAP']*$da['qtyAP'];
					$da['linecostAP']=$da['costAP']*$da['qtyinvAP'];
					
					$detail=$this->m_ap->insertD($da);
					if($da['BaseRefAP']!='GRPO' and $data['Service']==0)
					{
						$this->m_stock->updateStock($item,$da['qtyinvAP'],$da['whsAP']);//update stok menambah/mengurangi di gudang
						$this->m_stock->updateCost($item,$da['qtyinvAP'],$da['costAP'],$da['whsAP']);//update cost
						$this->m_stock->addMutation($item,$da['qtyinvAP'],$da['costAP'],$da['whsAP'],'AP',$data['DocDate'],$data['DocNum']);//add mutation
					}
					
				}
			}
			$_SESSION['totitemAP']=0;
			unset($_SESSION['itemcodeAP']);
			unset($_SESSION['idAP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['itemnameAP']);
			unset($_SESSION['qtyAP']);
			unset($_SESSION['qtyOpenAP']);
			unset($_SESSION['uomAP']);
			unset($_SESSION['priceAP']);
			unset($_SESSION['discAP']);
			unset($_SESSION['whsAP']);
			unset($_SESSION['statusAP']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_ap->editH($data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		echo 1;
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_ap->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_ap->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemAP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemAP'];$j++)
		{
			if($_SESSION['itemcodeAP'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyAP'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAP'][$j]=$_POST['detailQty'];
				$_SESSION['uomAP'][$j]=$_POST['detailUoM'];
				$_SESSION['priceAP'][$j]=$_POST['detailPrice'];
				$_SESSION['discAP'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAP'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAP'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameAP'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyAP'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAP'][$j]=$_POST['detailQty'];
				$_SESSION['uomAP'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceAP'][$j]=$_POST['detailPrice'];
				$_SESSION['discAP'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAP'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAP'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeAP'][$i]=$code;
				$_SESSION['itemnameAP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyAP'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenAP'][$i]=$_POST['detailQty'];
				$_SESSION['uomAP'][$i]=$_POST['detailUoM'];
				$_SESSION['priceAP'][$i]=$_POST['detailPrice'];
				$_SESSION['discAP'][$i]=$_POST['detailDisc'];
				$_SESSION['whsAP'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefAP'][$i]='';
				$_SESSION['statusAP'][$i]='O';
				$_SESSION['totitemAP']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeAP'][$i]='';
					$_SESSION['itemnameAP'][$i]=$_POST['detailItem'];
					$_SESSION['qtyAP'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenAP'][$i]=$_POST['detailQty'];
					$_SESSION['uomAP'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceAP'][$i]=$_POST['detailPrice'];
					$_SESSION['discAP'][$i]=$_POST['detailDisc'];
					$_SESSION['whsAP'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefAP'][$i]='';
					$_SESSION['statusAP'][$i]='O';
					$_SESSION['totitemAP']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemAP'];$j++)
		{
			if($_SESSION['itemcodeAP'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeAP'][$j]="";
				$_SESSION['itemnameAP'][$j]="";
				$_SESSION['qtyAP'][$j]="";
				$_SESSION['qtyOpenAP'][$j]="";
				$_SESSION['uomAP'][$j]="";
				$_SESSION['priceAP'][$j]="";
				$_SESSION['discAP'][$j]="";
				$_SESSION['whsAP'][$j]="";
				$_SESSION['BaseRefAP'][$j]='';
			}
			if($_SESSION['itemnameAP'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeAP'][$j]="";
				$_SESSION['itemnameAP'][$j]="";
				$_SESSION['qtyAP'][$j]="";
				$_SESSION['qtyOpenAP'][$j]="";
				$_SESSION['uomAP'][$j]="";
				$_SESSION['priceAP'][$j]="";
				$_SESSION['discAP'][$j]="";
				$_SESSION['whsAP'][$j]="";
				$_SESSION['BaseRefAP'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemAP'];$j++)
			{
				if($_SESSION['itemnameAP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAP'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameAP'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsAP'][$j]);
					
					if($_SESSION['uomAP'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomAP'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomAP'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomAP'][$j];
					}
					
					if($_SESSION['discAP'][$j]=='')
					{
						$_SESSION['discAP'][$j]=0;
					}
					
					if($_SESSION['statusAP'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discAP'][$j])/100)*$_SESSION['priceAP'][$j]*$_SESSION['qtyAP'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeAP'][$j].'</td>
						<td>'.$_SESSION['itemnameAP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyAP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenAP'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceAP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discAP'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusAP'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameAP"][$j].'\',\''.$_SESSION["itemcodeAP"][$j].'\',\''.$_SESSION["qtyAP"][$j].'\',\''.$_SESSION["uomAP"][$j].'\',\''.$viewUoM.'\',\''.$_SESSION["priceAP"][$j].'\',\''.$_SESSION["discAP"][$j].'\',\''.$_SESSION["whsAP"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeAP"][$j].'\',\''.$_SESSION["itemnameAP"][$j].'\',\''.$_SESSION["qtyAP"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
