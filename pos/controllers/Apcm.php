<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class APCM extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_apcm','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_pr','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemAPCM']=0;
			unset($_SESSION['itemcodeAPCM']);
			unset($_SESSION['idAPCM']);
			unset($_SESSION['idBaseRefAPCM']);
			unset($_SESSION['itemnameAPCM']);
			unset($_SESSION['qtyAPCM']);
			unset($_SESSION['qtyOpenAPCM']);
			unset($_SESSION['uomAPCM']);
			unset($_SESSION['priceAPCM']);
			unset($_SESSION['discAPCM']);
			unset($_SESSION['whsAPCM']);
			unset($_SESSION['statusAPCM']);
			
			$data['typetoolbar']='APCM';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('A/P Credit Memo',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_apcm->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listap']=$this->m_ap->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listpr']=$this->m_pr->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			$data['autobp']=$this->m_bp->GetAllDataVendor();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('S');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_apcm->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hAPCM');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->DueDate = date('m/d/Y',strtotime($r->dtDueDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$responce->AppliedAmount = $r->intApplied;
			$responce->BalanceDue = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAPCM');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->DueDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			$responce->AppliedAmount = 0;
			$responce->BalanceDue = 0;
			
		}
		echo json_encode($responce);
	}
	
	function getDataHeaderAP()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ap->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAPCM');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderPR()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_pr->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAPCM');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemAPCM']=0;
			unset($_SESSION['itemcodeAPCM']);
			unset($_SESSION['idAPCM']);
			unset($_SESSION['idBaseRefAPCM']);
			unset($_SESSION['itemnameAPCM']);
			unset($_SESSION['qtyAPCM']);
			unset($_SESSION['qtyOpenAPCM']);
			unset($_SESSION['uomAPCM']);
			unset($_SESSION['priceAPCM']);
			unset($_SESSION['discAPCM']);
			unset($_SESSION['whsAPCM']);
			unset($_SESSION['statusAPCM']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemAPCM']=0;
			unset($_SESSION['itemcodeAPCM']);
			unset($_SESSION['idAPCM']);
			unset($_SESSION['idBaseRefAPCM']);
			unset($_SESSION['itemnameAPCM']);
			unset($_SESSION['qtyAPCM']);
			unset($_SESSION['qtyOpenAPCM']);
			unset($_SESSION['uomAPCM']);
			unset($_SESSION['priceAPCM']);
			unset($_SESSION['discAPCM']);
			unset($_SESSION['whsAPCM']);
			unset($_SESSION['statusAPCM']);
			
			$r=$this->m_apcm->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idAPCM'][$j]=$d->intID;
				$_SESSION['BaseRefAPCM'][$j]='';
				$_SESSION['itemcodeAPCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAPCM'][$j]=$d->vcItemName;
				$_SESSION['qtyAPCM'][$j]=$d->intQty;
				$_SESSION['qtyOpenAPCM'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAPCM'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAPCM'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAPCM'][$j]=$d->intPrice;
				$_SESSION['discAPCM'][$j]=$d->intDiscPer;
				$_SESSION['whsAPCM'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusAPCM'][$j]='O';
				}
				else
				{
					$_SESSION['statusAPCM'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemAPCM']=$j;
		}
	}
	
	function loaddetailap()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAPCM']=0;
		unset($_SESSION['itemcodeAPCM']);
		unset($_SESSION['idAPCM']);
		unset($_SESSION['idBaseRefAPCM']);
		unset($_SESSION['itemnameAPCM']);
		unset($_SESSION['qtyAPCM']);
		unset($_SESSION['qtyOpenAPCM']);
		unset($_SESSION['uomAPCM']);
		unset($_SESSION['priceAPCM']);
		unset($_SESSION['discAPCM']);
		unset($_SESSION['whsAPCM']);
		unset($_SESSION['statusAPCM']);
		
		$r=$this->m_ap->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAPCM'][$j]=$d->intHID;
				$_SESSION['BaseRefAPCM'][$j]='AP';
				$_SESSION['itemcodeAPCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAPCM'][$j]=$d->vcItemName;
				$_SESSION['qtyAPCM'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAPCM'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAPCM'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAPCM'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAPCM'][$j]=$d->intPrice;
				$_SESSION['discAPCM'][$j]=$d->intDiscPer;
				$_SESSION['whsAPCM'][$j]=$d->intLocation;
				$_SESSION['statusAPCM'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAPCM']=$j;
	}
	function loaddetailpr()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAPCM']=0;
		unset($_SESSION['itemcodeAPCM']);
		unset($_SESSION['idAPCM']);
		unset($_SESSION['idBaseRefAPCM']);
		unset($_SESSION['itemnameAPCM']);
		unset($_SESSION['qtyAPCM']);
		unset($_SESSION['qtyOpenAPCM']);
		unset($_SESSION['uomAPCM']);
		unset($_SESSION['priceAPCM']);
		unset($_SESSION['discAPCM']);
		unset($_SESSION['whsAPCM']);
		unset($_SESSION['statusAPCM']);
		
		$r=$this->m_pr->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAPCM'][$j]=$d->intHID;
				$_SESSION['BaseRefAPCM'][$j]='PR';
				$_SESSION['itemcodeAPCM'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAPCM'][$j]=$d->vcItemName;
				$_SESSION['qtyAPCM'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAPCM'][$j]=$d->intOpenQty;
				$_SESSION['uomAPCM'][$j]=$d->intUoMType;
				$_SESSION['priceAPCM'][$j]=$d->intPrice;
				$_SESSION['discAPCM'][$j]=$d->intDiscPer;
				$_SESSION['whsAPCM'][$j]=$d->intLocation;
				$_SESSION['statusAPCM'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAPCM']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
		{
			if(($_SESSION['itemcodeAPCM'][$j]!='' and $_SESSION['itemcodeAPCM'][$j]!=null) or ($_SESSION['itemnameAPCM'][$j]!='' and $_SESSION['itemnameAPCM'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyAPCM'][$j]*$_SESSION['priceAPCM'][$j])-($_SESSION['discAPCM'][$j]/100*($_SESSION['qtyAPCM'][$j]*$_SESSION['priceAPCM'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
		{
			if(isset($_SESSION['itemcodeAPCM'][$j]) or isset($_SESSION['itemnameAPCM'][$j]))
			{
				if($_SESSION['itemcodeAPCM'][$j]!='' or $_SESSION['itemnameAPCM'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		if($_POST['Service']==0)
		{
			$hasil='';
			for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
			{
				if($_SESSION['itemcodeAPCM'][$j]!="")
				{
					$item=$this->m_item->GetIDByCode($_SESSION['itemcodeAPCM'][$j]);
					if($_SESSION['uomAPCM'][$j]==1)// konversi uom
					{
						$da['qtyinvAPCM']=$_SESSION['qtyAPCM'][$j];
					}
					else if($_SESSION['uomAPCM'][$j]==2)
					{
						$da['qtyinvAPCM']=$this->m_item->convert_qty($item,$_SESSION['qtyAPCM'][$j],'intSlsUoM',1);
					}
					else if($_SESSION['uomAPCM'][$j]==3)
					{
						$da['qtyinvAPCM']=$this->m_item->convert_qty($item,$_SESSION['qtyAPCM'][$j],'intPurUoM',1);
					}
					$res=$this->m_stock->cekMinusStock($item,$da['qtyinvAPCM'],$_SESSION['whsAPCM'][$j]);
					if($res==1 or $_SESSION['BaseRefAPCM'][$j]=='PR') // jika kasil cek stok ok atau baseref = PR tidak perlu cek stok
					{
						$hasil=$hasil;
					}
					else
					{
						$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeAPCM'][$j].' - '.$_SESSION['itemnameAPCM'][$j].')'.' '.$da['qtyinvAPCM'].''."".$res." ";
					}
				}
			}
			if($hasil==''){$hasil=1;}
			echo $hasil;
		}
		else
		{
			echo 1;
		}
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
		{
			if($_SESSION['itemcodeAPCM'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsAPCM'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	
	function cekcloseAP($id,$item,$qty,$qtyinv)
	{
		$this->m_ap->cekclose2($id,$item,$qty,$qtyinv);
	}
	function cekcloseAPCM($id,$item,$qty,$qtyinv)
	{
		$this->m_apcm->cekclose2($id,$item,$qty,$qtyinv);
	}
	function cekclosePR($id,$item,$qty,$qtyinv)
	{
		$this->m_pr->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		$data['AmmountTendered'] = 0;
		$data['Change'] = 0;
		$data['PaymentNote'] = '';
		$data['PaymentCode'] = '';
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_apcm->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemAPCM'];$j++)// save detail
			{
				if($_SESSION['itemcodeAPCM'][$j]!="" or $_SESSION['itemnameAPCM'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameAPCM'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameAPCM'][$j]);
					}
					else
					{
						$item=0;
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsAPCM'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeAPCM']=$_SESSION['itemcodeAPCM'][$j];
					$da['itemnameAPCM']=$_SESSION['itemnameAPCM'][$j];
					$da['qtyAPCM']=$_SESSION['qtyAPCM'][$j];
					$da['whsAPCM']=$_SESSION['whsAPCM'][$j];
					$da['whsNameAPCM']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discAPCM'][$j])/100*$_SESSION['priceAPCM'][$j];
					
					if($_SESSION['uomAPCM'][$j]==1 and $data['Service']==0)
					{
						$da['uomAPCM']=$vcUoM->vcUoM;
						$da['qtyinvAPCM']=$da['qtyAPCM'];
					}
					else if($_SESSION['uomAPCM'][$j]==2 and $data['Service']==0)
					{
						$da['uomAPCM']=$vcUoM->vcSlsUoM;
						$da['qtyinvAPCM']=$this->m_item->convert_qty($item,$da['qtyAPCM'],'intSlsUoM',1);
					}
					else if($_SESSION['uomAPCM'][$j]==3 and $data['Service']==0)
					{
						$da['uomAPCM']=$vcUoM->vcPurUoM;
						$da['qtyinvAPCM']=$this->m_item->convert_qty($item,$da['qtyAPCM'],'intPurUoM',1);
					}
					else
					{
						$da['uomAPCM']   = $_SESSION['uomAPCM'][$j];
						$da['qtyinvAPCM']= $da['qtyAPCM'];
					}
					
					if(isset($_SESSION['idBaseRefAPCM'][$j]))
					{
						$da['idBaseRefAPCM']=$_SESSION['idBaseRefAPCM'][$j];
						$da['BaseRefAPCM']=$_SESSION['BaseRefAPCM'][$j];
					}
					else
					{
						$da['idBaseRefAPCM']=0;
						$da['BaseRefAPCM']='';
					}
					
					//fungsi cek close status dokumen referensinya
					/*if($da['BaseRefAPCM']=='AP')
					{
						$this->cekcloseAP($da['idBaseRefAPCM'], $da['itemID'], $da['qtyAPCM'], $da['qtyinvAPCM']);
						
					}
					else */if($da['BaseRefAPCM']=='PR')
					{
						$this->cekclosePR($da['idBaseRefAPCM'], $da['itemID'], $da['qtyAPCM'], $da['qtyinvAPCM']);
					}
					$da['uomtypeAPCM']=$_SESSION['uomAPCM'][$j];
					if($data['Service']==0)
					{
						$da['uominvAPCM']=$vcUoM->vcUoM;
						$da['costAPCM']=$this->m_stock->GetCostItem($item,$da['whsAPCM']);
					}
					else
					{
						$da['uominvAPCM'] = $da['uomAPCM'];
						$da['costAPCM']=$hargasetelahdiskon;
					}
					
					
					
					$da['priceAPCM']= $_SESSION['priceAPCM'][$j];
					$da['discperAPCM'] = $_SESSION['discAPCM'][$j];
					$da['discAPCM'] = $_SESSION['discAPCM'][$j]/100*$da['priceAPCM'];
					$da['priceafterAPCM']=(100-$_SESSION['discAPCM'][$j])/100*$da['priceAPCM'];
					$da['linetotalAPCM']= $da['priceafterAPCM']*$da['qtyAPCM'];
					$da['linecostAPCM']=$da['costAPCM']*$da['qtyinvAPCM'];
					
					$detail=$this->m_apcm->insertD($da);
					$da['qtyinvAPCM']=$da['qtyinvAPCM']*-1;
					if($da['BaseRefAPCM']!='PR' and $data['Service']==0)
					{
						$this->m_stock->updateStock($item,$da['qtyinvAPCM'],$da['whsAPCM']);//update stok menambah/mengurangi di gudang
						$this->m_stock->addMutation($item,$da['qtyinvAPCM'],$da['costAPCM'],$da['whsAPCM'],'APCM',$data['DocDate'],$data['DocNum']);//add mutation
					}
					/*if($da['BaseRefAPCM']=='AP')
					{
						$this->cekcloseAPCM($head, $da['itemID'], $da['qtyAPCM'], $da['qtyinvAPCM']); // jika base ref ap maka apcm langsung close
					}*/
					
				}
			}
			$_SESSION['totitemAPCM']=0;
			unset($_SESSION['itemcodeAPCM']);
			unset($_SESSION['idAPCM']);
			unset($_SESSION['idBaseRefAPCM']);
			unset($_SESSION['itemnameAPCM']);
			unset($_SESSION['qtyAPCM']);
			unset($_SESSION['qtyOpenAPCM']);
			unset($_SESSION['uomAPCM']);
			unset($_SESSION['priceAPCM']);
			unset($_SESSION['discAPCM']);
			unset($_SESSION['whsAPCM']);
			unset($_SESSION['statusAPCM']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_apcm->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_apcm->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_apcm->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemAPCM'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
		{
			if($_SESSION['itemcodeAPCM'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyAPCM'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAPCM'][$j]=$_POST['detailQty'];
				$_SESSION['uomAPCM'][$j]=$_POST['detailUoM'];
				$_SESSION['priceAPCM'][$j]=$_POST['detailPrice'];
				$_SESSION['discAPCM'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAPCM'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAPCM'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameAPCM'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyAPCM'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAPCM'][$j]=$_POST['detailQty'];
				$_SESSION['uomAPCM'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceAPCM'][$j]=$_POST['detailPrice'];
				$_SESSION['discAPCM'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAPCM'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAPCM'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeAPCM'][$i]=$code;
				$_SESSION['itemnameAPCM'][$i]=$_POST['detailItem'];
				$_SESSION['qtyAPCM'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenAPCM'][$i]=$_POST['detailQty'];
				$_SESSION['uomAPCM'][$i]=$_POST['detailUoM'];
				$_SESSION['priceAPCM'][$i]=$_POST['detailPrice'];
				$_SESSION['discAPCM'][$i]=$_POST['detailDisc'];
				$_SESSION['whsAPCM'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefAPCM'][$i]='';
				$_SESSION['statusAPCM'][$i]='O';
				$_SESSION['totitemAPCM']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeAPCM'][$i]='';
					$_SESSION['itemnameAPCM'][$i]=$_POST['detailItem'];
					$_SESSION['qtyAPCM'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenAPCM'][$i]=$_POST['detailQty'];
					$_SESSION['uomAPCM'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceAPCM'][$i]=$_POST['detailPrice'];
					$_SESSION['discAPCM'][$i]=$_POST['detailDisc'];
					$_SESSION['whsAPCM'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefAPCM'][$i]='';
					$_SESSION['statusAPCM'][$i]='O';
					$_SESSION['totitemAPCM']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
		{
			if($_SESSION['itemcodeAPCM'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeAPCM'][$j]="";
				$_SESSION['itemnameAPCM'][$j]="";
				$_SESSION['qtyAPCM'][$j]="";
				$_SESSION['qtyOpenAPCM'][$j]="";
				$_SESSION['uomAPCM'][$j]="";
				$_SESSION['priceAPCM'][$j]="";
				$_SESSION['discAPCM'][$j]="";
				$_SESSION['whsAPCM'][$j]="";
				$_SESSION['BaseRefAPCM'][$j]='';
			}
			if($_SESSION['itemnameAPCM'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeAPCM'][$j]="";
				$_SESSION['itemnameAPCM'][$j]="";
				$_SESSION['qtyAPCM'][$j]="";
				$_SESSION['qtyOpenAPCM'][$j]="";
				$_SESSION['uomAPCM'][$j]="";
				$_SESSION['priceAPCM'][$j]="";
				$_SESSION['discAPCM'][$j]="";
				$_SESSION['whsAPCM'][$j]="";
				$_SESSION['BaseRefAPCM'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemAPCM'];$j++)
			{
				if($_SESSION['itemnameAPCM'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAPCM'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameAPCM'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsAPCM'][$j]);
					
					if($_SESSION['uomAPCM'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomAPCM'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomAPCM'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomAPCM'][$j];
					}
					
					if($_SESSION['discAPCM'][$j]=='')
					{
						$_SESSION['discAPCM'][$j]=0;
					}
					
					if($_SESSION['statusAPCM'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discAPCM'][$j])/100)*$_SESSION['priceAPCM'][$j]*$_SESSION['qtyAPCM'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeAPCM'][$j].'</td>
						<td>'.$_SESSION['itemnameAPCM'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyAPCM'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenAPCM'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceAPCM'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discAPCM'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusAPCM'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameAPCM"][$j].'\',\''.$_SESSION["itemcodeAPCM"][$j].'\',\''.$_SESSION["qtyAPCM"][$j].'\',\''.$_SESSION["uomAPCM"][$j].'\',\''.$viewUoM.'\',\''.$_SESSION["priceAPCM"][$j].'\',\''.$_SESSION["discAPCM"][$j].'\',\''.$_SESSION["whsAPCM"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeAPCM"][$j].'\',\''.$_SESSION["itemnameAPCM"][$j].'\',\''.$_SESSION["qtyAPCM"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
