<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ar extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
		
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			
			$data['typetoolbar']='AR';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('A/R Invoice',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_ar->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listso']=$this->m_so->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listdn']=$this->m_dn->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ar->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->DueDate = date('m/d/Y',strtotime($r->dtDueDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$responce->AppliedAmount = $r->intApplied;
			$responce->BalanceDue = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->DueDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			$responce->AppliedAmount = 0;
			$responce->BalanceDue = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSQ()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	function getDataHeaderSO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderDN()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_dn->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hAR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			
			$r=$this->m_ar->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idAR'][$j]=$d->intID;
				$_SESSION['BaseRefAR'][$j]='';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusAR'][$j]='O';
				}
				else
				{
					$_SESSION['statusAR'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemAR']=$j;
		}
	}
	function loaddetailsq()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		
		$r=$this->m_sq->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='SQ';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loaddetailso()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		
		$r=$this->m_so->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='SO';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomAR'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomAR'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loaddetaildn()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemAR']=0;
		unset($_SESSION['itemcodeAR']);
		unset($_SESSION['idAR']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['itemnameAR']);
		unset($_SESSION['qtyAR']);
		unset($_SESSION['qtyOpenAR']);
		unset($_SESSION['uomAR']);
		unset($_SESSION['priceAR']);
		unset($_SESSION['discAR']);
		unset($_SESSION['whsAR']);
		unset($_SESSION['statusAR']);
		
		$r=$this->m_dn->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefAR'][$j]=$d->intHID;
				$_SESSION['BaseRefAR'][$j]='DN';
				$_SESSION['itemcodeAR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameAR'][$j]=$d->vcItemName;
				$_SESSION['qtyAR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenAR'][$j]=$d->intOpenQty;
				$_SESSION['uomAR'][$j]=$d->intUoMType;
				$_SESSION['priceAR'][$j]=$d->intPrice;
				$_SESSION['discAR'][$j]=$d->intDiscPer;
				$_SESSION['whsAR'][$j]=$d->intLocation;
				$_SESSION['statusAR'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemAR']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if(($_SESSION['itemcodeAR'][$j]!='' and $_SESSION['itemcodeAR'][$j]!=null) or ($_SESSION['itemnameAR'][$j]!='' and $_SESSION['itemnameAR'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyAR'][$j]*$_SESSION['priceAR'][$j])-($_SESSION['discAR'][$j]/100*($_SESSION['qtyAR'][$j]*$_SESSION['priceAR'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if(isset($_SESSION['itemcodeAR'][$j]) or isset($_SESSION['itemnameAR'][$j]))
			{
				if($_SESSION['itemcodeAR'][$j]!='' or $_SESSION['itemnameAR'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$hasil='';
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeAR'][$j]);
				if($_SESSION['uomAR'][$j]==1)// konversi uom
				{
					$da['qtyinvAR']=$_SESSION['qtyAR'][$j];
				}
				else if($_SESSION['uomAR'][$j]==2)
				{
					$da['qtyinvAR']=$this->m_item->convert_qty($item,$_SESSION['qtyAR'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomAR'][$j]==3)
				{
					$da['qtyinvAR']=$this->m_item->convert_qty($item,$_SESSION['qtyAR'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvAR'],$_SESSION['whsAR'][$j]);
				if($res==1 or $_SESSION['BaseRefAR'][$j]=='DN')
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeAR'][$j].' - '.$_SESSION['itemnameAR'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsAR'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSQ($id,$item,$qty,$qtyinv)
	{
		$this->m_sq->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseSO($id,$item,$qty,$qtyinv)
	{
		$this->m_so->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseDN($id,$item,$qty,$qtyinv)
	{
		$this->m_dn->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		$data['AmmountTendered'] = 0;
		$data['Change'] = 0;
		$data['PaymentNote'] = '';
		$data['PaymentCode'] = '';
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_ar->insertH($data);
		//$this->m_bp->updateBDO($data['BPId'],'intBalance',$data['DocTotal']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemAR'];$j++)// save detail
			{
				if($_SESSION['itemcodeAR'][$j]!="" or $_SESSION['itemnameAR'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameAR'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameAR'][$j]);
					}
					else
					{
						$item=0;
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsAR'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeAR']=$_SESSION['itemcodeAR'][$j];
					$da['itemnameAR']=$_SESSION['itemnameAR'][$j];
					$da['qtyAR']=$_SESSION['qtyAR'][$j];
					$da['whsAR']=$_SESSION['whsAR'][$j];
					$da['whsNameAR']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discAR'][$j])/100*$_SESSION['priceAR'][$j];
					
					if($_SESSION['uomAR'][$j]==1 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcUoM;
						$da['qtyinvAR']=$da['qtyAR'];
					}
					else if($_SESSION['uomAR'][$j]==2 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcSlsUoM;
						$da['qtyinvAR']=$this->m_item->convert_qty($item,$da['qtyAR'],'intSlsUoM',1);
					}
					else if($_SESSION['uomAR'][$j]==3 and $data['Service']==0)
					{
						$da['uomAR']=$vcUoM->vcPurUoM;
						$da['qtyinvAR']=$this->m_item->convert_qty($item,$da['qtyAR'],'intPurUoM',1);
					}
					else
					{
						$da['uomAR']   = $_SESSION['uomAR'][$j];
						$da['qtyinvAR']= $da['qtyAR'];
					}
					
					if(isset($_SESSION['idBaseRefAR'][$j]))
					{
						$da['idBaseRefAR']=$_SESSION['idBaseRefAR'][$j];
						$da['BaseRefAR']=$_SESSION['BaseRefAR'][$j];
					}
					else
					{
						$da['idBaseRefAR']=0;
						$da['BaseRefAR']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefAR']=='SQ')
					{
						$this->cekcloseSQ($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					else if($da['BaseRefAR']=='SO')
					{
						$this->cekcloseSO($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					else if($da['BaseRefAR']=='DN')
					{
						$this->cekcloseDN($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					
					$da['uomtypeAR']=$_SESSION['uomAR'][$j];
					
					if($data['Service']==0)
					{
						$da['uominvAR']=$vcUoM->vcUoM;
						$da['costAR']=$this->m_stock->GetCostItem($item,$da['whsAR']);
					}
					else
					{
						$da['uominvAR'] = $da['uomAR'];
						$da['costAR']=0;// jika service maka tidak ada cost yang terbentuk
					}
					
					
					$da['priceAR']= $_SESSION['priceAR'][$j];
					$da['discperAR'] = $_SESSION['discAR'][$j];
					$da['discAR'] = $_SESSION['discAR'][$j]/100*$da['priceAR'];
					$da['priceafterAR']=(100-$_SESSION['discAR'][$j])/100*$da['priceAR'];
					$da['linetotalAR']= $da['priceafterAR']*$da['qtyAR'];
					$da['linecostAR']=$da['costAR']*$da['qtyinvAR'];
					
					$detail=$this->m_ar->insertD($da);
					$da['qtyinvAR']=$da['qtyinvAR']*-1;
					if($da['BaseRefAR']!='DN' and $data['Service']==0)
					{
						$this->m_stock->updateStock($item,$da['qtyinvAR'],$da['whsAR']);//update stok menambah/mengurangi di gudang
						$this->m_stock->addMutation($item,$da['qtyinvAR'],$da['costAR'],$da['whsAR'],'AR',$data['DocDate'],$data['DocNum']);//add mutation
					}
				}
			}
			$_SESSION['totitemAR']=0;
			unset($_SESSION['itemcodeAR']);
			unset($_SESSION['idAR']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['itemnameAR']);
			unset($_SESSION['qtyAR']);
			unset($_SESSION['qtyOpenAR']);
			unset($_SESSION['uomAR']);
			unset($_SESSION['priceAR']);
			unset($_SESSION['discAR']);
			unset($_SESSION['whsAR']);
			unset($_SESSION['statusAR']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_ar->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_ar->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_ar->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemAR'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyAR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$j]=$_POST['detailQty'];
				$_SESSION['uomAR'][$j]=$_POST['detailUoM'];
				$_SESSION['priceAR'][$j]=$_POST['detailPrice'];
				$_SESSION['discAR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAR'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameAR'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyAR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$j]=$_POST['detailQty'];
				$_SESSION['uomAR'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceAR'][$j]=$_POST['detailPrice'];
				$_SESSION['discAR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefAR'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeAR'][$i]=$code;
				$_SESSION['itemnameAR'][$i]=$_POST['detailItem'];
				$_SESSION['qtyAR'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenAR'][$i]=$_POST['detailQty'];
				$_SESSION['uomAR'][$i]=$_POST['detailUoM'];
				$_SESSION['priceAR'][$i]=$_POST['detailPrice'];
				$_SESSION['discAR'][$i]=$_POST['detailDisc'];
				$_SESSION['whsAR'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefAR'][$i]='';
				$_SESSION['statusAR'][$i]='O';
				$_SESSION['totitemAR']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeAR'][$i]='';
					$_SESSION['itemnameAR'][$i]=$_POST['detailItem'];
					$_SESSION['qtyAR'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenAR'][$i]=$_POST['detailQty'];
					$_SESSION['uomAR'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceAR'][$i]=$_POST['detailPrice'];
					$_SESSION['discAR'][$i]=$_POST['detailDisc'];
					$_SESSION['whsAR'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefAR'][$i]='';
					$_SESSION['statusAR'][$i]='O';
					$_SESSION['totitemAR']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemAR'];$j++)
		{
			if($_SESSION['itemcodeAR'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeAR'][$j]="";
				$_SESSION['itemnameAR'][$j]="";
				$_SESSION['qtyAR'][$j]="";
				$_SESSION['qtyOpenAR'][$j]="";
				$_SESSION['uomAR'][$j]="";
				$_SESSION['priceAR'][$j]="";
				$_SESSION['discAR'][$j]="";
				$_SESSION['whsAR'][$j]="";
				$_SESSION['BaseRefAR'][$j]='';
			}
			if($_SESSION['itemnameAR'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeAR'][$j]="";
				$_SESSION['itemnameAR'][$j]="";
				$_SESSION['qtyAR'][$j]="";
				$_SESSION['qtyOpenAR'][$j]="";
				$_SESSION['uomAR'][$j]="";
				$_SESSION['priceAR'][$j]="";
				$_SESSION['discAR'][$j]="";
				$_SESSION['whsAR'][$j]="";
				$_SESSION['BaseRefAR'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemAR'];$j++)
			{
				if($_SESSION['itemnameAR'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameAR'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameAR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsAR'][$j]);
					
					if($_SESSION['uomAR'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomAR'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomAR'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomAR'][$j];
					}
					
					if($_SESSION['discAR'][$j]=='')
					{
						$_SESSION['discAR'][$j]=0;
					}
					
					if($_SESSION['statusAR'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discAR'][$j])/100)*$_SESSION['priceAR'][$j]*$_SESSION['qtyAR'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeAR'][$j].'</td>
						<td>'.$_SESSION['itemnameAR'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyAR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenAR'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceAR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discAR'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusAR'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameAR"][$j].'\',\''.$_SESSION["itemcodeAR"][$j].'\',\''.$_SESSION["qtyAR"][$j].'\',\''.$_SESSION["uomAR"][$j].'\',\''.$viewUoM.'\',\''.$_SESSION["priceAR"][$j].'\',\''.$_SESSION["discAR"][$j].'\',\''.$_SESSION["whsAR"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeAR"][$j].'\',\''.$_SESSION["itemnameAR"][$j].'\',\''.$_SESSION["qtyAR"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
