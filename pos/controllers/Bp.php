<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bp extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('bp',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_bp->GetAllData();
			
			$data['listpricelist']=$this->m_bp->GetAllPriceList();
			$data['listtax']=$this->m_tax->GetActiveData();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function getDataHeader()
	{
		$code=$this->m_bp->GetLastCode($_POST['Type']);
		echo $code;
	}
	public function detail($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			if ($data['crudaccess']->intRead==0)
			{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('bp',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			
			$data['listpricelist']=$this->m_bp->GetAllPriceList();
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['bp']=$this->m_bp->getByID($id);
			if(is_object($data['bp']))
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['bp']->vcCode);
				$this->load->view($this->uri->segment(1).'/detail',$data);
			}
			else
			{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
		}
	}
	function lisGroup()
	{
		$listcategory=$this->m_bp->GetAllCategoryByType($_GET['type']);
		
		$getbp=isset($_GET['BP'])?$_GET['BP']:''; // get the requested page
		$data['bp']=$this->m_bp->getByID($getbp);
		
		if(isset($_GET['withoutcontrol']))
		{
			$disabled='disabled="true"';
		}
		else
		{
			$disabled='';
		}
		echo '
		<select class="" id="Category" name="Category" style="width: 100%; height:35px" '.$disabled.'>
		';
		foreach($listcategory->result() as $d) 
		{
			
			if($d->intID==$data['bp']->intCategory)
			{
				$selected='selected="true"';
			}
			else
			{
				$selected='';
			}
			echo '<option value="'.$d->intID.'" '.$selected.'>'.$d->vcName.'</option>';
		}
		
		echo '
		</select>
		';
	}
	function prosesadd()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intCreate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		//general
		$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
		$data['Category'] = isset($_POST['Category'])?$_POST['Category']:''; // get the requested page
		$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['TaxNumber'] = isset($_POST['TaxNumber'])?$_POST['TaxNumber']:''; // get the requested page
		$data['PriceList'] = isset($_POST['PriceList'])?$_POST['PriceList']:''; // get the requested page
		$data['CreditLimit'] = isset($_POST['CreditLimit'])?$_POST['CreditLimit']:''; // get the requested page
		$data['PaymentTerm'] = isset($_POST['PaymentTerm'])?$_POST['PaymentTerm']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		//contact
		$data['Phone1'] = isset($_POST['Phone1'])?$_POST['Phone1']:''; // get the requested page
		$data['Phone2'] = isset($_POST['Phone2'])?$_POST['Phone2']:''; // get the requested page
		$data['Fax'] = isset($_POST['Fax'])?$_POST['Fax']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Website'] = isset($_POST['Website'])?$_POST['Website']:''; // get the requested page
		$data['ContactPerson'] = isset($_POST['ContactPerson'])?$_POST['ContactPerson']:''; // get the requested page
		
		//address
		$data['CityBillTo'] = isset($_POST['CityBillTo'])?$_POST['CityBillTo']:''; // get the requested page
		$data['CountryBillTo'] = isset($_POST['CountryBillTo'])?$_POST['CountryBillTo']:''; // get the requested page
		$data['AddressBillTo'] = isset($_POST['AddressBillTo'])?$_POST['AddressBillTo']:''; // get the requested page
		$data['CityShipTo'] = isset($_POST['CityShipTo'])?$_POST['CityShipTo']:''; // get the requested page
		$data['CountryShipTo'] = isset($_POST['CountryShipTo'])?$_POST['CountryShipTo']:''; // get the requested page
		$data['AddressShipTo'] = isset($_POST['AddressShipTo'])?$_POST['AddressShipTo']:''; // get the requested page
		
		$this->db->trans_begin();
		$cek=$this->m_bp->insert($data);
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Code']);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	
	function prosesedit($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		//general
		$data['id']=$id;
		$data['Category'] = isset($_POST['Category'])?$_POST['Category']:''; // get the requested page
		$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
		$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['TaxNumber'] = isset($_POST['TaxNumber'])?$_POST['TaxNumber']:''; // get the requested page
		$data['PriceList'] = isset($_POST['PriceList'])?$_POST['PriceList']:''; // get the requested page
		$data['CreditLimit'] = isset($_POST['CreditLimit'])?$_POST['CreditLimit']:''; // get the requested page
		$data['PaymentTerm'] = isset($_POST['PaymentTerm'])?$_POST['PaymentTerm']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		
		//contact
		$data['Phone1'] = isset($_POST['Phone1'])?$_POST['Phone1']:''; // get the requested page
		$data['Phone2'] = isset($_POST['Phone2'])?$_POST['Phone2']:''; // get the requested page
		$data['Fax'] = isset($_POST['Fax'])?$_POST['Fax']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Website'] = isset($_POST['Website'])?$_POST['Website']:''; // get the requested page
		$data['ContactPerson'] = isset($_POST['ContactPerson'])?$_POST['ContactPerson']:''; // get the requested page
		
		//address
		$data['CityBillTo'] = isset($_POST['CityBillTo'])?$_POST['CityBillTo']:''; // get the requested page
		$data['CountryBillTo'] = isset($_POST['CountryBillTo'])?$_POST['CountryBillTo']:''; // get the requested page
		$data['AddressBillTo'] = isset($_POST['AddressBillTo'])?$_POST['AddressBillTo']:''; // get the requested page
		$data['CityShipTo'] = isset($_POST['CityShipTo'])?$_POST['CityShipTo']:''; // get the requested page
		$data['CountryShipTo'] = isset($_POST['CountryShipTo'])?$_POST['CountryShipTo']:''; // get the requested page
		$data['AddressShipTo'] = isset($_POST['AddressShipTo'])?$_POST['AddressShipTo']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
		$cek=$this->m_bp->edit($data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successedit';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroredit';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function prosesdelete($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_bp->delete($id);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
}
