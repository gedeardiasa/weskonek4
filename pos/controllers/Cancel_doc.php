<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cancel_doc extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_docnum','',TRUE);
		
		$this->load->model('m_inpay','',TRUE);
		$this->load->model('m_outpay','',TRUE);
		
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_stock','',TRUE);
		
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_apcm','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_so','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Cancel Document',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['listdoc']=$this->m_docnum->GetAllDocType();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function execute()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocType'] = isset($_POST['DocType'])?$_POST['DocType']:''; // get the requested page
		
		$this->db->trans_begin();
		if($data['DocType']=='OUTPAY')
		{
			$getstatusdoc=$this->m_outpay->GetHeaderByDocNum($data['DocNum']); // ambil status document
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='C')
				{
					echo "You can only cancel documents outgoing payment with closed status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$idHeader=$getstatusdoc->intID;
					$this->m_outpay->closeall($idHeader); // close document hOutpay
					$header = $this->m_outpay->GetHeaderByHeaderID($idHeader);
					$detail = $this->m_outpay->GetDetailByHeaderID($idHeader);
					
					foreach($detail->result() as $d)
					{
						$dintApplied = $d->intApplied;
						$basetype=$d->vcBaseType."X";
						$this->m_wallet->addMutation($header->intWallet,$header->dtDate,$basetype,$dintApplied,$d->vcDocNum,$header->vcDocNum);// add mutation wallet
						$this->m_wallet->updateBalance($header->intWallet,$dintApplied); // change wallet balance
						
						$intBaseRef = $d->intBaseRef;
						$vcBaseType = $d->vcBaseType;
						
						
						if($vcBaseType=='AP')
						{
							$dintApplied = $d->intApplied;
							$this->m_ap->rollback($intBaseRef, $dintApplied); // rollback ar
						}
						else if($vcBaseType=='APCM')
						{
							$dintApplied = $d->intApplied*-1;
							$this->m_apcm->rollback($intBaseRef, $dintApplied); // rollback apcm
						}
						else if($vcBaseType=='ARCM')
						{
							$dintApplied = $d->intApplied;
							$this->m_arcm->rollback($intBaseRef, $dintApplied); // rollback arcm
						}
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='INPAY')
		{
			$getstatusdoc=$this->m_inpay->GetHeaderByDocNum($data['DocNum']); // ambil status document
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='C')
				{
					echo "You can only cancel documents incoming payment with closed status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$idHeader=$getstatusdoc->intID;
					$this->m_inpay->closeall($idHeader); // close document hOutpay
					$header = $this->m_inpay->GetHeaderByHeaderID($idHeader);
					$detail = $this->m_inpay->GetDetailByHeaderID($idHeader);
					
					foreach($detail->result() as $d)
					{
						$dintAppliedminus = $d->intApplied*-1;
						$basetype=$d->vcBaseType."X";
						$this->m_wallet->addMutation($header->intWallet,$header->dtDate,$basetype,$dintAppliedminus,$d->vcDocNum,$header->vcDocNum);// add mutation wallet
						$this->m_wallet->updateBalance($header->intWallet,$dintAppliedminus); // change wallet balance
						
						$intBaseRef = $d->intBaseRef;
						$vcBaseType = $d->vcBaseType;
						
						
						if($vcBaseType=='AR')
						{
							$dintApplied = $d->intApplied;
							$this->m_ar->rollback($intBaseRef, $dintApplied); // rollback ar
						}
						else if($vcBaseType=='ARCM')
						{
							$dintApplied = $d->intApplied*-1;
							$this->m_arcm->rollback($intBaseRef, $dintApplied); // rollback arcm
						}
						else if($vcBaseType=='APCM')
						{
							$dintApplied = $d->intApplied;
							$this->m_apcm->rollback($intBaseRef, $dintApplied); // rollback apcm
						}
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='AP')
		{
			$getstatusdoc=$this->m_ap->GetHeaderByDocNum($data['DocNum']);
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents A/P Invoice with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_ap->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_ap->cancelall($getstatusdoc->intID);//cancel AP
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='GRPO')
						{
							//jika baseref GRPO hanya update open qty pada GRPO dan set GRPO to open
							$this->m_grpo->updatestatusH($ld->intBaseRef,'O');
							$this->m_grpo->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
							
						}
						else if($ld->vcBaseType=='PO')
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$this->m_po->updatestatusH($ld->intBaseRef,'O');
								$this->m_po->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
								
								$qtymin=$ld->intQtyInv*-1;
								$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'APX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else// jika tipe service
							{
								$this->m_po->updatestatusH($ld->intBaseRef,'O');
								$this->m_po->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
							}
						}
						else // tanpa referenci
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$qtymin=$ld->intQtyInv*-1;
								$this->m_stock->updateStock($ld->intItem,$qtymin,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtymin,$ld->intCost,$ld->intLocation,'APX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else // jika tipe service
							{
								// tidak terjadi apa-apa
							}
						}
					}
					echo 1;
				}
			}
		}
		else if($data['DocType']=='AR')
		{
			$getstatusdoc=$this->m_ar->GetHeaderByDocNum($data['DocNum']);
			
			if(is_object($getstatusdoc)==false)
			{
				echo "Document Not Found";
			}
			else
			{
				$statusdoc=$getstatusdoc->vcStatus;
				if($statusdoc!='O')
				{
					echo "You can only cancel documents A/R Invoice with open status";
				}
				else
				{
					$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$data['DocType'].'-'.$data['DocNum'].'');
					$listdata=$this->m_ar->GetDetailByHeaderID($getstatusdoc->intID);
					$this->m_ar->cancelall($getstatusdoc->intID);//cancel AP
					foreach($listdata->result() as $ld)
					{
						
						if($ld->vcBaseType=='DN')
						{
							//jika baseref DN hanya update open qty pada DN dan set DN to open
							$this->m_dn->updatestatusH($ld->intBaseRef,'O');
							$this->m_dn->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->intQty,$ld->intQtyInv);
							
						}
						else if($ld->vcBaseType=='SO')
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								$this->m_so->updatestatusH($ld->intBaseRef,'O');
								$this->m_so->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
								
								$qtyplus=$ld->intQtyInv;
								$this->m_stock->updateStock($ld->intItem,$qtyplus,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation,'ARX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else// jika tipe service
							{
								$this->m_so->updatestatusH($ld->intBaseRef,'O');
								$this->m_so->reOpenDetail($ld->intBaseRef,$ld->intItem,$ld->vcItemName,$ld->intQty,$ld->intQtyInv);
							}
						}
						else // tanpa referenci
						{
							if($ld->intService==0)// jika tipe bukan service/ tipe item
							{
								echo "Tanpa Referesni";
								$qtyplus=$ld->intQtyInv;
								$this->m_stock->updateStock($ld->intItem,$qtyplus,$ld->intLocation);//update stok menambah/mengurangi di gudang
								$this->m_stock->updateCost($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation);//update cost
								$this->m_stock->addMutation($ld->intItem,$qtyplus,$ld->intCost,$ld->intLocation,'ARX',$ld->dtDate,$ld->vcDocNum);//add mutation
							}
							else // jika tipe service
							{
								// tidak terjadi apa-apa
							}
						}
					}
					echo 1;
				}
			}
		}
		else
		{
			echo "You Cannot Cancel This Document Type";
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
}
