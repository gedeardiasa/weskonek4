<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coa_setting extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_coa_setting','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('wallet',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_coa_setting->GetAllData();
			$data['listgl']=$this->m_coa->GetAllDataLevel2();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	function prosesedit()
	{
		
		$list=$this->m_coa_setting->GetAllData();
		foreach($list->result() as $d)
		{
			$data[$d->vcCode]=$_POST[$d->vcCode];
			$this->m_coa_setting->Update($d->vcCode,$data[$d->vcCode]);
		}
		$cek=1;
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
			
		
	}
	
}
