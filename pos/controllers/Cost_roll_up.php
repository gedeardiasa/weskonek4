<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost_roll_up extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_stock','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['Whs']=isset($_GET['Whs'])?$_GET['Whs']:$data['defaultwhs']; // get the requested page
			
			$data['typetoolbar']='CRU';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Cost Roll Up',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			if(isset($_GET['execute']))
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'execute',$data['Whs']);
				$this->m_bom->emptyCRU();
				$bom=$this->m_bom->GetHeaderByLoc($data['Whs']);
				
				foreach($bom->result() as $b)
				{
					$da['intBOM']=$b->intID;
					$da['dtDate']=date('Y-m-d');
					$da['intCostBefore']=$b->intCost;
					$head = $this->m_bom->insertCRU($da);
					$dbom=$this->m_bom->GetDetailByHeaderID($b->intID);
					
					$newfgcost=0;
					foreach($dbom->result() as $dbm)
					{
						$dd['intHID']=$head;
						$dd['intItem']=$dbm->intItem;
						$dd['vcItemCode']=$dbm->vcItemCode;
						$dd['vcItemName']=$dbm->vcItemName;
						$dd['intQty']=$dbm->intQty;
						$dd['vcUoM']=$dbm->vcUoM;
						$dd['intCost']=$this->m_stock->GetCostItem($dd['intItem'],$data['Whs']);
						$this->m_bom->insertDCRU($dd);
						
						$newfgcost=$newfgcost+($dd['intQty']*$dd['intCost']);
						$this->m_bom->updatecostCRU($head,$newfgcost);
						
					}
				}
				$data['cru']=$this->m_bom->GetAllCRUByLoc($data['Whs']);
			}
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function detailbom()
	{
		$bom=$this->m_bom->GetDetailByHeaderID($_GET['id']);
		$cru=$this->m_bom->GetDetailCRUByBomID($_GET['id']);
		echo '
		<div class="row">
		 <div class="col-sm-6">
		 <b><i><u>BOM Detail</u></i></b><br>
		  <table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th>Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($bom->result() as $b)
		 {
			  echo'
			  <tr>
				<td>'.$b->vcItemCode.'</td>
				<td>'.$b->vcItemName.'</td>
				<td>'.$b->intQty.'</td>
                <td>'.$b->vcUoM.'</td>
				<td>'.$b->intCost.'</td>
			  </tr>
			  
			  ';
		 }  
		 echo '
			  </tbody>
			</table>
		 </div>
		 
		 <div class="col-sm-6">
		   <b><i><u>Cost Roll Up Detail</u></i></b><br>
		  <table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
			<thead>
			  <tr>
				<th>Code</th>
				<th>Name</th>
				<th>Qty</th>
                <th>UoM</th>
				<th>Cost</th>
			  </tr>
			  </thead>
			  <tbody>
			  ';
	     foreach($cru->result() as $c)
		 {
			  echo'
			  <tr>
				<td>'.$c->vcItemCode.'</td>
				<td>'.$c->vcItemName.'</td>
				<td>'.$c->intQty.'</td>
                <td>'.$c->vcUoM.'</td>
				<td>'.$c->intCost.'</td>
			  </tr>
			  
			  ';
		 }  
		 
		echo '
			  </tbody>
			</table>
		 </div>
		</div>
		';
		
	}
	
}
