<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disc extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_disc','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemDISC']=0;
			unset($_SESSION['itemcodeDISC']);
			unset($_SESSION['itemnameDISC']);
			unset($_SESSION['discDISC']);
			
			$data['typetoolbar']='DISC';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Discount',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_disc->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_disc->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hDiscount');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->UntilDate = date('m/d/Y',strtotime($r->dtUntil));
			$responce->Whs = $r->intLocation;
			$responce->Name = $r->vcName;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
			
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDiscount');
			$responce->DocDate = date('m/d/Y');
			$responce->UntilDate = date('m/d/Y');
			$responce->Whs = '';
			$responce->Name = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemDISC']=0;
			unset($_SESSION['itemcodeDISC']);
			unset($_SESSION['itemnameDISC']);
			unset($_SESSION['discDISC']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemDISC']=0;
			unset($_SESSION['itemcodeDISC']);
			unset($_SESSION['itemnameDISC']);
			unset($_SESSION['discDISC']);
			
			$r=$this->m_disc->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['itemcodeDISC'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDISC'][$j]=$d->vcItemName;
				$_SESSION['discDISC'][$j]=$d->intDisc;
				$j++;
			}
			$_SESSION['totitemDISC']=$j;
		}
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemDISC'];$j++)
		{
			if(isset($_SESSION['itemcodeDISC'][$j]))
			{
				if($_SESSION['itemcodeDISC'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['UntilDate'] = isset($_POST['UntilDate'])?$_POST['UntilDate']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['UntilDate'] = date('Y-m-d',strtotime($data['UntilDate']));
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_disc->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemDISC'];$j++)// save detail
			{
				if($_SESSION['itemcodeDISC'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDISC'][$j]);
					$da['intHID']=$head;
					$da['itemcodeDISC']=$_SESSION['itemcodeDISC'][$j];
					$da['itemnameDISC']=$_SESSION['itemnameDISC'][$j];
					$da['Whs']=$data['Whs'];
					$da['discDISC']=$_SESSION['discDISC'][$j];
					$detail=$this->m_disc->insertD($da);
					
				}
			}
			$_SESSION['totitemDISC']=0;
			unset($_SESSION['itemcodeDISC']);
			unset($_SESSION['itemnameDISC']);
			unset($_SESSION['discDISC']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesdelete()
	{
		$this->m_disc->delete($_POST['DocNum']);
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$_POST['DocNum']);
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_disc->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Until Date</th>
                  <th>Whs</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->dtUntil.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemDISC'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemDISC'];$j++)
		{
			if($_SESSION['itemcodeDISC'][$j]==$code)
			{
				$_SESSION['discDISC'][$j]=$_POST['detailDisc'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeDISC'][$i]=$code;
				$_SESSION['itemnameDISC'][$i]=$_POST['detailItem'];
				$_SESSION['discDISC'][$i]=$_POST['detailDisc'];
				$_SESSION['totitemDISC']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemDISC'];$j++)
		{
			if($_SESSION['itemcodeDISC'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeDISC'][$j]="";
				$_SESSION['itemnameDISC'][$j]="";
				$_SESSION['discDISC'][$j]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Disc</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemDISC'];$j++)
			{
				if($_SESSION['itemnameDISC'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDISC'][$j]);
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeDISC'][$j].'</td>
						<td>'.$_SESSION['itemnameDISC'][$j].'</td>
						<td align="right">'.number_format($_SESSION['discDISC'][$j],'2').'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameDISC"][$j].'\',\''.$_SESSION["itemcodeDISC"][$j].'\',\''.$_SESSION["discDISC"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeDISC"][$j].'\',\''.$_SESSION["discDISC"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
