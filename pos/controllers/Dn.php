<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dn extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			
			$data['typetoolbar']='DN';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('delivery note',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_dn->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listso']=$this->m_so->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_dn->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSQ()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	function getDataHeaderSO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hDN');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			
			$r=$this->m_dn->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idDN'][$j]=$d->intID;
				$_SESSION['BaseRefDN'][$j]='';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusDN'][$j]='O';
				}
				else
				{
					$_SESSION['statusDN'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemDN']=$j;
		}
	}
	function loaddetailsq()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemDN']=0;
		unset($_SESSION['itemcodeDN']);
		unset($_SESSION['idDN']);
		unset($_SESSION['idBaseRefDN']);
		unset($_SESSION['itemnameDN']);
		unset($_SESSION['qtyDN']);
		unset($_SESSION['qtyOpenDN']);
		unset($_SESSION['uomDN']);
		unset($_SESSION['priceDN']);
		unset($_SESSION['discDN']);
		unset($_SESSION['whsDN']);
		unset($_SESSION['statusDN']);
		
		$r=$this->m_sq->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefDN'][$j]=$d->intHID;
				$_SESSION['BaseRefDN'][$j]='SQ';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				$_SESSION['statusDN'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemDN']=$j;
	}
	function loaddetailso()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemDN']=0;
		unset($_SESSION['itemcodeDN']);
		unset($_SESSION['idDN']);
		unset($_SESSION['idBaseRefDN']);
		unset($_SESSION['itemnameDN']);
		unset($_SESSION['qtyDN']);
		unset($_SESSION['qtyOpenDN']);
		unset($_SESSION['uomDN']);
		unset($_SESSION['priceDN']);
		unset($_SESSION['discDN']);
		unset($_SESSION['whsDN']);
		unset($_SESSION['statusDN']);
		
		$r=$this->m_so->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefDN'][$j]=$d->intHID;
				$_SESSION['BaseRefDN'][$j]='SO';
				$_SESSION['itemcodeDN'][$j]=$d->vcItemCode;
				$_SESSION['itemnameDN'][$j]=$d->vcItemName;
				$_SESSION['qtyDN'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenDN'][$j]=$d->intOpenQty;
				$_SESSION['uomDN'][$j]=$d->intUoMType;
				$_SESSION['priceDN'][$j]=$d->intPrice;
				$_SESSION['discDN'][$j]=$d->intDiscPer;
				$_SESSION['whsDN'][$j]=$d->intLocation;
				$_SESSION['statusDN'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemDN']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!='' and $_SESSION['itemcodeDN'][$j]!=null)
			{
				$total=$total+(($_SESSION['qtyDN'][$j]*$_SESSION['priceDN'][$j])-($_SESSION['discDN'][$j]/100*($_SESSION['qtyDN'][$j]*$_SESSION['priceDN'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if(isset($_SESSION['itemcodeDN'][$j]))
			{
				if($_SESSION['itemcodeDN'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$hasil='';
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeDN'][$j]);
				if($_SESSION['uomDN'][$j]==1)// konversi uom
				{
					$da['qtyinvDN']=$_SESSION['qtyDN'][$j];
				}
				else if($_SESSION['uomDN'][$j]==2)
				{
					$da['qtyinvDN']=$this->m_item->convert_qty($item,$_SESSION['qtyDN'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomDN'][$j]==3)
				{
					$da['qtyinvDN']=$this->m_item->convert_qty($item,$_SESSION['qtyDN'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvDN'],$_SESSION['whsDN'][$j]);
				if($res==1)
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeDN'][$j].' - '.$_SESSION['itemnameDN'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsDN'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSQ($id,$item,$qty,$qtyinv)
	{
		$this->m_sq->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseSO($id,$item,$qty,$qtyinv)
	{
		$this->m_so->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_dn->insertH($data);
		//$this->m_bp->updateBDO($data['BPId'],'intDelivery',$data['DocTotal']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemDN'];$j++)// save detail
			{
				if($_SESSION['itemcodeDN'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDN'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameDN'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsDN'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeDN']=$_SESSION['itemcodeDN'][$j];
					$da['itemnameDN']=$_SESSION['itemnameDN'][$j];
					$da['qtyDN']=$_SESSION['qtyDN'][$j];
					$da['whsDN']=$_SESSION['whsDN'][$j];
					$da['whsNameDN']=$whs;
					
					if($_SESSION['uomDN'][$j]==1)
					{
						$da['uomDN']=$vcUoM->vcUoM;
						$da['qtyinvDN']=$da['qtyDN'];
					}
					else if($_SESSION['uomDN'][$j]==2)
					{
						$da['uomDN']=$vcUoM->vcSlsUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intSlsUoM',1);
					}
					else if($_SESSION['uomDN'][$j]==3)
					{
						$da['uomDN']=$vcUoM->vcPurUoM;
						$da['qtyinvDN']=$this->m_item->convert_qty($item,$da['qtyDN'],'intPurUoM',1);
					}
					
					if(isset($_SESSION['idBaseRefDN'][$j]))
					{
						$da['idBaseRefDN']=$_SESSION['idBaseRefDN'][$j];
						$da['BaseRefDN']=$_SESSION['BaseRefDN'][$j];
					}
					else
					{
						$da['idBaseRefDN']=0;
						$da['BaseRefDN']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefDN']=='SQ')
					{
						$this->cekcloseSQ($da['idBaseRefDN'], $da['itemID'], $da['qtyDN'], $da['qtyinvDN']);
					}
					else if($da['BaseRefDN']=='SO')
					{
						$this->cekcloseSO($da['idBaseRefDN'], $da['itemID'], $da['qtyDN'], $da['qtyinvDN']);
					}
					
					$da['uomtypeDN']=$_SESSION['uomDN'][$j];
					$da['uominvDN']=$vcUoM->vcUoM;
					$da['costDN']=$this->m_stock->GetCostItem($item,$da['whsDN']);
					
					$da['priceDN']= $_SESSION['priceDN'][$j];
					$da['discperDN'] = $_SESSION['discDN'][$j];
					$da['discDN'] = $_SESSION['discDN'][$j]/100*$da['priceDN'];
					$da['priceafterDN']=(100-$_SESSION['discDN'][$j])/100*$da['priceDN'];
					$da['linetotalDN']= $da['priceafterDN']*$da['qtyDN'];
					$da['linecostDN']=$da['costDN']*$da['qtyinvDN'];
					
					
					$detail=$this->m_dn->insertD($da);
					$da['qtyinvDN']=$da['qtyinvDN']*-1;
					$this->m_stock->updateStock($item,$da['qtyinvDN'],$da['whsDN']);//update stok menambah/mengurangi di gudang
					$this->m_stock->addMutation($item,$da['qtyinvDN'],$da['costDN'],$da['whsDN'],'DN',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
			$_SESSION['totitemDN']=0;
			unset($_SESSION['itemcodeDN']);
			unset($_SESSION['idDN']);
			unset($_SESSION['idBaseRefDN']);
			unset($_SESSION['itemnameDN']);
			unset($_SESSION['qtyDN']);
			unset($_SESSION['qtyOpenDN']);
			unset($_SESSION['uomDN']);
			unset($_SESSION['priceDN']);
			unset($_SESSION['discDN']);
			unset($_SESSION['whsDN']);
			unset($_SESSION['statusDN']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_dn->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_dn->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_dn->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemDN'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]==$code)
			{
				$_SESSION['qtyDN'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenDN'][$j]=$_POST['detailQty'];
				$_SESSION['uomDN'][$j]=$_POST['detailUoM'];
				$_SESSION['priceDN'][$j]=$_POST['detailPrice'];
				$_SESSION['discDN'][$j]=$_POST['detailDisc'];
				$_SESSION['whsDN'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefDN'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeDN'][$i]=$code;
				$_SESSION['itemnameDN'][$i]=$_POST['detailItem'];
				$_SESSION['qtyDN'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenDN'][$i]=$_POST['detailQty'];
				$_SESSION['uomDN'][$i]=$_POST['detailUoM'];
				$_SESSION['priceDN'][$i]=$_POST['detailPrice'];
				$_SESSION['discDN'][$i]=$_POST['detailDisc'];
				$_SESSION['whsDN'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefDN'][$j]='';
				$_SESSION['statusDN'][$i]='O';
				$_SESSION['totitemDN']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemDN'];$j++)
		{
			if($_SESSION['itemcodeDN'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeDN'][$j]="";
				$_SESSION['itemnameDN'][$j]="";
				$_SESSION['qtyDN'][$j]="";
				$_SESSION['qtyOpenDN'][$j]="";
				$_SESSION['uomDN'][$j]="";
				$_SESSION['priceDN'][$j]="";
				$_SESSION['discDN'][$j]="";
				$_SESSION['whsDN'][$j]="";
				$_SESSION['BaseRefDN'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemDN'];$j++)
			{
				if($_SESSION['itemnameDN'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameDN'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameDN'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsDN'][$j]);
					
					if($_SESSION['uomDN'][$j]==1)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomDN'][$j]==2)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomDN'][$j]==3)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					
					if($_SESSION['discDN'][$j]=='')
					{
						$_SESSION['discDN'][$j]=0;
					}
					
					if($_SESSION['statusDN'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discDN'][$j])/100)*$_SESSION['priceDN'][$j]*$_SESSION['qtyDN'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeDN'][$j].'</td>
						<td>'.$_SESSION['itemnameDN'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyDN'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenDN'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceDN'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discDN'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusDN'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameDN"][$j].'\',\''.$_SESSION["itemcodeDN"][$j].'\',\''.$_SESSION["qtyDN"][$j].'\',\''.$_SESSION["uomDN"][$j].'\',\''.$_SESSION["priceDN"][$j].'\',\''.$_SESSION["discDN"][$j].'\',\''.$_SESSION["whsDN"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeDN"][$j].'\',\''.$_SESSION["qtyDN"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
