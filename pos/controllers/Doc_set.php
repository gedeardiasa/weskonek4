<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doc_set extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Doc',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['listdoc']=$this->m_docnum->GetAllDocType();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "edit":
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Width'] = isset($_POST['Width'])?$_POST['Width']:''; // get the requested page
				$data['Height'] = isset($_POST['Height'])?$_POST['Height']:''; // get the requested page
				$data['Font'] = isset($_POST['Font'])?$_POST['Font']:''; // get the requested page
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
				$cek=$this->m_docnum->edit($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successedit';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroredit';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
}
