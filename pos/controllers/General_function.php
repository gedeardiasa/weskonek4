<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_function extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_period','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_disc','',TRUE);
		$this->load->library('message');
        $this->load->database();
    }
	public function cekpostperiod()
	{
		$result=$this->m_period->cekperiod($_POST['DocDate']);
		echo $result;
	}
	function loaddisc()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$whs=$this->m_location->GetIDByName($_POST['WhsName']);
		echo $this->m_disc->GetDiscByItemAndWhs($item,$whs);
		
	}
}
