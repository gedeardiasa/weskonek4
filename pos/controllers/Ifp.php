<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ifp extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_ifp','',TRUE);
		$this->load->model('m_pro','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemIFP']=0;
			unset($_SESSION['itemcodeIFP']);
			unset($_SESSION['itemnameIFP']);
			unset($_SESSION['qtyIFP']);
			unset($_SESSION['uomIFP']);
			unset($_SESSION['costIFP']);
			
			$data['typetoolbar']='IFP';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('IFP',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_ifp->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listpro']=$this->m_pro->GetAllDataWithPlanAccessRelease($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_ifp->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hIFP');
			}
			$responce->PRONum = $r->PRONum;
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->Whs = $r->intLocation;
			$responce->Type = $r->vcType;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hIFP');
			$responce->PRONum = '';
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->Whs = '';
			$responce->Type = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderPro()
	{
		
			$id=$_POST['id'];
			$r=$this->m_pro->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			$responce->PRONum = $r->vcDocNum;
			$responce->Whs = $r->intLocation;
			
			
		echo json_encode($responce);
	}
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemIFP']=0;
			unset($_SESSION['itemcodeIFP']);
			unset($_SESSION['itemnameIFP']);
			unset($_SESSION['qtyIFP']);
			unset($_SESSION['uomIFP']);
			unset($_SESSION['costIFP']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemIFP']=0;
			unset($_SESSION['itemcodeIFP']);
			unset($_SESSION['itemnameIFP']);
			unset($_SESSION['qtyIFP']);
			unset($_SESSION['uomIFP']);
			unset($_SESSION['costIFP']);
			
			$r=$this->m_ifp->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['itemcodeIFP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameIFP'][$j]=$d->vcItemName;
				$dintQty=(float)$d->intQty;
				if($dintQty<0)
				{
					$_SESSION['qtyIFP'][$j]=$dintQty*-1;
				}
				else
				{
					$_SESSION['qtyIFP'][$j]=$dintQty;
				}
				$_SESSION['uomIFP'][$j]=$d->vcUoM;
				$_SESSION['costIFP'][$j]=$d->intCost;
				$j++;
			}
			$_SESSION['totitemIFP']=$j;
		}
	}
	function loaddetailpro()
	{
			$id=$_POST['id'];
			$Type=$_POST['Type'];
			$_SESSION['totitemIFP']=0;
			unset($_SESSION['itemcodeIFP']);
			unset($_SESSION['itemnameIFP']);
			unset($_SESSION['qtyIFP']);
			unset($_SESSION['uomIFP']);
			unset($_SESSION['costIFP']);
			
			$head=$this->m_pro->GetHeaderByHeaderID($id);
			$whs=$head->intLocation;
			$r=$this->m_pro->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				if($d->intAutoIssue==0)
				{
					$_SESSION['itemcodeIFP'][$j]=$d->vcItemCode;
					$_SESSION['itemnameIFP'][$j]=$d->vcItemName;
					
					if($Type=='GI')
					{
						$_SESSION['qtyIFP'][$j]=$d->intPlannedQty-$d->intIssueQty;
					}
					else if($Type=='GR')
					{
						$_SESSION['qtyIFP'][$j]=$d->intIssueQty;
					}
					else
					{
						$_SESSION['qtyIFP'][$j]=$d->intPlannedQty;
					}
					$_SESSION['uomIFP'][$j]=$d->vcUoM;
					
					$item=$this->m_item->GetIDByName($d->vcItemName);
					$cost=$this->m_stock->GetCostItem($item,$whs);
					
					$_SESSION['costIFP'][$j]=$cost;
					$j++;
				}
			}
			$_SESSION['totitemIFP']=$j;
		
	}
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		echo $cost;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekminusD()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$cek=$this->m_stock->cekMinusStock($item,$_POST['detailQty'],$_POST['Whs']);
		echo $cek;
	}
	function cekminusH()
	{
		$hasil=1;
		for($j=0;$j<$_SESSION['totitemIFP'];$j++)
		{
			if(isset($_SESSION['itemcodeIFP'][$j]))
			{
				if($_SESSION['itemcodeIFP'][$j]!='')
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIFP'][$j]);
					$cek=$this->m_stock->cekMinusStock($item,$_SESSION['qtyIFP'][$j],$_POST['Whs']);
					if($cek==0)
					{
						$hasil=$_SESSION['itemnameIFP'][$j];
						break;
					}
				}
			}
		}
		echo $hasil;
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemIFP'];$j++)
		{
			if(isset($_SESSION['itemcodeIFP'][$j]))
			{
				if($_SESSION['itemcodeIFP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekPro()
	{
		$r=$this->m_pro->CekNumPro($_POST['PRONum']);
		echo $r;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['PRONum'] = isset($_POST['PRONum'])?$_POST['PRONum']:''; // get the requested page
		$data['PRONum'] = $this->m_pro->GetIDByDocNum($data['PRONum']); // ambil ID PRO berdasarkan DocNum PRO
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_ifp->insertH($data);
		
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemIFP'];$j++)// save detail
			{
				if($_SESSION['itemcodeIFP'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIFP'][$j]);
					$da['intHID']=$head;
					$da['itemcodeIFP']=$_SESSION['itemcodeIFP'][$j];
					$da['itemnameIFP']=$_SESSION['itemnameIFP'][$j];
					$da['Whs']=$data['Whs'];
					if($data['Type']=='GR')
					{
						$da['qtyIFP']=$_SESSION['qtyIFP'][$j];
						$tipemutasi = 'CGR';
					}
					else if($data['Type']=='GI')
					{
						$da['qtyIFP']=$_SESSION['qtyIFP'][$j]*-1;
						$tipemutasi = 'CGI';
					}
					else
					{
						$tipemutasi = 'Unknown';
					}
					$da['uomIFP']=$_SESSION['uomIFP'][$j];
					$da['costIFP']=$_SESSION['costIFP'][$j];
					$da['linetotalcost'] = $da['qtyIFP']*$da['costIFP'];
					$detail=$this->m_ifp->insertD($da);
					if($detail==0)
					{
						$errordetail++;
					}
					
					$this->m_stock->updateStock($item,$da['qtyIFP'],$data['Whs']);//update stok menambah/mengurangi di gudang
					if($data['Type']=='GR')
					{
						$this->m_stock->updateCost($item,$da['qtyIFP'],$da['costIFP'],$da['Whs']);//update cost per gudang hanya untuk good receipt
					}
					
					$da['linetotalcost']=$da['linetotalcost']*-1;
					
					$this->m_stock->addMutation($item,$da['qtyIFP'],$da['costIFP'],$da['Whs'],$tipemutasi,$data['DocDate'],$data['DocNum']);//add mutation
					
					$this->m_pro->updateIssueQty($data['PRONum'],$item,$da['qtyIFP']);//update Issue Qty
					$this->m_pro->updateComponentCost($data['PRONum'],$da['linetotalcost']);//update PRO
				}
			}
			$_SESSION['totitemIFP']=0;
			unset($_SESSION['itemcodeIFP']);
			unset($_SESSION['itemnameIFP']);
			unset($_SESSION['qtyIFP']);
			unset($_SESSION['uomIFP']);
			unset($_SESSION['costIFP']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_ifp->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>PRO. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>Whs</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->PRONum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemIFP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemIFP'];$j++)
		{
			if($_SESSION['itemcodeIFP'][$j]==$code)
			{
				$_SESSION['qtyIFP'][$j]=$_POST['detailQty'];
				$_SESSION['costIFP'][$j]=$_POST['detailCost'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeIFP'][$i]=$code;
				$_SESSION['itemnameIFP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyIFP'][$i]=$_POST['detailQty'];
				$_SESSION['costIFP'][$i]=$_POST['detailCost'];
				$_SESSION['uomIFP'][$i]=$UoM;
				$_SESSION['totitemIFP']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemIFP'];$j++)
		{
			if($_SESSION['itemcodeIFP'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeIFP'][$j]="";
				$_SESSION['itemnameIFP'][$j]="";
				$_SESSION['qtyIFP'][$j]="";
				$_SESSION['uomIFP'][$j]="";
				$_SESSION['costIFP'][$j]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemIFP'];$j++)
			{
				if($_SESSION['itemnameIFP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIFP'][$j]);
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeIFP'][$j].'</td>
						<td>'.$_SESSION['itemnameIFP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyIFP'][$j],'2').'</td>
						<td>'.$_SESSION['uomIFP'][$j].'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameIFP"][$j].'\',\''.$_SESSION["itemcodeIFP"][$j].'\',\''.$_SESSION["qtyIFP"][$j].'\',\''.$_SESSION["costIFP"][$j].'\',\''.$_SESSION["uomIFP"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeIFP"][$j].'\',\''.$_SESSION["qtyIFP"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
