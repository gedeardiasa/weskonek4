<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_bom','',TRUE);

		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			
			$data['datatype'] = isset($_GET['data'])?$_GET['data']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Import',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function emptysession()
	{
		unset($_SESSION['totitemIMPORT']);
		unset($_SESSION['messageIMPORT']);
		
		//item
		unset($_SESSION['Category']);
		unset($_SESSION['Code']);
		unset($_SESSION['Name']);
		unset($_SESSION['Price']);
		unset($_SESSION['Tax']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['UoM']);
		unset($_SESSION['PurUoM']);
		unset($_SESSION['intPurUoM']);
		unset($_SESSION['SlsUoM']);
		unset($_SESSION['intSlsUoM']);
		unset($_SESSION['intSalesItem']);
		unset($_SESSION['intPurchaseItem']);
		unset($_SESSION['intSalesWithoutQty']);
		unset($_SESSION['Barcode']);
		//end item
		
		//ib
		unset($_SESSION['Date']);
		unset($_SESSION['ItemCode']);
		unset($_SESSION['Type']);
		unset($_SESSION['Location']);
		unset($_SESSION['Qty']);
		unset($_SESSION['Cost']);
		//endib
		
		
		//price
		unset($_SESSION['Category']);
		unset($_SESSION['ItemCode']);
		unset($_SESSION['Price']);
		//endprice
		
		
		//bp
		unset($_SESSION['Type']);
		unset($_SESSION['Category']);
		unset($_SESSION['Code']);
		unset($_SESSION['Name']);
		unset($_SESSION['TaxNumber']);
		unset($_SESSION['PriceList']);
		unset($_SESSION['CreditLimit']);
		unset($_SESSION['PaymentTerm']);
		unset($_SESSION['Tax']);
		unset($_SESSION['Phone1']);
		unset($_SESSION['Phone2']);
		unset($_SESSION['Fax']);
		unset($_SESSION['Email']);
		unset($_SESSION['Website']);
		unset($_SESSION['ContactPerson']);
		unset($_SESSION['CityBillTo']);
		unset($_SESSION['CountryBillTo']);
		unset($_SESSION['AddressBillTo']);
		unset($_SESSION['CityShipTo']);
		unset($_SESSION['CountryShipTo']);
		unset($_SESSION['AddressShipTo']);
		
		//bp
		
		//ar and ap
		unset($_SESSION['BPCode']);
		unset($_SESSION['RefNum']);
		unset($_SESSION['DocDate']);
		unset($_SESSION['DelDate']);
		unset($_SESSION['SalesEmp']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['DocTotal']);
		unset($_SESSION['Location']);
		//ar and ap
		
		//bom
		unset($_SESSION['DocDate']);
		unset($_SESSION['FGItem']);
		unset($_SESSION['Whs']);
		unset($_SESSION['FGQty']);
		unset($_SESSION['Rev']);
		unset($_SESSION['Remarks']);
		unset($_SESSION['itemcodeBOM']);
		unset($_SESSION['qtyBOM']);
		unset($_SESSION['autoBOM']);
		//bom
	}
	public function reloaddownload()
	{
		echo '<i class="fa fa-file-excel-o"></i><br>';
		if($_GET['Type']=='item')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_master_data.xls">download template master data</a>';
		}
		if($_GET['Type']=='ib')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_ib_stock.xls">download template initial balance stock</a>';
		}
		if($_GET['Type']=='price')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_price.xls">download template price list</a>';
		}
		if($_GET['Type']=='bp')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_bp.xls">download template business partner</a>';
		}
		if($_GET['Type']=='ar')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_ar.xls">download template a/r invoice</a>';
		}
		if($_GET['Type']=='ap')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_ap.xls">download template a/p invoice</a>';
		}
		if($_GET['Type']=='bom')
		{
			echo '<a href="'.$this->config->base_url().'data/template_import/template_bom.xls">download template BOM</a>';
		}
	}
	public function upload()
	{
		if($_GET['type']=='item')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_item_category->GetIDByCode($data['Category']);
				if($data['Category']!=0)
				{
					$data['Code'] = $this->m_item->GetLastCode($data['Category']);
				}
				else
				{
					$data['Code']=0;
				}
				$data['Name'] =  $_SESSION['Name'][$i];
				$samename=$this->m_item->GetIDByName($data['Name']);
				$data['Price'] =  $_SESSION['Price'][$i];
				$data['Tax'] =  $_SESSION['Tax'][$i];
				$data['Tax'] = $this->m_tax->GetIDByCode($data['Tax']);
				$data['Remarks'] =  $_SESSION['Remarks'][$i];
				$data['UoM'] =  $_SESSION['UoM'][$i];
				$data['PurUoM'] =  $_SESSION['PurUoM'][$i];
				$data['intPurUoM'] =  $_SESSION['intPurUoM'][$i];
				
				$data['SlsUoM'] =  $_SESSION['SlsUoM'][$i];
				$data['intSlsUoM'] =  $_SESSION['intSlsUoM'][$i];
				
				//setting
				$data['intSalesItem'] =  $_SESSION['intSalesItem'][$i];
				$data['intPurchaseItem'] =  $_SESSION['intPurchaseItem'][$i];
				$data['intSalesWithoutQty'] =  $_SESSION['intSalesWithoutQty'][$i];
				$data['Barcode'] =  $_SESSION['Barcode'][$i];
				$data['imgData']=null;
				
				
				if($data['Category']==0)
				{
					$upload=0;
					$message=$message.'Item Category Not Found, ';
				}
				if (is_numeric($data['Price'])==false)
				{
					$upload=0;
					$message=$message.'The price is only allowed with the format number, ';
				}
				if($data['Tax']==0)
				{
					$upload=0;
					$message=$message.'Tax Not Found, ';
				}
				if (is_numeric($data['intPurUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intPurUoM is only allowed with the format number, ';
				}
				if (is_numeric($data['intPurUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intPurUoM is only allowed with the format number, ';
				}
				if (is_numeric($data['intSlsUoM'])==false)
				{
					$upload=0;
					$message=$message.'The intSlsUoM is only allowed with the format number, ';
				}
				if ($data['intSalesItem']!=0 and $data['intSalesItem']!=1)
				{
					$upload=0;
					$message=$message.'The intSalesItem is only allowed 0 or 1, ';
				}
				if ($data['intPurchaseItem']!=0 and $data['intPurchaseItem']!=1)
				{
					$upload=0;
					$message=$message.'The intPurchaseItem is only allowed 0 or 1, ';
				}
				if ($data['intSalesWithoutQty']!=0 and $data['intSalesWithoutQty']!=1)
				{
					$upload=0;
					$message=$message.'The intSalesWithoutQty is only allowed 0 or 1, ';
				}
				if ($samename!=null)
				{
					$upload=0;
					$message=$message.'You already have a master data with this name, ';
				}
				
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_item->insert($data);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=item&finish=true';
		}
		if($_GET['type']=='ib')
		{
			$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAdjustment');
			$data['DocDate'] = $_SESSION['Date'][0];
			$data['RefNum'] = 'Initial Balance';
			$data['Whs'] = $this->m_location->GetIDByName($_SESSION['Location'][0]);
			$data['Type'] = $_SESSION['Type'][0];
			$data['Remarks'] = 'Initial Balance';
			$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
			if($data['Whs']!=null and ( $data['Type']=='GR' or $data['Type']=='GI') and DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE)
			{
				$this->db->trans_begin();
				$head=$this->m_adjustment->insertH($data);
				if($head!=0)
				{
					$itemuplod=0;
					for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
					{
						$message='';
						$upload=1;
						
						if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
						{
						  // it's a date
						}
						else
						{
							$upload=0;
							$message=$message.'Your Date Format is Wrong, ';
						}
						$item=$this->m_item->GetIDByCode($_SESSION['ItemCode'][$i]);
						if($item==null)
						{
							$upload=0;
							$message=$message.'Item Code Not Found, ';
						}
						if($_SESSION['Type'][$i]!='GR' and $_SESSION['Type'][$i]!='GI')
						{
							$upload=0;
							$message=$message.'Unknown Type, ';
						}
						$dwhs=$this->m_location->GetIDByName($_SESSION['Location'][$i]);
						if($dwhs==null)
						{
							$upload=0;
							$message=$message.'Location Not Found, ';
						}
						if(is_numeric($_SESSION['Qty'][$i])==false)
						{
							$upload=0;
							$message=$message.'The Qty is only allowed with the format number, ';
						}
						if(is_numeric($_SESSION['Cost'][$i])==false)
						{
							$upload=0;
							$message=$message.'The Cost is only allowed with the format number, ';
						}
						if($upload==1)	
						{
							$cek=1;
							$item=$this->m_item->GetIDByCode($_SESSION['ItemCode'][$i]);
							$da['intHID']=$head;
							$da['itemcodeAD']=$_SESSION['ItemCode'][$i];
							$da['itemnameAD']=$this->m_item->GetNameByCode($_SESSION['ItemCode'][$i]);
							$da['Whs']=$data['Whs'];
							if($data['Type']=='GR')
							{
								$da['qtyAD']=$_SESSION['Qty'][$i];
							}
							else if($data['Type']=='GI')
							{
								$da['qtyAD']=$_SESSION['Qty'][$i]*-1;
							}
							$da['uomAD']=$this->m_item->GetUoMByID($item);
							$da['costAD']=$_SESSION['Cost'][$i];
							$detail=$this->m_adjustment->insertD($da);
							$itemuplod++;
							$this->m_stock->updateStock($item,$da['qtyAD'],$data['Whs']);//update stok menambah/mengurangi di gudang
							if($data['Type']=='GR')
							{
								$this->m_stock->updateCost($item,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
							}
							$this->m_stock->addMutation($item,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data['DocDate'],$data['DocNum']);//add mutation
							$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
						}
						else
						{
							$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
						}
					}
				}
				if($itemuplod==0)
				{
					$this->m_adjustment->deleteH($head);
				}
				
				$this->db->trans_complete();
			
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib&finish=true';
			}
			else
			{
				$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib&error=true';
			}
		}
		if($_GET['type']=='price')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_price->GetIDByName($data['Category']);
				
				$data['ItemCode'] = $_SESSION['ItemCode'][$i];
				$data['Item'] = $this->m_item->GetIDByCode($data['ItemCode']);
				
				$data['Price'] = $_SESSION['Price'][$i];
				
				if($data['Category']==null)
				{
					$upload=0;
					$message=$message.'Price Name Not Found, ';
				}
				if($data['Item']==null)
				{
					$upload=0;
					$message=$message.'Item Not Found, ';
				}
				if (is_numeric($data['Price'])==false)
				{
					$upload=0;
					$message=$message.'Price is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_price->editByItem($data['Category'],$data['Item'],$data['Price']);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=price&finish=true';
		}
		if($_GET['type']=='bp')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				//general
				$data['Type'] = $_SESSION['Type'][$i];
				$data['Category'] = $_SESSION['Category'][$i];
				$data['Category'] = $this->m_bp_category->GetIDByName($data['Category']);
				$data['Code'] = $this->m_bp->GetLastCode($data['Type']);
				$data['Name'] = $_SESSION['Name'][$i];
				$data['TaxNumber'] = $_SESSION['TaxNumber'][$i];
				$data['PriceList'] = $_SESSION['PriceList'][$i];
				$data['PriceList'] = $this->m_price->GetIDByName($data['PriceList']);
				$data['CreditLimit'] = $_SESSION['CreditLimit'][$i];
				$data['PaymentTerm'] = $_SESSION['PaymentTerm'][$i];
				$data['Tax'] = $_SESSION['Tax'][$i];
				$data['Tax'] = $this->m_tax->GetIDByCode($data['Tax']);
				//contact
				$data['Phone1'] = $_SESSION['Phone1'][$i];
				$data['Phone2'] = $_SESSION['Phone2'][$i];
				$data['Fax'] = $_SESSION['Fax'][$i];
				$data['Email'] = $_SESSION['Email'][$i];
				$data['Website'] = $_SESSION['Website'][$i];
				$data['ContactPerson'] = $_SESSION['ContactPerson'][$i];
				
				//address
				$data['CityBillTo'] = $_SESSION['CityBillTo'][$i];
				$data['CountryBillTo'] = $_SESSION['CountryBillTo'][$i];
				$data['AddressBillTo'] = $_SESSION['AddressBillTo'][$i];
				$data['CityShipTo'] = $_SESSION['CityShipTo'][$i];
				$data['CountryShipTo'] = $_SESSION['CountryShipTo'][$i];
				$data['AddressShipTo'] = $_SESSION['AddressShipTo'][$i];
				
				if($data['Type']!='C' and $data['Type']!='S')
				{
					$upload=0;
					$message=$message.'Type is only allowed C or S, ';
				}
				if($data['Category']==null)
				{
					$upload=0;
					$message=$message.'Category Not Found, ';
				}
				if($data['PriceList']==null)
				{
					$upload=0;
					$message=$message.'Price Not Found, ';
				}
				if($data['Tax']==0)
				{
					$upload=0;
					$message=$message.'Tax Not Found, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$cek=$this->m_bp->insert($data);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bp&finish=true';
		}
		if($_GET['type']=='ar')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAR');
				$data['BPCode'] = $_SESSION['BPCode'][$i];
				$data['BPName'] = $this->m_bp->getBPName($data['BPCode']);
				$data['RefNum'] = $_SESSION['RefNum'][$i];
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DelDate'] = $_SESSION['DelDate'][$i];
				$data['SalesEmp'] = $_SESSION['SalesEmp'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Service'] = 1;
				
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
				
				$data['DocTotalBefore'] = $_SESSION['DocTotal'][$i];
				$data['DiscPer'] = 0;
				$data['Disc'] = 0;
				$data['Freight'] = 0;
				$data['TaxPer'] = 0;
				$data['Tax'] = 0;
				$data['DocTotal'] = $_SESSION['DocTotal'][$i];
				$data['AmmountTendered'] = 0;
				$data['Change'] = 0;
				$data['PaymentNote'] = '';
				$data['PaymentCode'] = '';
				$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
				
				if($data['BPName']=='')
				{
					$upload=0;
					$message=$message.'BP Code Not Found, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Doc. Date Format is Wrong, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DelDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Del. Date Format is Wrong, ';
				}
				$dwhs=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
				if($dwhs==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['DocTotal'][$i])==false)
				{
					$upload=0;
					$message=$message.'The Doc. Total is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$head=$this->m_ar->insertH($data);
					
					$da['intHID']=$head;
					$da['itemID']=0;
					$da['itemcodeAR']='';
					$da['itemnameAR']=$data['Remarks'];
					$da['qtyAR']=1;
					$da['whsAR']=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
					$da['whsNameAR']=$this->m_location->GetNameByID($da['whsAR']);
					$da['uomAR']   = 'Invoice';
					$da['qtyinvAR']=1;
					$da['idBaseRefAR']=0;
					$da['BaseRefAR']='';
					$da['uominvAR']='Invoice';
					$da['uomtypeAR'] = 0;
					$da['costAR']=0;
					
					$da['priceAR']= $data['DocTotal'];
					$da['discperAR'] = 0;
					$da['discAR'] = 0;
					$da['priceafterAR']=$data['DocTotal'];
					$da['linetotalAR']= $data['DocTotal'];
					$da['linecostAR']=0;
					
					$detail=$this->m_ar->insertD($da);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$this->db->trans_complete();
		
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ar&finish=true';
		}
		if($_GET['type']=='ap')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAP');
				$data['BPCode'] = $_SESSION['BPCode'][$i];
				$data['BPName'] = $this->m_bp->getBPName($data['BPCode']);
				$data['RefNum'] = $_SESSION['RefNum'][$i];
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DelDate'] = $_SESSION['DelDate'][$i];
				$data['SalesEmp'] = $_SESSION['SalesEmp'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Service'] = 1;
				
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
				
				$data['DocTotalBefore'] = $_SESSION['DocTotal'][$i];
				$data['DiscPer'] = 0;
				$data['Disc'] = 0;
				$data['Freight'] = 0;
				$data['TaxPer'] = 0;
				$data['Tax'] = 0;
				$data['DocTotal'] = $_SESSION['DocTotal'][$i];
				$data['AmmountTendered'] = 0;
				$data['Change'] = 0;
				$data['PaymentNote'] = '';
				$data['PaymentCode'] = '';
				$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
				
				if($data['BPName']=='')
				{
					$upload=0;
					$message=$message.'BP Code Not Found, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DocDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Doc. Date Format is Wrong, ';
				}
				if (DateTime::createFromFormat('Y-m-d', $data['DelDate']) !== FALSE) 
				{
				  // it's a date
				}
				else
				{
					$upload=0;
					$message=$message.'Your Del. Date Format is Wrong, ';
				}
				$dwhs=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
				if($dwhs==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['DocTotal'][$i])==false)
				{
					$upload=0;
					$message=$message.'The Doc. Total is only allowed with the format number, ';
				}
				if($upload==1)
				{
					$this->db->trans_begin();
					$head=$this->m_ap->insertH($data);
					
					$da['intHID']=$head;
					$da['itemID']=0;
					$da['itemcodeAP']='';
					$da['itemnameAP']=$data['Remarks'];
					$da['qtyAP']=1;
					$da['whsAP']=$this->m_location->GetIDByCode($_SESSION['Location'][$i]);
					$da['whsNameAP']=$this->m_location->GetNameByID($da['whsAP']);
					$da['uomAP']   = 'Invoice';
					$da['qtyinvAP']=1;
					$da['idBaseRefAP']=0;
					$da['BaseRefAP']='';
					$da['uominvAP']='Invoice';
					$da['uomtypeAP'] = 0;
					$da['costAP']=$data['DocTotal'];
					
					$da['priceAP']= $data['DocTotal'];
					$da['discperAP'] = 0;
					$da['discAP'] = 0;
					$da['priceafterAP']=$data['DocTotal'];
					$da['linetotalAP']= $data['DocTotal'];
					$da['linecostAP']=$data['DocTotal'];
					
					$detail=$this->m_ap->insertD($da);
					$this->db->trans_complete();
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-green">Success</small> ';
				}
				else
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
						
			}
			$this->db->trans_complete();
		
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ap&finish=true';
		}
		if($_GET['type']=='bom')
		{
			for($i=0;$i<$_SESSION['totitemIMPORT'];$i++)
			{
				$message='';
				$upload=1;
				$data['DocNum'] = $this->m_docnum->GetLastDocNum('hBOM');
				$data['DocDate'] = $_SESSION['DocDate'][$i];
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['FGItem'] = $this->m_item->GetNameByCode($_SESSION['FGItem'][$i]);
				$data['Whs'] = $this->m_location->GetIDByCode($_SESSION['Whs'][$i]);
				$data['FGQty'] = $_SESSION['FGQty'][$i];
				$data['Remarks'] = $_SESSION['Remarks'][$i];
				$data['Rev'] = $_SESSION['Rev'][$i];
				$data['FGCost'] = 0;
				$_SESSION['messageIMPORT'][$i]='';
				
				$data['FGItemID'] = $this->m_item->GetIDByName($data['FGItem']);
				$result=$this->m_bom->cekrev($data);
				
				if($result==0)
				{
					$upload=0;
					$message=$message.'Bom Version Already exist, ';
				}
				if($data['FGItem']==null)
				{
					$upload=0;
					$message=$message.'FG Item Not Found, ';
				}
				if($data['Whs']==null)
				{
					$upload=0;
					$message=$message.'Location Not Found, ';
				}
				if(is_numeric($_SESSION['FGQty'][$i])==false)
				{
					$upload=0;
					$message=$message.'FG Qty is only allowed with the format number, ';
				}
				
				if($upload==0)
				{
					$_SESSION['messageIMPORT'][$i]='<small class="label pull-right bg-red">Error</small> '.$message.'';
				}
				else
				{
					$head=0;
					$this->db->trans_begin();
					if($_SESSION['FGItem'][$i]!='')
					{
						$head=$this->m_bom->insertH($data);
					}
					if($head!=0)
					{
						$rollback=0;
						for($j=$i;$j<=$_SESSION['totitemIMPORT'];$j++)
						{
							$detail=0;
							if(isset($_SESSION['FGItem'][$j]))
							{
								if($j==$i or $_SESSION['FGItem'][$j]=='')
								{
									
									
									$item=$this->m_item->GetIDByCode($_SESSION['itemcodeBOM'][$j]);
									if($item==null)
									{
										$rollback=1;
										$message=$message.'Component Item Not Found, ';
									}
									else if(is_numeric($_SESSION['qtyBOM'][$j])==false)
									{
										$upload=0;
										$message=$message.'Component Qty is only allowed with the format number, ';
									}
									else
									{
										$da['intHID']=$head;
										$da['itemcodeBOM']=$_SESSION['itemcodeBOM'][$j];
										$da['itemnameBOM']=$this->m_item->GetNameByCode($_SESSION['itemcodeBOM'][$j]);
										$da['qtyBOM']=$_SESSION['qtyBOM'][$j];
										$da['uomBOM']=$this->m_item->GetUoMByName($da['itemnameBOM']);
										$da['costBOM']=0;
										$da['autoBOM']=$_SESSION['autoBOM'][$j];
										$detail=$this->m_bom->insertD($da);
										
									}
								}
								if($j!=$i and $_SESSION['FGItem'][$j]!='')
								{
									break;
								}
							}
							if($detail==1)
							{
								$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-green">Success</small> ';
							}
							else
							{
								$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-red">Error</small> '.$message.'';
							}
						}
						$i=$j-1;
					}	
					$this->db->trans_complete();
			
					if($this->db->trans_status() === FALSE or $rollback==1)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
				}
			}
			
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bom&finish=true';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
	}
	public function prosesadd()
	{
		$this->emptysession();
		
		if($_FILES['Image']['tmp_name']!='')
		{
			require_once APPPATH.'third_party/excel_reader/excel_reader.php';
			$data= new Spreadsheet_Excel_Reader($_FILES['Image']['tmp_name']);
			$baris=$data->rowcount();
			$_SESSION['totitemIMPORT']=0;
			$j=0;
			for($i=2;$i<=$baris;$i++)
			{
				if($_POST['Type']=='item')
				{
					$_SESSION['Category'][$j]=$data->val($i,1);
					$_SESSION['Code'][$j]=$data->val($i,2);
					$_SESSION['Name'][$j]=$data->val($i,3);
					$_SESSION['Price'][$j]=$data->val($i,4);
					$_SESSION['Tax'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['UoM'][$j]=$data->val($i,7);
					$_SESSION['PurUoM'][$j]=$data->val($i,8);
					$_SESSION['intPurUoM'][$j]=$data->val($i,9);
					$_SESSION['SlsUoM'][$j]=$data->val($i,10);
					$_SESSION['intSlsUoM'][$j]=$data->val($i,11);
					$_SESSION['intSalesItem'][$j]=$data->val($i,12);
					$_SESSION['intPurchaseItem'][$j]=$data->val($i,13);
					$_SESSION['intSalesWithoutQty'][$j]=$data->val($i,14);
					$_SESSION['Barcode'][$j]=$data->val($i,15);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=item';
				}
				if($_POST['Type']=='ib')
				{
					$_SESSION['Date'][$j]=$data->val($i,1);
					$_SESSION['ItemCode'][$j]=$data->val($i,2);
					$_SESSION['Type'][$j]=$data->val($i,3);
					$_SESSION['Location'][$j]=$data->val($i,4);
					$_SESSION['Qty'][$j]=$data->val($i,5);
					$_SESSION['Cost'][$j]=$data->val($i,6);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ib';
				}
				if($_POST['Type']=='price')
				{
					$_SESSION['Category'][$j]=$data->val($i,1);
					$_SESSION['ItemCode'][$j]=$data->val($i,2);
					$_SESSION['Price'][$j]=$data->val($i,3);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=price';
				}
				if($_POST['Type']=='bp')
				{
					$_SESSION['Type'][$j]=$data->val($i,1);
					$_SESSION['Category'][$j]=$data->val($i,2);
					$_SESSION['Code'][$j]=$data->val($i,3);
					$_SESSION['Name'][$j]=$data->val($i,4);
					$_SESSION['TaxNumber'][$j]=$data->val($i,5);
					$_SESSION['PriceList'][$j]=$data->val($i,6);
					$_SESSION['CreditLimit'][$j]=$data->val($i,7);
					$_SESSION['PaymentTerm'][$j]=$data->val($i,8);
					$_SESSION['Tax'][$j]=$data->val($i,9);
					$_SESSION['Phone1'][$j]=$data->val($i,10);
					$_SESSION['Phone2'][$j]=$data->val($i,11);
					$_SESSION['Fax'][$j]=$data->val($i,12);
					$_SESSION['Email'][$j]=$data->val($i,13);
					$_SESSION['Website'][$j]=$data->val($i,14);
					
					$_SESSION['ContactPerson'][$j]=$data->val($i,15);
					$_SESSION['CityBillTo'][$j]=$data->val($i,16);
					$_SESSION['CountryBillTo'][$j]=$data->val($i,17);
					$_SESSION['AddressBillTo'][$j]=$data->val($i,18);
					$_SESSION['CityShipTo'][$j]=$data->val($i,19);
					$_SESSION['CountryShipTo'][$j]=$data->val($i,20);
					$_SESSION['AddressShipTo'][$j]=$data->val($i,21);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bp';
				}
				if($_POST['Type']=='ar')
				{
					$_SESSION['BPCode'][$j]=$data->val($i,1);
					$_SESSION['RefNum'][$j]=$data->val($i,2);
					$_SESSION['DocDate'][$j]=$data->val($i,3);
					$_SESSION['DelDate'][$j]=$data->val($i,4);
					$_SESSION['SalesEmp'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['DocTotal'][$j]=$data->val($i,7);
					$_SESSION['Location'][$j]=$data->val($i,8);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ar';
				}
				if($_POST['Type']=='ap')
				{
					$_SESSION['BPCode'][$j]=$data->val($i,1);
					$_SESSION['RefNum'][$j]=$data->val($i,2);
					$_SESSION['DocDate'][$j]=$data->val($i,3);
					$_SESSION['DelDate'][$j]=$data->val($i,4);
					$_SESSION['SalesEmp'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['DocTotal'][$j]=$data->val($i,7);
					$_SESSION['Location'][$j]=$data->val($i,8);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=ap';
				}
				if($_POST['Type']=='bom')
				{
					$_SESSION['DocDate'][$j]=$data->val($i,1);
					$_SESSION['FGItem'][$j]=$data->val($i,2);
					$_SESSION['Whs'][$j]=$data->val($i,3);
					$_SESSION['FGQty'][$j]=$data->val($i,4);
					$_SESSION['Rev'][$j]=$data->val($i,5);
					$_SESSION['Remarks'][$j]=$data->val($i,6);
					$_SESSION['itemcodeBOM'][$j]=$data->val($i,7);
					$_SESSION['qtyBOM'][$j]=$data->val($i,8);
					$_SESSION['autoBOM'][$j]=$data->val($i,9);
					$_SESSION['messageIMPORT'][$j]='<small class="label pull-right bg-yellow">Pending</small> ';
					$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?data=bom';
				}
				$j++;
			}
			$_SESSION['totitemIMPORT']=$j;
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
		}
		else
		{
			$dt['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=nofile';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$dt[link]\"></head>";exit;
		}
	}
	
}
