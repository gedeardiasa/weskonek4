<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inv_posting extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_inv_posting','',TRUE);
		$this->load->model('m_pid','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemIP']=0;
			unset($_SESSION['idBaseRefIP']);
			unset($_SESSION['itemcodeIP']);
			unset($_SESSION['itemnameIP']);
			unset($_SESSION['qtyIP']);
			unset($_SESSION['qtybeforeIP']);
			unset($_SESSION['uomIP']);
			unset($_SESSION['costIP']);
			
			$data['typetoolbar']='IP';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Inventory Posting',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_inv_posting->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listpid']=$this->m_pid->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeaderPID()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_pid->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hIP');
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->Whs = $r->intLocation;
			$responce->Remarks = $r->vcRemarks;

		}
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_inv_posting->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hIP');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->Whs = $r->intLocation;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hIP');
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->Whs = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemIP']=0;
			unset($_SESSION['idBaseRefIP']);
			unset($_SESSION['itemcodeIP']);
			unset($_SESSION['itemnameIP']);
			unset($_SESSION['qtyIP']);
			unset($_SESSION['qtybeforeIP']);
			unset($_SESSION['uomIP']);
			unset($_SESSION['costIP']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemIP']=0;
			unset($_SESSION['idBaseRefIP']);
			unset($_SESSION['itemcodeIP']);
			unset($_SESSION['itemnameIP']);
			unset($_SESSION['qtyIP']);
			unset($_SESSION['qtybeforeIP']);
			unset($_SESSION['uomIP']);
			unset($_SESSION['costIP']);
			
			$r=$this->m_inv_posting->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idBaseRefIP'] = '';
				$_SESSION['itemcodeIP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameIP'][$j]=$d->vcItemName;
				$_SESSION['qtyIP'][$j]=$d->intQty;
				$_SESSION['qtybeforeIP'][$j]=$d->intQtyBefore;
				$_SESSION['uomIP'][$j]=$d->vcUoM;
				$_SESSION['costIP'][$j]=$d->intCost;
				$j++;
			}
			$_SESSION['totitemIP']=$j;
		}
	}
	function loaddetailpid()
	{
		$id=$_POST['id'];
		$_SESSION['totitemIP']=0;
		unset($_SESSION['idBaseRefIP']);
		unset($_SESSION['itemcodeIP']);
		unset($_SESSION['itemnameIP']);
		unset($_SESSION['qtyIP']);
		unset($_SESSION['qtybeforeIP']);
		unset($_SESSION['uomIP']);
		unset($_SESSION['costIP']);
		
		$r=$this->m_pid->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			$_SESSION['idBaseRefIP'] = $d->intHID;
			$_SESSION['itemcodeIP'][$j]=$d->vcItemCode;
			$_SESSION['itemnameIP'][$j]=$d->vcItemName;
			$_SESSION['qtyIP'][$j]=$d->intQty;
			$_SESSION['qtybeforeIP'][$j]=$d->intQtyBefore;
			$_SESSION['uomIP'][$j]=$d->vcUoM;
			$_SESSION['costIP'][$j]=$this->m_stock->GetCostItem($d->intItem,$d->intLocation);
			$j++;
		}
		$_SESSION['totitemIP']=$j;
	}
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		echo $cost;
	}
	function loadqty()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$qty=$this->m_stock->GetStockItem($item,$data['Whs']);
		echo $qty;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemIP'];$j++)
		{
			if(isset($_SESSION['itemcodeIP'][$j]))
			{
				if($_SESSION['itemcodeIP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_inv_posting->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemIP'];$j++)// save detail
			{
				if($_SESSION['itemcodeIP'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIP'][$j]);
					$da['intHID']=$head;
					$da['idBaseRefIP']=$_SESSION['idBaseRefIP'][$j];
					
					if($da['idBaseRefIP']!=null and $da['idBaseRefIP']!='' and $da['idBaseRefIP']!=0 and $da['idBaseRefIP']!='0')
					{
						$this->m_pid->closeall($da['idBaseRefIP']);
					}
					$da['itemcodeIP']=$_SESSION['itemcodeIP'][$j];
					$da['itemnameIP']=$_SESSION['itemnameIP'][$j];
					$da['Whs']=$data['Whs'];
					$da['qtyIP']=$_SESSION['qtyIP'][$j];
					$da['qtybeforeIP']=$_SESSION['qtybeforeIP'][$j];
					$da['uomIP']=$_SESSION['uomIP'][$j];
					$da['costIP']=$this->m_stock->GetCostItem($item,$da['Whs']);
					
					$netqty=$da['qtyIP']-$da['qtybeforeIP'];
					$detail=$this->m_inv_posting->insertD($da);
					$this->m_stock->updateStock($item,$netqty,$da['Whs']);//update stok menambah/mengurangi di gudang
					$this->m_stock->addMutation($item,$netqty,$da['costIP'],$da['Whs'],'IP',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
			$_SESSION['totitemIP']=0;
			unset($_SESSION['idBaseRefIP']);
			unset($_SESSION['itemcodeIP']);
			unset($_SESSION['itemnameIP']);
			unset($_SESSION['qtyIP']);
			unset($_SESSION['qtybeforeIP']);
			unset($_SESSION['uomIP']);
			unset($_SESSION['costIP']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_inv_posting->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>Whs</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemIP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemIP'];$j++)
		{
			if($_SESSION['itemcodeIP'][$j]==$code)
			{
				$_SESSION['qtyIP'][$j]=$_POST['detailQty'];
				$_SESSION['qtybeforeIP'][$j]=$_POST['detailQtyBefore'];
				$_SESSION['costIP'][$j]=$_POST['detailCost'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['idBaseRefIP'][$i]='';
				$_SESSION['itemcodeIP'][$i]=$code;
				$_SESSION['itemnameIP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyIP'][$i]=$_POST['detailQty'];
				$_SESSION['qtybeforeIP'][$i]=$_POST['detailQtyBefore'];
				$_SESSION['costIP'][$i]=$_POST['detailCost'];
				$_SESSION['uomIP'][$i]=$UoM;
				$_SESSION['totitemIP']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemIP'];$j++)
		{
			if($_SESSION['itemcodeIP'][$j]==$_POST['code'])
			{
				$_SESSION['idBaseRefIP'][$j]="";
				$_SESSION['itemcodeIP'][$j]="";
				$_SESSION['itemnameIP'][$j]="";
				$_SESSION['qtyIP'][$j]="";
				$_SESSION['qtybeforeIP'][$j]="";
				$_SESSION['uomIP'][$j]="";
				$_SESSION['costIP'][$j]="";
			}
		}
	}
	
	
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty Before</th>
				  <th>Qty</th>
                  <th>UoM</th>
				  <th>Cost</th>
				  <th>Value</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemIP'];$j++)
			{
				if($_SESSION['itemnameIP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIP'][$j]);
					
					$value=($_SESSION['qtyIP'][$j]-$_SESSION['qtybeforeIP'][$j])*$_SESSION['costIP'][$j];
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeIP'][$j].'</td>
						<td>'.$_SESSION['itemnameIP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtybeforeIP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyIP'][$j],'2').'</td>
						<td>'.$_SESSION['uomIP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['costIP'][$j],'2').'</td>
						<td align="right">'.number_format($value,'2').'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameIP"][$j].'\',\''.$_SESSION["itemcodeIP"][$j].'\',\''.$_SESSION["qtybeforeIP"][$j].'\',\''.$_SESSION["qtyIP"][$j].'\',\''.$_SESSION["costIP"][$j].'\',\''.$_SESSION["uomIP"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeIP"][$j].'\',\''.$_SESSION["qtybeforeIP"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
