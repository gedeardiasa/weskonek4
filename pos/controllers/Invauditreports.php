<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invauditreports extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['ItemCode']=isset($_GET['ItemCode'])?$_GET['ItemCode']:''; // get the requested page
			$data['ItemName']=isset($_GET['ItemName'])?$_GET['ItemName']:''; // get the requested page
			$data['ItemGroup']=isset($_GET['ItemGroup'])?$_GET['ItemGroup']:0; // get the requested page
			
			$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
			$dt=explode("-",$data['daterange']);
			$data['from']=str_replace("/","-",$dt[0]);
			$data['until']=str_replace("/","-",$dt[1]);
			
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('stock',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listgrp']=$this->m_item_category->GetAllData();
			$data['autoitem']=$this->m_item->GetAllData();
			$whs = 'and a.intLocation in (0,';
			
			foreach($data['listwhs']->result() as $a)
			{
				$index=$a->vcCode;
				if(isset($_GET["loc".$index]))
				{
					$data['sloc'][$index]=1;
					$whs = $whs."".$a->intID.",";
					
				}
			}
			$whs = rtrim($whs,", ");
			$whs =$whs.")";
			$data['whs']=$whs;
			
			$data['liststock']=$this->m_stock->GetDataAuditStockReport($data);
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterange']);
			$in=1;
			$cekkode='';
			foreach($data['liststock']->result() as $d) 
			{
				if($cekkode!=$d->ItemCode)
				{
					$data['allitem'][$in]=$d->ItemCode;
					$data['allitemname'][$in]=$d->ItemName;
					$data['allitemkey'][$in]=$in;
					$in++;
					$cekkode=$d->ItemCode;
				}
			}
			$data['in']=$in;
			$data['uservalue']=$this->m_user->getByID($_SESSION['IDPOS'])->intValue;
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
}
