<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('item',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation('item');
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_item->GetAllData();
			$data['listcategory']=$this->m_item->GetAllCategory();
			$data['listtax']=$this->m_tax->GetActiveData();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function getDataHeader()
	{
		$code=$this->m_item->GetLastCode($_POST['Category']);
		echo $code;
	}
	public function detail($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('item',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listcategory']=$this->m_item->GetAllCategory();
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['liststock']=$this->m_item->GetListStock($id);
			$data['item']=$this->m_item->getByID($id);
			$data['opentrans']=$this->m_item->GetOpenTranByID($id);
			if(is_object($data['item']))
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['item']->vcCode);
				$this->load->view($this->uri->segment(1).'/detail',$data);
			}
			else
			{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
		}
	}
	function prosesadd()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intCreate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		//general
		$data['Category'] = isset($_POST['Category'])?$_POST['Category']:''; // get the requested page
		$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
		$data['Barcode'] = isset($_POST['Barcode'])?$_POST['Barcode']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Price'] = isset($_POST['Price'])?$_POST['Price']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['UoM'] = isset($_POST['UoM'])?$_POST['UoM']:''; // get the requested page
		$data['PurUoM'] = isset($_POST['PurUoM'])?$_POST['PurUoM']:''; // get the requested page
		$data['intPurUoM'] = isset($_POST['intPurUoM'])?$_POST['intPurUoM']:''; // get the requested page
		$data['SlsUoM'] = isset($_POST['SlsUoM'])?$_POST['SlsUoM']:''; // get the requested page
		$data['intSlsUoM'] = isset($_POST['intSlsUoM'])?$_POST['intSlsUoM']:''; // get the requested page
		
		//setting
		$data['intSalesItem'] = isset($_POST['intSalesItem'])?1:''; // get the requested page
		$data['intPurchaseItem'] = isset($_POST['intPurchaseItem'])?1:''; // get the requested page
		$data['intSalesWithoutQty'] = isset($_POST['intSalesWithoutQty'])?1:''; // get the requested page
		
		if($_FILES['Image']['tmp_name']!='')
		{
			$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
			$data['imgDataAsli']=$_FILES['Image']['tmp_name'];
			$data['imgDataName']=$_FILES['Image']['name'];
		}
		else
		{
			$data['imgData']=null;
		}
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Code']);
		$cek=$this->m_item->insert($data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	
	function prosesedit($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		//general
		$data['id'] = $id; // get the requested page
		$data['Category'] = isset($_POST['Category'])?$_POST['Category']:''; // get the requested page
		$data['LastCode'] = isset($_POST['LastCode'])?$_POST['LastCode']:''; // get the requested page
		$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
		$data['Barcode'] = isset($_POST['Barcode'])?$_POST['Barcode']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Price'] = isset($_POST['Price'])?$_POST['Price']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['UoM'] = isset($_POST['UoM'])?$_POST['UoM']:''; // get the requested page
		$data['PurUoM'] = isset($_POST['PurUoM'])?$_POST['PurUoM']:''; // get the requested page
		$data['intPurUoM'] = isset($_POST['intPurUoM'])?$_POST['intPurUoM']:''; // get the requested page
		$data['SlsUoM'] = isset($_POST['SlsUoM'])?$_POST['SlsUoM']:''; // get the requested page
		$data['intSlsUoM'] = isset($_POST['intSlsUoM'])?$_POST['intSlsUoM']:''; // get the requested page
		
		//setting
		$data['intSalesItem'] = isset($_POST['intSalesItem'])?1:''; // get the requested page
		$data['intPurchaseItem'] = isset($_POST['intPurchaseItem'])?1:''; // get the requested page
		$data['intSalesWithoutQty'] = isset($_POST['intSalesWithoutQty'])?1:''; // get the requested page
		$data['intActive'] = isset($_POST['intActive'])?1:''; // get the requested page
		
		if($_FILES['Image']['tmp_name']!='')
		{
			$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
			$data['imgDataAsli']=$_FILES['Image']['tmp_name'];
			$data['imgDataName']=$_FILES['Image']['name'];
		}
		else
		{
			$data['imgData']=null;
		}
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
		$cek=$this->m_item->edit($data);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successedit';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroredit';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function prosesdelete($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_item->delete($id);
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
}
