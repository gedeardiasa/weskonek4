<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_jurnal','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemJE']=0;
			unset($_SESSION['GLCodeJE']);
			unset($_SESSION['GLNameJE']);
			unset($_SESSION['GLCodeJEX']);
			unset($_SESSION['GLNameJEX']);
			unset($_SESSION['DCJE']);
			unset($_SESSION['ValueJE']);
			
			$data['typetoolbar']='JE';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('journal entry',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['autogl']=$this->m_coa->GetAllDataLevel5();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getCOACode()
	{
		$data=$this->m_coa->GetCodeByName($_POST['detailCOA']);
		echo $data;
	}
	function getCOAName()
	{
		$data=$this->m_coa->GetNameByCode($_POST['detailCOACode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_jurnal->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hJE');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->RefType = $r->vcRefType;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
			
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hJE');
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->RefType = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemJE']=0;
			unset($_SESSION['GLCodeJE']);
			unset($_SESSION['GLNameJE']);
			unset($_SESSION['GLCodeJEX']);
			unset($_SESSION['GLNameJEX']);
			unset($_SESSION['DCJE']);
			unset($_SESSION['ValueJE']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemJE']=0;
			unset($_SESSION['GLCodeJE']);
			unset($_SESSION['GLNameJE']);
			unset($_SESSION['GLCodeJEX']);
			unset($_SESSION['GLNameJEX']);
			unset($_SESSION['DCJE']);
			unset($_SESSION['ValueJE']);
			
			$r=$this->m_jurnal->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['GLCodeJE'][$j]=$d->vcGLCode;
				$_SESSION['GLNameJE'][$j]=$d->vcGLName;
				$_SESSION['GLCodeJEX'][$j]=$d->vcGLCodeX;
				$_SESSION['GLNameJEX'][$j]=$d->vcGLNameX;
				$_SESSION['DCJE'][$j]=$d->vcDC;
				$_SESSION['ValueJE'][$j]=$d->intValue;
				$j++;
			}
			$_SESSION['totitemJE']=$j;
		}
	}
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemJE'];$j++)
		{
			if(isset($_SESSION['GLCodeJE'][$j]))
			{
				if($_SESSION['GLCodeJE'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	
	
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['RefType'] = isset($_POST['RefType'])?$_POST['RefType']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_jurnal->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemJE'];$j++)// save detail
			{
				if($_SESSION['GLCodeJE'][$j]!="")
				{
					$cek=1;
					$da['intHID']=$head;
					$da['GLCodeJE']=$_SESSION['GLCodeJE'][$j];
					$da['GLNameJE']=$_SESSION['GLNameJE'][$j];
					$da['GLCodeJEX']=$_SESSION['GLCodeJEX'][$j];
					$da['GLNameJEX']=$_SESSION['GLNameJEX'][$j];
					
					$da['DCJE']=$_SESSION['DCJE'][$j];
					$da['ValueJE']=$_SESSION['ValueJE'][$j];
					$detail=$this->m_jurnal->insertD($da);
				}
			}
			$_SESSION['totitemJE']=0;
			unset($_SESSION['GLCodeJE']);
			unset($_SESSION['GLNameJE']);
			unset($_SESSION['GLCodeJEX']);
			unset($_SESSION['GLNameJEX']);
			unset($_SESSION['DCJE']);
			unset($_SESSION['ValueJE']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_jurnal->GetAllDataWithDate($data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Ref. Type</th>
                  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRefType.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemJE'];
		$code=$this->m_coa->GetCodeByName($_POST['detailCOA']);
		$codex=$this->m_coa->GetCodeByName($_POST['detailCOAX']);
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemJE'];$j++)
		{
			if($_SESSION['GLCodeJE'][$j]==$code)
			{
				$_SESSION['GLCodeJEX'][$j]=$codex;
				$_SESSION['GLNameJEX'][$j]=$_POST['detailCOAX'];
				$_SESSION['DCJE'][$j]=$_POST['detailDC'];
				$_SESSION['ValueJE'][$j]=$_POST['detailValue'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['GLCodeJE'][$i]=$code;
				$_SESSION['GLNameJE'][$i]=$_POST['detailCOA'];
				$_SESSION['GLCodeJEX'][$i]=$codex;
				$_SESSION['GLNameJEX'][$i]=$_POST['detailCOAX'];
				$_SESSION['DCJE'][$i]=$_POST['detailDC'];
				$_SESSION['ValueJE'][$i]=$_POST['detailValue'];
				$_SESSION['totitemJE']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemJE'];$j++)
		{
			if($_SESSION['GLCodeJE'][$j]==$_POST['code'])
			{
				$_SESSION['GLCodeJE'][$j]="";
				$_SESSION['GLNameJE'][$j]="";
				$_SESSION['GLCodeJEX'][$j]="";
				$_SESSION['GLNameJEX'][$j]="";
				$_SESSION['DCJE'][$j]="";
				$_SESSION['ValueJE'][$j]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Type</th>
                  <th>Code</th>
				  <th>Name</th>
				  <th>Value</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
				
			for($j=0;$j<$_SESSION['totitemJE'];$j++)
			{
				if($_SESSION['GLNameJE'][$j]!="")
				{
					echo '
					<tr>
						<td>'.$_SESSION['GLCodeJE'][$j].'</td>
						<td>'.$_SESSION['GLNameJE'][$j].'</td>
						<td>'.$_SESSION['DCJE'][$j].'</td>
						<td>'.$_SESSION['GLCodeJEX'][$j].'</td>
						<td>'.$_SESSION['GLNameJEX'][$j].'</td>
						<td align="right">'.number_format($_SESSION['ValueJE'][$j],'2').'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["GLNameJE"][$j].'\',\''.$_SESSION["GLCodeJE"][$j].'\',\''.$_SESSION["GLNameJEX"][$j].'\',\''.$_SESSION["GLCodeJEX"][$j].'\',\''.$_SESSION["DCJE"][$j].'\',\''.$_SESSION["ValueJE"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["GLCodeJE"][$j].'\',\''.$_SESSION["ValueJE"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
