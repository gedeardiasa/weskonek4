<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outpay extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_outpay','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_apcm','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemOUTPAY']=0;
			unset($_SESSION['docnumOUTPAY']);
			unset($_SESSION['idOUTPAY']);
			unset($_SESSION['idBaseRefOUTPAY']);
			unset($_SESSION['checkOUTPAY']);
			
			$data['typetoolbar']='OUTPAY';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Outgoing Payment',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_outpay->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwallet']=$this->m_wallet->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autobp']=$this->m_bp->GetAllData();
			$data['autobpcategory']=$this->m_bp->GetAllCategory();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getBPCode()
	{
		$data=$this->m_bp->getBPCode($_POST['BPName']);
		echo $data;
	}
	function getBPName()
	{
		$data=$this->m_bp->getBPName($_POST['BPCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_outpay->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hOUTPAY');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->Remarks = $r->vcRemarks;
			
			$responce->intAmount = $r->intAmount;
			$responce->intWallet = $r->intWallet;
			$responce->intApplied = $r->intApplied;
			$responce->intBalance = $r->intBalance;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hOUTPAY');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->Remarks = '';
			
			$responce->intAmount = 0;
			$responce->intWallet = 0;
			$responce->intApplied = 0;
			$responce->intBalance = 0;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemOUTPAY']=0;
			unset($_SESSION['docnumOUTPAY']);
			unset($_SESSION['idOUTPAY']);
			unset($_SESSION['idBaseRefOUTPAY']);
			unset($_SESSION['checkOUTPAY']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemOUTPAY']=0;
			unset($_SESSION['docnumOUTPAY']);
			unset($_SESSION['idOUTPAY']);
			unset($_SESSION['idBaseRefOUTPAY']);
			unset($_SESSION['checkOUTPAY']);
			
			$r=$this->m_outpay->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idOUTPAY'][$j]=$d->intID;
				$_SESSION['HidOUTPAY'][$j]=$d->intHID;
				$_SESSION['idBaseRefOUTPAY'][$j]=$d->intBaseRef;
				$_SESSION['BaseRefOUTPAY'][$j]=$d->vcBaseType;
				$_SESSION['docnumOUTPAY'][$j]=$d->vcDocNum;
				$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal;
				$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied;
				
				$j++;
			}
			$_SESSION['totitemOUTPAY']=$j;
		}
	}
	function loaddataap()
	{
		$id=$this->m_bp->getIDByCode($_POST['BPCode']);
		$_SESSION['totitemOUTPAY']=0;
		unset($_SESSION['docnumOUTPAY']);
		unset($_SESSION['idOUTPAY']);
		unset($_SESSION['idBaseRefOUTPAY']);
		unset($_SESSION['checkOUTPAY']);
		
		$r=$this->m_ap->GetHeaderOpenByBPID($id);
		$r3=$this->m_apcm->GetHeaderOpenByBPID($id);
		$r2=$this->m_arcm->GetHeaderOpenByBPID($id);
		$j=0;
		foreach($r3->result() as $d)
		{
			$_SESSION['idOUTPAY'][$j]=0;
			$_SESSION['HidOUTPAY'][$j]=0;
			$_SESSION['idBaseRefOUTPAY'][$j]=$d->intID;
			$_SESSION['BaseRefOUTPAY'][$j]='APCM';
			//$_SESSION['BaseRefOUTPAY'][$j]='ARCM';
			$_SESSION['docnumOUTPAY'][$j]=$d->vcDocNum;
			$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal*-1;
			$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied*-1;
			//$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal;
			//$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied;
			
			$j++;
		}
		foreach($r2->result() as $d)
		{
			$_SESSION['idOUTPAY'][$j]=0;
			$_SESSION['HidOUTPAY'][$j]=0;
			$_SESSION['idBaseRefOUTPAY'][$j]=$d->intID;
			//$_SESSION['BaseRefOUTPAY'][$j]='APCM';
			$_SESSION['BaseRefOUTPAY'][$j]='ARCM';
			$_SESSION['docnumOUTPAY'][$j]=$d->vcDocNum;
			//$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal*-1;
			//$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied*-1;
			$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal;
			$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied;
			
			$j++;
		}
		foreach($r->result() as $d)
		{
			$_SESSION['idOUTPAY'][$j]=0;
			$_SESSION['HidOUTPAY'][$j]=0;
			$_SESSION['idBaseRefOUTPAY'][$j]=$d->intID;
			$_SESSION['BaseRefOUTPAY'][$j]='AP';
			$_SESSION['docnumOUTPAY'][$j]=$d->vcDocNum;
			$_SESSION['DocTotalOUTPAY'][$j]=$d->intDocTotal;
			$_SESSION['AppliedOUTPAY'][$j]=$d->intApplied;
			
			$j++;
		}
		$_SESSION['totitemOUTPAY']=$j;
		
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemOUTPAY'];$j++)
		{
			if(isset($_SESSION['checkOUTPAY'][$j]))
			{
				if($_SESSION['checkOUTPAY'][$j]==1)
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['AppliedAmount'] = isset($_POST['AppliedAmount'])?$_POST['AppliedAmount']:''; // get the requested page
		$data['BalanceDue'] = isset($_POST['BalanceDue'])?$_POST['BalanceDue']:''; // get the requested page
		$data['Wallet'] = isset($_POST['Wallet'])?$_POST['Wallet']:''; // get the requested page
		
		$sisa=$data['AppliedAmount'];
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_outpay->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemOUTPAY'];$j++)// save detail
			{
				if($_SESSION['docnumOUTPAY'][$j]!="")
				{
					if(isset($_SESSION['checkOUTPAY'][$j]) and $_SESSION['checkOUTPAY'][$j]==1)
					{
						
						
						$da['intHID']=$head;
						$da['intBaseRef']=$_SESSION['idBaseRefOUTPAY'][$j];
						$da['vcBaseType']=$_SESSION['BaseRefOUTPAY'][$j];
						$da['vcDocNum']=$_SESSION['docnumOUTPAY'][$j];
						$da['intDocTotal']=$_SESSION['DocTotalOUTPAY'][$j];
						$da['intBalance']=$_SESSION['DocTotalOUTPAY'][$j]-$_SESSION['AppliedOUTPAY'][$j];
						
						if($da['intBalance']<=$sisa)
						{
							$da['intApplied']=$da['intBalance'];
							$sisa=$sisa-$da['intBalance'];
						}
						else
						{
							if($sisa<=0)
							{
								$da['intApplied']=0;
							}
							else
							{
								$da['intApplied']=$sisa;
							}
							$sisa=$sisa-$da['intBalance'];
						}
						$detail=$this->m_outpay->insertD($da);
						$da['intAppliedminus']=$da['intApplied']*-1;
						$this->m_wallet->addMutation($data['Wallet'],$data['DocDate'],$da['vcBaseType'],$da['intAppliedminus'],$da['vcDocNum'],$data['DocNum']);// add mutation wallet
						$this->m_wallet->updateBalance($data['Wallet'],$da['intAppliedminus']); // change wallet balance
						$da['intAppliedminus']=$da['intApplied']*-1;
						if($da['vcBaseType']=='AP')
						{
							$this->m_ap->cekclose($da['intBaseRef'],$da['intApplied']); // set ap to close
						}
						else if($da['vcBaseType']=='APCM')
						{
							$this->m_apcm->cekclose($da['intBaseRef'],$da['intAppliedminus']); // set ar to close
						}
						else if($da['vcBaseType']=='ARCM')
						{
							$this->m_arcm->cekclose($da['intBaseRef'],$da['intApplied']); // set ar to close
						}
					}
				}
				
			}
			
		}
		$_SESSION['totitemOUTPAY']=0;
		unset($_SESSION['docnumOUTPAY']);
		unset($_SESSION['idOUTPAY']);
		unset($_SESSION['idBaseRefOUTPAY']);
		unset($_SESSION['checkOUTPAY']);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_outpay->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>BP</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->BPName.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->db->trans_begin();
		$this->m_outpay->closeall($_POST['idHeader']); // close document hInpay
		$header = $this->m_outpay->GetHeaderByHeaderID($_POST['idHeader']);
		$detail = $this->m_outpay->GetDetailByHeaderID($_POST['idHeader']);
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'cancel',$header->vcDocNum);
		foreach($detail->result() as $d)
		{
			$dintApplied = $d->intApplied;
			$basetype=$d->vcBaseType."X";
			$this->m_wallet->addMutation($header->intWallet,$header->dtDate,$basetype,$dintApplied,$d->vcDocNum,$header->vcDocNum);// add mutation wallet
			$this->m_wallet->updateBalance($header->intWallet,$dintApplied); // change wallet balance
			
			$intBaseRef = $d->intBaseRef;
			$vcBaseType = $d->vcBaseType;
			
			
			if($vcBaseType=='AP')
			{
				$dintApplied = $d->intApplied;
				$this->m_ap->rollback($intBaseRef, $dintApplied); // rollback ar
			}
			else if($vcBaseType=='APCM')
			{
				$dintApplied = $d->intApplied*-1;
				$this->m_apcm->rollback($intBaseRef, $dintApplied); // rollback apcm
			}
			else if($vcBaseType=='ARCM')
			{
				$dintApplied = $d->intApplied;
				$this->m_arcm->rollback($intBaseRef, $dintApplied); // rollback arcm
			}
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		for($j=0;$j<$_SESSION['totitemOUTPAY'];$j++)
		{
			$kodecek=$_SESSION['docnumOUTPAY'][$j].''.$_SESSION['BaseRefOUTPAY'][$j].'';
			if($kodecek==$_POST['doc'])
			{
				if($_POST['type']=='add')
				{
					$_SESSION['checkOUTPAY'][$j]=1;
					echo 1;
				}
				else
				{
					$_SESSION['checkOUTPAY'][$j]=0;
					echo 0;
				}
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>';
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
				  <th style="width:15px">Control</th>
			';
		}
		
		echo '
				  <th>Doc. Number</th>
				  <th>Doc. Type</th>
				  <th>Total</th>
				  <th>Applied</th>
                  <th>Balance</th>
				  
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemOUTPAY'];$j++)
			{
				if($_SESSION['docnumOUTPAY'][$j]!="")
				{
					$balanceOUTPAY=$_SESSION['DocTotalOUTPAY'][$j]-$_SESSION['AppliedOUTPAY'][$j];
					echo '
					<tr>';
					$kodecek=$_SESSION['docnumOUTPAY'][$j].''.$_SESSION['BaseRefOUTPAY'][$j].'';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
								<div id="controldetail">
								<input type="checkbox" id="check'.$_SESSION['docnumOUTPAY'][$j].''.$_SESSION['BaseRefOUTPAY'][$j].'" onclick="addDetail(\''.$balanceOUTPAY.'\',\''.$kodecek.'\')">
								</div>
							</td>
						';
					}
					
					echo'
						<td>'.$_SESSION['docnumOUTPAY'][$j].'</td>
						<td>'.$_SESSION['BaseRefOUTPAY'][$j].'</td>
						<td align="right">'.number_format($_SESSION['DocTotalOUTPAY'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['AppliedOUTPAY'][$j],'2').'</td>
						<td align="right">'.number_format($balanceOUTPAY,'2').'</td>
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
