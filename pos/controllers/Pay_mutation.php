<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay_mutation extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			
			$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
			$dt=explode("-",$data['daterange']);
			$data['from']=str_replace("/","-",$dt[0]);
			$data['until']=str_replace("/","-",$dt[1]);
			
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('stock',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listwallet']=$this->m_wallet->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$walletq = 'and a.intWallet in (0,';
			
			foreach($data['listwallet']->result() as $a)
			{
				$index=$a->vcCode;
				if(isset($_GET["wallet".$index]))
				{
					$data['wallet'][$index]=1;
					$walletq = $walletq."".$a->intID.",";
					
				}
			}
			$walletq = rtrim($walletq,", ");
			$walletq =$walletq.")";
			$data['walletq']=$walletq;
			
			$data['listmutation']=$this->m_wallet->GetDataMutation($data);
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterange']);
			$in=1;
			$cekwallet='';
			foreach($data['listmutation']->result() as $d) 
			{
				if($cekwallet!=$d->intWallet)
				{
					$data['allitem'][$in]=$d->WalletName;
					$data['allitemkey'][$in]=$in;
					$in++;
					$cekwallet=$d->intWallet;
				}
			}
			$data['in']=$in;
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
}
