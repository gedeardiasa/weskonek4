<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('plan',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_plan->GetAllData();
			$data['autocountry']=$this->m_plan->GetAllCountry();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "add":
				
				if ($data['crudaccess']->intCreate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['Address'] = isset($_POST['Address'])?$_POST['Address']:''; // get the requested page
				$data['City'] = isset($_POST['City'])?$_POST['City']:''; // get the requested page
				$data['State'] = isset($_POST['State'])?$_POST['State']:''; // get the requested page
				$data['Country'] = isset($_POST['Country'])?$_POST['Country']:''; // get the requested page
				$data['Telp'] = isset($_POST['Telp'])?$_POST['Telp']:''; // get the requested page
				$data['Fax'] = isset($_POST['Fax'])?$_POST['Fax']:''; // get the requested page
				$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
				
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Code']);
				$cek=$this->m_plan->insert($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/plan?success=true&type=successadd';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/plan?error=true&type=erroradd';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			case "edit":
				if ($data['crudaccess']->intUpdate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['Address'] = isset($_POST['Address'])?$_POST['Address']:''; // get the requested page
				$data['City'] = isset($_POST['City'])?$_POST['City']:''; // get the requested page
				$data['State'] = isset($_POST['State'])?$_POST['State']:''; // get the requested page
				$data['Country'] = isset($_POST['Country'])?$_POST['Country']:''; // get the requested page
				$data['Telp'] = isset($_POST['Telp'])?$_POST['Telp']:''; // get the requested page
				$data['Fax'] = isset($_POST['Fax'])?$_POST['Fax']:''; // get the requested page
				$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
				if($_FILES['Image']['tmp_name']!='')
				{
					$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
				}
				else
				{
					$data['imgData']=null;
				}
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
				$cek=$this->m_plan->edit($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/plan?success=true&type=successedit';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/plan?error=true&type=erroredit';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/plan';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
	function prosesdelete()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$id=$_POST['ID2'];
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_plan->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/plan/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/plan/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
	function getimage()
	{
		$data=$this->m_plan->getByID($_POST['id']);
		echo base64_encode( $data->blpImage );
	}
}
