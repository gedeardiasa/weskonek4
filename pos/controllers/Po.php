<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPO']=0;
			unset($_SESSION['itemcodePO']);
			unset($_SESSION['idPO']);
			unset($_SESSION['idBaseRefPO']);
			unset($_SESSION['itemnamePO']);
			unset($_SESSION['qtyPO']);
			unset($_SESSION['qtyOpenPO']);
			unset($_SESSION['uomPO']);
			unset($_SESSION['pricePO']);
			unset($_SESSION['discPO']);
			unset($_SESSION['whsPO']);
			unset($_SESSION['statusPO']);
			
			$data['typetoolbar']='PO';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('purchase order',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_po->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			$data['autobp']=$this->m_bp->GetAllDataVendor();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('S');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_po->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hPO');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hPO');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				//echo $price;
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemPO']=0;
			unset($_SESSION['itemcodePO']);
			unset($_SESSION['idPO']);
			unset($_SESSION['idBaseRefPO']);
			unset($_SESSION['itemnamePO']);
			unset($_SESSION['qtyPO']);
			unset($_SESSION['qtyOpenPO']);
			unset($_SESSION['uomPO']);
			unset($_SESSION['pricePO']);
			unset($_SESSION['discPO']);
			unset($_SESSION['whsPO']);
			unset($_SESSION['statusPO']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemPO']=0;
			unset($_SESSION['itemcodePO']);
			unset($_SESSION['idPO']);
			unset($_SESSION['idBaseRefPO']);
			unset($_SESSION['itemnamePO']);
			unset($_SESSION['qtyPO']);
			unset($_SESSION['qtyOpenPO']);
			unset($_SESSION['uomPO']);
			unset($_SESSION['pricePO']);
			unset($_SESSION['discPO']);
			unset($_SESSION['whsPO']);
			unset($_SESSION['statusPO']);
			
			$r=$this->m_po->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idPO'][$j]=$d->intID;
				$_SESSION['BaseRefPO'][$j]='';
				$_SESSION['itemcodePO'][$j]=$d->vcItemCode;
				$_SESSION['itemnamePO'][$j]=$d->vcItemName;
				$_SESSION['qtyPO'][$j]=$d->intQty;
				$_SESSION['qtyOpenPO'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomPO'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomPO'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['pricePO'][$j]=$d->intPrice;
				$_SESSION['discPO'][$j]=$d->intDiscPer;
				$_SESSION['whsPO'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusPO'][$j]='O';
				}
				else
				{
					$_SESSION['statusPO'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemPO']=$j;
		}
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemPO'];$j++)
		{
			if(($_SESSION['itemcodePO'][$j]!='' and $_SESSION['itemcodePO'][$j]!=null) or ($_SESSION['itemnamePO'][$j]!='' and $_SESSION['itemnamePO'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtyPO'][$j]*$_SESSION['pricePO'][$j])-($_SESSION['discPO'][$j]/100*($_SESSION['qtyPO'][$j]*$_SESSION['pricePO'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemPO'];$j++)
		{
			if(isset($_SESSION['itemcodePO'][$j]) or isset($_SESSION['itemnamePO'][$j]))
			{
				if($_SESSION['itemcodePO'][$j]!='' or $_SESSION['itemnamePO'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_po->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPO'];$j++)// save detail
			{
				if($_SESSION['itemcodePO'][$j]!="" or $_SESSION['itemnamePO'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnamePO'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePO'][$j]);
					}
					else
					{
						$item=0;
					}
					$whs=$this->m_location->GetNameByID($_SESSION['whsPO'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodePO']=$_SESSION['itemcodePO'][$j];
					$da['itemnamePO']=$_SESSION['itemnamePO'][$j];
					$da['qtyPO']=$_SESSION['qtyPO'][$j];
					$da['whsPO']=$_SESSION['whsPO'][$j];
					$da['whsNamePO']=$whs;
					
					if($_SESSION['uomPO'][$j]==1 and $data['Service']==0)
					{
						$da['uomPO']=$vcUoM->vcUoM;
						$da['qtyinvPO']=$da['qtyPO'];
					}
					else if($_SESSION['uomPO'][$j]==2 and $data['Service']==0)
					{
						$da['uomPO']=$vcUoM->vcSlsUoM;
						$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intSlsUoM',1);
					}
					else if($_SESSION['uomPO'][$j]==3 and $data['Service']==0)
					{
						$da['uomPO']=$vcUoM->vcPurUoM;
						$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intPurUoM',1);
					}
					else
					{
						$da['uomPO']   = $_SESSION['uomPO'][$j];
						$da['qtyinvPO']= $da['qtyPO'];
					}
					
					if(isset($_SESSION['idBaseRefPO'][$j]))
					{
						$da['idBaseRefPO']=$_SESSION['idBaseRefPO'][$j];
						$da['BaseRefPO']=$_SESSION['BaseRefPO'][$j];
					}
					else
					{
						$da['idBaseRefPO']=0;
						$da['BaseRefPO']='';
					}
					
					
					$da['uomtypePO']=$_SESSION['uomPO'][$j];
					if($data['Service']==0)
					{
						$da['uominvPO']=$vcUoM->vcUoM;
					}
					else
					{
						$da['uominvPO'] = $da['uomPO'];
					}
					
					$da['pricePO']= $_SESSION['pricePO'][$j];
					$da['discperPO'] = $_SESSION['discPO'][$j];
					$da['discPO'] = $_SESSION['discPO'][$j]/100*$da['pricePO'];
					$da['priceafterPO']=(100-$_SESSION['discPO'][$j])/100*$da['pricePO'];
					$da['linetotalPO']= $da['priceafterPO']*$da['qtyPO'];
					$detail=$this->m_po->insertD($da);
				}
			}
			$_SESSION['totitemPO']=0;
			unset($_SESSION['itemcodePO']);
			unset($_SESSION['idPO']);
			unset($_SESSION['idBaseRefPO']);
			unset($_SESSION['itemnamePO']);
			unset($_SESSION['qtyPO']);
			unset($_SESSION['qtyOpenPO']);
			unset($_SESSION['uomPO']);
			unset($_SESSION['pricePO']);
			unset($_SESSION['discPO']);
			unset($_SESSION['whsPO']);
			unset($_SESSION['statusPO']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_po->editH($data);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemPO'];$j++)// save detail
			{
				if($_SESSION['statusPO'][$j]=='O')
				{
					$cek=1;
					
					if($_SESSION['itemcodePO'][$j]=='' and $_SESSION['itemnamePO'][$j]=='')//jika tidak ada item code / item name artinya detail ini dihapus
					{
						if(isset($_SESSION['idPO'][$j]))
						{
							$this->m_po->deleteD($_SESSION['idPO'][$j]);
						}
					}
					else
					{
						if($data['Service']==0)
						{
							$item=$this->m_item->GetIDByName($_SESSION['itemnamePO'][$j]);
							$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePO'][$j]);
						}
						else
						{
							$item=0;
						}
						
						$whs=$this->m_location->GetNameByID($_SESSION['whsPO'][$j]);
						
						
						$da['intHID']=$data['id'];
						$da['itemID']=$item;
						$da['itemcodePO']=$_SESSION['itemcodePO'][$j];
						$da['itemnamePO']=$_SESSION['itemnamePO'][$j];
						$da['qtyPO']=$_SESSION['qtyPO'][$j];
						$da['whsPO']=$_SESSION['whsPO'][$j];
						$da['whsNamePO']=$whs;
						
						if($_SESSION['uomPO'][$j]==1 and $data['Service']==0)
						{
							$da['uomPO']=$vcUoM->vcUoM;
							$da['qtyinvPO']=$da['qtyPO'];
						}
						else if($_SESSION['uomPO'][$j]==2 and $data['Service']==0)
						{
							$da['uomPO']=$vcUoM->vcSlsUoM;
							$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intSlsUoM',1);
						}
						else if($_SESSION['uomPO'][$j]==3 and $data['Service']==0)
						{
							$da['uomPO']=$vcUoM->vcPurUoM;
							$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intPurUoM',1);
						}
						else
						{
							$da['uomPO']   = $_SESSION['uomPO'][$j];
							$da['qtyinvPO']= $da['qtyPO'];
						}
						$da['uomtypePO']=$_SESSION['uomPO'][$j];
						if($data['Service']==0)
						{
							$da['uominvPO']=$vcUoM->vcUoM;
						}
						else
						{
							$da['uominvPO'] = $da['uomPO'];
						}
						
						$da['pricePO']= $_SESSION['pricePO'][$j];
						$da['discperPO'] = $_SESSION['discPO'][$j];
						$da['discPO'] = $_SESSION['discPO'][$j]/100*$da['pricePO'];
						$da['priceafterPO']=(100-$_SESSION['discPO'][$j])/100*$da['pricePO'];
						$da['linetotalPO']= $da['priceafterPO']*$da['qtyPO'];
						
						if(isset($_SESSION['idPO'][$j])) //jika ada session id artinya data detail dirubah
						{
							$da['intID']=$_SESSION['idPO'][$j];
							$detail=$this->m_po->editD($da);
						}
						else //jika tidak artinya ini merupakan detail tambahan
						{
							$detail=$this->m_po->insertD($da);
						}
						
					}
				}
			}
			$_SESSION['totitemPO']=0;
			unset($_SESSION['itemcodePO']);
			unset($_SESSION['idPO']);
			unset($_SESSION['idBaseRefPO']);
			unset($_SESSION['itemnamePO']);
			unset($_SESSION['qtyPO']);
			unset($_SESSION['qtyOpenPO']);
			unset($_SESSION['uomPO']);
			unset($_SESSION['pricePO']);
			unset($_SESSION['discPO']);
			unset($_SESSION['whsPO']);
			unset($_SESSION['statusPO']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_po->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_po->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemPO'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemPO'];$j++)
		{
			if($_SESSION['itemcodePO'][$j]==$code and $code!=null)
			{
				$_SESSION['qtyPO'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenPO'][$j]=$_POST['detailQty'];
				$_SESSION['uomPO'][$j]=$_POST['detailUoM'];
				$_SESSION['pricePO'][$j]=$_POST['detailPrice'];
				$_SESSION['discPO'][$j]=$_POST['detailDisc'];
				$_SESSION['whsPO'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefPO'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnamePO'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtyPO'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenPO'][$j]=$_POST['detailQty'];
				$_SESSION['uomPO'][$j]=$_POST['detailUoMS'];
				$_SESSION['pricePO'][$j]=$_POST['detailPrice'];
				$_SESSION['discPO'][$j]=$_POST['detailDisc'];
				$_SESSION['whsPO'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefPO'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodePO'][$i]=$code;
				$_SESSION['itemnamePO'][$i]=$_POST['detailItem'];
				$_SESSION['qtyPO'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenPO'][$i]=$_POST['detailQty'];
				$_SESSION['uomPO'][$i]=$_POST['detailUoM'];
				$_SESSION['pricePO'][$i]=$_POST['detailPrice'];
				$_SESSION['discPO'][$i]=$_POST['detailDisc'];
				$_SESSION['whsPO'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefPO'][$i]='';
				$_SESSION['statusPO'][$i]='O';
				$_SESSION['totitemPO']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodePO'][$i]='';
					$_SESSION['itemnamePO'][$i]=$_POST['detailItem'];
					$_SESSION['qtyPO'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenPO'][$i]=$_POST['detailQty'];
					$_SESSION['uomPO'][$i]=$_POST['detailUoMS'];
					$_SESSION['pricePO'][$i]=$_POST['detailPrice'];
					$_SESSION['discPO'][$i]=$_POST['detailDisc'];
					$_SESSION['whsPO'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefPO'][$i]='';
					$_SESSION['statusPO'][$i]='O';
					$_SESSION['totitemPO']++;
				}
				else
				{
					echo "false";
				}
				
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemPO'];$j++)
		{
			if($_SESSION['itemcodePO'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodePO'][$j]="";
				$_SESSION['itemnamePO'][$j]="";
				$_SESSION['qtyPO'][$j]="";
				$_SESSION['qtyOpenPO'][$j]="";
				$_SESSION['uomPO'][$j]="";
				$_SESSION['pricePO'][$j]="";
				$_SESSION['discPO'][$j]="";
				$_SESSION['whsPO'][$j]="";
				$_SESSION['BaseRefPO'][$j]='';
			}
			if($_SESSION['itemnamePO'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodePO'][$j]="";
				$_SESSION['itemnamePO'][$j]="";
				$_SESSION['qtyPO'][$j]="";
				$_SESSION['qtyOpenPO'][$j]="";
				$_SESSION['uomPO'][$j]="";
				$_SESSION['pricePO'][$j]="";
				$_SESSION['discPO'][$j]="";
				$_SESSION['whsPO'][$j]="";
				$_SESSION['BaseRefPO'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemPO'];$j++)
			{
				if($_SESSION['itemnamePO'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePO'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnamePO'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsPO'][$j]);
					
					if($_SESSION['uomPO'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomPO'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomPO'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomPO'][$j];
					}
					
					if($_SESSION['discPO'][$j]=='')
					{
						$_SESSION['discPO'][$j]=0;
					}
					
					if($_SESSION['statusPO'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discPO'][$j])/100)*$_SESSION['pricePO'][$j]*$_SESSION['qtyPO'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodePO'][$j].'</td>
						<td>'.$_SESSION['itemnamePO'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenPO'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['pricePO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discPO'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusPO'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnamePO"][$j].'\',\''.$_SESSION["itemcodePO"][$j].'\',\''.$_SESSION["qtyPO"][$j].'\',\''.$_SESSION["uomPO"][$j].'\',\''.$viewUoM.'\',\''.$_SESSION["pricePO"][$j].'\',\''.$_SESSION["discPO"][$j].'\',\''.$_SESSION["whsPO"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodePO"][$j].'\',\''.$_SESSION["itemnamePO"][$j].'\',\''.$_SESSION["qtyPO"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
