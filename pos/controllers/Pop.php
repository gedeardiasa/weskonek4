<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pop extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pos','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_table','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_outpay','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cektopform($this->uri->segment(1));
    }
	public function index()
	{
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPOP']=0;
			unset($_SESSION['itemcodePOP']);
			unset($_SESSION['itemnamePOP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['idDPOP']);
			unset($_SESSION['qtyPOP']);
			unset($_SESSION['pricePOP']);
			unset($_SESSION['discPOP']);
			unset($_SESSION['perluGR']);
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('pop',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			
			$data['autobp']=$this->m_bp->GetAllDataVendor();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('S');
			
			$data['defaultbp']=$this->m_user->getValueUser('intDefaultBP',$_SESSION['IDPOS']);
			$data['defaultbp']=$this->m_bp->getNameByID($data['defaultbp']);
			
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_location->GetNameByID($data['defaultwhs']);
			
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listtable']=$this->m_table->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['production_area']=$this->m_setting->getValueByCode('production_area');
			$data['docsetting']=$this->m_docnum->GetDocSetting('POP');
			
			$data['autowhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function listpo()
	{
		$listso=$this->m_po->GetOpenDataWithPlanAccessAndTable($_SESSION['IDPOS']);
		echo '<table id="examplePO" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>Table</th>
				  <th>BP. Name</th>
				  <th>Date</th>
				  <th>Remarks</th>
                </tr>
                </thead>
                <tbody>';
				foreach($listso->result() as $d) 
				{
                echo '<tr onclick="insertPO(\''.$d->intID.'\');">
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->TableName.'</td>
				  <td>'.$d->vcBPName.'</td>
				  <td>'.date('d F Y',strtotime($d->dtDate)).'</td>
				  <td>'.$d->vcRemarks.'</td>
                </tr>
				';
				}
				echo '
                </tbody>

              </table>';
			  
			  echo '
		
		<script>
		  $(function () {
			$("#exampleSO").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function getItemName()
	{
		$itemname=$this->m_item->GetNameByBarcode($_POST['detailBarcode']);
		echo $itemname;
	}
	/*function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemPOP'];$j++)
		{
			if($_SESSION['itemcodePOP'][$j]!='' and $_SESSION['itemcodePOP'][$j]!=null)
			{
				$total=$total+(($_SESSION['pricePOP'][$j]-($_SESSION['discPOP'][$j]/100*$_SESSION['pricePOP'][$j]))*$_SESSION['qtyPOP'][$j]);
			}
		}
		echo $total;
	}*/
	function loaddetailpo()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemPOP']=0;
		
		$r=$this->m_po->GetDetailByHeaderID($id);
		$j=0;
		$responce=new stdClass();
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				//$_SESSION['idDPOP'][$j]=$d->intID;
				//$_SESSION['idBaseRefAP'][$j]=$d->intHID;
				//$_SESSION['BaseRefAP'][$j]='PO';
				//$_SESSION['itemcodePOP'][$j]=$d->vcItemCode;
				//$_SESSION['itemnamePOP'][$j]=$d->vcItemName;
				//$_SESSION['qtyPOP'][$j]=$d->intOpenQty;
				//$_SESSION['pricePOP'][$j]=$d->intPrice;	
				//$_SESSION['discPOP'][$j]=$d->intDiscPer;	
				$responce->rows[$j]['idDPOP'] = $d->intID;
				$responce->rows[$j]['idBaseRefAP'] = $d->intHID;
				$responce->rows[$j]['BaseRefAP'] = 'PO';
				$responce->rows[$j]['itemnamePOP'] = $d->vcItemName;
				$responce->rows[$j]['qtyPOP'] = $d->intOpenQty;
				$responce->rows[$j]['pricePOP'] = $d->intPrice;
				$responce->rows[$j]['discPOP'] = $d->intDiscPer;
				$j++;
				
			}
		}
		echo json_encode($responce);
		//$_SESSION['totitemPOP']=$j;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByName($_POST['BPName']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekloc()
	{
		$cek=$this->m_location->getByNameandAccessPlan($_POST['WhsName'],$_SESSION['IDPOS']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	/*function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemPOP'];$j++)
		{
			if(isset($_SESSION['itemcodePOP'][$j]))
			{
				if($_SESSION['itemcodePOP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}*/
	/*
	
		HEADER FUNCTION
	
	*/
	function cekclosePO($id,$item,$qty,$qtyinv)
	{
		$this->m_po->cekclose($id,$item,$qty,$qtyinv);
	}
	function getDataHeaderPO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			//$id=28;
			$r=$this->m_po->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			$responce->BPName = $r->vcBPName;
			$responce->Table = $r->intTable;
			$responce->WhsName = $this->m_po->GetWhsNameByHeader($id);
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax = $r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			
		}
		echo json_encode($responce);
	}
	function loadsession($ddidDPOP,$ddidBaseRef,$ddBaseRef,$ddItem,$ddQty,$ddPrice,$ddDisc,$perluGR)
	{
		$arrayddidDPOP=json_decode($ddidDPOP);
		$arrayddidBaseRef=json_decode($ddidBaseRef);
		$arrayddBaseRef=json_decode($ddBaseRef);
		$arrayddItem=json_decode($ddItem);
		$arrayddQty=json_decode($ddQty);
		$arrayddPrice=json_decode($ddPrice);
		$arrayddDisc=json_decode($ddDisc);
		$arrayperluGR=json_decode($perluGR);
		
		for($j=0;$j<count($arrayddItem);$j++)
		{
			$_SESSION['idDPOP'][$j]=$arrayddidDPOP[$j];
			$_SESSION['idBaseRefAP'][$j]=$arrayddidBaseRef[$j];
			$_SESSION['BaseRefAP'][$j]=$arrayddBaseRef[$j];
			$_SESSION['itemcodePOP'][$j]=$this->m_item->GetCodeByName($arrayddItem[$j]);
			$_SESSION['itemnamePOP'][$j]=$arrayddItem[$j];
			$_SESSION['qtyPOP'][$j]=$arrayddQty[$j];
			$_SESSION['pricePOP'][$j]=$arrayddPrice[$j];
			$_SESSION['discPOP'][$j]=$arrayddDisc[$j];
			$_SESSION['perluGR'][$j]=$arrayperluGR[$j];
			$_SESSION['totitemPOP']++;
		}
	}
	function prosessavepo()
	{
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hPO');
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		$data['BPCode'] = $this->m_bp->getBPCode($data['BPName']);
		$data['RefNum'] = ''; 
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = ''; 
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->loadsession($_POST['ddidDPOP'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_po->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPOP'];$j++)// save detail
			{
				if($_SESSION['itemcodePOP'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOP'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOP'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodePO']=$_SESSION['itemcodePOP'][$j];
					$da['itemnamePO']=$_SESSION['itemnamePOP'][$j];
					$da['qtyPO']=$_SESSION['qtyPOP'][$j];
					$da['whsPO']=$this->m_location->GetIDByName($data['WhsName']);
					$da['whsNamePO']=$data['WhsName'];
					
					$da['uomPO']=$vcUoM->vcPurUoM;
					$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intPurUoM',1);
					
					
					$da['idBaseRefPO']=0;
					$da['BaseRefPO']='POP';
					
					$da['uomtypePO']=3; // uom type for sales UoM
					$da['uominvPO']=$vcUoM->vcUoM;
					
					$da['pricePO']= $_SESSION['pricePOP'][$j];
					$da['discperPO'] = $_SESSION['discPOP'][$j];
					$da['discPO'] = $_SESSION['discPOP'][$j]/100*$_SESSION['pricePOP'][$j];
					$da['priceafterPO']=$da['pricePO']-$da['discPO'];
					$da['linetotalPO']= $da['priceafterPO']*$da['qtyPO'];
					$detail=$this->m_po->insertD($da);
				}
			}
			$_SESSION['totitemPOP']=0;
			unset($_SESSION['itemcodePOP']);
			unset($_SESSION['itemnamePOP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['idDPOP']);
			unset($_SESSION['qtyPOP']);
			unset($_SESSION['pricePOP']);
			unset($_SESSION['discPOP']);
			unset($_SESSION['perluGR']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function proseseditpo()
	{
		$data['id']=isset($_POST['IDPO'])?$_POST['IDPO']:''; // get the requested page
		
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = '';
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->loadsession($_POST['ddidDPOP'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
		$this->m_po->editH($data);
		
		if($data['id']!=0)
		{
			$notdelitem = '(';
			for($j=0;$j<$_SESSION['totitemPOP'];$j++)// save detail
			{
				
					$cek=1;
					
					if($_SESSION['itemcodePOP'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
					{
						if(isset($_SESSION['idDPOP'][$j]))
						{
							$this->m_po->deleteD($_SESSION['idDPOP'][$j]);
						}
					}
					else
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnamePOP'][$j]);
						$notdelitem = $notdelitem."".$item.",";
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOP'][$j]);
						$whs=$data['WhsName'];
						
						if(isset($_SESSION['idBaseRefPO'][$j]))
						{
							$da['idBaseRefPO']=$_SESSION['idBaseRefPO'][$j];
							$da['BaseRefPO']=$_SESSION['BaseRefPO'][$j];
						}
						else
						{
							$da['idBaseRefPO']=0;
							$da['BaseRefPO']='';
						}
						
						$da['intHID']=$data['id'];
						$da['itemID']=$item;
						$da['itemcodePO']=$_SESSION['itemcodePOP'][$j];
						$da['itemnamePO']=$_SESSION['itemnamePOP'][$j];
						$da['qtyPO']=$_SESSION['qtyPOP'][$j];
						$da['whsPO']=$this->m_location->GetIDByName($whs);
						$da['whsNamePO']=$whs;
						
						
							$da['uomPO']=$vcUoM->vcPurUoM;
							$da['qtyinvPO']=$this->m_item->convert_qty($item,$da['qtyPO'],'intPurUoM',1);
						
						$da['uomtypePO']=3;
						$da['uominvPO']=$vcUoM->vcUoM;
						
						$da['pricePO']= $_SESSION['pricePOP'][$j];
						$da['discperPO'] = $_SESSION['discPOP'][$j];
						$da['discPO'] = $_SESSION['discPOP'][$j]/100*$_SESSION['pricePOP'][$j];
						$da['priceafterPO']=$da['pricePO']-$da['discPO'];
						$da['linetotalPO']= $da['priceafterPO']*$da['qtyPO'];
						
						if(isset($_SESSION['idDPOP'][$j]) and $_SESSION['idDPOP'][$j]!='' and $_SESSION['idDPOP'][$j]!=null) //jika ada session id artinya data detail dirubah
						{
							$da['intID']=$_SESSION['idDPOP'][$j];
							$detail=$this->m_po->editD($da);
						}
						else //jika tidak artinya ini merupakan detail tambahan
						{
							$detail=$this->m_po->insertD($da);
						}
					}
			}
			$notdelitem = rtrim($notdelitem,", ");
			$notdelitem =$notdelitem.")";
			$this->m_po->deleteD2($data['id'],$notdelitem);
			
			$_SESSION['totitemPOP']=0;
			unset($_SESSION['itemcodePOP']);
			unset($_SESSION['itemnamePOP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['idDPOP']);
			unset($_SESSION['qtyPOP']);
			unset($_SESSION['pricePOP']);
			unset($_SESSION['discPOP']);
			unset($_SESSION['perluGR']);
			
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosessavepayment()
	{
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAP');
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		$data['BPCode'] = $this->m_bp->getBPCode($data['BPName']);
		$data['RefNum'] = ''; 
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = ''; 
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['AmmountTendered'] = isset($_POST['AmmountTendered'])?$_POST['AmmountTendered']:''; // get the requested page
		$data['Change'] = isset($_POST['Change'])?$_POST['Change']:''; // get the requested page
		$data['PaymentNote'] = isset($_POST['PaymentNote'])?$_POST['PaymentNote']:''; // get the requested page
		$data['PaymentCode'] = isset($_POST['PaymentCode'])?$_POST['PaymentCode']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->loadsession($_POST['ddidDPOP'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add','payment');
		//
		// CREATE AR
		//
		$headAP=$this->m_ap->insertH($data); // create AP
		
			for($j=0;$j<$_SESSION['totitemPOP'];$j++)// save detail
			{
				if($_SESSION['itemcodePOP'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOP'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOP'][$j]);
					
					$da['intHID']=$headAP;
					$da['itemID']=$item;
					$da['itemcodeAP']=$_SESSION['itemcodePOP'][$j];
					$da['itemnameAP']=$_SESSION['itemnamePOP'][$j];
					$da['qtyAP']=$_SESSION['qtyPOP'][$j];
					$da['whsAP']=$this->m_location->GetIDByName($data['WhsName']);
					$da['whsNameAP']=$data['WhsName'];
					$hargasetelahdiskon=(100-$_SESSION['discPOP'][$j])/100*$_SESSION['pricePOP'][$j];
					
					$da['uomAP']=$vcUoM->vcPurUoM;
					$da['qtyinvAP']=$this->m_item->convert_qty($item,$da['qtyAP'],'intPurUoM',1);
					
					if(isset($_SESSION['idBaseRefAP'][$j]))
					{
						$da['idBaseRefAP']=$_SESSION['idBaseRefAP'][$j];
						$da['BaseRefAP']=$_SESSION['BaseRefAP'][$j];
					}
					else
					{
						$da['idBaseRefAP']=0;
						$da['BaseRefAP']='POP';
					}
					
					if($da['BaseRefAP']=='PO')
					{
						$this->cekclosePO($da['idBaseRefAP'], $da['itemID'], $da['qtyAP'], $da['qtyinvAP']);
					}
					
					$da['uomtypeAP']=3; // uom type for pur UoM
					$da['uominvAP']=$vcUoM->vcUoM;
					$da['costAP'] = $this->m_item->convert_price($item,$hargasetelahdiskon, 'intPurUoM' ,1);
					
					$da['priceAP']= $_SESSION['pricePOP'][$j];
					$da['discperAP'] = $_SESSION['discPOP'][$j];
					$da['discAP'] = $_SESSION['discPOP'][$j]/100*$_SESSION['pricePOP'][$j];
					$da['priceafterAP']=$da['priceAP']-$da['discAP'];
					$da['linetotalAP']= $da['priceafterAP']*$da['qtyAP'];
					$da['linecostAP']=$da['costAP']*$da['qtyinvAP'];
					
					$detail=$this->m_ap->insertD($da);
					//$da['qtyinvAR']=$da['qtyinvAR']*-1;
					$this->m_stock->updateStock($item,$da['qtyinvAP'],$da['whsAP']);//update stok menambah/mengurangi di gudang
					$this->m_stock->updateCost($item,$da['qtyinvAP'],$da['costAP'],$da['whsAP']);//update cost
					$this->m_stock->addMutation($item,$da['qtyinvAP'],$da['costAP'],$da['whsAP'],'AP',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
		//
		// CREATE OUTPAY
		//
		$data2['DocNum'] = $this->m_docnum->GetLastDocNum('hOUTPAY');
		$data2['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data2['BPCode'] = $this->m_bp->getBPCode($data2['BPName']);
		$data2['RefNum'] = ''; // get the requested page
		$data2['DocDate'] = date('Y-m-d'); 
		$data2['Remarks'] = ''; // get the requested page
		
		
		$data2['DocTotalBefore'] = $data['DocTotal'];
		$data2['AppliedAmount'] = $data['DocTotal'];
		$data2['BalanceDue'] = 0; // get the requested page
		$data2['Wallet'] = isset($_POST['Wallet'])?$_POST['Wallet']:''; // get the requested page
		
		$sisa=$data2['AppliedAmount'];
		
		$data2['BPId']=$this->m_bp->getIDByCode($data2['BPCode']);
		$head=$this->m_outpay->insertH($data2);
		
		
						$da2['intHID']=$head;
						$da2['intBaseRef']=$headAP; // intID AP nya
						$da2['vcBaseType']='AP';
						$da2['vcDocNum']=$data['DocNum']; //nomer dokumen AP nya
						$da2['intDocTotal']=$data['DocTotal'];
						
						if($da2['intDocTotal']<=$sisa)
						{
							$da2['intApplied']=$da2['intDocTotal'];
							$sisa=$sisa-$da2['intDocTotal'];
						}
						else
						{
							if($sisa<=0)
							{
								$da2['intApplied']=0;
							}
							else
							{
								$da2['intApplied']=$sisa;
							}
							$sisa=$sisa-$da2['intDocTotal'];
						}
						$detail=$this->m_outpay->insertD($da2);
						$da2['intAppliedminus']=$da2['intApplied']*-1;
						$this->m_wallet->addMutation($data2['Wallet'],$data2['DocDate'],$da2['vcBaseType'],$da2['intAppliedminus'],$data['DocNum'],$data2['DocNum']);// add mutation wallet
						$this->m_wallet->updateBalance($data2['Wallet'],$da2['intAppliedminus']); // change wallet balance
						if($da2['vcBaseType']=='AP')
						{
							$this->m_ap->cekclose($da2['intBaseRef'],$da2['intApplied']); // set ap to close
						}
		
		echo $headAP;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$_SESSION['totitemPOP']=0;
		unset($_SESSION['itemcodePOP']);
		unset($_SESSION['itemnamePOP']);
		unset($_SESSION['idBaseRefAP']);
		unset($_SESSION['idDPOP']);
		unset($_SESSION['qtyPOP']);
		unset($_SESSION['pricePOP']);
		unset($_SESSION['discPOP']);
		unset($_SESSION['perluGR']);
	}
	function listwallet()
	{
		$plan=$this->m_location->GetPlanIDByName($_GET['WhsName']);
		$wallet=$this->m_wallet->GetAllByPlan($plan);
		$defaultwallet=$this->m_user->getDefaultWallet($_SESSION['IDPOS']);
		
		echo '<select class="" id="Wallet" name="Wallet" style="width: 100%; height:35px">';
				  
		foreach($wallet->result() as $d) 
		{
			if($d->intID==$defaultwallet)
			{
				$selected='selected="true"';
			}
			else
			{
				$selected='';
			}
			echo '<option value="'.$d->intID.'" '.$selected.'>'.$d->vcName.'</option>';
		}
		echo '</select>';
		
		
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	/*function addDetail()
	{
		$i=$_SESSION['totitemPOP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemPOP'];$j++)
		{
			if($_SESSION['itemcodePOP'][$j]==$code)
			{
				$_SESSION['qtyPOP'][$j]=$_SESSION['qtyPOP'][$j]+$_POST['detailQty'];
				$_SESSION['pricePOP'][$j]=$_POST['detailPrice'];
				$_SESSION['discPOP'][$j]=$_POST['detailDisc'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodePOP'][$i]=$code;
				$_SESSION['itemnamePOP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyPOP'][$i]=$_POST['detailQty'];
				$_SESSION['pricePOP'][$i]=$_POST['detailPrice'];
				$_SESSION['discPOP'][$i]=$_POST['detailDisc'];
				$_SESSION['totitemPOP']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemPOP'];$j++)
		{
			if($_SESSION['itemnamePOP'][$j]==$_POST['name'])
			{
				$_SESSION['itemcodePOP'][$j]="";
				$_SESSION['itemnamePOP'][$j]="";
				$_SESSION['qtyPOP'][$j]="";
				$_SESSION['pricePOP'][$j]="";
				$_SESSION['discPOP'][$j]="";
				
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemPOP']=0;
			unset($_SESSION['itemcodePOP']);
			unset($_SESSION['idBaseRefAP']);
			unset($_SESSION['idDPOP']);
		}
	}
	function lisDetail()
	{
		
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th align="center">Item</th>
				  <th align="center">Qty</th>
				  <th align="center">Price</th>
				  <th align="center">Disc</th>
				  <th align="center">Line Total</th>
				  <th align="center" style="width:15px">Control</th>
				</tr>
				</thead>
				<tbody>
		';
			$total=0;
			for($j=0;$j<$_SESSION['totitemPOP'];$j++)
			{
				if($_SESSION['itemnamePOP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOP'][$j]);
					
					if($_SESSION['discPOP'][$j]=='')
					{
						$_SESSION['discPOP'][$j]=0;
					}
					
					$disc=$_SESSION['discPOP'][$j]/100*$_SESSION['pricePOP'][$j];
					$lineTotal=($_SESSION['pricePOP'][$j]-$disc)*$_SESSION['qtyPOP'][$j];
					$total=$total+$lineTotal;
					echo '
					<tr>
						<td>'.$_SESSION['itemnamePOP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPOP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['pricePOP'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discPOP'][$j],'2').'</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td align="center">
							<div id="controldetail">
							<a onclick="delDetail(\''.$_SESSION["itemnamePOP"][$j].'\',\''.$_SESSION["qtyPOP"][$j].'\')">
							<i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</td>
					</tr>
					';
				}
			}			
			
			echo "
				
				</tbody>
			</table><br>
			";
		
		
	}*/
}
