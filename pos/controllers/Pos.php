<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pos','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_table','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_inpay','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_adjustment','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cektopform($this->uri->segment(1));
    }
	public function item()
	{
		$this->load->view('localdata/item');
	}
	public function index()
	{
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPOS']=0;
			unset($_SESSION['itemcodePOS']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['idDPOS']);
			unset($_SESSION['itemnamePOS']);
			unset($_SESSION['qtyPOS']);
			unset($_SESSION['pricePOS']);
			unset($_SESSION['discPOS']);
			unset($_SESSION['perluGR']);
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('pos',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			
			$data['defaultbp']=$this->m_user->getValueUser('intDefaultBP',$_SESSION['IDPOS']);
			$data['defaultbp']=$this->m_bp->getNameByID($data['defaultbp']);
			
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_location->GetNameByID($data['defaultwhs']);
			
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listtable']=$this->m_table->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['production_area']=$this->m_setting->getValueByCode('production_area');
			$data['docsetting']=$this->m_docnum->GetDocSetting('POS');
			
			$data['autowhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function listso()
	{
		//$listso=$this->m_so->GetOpenDataWithPlanAccessAndTable($_SESSION['IDPOS']);
		$listso=$this->m_so->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
		echo '<table id="exampleSO" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Doc. Number</th>
				  <th>
				  <div class="col-sm-3">Table</div>
				  <div class="col-sm-3">BP. Name</div>
				  </th>
                </tr>
                </thead>
                <tbody>';
				foreach($listso->result() as $d) 
				{
                echo '<tr onclick="insertSO(\''.$d->intID.'\');">
                  <td>'.$d->vcDocNum.'</td>
				  <td>
				  <div class="col-sm-3">'.$d->TableName.'</div>
				  <div class="col-sm-3">'.$d->vcBPName.'</div>
				  </td>
                </tr>
				';
				}
				echo '
                </tbody>

              </table>';
			  
			  echo '
		
		<script>
		  $(function () {
			$("#exampleSO").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	/*
	
		LOAD FUNCTION
	
	*/
	/*function getItemName() // digunakan saat scan barcode
	{
		$itemname=$this->m_item->GetNameByBarcode($_POST['detailBarcode']);
		echo $itemname;
	}*/
	function loaddetailso()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemPOS']=0;
		
		$r=$this->m_so->GetDetailByHeaderID($id);
		$j=0;
		$responce=new stdClass();
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$responce->rows[$j]['idDPOS'] = $d->intID;
				$responce->rows[$j]['idBaseRefAR'] = $d->intHID;
				$responce->rows[$j]['BaseRefAR'] = 'SO';
				$responce->rows[$j]['itemnamePOS'] = $d->vcItemName;
				$responce->rows[$j]['qtyPOS'] = $d->intOpenQty;
				$responce->rows[$j]['pricePOS'] = $d->intPrice;
				$responce->rows[$j]['discPOS'] = $d->intDiscPer;
				$j++;
			}
		}
		echo json_encode($responce);
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByName($_POST['BPName']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekloc()
	{
		$cek=$this->m_location->getByNameandAccessPlan($_POST['WhsName'],$_SESSION['IDPOS']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetailstok()
	{
		$hasil='';
		$da['whsPOS']=$this->m_location->GetIDByName($_POST['WhsName']);
		$perlugr=0;
		$this->loadsession($_POST['ddidDPOS'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		
		//prepare data header jika perlu GR
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAdjustment');
		$data['DocDate'] = date('Y-m-d');
		$data['RefNum'] = '';
		$data['Whs'] = $da['whsPOS'];
		$data['Type'] = 'GR';
		$data['Remarks'] = 'auto create by POS';
		//
		
		for($j=0;$j<$_SESSION['totitemPOS'];$j++)
		{
			if($_SESSION['itemcodePOS'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodePOS'][$j]);
				$da['qtyinvPOS']=$this->m_item->convert_qty($item,$_SESSION['qtyPOS'][$j],'intSlsUoM',1);
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvPOS'],$da['whsPOS']);
				if($res==1)
				{
					$hasil=$hasil;
				}
				else
				{
					//jika tidak ada tetap perlu cek settingan item apakah di ijinkan menjual tanpa stok. Disini akan terbentuk dokumen GR otomatis untuk menambah stok
					$dataitem=$this->m_item->getByID($item);
					if($dataitem->intSalesWithoutQty==1)
					{
						$perlugr=1;
						// buat GR terlebih dahulu
						$_SESSION['perluGR'][$j]=1;
						
					}
					else
					{
						$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodePOS'][$j].' - '.$_SESSION['itemnamePOS'][$j].') ';
					}
				}
			}
		}
		if($perlugr==1 and $hasil=='')
		{
			$head=$this->m_adjustment->insertH($data);
			for($j=0;$j<$_SESSION['totitemPOS'];$j++)
			{
				if($_SESSION['itemcodePOS'][$j]!="" and isset($_SESSION['perluGR'][$j]))
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOS'][$j]);
					$da['intHID']=$head;
					$da['itemcodeAD']=$_SESSION['itemcodePOS'][$j];
					$da['itemnameAD']=$_SESSION['itemnamePOS'][$j];
					$da['Whs']=$data['Whs'];
					$da['qtyAD']=$this->m_item->convert_qty($item,$_SESSION['qtyPOS'][$j],'intSlsUoM',1);
					$da['uomAD']=$this->m_item->GetUoMByID($item);
					$da['costAD']=0;
					
					$detail=$this->m_adjustment->insertD($da);
					
					$this->m_stock->updateStock($item,$da['qtyAD'],$data['Whs']);//update stok menambah/mengurangi di gudang
					
					$this->m_stock->updateCost($item,$da['qtyAD'],$da['costAD'],$da['Whs']);//update cost per gudang hanya untuk good receipt
					
					$this->m_stock->addMutation($item,$da['qtyAD'],$da['costAD'],$da['Whs'],'AD',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
		$_SESSION['totitemPOS']=0;
		unset($_SESSION['itemcodePOS']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['idDPOS']);
		unset($_SESSION['itemnamePOS']);
		unset($_SESSION['qtyPOS']);
		unset($_SESSION['pricePOS']);
		unset($_SESSION['discPOS']);
		unset($_SESSION['perluGR']);
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSO($id,$item,$qty,$qtyinv)
	{
		$this->m_so->cekclose($id,$item,$qty,$qtyinv);
	}
	function getDataHeaderSO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			$responce->BPName = $r->vcBPName;
			$responce->Table = $r->intTable;
			$responce->WhsName = $this->m_so->GetWhsNameByHeader($id);
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax = $r->intTax;
			$responce->DocTotal = $r->intDocTotal;
		}
		echo json_encode($responce);
	}
	function loadsession($ddidDPOS,$ddidBaseRef,$ddBaseRef,$ddItem,$ddQty,$ddPrice,$ddDisc,$perluGR)
	{
		$arrayddidDPOS=json_decode($ddidDPOS);
		$arrayddidBaseRef=json_decode($ddidBaseRef);
		$arrayddBaseRef=json_decode($ddBaseRef);
		$arrayddItem=json_decode($ddItem);
		$arrayddQty=json_decode($ddQty);
		$arrayddPrice=json_decode($ddPrice);
		$arrayddDisc=json_decode($ddDisc);
		$arrayperluGR=json_decode($perluGR);
		
		for($j=0;$j<count($arrayddItem);$j++)
		{
			$_SESSION['idDPOS'][$j]=$arrayddidDPOS[$j];
			$_SESSION['idBaseRefAR'][$j]=$arrayddidBaseRef[$j];
			$_SESSION['BaseRefAR'][$j]=$arrayddBaseRef[$j];
			$_SESSION['itemcodePOS'][$j]=$this->m_item->GetCodeByName($arrayddItem[$j]);
			$_SESSION['itemnamePOS'][$j]=$arrayddItem[$j];
			$_SESSION['qtyPOS'][$j]=$arrayddQty[$j];
			$_SESSION['pricePOS'][$j]=$arrayddPrice[$j];
			$_SESSION['discPOS'][$j]=$arrayddDisc[$j];
			$_SESSION['perluGR'][$j]=$arrayperluGR[$j];
			$_SESSION['totitemPOS']++;
		}
	}
	function prosessaveso()
	{
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hSO');
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		$data['BPCode'] = $this->m_bp->getBPCode($data['BPName']);
		$data['RefNum'] = ''; 
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = ''; 
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->loadsession($_POST['ddidDPOS'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_so->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPOS'];$j++)// save detail
			{
				if($_SESSION['itemcodePOS'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOS'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOS'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeSO']=$_SESSION['itemcodePOS'][$j];
					$da['itemnameSO']=$_SESSION['itemnamePOS'][$j];
					$da['qtySO']=$_SESSION['qtyPOS'][$j];
					$da['whsSO']=$this->m_location->GetIDByName($data['WhsName']);
					$da['whsNameSO']=$data['WhsName'];
					
					$da['uomSO']=$vcUoM->vcSlsUoM;
					$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intSlsUoM',1);
					
					
					$da['idBaseRefSO']=0;
					$da['BaseRefSO']='POS';
					
					$da['uomtypeSO']=2; // uom type for sales UoM
					$da['uominvSO']=$vcUoM->vcUoM;
					
					$da['priceSO']= $_SESSION['pricePOS'][$j];
					$da['discperSO'] = $_SESSION['discPOS'][$j];
					$da['discSO'] = $_SESSION['discPOS'][$j]/100*$_SESSION['pricePOS'][$j];
					$da['priceafterSO']=$da['priceSO']-$da['discSO'];
					$da['linetotalSO']= $da['priceafterSO']*$da['qtySO'];
					$detail=$this->m_so->insertD($da);
				}
			}
			$_SESSION['totitemPOS']=0;
			unset($_SESSION['itemcodePOS']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['idDPOS']);
			unset($_SESSION['itemnamePOS']);
			unset($_SESSION['qtyPOS']);
			unset($_SESSION['pricePOS']);
			unset($_SESSION['discPOS']);
			unset($_SESSION['perluGR']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function proseseditso()
	{
		$data['id']=isset($_POST['IDSO'])?$_POST['IDSO']:''; // get the requested page
		
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = '';
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->loadsession($_POST['ddidDPOS'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$lastdata=$this->m_so->GetHeaderByHeaderID($data['id']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
		$this->m_so->editH($data);
		
		$data['DocTotalBP']=$data['DocTotal']-$lastdata->intDocTotal;
		$data['BPId']=$lastdata->intBP;
		if($data['id']!=0)
		{
			$notdelitem = '(';
			for($j=0;$j<$_SESSION['totitemPOS'];$j++)// save detail
			{
				$cek=1;
					
				if($_SESSION['itemcodePOS'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
				{
					if(isset($_SESSION['idDPOS'][$j]))
					{
						$this->m_so->deleteD($_SESSION['idDPOS'][$j]);
					}
				}
				else
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOS'][$j]);
					$notdelitem = $notdelitem."".$item.",";
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOS'][$j]);
					$whs=$data['WhsName'];
					
					if(isset($_SESSION['idBaseRefSO'][$j]))
					{
						$da['idBaseRefSO']=$_SESSION['idBaseRefSO'][$j];
						$da['BaseRefSO']='POS';
					}
					else
					{
						$da['idBaseRefSO']=0;
						$da['BaseRefSO']='POS';
					}
					
					$da['intHID']=$data['id'];
					$da['itemID']=$item;
					$da['itemcodeSO']=$_SESSION['itemcodePOS'][$j];
					$da['itemnameSO']=$_SESSION['itemnamePOS'][$j];
					$da['qtySO']=$_SESSION['qtyPOS'][$j];
					$da['whsSO']=$this->m_location->GetIDByName($whs);
					$da['whsNameSO']=$whs;
					
					
					$da['uomSO']=$vcUoM->vcSlsUoM;
					$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intSlsUoM',1);
					
					$da['uomtypeSO']=2;
					$da['uominvSO']=$vcUoM->vcUoM;
					
					$da['priceSO']= $_SESSION['pricePOS'][$j];
					$da['discperSO'] = $_SESSION['discPOS'][$j];
					$da['discSO'] = $_SESSION['discPOS'][$j]/100*$_SESSION['pricePOS'][$j];
					$da['priceafterSO']=$da['priceSO']-$da['discSO'];
					$da['linetotalSO']= $da['priceafterSO']*$da['qtySO'];
					//echo $_SESSION['idDPOS'][$j]."-";
					if(isset($_SESSION['idDPOS'][$j]) and $_SESSION['idDPOS'][$j]!='' and $_SESSION['idDPOS'][$j]!=null) //jika ada session id artinya data detail dirubah
					{
						$da['intID']=$_SESSION['idDPOS'][$j];
						$detail=$this->m_so->editD($da);
					}
					else //jika tidak artinya ini merupakan detail tambahan
					{
						$detail=$this->m_so->insertD($da);
					}
					
				}
			}
			$notdelitem = rtrim($notdelitem,", ");
			$notdelitem =$notdelitem.")";
			$this->m_so->deleteD2($data['id'],$notdelitem);
			
			$_SESSION['totitemPOS']=0;
			unset($_SESSION['itemcodePOS']);
			unset($_SESSION['idBaseRefAR']);
			unset($_SESSION['idDPOS']);
			unset($_SESSION['itemnamePOS']);
			unset($_SESSION['qtyPOS']);
			unset($_SESSION['pricePOS']);
			unset($_SESSION['discPOS']);
			unset($_SESSION['perluGR']);
			
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosessavepayment()
	{
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hAR');
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['WhsName'] = isset($_POST['WhsName'])?$_POST['WhsName']:''; // get the requested page
		$data['BPCode'] = $this->m_bp->getBPCode($data['BPName']);
		$data['RefNum'] = ''; 
		$data['DocDate'] = date('Y-m-d'); 
		$data['DelDate'] = date('Y-m-d'); 
		$data['SalesEmp'] = $_SESSION['NamePOS']; 
		$data['Remarks'] = ''; 
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['AmmountTendered'] = isset($_POST['AmmountTendered'])?$_POST['AmmountTendered']:''; // get the requested page
		$data['Change'] = isset($_POST['Change'])?$_POST['Change']:''; // get the requested page
		$data['PaymentNote'] = isset($_POST['PaymentNote'])?$_POST['PaymentNote']:''; // get the requested page
		$data['PaymentCode'] = isset($_POST['PaymentCode'])?$_POST['PaymentCode']:''; // get the requested page
		$this->loadsession($_POST['ddidDPOS'],$_POST['ddidBaseRef'],$_POST['ddBaseRef'],$_POST['ddItem'],$_POST['ddQty'],$_POST['ddPrice'],$_POST['ddDisc'],$_POST['perluGR']);
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add','payment');
		//
		// CREATE AR
		//
		$headAR=$this->m_ar->insertH($data); // create AR
		
			for($j=0;$j<$_SESSION['totitemPOS'];$j++)// save detail
			{
				if($_SESSION['itemcodePOS'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOS'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePOS'][$j]);
					
					$da['intHID']=$headAR;
					$da['itemID']=$item;
					$da['itemcodeAR']=$_SESSION['itemcodePOS'][$j];
					$da['itemnameAR']=$_SESSION['itemnamePOS'][$j];
					$da['qtyAR']=$_SESSION['qtyPOS'][$j];
					$da['whsAR']=$this->m_location->GetIDByName($data['WhsName']);
					$da['whsNameAR']=$data['WhsName'];
					
					$da['uomAR']=$vcUoM->vcSlsUoM;
					$da['qtyinvAR']=$this->m_item->convert_qty($item,$da['qtyAR'],'intSlsUoM',1);
					
					if(isset($_SESSION['idBaseRefAR'][$j]))
					{
						$da['idBaseRefAR']=$_SESSION['idBaseRefAR'][$j];
						$da['BaseRefAR']=$_SESSION['BaseRefAR'][$j];
					}
					else
					{
						$da['idBaseRefAR']=0;
						$da['BaseRefAR']='POS';
					}
					
					if($da['BaseRefAR']=='SO')
					{
						$this->cekcloseSO($da['idBaseRefAR'], $da['itemID'], $da['qtyAR'], $da['qtyinvAR']);
					}
					
					$da['uomtypeAR']=2; // uom type for sales UoM
					$da['uominvAR']=$vcUoM->vcUoM;
					$da['costAR']=$this->m_stock->GetCostItem($item,$da['whsAR']);
					
					$da['priceAR']= $_SESSION['pricePOS'][$j];
					$da['discperAR'] = $_SESSION['discPOS'][$j];
					$da['discAR'] = $_SESSION['discPOS'][$j]/100*$_SESSION['pricePOS'][$j];
					$da['priceafterAR']=$da['priceAR']-$da['discAR'];
					$da['linetotalAR']= $da['priceafterAR']*$da['qtyAR'];
					$da['linecostAR']=$da['costAR']*$da['qtyinvAR'];
					
					$detail=$this->m_ar->insertD($da);
					$da['qtyinvAR']=$da['qtyinvAR']*-1;
					$this->m_stock->updateStock($item,$da['qtyinvAR'],$da['whsAR']);//update stok menambah/mengurangi di gudang
					$this->m_stock->addMutation($item,$da['qtyinvAR'],$da['costAR'],$da['whsAR'],'AR',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
		//
		// CREATE INPAY
		//
		$data2['DocNum'] = $this->m_docnum->GetLastDocNum('hINPAY');
		$data2['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data2['BPCode'] = $this->m_bp->getBPCode($data2['BPName']);
		$data2['RefNum'] = ''; // get the requested page
		$data2['DocDate'] = date('Y-m-d'); 
		$data2['Remarks'] = ''; // get the requested page
		
		
		$data2['DocTotalBefore'] = $data['DocTotal'];
		$data2['AppliedAmount'] = $data['DocTotal'];
		$data2['BalanceDue'] = 0; // get the requested page
		$data2['Wallet'] = isset($_POST['Wallet'])?$_POST['Wallet']:''; // get the requested page
		
		$sisa=$data2['AppliedAmount'];
		
		$data2['BPId']=$this->m_bp->getIDByCode($data2['BPCode']);
		$head=$this->m_inpay->insertH($data2);
		if($da['BaseRefAR']=='SO')
		{
			$data['DocTotalmin']=$data['DocTotal']*-1;
			//$this->m_bp->updateBDO($data2['BPId'],'intOrder',$data['DocTotalmin']);
		}
		
		$da2['intHID']=$head;
		$da2['intBaseRef']=$headAR; // intID AR nya
		$da2['vcBaseType']='AR';
		$da2['vcDocNum']=$data['DocNum']; //nomer dokumen ar nya
		$da2['intDocTotal']=$data['DocTotal'];
						
		if($da2['intDocTotal']<=$sisa)
		{
			$da2['intApplied']=$da2['intDocTotal'];
			$sisa=$sisa-$da2['intDocTotal'];
		}
		else
		{
			if($sisa<=0)
			{
				$da2['intApplied']=0;
			}
			else
			{
				$da2['intApplied']=$sisa;
			}
			$sisa=$sisa-$da2['intDocTotal'];
		}
		$detail=$this->m_inpay->insertD($da2);
		$this->m_wallet->addMutation($data2['Wallet'],$data2['DocDate'],$da2['vcBaseType'],$da2['intApplied'],$data['DocNum'],$data2['DocNum']);// add mutation wallet
		$this->m_wallet->updateBalance($data2['Wallet'],$da2['intApplied']); // change wallet balance
		if($da2['vcBaseType']=='AR')
		{
			$this->m_ar->cekclose($da2['intBaseRef'],$da2['intApplied']); // set ar to close
		}
		echo $headAR;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$_SESSION['totitemPOS']=0;
		unset($_SESSION['itemcodePOS']);
		unset($_SESSION['idBaseRefAR']);
		unset($_SESSION['idDPOS']);
		unset($_SESSION['itemnamePOS']);
		unset($_SESSION['qtyPOS']);
		unset($_SESSION['pricePOS']);
		unset($_SESSION['discPOS']);
		unset($_SESSION['perluGR']);
	}
	function listwallet()
	{
		$plan=$this->m_location->GetPlanIDByName($_GET['WhsName']);
		$wallet=$this->m_wallet->GetAllByPlan($plan);
		$defaultwallet=$this->m_user->getDefaultWallet($_SESSION['IDPOS']);
		
		echo '<select class="" id="Wallet" name="Wallet" style="width: 100%; height:35px">';
				  
		foreach($wallet->result() as $d) 
		{
			if($d->intID==$defaultwallet)
			{
				$selected='selected="true"';
			}
			else
			{
				$selected='';
			}
			echo '<option value="'.$d->intID.'" '.$selected.'>'.$d->vcName.'</option>';
		}
		echo '</select>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	/*function lisDetail()
	{
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th align="center">Item</th>
				  <th align="center">Qty</th>
				  <th align="center">
				  <div class="col-sm-4">Price</div>
				  <div class="col-sm-4">Disc</div>
				  <div class="col-sm-4">Line Total</div>
				  </th>
				  <th align="center">Control</th>
				</tr>
				</thead>
				<tbody>
		';
			$total=0;
			for($j=0;$j<$_SESSION['totitemPOS'];$j++)
			{
				if($_SESSION['itemnamePOS'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePOS'][$j]);
					
					if($_SESSION['discPOS'][$j]=='')
					{
						$_SESSION['discPOS'][$j]=0;
					}
					
					$disc=$_SESSION['discPOS'][$j]/100*$_SESSION['pricePOS'][$j];
					$lineTotal=($_SESSION['pricePOS'][$j]-$disc)*$_SESSION['qtyPOS'][$j];
					$total=$total+$lineTotal;
					echo '
					<tr>
						<td>'.$_SESSION['itemnamePOS'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPOS'][$j],'2').'</td>
						<td align="right">
						  <div class="col-sm-4" align="right">'.number_format($_SESSION['pricePOS'][$j],'2').'</div>
						  <div class="col-sm-4" align="right">'.number_format($_SESSION['discPOS'][$j],'2').'</div>
						  <div class="col-sm-4" align="right">'.number_format($lineTotal,'2').'</div>
						  
							
						  
						
						</td>
						<td align="center">
							<div id="controldetail">
							<a onclick="delDetail(\''.$_SESSION["itemnamePOS"][$j].'\',\''.$_SESSION["qtyPOS"][$j].'\')">
							<i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</td>
						</td>
					</tr>
					';
				}
			}			
			
			echo "
				
				</tbody>
			</table><br>
			";
	}*/
}
