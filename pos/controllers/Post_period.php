<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_period extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_period','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Period',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_period->GetAllData();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	function prosesadd()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		
		if ($data['crudaccess']->intCreate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Year'] = isset($_POST['Year'])?$_POST['Year']:''; // get the requested page
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Name']);
		$cek=$this->m_period->insert($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;		
		
	}
	function prosesedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		
		if ($data['crudaccess']->intCreate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['ID'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
		$data['Status'] = isset($_POST['Status'])?$_POST['Status']:''; // get the requested page
		$data['Name'] = isset($_POST['NameE'])?$_POST['NameE']:''; // get the requested page
		$data['Year'] = isset($_POST['YearE'])?$_POST['YearE']:''; // get the requested page
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Name']);
		$cek=$this->m_period->edit($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;		
		
	}
}
