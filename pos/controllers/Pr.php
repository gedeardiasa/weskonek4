<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pr extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pr','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPR']=0;
			unset($_SESSION['itemcodePR']);
			unset($_SESSION['idPR']);
			unset($_SESSION['idBaseRefPR']);
			unset($_SESSION['itemnamePR']);
			unset($_SESSION['qtyPR']);
			unset($_SESSION['qtyOpenPR']);
			unset($_SESSION['uomPR']);
			unset($_SESSION['pricePR']);
			unset($_SESSION['discPR']);
			unset($_SESSION['whsPR']);
			unset($_SESSION['statusPR']);
			
			$data['typetoolbar']='PR';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('purchase return',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_pr->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listgrpo']=$this->m_grpo->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataPur();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataPur();
			$data['autobp']=$this->m_bp->GetAllDataVendor();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('S');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_pr->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hPR');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hPR');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	
	function getDataHeaderGRPO()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_grpo->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hPR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		if($_POST['detailItem']!='')
		{
			$item=$this->m_item->GetIDByName($_POST['detailItem']);
			$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
			if($item==null)
			{
				$price=0;
			}
			else
			{
				
				if($bp=='')
				{
					$price=$this->m_item->GetPriceByID($item);
					
					if($price==null)
					{
						$price=0;
					}
				}
				else
				{
					$price=$this->m_price->getpricebybpanditem($bp,$item);
				}
			}
			if($price==0)
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
				echo $price;
			}
			else
			{
				if($_POST['detailUoM']==1)
				{
					
					echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
				}
				else if($_POST['detailUoM']==2)
				{
					echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
				}
				else if($_POST['detailUoM']==3)
				{
					echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
				}
				else
				{
					echo $price;
				}
			}
		}
		else
		{
			echo '';
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemPR']=0;
			unset($_SESSION['itemcodePR']);
			unset($_SESSION['idPR']);
			unset($_SESSION['idBaseRefPR']);
			unset($_SESSION['itemnamePR']);
			unset($_SESSION['qtyPR']);
			unset($_SESSION['qtyOpenPR']);
			unset($_SESSION['uomPR']);
			unset($_SESSION['pricePR']);
			unset($_SESSION['discPR']);
			unset($_SESSION['whsPR']);
			unset($_SESSION['statusPR']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemPR']=0;
			unset($_SESSION['itemcodePR']);
			unset($_SESSION['idPR']);
			unset($_SESSION['idBaseRefPR']);
			unset($_SESSION['itemnamePR']);
			unset($_SESSION['qtyPR']);
			unset($_SESSION['qtyOpenPR']);
			unset($_SESSION['uomPR']);
			unset($_SESSION['pricePR']);
			unset($_SESSION['discPR']);
			unset($_SESSION['whsPR']);
			unset($_SESSION['statusPR']);
			
			$r=$this->m_pr->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idPR'][$j]=$d->intID;
				$_SESSION['BaseRefPR'][$j]='';
				$_SESSION['itemcodePR'][$j]=$d->vcItemCode;
				$_SESSION['itemnamePR'][$j]=$d->vcItemName;
				$_SESSION['qtyPR'][$j]=$d->intQty;
				$_SESSION['qtyOpenPR'][$j]=$d->intOpenQty;
				$_SESSION['uomPR'][$j]=$d->intUoMType;
				$_SESSION['pricePR'][$j]=$d->intPrice;
				$_SESSION['discPR'][$j]=$d->intDiscPer;
				$_SESSION['whsPR'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusPR'][$j]='O';
				}
				else
				{
					$_SESSION['statusPR'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemPR']=$j;
		}
	}
	function loaddetailgrpo()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemPR']=0;
		unset($_SESSION['itemcodePR']);
		unset($_SESSION['idPR']);
		unset($_SESSION['idBaseRefPR']);
		unset($_SESSION['itemnamePR']);
		unset($_SESSION['qtyPR']);
		unset($_SESSION['qtyOpenPR']);
		unset($_SESSION['uomPR']);
		unset($_SESSION['pricePR']);
		unset($_SESSION['discPR']);
		unset($_SESSION['whsPR']);
		unset($_SESSION['statusPR']);
		
		$r=$this->m_grpo->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefPR'][$j]=$d->intHID;
				$_SESSION['BaseRefPR'][$j]='GRPO';
				$_SESSION['itemcodePR'][$j]=$d->vcItemCode;
				$_SESSION['itemnamePR'][$j]=$d->vcItemName;
				$_SESSION['qtyPR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenPR'][$j]=$d->intOpenQty;
				$_SESSION['uomPR'][$j]=$d->intUoMType;
				$_SESSION['pricePR'][$j]=$d->intPrice;
				$_SESSION['discPR'][$j]=$d->intDiscPer;
				$_SESSION['whsPR'][$j]=$d->intLocation;
				$_SESSION['statusPR'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemPR']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if($_SESSION['itemcodePR'][$j]!='' and $_SESSION['itemcodePR'][$j]!=null)
			{
				$total=$total+(($_SESSION['qtyPR'][$j]*$_SESSION['pricePR'][$j])-($_SESSION['discPR'][$j]/100*($_SESSION['qtyPR'][$j]*$_SESSION['pricePR'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if(isset($_SESSION['itemcodePR'][$j]))
			{
				if($_SESSION['itemcodePR'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$hasil='';
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if($_SESSION['itemcodePR'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodePR'][$j]);
				if($_SESSION['uomPR'][$j]==1)// konversi uom
				{
					$da['qtyinvPR']=$_SESSION['qtyPR'][$j];
				}
				else if($_SESSION['uomPR'][$j]==2)
				{
					$da['qtyinvPR']=$this->m_item->convert_qty($item,$_SESSION['qtyPR'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomPR'][$j]==3)
				{
					$da['qtyinvPR']=$this->m_item->convert_qty($item,$_SESSION['qtyPR'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvPR'],$_SESSION['whsPR'][$j]);
				if($res==1)
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodePR'][$j].' - '.$_SESSION['itemnamePR'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if($_SESSION['itemcodePR'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsPR'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	
	function cekcloseGRPO($id,$item,$qty,$qtyinv)
	{
		$this->m_grpo->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_pr->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPR'];$j++)// save detail
			{
				if($_SESSION['itemcodePR'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePR'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnamePR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsPR'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodePR']=$_SESSION['itemcodePR'][$j];
					$da['itemnamePR']=$_SESSION['itemnamePR'][$j];
					$da['qtyPR']=$_SESSION['qtyPR'][$j];
					$da['whsPR']=$_SESSION['whsPR'][$j];
					$da['whsNamePR']=$whs;
					$hargasetelahdiskon=(100-$_SESSION['discPR'][$j])/100*$_SESSION['pricePR'][$j];
					
					if($_SESSION['uomPR'][$j]==1)
					{
						$da['uomPR']=$vcUoM->vcUoM;
						$da['qtyinvPR']=$da['qtyPR'];
					}
					else if($_SESSION['uomPR'][$j]==2)
					{
						$da['uomPR']=$vcUoM->vcSlsUoM;
						$da['qtyinvPR']=$this->m_item->convert_qty($item,$da['qtyPR'],'intSlsUoM',1);
					}
					else if($_SESSION['uomPR'][$j]==3)
					{
						$da['uomPR']=$vcUoM->vcPurUoM;
						$da['qtyinvPR']=$this->m_item->convert_qty($item,$da['qtyPR'],'intPurUoM',1);
					}
					
					if(isset($_SESSION['idBaseRefPR'][$j]))
					{
						$da['idBaseRefPR']=$_SESSION['idBaseRefPR'][$j];
						$da['BaseRefPR']=$_SESSION['BaseRefPR'][$j];
					}
					else
					{
						$da['idBaseRefPR']=0;
						$da['BaseRefPR']='';
					}
					
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefPR']=='GRPO')
					{
						$this->cekcloseGRPO($da['idBaseRefPR'], $da['itemID'], $da['qtyPR'], $da['qtyinvPR']);
					}
					
					$da['uomtypePR']=$_SESSION['uomPR'][$j];
					$da['uominvPR']=$vcUoM->vcUoM;
					$da['costPR']=$this->m_stock->GetCostItem($item,$da['whsPR']);
					
					$da['pricePR']= $_SESSION['pricePR'][$j];
					$da['discperPR'] = $_SESSION['discPR'][$j];
					$da['discPR'] = $_SESSION['discPR'][$j]/100*$da['pricePR'];
					$da['priceafterPR']=(100-$_SESSION['discPR'][$j])/100*$da['pricePR'];
					$da['linetotalPR']= $da['priceafterPR']*$da['qtyPR'];
					$da['linecostPR']=$da['costPR']*$da['qtyinvPR'];
					
					
					$detail=$this->m_pr->insertD($da);
					$da['qtyinvPR'] = $da['qtyinvPR']*-1;
					$this->m_stock->updateStock($item,$da['qtyinvPR'],$da['whsPR']);//update stok menambah/mengurangi di gudang
					//$this->m_stock->updateCost($item,$da['qtyinvPR'],$da['costPR'],$da['whsPR']);//update cost
					$this->m_stock->addMutation($item,$da['qtyinvPR'],$da['costPR'],$da['whsPR'],'PR',$data['DocDate'],$data['DocNum']);//add mutation
					
				}
			}
			$_SESSION['totitemPR']=0;
			unset($_SESSION['itemcodePR']);
			unset($_SESSION['idPR']);
			unset($_SESSION['idBaseRefPR']);
			unset($_SESSION['itemnamePR']);
			unset($_SESSION['qtyPR']);
			unset($_SESSION['qtyOpenPR']);
			unset($_SESSION['uomPR']);
			unset($_SESSION['pricePR']);
			unset($_SESSION['discPR']);
			unset($_SESSION['whsPR']);
			unset($_SESSION['statusPR']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_pr->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_pr->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_pr->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemPR'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if($_SESSION['itemcodePR'][$j]==$code)
			{
				$_SESSION['qtyPR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenPR'][$j]=$_POST['detailQty'];
				$_SESSION['uomPR'][$j]=$_POST['detailUoM'];
				$_SESSION['pricePR'][$j]=$_POST['detailPrice'];
				$_SESSION['discPR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsPR'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefPR'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodePR'][$i]=$code;
				$_SESSION['itemnamePR'][$i]=$_POST['detailItem'];
				$_SESSION['qtyPR'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenPR'][$i]=$_POST['detailQty'];
				$_SESSION['uomPR'][$i]=$_POST['detailUoM'];
				$_SESSION['pricePR'][$i]=$_POST['detailPrice'];
				$_SESSION['discPR'][$i]=$_POST['detailDisc'];
				$_SESSION['whsPR'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefPR'][$i]='';
				$_SESSION['statusPR'][$i]='O';
				$_SESSION['totitemPR']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemPR'];$j++)
		{
			if($_SESSION['itemcodePR'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodePR'][$j]="";
				$_SESSION['itemnamePR'][$j]="";
				$_SESSION['qtyPR'][$j]="";
				$_SESSION['qtyOpenPR'][$j]="";
				$_SESSION['uomPR'][$j]="";
				$_SESSION['pricePR'][$j]="";
				$_SESSION['discPR'][$j]="";
				$_SESSION['whsPR'][$j]="";
				$_SESSION['BaseRefPR'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemPR'];$j++)
			{
				if($_SESSION['itemnamePR'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePR'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnamePR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsPR'][$j]);
					
					if($_SESSION['uomPR'][$j]==1)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomPR'][$j]==2)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomPR'][$j]==3)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					
					if($_SESSION['discPR'][$j]=='')
					{
						$_SESSION['discPR'][$j]=0;
					}
					
					if($_SESSION['statusPR'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discPR'][$j])/100)*$_SESSION['pricePR'][$j]*$_SESSION['qtyPR'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodePR'][$j].'</td>
						<td>'.$_SESSION['itemnamePR'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenPR'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['pricePR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discPR'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusPR'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnamePR"][$j].'\',\''.$_SESSION["itemcodePR"][$j].'\',\''.$_SESSION["qtyPR"][$j].'\',\''.$_SESSION["uomPR"][$j].'\',\''.$_SESSION["pricePR"][$j].'\',\''.$_SESSION["discPR"][$j].'\',\''.$_SESSION["whsPR"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodePR"][$j].'\',\''.$_SESSION["qtyPR"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
