<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('price',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_price->GetAllData();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "add":
				if ($data['crudaccess']->intCreate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				
				$this->db->trans_begin();
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Name']);
				$idnew=$this->m_price->insert($data);
				if($idnew!=0)
				{
					//set plan
					$access=$this->m_price->getAccessAllPlan();
					$i=0;
					foreach($access->result() as $a)
					{
						$index=$a->vcCode;
						if(isset($_POST["plan".$index]))
						{
							$cek=$this->m_price->addnewaccessplan($idnew,$a->intID);// add new access
						}
						else
						{
							//simpan id yang harus di hapus
							$del[$i]=$a->intID;
							$i++;
						}
					}
					for($j=0;$j<$i;$j++)
					{
						$this->m_price->dellastaccessbyidplan($idnew,$del[$j]);// delete last access
					}
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
				}
				else{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
				}
				$this->db->trans_complete();
		
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			case "edit":
				if ($data['crudaccess']->intUpdate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				
				$this->db->trans_begin();
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Name']);
				$cek=$this->m_price->edit($data);
				
				$detailprice=$this->m_price->getDetailPrice($data['id']);
				foreach($detailprice->result() as $rp)
				{
					$this->m_price->editD($rp->intID,$_POST['price'.$rp->intID.'']);
				}
				if($cek==1)
				{
					//set plan
					$access=$this->m_price->getAccessAllPlan();
					$i=0;
					foreach($access->result() as $a)
					{
						$index=$a->vcCode;
						if(isset($_POST["plan".$index]))
						{
							$cek=$this->m_price->addnewaccessplan($data['id'],$a->intID);// add new access
						}
						else
						{
							//simpan id yang harus di hapus
							$del[$i]=$a->intID;
							$i++;
						}
					}
					for($j=0;$j<$i;$j++)
					{
						$this->m_price->dellastaccessbyidplan($data['id'],$del[$j]);// delete last access
					}
					
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successedit';
				}
				else{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroredit';
				}
				$this->db->trans_complete();
		
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/item_category';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
	function prosesdelete()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$id=$_POST['ID2'];
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_price->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
	function lisDetail($id)
	{
		if(isset($_GET['withoutcontrol']))
		{
			$disabled='disabled="true"';
		}
		else
		{
			$disabled='';
		}
		
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Price</th>
				  <th>UoM</th>
				</tr>
				</thead>
				<tbody>
		';
		
		$data=$this->m_price->getDetailPrice($id);
		
		foreach($data->result() as $r)
		{
			echo '
				<tr>
					<td>'.$r->vcCode.'</td>
					<td>'.$r->vcName.'</td>
					<td><input type="text" name="price'.$r->intID.'" value="'.$r->intPrice.'" '.$disabled.'></td>
					<td>'.$r->vcSlsUoM.'</td>
				</tr>
			';			
		}
		
		echo "
				
				</tbody>
		</table><br>
		";
		
		
	}
	function lisPlan($id)
	{
		if(isset($_GET['withoutcontrol']))
		{
			$disabled='disabled="true"';
		}
		else
		{
			$disabled='';
		}
		$data['accessPlan']=$this->m_price->getAccessPlan($id);
		echo '<table class="table table-striped dt-responsive jambo_table tree">
                    <tr>
                        <th>Name </th>
						<th>Control </th>
                    </tr>';
		$db=$this->load->database('default', TRUE);
		foreach($data['accessPlan']->result() as $h) 
		{
			$idprice=$id;
			$openplan='';
			$idplan=$h->intID;
			$cekopenform=$db->query("
			SELECT 
			a.*
			FROM mpriceplan a 
			LEFT JOIN mplan b ON a.`intPlan`=b.`intID`
			WHERE a.`intPrice`='$idprice' AND a.`intPlan`='$idplan'
			");
				
			if($cekopenform->num_rows()>0)
			{
				$openplan='checked=checked';
			}
			echo '
			<tr>
                <td>'.$h->vcName.'</td>	
				<td><input type="checkbox" name="plan'.$h->vcCode.'" id="plan'.$h->intID.'" '.$openplan.' '.$disabled.'></td>
            </tr>
			';
		}
		echo '
		</table>
		';
	}
}
