<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pro extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pro','',TRUE);
		$this->load->model('m_ifp','',TRUE);
		$this->load->model('m_rfp','',TRUE);
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemPRO']=0;
			unset($_SESSION['itemcodePRO']);
			unset($_SESSION['itemnamePRO']);
			unset($_SESSION['qtyPRO']);
			unset($_SESSION['plannedqtyPRO']);
			unset($_SESSION['issueqtyPRO']);
			unset($_SESSION['uomPRO']);
			unset($_SESSION['costPRO']);
			unset($_SESSION['autoPRO']);
			
			$data['typetoolbar']='PRO';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('PRO',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['list']=$this->m_pro->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listbom']=$this->m_bom->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_pro->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hPRO');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->FGItem = $r->vcItemName;
			$responce->Whs = $r->intLocation;
			$responce->FGQty = $r->intQty;
			$responce->Remarks = $r->vcRemarks;
			$responce->vcRef = $r->vcRef;
			$responce->FGCost = $r->intCost;
			
			$responce->PlannedQty = $r->intPlannedQty;
			$responce->ActualQty = $r->intActualQty;
			$responce->RejectQty = $r->intRejectQty;
			$responce->FGUoM = $r->FGUoM;
			$responce->SONumber = $r->vcSONumber;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->Status = $r->intStatus;
			}
			else
			{
				$responce->Status = 0;
			}
			
			$responce->ReqDate = date('m/d/Y',strtotime($r->dtReqDate));
			if($r->dtReleaseDate=='0000-00-00')
			{
				$responce->ReleaseDate = '';
			}
			else
			{
				$responce->ReleaseDate = date('m/d/Y',strtotime($r->dtReleaseDate));
			}
			if($r->dtCloseDate=='0000-00-00')
			{
				$responce->ClosedDate = '';
			}
			else
			{
				$responce->ClosedDate = date('m/d/Y',strtotime($r->dtCloseDate));
			}
			
			$responce->FGActualComponentCost = number_format($r->intComponentCost,'2');
			$responce->FGActualProductCost = number_format($r->intProductCost,'2');
			$responce->FGActualRejectCost = number_format($r->intRejectCost,'2');
			$responce->FGTotalVariance = number_format($r->intProductCost+$r->intRejectCost-$r->intComponentCost,'2');
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
			
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hPRO');
			$responce->DocDate = date('m/d/Y');
			$responce->FGItem = '';
			$responce->Whs = '';
			$responce->FGQty = '';
			$responce->Remarks = '';
			$responce->vcRef = '';
			$responce->FGCost = '';
			
			$responce->PlannedQty = '';
			$responce->ActualQty = '';
			$responce->RejectQty = '';
			$responce->FGUoM = '';
			$responce->SONumber = '';
			$responce->Status = 0;
			$responce->ReqDate = '';
			$responce->ReleaseDate = '';
			$responce->ClosedDate = '';
			
			$responce->FGActualComponentCost = '';
			$responce->FGActualProductCost = '';
			$responce->FGActualRejectCost = '';
			$responce->FGTotalVariance = '';
		}
		echo json_encode($responce);
	}
	function getDataHeaderBom()
	{
		
			$id=$_POST['id'];
			$r=$this->m_bom->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			$responce->DocDate = date('m/d/Y');
			$responce->FGItem = $r->vcItemName;
			$responce->Whs = $r->intLocation;
			$responce->FGQty = $r->intQty;
			$responce->Remarks = $r->vcRemarks;
			$responce->vcRef = $r->vcRef;
			$responce->FGCost = $r->intCost;
			$responce->PlannedQty = $r->intQty;
			
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		echo $cost;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemPRO']=0;
			unset($_SESSION['idPRO']);
			unset($_SESSION['itemcodePRO']);
			unset($_SESSION['itemnamePRO']);
			unset($_SESSION['qtyPRO']);
			unset($_SESSION['plannedqtyPRO']);
			unset($_SESSION['issueqtyPRO']);
			unset($_SESSION['uomPRO']);
			unset($_SESSION['costPRO']);
			unset($_SESSION['autoPRO']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemPRO']=0;
			unset($_SESSION['itemcodePRO']);
			unset($_SESSION['itemnamePRO']);
			unset($_SESSION['qtyPRO']);
			unset($_SESSION['plannedqtyPRO']);
			unset($_SESSION['issueqtyPRO']);
			unset($_SESSION['uomPRO']);
			unset($_SESSION['costPRO']);
			unset($_SESSION['autoPRO']);
			
			$r=$this->m_pro->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idPRO'][$j]=$d->intID;
				$_SESSION['itemcodePRO'][$j]=$d->vcItemCode;
				$_SESSION['itemnamePRO'][$j]=$d->vcItemName;
				$_SESSION['qtyPRO'][$j]=$d->intQty;
				$_SESSION['plannedqtyPRO'][$j]=$d->intPlannedQty;
				$_SESSION['issueqtyPRO'][$j]=$d->intIssueQty;
				$_SESSION['uomPRO'][$j]=$d->vcUoM;
				$_SESSION['costPRO'][$j]=$d->intCost;
				$_SESSION['autoPRO'][$j]=$d->intAutoIssue;
				$j++;
			}
			$_SESSION['totitemPRO']=$j;
		}
	}
	function loaddetailbom()
	{
		$id=$_POST['id'];
		$_SESSION['totitemPRO']=0;
		unset($_SESSION['itemcodePRO']);
		unset($_SESSION['itemnamePRO']);
		unset($_SESSION['qtyPRO']);
		unset($_SESSION['plannedqtyPRO']);
		unset($_SESSION['issueqtyPRO']);
		unset($_SESSION['uomPRO']);
		unset($_SESSION['costPRO']);
		unset($_SESSION['autoPRO']);
		
		$r=$this->m_bom->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			$_SESSION['idPRO'][$j]=$d->intID;
			$_SESSION['itemcodePRO'][$j]=$d->vcItemCode;
			$_SESSION['itemnamePRO'][$j]=$d->vcItemName;
			$_SESSION['qtyPRO'][$j]=$d->intQty;
			$_SESSION['plannedqtyPRO'][$j]=$d->intQty;
			$_SESSION['issueqtyPRO'][$j]=0;
			$_SESSION['uomPRO'][$j]=$d->vcUoM;
			$_SESSION['costPRO'][$j]=$d->intCost;
			$_SESSION['autoPRO'][$j]=$d->intAutoIssue;
			$j++;
		}
		$_SESSION['totitemPRO']=$j;
		
	}
	function reloadplannedqty()
	{
		for($j=0;$j<$_SESSION['totitemPRO'];$j++)
		{
			if(isset($_SESSION['itemcodePRO'][$j]))
			{
				if($_SESSION['itemcodePRO'][$j]!='')
				{
					$_SESSION['plannedqtyPRO'][$j]=$_SESSION['qtyPRO'][$j]/$_POST['FGQty']*$_POST['FGPlannedQty'];
				}
				
			}
			
		}
	}
	/*
	
		CHECK FUNCTION
	
	*/
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemPRO'];$j++)
		{
			if(isset($_SESSION['itemcodePRO'][$j]))
			{
				if($_SESSION['itemcodePRO'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['FGItem'] = isset($_POST['FGItem'])?$_POST['FGItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:0; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		$data['FGCost'] = isset($_POST['FGCost'])?$_POST['FGCost']:''; // get the requested page
		
		$data['PlannedQty'] = isset($_POST['PlannedQty'])?$_POST['PlannedQty']:0; // get the requested page
		$data['SONumber'] = isset($_POST['SONumber'])?$_POST['SONumber']:''; // get the requested page
		$data['ReqDate'] = isset($_POST['ReqDate'])?$_POST['ReqDate']:''; // get the requested page
		$data['ReqDate'] = date('Y-m-d',strtotime($data['ReqDate']));
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_pro->insertH($data);
		$totalcost=0;
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPRO'];$j++)// save detail
			{
				if($_SESSION['itemcodePRO'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePRO'][$j]);
					$da['intHID']=$head;
					$da['itemcodePRO']=$_SESSION['itemcodePRO'][$j];
					$da['itemnamePRO']=$_SESSION['itemnamePRO'][$j];
					$da['qtyPRO']=$_SESSION['qtyPRO'][$j];
					$da['plannedqtyPRO']=$_SESSION['qtyPRO'][$j]/$data['FGQty']*$data['PlannedQty'];
					$da['uomPRO']=$_SESSION['uomPRO'][$j];
					$da['costPRO']=$_SESSION['costPRO'][$j];
					$da['autoPRO']=$_SESSION['autoPRO'][$j];
					$detail=$this->m_pro->insertD($da);
					$totalcost=$totalcost+($da['qtyPRO']*$da['costPRO']);
					
				}
			}
			$this->m_pro->updateCost($head,$totalcost);
			$_SESSION['totitemPRO']=0;
			unset($_SESSION['itemcodePRO']);
			unset($_SESSION['itemnamePRO']);
			unset($_SESSION['qtyPRO']);
			unset($_SESSION['plannedqtyPRO']);
			unset($_SESSION['issueqtyPRO']);
			unset($_SESSION['uomPRO']);
			unset($_SESSION['costPRO']);
			unset($_SESSION['autoPRO']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['FGQty'] = isset($_POST['FGQty'])?$_POST['FGQty']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Rev'] = isset($_POST['Rev'])?$_POST['Rev']:''; // get the requested page
		
		$data['PlannedQty'] = isset($_POST['PlannedQty'])?$_POST['PlannedQty']:''; // get the requested page
		$data['SONumber'] = isset($_POST['SONumber'])?$_POST['SONumber']:''; // get the requested page
		$data['ReqDate'] = isset($_POST['ReqDate'])?$_POST['ReqDate']:''; // get the requested page
		$data['ReqDate'] = date('Y-m-d',strtotime($data['ReqDate']));
		$data['Status'] = isset($_POST['Status'])?$_POST['Status']:''; // get the requested page
		
		$adaautoissue=0;
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_pro->editH($data);
		
		if($data['Status']==1)
		{
			$this->m_pro->release($data);
		}
		else if($data['Status']==2)
		{
			$this->m_pro->close($data);
		}
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemPRO'];$j++)// save detail
			{
				if($_SESSION['autoPRO'][$j]==1)
				{
					$adaautoissue++;
				}
				
				if($_SESSION['itemcodePRO'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
				{
					$this->m_pro->deleteD($_SESSION['idPRO'][$j]);
				}
				else
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePRO'][$j]);
					$da['intHID']=$data['id'];
					$da['itemcodePRO']=$_SESSION['itemcodePRO'][$j];
					$da['itemnamePRO']=$_SESSION['itemnamePRO'][$j];
					$da['qtyPRO']=$_SESSION['qtyPRO'][$j];
					$da['plannedqtyPRO']=$_SESSION['qtyPRO'][$j]/$data['FGQty']*$data['PlannedQty'];
					$da['uomPRO']=$_SESSION['uomPRO'][$j];
					$da['costPRO']=$_SESSION['costPRO'][$j];
					$da['autoPRO']=$_SESSION['autoPRO'][$j];
					if(isset($_SESSION['idPRO'][$j])) //jika ada session id artinya data detail dirubah
					{
						$da['intID']=$_SESSION['idPRO'][$j];
						$detail=$this->m_pro->editD($da);
					}
					else //jika tidak artinya ini merupakan detail tambahan
					{
						$detail=$this->m_pro->insertD($da);
					}
				}
				
			}
			$listdetail=$this->m_pro->GetDetailByHeaderID($data['id']);
			$totalcost=0;
			foreach($listdetail->result() as $ld)
			{
				$totalcost=$totalcost+($ld->intQty*$ld->intCost);
			}
			$this->m_pro->updateCost($data['id'],$totalcost);
			
			echo 1;
			
			//jika ada auto issue
			if($adaautoissue>0 and $data['Status']==2)
			{
				$this->createIFP($data['id'],$data['Whs']);
			}
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function createIFP($PROID,$WHS)// harus sama dengan proses add IFP
	{
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hIFP');
		$data['PRONum'] = $PROID;
		$data['DocDate'] = date('Y/m/d');
		$data['RefNum'] = '';
		$data['Whs'] = $WHS;
		$data['Type'] = 'GI';
		$data['Remarks'] = '';
				
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_ifp->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPRO'];$j++)// save detail
			{
				if($_SESSION['itemcodePRO'][$j]!="")
				{
					if($_SESSION['autoPRO'][$j]==1)
					{
						$cek=1;
						$item=$this->m_item->GetIDByName($_SESSION['itemnamePRO'][$j]);
						$da['intHID']=$head;
						$da['itemcodeIFP']=$_SESSION['itemcodePRO'][$j];
						$da['itemnameIFP']=$_SESSION['itemnamePRO'][$j];
						$da['Whs']=$data['Whs'];
						$da['qtyIFP']=$_SESSION['plannedqtyPRO'][$j]*-1;
						$tipemutasi = 'CGI';
						
						$da['uomIFP']=$_SESSION['uomPRO'][$j];
						
						$cost=$this->m_stock->GetCostItem($item,$da['Whs']);
						
						$da['costIFP']=$cost;
						$da['linetotalcost'] = $da['qtyIFP']*$da['costIFP'];
						$detail=$this->m_ifp->insertD($da);
						if($detail==0)
						{
							$errordetail++;
						}
						
						$this->m_stock->updateStock($item,$da['qtyIFP'],$data['Whs']);//update stok menambah/mengurangi di gudang
						
						$da['linetotalcost']=$da['linetotalcost']*-1;
						
						$this->m_stock->addMutation($item,$da['qtyIFP'],$da['costIFP'],$da['Whs'],$tipemutasi,$data['DocDate'],$data['DocNum']);//add mutation
						
						$this->m_pro->updateIssueQty($data['PRONum'],$item,$da['qtyIFP']);//update Issue Qty
						$this->m_pro->updateComponentCost($data['PRONum'],$da['linetotalcost']);//update PRO
					}
				}
			}
			
			//echo 1;
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_pro->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Item Code</th>
                  <th>Item Name</th>
				  <th>Ver</th>
				  <th>Location</th>
				  <th>Status</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcItemCode.'</td>
				  <td>'.$d->vcItemName.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td>'.$d->vcStatus.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemPRO'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemPRO'];$j++)
		{
			if($_SESSION['itemcodePRO'][$j]==$code)
			{
				$_SESSION['qtyPRO'][$j]=$_POST['detailQty'];
				$_SESSION['costPRO'][$j]=$_POST['detailCost'];
				$_SESSION['autoPRO'][$j]=$_POST['detailAuto'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodePRO'][$i]=$code;
				$_SESSION['itemnamePRO'][$i]=$_POST['detailItem'];
				$_SESSION['qtyPRO'][$i]=$_POST['detailQty'];
				$_SESSION['plannedqtyPRO'][$i]=$_POST['detailQty']/$_POST['FGQty']*$_POST['FGPlannedQty'];
				$_SESSION['issueqtyPRO'][$i]=0;
				$_SESSION['costPRO'][$i]=$_POST['detailCost'];
				$_SESSION['uomPRO'][$i]=$UoM;
				$_SESSION['autoPRO'][$i]=$_POST['detailAuto'];
				$_SESSION['totitemPRO']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemPRO'];$j++)
		{
			if($_SESSION['itemcodePRO'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodePRO'][$j]="";
				$_SESSION['itemnamePRO'][$j]="";
				$_SESSION['qtyPRO'][$j]="";
				$_SESSION['plannedqtyPRO'][$j]="";
				$_SESSION['issueqtyPRO'][$j]="";
				$_SESSION['uomPRO'][$j]="";
				$_SESSION['costPRO'][$j]="";
				$_SESSION['autoPRO'][$i]="";
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Planned Qty</th>
				  <th>Issue Qty</th>
                  <th>UoM</th>
				  <th>Cost</th>
				  <th>Issue Method</th>
		';
		
		/*if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}*/
		echo '
					  <th style="width:15px">Control</th>
			';
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemPRO'];$j++)
			{
				if($_SESSION['itemnamePRO'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePRO'][$j]);
					if($_SESSION["autoPRO"][$j]==1)
					{
						$autoPRO = 'Auto';
					}
					else
					{
						$autoPRO = 'Manual';
					}
					echo '
					<tr>
						<td>'.$_SESSION['itemcodePRO'][$j].'</td>
						<td>'.$_SESSION['itemnamePRO'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyPRO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['plannedqtyPRO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['issueqtyPRO'][$j],'2').'</td>
						<td>'.$_SESSION['uomPRO'][$j].'</td>
						<td align="right">'.number_format($_SESSION['costPRO'][$j],'2').'</td>
						<td>'.$autoPRO.'</td>
					';
					if(!isset($_GET['withoutcontrol']) and $_SESSION['issueqtyPRO'][$j]==0)
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnamePRO"][$j].'\',\''.$_SESSION["itemcodePRO"][$j].'\',\''.$_SESSION["qtyPRO"][$j].'\',\''.$_SESSION["costPRO"][$j].'\',\''.$_SESSION["autoPRO"][$j].'\',\''.$_SESSION["uomPRO"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodePRO"][$j].'\',\''.$_SESSION["qtyPRO"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					else
					{
						echo '
							<td>
							Not Available
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	function lisrfpproduct()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		$rfpproduct=$this->m_rfp->GetDetailDataByPROID($_GET['idpro']);
		
		echo '
		<br>
			<table id="examplelisrfpproduct" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Doc. Number</th>
				  <th>Type</th>
				  <th>Doc. Date</th>
				  <th>Location</th>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Cost</th>
				</tr>
				</thead>
				<tbody>'
		;
			
		foreach($rfpproduct->result() as $d)
		{
			if($d->vcType=='GR' or $d->vcType=='GI')
					echo '
					<tr>
						<td>'.$d->vcDocNum.'</td>
						<td>'.$d->vcType.'</td>
						<td>'.date('d F Y',strtotime($d->dtDate)).'</td>
						<td>'.$d->vcLocation.'</td>
						<td>'.$d->vcItemCode.'</td>
						<td>'.$d->vcItemName.'</td>
						<td align="right">'.number_format($d->intQty,'2').'</td>
						<td align="right">'.number_format($d->intCost,'2').'</td>
						
					';
					echo'
					</tr>
					';
		}		
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#examplelisrfpproduct").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	function lisrfpreject()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		$rfpproduct=$this->m_rfp->GetDetailDataByPROID($_GET['idpro']);
		
		echo '
		<br>
			<table id="examplelisrfpreject" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Doc. Number</th>
				  <th>Type</th>
				  <th>Doc. Date</th>
				  <th>Location</th>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Cost</th>
				</tr>
				</thead>
				<tbody>'
		;
			
		foreach($rfpproduct->result() as $d)
		{
			if($d->vcType=='GRC' or $d->vcType=='GIC')
					echo '
					<tr>
						<td>'.$d->vcDocNum.'</td>
						<td>'.$d->vcType.'</td>
						<td>'.date('d F Y',strtotime($d->dtDate)).'</td>
						<td>'.$d->vcLocation.'</td>
						<td>'.$d->vcItemCode.'</td>
						<td>'.$d->vcItemName.'</td>
						<td align="right">'.number_format($d->intQty,'2').'</td>
						<td align="right">'.number_format($d->intCost,'2').'</td>
						
					';
					echo'
					</tr>
					';
		}		
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#examplelisrfpreject").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	function lisifpcomponent()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		$ifpcomponent=$this->m_ifp->GetDetailDataByPROID($_GET['idpro']);
		
		echo '
		<br>
			<table id="examplelisifpcomponent" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Doc. Number</th>
				  <th>Type</th>
				  <th>Doc. Date</th>
				  <th>Location</th>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Cost</th>
				</tr>
				</thead>
				<tbody>'
		;
			
		foreach($ifpcomponent->result() as $d)
		{
			
					echo '
					<tr>
						<td>'.$d->vcDocNum.'</td>
						<td>'.$d->vcType.'</td>
						<td>'.date('d F Y',strtotime($d->dtDate)).'</td>
						<td>'.$d->vcLocation.'</td>
						<td>'.$d->vcItemCode.'</td>
						<td>'.$d->vcItemName.'</td>
						<td align="right">'.number_format($d->intQty,'2').'</td>
						<td align="right">'.number_format($d->intCost,'2').'</td>
						
					';
					echo'
					</tr>
					';
		}		
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#examplelisifpcomponent").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
