<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Protop extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_bom','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_pro','',TRUE);
		$this->load->model('m_ifp','',TRUE);
		$this->load->model('m_rfp','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cektopform($this->uri->segment(1));
    }
	public function index()
	{
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['WithBom'] = isset($_GET['WithBom'])?$_GET['WithBom']:1; // get the requested page
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Pro Top',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function cekbom()
	{
		$loc=$this->m_so->GetLocaByIDSO($_POST['idso']);
		$cek=$this->m_bom->CekBom($_SESSION['IDPOS'],$_POST['item'],$loc);
		echo $cek;
	}
	public function createPOD()
	{
		$headerSO=$this->m_so->GetHeaderByHeaderID($_POST['idso']);
		$detailSO=$this->m_so->GetDetailByHeaderIDandItem($_POST['idso'],$_POST['item']);
		$whs=$detailSO->intLocation;
		$bom=$this->m_bom->GetDataByItemAndLoc($_POST['item'],$whs);
		
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hPRO');
		$data['DocDate'] = date('Y-m-d');
		$data['FGItem'] = $this->m_item->GetNameById($_POST['item']);
		$data['Whs'] = $detailSO->intLocation;
		$data['FGQty'] = $bom->intQty;
		$data['Remarks'] = 'Auto Create From PRO Top'; // get the requested page
		$data['Rev'] = $bom->vcRef;
		$data['FGCost'] = $bom->intCost;
		
		$data['PlannedQty'] = $detailSO->intOpenQtyInv;
		$data['SONumber'] = $headerSO->vcDocNum;
		$data['ReqDate'] = date('Y-m-d');
		
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_pro->insertH($data);
		$this->m_pro->editStatus($head,1);
		$this->m_so->updateLoadDetail($_POST['idso'],$_POST['item']);
		$totalcost=0;
		
		
		
		//load detail PRO from BOM
		$r=$this->m_bom->GetDetailByHeaderID($bom->intID);
		$j=0;
		foreach($r->result() as $d)
		{
			$_SESSION['idPRO'][$j]=$d->intID;
			$_SESSION['itemcodePRO'][$j]=$d->vcItemCode;
			$_SESSION['itemnamePRO'][$j]=$d->vcItemName;
			$_SESSION['qtyPRO'][$j]=$d->intQty;
			$_SESSION['plannedqtyPRO'][$j]=$d->intQty;
			$_SESSION['issueqtyPRO'][$j]=0;
			$_SESSION['uomPRO'][$j]=$d->vcUoM;
			$_SESSION['costPRO'][$j]=$d->intCost;
			$_SESSION['autoPRO'][$j]=$d->intAutoIssue;
			$j++;
		}
		$_SESSION['totitemPRO']=$j;
		//end load detail PRO from BOM
		
		
		//create detail POD
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemPRO'];$j++)// save detail
			{
				if($_SESSION['itemcodePRO'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnamePRO'][$j]);
					$da['intHID']=$head;
					$da['itemcodePRO']=$_SESSION['itemcodePRO'][$j];
					$da['itemnamePRO']=$_SESSION['itemnamePRO'][$j];
					$da['qtyPRO']=$_SESSION['qtyPRO'][$j];
					$da['plannedqtyPRO']=$_SESSION['qtyPRO'][$j]/$data['FGQty']*$data['PlannedQty'];
					$da['uomPRO']=$_SESSION['uomPRO'][$j];
					$da['costPRO']=$_SESSION['costPRO'][$j];
					$da['autoPRO']=$_SESSION['autoPRO'][$j];
					$detail=$this->m_pro->insertD($da);
					$totalcost=$totalcost+($da['qtyPRO']*$da['costPRO']);
					
				}
			}
			$this->m_pro->updateCost($head,$totalcost);
			$_SESSION['totitemPRO']=0;
			unset($_SESSION['itemcodePRO']);
			unset($_SESSION['itemnamePRO']);
			unset($_SESSION['qtyPRO']);
			unset($_SESSION['uomPRO']);
			unset($_SESSION['costPRO']);
			
		}
		//end create detail POD
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
	}
	public function closePOD()
	{
		$this->db->trans_begin();
		
		//close pro
		$sonumber=$this->m_so->GetHeaderByHeaderID($_POST['idso']);
		$pro=$this->m_pro->GetDatabysonumberanditem($sonumber->vcDocNum,$_POST['item']);
		$this->m_pro->editStatus($pro->intID,2);
		//end close pro
		
		//create IFP
		$data['DocNum'] = $this->m_docnum->GetLastDocNum('hIFP');
		$data['PRONum'] = $pro->intID;
		$data['DocDate'] = date('Y/m/d');
		$data['RefNum'] = '';
		$data['Whs'] = $pro->intLocation;
		$data['Type'] = 'GI';
		$data['Remarks'] = 'Auto Create From PRO Top';
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add-IFP',$data['DocNum']);		
		$head=$this->m_ifp->insertH($data);
		$r=$this->m_pro->GetDetailByHeaderID($pro->intID);
		foreach($r->result() as $d)
		{
			$da['intHID']=$head;
			$da['itemcodeIFP']=$d->vcItemCode;
			$da['itemnameIFP']=$d->vcItemName;
			$da['Whs']=$data['Whs'];
			$da['qtyIFP']=$d->intPlannedQty*-1;
			$tipemutasi = 'CGI';
						
			$da['uomIFP']=$d->vcUoM;
			
			$cost=$this->m_stock->GetCostItem($d->intItem,$da['Whs']);
						
			$da['costIFP']=$cost;
			$da['linetotalcost'] = $da['qtyIFP']*$da['costIFP'];
			$detail=$this->m_ifp->insertD($da);
			
						
			$this->m_stock->updateStock($d->intItem,$da['qtyIFP'],$data['Whs']);//update stok menambah/mengurangi di gudang
						
			$da['linetotalcost']=$da['linetotalcost']*-1;
						
			$this->m_stock->addMutation($d->intItem,$da['qtyIFP'],$da['costIFP'],$da['Whs'],$tipemutasi,$data['DocDate'],$data['DocNum']);//add mutation
						
			$this->m_pro->updateIssueQty($data['PRONum'],$d->intItem,$da['qtyIFP']);//update Issue Qty
			$this->m_pro->updateComponentCost($data['PRONum'],$da['linetotalcost']);//update PRO
		}
		//End create IFP
		
		//create RFP
		$dataR['DocNum'] = $this->docnum->GetLastDocNum('hRFP');
		$dataR['PRONum'] = $pro->intID;
		$dataR['DocDate'] = date('Y/m/d');
		$dataR['RefNum'] = '';
		$dataR['Whs'] = $pro->intLocation;
		$dataR['Type'] = 'GR';
		$dataR['Remarks'] = 'Auto Create From PRO Top';
		
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add-RFP',$data['DocNum']);
		$headR=$this->m_rfp->insertH($dataR);
		
		
					$daR['intHID']=$headR;
					$daR['itemcodeRFP']=$pro->vcItemCode;
					$daR['itemnameRFP']=$pro->vcItemName;
					$daR['Whs']=$dataR['Whs'];
					
					$daR['qtyRFP']=$pro->intPlannedQty;
					$tipemutasi = 'RGR';
					
					$daR['uomRFP']=$pro->vcUoM;
					$daR['costRFP']=$pro->intCost/$pro->intQty;
					$daR['linetotalcost'] = $daR['qtyRFP']*$daR['costRFP'];
					$detail=$this->m_rfp->insertD($daR);
					
					$this->m_stock->updateStock($pro->intItem,$daR['qtyRFP'],$dataR['Whs']);//update stok menambah/mengurangi di gudang
					
						$this->m_stock->updateCost($pro->intItem,$daR['qtyRFP'],$daR['costRFP'],$daR['Whs']);//update cost per gudang hanya untuk good receipt
					
					
					
					$this->m_stock->addMutation($pro->intItem,$daR['qtyRFP'],$daR['costRFP'],$daR['Whs'],$tipemutasi,$dataR['DocDate'],$dataR['DocNum']);//add mutation
					
					
						$this->m_pro->updateActualQty($dataR['PRONum'],$daR['qtyRFP']);//update Actual Qty
						$this->m_pro->updateProductCost($dataR['PRONum'],$daR['linetotalcost']);//update PRO
				
					
		
		//end create RFP
		
		
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		
	}
	public function updateloaddetail()
	{
		$this->m_so->updateLoadDetail($_POST['idso'],$_POST['item']);
	}
	public function updatecreated()
	{
		$this->m_so->updateCreated($_POST['idso'],$_POST['item']);
	}
	public function getcontent()
	{
		$data['list']=$this->m_so->GetDetailOpenDataWithPlanAccess($_SESSION['IDPOS']);
		
		foreach($data['list']->result() as $dl)
		{
			if($dl->blpImageMin!=null)
			{
				$image= '<img src="data:image/jpeg;base64,'.base64_encode( $dl->blpImageMin ).'" style="width:100px;height:100px" alt="User profile picture" class="profile-user-img img-responsive img-circle"/>';
			}
			else
			{
				$image= '<img src="'.base_url().'data/general/dist/img/box.png" alt="User profile picture" class="profile-user-img img-responsive img-circle">';
			}
			if($dl->intLoadQty<$dl->intOpenQty)
			{
				$button ='<button type="button" class="btn btn-block btn-warning btn-lg" onclick="createPOD('.$dl->intID.','.$dl->intItem.','.$dl->intOpenQty.')">Waiting</button>';
			}
			else
			{
				if(isset($this->m_pro->GetDatabysonumberanditem($dl->vcDocNum,$dl->intItem)->intStatus))
				{
					$prostatus=$this->m_pro->GetDatabysonumberanditem($dl->vcDocNum,$dl->intItem)->intStatus;
				}
				else
				{
					$prostatus=9999; // no pro
				}
				if($prostatus==1)
				{
					$button ='<button type="button" class="btn btn-block btn-primary btn-lg" onclick="closePOD('.$dl->intID.','.$dl->intItem.')">On Process
					<img src="'.base_url().'data/img/page-loader.gif" style="width:15px;height:15px"></button>';
				}
				else
				{
					$button ='<button type="button" class="btn btn-block btn-success btn-lg">Finished</button>';
				}
			}
			echo '
			<div class="col-lg-3 col-xs-12">
			  <div class="small-box bg-aqua">
				<div class="inner">
				  <h4>'.$dl->vcItemName.'</h4>

				  <p>'.$dl->intOpenQty.' '.$dl->vcUoM.'<br>Table '.$dl->TableName.'<br>'.$dl->vcLocation.'</p>
				</div>
				<div class="icon">
				  '.$image.'
				</div>
				'.$button.'
			  </div>
			</div>
			';
		}
	}
	public function getcontent2()
	{
		$data['list']=$this->m_so->GetDetailOpenDataWithPlanAccess($_SESSION['IDPOS']);
		foreach($data['list']->result() as $dl)
		{
			if($dl->blpImageMin!=null)
			{
				$image= '<img src="data:image/jpeg;base64,'.base64_encode( $dl->blpImageMin ).'" style="width:100px;height:100px" alt="User profile picture" class="profile-user-img img-responsive img-circle"/>';
			}
			else
			{
				$image= '<img src="'.base_url().'data/general/dist/img/box.png" alt="User profile picture" class="profile-user-img img-responsive img-circle">';
			}
			if($dl->intLoadQty<$dl->intOpenQty)
			{
				$button ='<button type="button" class="btn btn-block btn-warning btn-lg" onclick="updateloaddetail('.$dl->intID.','.$dl->intItem.')">Waiting</button>';
			}
			else
			{
				
				if($dl->intCreated==0)
				{
					$button ='<button type="button" class="btn btn-block btn-primary btn-lg" onclick="updatecreated('.$dl->intID.','.$dl->intItem.')">On Process
					<img src="'.base_url().'data/img/page-loader.gif" style="width:15px;height:15px"></button>';
				}
				else
				{
					$button ='<button type="button" class="btn btn-block btn-success btn-lg">Finished</button>';
				}
			}
			echo '
			<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-aqua">
				<div class="inner">
				  <h4>'.$dl->vcItemName.'</h4>

				  <p>'.$dl->intOpenQty.' '.$dl->vcUoM.'<br>Table '.$dl->TableName.'<br>'.$dl->vcLocation.'</p>
				</div>
				<div class="icon">
				  '.$image.'
				</div>
				'.$button.'
			  </div>
			</div>
			';
		}
	}
	
}
