<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pur_report extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_pur_report','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['setting']=isset($_GET['setting'])?$_GET['setting']:'customer'; // get the requested page
			
			//C
			
			$data['PlanC']=isset($_GET['PlanC'])?$_GET['PlanC']:999999999999999; // get the requested page
			$data['BPCodeC']=isset($_GET['BPCodeC'])?$_GET['BPCodeC']:''; // get the requested page
			$data['BPNameC']=isset($_GET['BPNameC'])?$_GET['BPNameC']:''; // get the requested page
			$data['BPGroupC']=isset($_GET['BPGroupC'])?$_GET['BPGroupC']:0; // get the requested page
			$data['CategoryC']=isset($_GET['CategoryC'])?$_GET['CategoryC']:'A'; // get the requested page
			$data['RefC']=isset($_GET['RefC'])?$_GET['RefC']:'AP'; // get the requested page
			$data['daterangeC'] = isset($_GET['daterangeC'])?$_GET['daterangeC']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			$dt=explode("-",$data['daterangeC']);
			$data['fromC']=str_replace("/","-",$dt[0]);
			$data['untilC']=str_replace("/","-",$dt[1]);
			
			//C
			
			//I
			$data['PlanI']=isset($_GET['PlanI'])?$_GET['PlanI']:999999999999999; // get the requested page
			$data['ItemCodeI']=isset($_GET['ItemCodeI'])?$_GET['ItemCodeI']:''; // get the requested page
			$data['ItemNameI']=isset($_GET['ItemNameI'])?$_GET['ItemNameI']:''; // get the requested page
			$data['ItemGroupI']=isset($_GET['ItemGroupI'])?$_GET['ItemGroupI']:0; // get the requested page
			$data['CategoryI']=isset($_GET['CategoryI'])?$_GET['CategoryI']:'A'; // get the requested page
			$data['RefI']=isset($_GET['RefI'])?$_GET['RefI']:'AP'; // get the requested page
			$data['daterangeI'] = isset($_GET['daterangeI'])?$_GET['daterangeI']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			$dt=explode("-",$data['daterangeI']);
			$data['fromI']=str_replace("/","-",$dt[0]);
			$data['untilI']=str_replace("/","-",$dt[1]);
			
			//I
			
			//G
			$data['PlanG']=isset($_GET['PlanG'])?$_GET['PlanG']:999999999999999; // get the requested page
			$data['BPCodeG']=isset($_GET['BPCodeG'])?$_GET['BPCodeG']:''; // get the requested page
			$data['BPNameG']=isset($_GET['BPNameG'])?$_GET['BPNameG']:''; // get the requested page
			$data['BPGroupG']=isset($_GET['BPGroupG'])?$_GET['BPGroupG']:0; // get the requested page
			$data['ItemCodeG']=isset($_GET['ItemCodeG'])?$_GET['ItemCodeG']:''; // get the requested page
			$data['ItemNameG']=isset($_GET['ItemNameG'])?$_GET['ItemNameG']:''; // get the requested page
			$data['ItemGroupG']=isset($_GET['ItemGroupG'])?$_GET['ItemGroupG']:0; // get the requested page
			$data['RefG']=isset($_GET['RefG'])?$_GET['RefG']:'AP'; // get the requested page
			$data['daterangeG'] = isset($_GET['daterangeG'])?$_GET['daterangeG']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			$dt=explode("-",$data['daterangeG']);
			$data['fromG']=str_replace("/","-",$dt[0]);
			$data['untilG']=str_replace("/","-",$dt[1]);
			
			//G
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Purchase Report',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['listgrp']=$this->m_item_category->GetAllData();
			$data['listgrpBP']=$this->m_bp_category->GetAllData();
			$data['listplan']=$this->m_plan->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['userprofit']=$this->m_user->getByID($_SESSION['IDPOS'])->intProfit;
			
			$data['haveaccessallplan']=$this->m_plan->cekishaveallaccess($_SESSION['IDPOS']);;
			
			
			
			if($data['setting']=='items')
			{
				$data['listreport']=$this->m_pur_report->GetRepIA($data);
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterangeI']);
			}
			else if($data['setting']=='customer')
			{
				$data['listreport']=$this->m_pur_report->GetRepCA($data);
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterangeC']);
			}
			else if($data['setting']=='general')
			{
				$data['listreport']=$this->m_pur_report->GetRepG($data);
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$data['daterangeG']);
			}
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
}
