<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rfp extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_rfp','',TRUE);
		$this->load->model('m_pro','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemRFP']=0;
			unset($_SESSION['itemcodeRFP']);
			unset($_SESSION['itemnameRFP']);
			unset($_SESSION['qtyRFP']);
			unset($_SESSION['uomRFP']);
			unset($_SESSION['costRFP']);
			
			$data['typetoolbar']='RFP';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('RFP',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_rfp->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listpro']=$this->m_pro->GetAllDataWithPlanAccessRelease($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_rfp->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hRFP');
			}
			$responce->PRONum = $r->PRONum;
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->Whs = $r->intLocation;
			$responce->Type = $r->vcType;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hRFP');
			$responce->PRONum = '';
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->Whs = '';
			$responce->Type = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderPro()
	{
		
			$id=$_POST['id'];
			$r=$this->m_pro->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			$responce->PRONum = $r->vcDocNum;
			$responce->Whs = $r->intLocation;
			
			
		echo json_encode($responce);
	}
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemRFP']=0;
			unset($_SESSION['itemcodeRFP']);
			unset($_SESSION['itemnameRFP']);
			unset($_SESSION['qtyRFP']);
			unset($_SESSION['uomRFP']);
			unset($_SESSION['costRFP']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemRFP']=0;
			
			$r=$this->m_rfp->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['itemcodeRFP'][$j]=$d->vcItemCode;
				$_SESSION['itemnameRFP'][$j]=$d->vcItemName;
				$dintQty=(float)$d->intQty;
				if($dintQty<0)
				{
					$_SESSION['qtyRFP'][$j]=$dintQty*-1;
				}
				else
				{
					$_SESSION['qtyRFP'][$j]=$dintQty;
				}
				$_SESSION['uomRFP'][$j]=$d->vcUoM;
				$_SESSION['costRFP'][$j]=$d->intCost;
				$j++;
			}
			$_SESSION['totitemRFP']=$j;
		}
	}
	function loaddetailpro()
	{
			$id=$_POST['id'];
			$Type=$_POST['Type'];
			$_SESSION['totitemRFP']=0;
			
			
			$head=$this->m_pro->GetHeaderByHeaderID($id);
			$whs=$head->intLocation;
			$j=0;
			
				$_SESSION['itemcodeRFP'][$j]=$head->vcItemCode;
				$_SESSION['itemnameRFP'][$j]=$head->vcItemName;
				
				if($Type=='GI' or $Type=='GIC')
				{
					$_SESSION['qtyRFP'][$j]=$head->intActualQty;
				}
				else if($Type=='GR' or $Type=='GRC')
				{
					$_SESSION['qtyRFP'][$j]=$head->intPlannedQty-$head->intActualQty;
				}
				else
				{
					$_SESSION['qtyRFP'][$j]=$head->intPlannedQty;
				}
				
				
				$item=$this->m_item->GetIDByName($head->vcItemName);
				$uom=$this->m_item->GetUoMByName($head->vcItemName);
				
				$_SESSION['uomRFP'][$j]=$uom;
				$_SESSION['costRFP'][$j]=$head->intCost/$head->intQty;
				
			
			$_SESSION['totitemRFP']=$j+1;
		
	}
	function loadcost()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		
		$item=$this->m_item->GetIDByName($data['detailItem']);
		$cost=$this->m_stock->GetCostItem($item,$data['Whs']);
		echo $cost;
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	
	/*
	
		CHECK FUNCTION
	
	*/
	function cekminusD()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$cek=$this->m_stock->cekMinusStock($item,$_POST['detailQty'],$_POST['Whs']);
		echo $cek;
	}
	function cekminusH()
	{
		$hasil=1;
		for($j=0;$j<$_SESSION['totitemRFP'];$j++)
		{
			if(isset($_SESSION['itemcodeRFP'][$j]))
			{
				if($_SESSION['itemcodeRFP'][$j]!='')
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameRFP'][$j]);
					$cek=$this->m_stock->cekMinusStock($item,$_SESSION['qtyRFP'][$j],$_POST['Whs']);
					if($cek==0)
					{
						$hasil=$_SESSION['itemnameRFP'][$j];
						break;
					}
				}
			}
		}
		echo $hasil;
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemRFP'];$j++)
		{
			if(isset($_SESSION['itemcodeRFP'][$j]))
			{
				if($_SESSION['itemcodeRFP'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekPro()
	{
		$r=$this->m_pro->CekNumPro($_POST['PRONum']);
		echo $r;
	}
	
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['PRONum'] = isset($_POST['PRONum'])?$_POST['PRONum']:''; // get the requested page
		$data['PRONum'] = $this->m_pro->GetIDByDocNum($data['PRONum']); // ambil ID PRO berdasarkan DocNum PRO
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['Whs'] = isset($_POST['Whs'])?$_POST['Whs']:''; // get the requested page
		$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_rfp->insertH($data);
		
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemRFP'];$j++)// save detail
			{
				if($_SESSION['itemcodeRFP'][$j]!="")
				{
					$cek=1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameRFP'][$j]);
					$da['intHID']=$head;
					$da['itemcodeRFP']=$_SESSION['itemcodeRFP'][$j];
					$da['itemnameRFP']=$_SESSION['itemnameRFP'][$j];
					$da['Whs']=$data['Whs'];
					if($data['Type']=='GR')
					{
						$da['qtyRFP']=$_SESSION['qtyRFP'][$j];
						$tipemutasi = 'RGR';
					}
					else if($data['Type']=='GRC')
					{
						$da['qtyRFP']=$_SESSION['qtyRFP'][$j];
						$tipemutasi = 'RGRC';
					}
					else if($data['Type']=='GI')
					{
						$da['qtyRFP']=$_SESSION['qtyRFP'][$j]*-1;
						$tipemutasi = 'RGI';
					}
					else if($data['Type']=='GIC')
					{
						$da['qtyRFP']=$_SESSION['qtyRFP'][$j]*-1;
						$tipemutasi = 'RGIC';
					}
					else
					{
						$tipemutasi = 'Unknown';
					}
					$da['uomRFP']=$_SESSION['uomRFP'][$j];
					$da['costRFP']=$_SESSION['costRFP'][$j];
					$da['linetotalcost'] = $da['qtyRFP']*$da['costRFP'];
					$detail=$this->m_rfp->insertD($da);
					if($detail==0)
					{
						$errordetail++;
					}
					
					$this->m_stock->updateStock($item,$da['qtyRFP'],$data['Whs']);//update stok menambah/mengurangi di gudang
					if($data['Type']=='GR' or $data['Type']=='GRC')
					{
						$this->m_stock->updateCost($item,$da['qtyRFP'],$da['costRFP'],$da['Whs']);//update cost per gudang hanya untuk good receipt
					}
					
					//$da['linetotalcost']=$da['linetotalcost']*-1;
					
					$this->m_stock->addMutation($item,$da['qtyRFP'],$da['costRFP'],$da['Whs'],$tipemutasi,$data['DocDate'],$data['DocNum']);//add mutation
					
					if($data['Type']=='GR' or $data['Type']=='GI')
					{
						$this->m_pro->updateActualQty($data['PRONum'],$da['qtyRFP']);//update Actual Qty
						$this->m_pro->updateProductCost($data['PRONum'],$da['linetotalcost']);//update PRO
				
					}
					else if($data['Type']=='GRC' or $data['Type']=='GIC')
					{
						$this->m_pro->updateRejectQty($data['PRONum'],$da['qtyRFP']);//update Reject Qty
						$this->m_pro->updateRejectCost($data['PRONum'],$da['linetotalcost']);//update PRO
					}
				}
					
					
			}
			$_SESSION['totitemRFP']=0;
			unset($_SESSION['itemcodeRFP']);
			unset($_SESSION['itemnameRFP']);
			unset($_SESSION['qtyRFP']);
			unset($_SESSION['uomRFP']);
			unset($_SESSION['costRFP']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_rfp->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>PRO. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>Whs</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->PRONum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocation.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemRFP'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemRFP'];$j++)
		{
			if($_SESSION['itemcodeRFP'][$j]==$code)
			{
				$_SESSION['qtyRFP'][$j]=$_POST['detailQty'];
				$_SESSION['costRFP'][$j]=$_POST['detailCost'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeRFP'][$i]=$code;
				$_SESSION['itemnameRFP'][$i]=$_POST['detailItem'];
				$_SESSION['qtyRFP'][$i]=$_POST['detailQty'];
				$_SESSION['costRFP'][$i]=$_POST['detailCost'];
				$_SESSION['uomRFP'][$i]=$UoM;
				$_SESSION['totitemRFP']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemRFP'];$j++)
		{
			if($_SESSION['itemcodeRFP'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeRFP'][$j]="";
				$_SESSION['itemnameRFP'][$j]="";
				$_SESSION['qtyRFP'][$j]="";
				$_SESSION['uomRFP'][$j]="";
				$_SESSION['costRFP'][$j]="";
			}
		}
	}
	
	
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemRFP'];$j++)
			{
				if($_SESSION['itemnameRFP'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameRFP'][$j]);
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeRFP'][$j].'</td>
						<td>'.$_SESSION['itemnameRFP'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyRFP'][$j],'2').'</td>
						<td>'.$_SESSION['uomRFP'][$j].'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameRFP"][$j].'\',\''.$_SESSION["itemcodeRFP"][$j].'\',\''.$_SESSION["qtyRFP"][$j].'\',\''.$_SESSION["costRFP"][$j].'\',\''.$_SESSION["uomRFP"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeRFP"][$j].'\',\''.$_SESSION["qtyRFP"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
	
}
