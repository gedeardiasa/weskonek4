<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Root extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_dashboard','',TRUE);
        $this->load->database();
		
    }
	
	public function index()
	{	
		
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('loginroot',$data);
		
		
	}
	
}
