<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class So extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemSO']=0;
			unset($_SESSION['itemcodeSO']);
			unset($_SESSION['idSO']);
			unset($_SESSION['idBaseRefSO']);
			unset($_SESSION['itemnameSO']);
			unset($_SESSION['qtySO']);
			unset($_SESSION['qtyOpenSO']);
			unset($_SESSION['uomSO']);
			unset($_SESSION['priceSO']);
			unset($_SESSION['discSO']);
			unset($_SESSION['whsSO']);
			unset($_SESSION['statusSO']);
			unset($_SESSION['qtyLoadSO']);
			
			$data['typetoolbar']='SO';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('sales quotation',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			$data['list']=$this->m_so->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_so->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hSO');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hSO');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Service = '0';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderSQ()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sq->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hSO');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			$responce->Service = $r->intService;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		
		echo json_encode($responce);
	}
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemSO']=0;
			unset($_SESSION['itemcodeSO']);
			unset($_SESSION['idSO']);
			unset($_SESSION['idBaseRefSO']);
			unset($_SESSION['itemnameSO']);
			unset($_SESSION['qtySO']);
			unset($_SESSION['qtyOpenSO']);
			unset($_SESSION['uomSO']);
			unset($_SESSION['priceSO']);
			unset($_SESSION['discSO']);
			unset($_SESSION['whsSO']);
			unset($_SESSION['statusSO']);
			unset($_SESSION['qtyLoadSO']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemSO']=0;
			unset($_SESSION['itemcodeSO']);
			unset($_SESSION['idSO']);
			unset($_SESSION['idBaseRefSO']);
			unset($_SESSION['itemnameSO']);
			unset($_SESSION['qtySO']);
			unset($_SESSION['qtyOpenSO']);
			unset($_SESSION['uomSO']);
			unset($_SESSION['priceSO']);
			unset($_SESSION['discSO']);
			unset($_SESSION['whsSO']);
			unset($_SESSION['statusSO']);
			unset($_SESSION['qtyLoadSO']);
			
			$r=$this->m_so->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idSO'][$j]=$d->intID;
				$_SESSION['BaseRefSO'][$j]='';
				$_SESSION['itemcodeSO'][$j]=$d->vcItemCode;
				$_SESSION['itemnameSO'][$j]=$d->vcItemName;
				$_SESSION['qtySO'][$j]=$d->intQty;
				$_SESSION['qtyOpenSO'][$j]=$d->intOpenQty;
				$_SESSION['qtyLoadSO'][$j]=$d->intLoadQty;
				if($d->intService==0)
				{
					$_SESSION['uomSO'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomSO'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceSO'][$j]=$d->intPrice;
				$_SESSION['discSO'][$j]=$d->intDiscPer;
				$_SESSION['whsSO'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusSO'][$j]='O';
				}
				else
				{
					$_SESSION['statusSO'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemSO']=$j;
		}
	}
	function loaddetailsq()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemSO']=0;
		unset($_SESSION['itemcodeSO']);
		unset($_SESSION['idSO']);
		unset($_SESSION['idBaseRefSO']);
		unset($_SESSION['itemnameSO']);
		unset($_SESSION['qtySO']);
		unset($_SESSION['qtyOpenSO']);
		unset($_SESSION['uomSO']);
		unset($_SESSION['priceSO']);
		unset($_SESSION['discSO']);
		unset($_SESSION['whsSO']);
		unset($_SESSION['statusSO']);
		unset($_SESSION['qtyLoadSO']);
		
		$r=$this->m_sq->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefSO'][$j]=$d->intHID;
				$_SESSION['BaseRefSO'][$j]='SQ';
				$_SESSION['itemcodeSO'][$j]=$d->vcItemCode;
				$_SESSION['itemnameSO'][$j]=$d->vcItemName;
				$_SESSION['qtySO'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenSO'][$j]=$d->intOpenQty;
				if($d->intService==0)
				{
					$_SESSION['uomSO'][$j]=$d->intUoMType;
				}
				else
				{
					$_SESSION['uomSO'][$j]=$d->vcUoMInv;
				}
				
				$_SESSION['priceSO'][$j]=$d->intPrice;
				$_SESSION['discSO'][$j]=$d->intDiscPer;
				$_SESSION['whsSO'][$j]=$d->intLocation;
				$_SESSION['statusSO'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemSO']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if(($_SESSION['itemcodeSO'][$j]!='' and $_SESSION['itemcodeSO'][$j]!=null) or ($_SESSION['itemnameSO'][$j]!='' and $_SESSION['itemnameSO'][$j]!=null))
			{
				$total=$total+(($_SESSION['qtySO'][$j]*$_SESSION['priceSO'][$j])-($_SESSION['discSO'][$j]/100*($_SESSION['qtySO'][$j]*$_SESSION['priceSO'][$j])));
			}
		}
		echo $total;
	}
	
	
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if(isset($_SESSION['itemcodeSO'][$j]) or isset($_SESSION['itemnameSO'][$j]))
			{
				if($_SESSION['itemcodeSO'][$j]!='' or $_SESSION['itemnameSO'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseSQ($id,$item,$qty,$qtyinv)
	{
		$this->m_sq->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_so->insertH($data);
		//$this->m_bp->updateBDO($data['BPId'],'intOrder',$data['DocTotal']);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemSO'];$j++)// save detail
			{
				if($_SESSION['itemcodeSO'][$j]!="" or $_SESSION['itemnameSO'][$j]!="")
				{
					$cek=1;
					
					if($data['Service']==0)
					{
						$item=$this->m_item->GetIDByName($_SESSION['itemnameSO'][$j]);
						$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameSO'][$j]);
					}
					else
					{
						$item=0;
					}
					
					$whs=$this->m_location->GetNameByID($_SESSION['whsSO'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeSO']=$_SESSION['itemcodeSO'][$j];
					$da['itemnameSO']=$_SESSION['itemnameSO'][$j];
					$da['qtySO']=$_SESSION['qtySO'][$j];
					$da['whsSO']=$_SESSION['whsSO'][$j];
					$da['whsNameSO']=$whs;
					
					if($_SESSION['uomSO'][$j]==1 and $data['Service']==0)
					{
						$da['uomSO']=$vcUoM->vcUoM;
						$da['qtyinvSO']=$da['qtySO'];
					}
					else if($_SESSION['uomSO'][$j]==2 and $data['Service']==0)
					{
						$da['uomSO']=$vcUoM->vcSlsUoM;
						$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intSlsUoM',1);
					}
					else if($_SESSION['uomSO'][$j]==3 and $data['Service']==0)
					{
						$da['uomSO']=$vcUoM->vcPurUoM;
						$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intPurUoM',1);
					}
					else
					{
						$da['uomSO']   = $_SESSION['uomSO'][$j];
						$da['qtyinvSO']= $da['qtySO'];
					}
					
					if(isset($_SESSION['idBaseRefSO'][$j]))
					{
						$da['idBaseRefSO']=$_SESSION['idBaseRefSO'][$j];
						$da['BaseRefSO']=$_SESSION['BaseRefSO'][$j];
					}
					else
					{
						$da['idBaseRefSO']=0;
						$da['BaseRefSO']='';
					}
					
					if($da['BaseRefSO']=='SQ')
					{
						$this->cekcloseSQ($da['idBaseRefSO'], $da['itemID'], $da['qtySO'], $da['qtyinvSO']);
					}
					$da['uomtypeSO']=$_SESSION['uomSO'][$j];
					if($data['Service']==0)
					{
						$da['uominvSO']=$vcUoM->vcUoM;
					}
					else
					{
						$da['uominvSO'] = $da['uomSO'];
					}
					
					
					
					if(!isset($_SESSION['qtyLoadSO'][$j]))
					{
						$da['qtyLoadSO']=0;
					}
					else
					{
						$da['qtyLoadSO']=$_SESSION['qtyLoadSO'][$j];
					}
					$da['priceSO']= $_SESSION['priceSO'][$j];
					$da['discperSO'] = $_SESSION['discSO'][$j];
					$da['discSO'] = $_SESSION['discSO'][$j]/100*$da['priceSO'];
					$da['priceafterSO']=(100-$_SESSION['discSO'][$j])/100*$da['priceSO'];
					$da['linetotalSO']= $da['priceafterSO']*$da['qtySO'];
					$detail=$this->m_so->insertD($da);
				}
			}
			$_SESSION['totitemSO']=0;
			unset($_SESSION['itemcodeSO']);
			unset($_SESSION['idSO']);
			unset($_SESSION['idBaseRefSO']);
			unset($_SESSION['itemnameSO']);
			unset($_SESSION['qtySO']);
			unset($_SESSION['qtyOpenSO']);
			unset($_SESSION['uomSO']);
			unset($_SESSION['priceSO']);
			unset($_SESSION['discSO']);
			unset($_SESSION['whsSO']);
			unset($_SESSION['statusSO']);
			unset($_SESSION['qtyLoadSO']);
			echo 1;
		}
		
		
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['Table'] = isset($_POST['Table'])?$_POST['Table']:0; // get the requested page
		$data['Service'] = isset($_POST['Service'])?$_POST['Service']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		//$lastdata=$this->m_so->GetHeaderByHeaderID($data['id']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_so->editH($data);
		
		//$data['DocTotalBP']=$data['DocTotal']-$lastdata->intDocTotal;
		//$data['BPId']=$lastdata->intBP;
		//$this->m_bp->updateBDO($data['BPId'],'intOrder',$data['DocTotalBP']);
		if($data['id']!=0)
		{
			for($j=0;$j<$_SESSION['totitemSO'];$j++)// save detail
			{
				if($_SESSION['statusSO'][$j]=='O')
				{
					$cek=1;
					
					if($_SESSION['itemcodeSO'][$j]=='' and $_SESSION['itemnameSO'][$j]=='')//jika tidak ada item code artinya detail ini dihapus
					{
						if(isset($_SESSION['idSO'][$j]))
						{
							$this->m_so->deleteD($_SESSION['idSO'][$j]);
						}
					}
					else
					{
						if($data['Service']==0)
						{
							$item=$this->m_item->GetIDByName($_SESSION['itemnameSO'][$j]);
							$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameSO'][$j]);
						}
						else
						{
							$item=0;
						}
						
						$whs=$this->m_location->GetNameByID($_SESSION['whsSO'][$j]);
						
						
						$da['intHID']=$data['id'];
						$da['itemID']=$item;
						$da['itemcodeSO']=$_SESSION['itemcodeSO'][$j];
						$da['itemnameSO']=$_SESSION['itemnameSO'][$j];
						$da['qtySO']=$_SESSION['qtySO'][$j];
						$da['whsSO']=$_SESSION['whsSO'][$j];
						$da['whsNameSO']=$whs;
						
						if($_SESSION['uomSO'][$j]==1 and $data['Service']==0)
						{
							$da['uomSO']=$vcUoM->vcUoM;
							$da['qtyinvSO']=$da['qtySO'];
						}
						else if($_SESSION['uomSO'][$j]==2 and $data['Service']==0)
						{
							$da['uomSO']=$vcUoM->vcSlsUoM;
							$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intSlsUoM',1);
						}
						else if($_SESSION['uomSO'][$j]==3 and $data['Service']==0)
						{
							$da['uomSO']=$vcUoM->vcPurUoM;
							$da['qtyinvSO']=$this->m_item->convert_qty($item,$da['qtySO'],'intPurUoM',1);
						}
						else
						{
							$da['uomSO']   = $_SESSION['uomSO'][$j];
							$da['qtyinvSO']= $da['qtySO'];
						}
						
						if(isset($_SESSION['idBaseRefSO'][$j]))
						{
							$da['idBaseRefSO']=$_SESSION['idBaseRefSO'][$j];
							$da['BaseRefSO']=$_SESSION['BaseRefSO'][$j];
						}
						else
						{
							$da['idBaseRefSO']=0;
							$da['BaseRefSO']='';
						}
						
						$da['uomtypeSO']=$_SESSION['uomSO'][$j];
						if($data['Service']==0)
						{
							$da['uominvSO']=$vcUoM->vcUoM;
						}
						else
						{
							$da['uominvSO'] = $da['uomSO'];
						}
						
						
						$da['priceSO']= $_SESSION['priceSO'][$j];
						$da['discperSO'] = $_SESSION['discSO'][$j];
						$da['discSO'] = $_SESSION['discSO'][$j]/100*$da['priceSO'];
						$da['priceafterSO']=(100-$_SESSION['discSO'][$j])/100*$da['priceSO'];
						$da['linetotalSO']= $da['priceafterSO']*$da['qtySO'];
						
						if(!isset($_SESSION['qtyLoadSO'][$j]))
						{
							$da['qtyLoadSO']=0;
						}
						else
						{
							$da['qtyLoadSO']=$_SESSION['qtyLoadSO'][$j];
						}
						if(isset($_SESSION['idSO'][$j])) //jika ada session id artinya data detail dirubah
						{
							//echo $_SESSION['idSO'][$j];
							$da['intID']=$_SESSION['idSO'][$j];
							$detail=$this->m_so->editD($da);
							
						}
						else //jika tidak artinya ini merupakan detail tambahan
						{
							$detail=$this->m_so->insertD($da);
						}
						
					}
				}
			}
			$_SESSION['totitemSO']=0;
			unset($_SESSION['itemcodeSO']);
			unset($_SESSION['idSO']);
			unset($_SESSION['idBaseRefSO']);
			unset($_SESSION['itemnameSO']);
			unset($_SESSION['qtySO']);
			unset($_SESSION['qtyOpenSO']);
			unset($_SESSION['uomSO']);
			unset($_SESSION['priceSO']);
			unset($_SESSION['discSO']);
			unset($_SESSION['whsSO']);
			unset($_SESSION['statusSO']);
			unset($_SESSION['qtyLoadSO']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_so->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			
			if($d->abc==0)
			{
				$fontcolor='';
			}
			else
			{
				$fontcolor='style="color:red"';
			}
			echo '
			<tr '.$background.' '.$fontcolor.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_so->closeall($_POST['idHeader']);
		$lastdata=$this->m_so->GetHeaderByHeaderID($_POST['idHeader']);
		$data['DocTotalBP']=$lastdata->intDocTotal*-1;
		$data['BPId']=$lastdata->intBP;
		$this->m_bp->updateBDO($data['BPId'],'intOrder',$data['DocTotalBP']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemSO'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if($_SESSION['itemcodeSO'][$j]==$code and $code!=null)
			{
				$_SESSION['qtySO'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenSO'][$j]=$_POST['detailQty'];
				$_SESSION['uomSO'][$j]=$_POST['detailUoM'];
				$_SESSION['priceSO'][$j]=$_POST['detailPrice'];
				$_SESSION['discSO'][$j]=$_POST['detailDisc'];
				$_SESSION['whsSO'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefSO'][$j]='';
				$updateqty=1;
			}
			if($_POST['Service']==1 and $_SESSION['itemnameSO'][$j]==$_POST['detailItem']) //jika Service = 1 maka ijinkan walau tidak ada itemcode
			{
				$_SESSION['qtySO'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenSO'][$j]=$_POST['detailQty'];
				$_SESSION['uomSO'][$j]=$_POST['detailUoMS'];
				$_SESSION['priceSO'][$j]=$_POST['detailPrice'];
				$_SESSION['discSO'][$j]=$_POST['detailDisc'];
				$_SESSION['whsSO'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefSO'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeSO'][$i]=$code;
				$_SESSION['itemnameSO'][$i]=$_POST['detailItem'];
				$_SESSION['qtySO'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenSO'][$i]=$_POST['detailQty'];
				$_SESSION['uomSO'][$i]=$_POST['detailUoM'];
				$_SESSION['priceSO'][$i]=$_POST['detailPrice'];
				$_SESSION['discSO'][$i]=$_POST['detailDisc'];
				$_SESSION['whsSO'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefSO'][$i]='';
				$_SESSION['statusSO'][$i]='O';
				$_SESSION['totitemSO']++;
			}
			else
			{
				if($_POST['Service']==1) //jika Service = 1 maka ijinkan walau tidak ada itemcode
				{
					$_SESSION['itemcodeSO'][$i]='';
					$_SESSION['itemnameSO'][$i]=$_POST['detailItem'];
					$_SESSION['qtySO'][$i]=$_POST['detailQty'];
					$_SESSION['qtyOpenSO'][$i]=$_POST['detailQty'];
					$_SESSION['uomSO'][$i]=$_POST['detailUoMS'];
					$_SESSION['priceSO'][$i]=$_POST['detailPrice'];
					$_SESSION['discSO'][$i]=$_POST['detailDisc'];
					$_SESSION['whsSO'][$i]=$_POST['detailWhs'];
					$_SESSION['BaseRefSO'][$i]='';
					$_SESSION['statusSO'][$i]='O';
					$_SESSION['totitemSO']++;
				}
				else
				{
					echo "false";
				}
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if($_SESSION['itemcodeSO'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['itemcodeSO'][$j]="";
				$_SESSION['itemnameSO'][$j]="";
				$_SESSION['qtySO'][$j]="";
				$_SESSION['qtyOpenSO'][$j]="";
				$_SESSION['uomSO'][$j]="";
				$_SESSION['priceSO'][$j]="";
				$_SESSION['discSO'][$j]="";
				$_SESSION['whsSO'][$j]="";
				$_SESSION['BaseRefSO'][$j]='';
			}
			if($_SESSION['itemnameSO'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['itemcodeSO'][$j]="";
				$_SESSION['itemnameSO'][$j]="";
				$_SESSION['qtySO'][$j]="";
				$_SESSION['qtyOpenSO'][$j]="";
				$_SESSION['uomSO'][$j]="";
				$_SESSION['priceSO'][$j]="";
				$_SESSION['discSO'][$j]="";
				$_SESSION['whsSO'][$j]="";
				$_SESSION['BaseRefSO'][$j]='';
			}
		}
	}
	function upDetail()
	{
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if(!isset($_SESSION['qtyLoadSO'][$j]))
			{
				$_SESSION['qtyLoadSO'][$j]=0;
			}
			if($_SESSION['itemcodeSO'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['qtyLoadSO'][$j]=$_SESSION['qtyLoadSO'][$j]+1;
				
			}
			if($_SESSION['itemnameSO'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['qtyLoadSO'][$j]=$_SESSION['qtyLoadSO'][$j]+1;
			}
		}
	}
	function downDetail()
	{
		for($j=0;$j<$_SESSION['totitemSO'];$j++)
		{
			if(!isset($_SESSION['qtyLoadSO'][$j]))
			{
				$_SESSION['qtyLoadSO'][$j]=0;
			}
			if($_SESSION['itemcodeSO'][$j]==$_POST['code'] and $_POST['Service']==0)
			{
				$_SESSION['qtyLoadSO'][$j]=$_SESSION['qtyLoadSO'][$j]-1;
				
			}
			if($_SESSION['itemnameSO'][$j]==$_POST['name'] and $_POST['Service']==1)
			{
				$_SESSION['qtyLoadSO'][$j]=$_SESSION['qtyLoadSO'][$j]-1;
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
				  <th>Load Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemSO'];$j++)
			{
				if($_SESSION['itemnameSO'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameSO'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameSO'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsSO'][$j]);
					
					if($_SESSION['uomSO'][$j]==1 and $item!=null)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomSO'][$j]==2 and $item!=null)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomSO'][$j]==3 and $item!=null)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					else
					{
						$viewUoM=$_SESSION['uomSO'][$j];
					}
					
					if($_SESSION['discSO'][$j]=='')
					{
						$_SESSION['discSO'][$j]=0;
					}
					
					if($_SESSION['statusSO'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					
					if(!isset($_SESSION['qtyLoadSO'][$j]))
					{
						$_SESSION['qtyLoadSO'][$j]=0;
					}
					
					if($_SESSION['qtyOpenSO'][$j]>$_SESSION['qtyLoadSO'][$j])
					{
						$colorload="red";
						$colorfont="#ffffff";
					}
					else
					{
						$colorload='';
						$colorfont="";
					}
					$lineTotal=((100-$_SESSION['discSO'][$j])/100)*$_SESSION['priceSO'][$j]*$_SESSION['qtySO'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeSO'][$j].'</td>
						<td style="color:'.$colorload.'">'.$_SESSION['itemnameSO'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtySO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenSO'][$j],'2').'</td>
						<td align="right" style="background-color:'.$colorload.'; color:'.$colorfont.'">'.number_format($_SESSION['qtyLoadSO'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceSO'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discSO'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusSO'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameSO"][$j].'\',\''.$_SESSION["itemcodeSO"][$j].'\',\''.$_SESSION["qtySO"][$j].'\',\''.$_SESSION["uomSO"][$j].'\',\''.$viewUoM.'\',\''.$_SESSION["priceSO"][$j].'\',\''.$_SESSION["discSO"][$j].'\',\''.$_SESSION["whsSO"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true" title="Edit"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeSO"][$j].'\',\''.$_SESSION["itemnameSO"][$j].'\',\''.$_SESSION["qtySO"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true" title="Delete"></i></a>
								<a href="#" onclick="upDetail(\''.$_SESSION["itemcodeSO"][$j].'\',\''.$_SESSION["itemnameSO"][$j].'\',\''.$_SESSION["qtySO"][$j].'\')">
								<i class="fa fa-arrow-up '.$data['usericon'].'" aria-hidden="true" title="Add the number of load"></i></a>
								<a href="#" onclick="downDetail(\''.$_SESSION["itemcodeSO"][$j].'\',\''.$_SESSION["itemnameSO"][$j].'\',\''.$_SESSION["qtySO"][$j].'\')">
								<i class="fa fa-arrow-down '.$data['usericon'].'" aria-hidden="true" title="Reduce the amount of load"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
