<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sr extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_sr','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_bp_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_price','',TRUE);
		$this->load->model('m_tax','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemSR']=0;
			unset($_SESSION['itemcodeSR']);
			unset($_SESSION['idSR']);
			unset($_SESSION['idBaseRefSR']);
			unset($_SESSION['itemnameSR']);
			unset($_SESSION['qtySR']);
			unset($_SESSION['qtyOpenSR']);
			unset($_SESSION['uomSR']);
			unset($_SESSION['priceSR']);
			unset($_SESSION['discSR']);
			unset($_SESSION['whsSR']);
			unset($_SESSION['statusSR']);
			
			$data['typetoolbar']='SR';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('sales return',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			
			//$data['list']=$this->m_sr->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listtax']=$this->m_tax->GetActiveData();
			$data['listsq']=$this->m_sq->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listso']=$this->m_so->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listdn']=$this->m_dn->GetOpenDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['autoitem']=$this->m_item->GetAllDataSls();
			$data['autoitemcategory']=$this->m_item_category->GetAllDataSls();
			$data['autobp']=$this->m_bp->GetAllDataCustomer();
			$data['autobpcategory']=$this->m_bp->GetAllCategoryByType('C');
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	/*
	
		GET FUNCTION
	
	*/
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_sr->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
				$responce->Status = $r->vcStatusName;
				$responce->StatusCode = $r->vcStatus;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hSR');
				$responce->Status = 'Open';
				$responce->StatusCode = 'O';
				
			}
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DocTotalBefore = $r->intDocTotalBefore;
			$responce->DiscPer = $r->intDiscPer;
			$responce->Disc = $r->intDisc;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			$responce->Tax =$r->intTax;
			$responce->DocTotal = $r->intDocTotal;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hSR');
			$responce->BPCode = '';
			$responce->BPName = '';
			$responce->RefNum = '';
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
			$responce->DocDate = date('m/d/Y');
			$responce->DelDate = date('m/d/Y');
			$responce->SalesEmp = '';
			$responce->Remarks = '';
			
			$responce->DocTotalBefore = 0;
			$responce->DiscPer = 0;
			$responce->Disc = 0;
			$responce->Freight = 0;
			$responce->TaxPer = 0;
			$responce->Tax = 0;
			$responce->DocTotal = 0;
			
		}
		echo json_encode($responce);
	}
	function getDataHeaderDN()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_dn->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hSR');
			$responce->Status = 'Open';
			$responce->StatusCode = 'O';
				
			
			$responce->BPCode = $r->vcBPCode;
			$responce->BPName = $r->vcBPName;
			$responce->RefNum = $r->vcRef;
			
			
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->DelDate = date('m/d/Y',strtotime($r->dtDelDate));
			$responce->SalesEmp = $r->vcSalesName;
			$responce->Remarks = $r->vcRemarks;
			
			$responce->DiscPer = $r->intDiscPer;
			$responce->Freight = $r->intFreight;
			$responce->TaxPer = $r->intTaxPer;
			
		}
		echo json_encode($responce);
	}
	/*
	
		LOAD FUNCTION
	
	*/
	function loadUoM()
	{
		if(isset($_POST['detailItem']))
		{
			$cek=$this->m_item->GetUoMAllByName($_POST['detailItem']);
			if($_POST['type']=='inv' and is_object($cek))
			{
				echo $cek->vcUoM." (Inv. UoM)";
			}
			else if($_POST['type']=='sls' and is_object($cek))
			{
				echo $cek->vcSlsUoM." (Sls. UoM)";
			}
			else if($_POST['type']=='pur' and is_object($cek))
			{
				echo $cek->vcPurUoM." (Pur. UoM)";
			}
		}
	}
	function loadprice()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$bp=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($item==null)
		{
			$price=0;
		}
		else
		{
			
			if($bp=='')
			{
				$price=$this->m_item->GetPriceByID($item);
				
				if($price==null)
				{
					$price=0;
				}
			}
			else
			{
				$price=$this->m_price->getpricebybpanditem($bp,$item);
			}
		}
		if($price==0)
		{
			$price=$this->m_price->getpricebybpanditem($bp,$item);
			echo $price;
		}
		else
		{
			if($_POST['detailUoM']==1)
			{
				
				echo $this->m_item->convert_price($item,$price,'intSlsUoM',1);
			}
			else if($_POST['detailUoM']==2)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intSlsUoM');
			}
			else if($_POST['detailUoM']==3)
			{
				echo $this->m_item->convert_price($item,$price,'intSlsUoM','intPurUoM');
			}
			else
			{
				echo $price;
			}
		}
	}
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemSR']=0;
			unset($_SESSION['itemcodeSR']);
			unset($_SESSION['idSR']);
			unset($_SESSION['idBaseRefSR']);
			unset($_SESSION['itemnameSR']);
			unset($_SESSION['qtySR']);
			unset($_SESSION['qtyOpenSR']);
			unset($_SESSION['uomSR']);
			unset($_SESSION['priceSR']);
			unset($_SESSION['discSR']);
			unset($_SESSION['whsSR']);
			unset($_SESSION['statusSR']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemSR']=0;
			unset($_SESSION['itemcodeSR']);
			unset($_SESSION['idSR']);
			unset($_SESSION['idBaseRefSR']);
			unset($_SESSION['itemnameSR']);
			unset($_SESSION['qtySR']);
			unset($_SESSION['qtyOpenSR']);
			unset($_SESSION['uomSR']);
			unset($_SESSION['priceSR']);
			unset($_SESSION['discSR']);
			unset($_SESSION['whsSR']);
			unset($_SESSION['statusSR']);
			
			$r=$this->m_sr->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['idSR'][$j]=$d->intID;
				$_SESSION['BaseRefSR'][$j]='';
				$_SESSION['itemcodeSR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameSR'][$j]=$d->vcItemName;
				$_SESSION['qtySR'][$j]=$d->intQty;
				$_SESSION['qtyOpenSR'][$j]=$d->intOpenQty;
				$_SESSION['uomSR'][$j]=$d->intUoMType;
				$_SESSION['priceSR'][$j]=$d->intPrice;
				$_SESSION['discSR'][$j]=$d->intDiscPer;
				$_SESSION['whsSR'][$j]=$d->intLocation;
				if(isset($_POST['duplicated']))
				{
					$_SESSION['statusSR'][$j]='O';
				}
				else
				{
					$_SESSION['statusSR'][$j]=$d->vcStatus;
				}
				
				$j++;
			}
			$_SESSION['totitemSR']=$j;
		}
	}
	function loaddetaildn()
	{
		
		$id=$_POST['id'];
		$_SESSION['totitemSR']=0;
		unset($_SESSION['itemcodeSR']);
		unset($_SESSION['idSR']);
		unset($_SESSION['idBaseRefSR']);
		unset($_SESSION['itemnameSR']);
		unset($_SESSION['qtySR']);
		unset($_SESSION['qtyOpenSR']);
		unset($_SESSION['uomSR']);
		unset($_SESSION['priceSR']);
		unset($_SESSION['discSR']);
		unset($_SESSION['whsSR']);
		unset($_SESSION['statusSR']);
		
		$r=$this->m_dn->GetDetailByHeaderID($id);
		$j=0;
		foreach($r->result() as $d)
		{
			if($d->vcStatus=='O')
			{
				$_SESSION['idBaseRefSR'][$j]=$d->intHID;
				$_SESSION['BaseRefSR'][$j]='DN';
				$_SESSION['itemcodeSR'][$j]=$d->vcItemCode;
				$_SESSION['itemnameSR'][$j]=$d->vcItemName;
				$_SESSION['qtySR'][$j]=$d->intOpenQty;
				$_SESSION['qtyOpenSR'][$j]=$d->intOpenQty;
				$_SESSION['uomSR'][$j]=$d->intUoMType;
				$_SESSION['priceSR'][$j]=$d->intPrice;
				$_SESSION['discSR'][$j]=$d->intDiscPer;
				$_SESSION['whsSR'][$j]=$d->intLocation;
				$_SESSION['statusSR'][$j]=$d->vcStatus;
				
				$j++;
			}
		}
		$_SESSION['totitemSR']=$j;
	}
	function loadtotal()
	{
		$total=0;
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if($_SESSION['itemcodeSR'][$j]!='' and $_SESSION['itemcodeSR'][$j]!=null)
			{
				$total=$total+(($_SESSION['qtySR'][$j]*$_SESSION['priceSR'][$j])-($_SESSION['discSR'][$j]/100*($_SESSION['qtySR'][$j]*$_SESSION['priceSR'][$j])));
			}
		}
		echo $total;
	}
	/*
	
		CHECK FUNCTION
		
	*/
	function cekbp()
	{
		$cek=$this->m_bp->getIDByCode($_POST['BPCode']);
		if($cek=="")
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if(isset($_SESSION['itemcodeSR'][$j]))
			{
				if($_SESSION['itemcodeSR'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	function cekdetailstok()
	{
		$hasil='';
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if($_SESSION['itemcodeSR'][$j]!="")
			{
				$item=$this->m_item->GetIDByCode($_SESSION['itemcodeSR'][$j]);
				if($_SESSION['uomSR'][$j]==1)// konversi uom
				{
					$da['qtyinvSR']=$_SESSION['qtySR'][$j];
				}
				else if($_SESSION['uomSR'][$j]==2)
				{
					$da['qtyinvSR']=$this->m_item->convert_qty($item,$_SESSION['qtySR'][$j],'intSlsUoM',1);
				}
				else if($_SESSION['uomSR'][$j]==3)
				{
					$da['qtyinvSR']=$this->m_item->convert_qty($item,$_SESSION['qtySR'][$j],'intPurUoM',1);
				}
				$res=$this->m_stock->cekMinusStock($item,$da['qtyinvSR'],$_SESSION['whsSR'][$j]);
				if($res==1)
				{
					$hasil=$hasil;
				}
				else
				{
					$hasil=$hasil.'Insufficient stock ('.$_SESSION['itemcodeSR'][$j].' - '.$_SESSION['itemnameSR'][$j].') ';
				}
			}
		}
		if($hasil==''){$hasil=1;}
		echo $hasil;
	}
	function cekdifferentplan()
	{
		$hasil=1;
		$lastplan=0;
		$k=0;
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if($_SESSION['itemcodeSR'][$j]!="")
			{
				$datawhs=$this->m_location->getByID($_SESSION['whsSR'][$j]);
				if($datawhs->intPlan!=$lastplan and $k>0)
				{
					$hasil=0;
				}
				$lastplan=$datawhs->intPlan;
				$k++;
			}
		}
		echo $hasil;
	}
	/*
	
		HEADER FUNCTION
	
	*/
	function cekcloseDN($id,$item,$qty,$qtyinv)
	{
		$this->m_dn->cekclose($id,$item,$qty,$qtyinv);
	}
	function cekcloseSR($id,$item,$qty,$qtyinv)
	{
		$this->m_sr->cekclose($id,$item,$qty,$qtyinv);
	}
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['BPCode'] = isset($_POST['BPCode'])?$_POST['BPCode']:''; // get the requested page
		$data['BPName'] = isset($_POST['BPName'])?$_POST['BPName']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$data['BPId']=$this->m_bp->getIDByCode($data['BPCode']);
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_sr->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemSR'];$j++)// save detail
			{
				if($_SESSION['itemcodeSR'][$j]!="")
				{
					$cek=1;
					
					$item=$this->m_item->GetIDByName($_SESSION['itemnameSR'][$j]);
					$vcUoM=$this->m_item->GetUoMAllByName($_SESSION['itemnameSR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsSR'][$j]);
					
					$da['intHID']=$head;
					$da['itemID']=$item;
					$da['itemcodeSR']=$_SESSION['itemcodeSR'][$j];
					$da['itemnameSR']=$_SESSION['itemnameSR'][$j];
					$da['qtySR']=$_SESSION['qtySR'][$j];
					$da['whsSR']=$_SESSION['whsSR'][$j];
					$da['whsNameSR']=$whs;
					
					if($_SESSION['uomSR'][$j]==1)
					{
						$da['uomSR']=$vcUoM->vcUoM;
						$da['qtyinvSR']=$da['qtySR'];
					}
					else if($_SESSION['uomSR'][$j]==2)
					{
						$da['uomSR']=$vcUoM->vcSlsUoM;
						$da['qtyinvSR']=$this->m_item->convert_qty($item,$da['qtySR'],'intSlsUoM',1);
					}
					else if($_SESSION['uomSR'][$j]==3)
					{
						$da['uomSR']=$vcUoM->vcPurUoM;
						$da['qtyinvSR']=$this->m_item->convert_qty($item,$da['qtySR'],'intPurUoM',1);
					}
					
					if(isset($_SESSION['idBaseRefSR'][$j]))
					{
						$da['idBaseRefSR']=$_SESSION['idBaseRefSR'][$j];
						$da['BaseRefSR']=$_SESSION['BaseRefSR'][$j];
					}
					else
					{
						$da['idBaseRefSR']=0;
						$da['BaseRefSR']='';
					}
					
					$da['uomtypeSR']=$_SESSION['uomSR'][$j];
					$da['uominvSR']=$vcUoM->vcUoM;
					$da['costSR']=$this->m_stock->GetCostItem($item,$da['whsSR']);
					
					$da['priceSR']= $_SESSION['priceSR'][$j];
					$da['discperSR'] = $_SESSION['discSR'][$j];
					$da['discSR'] = $_SESSION['discSR'][$j]/100*$da['priceSR'];
					$da['priceafterSR']=(100-$_SESSION['discSR'][$j])/100*$da['priceSR'];
					$da['linetotalSR']= $da['priceafterSR']*$da['qtySR'];
					$da['linecostSR']=$da['costSR']*$da['qtyinvSR'];
					
					
					$detail=$this->m_sr->insertD($da);
					//fungsi cek close status dokumen referensinya
					if($da['BaseRefSR']=='DN')
					{
						$this->cekcloseDN($da['idBaseRefSR'], $da['itemID'], $da['qtySR'], $da['qtyinvSR']);
						$this->cekcloseSR($head, $da['itemID'], $da['qtySR'], $da['qtyinvSR']); //jika referensi dari DN maka SR akan langsung berubah menjadi close statusnya
					}
					/*$da['qtyinvSR']=$da['qtyinvSR']*-1;*/
					$this->m_stock->updateStock($item,$da['qtyinvSR'],$da['whsSR']);//update stok menambah/mengurangi di gudang
					$this->m_stock->addMutation($item,$da['qtyinvSR'],$da['costSR'],$da['whsSR'],'SR',$data['DocDate'],$data['DocNum']);//add mutation
				}
			}
			$_SESSION['totitemSR']=0;
			unset($_SESSION['itemcodeSR']);
			unset($_SESSION['idSR']);
			unset($_SESSION['idBaseRefSR']);
			unset($_SESSION['itemnameSR']);
			unset($_SESSION['qtySR']);
			unset($_SESSION['qtyOpenSR']);
			unset($_SESSION['uomSR']);
			unset($_SESSION['priceSR']);
			unset($_SESSION['discSR']);
			unset($_SESSION['whsSR']);
			unset($_SESSION['statusSR']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function prosesedit()
	{
		$data['id']=$_POST['idHeader'];
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['DelDate'] = isset($_POST['DelDate'])?$_POST['DelDate']:''; // get the requested page
		$data['SalesEmp'] = isset($_POST['SalesEmp'])?$_POST['SalesEmp']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		$data['DelDate'] = date('Y-m-d',strtotime($data['DelDate']));
		
		$data['DocTotalBefore'] = isset($_POST['DocTotalBefore'])?$_POST['DocTotalBefore']:''; // get the requested page
		$data['DiscPer'] = isset($_POST['DiscPer'])?$_POST['DiscPer']:''; // get the requested page
		$data['Disc'] = isset($_POST['Disc'])?$_POST['Disc']:''; // get the requested page
		$data['Freight'] = isset($_POST['Freight'])?$_POST['Freight']:''; // get the requested page
		$data['TaxPer'] = isset($_POST['TaxPer'])?$_POST['TaxPer']:''; // get the requested page
		$data['Tax'] = isset($_POST['Tax'])?$_POST['Tax']:''; // get the requested page
		$data['DocTotal'] = isset($_POST['DocTotal'])?$_POST['DocTotal']:''; // get the requested page
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['DocNum']);
		$this->m_sr->editH($data);
		echo 1;
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_sr->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>BP</th>
				  <th>Status</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			if($d->vcStatus=='C' or $d->vcStatus=='X')
			{
				$background='style="background-color:#EEEEEE"';
			}
			else
			{
				$background='';
			}
			echo '
			<tr '.$background.'>
                  <td>'.$d->vcDocNum.'</td>
				  <td>'.$d->BPName.'</td>
				  <td>'.$d->vcStatus.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	function closeall()
	{
		$this->m_sr->closeall($_POST['idHeader']);
	}
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemSR'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if($_SESSION['itemcodeSR'][$j]==$code)
			{
				$_SESSION['qtySR'][$j]=$_POST['detailQty'];
				$_SESSION['qtyOpenSR'][$j]=$_POST['detailQty'];
				$_SESSION['uomSR'][$j]=$_POST['detailUoM'];
				$_SESSION['priceSR'][$j]=$_POST['detailPrice'];
				$_SESSION['discSR'][$j]=$_POST['detailDisc'];
				$_SESSION['whsSR'][$j]=$_POST['detailWhs'];
				//$_SESSION['BaseRefSR'][$j]='';
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$_SESSION['itemcodeSR'][$i]=$code;
				$_SESSION['itemnameSR'][$i]=$_POST['detailItem'];
				$_SESSION['qtySR'][$i]=$_POST['detailQty'];
				$_SESSION['qtyOpenSR'][$i]=$_POST['detailQty'];
				$_SESSION['uomSR'][$i]=$_POST['detailUoM'];
				$_SESSION['priceSR'][$i]=$_POST['detailPrice'];
				$_SESSION['discSR'][$i]=$_POST['detailDisc'];
				$_SESSION['whsSR'][$i]=$_POST['detailWhs'];
				$_SESSION['BaseRefSR'][$i]='';
				$_SESSION['statusSR'][$i]='O';
				$_SESSION['totitemSR']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemSR'];$j++)
		{
			if($_SESSION['itemcodeSR'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeSR'][$j]="";
				$_SESSION['itemnameSR'][$j]="";
				$_SESSION['qtySR'][$j]="";
				$_SESSION['qtyOpenSR'][$j]="";
				$_SESSION['uomSR'][$j]="";
				$_SESSION['priceSR'][$j]="";
				$_SESSION['discSR'][$j]="";
				$_SESSION['whsSR'][$j]="";
				$_SESSION['BaseRefSR'][$j]='';
			}
		}
	}
	function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
                  <th>UoM</th>
				  <th>Price</th>
				  <th>Disc.</th>
				  <th>Line Total</th>
				  <th>Whs.</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemSR'];$j++)
			{
				if($_SESSION['itemnameSR'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameSR'][$j]);
					$uom=$this->m_item->GetUoMAllByName($_SESSION['itemnameSR'][$j]);
					$whs=$this->m_location->GetNameByID($_SESSION['whsSR'][$j]);
					
					if($_SESSION['uomSR'][$j]==1)
					{
						$viewUoM=$uom->vcUoM;
					}
					else if($_SESSION['uomSR'][$j]==2)
					{
						$viewUoM=$uom->vcSlsUoM;
					}
					else if($_SESSION['uomSR'][$j]==3)
					{
						$viewUoM=$uom->vcPurUoM;
					}
					
					if($_SESSION['discSR'][$j]=='')
					{
						$_SESSION['discSR'][$j]=0;
					}
					
					if($_SESSION['statusSR'][$j]=='O')
					{
						$colorcolumn='';
					}
					else
					{
						$colorcolumn='style="background-color:#EEEEEE"';
					}
					$lineTotal=((100-$_SESSION['discSR'][$j])/100)*$_SESSION['priceSR'][$j]*$_SESSION['qtySR'][$j];
					echo '
					<tr '.$colorcolumn.'">
						<td>'.$_SESSION['itemcodeSR'][$j].'</td>
						<td>'.$_SESSION['itemnameSR'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtySR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['qtyOpenSR'][$j],'2').'</td>
						<td>'.$viewUoM.'</td>
						<td align="right">'.number_format($_SESSION['priceSR'][$j],'2').'</td>
						<td align="right">'.number_format($_SESSION['discSR'][$j],'2').' %</td>
						<td align="right">'.number_format($lineTotal,'2').'</td>
						<td>'.$whs.'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
						';
						if($_SESSION['statusSR'][$j]=='O')
						{
							echo'
								<div id="controldetail">
								<a href="#" onclick="editDetail(\''.$_SESSION["itemnameSR"][$j].'\',\''.$_SESSION["itemcodeSR"][$j].'\',\''.$_SESSION["qtySR"][$j].'\',\''.$_SESSION["uomSR"][$j].'\',\''.$_SESSION["priceSR"][$j].'\',\''.$_SESSION["discSR"][$j].'\',\''.$_SESSION["whsSR"][$j].'\')">
								<i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
								<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeSR"][$j].'\',\''.$_SESSION["qtySR"][$j].'\')">
								<i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
								</div>
							';
						}
						else
						{
							echo "NOT EDITABLE";
						}
						echo'
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
}
