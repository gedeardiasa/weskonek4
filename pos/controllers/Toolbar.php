<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toolbar extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		
		$this->load->model('m_toolbar','',TRUE);
		$this->load->model('m_setting','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_setting','',TRUE);
		
		$this->load->model('m_ar','',TRUE);
		$this->load->model('m_arcm','',TRUE);
		$this->load->model('m_sq','',TRUE);
		$this->load->model('m_so','',TRUE);
		$this->load->model('m_dn','',TRUE);
		$this->load->model('m_sr','',TRUE);
		
		$this->load->model('m_po','',TRUE);
		$this->load->model('m_pr','',TRUE);
		$this->load->model('m_grpo','',TRUE);
		$this->load->model('m_ap','',TRUE);
		$this->load->model('m_apcm','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		
    }
	function prints($id)
	{
		if($_GET['type']=='POS')
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Print',$typemessage);
			$data['profile']=$this->m_toolbar->GetProfil($id);
			$data['profileH']=$this->m_toolbar->GetProfilHeader();
			
			$data['headerAR']=$this->m_ar->GetHeaderByHeaderID($id);
			$data['detailAR']=$this->m_ar->GetDetailByHeaderID($id);
			$data['cashier']=$this->m_user->getByCode($data['headerAR']->vcUser);
			$data['docsetting']=$this->m_docnum->GetDocSetting('POS');
			
			$this->load->view($this->uri->segment(1).'/print/pos',$data);
			
		}
		
		
		if($_GET['type']=='ARCM')
		{
			$data['header']=$this->m_arcm->GetHeaderByDocNum($id);
			
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_arcm->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('ARCM');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/arcm',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/arcm',$data);
				}
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='SQ')
		{
			$data['header']=$this->m_sq->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_sq->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SQ');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/sq',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/sq',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='AR')
		{
			$data['header']=$this->m_ar->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_ar->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('AR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/ar',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/ar',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='SO')
		{
			$data['header']=$this->m_so->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_so->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/so',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/so',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='DN')
		{
			$data['header']=$this->m_dn->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_dn->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('DN');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/dn',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/dn',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='SR')
		{
			$data['header']=$this->m_sr->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_sr->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('SR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/sr',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/sr',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PO')
		{
			$data['header']=$this->m_po->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_po->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('PO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/po',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/po',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='PR')
		{
			$data['header']=$this->m_pr->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_pr->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('PR');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/pr',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/pr',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='GRPO')
		{
			$data['header']=$this->m_grpo->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_grpo->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('GRPO');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/grpo',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/grpo',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='AP')
		{
			$data['header']=$this->m_ap->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_ap->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('AP');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/ap',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/ap',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
		else if($_GET['type']=='APCM')
		{
			$data['header']=$this->m_apcm->GetHeaderByDocNum($id);
			if(is_object($data['header']))
			{
				$data['detail']=$this->m_apcm->GetDetailByHeaderID($data['header']->intID);
				$data['docsetting']=$this->m_docnum->GetDocSetting('APCM');
				foreach($data['detail']->result() as $dd)
				{
					$intlocation=$dd->intLocation;
					$intplan=$this->m_location->getByID($intlocation)->intPlan;
					break;
				}
				$data['profile']=$this->m_plan->getByID($intplan);
				$data['company']=$this->m_setting->getValueByCode('company_name');
				if(isset($_GET['pdf']))
				{
					$this->load->view($this->uri->segment(1).'/pdf/apcm',$data);
				}
				else
				{
					$this->load->view($this->uri->segment(1).'/print/apcm',$data);
				}
				
			}
			else
			{
				echo "DATA NOT FOUND";
			}
		}
	}
	function pdf($id)
	{
		if($_GET['type']=='ARCM')
		{
			$data['docsetting']=$this->m_docnum->GetDocSetting('ARCM');
			$this->load->library('Pdf');
			$pdf = new TCPDF('L', 'mm', array('236','140'), true, 'UTF-8', false);
			
			//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->SetFont('dejavusans', '', 9);

			$pdf->AddPage();

			$html = file_get_contents('http://trosobo.alamjayaprimanusa.com/pos_test/index.php/toolbar/prints/'.$id.'?type=ARCM&pdf=true');
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output('example_006.pdf', 'I');
		}
		else if($_GET['type']=='AR')
		{
			$data['docsetting']=$this->m_docnum->GetDocSetting('AR');
			$this->load->library('Pdf');
			$pdf = new TCPDF('L', 'mm', array('236','140'), true, 'UTF-8', false);
			
			//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->SetFont('dejavusans', '', 9);

			$pdf->AddPage();

			$html = file_get_contents('http://trosobo.alamjayaprimanusa.com/pos_test/index.php/toolbar/prints/'.$id.'?type=AR&pdf=true');
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output('example_006.pdf', 'I');
		}
	}
	function next()
	{
		
		$id=$_POST['idHeader'];
		if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='AD')
		{
			$table='hAdjustment';
		}
		else if($_POST['type']=='IT')
		{
			$table='hIT';
		}
		else if($_POST['type']=='SO')
		{
			$table='hSO';
		}
		else if($_POST['type']=='DN')
		{
			$table='hDN';
		}
		else if($_POST['type']=='AR')
		{
			$table='hAR';
		}
		else if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='ARCM')
		{
			$table='hARCM';
		}
		else if($_POST['type']=='SR')
		{
			$table='hSR';
		}
		else if($_POST['type']=='PO')
		{
			$table='hPO';
		}
		else if($_POST['type']=='GRPO')
		{
			$table='hGRPO';
		}
		else if($_POST['type']=='AP')
		{
			$table='hAP';
		}
		else if($_POST['type']=='PR')
		{
			$table='hPR';
		}
		else if($_POST['type']=='APCM')
		{
			$table='hAPCM';
		}
		else if($_POST['type']=='INPAY')
		{
			$table='hINPAY';
		}
		else if($_POST['type']=='OUTPAY')
		{
			$table='hOUTPAY';
		}
		else if($_POST['type']=='REVALUATION')
		{
			$table='hRevaluation';
		}
		else if($_POST['type']=='PID')
		{
			$table='hPID';
		}
		else if($_POST['type']=='IP')
		{
			$table='hIP';
		}
		else if($_POST['type']=='DISC')
		{
			$table='hDiscount';
		}
		else if($_POST['type']=='BOM')
		{
			$table='hBOM';
		}
		else if($_POST['type']=='PRO')
		{
			$table='hPRO';
		}
		else if($_POST['type']=='JE')
		{
			$table='hJE';
		}
		$nextid=$this->m_toolbar->GetNextId($id,$table);
		echo $nextid;
	}
	function previous()
	{
		$id=$_POST['idHeader'];
		if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='AD')
		{
			$table='hAdjustment';
		}
		else if($_POST['type']=='IT')
		{
			$table='hIT';
		}
		else if($_POST['type']=='SO')
		{
			$table='hSO';
		}
		else if($_POST['type']=='DN')
		{
			$table='hDN';
		}
		else if($_POST['type']=='AR')
		{
			$table='hAR';
		}
		else if($_POST['type']=='SQ')
		{
			$table='hSQ';
		}
		else if($_POST['type']=='ARCM')
		{
			$table='hARCM';
		}
		else if($_POST['type']=='SR')
		{
			$table='hSR';
		}
		else if($_POST['type']=='PO')
		{
			$table='hPO';
		}
		else if($_POST['type']=='GRPO')
		{
			$table='hGRPO';
		}
		else if($_POST['type']=='AP')
		{
			$table='hAP';
		}
		else if($_POST['type']=='PR')
		{
			$table='hPR';
		}
		else if($_POST['type']=='APCM')
		{
			$table='hAPCM';
		}
		else if($_POST['type']=='INPAY')
		{
			$table='hINPAY';
		}
		else if($_POST['type']=='OUTPAY')
		{
			$table='hOUTPAY';
		}
		else if($_POST['type']=='REVALUATION')
		{
			$table='hRevaluation';
		}
		else if($_POST['type']=='PID')
		{
			$table='hPID';
		}
		else if($_POST['type']=='IP')
		{
			$table='hIP';
		}
		else if($_POST['type']=='DISC')
		{
			$table='hDiscount';
		}
		else if($_POST['type']=='BOM')
		{
			$table='hBOM';
		}
		else if($_POST['type']=='PRO')
		{
			$table='hPRO';
		}
		else if($_POST['type']=='JE')
		{
			$table='hJE';
		}
		$previousid=$this->m_toolbar->GetPreviousId($id,$table);
		echo $previousid;
	}
}
