<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfer extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_transfer','',TRUE);
		$this->load->model('m_item','',TRUE);
		$this->load->model('m_item_category','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_stock','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$_SESSION['totitemIT']=0;
			unset($_SESSION['itemcodeIT']);
			unset($_SESSION['itemnameIT']);
			unset($_SESSION['qtyIT']);
			unset($_SESSION['uomIT']);
			
			
			$data['typetoolbar']='IT';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('transfer',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			//$data['list']=$this->m_transfer->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['defaultwhs']=$this->m_user->getValueUser('intDefaultLoc',$_SESSION['IDPOS']);
			$data['listwhs']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['autoitem']=$this->m_item->GetAllData();
			$data['autoitemcategory']=$this->m_item_category->GetAllData();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	/*
	
		GET FUNCTION
	
	*/
	function getItemCode()
	{
		$data=$this->m_item->GetCodeByName($_POST['detailItem']);
		echo $data;
	}
	function getItemName()
	{
		$data=$this->m_item->GetNameByCode($_POST['detailItemCode']);
		echo $data;
	}
	function getDataHeader()
	{
		if($_POST['id']!='0')
		{
			$id=$_POST['id'];
			$r=$this->m_transfer->GetHeaderByHeaderID($id);
			
			$responce=new stdClass();
			
			$responce->idHeader = $r->intID;
			if($_POST['dup']==0)//jika variabel dup/duplicate==0 artinya bukan duplicate dan docnum sesuai data
			{
				$responce->DocNum = $r->vcDocNum;
			}
			else
			{
				$responce->DocNum = $this->m_docnum->GetLastDocNum('hIT');
			}
			$responce->DocDate = date('m/d/Y',strtotime($r->dtDate));
			$responce->RefNum = $r->vcRef;
			$responce->FromWhs = $r->intLocationFrom;
			$responce->ToWhs = $r->intLocationTo;
			$responce->Remarks = $r->vcRemarks;
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view',$responce->DocNum);
		}
		else
		{
			$responce=new stdClass();
			
			$responce->idHeader = 0;
			$responce->DocNum = $this->m_docnum->GetLastDocNum('hIT');
			$responce->DocDate = date('m/d/Y');
			$responce->RefNum = '';
			$responce->FromWhs = '';
			$responce->ToWhs = '';
			$responce->Remarks = '';
			
		}
		echo json_encode($responce);
		
	}
	
	/*
	
		LOAD FUNCTION
	
	*/
	function loaddetail()
	{
		if(!isset($_POST['id']))
		{
			$_SESSION['totitemIT']=0;
			unset($_SESSION['itemcodeIT']);
			unset($_SESSION['itemnameIT']);
			unset($_SESSION['qtyIT']);
			unset($_SESSION['uomIT']);
		}
		else
		{
			$id=$_POST['id'];
			$_SESSION['totitemIT']=0;
			unset($_SESSION['itemcodeIT']);
			unset($_SESSION['itemnameIT']);
			unset($_SESSION['qtyIT']);
			unset($_SESSION['uomIT']);
			
			$r=$this->m_transfer->GetDetailByHeaderID($id);
			$j=0;
			foreach($r->result() as $d)
			{
				$_SESSION['itemcodeIT'][$j]=$d->vcItemCode;
				$_SESSION['itemnameIT'][$j]=$d->vcItemName;
				$_SESSION['qtyIT'][$j]=$d->intQty;
				$_SESSION['uomIT'][$j]=$d->vcUoM;
				$j++;
			}
			$_SESSION['totitemIT']=$j;
		}
	}
	function loaduom()
	{
		$data['detailItem'] = isset($_POST['detailItem'])?$_POST['detailItem']:''; // get the requested page
		$uom=$this->m_item->GetUoMByName($data['detailItem']);
		echo $uom;
	}
	/*
	
		CHECK FUNCTION
	
	*/
	function cekminusD()
	{
		$item=$this->m_item->GetIDByName($_POST['detailItem']);
		$cek=$this->m_stock->cekMinusStock($item,$_POST['detailQty'],$_POST['FromWhs']);
		echo $cek;
	}
	function cekminusH()
	{
		$hasil=1;
		for($j=0;$j<$_SESSION['totitemIT'];$j++)
		{
			if(isset($_SESSION['itemcodeIT'][$j]))
			{
				$item=$this->m_item->GetIDByName($_SESSION['itemnameIT'][$j]);
				$cek=$this->m_stock->cekMinusStock($item,$_SESSION['qtyIT'][$j],$_POST['FromWhs']);
				if($cek==0)
				{
					$hasil=$_SESSION['itemnameIT'][$j];
					break;
				}
			}
		}
		echo $hasil;
	}
	function cekdetail()
	{
		$hasil=0;
		for($j=0;$j<$_SESSION['totitemIT'];$j++)
		{
			if(isset($_SESSION['itemcodeIT'][$j]))
			{
				if($_SESSION['itemcodeIT'][$j]!='')
				{
					$hasil=1; break;
				}
				else
				{
					$hasil=0;
				}
			}
			else
			{
				$hasil=0;
			}
		}
		echo $hasil;
	}
	
	/*
	
		HEADER FUNCTION
	
	*/
	function prosesadd()
	{
		$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
		$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
		$data['RefNum'] = isset($_POST['RefNum'])?$_POST['RefNum']:''; // get the requested page
		$data['FromWhs'] = isset($_POST['FromWhs'])?$_POST['FromWhs']:''; // get the requested page
		$data['ToWhs'] = isset($_POST['ToWhs'])?$_POST['ToWhs']:''; // get the requested page
		$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
		$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
		
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
		$head=$this->m_transfer->insertH($data);
		if($head!=0)
		{
			for($j=0;$j<$_SESSION['totitemIT'];$j++)// save detail
			{
				if($_SESSION['itemcodeIT'][$j]!="")
				{
					$cek=1;
					$qtyminus=$_SESSION['qtyIT'][$j]*-1;
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIT'][$j]);
					$costfrom=$this->m_stock->GetCostItem($item,$data['FromWhs']);//ambil cost gudang asal
					
					$da['intHID']=$head;
					$da['itemcodeIT']=$_SESSION['itemcodeIT'][$j];
					$da['itemnameIT']=$_SESSION['itemnameIT'][$j];
					$da['qtyIT']=$_SESSION['qtyIT'][$j];
					$da['uomIT']=$_SESSION['uomIT'][$j];
					$da['FromWhs']=$data['FromWhs'];
					$da['ToWhs']=$data['ToWhs'];
					$detail=$this->m_transfer->insertD($da);
					$this->m_stock->updateStock($item,$_SESSION['qtyIT'][$j],$data['ToWhs']);//update stok menambah di gudang tujuan
					$this->m_stock->updateStock($item,$qtyminus,$data['FromWhs']);//update stok mengurangi di gudang asal
					
					$this->m_stock->updateCost($item,$da['qtyIT'],$costfrom,$data['ToWhs']);//update cost gudang tujuan
					
					$this->m_stock->addMutation($item,$da['qtyIT'],$costfrom,$data['ToWhs'],'IT',$data['DocDate']);//add mutation gudang tujuan
					$this->m_stock->addMutation($item,$qtyminus,$costfrom,$data['FromWhs'],'IT',$data['DocDate']);//add mutation gudang asal
				}
			}
			$_SESSION['totitemIT']=0;
			unset($_SESSION['itemcodeIT']);
			unset($_SESSION['itemnameIT']);
			unset($_SESSION['qtyIT']);
			unset($_SESSION['uomIT']);
			echo 1;
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
	}
	function lisHeader()
	{
		//inisialisasi
		$data['crudaccess']=$this->general['crudaccess'];
		
		$data['daterange'] = isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0]);
		$data['until']=str_replace("/","-",$dt[1]);
		$data['list']=$this->m_transfer->GetAllDataWithPlanAccessAndDate($_SESSION['IDPOS'],$data);
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Doc. Num</th>
				  <th>Date</th>
				  <th>Ref. Num</th>
                  <th>From</th>
				  <th>To</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			echo '
			<tr>
                  <td>'.$d->vcDocNum.'</td>
                  <td>'.$d->dtDate.'</td>
				  <td>'.$d->vcRef.'</td>
				  <td>'.$d->vcLocationFrom.'</td>
				  <td>'.$d->vcLocationTo.'</td>
				  <td align="center">
			';
			if($data['crudaccess']->intRead==1) {
				echo '<i class="fa fa-search '.$data['usericon'].'" aria-hidden="true" data-toggle="modal" data-target="#modal-add-edit" onclick="initialedit(\''.$d->intID.'\')"></i>';
			}
			else
			{
				echo 'locked';
			}
			echo '
				</td>
            </tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
	}
	
	
	/*
	
		DETAIL FUNCTION
	
	*/
	function addDetail()
	{
		$i=$_SESSION['totitemIT'];
		$code=$this->m_item->GetCodeByName($_POST['detailItem']);
		
		$updateqty=0;
		for($j=0;$j<$_SESSION['totitemIT'];$j++)
		{
			if($_SESSION['itemcodeIT'][$j]==$code)
			{
				$_SESSION['qtyIT'][$j]=$_POST['detailQty'];
				$updateqty=1;
			}
		}
		if($updateqty==0)
		{
			if($code!=null)
			{
				
				$UoM=$this->m_item->GetUoMByName($_POST['detailItem']);
				$_SESSION['itemcodeIT'][$i]=$code;
				$_SESSION['itemnameIT'][$i]=$_POST['detailItem'];
				$_SESSION['qtyIT'][$i]=$_POST['detailQty'];
				$_SESSION['uomIT'][$i]=$UoM;
				$_SESSION['totitemIT']++;
			}
			else
			{
				echo "false";
			}
		}
	}
	function delDetail()
	{
		for($j=0;$j<$_SESSION['totitemIT'];$j++)
		{
			if($_SESSION['itemcodeIT'][$j]==$_POST['code'])
			{
				$_SESSION['itemcodeIT'][$j]="";
				$_SESSION['itemnameIT'][$j]="";
				$_SESSION['qtyIT'][$j]="";
				$_SESSION['uomIT'][$j]="";
			}
		}
	}
	
	
	
	public function lisDetail()
	{
		$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
		echo '
		<br>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<thead>
				<tr>
				  <th>Code</th>
				  <th>Name</th>
				  <th>Qty</th>
                  <th>UoM</th>
		';
		
		if(!isset($_GET['withoutcontrol']))
		{
			echo '
					  <th style="width:15px">Control</th>
			';
		}
		echo '
				</tr>
				</thead>
				<tbody>'
		;
			
			for($j=0;$j<$_SESSION['totitemIT'];$j++)
			{
				if($_SESSION['itemnameIT'][$j]!="")
				{
					$item=$this->m_item->GetIDByName($_SESSION['itemnameIT'][$j]);
					echo '
					<tr>
						<td>'.$_SESSION['itemcodeIT'][$j].'</td>
						<td>'.$_SESSION['itemnameIT'][$j].'</td>
						<td align="right">'.number_format($_SESSION['qtyIT'][$j],'2').'</td>
						<td>'.$_SESSION['uomIT'][$j].'</td>
					';
					if(!isset($_GET['withoutcontrol']))
					{
						echo '
							<td>
							<a href="#" onclick="editDetail(\''.$_SESSION["itemnameIT"][$j].'\',\''.$_SESSION["itemcodeIT"][$j].'\',\''.$_SESSION["qtyIT"][$j].'\',\''.$_SESSION["uomIT"][$j].'\')"><i class="fa fa-pencil-square-o '.$data['usericon'].'" aria-hidden="true"></i></a>
							<a href="#" onclick="delDetail(\''.$_SESSION["itemcodeIT"][$j].'\',\''.$_SESSION["qtyIT"][$j].'\')"><i class="fa fa-trash '.$data['usericon'].'" aria-hidden="true"></i></a>
							</td>
						';
					}
					echo'
					</tr>
					';
				}
			}			
			echo "
				
				</tbody>
			</table><br>
			";
		echo '
		
		<script>
		  $(function () {
			$("#example3").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	
}
