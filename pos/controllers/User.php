<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_location','',TRUE);
		$this->load->model('m_bp','',TRUE);
		$this->load->model('m_wallet','',TRUE);
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('user',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['listuser']=$this->m_user->GetAllUser();
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	public function detail($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			if ($data['crudaccess']->intRead==0)
			{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('user',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation('user');
			$data['user']=$this->m_user->getByID($id);
			$data['listloc']=$this->m_location->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listwallet']=$this->m_wallet->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			$data['listbp']=$this->m_bp->GetAllData();
			if(is_object($data['user']))
			{
			$data['accessuser']=$this->m_user->getAccessUser($id);
			$data['accessuserHead']=$this->m_user->getAccessUserHead($id);
			$data['accessuserPlan']=$this->m_user->getAccessUserPlan($id);
			$data['accessuserTop']=$this->m_user->getAccessUserTop($id);
			$data['accessdash']=$this->m_user->getAccessUserDashboard($id);
			$this->load->view($this->uri->segment(1).'/detail',$data);
			}
			else
			{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			}
		}
	}
	function lisactivity()
	{
		$data['daterange'] 	= isset($_GET['daterange'])?$_GET['daterange']:date('Y/m/d')." - ".date('Y/m/d'); // get the requested page
		$data['iduser']		= $_GET['iduser'];
			
		$dt=explode("-",$data['daterange']);
		$data['from']=str_replace("/","-",$dt[0])." 00:00:00";
		$data['until']=str_replace("/","-",$dt[1])." 23:59:59";
		
		$data['list']=$this->m_user->GetActivityByDate($data);
				
		echo '
		<table id="example1" class="table table-striped dt-responsive jambo_table">
            <thead>
                <tr>
                  <th>Controller</th>
				  <th>Function</th>
				  <th>Activity</th>
				  <th>Key</th>
                </tr>
                </thead>
                <tbody>
		';
		
		foreach($data['list']->result() as $d) 
		{
			
			
			echo '
			<tr>
                  <td>'.$d->vcController.'</td>
				  <td>'.$d->vcFunction.'</td>
				  <td>'.$d->vcActivity.'</td>
                  <td>'.$d->vcKey.'</td>
			</tr>
			';
		}
		echo '
			</tbody>
        </table>
		';
		
		echo '
		
		<script>
		  $(function () {
			$("#example1").DataTable({
				"oLanguage": {
				  "sSearch": "Search:"
				},
				\'iDisplayLength\': 10,
				//"sPaginationType": "full_numbers",
				"dom": \'T<"clear">lfrtip\',
				"tableTools": {
				  "sSwfPath": ""
				},
				dom: \'Blfrtip\',
				"aaSorting": [],
				buttons: [
				   {
					   extend: \'pdf\',
					   footer: false,
				   },
				   {
					   extend: \'csv\',
					   footer: false
					  
				   },
				   {
					   extend: \'excel\',
					   footer: false
				   }         
				]  
			});
		  });
		  
		 
		</script>';
		
	}
	function prosesadd()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intCreate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['UserID'] = isset($_POST['UserID'])?$_POST['UserID']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Password'] = isset($_POST['Password'])?$_POST['Password']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		if($_FILES['Image']['tmp_name']!='')
		{
			$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
		}
		else
		{
			$data['imgData']=null;
		}
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['UserID']);
		$cek=$this->m_user->insert($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	
	function prosesedit($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['id'] = $id; // get the requested page
		$data['UserID'] = isset($_POST['UserID'])?$_POST['UserID']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Phone'] = isset($_POST['Phone'])?$_POST['Phone']:''; // get the requested page
		if($_FILES['Image']['tmp_name']!='')
		{
			$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
		}
		else
		{
			$data['imgData']=null;
		}
		$this->db->trans_begin();
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['UserID']);
		$cek=$this->m_user->edit($data);
		
		//SETTING
		$data['DefaultLoc'] = isset($_POST['DefaultLoc'])?$_POST['DefaultLoc']:''; // get the requested page
		$data['DefaultBP'] = isset($_POST['DefaultBP'])?$_POST['DefaultBP']:''; // get the requested page
		$data['DefaultWallet'] = isset($_POST['DefaultWallet'])?$_POST['DefaultWallet']:''; // get the requested page
		$data['SetValue'] = isset($_POST['SetValue'])?$_POST['SetValue']:''; // get the requested page
		$data['SetProfit'] = isset($_POST['SetProfit'])?$_POST['SetProfit']:''; // get the requested page
		$data['SetIcon'] = isset($_POST['SetIcon'])?$_POST['SetIcon']:''; // get the requested page
		
		$cek=$this->m_user->editsetting($data);
		//SETTING
		
		//ACCESS
		$access=$this->m_user->getAccessAllWithoutHeader();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST[$index]))
			{
				$cek=$this->m_user->addnewaccess($id,$a->intID);// add new access
				//update CRUD
				$crud['C']=isset($_POST["CF".$index])?1:0; // get the requested page
				$crud['R']=isset($_POST["RF".$index])?1:0; // get the requested page
				$crud['U']=isset($_POST["UF".$index])?1:0; // get the requested page
				$crud['D']=isset($_POST["DF".$index])?1:0; // get the requested page
				$this->m_user->editcrud($id,$a->intID,$crud);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyid($id,$del[$j]);// delete last access
		}
		
		//edit subaccess
		$access=$this->m_user->getAccessAllWithoutHeaderSub();
		$i=0;
		foreach($access->result() as $a)
		{
			
			$index='subcode-'.$a->HCode.''.$a->vcCode;
			//echo $index."<br>";
			if(isset($_POST[$index]))
			{
				//echo $a->intID."<br>";
				$cek=$this->m_user->addnewaccessSub($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del2[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidSub($id,$del2[$j]);// delete last access
		}
		//ACCESS
		
		//ACCESS PLAN
		$access=$this->m_user->getAccessAllPlan();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST["plan".$index]))
			{
				$cek=$this->m_user->addnewaccessplan($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidplan($id,$del[$j]);// delete last access
		}
		//ACCESS PLAN
		
		//ACCESS TOP
		$access=$this->m_user->getAccessAllTop();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST["topform".$index]))
			{
				$cek=$this->m_user->addnewaccesstop($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidtop($id,$del[$j]);// delete last access
		}
		//ACCESS TOP
		
		
		//DASHBOARD
		
		$this->m_user->dellastaccessdashboard($id);// delete last access
		$access=$this->m_user->getAccessAllDashboard();
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST[$index]))
			{
				$order=$_POST[$index."order"];
				$cek=$this->m_user->addnewaccessdashboard($id,$a->intID,$order);// add new access
			}
		}
		
		//DASHBOARD
		
		//Password
		$data['OldPassword'] = isset($_POST['OldPassword'])?$_POST['OldPassword']:''; // get the requested page
		$data['NewPassword'] = isset($_POST['NewPassword'])?$_POST['NewPassword']:''; // get the requested page
		$data['ConfirmPassword'] = isset($_POST['ConfirmPassword'])?$_POST['ConfirmPassword']:''; // get the requested page
		$errorpassword='';
		if($data['NewPassword']!=$data['ConfirmPassword'])
		{
			$errorpassword='&passworderror=true';
		}
		else if($data['NewPassword']!='')
		{
			//$cek=$this->m_user->cekpass($data);
			$cek=1; //by pass cek old password
			if($cek==1)
			{
				$this->m_user->editpass($data);
			}
			$errorpassword='';
		}
		//PASSWORD
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$cek=0;
		}else{
			$this->db->trans_commit();
		}
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successedit'.$errorpassword.'';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroredit'.$errorpassword.'';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditpass($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['id'] = $id; // get the requested page
		$data['OldPassword'] = isset($_POST['OldPassword'])?$_POST['OldPassword']:''; // get the requested page
		$data['NewPassword'] = isset($_POST['NewPassword'])?$_POST['NewPassword']:''; // get the requested page
		$data['ConfirmPassword'] = isset($_POST['ConfirmPassword'])?$_POST['ConfirmPassword']:''; // get the requested page
		
		if($data['NewPassword']!=$data['ConfirmPassword'])
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroreditpassconfirm';
		}
		else
		{
			$cek=$this->m_user->cekpass($data);
			if($cek==1)
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit-password',$data['id']);
				$this->m_user->editpass($data);
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successeditpass';
			}
			else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroreditpass';
			}
		}
		
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditaccess($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		//$this->m_user->dellastaccess($id);// delete last access
		
		//edit access
		$this->db->trans_begin();
		$access=$this->m_user->getAccessAllWithoutHeader();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST[$index]))
			{
				$cek=$this->m_user->addnewaccess($id,$a->intID);// add new access
				//update CRUD
				$crud['C']=isset($_POST["CF".$index])?1:0; // get the requested page
				$crud['R']=isset($_POST["RF".$index])?1:0; // get the requested page
				$crud['U']=isset($_POST["UF".$index])?1:0; // get the requested page
				$crud['D']=isset($_POST["DF".$index])?1:0; // get the requested page
				$this->m_user->editcrud($id,$a->intID,$crud);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyid($id,$del[$j]);// delete last access
		}
		
		//edit subaccess
		$access=$this->m_user->getAccessAllWithoutHeaderSub();
		$i=0;
		foreach($access->result() as $a)
		{
			
			$index='subcode-'.$a->HCode.''.$a->vcCode;
			//echo $index."<br>";
			if(isset($_POST[$index]))
			{
				//echo $a->intID."<br>";
				$cek=$this->m_user->addnewaccessSub($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del2[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidSub($id,$del2[$j]);// delete last access
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successeditaccess';
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditaccessplan($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
		$this->db->trans_begin();
		$access=$this->m_user->getAccessAllPlan();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST["plan".$index]))
			{
				$cek=$this->m_user->addnewaccessplan($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidplan($id,$del[$j]);// delete last access
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successeditaccess';
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditaccesstop($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
		$this->db->trans_begin();
		$access=$this->m_user->getAccessAllTop();
		$i=0;
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST["topform".$index]))
			{
				$cek=$this->m_user->addnewaccesstop($id,$a->intID);// add new access
			}
			else
			{
				//simpan id yang harus di hapus
				$del[$i]=$a->intID;
				$i++;
			}
		}
		for($j=0;$j<$i;$j++)
		{
			$this->m_user->dellastaccessbyidtop($id,$del[$j]);// delete last access
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successeditaccess';
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditsetting($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intUpdate==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$data['id'] = $id; // get the requested page
		$data['DefaultLoc'] = isset($_POST['DefaultLoc'])?$_POST['DefaultLoc']:''; // get the requested page
		$data['DefaultBP'] = isset($_POST['DefaultBP'])?$_POST['DefaultBP']:''; // get the requested page
		$data['DefaultWallet'] = isset($_POST['DefaultWallet'])?$_POST['DefaultWallet']:''; // get the requested page
		$data['SetValue'] = isset($_POST['SetValue'])?$_POST['SetValue']:''; // get the requested page
		$data['SetProfit'] = isset($_POST['SetProfit'])?$_POST['SetProfit']:''; // get the requested page
		
		$cek=$this->m_user->editsetting($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successedit';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?error=true&type=erroredit';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditaccessdashboard($id)
	{
		$this->db->trans_begin();
		$this->m_user->dellastaccessdashboard($id);// delete last access
		$access=$this->m_user->getAccessAllDashboard();
		foreach($access->result() as $a)
		{
			$index=$a->vcCode;
			if(isset($_POST[$index]))
			{
				$order=$_POST[$index."order"];
				$cek=$this->m_user->addnewaccessdashboard($id,$a->intID,$order);// add new access
			}
		}
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/detail/'.$id.'?success=true&type=successedit';
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function prosesdelete($id)
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$cek=$this->m_user->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
}
