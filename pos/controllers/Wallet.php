<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_plan','',TRUE);
		$this->load->model('m_coa','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->library('message');
        $this->load->database();
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('wallet',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			$data['list']=$this->m_wallet->GetAllData();
			$data['listplan']=$this->m_plan->GetAllData();
			$data['listgl']=$this->m_coa->GetAllDataLevel5();
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "add":
				if ($data['crudaccess']->intCreate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['Plan'] = isset($_POST['Plan'])?$_POST['Plan']:''; // get the requested page
				$data['Gl'] = isset($_POST['Gl'])?$_POST['Gl']:''; // get the requested page
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['Code']);
				$cek=$this->m_wallet->insert($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			case "edit":
				if ($data['crudaccess']->intUpdate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['id'] = isset($_POST['ID'])?$_POST['ID']:''; // get the requested page
				$data['Code'] = isset($_POST['Code'])?$_POST['Code']:''; // get the requested page
				$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
				$data['Plan'] = isset($_POST['Plan'])?$_POST['Plan']:''; // get the requested page
				$data['Gl'] = isset($_POST['Gl'])?$_POST['Gl']:''; // get the requested page
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['Code']);
				$cek=$this->m_wallet->edit($data);
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successedit';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroredit';
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
	function prosesdelete()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if($data['crudaccess']->intDelete==0)
		{
			$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$id=$_POST['ID2'];
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'delete',$id);
		$cek=$this->m_wallet->delete($id);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?success=true&type=successdelete';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/?error=true&type=errordelete';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		
	}
}
