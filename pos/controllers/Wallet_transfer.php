<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet_transfer extends CI_Controller {

	public $general = array();
	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		if ($this->authorization->ceklogin()==false)
		{
			$data['link']=$this->config->base_url().'index.php/';
			echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		$this->load->model('m_wallet','',TRUE);
		$this->load->model('m_docnum','',TRUE);
		$this->load->model('m_user','',TRUE);
		
		$this->load->library('message');
        $this->load->database('default');
		$this->authorization->cekform($this->uri->segment(1));
		$this->general['crudaccess'] = $this->authorization->getcrudaccess($this->uri->segment(1));
    }
	public function index()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		if ($this->authorization->ceklogin()==false)
		{
			$this->load->view('login');
		}
		else
		{
			$data['typetoolbar']='Wallet Transfer';
			
			$typemessage = isset($_GET['type'])?$_GET['type']:''; // get the requested page
			$data['message']=$this->message->GetMessage('Wallet Transfer',$typemessage);
			$data['form']=$this->authorization->GetForm($_SESSION);
			$data['navigation']=$this->authorization->GetNavigation($this->uri->segment(1));
			$data['usericon']=$this->m_user->getByID($_SESSION['IDPOS'])->vcIcon;
			
			$data['liswallet']=$this->m_wallet->GetAllDataWithPlanAccess($_SESSION['IDPOS']);
			
			$data['list']=$this->m_wallet->GetAllDataWalletTransferPlanAccess($_SESSION['IDPOS']);
			$data['DocNum']=$this->m_docnum->GetLastDocNum('hWalletTransfer');
			$data['DocDate']=date('m/d/Y');
			
			$this->load->view($this->uri->segment(1).'/view',$data);
		}
	}
	function prosesaddedit()
	{
		$data['crudaccess']=$this->general['crudaccess'];
		switch ($_POST['type']) {
			case "add":
				if ($data['crudaccess']->intCreate==0)
				{
					$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'/';
					echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
				}
				$data['DocNum'] = isset($_POST['DocNum'])?$_POST['DocNum']:''; // get the requested page
				$data['DocDate'] = isset($_POST['DocDate'])?$_POST['DocDate']:''; // get the requested page
				$data['DocDate'] = date('Y-m-d',strtotime($data['DocDate']));
				$data['Type'] = isset($_POST['Type'])?$_POST['Type']:''; // get the requested page
				$data['WalletFrom'] = isset($_POST['WalletFrom'])?$_POST['WalletFrom']:''; // get the requested page
				$data['WalletFromName'] =$this->m_wallet->getByID($data['WalletFrom'])->vcName;
				$data['WalletTo'] = isset($_POST['WalletTo'])?$_POST['WalletTo']:''; // get the requested page
				$data['WalletToName'] =$this->m_wallet->getByID($data['WalletTo'])->vcName;
				$data['Value'] = isset($_POST['Value'])?$_POST['Value']:''; // get the requested page
				$data['Remarks'] = isset($_POST['Remarks'])?$_POST['Remarks']:''; // get the requested page
				
				$this->db->trans_begin();
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'add',$data['DocNum']);
				$cek=$this->m_wallet->insertTransfer($data);
				$cost=$data['Value'];
				$costmin=$data['Value']*-1;
				
				
				$this->m_wallet->addMutation($data['WalletFrom'],$data['DocDate'],'WT',$costmin,$data['DocNum'],'');
				$this->m_wallet->addMutation($data['WalletTo'],$data['DocDate'],'WT',$cost,$data['DocNum'],'');
				
				$this->m_wallet->updateBalance($data['WalletFrom'],$costmin); // change wallet balance
				$this->m_wallet->updateBalance($data['WalletTo'],$cost); // change wallet balance
				if($cek==1)
				{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?success=true&type=successadd';
				}
				else{
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'?error=true&type=erroradd';
				}
				$this->db->trans_complete();
		
				if($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
			
				break;
			
			default:
				$data['link']=$this->config->base_url().'index.php/'.$this->uri->segment(1).'';
				echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
		}
		
	}
}
