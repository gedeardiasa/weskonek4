<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
    {
		session_start();
        parent::__construct();
        $this->load->helper(array('url'));
		$this->load->model('authorization','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_dashboard','',TRUE);
        $this->load->database();
		
    }
	
	public function index()
	{	
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{	
			if(!isset($_SESSION[md5('posroot')]))
			{
				$data['dashboard']=$this->m_dashboard->GetAccessDashboard($_SESSION);
				
				if($data['dashboard']->num_rows()>0)
				{
					$data['show']=1;
				}
				else{
					$data['show']=0;
				}
				$data['form']=$this->authorization->GetForm($_SESSION);
				$data['navigation']=$this->authorization->GetNavigation('user');
			}
			else
			{
				$data['root'] = true;
			}
			
			$this->load->view('dashboard',$data);
		}
	}
	function ceklogin()
	{
		$data['link']=$this->config->base_url().'';
		$cek=$this->authorization->login(str_replace(' ','',$_GET['Username']),$_GET['Password']);
		if($cek==1)
		{
			echo '1';
		}
		else if($cek==2)
		{
			echo '2';
		}
		else
		{
			echo '0';
		}
	}
	function coba()
	{
		$this->load->view('coba');
		
	}
	function coba2()
	{
		echo "Hahahahahaha";
	}
	function continuelogons()
	{
		$this->authorization->continuelogons();
	}
	public function logout()
	{
		$data['link']=$this->config->base_url().'';
		if(!isset($_SESSION[md5('posroot')]))
		{
			$cek=$this->authorization->deleteToken($_SESSION['IDPOS']);
		}
		unset($_SESSION['IDPOS']);
		unset($_SESSION['UsernamePOS']);
		unset($_SESSION['PasswordPOS']);
		unset($_SESSION['NamePOS']);
		unset($_SESSION['EmailPOS']);
		unset($_SESSION['DatePOS']);
		unset($_SESSION['tokenPOS']);
		$xx=md5('db_pos_setting');
		unset($_SESSION[$xx]);
		unset($_SESSION[md5('posroot')]);
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	public function profile()
	{
		if ($this->authorization->ceklogin()==false)
		{
			$data['UserName']='';
			$data['Password']='';
			$this->load->view('login',$data);
		}
		else
		{
			$id=$_SESSION['IDPOS'];
			if(!isset($_SESSION[md5('posroot')]))
			{
				$data['form']=$this->authorization->GetForm($_SESSION);
				$data['navigation']=$this->authorization->GetNavigation('welcome');
				$data['user']=$this->m_user->getByID($id);
			}
			else
			{
				$data['user']=$this->m_user->GetUserRoot();
			}
			$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'view','');
			$this->load->view('profile',$data);
		}
	}
	function proseseditprofile()
	{
		$id=$_SESSION['IDPOS'];
		$data['id'] = $id; // get the requested page
		$data['UserID'] = isset($_POST['UserID'])?$_POST['UserID']:''; // get the requested page
		$data['Email'] = isset($_POST['Email'])?$_POST['Email']:''; // get the requested page
		$data['Name'] = isset($_POST['Name'])?$_POST['Name']:''; // get the requested page
		$data['Phone'] = isset($_POST['Phone'])?$_POST['Phone']:''; // get the requested page
		$data['SetIcon'] = isset($_POST['SetIcon'])?$_POST['SetIcon']:''; // get the requested page
		if(!isset($_SESSION[md5('posroot')]))
		{
			if($_FILES['Image']['tmp_name']!='')
			{
				$data['imgData'] = addslashes(file_get_contents($_FILES['Image']['tmp_name']));
			}
			else
			{
				$data['imgData']=null;
			}
		}
		$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
		$cek=$this->m_user->editProfile($data);
		if($cek==1)
		{
		$data['link']=$this->config->base_url().'index.php/welcome/profile?success_edit=true';
		}
		else{
		$data['link']=$this->config->base_url().'index.php/welcome/profile?error_edit=true';
		}
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function proseseditprofilepass()
	{
		$id=$_SESSION['IDPOS'];
		$data['id'] = $id; // get the requested page
		$data['OldPassword'] = isset($_POST['OldPassword'])?$_POST['OldPassword']:''; // get the requested page
		$data['NewPassword'] = isset($_POST['NewPassword'])?$_POST['NewPassword']:''; // get the requested page
		$data['ConfirmPassword'] = isset($_POST['ConfirmPassword'])?$_POST['ConfirmPassword']:''; // get the requested page
		
		if($data['NewPassword']!=$data['ConfirmPassword'])
		{
			$data['link']=$this->config->base_url().'index.php/welcome/profile?error_confirmpass=true';
		}
		else
		{
			$cek=$this->m_user->cekpass($data);
			if($cek==1)
			{
				$this->authorization->insertactivity($_SESSION,$this->uri->segment(1),$this->uri->segment(2),'edit',$data['id']);
				$this->m_user->editpass($data);
				$data['link']=$this->config->base_url().'index.php/welcome/logout';
			}
			else{
				$data['link']=$this->config->base_url().'index.php/welcome/profile?error_oldpass=true';
			}
		}
		
		echo "<head><meta http-equiv=\"Refresh\" content=\"0; URL=$data[link]\"></head>";exit;
	}
	function getLogonsInformation()
	{
		$log=$this->authorization->getLogonsInformation($_SESSION['IDPOS']);
		echo '
		User '.$log->vcUserID.' is already logged on system<br>
		(Terminal '.$log->vcIP.'-'.$log->vcCompName.', since '.$log->dtInsertTime.')<br>
		Note : Continue this logon will end any other logons on system with same user
		';
	}
	function skiptutorial()
	{
		$this->authorization->skiptutorial();
	}
}
