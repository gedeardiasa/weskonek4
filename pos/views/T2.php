
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/tree/css/jquery.treegrid.css">

<!-- jQuery 3.1.1 -->
<script src="<?php echo base_url(); ?>data/general/plugins/jQuery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/tree/js/jquery.treegrid.js"></script>
<script type="text/javascript">
$(document).ready(function() {
		//$('.tree').treegrid();
		//$('.tree').treegrid('collapseAll');
		$('[data-toggle="tooltip"]').tooltip();   
		
		
});
/*if (document.addEventListener) { // IE >= 9; other browsers
	document.addEventListener('contextmenu', function(e) {
	alert("You've tried to open context menu"); //here you draw your own menu
	e.preventDefault();
	}, false);
} else { // IE < 9
    document.attachEvent('oncontextmenu', function() {
    alert("You've tried to open context menu");
    window.event.returnValue = false;
    });
}*/
</script>

<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>asset/jquery/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>data/general/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!--<script src="<?php echo base_url(); ?>asset/cloudflare/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/morris/morris.min.js"></script> kalau ada error tampilan buka js ini-->
<!-- Sparkline -->
<!--<script src="<?php echo base_url(); ?>data/general/plugins/sparkline/jquery.sparkline.min.js"></script> kalau ada error tampilan buka js ini-->
<!-- jvectormap -->
<!--<script src="<?php echo base_url(); ?>data/general/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> kalau ada error tampilan buka js ini (untuk map dunia)-->
<!-- jQuery Knob Chart -->
<!--<script src="<?php echo base_url(); ?>data/general/plugins/knob/jquery.knob.js"></script> kalau ada error tampilan buka js ini-->
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>asset/cloudflare/moment.min.js"></script>
<script src="<?php echo base_url(); ?>data/general/plugins/daterangepicker/daterangepicker.min.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>data/general/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="<?php echo base_url(); ?>data/general/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>data/general/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>data/general/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>data/general/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?php echo base_url(); ?>data/general/dist/js/pages/dashboard.js"></script> kalau ada error tampilan buka js ini-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>data/general/dist/js/demo.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>data/general/plugins/select2/select2.full.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>asset/respon/datatables.net/js/jquery.dataTables.min.js"></script> <!-- data table untuk tampilan + -->
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-bs/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-fixedheader/js/dataTables.fixedHeader.js"></script>
<script src="<?php echo base_url(); ?>asset/respon/datatables.net-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/buttons.dataTables.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/jquery.dataTables.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>asset/cdn/select.dataTables.css" type="text/css" />


<script type="text/javascript" src="<?php echo base_url(); ?>asset/cdn/jquery.dataTables.min.js"></script> <!-- data table untuk tampilan yang lebih bagus-->
<script src="<?php echo base_url(); ?>asset/cdn/dataTables.buttons.js"></script>
<!--<script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>-->

<!-- Typeahead -->
<script src="<?php echo base_url(); ?>asset/cloudflare/bootstrap3-typeahead.min.js"></script>

<!-- Input Format Number -->
<script src="<?php echo base_url(); ?>asset/mask/jquery.mask.js"></script>
<script>
function unformat_number(val)
{
	var res = val.split(',').join('');
	return parseFloat(res);
}
function addCommas(nStr)
{
	var nStr2=parseFloat(Math.round(nStr * 100) / 100).toFixed(2);
    nStr2 += '';
    x = nStr2.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

</script>
<?php if(!isset($_SESSION[md5('posroot')]))
{?>


<?php 
$tutorialcode='';
$showtorial=1;
$q2=$this->db->query("SELECT 
					a.`intID`, c.`vcName` AS vcFormName, c.`vcCode` AS vcFormCode, c.`intID` AS FormID, a.intSetup
					FROM maccess a
					LEFT JOIN mform c ON a.`intForm`=c.`intID`
					WHERE c.`IsHeader`=0 AND c.`intDeleted`=0 and a.intUserID='$_SESSION[IDPOS]'
					order by c.intSort asc
					");
foreach($q2->result() as $d) 
{
	if($d->intSetup==0 and $showtorial==1)
	{
		$this->load->view('/tutorial/'.$d->vcFormCode.'');
		$tutorialcode=$d->vcFormCode;
		$showtorial=0;
	}
}
//update maccess intSetup to 1
$thisurisegment=$this->uri->segment(1);
$getthismenu=$this->db->query("select intID from mform where vcCode='$thisurisegment'
");

if($getthismenu->num_rows()>0)
{
	$intformmenu=$getthismenu->row()->intID;
	$this->db->query("update maccess set intSetup=1 where intForm='$intformmenu' and intUserID='$_SESSION[IDPOS]'");
}
//end update maccess intSetup to 1
?>
<script type="text/javascript">
$(document).ready(function() {
	$(".loadingbar3").hide();
	$(':button').prop('disabled', false);
	//checkconection();
<?php if($tutorialcode!='' and $tutorialcode!=$thisurisegment){?>
		 $('#modal-tutorial-<?php echo $tutorialcode;?>').modal('show');
<?php } ?>
		
});

function menuloading()
{
	$(".loadingbar3").show();
	$(':button').prop('disabled', true);
	
}
</script>

<?php }
else
{?>
<script type="text/javascript">
$(document).ready(function() {
	$(".loadingbar3").hide();
	$(':button').prop('disabled', false);
		
});

function menuloading()
{
	$(".loadingbar3").show();
	$(':button').prop('disabled', true);
	
}
</script>
<?php
}
?>


