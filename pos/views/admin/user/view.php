<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-addedit">
  <form role="form" method="POST"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/<?php echo $this->uri->segment(2);?>/prosesaddedit">
	<input type="hidden" class="form-control" id="ID" name="ID" maxlength="25" >
	<input type="hidden" class="form-control" id="type" name="type" maxlength="25" >
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" id="tittleAdd">Add New User</h3>
			  <h3 class="box-title" id="tittleEdit">Edit User</h3>
            </div>
            
              <div class="box-body">
			    <div class="form-group">
					<label for="Client">Client</label>
					<input type="hidden" id="Client2" name="Client2">
					<select class="form-control" data-toggle="tooltip" data-placement="top" id="Client" name="Client">
					<?php 
					foreach($listclient->result() as $d) 
					{
					?>	
					<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
					<?php 
					}
					?>
					</select>
				</div>
			    <div class="form-group">
                  <label for="User">User ID</label>
                  <input type="text" class="form-control" id="User" name="User" maxlength="25" required="true" placeholder="Enter User">
				  <input type="hidden" id="LastUser" name="LastUser">
                </div>
				<div class="form-group">
                  <label for="Email">Email</label>
                  <input type="text" class="form-control" id="Email" name="Email" maxlength="25" required="true" placeholder="Enter Email">
				  <input type="hidden" id="LastEmail" name="LastEmail">
                </div>
				<div class="form-group">
                  <label for="Email">Admin User?</label>
                  <input type="checkbox"  name="Admin"  id="Admin" >
				  <input type="hidden" id="LastAdmin" name="LastAdmin">
                </div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="button" class="btn btn-danger" id="deletebutton" data-toggle="modal" data-target="#modal-delete">Delete</button>
		   <button type="submit" class="btn btn-primary" id="addbutton">Save changes</button>
		   <button type="submit" class="btn btn-primary" id="editbutton">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  <div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
		  <form role="form" method="POST"
		  action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/<?php echo $this->uri->segment(2);?>/prosesdelete/">
		  <input type="hidden" class="form-control" id="ID2" name="ID2" maxlength="25" >
          <button type="submit" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <button type="submit" class="btn btn-danger">Delete</button>
		  </form>
         </div>
       </div>
     </div>
  </div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <button type="button" class="btn btn-dark" onclick="add()"  data-toggle="modal" data-target="#modal-addedit"
				  ><span class="glyphicon glyphicon-plus"></span> 
				  <?php echo 'Add New'; ?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
				  <th>User ID </th>
				  <th>Email </th>
				  <th>Client </th>
				  <th>Deleted </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
				  <td class=" "><?php echo $d->vcUserID; ?></td>
				  <td class=" "><?php echo $d->vcEmail; ?></td>
				  <td class=" "><?php echo $d->vcName; ?></td>
				  <td class=" "><?php echo $d->intDeleted; ?></td>
				  <td align="center">
				  <i class="fa fa-search fa-2x" data-toggle="modal" onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcUserID); ?>','<?php echo str_replace("'","\'",$d->vcEmail); ?>','<?php echo str_replace("'","\'",$d->intClient); ?>')" 
				  data-target="#modal-addedit" aria-hidden="true"></i>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	function edit(id,user,email,client)
	{
		
		$("#tittleEdit").show();
		$("#tittleAdd").hide();
		$("#deletebutton").show();
		$("#addbutton").hide();
		$("#editbutton").show();
		document.getElementById("type").value = 'edit';
		document.getElementById("ID").value = id;
		document.getElementById("ID2").value = id;
		document.getElementById("User").value = user;
		document.getElementById("LastUser").value = user;
		document.getElementById("Email").value = email;
		document.getElementById("LastEmail").value = email;
		document.getElementById("Client").value = client;
		document.getElementById("Client2").value = client;
		$('#User').attr("disabled", false);
		$('#Email').attr("disabled", false);
		$('#Client').attr("disabled", true);
		$('#Admin').attr("disabled", true);
		
	}
	function add()
	{
		 $("#tittleAdd").show();
		 $("#tittleEdit").hide();
		 $("#deletebutton").hide();
		 $("#addbutton").show();
		 $("#editbutton").hide();
		 document.getElementById("type").value = 'add';
		 document.getElementById("User").value = '';
		 document.getElementById("Email").value = '';
		 $('#User').attr("disabled", false);
		 $('#Email').attr("disabled", false);
		 $('#Client').attr("disabled", false);
		 $('#Admin').attr("disabled", false);
	}
  $(function () {
	  
	$(".select2").select2();
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
