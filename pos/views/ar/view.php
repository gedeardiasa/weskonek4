<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>

.modal-dialog {
  width: 90%;
 
}
</style>
<div class="wrapper">   
  
  
  <div class="modal fade" id="modal-add-edit">
  <div id="printarea_toolbar">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodaladd">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Add New A/R Invoice</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			<div class="box-header with-border" id="tittlemodaledit">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Detail A/R Invoice</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
              <div class="box-body">
			  <?php $this->load->view('loadingbar2'); ?>
				<div class="row">
				  <div class="alert alert-success alert-dismissible" id="successalert">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i>Success. You successfully process the data
				  </div>
				  <div class="alert alert-danger  alert-dismissible" id="dangeralert">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-ban"></i>Error. You failed to process the data
				  </div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" data-toggle="tooltip" data-placement="top" title="Document Number" readonly="true">
							<input type="hidden" id="idHeader" name="idHeader">
							<input type="hidden" id="IdToolbar" name="IdToolbar">
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">BP Code</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group">
							  <div class="input-group-addon" id="triggermodalbp" data-toggle="modal" data-target="#modal-bp">
								<i class="fa fa-users" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control" id="BPCode" name="BPCode" placeholder="Bussiness Partner Code" data-toggle="tooltip" data-placement="top" title="Bussiness Partner Code" onchange="loadbpcode()" onpaste="loadbpcode()" >
							</div>
							</div>
							
						</div>
						<div class="form-group">
						  <label for="BPName" class="col-sm-4 control-label" style="height:20px">BP Name</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="BPName" name="BPName" placeholder="Bussiness Partner Name" onchange="loadbpname()" onpaste="loadbpname()" data-toggle="tooltip" data-placement="top" title="Bussiness Partner Name">
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" data-toggle="tooltip" data-placement="top" title="Reference Number">
							</div>
						</div>
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Type</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Service" name="Service"class="form-control" data-toggle="tooltip" data-placement="top" title="Service" onchange="changeservice()">
								<option value="0" >Item</option>
								<option value="1" >Service</option>
						    </select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label for="Status" class="col-sm-4 control-label" style="height:20px">Status</label>
							<div class="col-sm-8" style="height:45px">
							<table style="width:100%">
							  <tr>
								<td><input type="text" class="form-control" id="Status" name="Status" readonly="true" data-toggle="tooltip" data-placement="top" title="Status">
								</td>
								<td>
								<span data-toggle="modal" data-target="#modal-closeall">
								<i id="iconcloseheader" 
								data-toggle="tooltip" data-placement="top" title="Close Document" class="fa fa-close fa-2x" aria-hidden="true"></i>
								</span>
								</td>
							  </tr>
							</table>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Del. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DelDate" name="DelDate" data-toggle="tooltip" data-placement="top" title="Delivery Date">
							</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Due. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DueDate" disabled="true" name="DueDate" data-toggle="tooltip" data-placement="top" title="Due Date">
							</div>
							</div>
						</div>
						<div class="form-group">
						  <label for="SalesEmp" class="col-sm-4 control-label" style="height:20px">Sales Employee</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="SalesEmp" name="SalesEmp" placeholder="Sales Employee" data-toggle="tooltip" data-placement="top" title="Sales Employee">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="" id="form-adddetail"  style="padding-bottom:70px">
					<b><i>Form Detail</i></b><br>
					<div class="col-sm-6" style="padding-left:0px;padding-bottom:0px">
						<div class="input-group">
						  <div class="input-group-addon" id="triggermodalitem" data-toggle="modal" data-target="#modal-item">
							<i class="fa fa-cubes" aria-hidden="true"></i>
						  </div>
						  <input type="text" class="form-control inputenter" id="detailItem" name="detailItem" maxlength="100" required="true" 
						  onchange="isiitem()" onpaste="isiitem()" placeholder="Select Item Name" data-toggle="tooltip" data-placement="top" title="Item Name">
						</div>
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						  <input type="text" class="form-control inputenter" id="detailItemCode" name="detailItemCode" maxlength="50" required="true" 
						  onchange="isiitemcode()" onpaste="isiitemcode()" placeholder="Select Code" data-toggle="tooltip" data-placement="top" title="Item Code">
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						  <input type="number" step="any" class="form-control inputenter" id="detailQty" name="detailQty" onchange="isiqty()" 
						  onpaste="isiqty()"maxlength="15" required="true" data-toggle="tooltip" data-placement="top" title="Quantity"
						  placeholder="Qty">
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						<select id="detailUoM" name="detailUoM" class="form-control" data-toggle="tooltip" data-placement="top" title="UoM"
						onchange="loadprice()">
						</select>
						<input type="text" class="form-control inputenter" id="detailUoMS" name="detailUoMS" maxlength="50" required="true" 
						  placeholder="Select UoM" data-toggle="tooltip" data-placement="top" title="UoM">
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						  <input type="number" step="any" class="form-control inputenter" id="detailPrice" name="detailPrice" maxlength="15" required="true" 
						  placeholder="Price" data-toggle="tooltip" data-placement="top" title="Price">
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						  <input type="number" step="any" class="form-control inputenter" id="detailDisc" name="detailDisc" maxlength="15" required="true" 
						  placeholder="Discount" data-toggle="tooltip" data-placement="top" title="Discount">
					</div>
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						  <select id="detailWhs" name="detailWhs"class="form-control" data-toggle="tooltip" data-placement="top" title="Warehouse">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
						  </select>
					</div>
					
					<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
						<button type="button" class="btn btn-primary" id="buttonadddetail" onclick="addDetail()">Add</button>
						<button type="button" class="btn btn-primary" id="buttoneditdetail" onclick="addDetail()">Update</button>
						<button type="button" class="btn btn-default" id="buttoncanceldetail" onclick="cancelDetail()">Cancel</button>
					</div>
				</div>
				<div id="mix_detail"></div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label>Remarks</label>
						  <textarea class="form-control" rows="6" id="Remarks" name="Remarks" placeholder="Remarks ..."
						  data-toggle="tooltip" data-placement="top" title="Remarks"
						  ></textarea>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocTotalBefore" class="col-sm-4 control-label" style="height:20px">Total</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocTotalBefore" name="DocTotalBefore" readonly="true" style="text-align:right;"
							data-toggle="tooltip" data-placement="top" title="Total"
							>
							
							</div>
						</div>
						<div class="form-group">
						  <label for="Disc" class="col-sm-4 control-label" style="height:20px">Disc</label>
							<div class="col-sm-3" style="height:45px">
							<input type="text" class="form-control" id="DiscPer" name="DiscPer" style="text-align:right;" 
							data-toggle="tooltip" data-placement="top" title="Discount %"
							placeholder="%" onchange="loaddisc()" onpaste="loaddisc()" onkeyup="loaddisc()" onclick="loaddisc()">
							</div>
							<div class="col-sm-5" style="height:45px">
							<input type="text" class="form-control" id="Disc" name="Disc" style="text-align:right;" 
							data-toggle="tooltip" data-placement="top" title="Discount"
							>
							</div>
						</div>
						<div class="form-group">
						  <label for="Freight" class="col-sm-4 control-label" style="height:20px">Freight</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="Freight" name="Freight" style="text-align:right;" 
							data-toggle="tooltip" data-placement="top" title="Freight"
							onchange="loadgrandtotal()" onpaste="loadgrandtotal()" onkeyup="loadgrandtotal()" onclick="loadgrandtotal()">
							</div>
						</div>
						<div class="form-group">
						  <label for="Tax" class="col-sm-4 control-label" style="height:20px">Tax</label>
							<div class="col-sm-3" style="height:45px">
							<select class="" id="TaxPer" name="TaxPer" style="width: 100%; height:35px" onchange="loadtax()" data-toggle="tooltip" data-placement="top" title="Tax %">
							<?php 
							foreach($listtax->result() as $d) 
							{
							?>	
							<option value="<?php echo $d->intRate;?>"><?php echo $d->vcName;?></option>
							<?php 
							}
							?>
							</select>
							
							</div>
							<div class="col-sm-5" style="height:45px">
							<input type="text" class="form-control" id="Tax" name="Tax" data-toggle="tooltip" data-placement="top" title="Tax" style="text-align:right;">
							</div>
						</div>
						<div class="form-group">
						  <label for="DocTotal" class="col-sm-4 control-label" style="height:20px">Grand Total</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocTotal" name="DocTotal" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Grand Total">
							</div>
						</div>
						<div class="form-group">
						  <label for="AppliedAmount" class="col-sm-4 control-label" style="height:20px">Applied Amount</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="AppliedAmount" name="AppliedAmount" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Applied Amount">
							</div>
						</div>
						<div class="form-group">
						  <label for="BalanceDue" class="col-sm-4 control-label" style="height:20px">Balance Due</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="BalanceDue" name="BalanceDue" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Balance Due">
							</div>
						</div>
					</div>
				</div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Cancel</button>
		   <div class="btn-group" id="buttoncopyfrom">
                  <button type="button" class="btn btn-default">Copy From</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu dropup" role="menu">
                    <li onclick="copyfrom('SQ')"><a href="#">Sales Quotation</a></li>
					<li onclick="copyfrom('SO')"><a href="#">Sales Order</a></li>
					<li onclick="copyfrom('DN')"><a href="#">Delivery</a></li>
                  </ul>
           </div>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?> id="buttonsave">Save changes</button>
		   <button type="button" class="btn btn-primary" onclick="editHeader()"  <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> id="buttonedit">Edit changes</button>
		 </div>
       </div>
     </div>
   </div>
   </div>
   
   <div class="modal fade" id="modal-closeall">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to close this data?</h2>
         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <button type="button" class="btn btn-danger" onclick="closeall()">Close</button></a>
         </div>
       </div>
     </div>
  </div>
  <?php $this->load->view('selectitem'); ?>
  <?php $this->load->view('selectbp'); ?>
  <?php $this->load->view('selectsq'); ?>
  <?php $this->load->view('selectso'); ?>
  <?php $this->load->view('selectdn'); ?>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<?php $this->load->view('loadingbar2'); ?>
				<div class="col-sm-2" style="padding-left:0px;margin-left:0px">
                  <button type="button" style="width:100%" class="btn btn-dark" data-toggle="modal" data-target="#modal-add-edit" onclick="initialadd()" 
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
				</div>
				<div class="col-sm-7" style="padding-left:0px;margin-left:0px">
				&nbsp;
				</div>
				<div class="col-sm-3" style="padding-left:0px;margin-right:0px">
				<table align="center" style="width:100%">
					<tr>
						<td><input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff"></td>
						<td style="padding-left:5px" align="right"><button type="submit" class="btn btn-primary" onclick="loaddetailheader()">Reload</button>
						</td>
					</tr>
				</table>
				</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<div id="mix_header"></div>
				<div class="loader"></div>
			  
			  
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
  <?php $this->load->view('localdata/bp'); ?>
  <?php $this->load->view('localdata/item'); ?>
<script>

  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode == 13) //shortcut enter pada detail data untuk menambah data detail
	{
        addDetail();
    }
  });
  
  var globalDocTotalBefore;
  
  /*
  
	FUNGSI COPY FROM
  
  */
  function copyfrom(val)
  {
	  if(val=='SQ')
	  {
		  $('#modal-sq').modal('show');
	  }
	  else if(val=='SO')
	  {
		  $('#modal-so').modal('show');
	  }
	  else if(val=='DN')
	  {
		  $('#modal-dn').modal('show');
	  }
  }
  function insertSQ(id) //fungsi saat pilih item dengan modal
  {
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetailsq?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				$('#modal-sq').modal('hide');
				loadheadersq(id);
			}
		});
	  
  }
  function insertSO(id) //fungsi saat pilih item dengan modal
  {
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetailso?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				$('#modal-so').modal('hide');
				loadheaderso(id);
				
			}
		});
	  
  }
  function insertDN(id) //fungsi saat pilih item dengan modal
  {
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetaildn?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				$('#modal-dn').modal('hide');
				loadheaderdn(id);
				
			}
		});
	  
  }
  function loadheadersq(id)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderSQ?", 
			data: "id="+id+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				document.getElementById("BPCode").value = obj.BPCode;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("RefNum").value = obj.RefNum;
				document.getElementById("Status").value = obj.Status;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				$( "#DelDate" ).datepicker( "setDate", obj.DelDate);
				document.getElementById("SalesEmp").value = obj.SalesEmp;
				document.getElementById("Remarks").value = obj.Remarks;
				document.getElementById("Service").value = obj.Service;
				$('#Service').attr("disabled", true);
				
				loadtotal();
				document.getElementById("DiscPer").value = addCommas(obj.DiscPer);
				document.getElementById("Freight").value = addCommas(obj.Freight);
				document.getElementById("TaxPer").value = obj.TaxPer;
				loadtotal();
				changeservice();
			},
			async: false
		});
  }
  function loadheaderso(id)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderSO?", 
			data: "id="+id+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				document.getElementById("BPCode").value = obj.BPCode;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("RefNum").value = obj.RefNum;
				document.getElementById("Status").value = obj.Status;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				$( "#DelDate" ).datepicker( "setDate", obj.DelDate);
				document.getElementById("SalesEmp").value = obj.SalesEmp;
				document.getElementById("Remarks").value = obj.Remarks;
				document.getElementById("Service").value = obj.Service;
				$('#Service').attr("disabled", true);
				
				loadtotal();
				document.getElementById("DiscPer").value = addCommas(obj.DiscPer);
				document.getElementById("Freight").value = addCommas(obj.Freight);
				document.getElementById("TaxPer").value = obj.TaxPer;
				loadtotal();
				changeservice();
			},
			async: false
		});
  }
  function loadheaderdn(id)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderDN?", 
			data: "id="+id+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				document.getElementById("BPCode").value = obj.BPCode;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("RefNum").value = obj.RefNum;
				document.getElementById("Status").value = obj.Status;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				$( "#DelDate" ).datepicker( "setDate", obj.DelDate);
				document.getElementById("SalesEmp").value = obj.SalesEmp;
				document.getElementById("Remarks").value = obj.Remarks;
				document.getElementById("Service").value = 0;
				$('#Service').attr("disabled", true);
				
				loadtotal();
				document.getElementById("DiscPer").value = addCommas(obj.DiscPer);
				document.getElementById("Freight").value = addCommas(obj.Freight);
				document.getElementById("TaxPer").value = obj.TaxPer;
				loadtotal();
				changeservice();
			},
			async: false
		});
  }
  /*
  
	FUNGSI LOAD
  
  */
  function loadbpcode() //untuk reload BP saat Code diketik
  {
	  var BPCode = $("#BPCode").val();
		for(var i=0;i<=totalbp;i++)
		{
			if(allbp_BPCode[i]==BPCode)
			{
				document.getElementById("BPName").value = allbp_BPName[i];
			}
		}
	  loadprice();
	  loadtaxgroup();
  }
  function loadbpname() //untuk reload BP saat Name diketik
  {
	  var BPName = $("#BPName").val();
	  for(var i=0;i<=totalbp;i++)
		{
			if(allbp_BPName[i]==BPName)
			{
				document.getElementById("BPCode").value = allbp_BPCode[i];
			}
		}
	  loadprice();
	  loadtaxgroup();
	  
  }
  function loadtotal() //untuk reload total sblm discount,tax, dan freight
  {
	  $.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/loadtotal", 
		data: "",  
		cache: false, 
		success: function(data){ 
			document.getElementById("DocTotalBefore").value = addCommas(data);
			globalDocTotalBefore=data;
			loadgrandtotal();
			loaddisc();
		} 
	}); 
  }
  function loaddisc() //untuk reload disc
  {
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  var dp = $("#DiscPer").val();
	  if(dp=='')
	  {
		  var DiscPer=0;
	  }
	  else
	  {
		var DiscPer = unformat_number(dp);
	  }
	  
	  document.getElementById("Disc").value = addCommas(DiscPer/100*DocTotalBefore);
	  loadgrandtotal();
	  loadtax();
  }
  function loadtax() //untuk reload tax
  {
	 
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  var Disc = unformat_number($("#Disc").val());
	  var Fr=$("#Freight").val();
	  if(Fr=='')
	  {
		  var Freight=0;
	  }
	  else
	  {
		  var Freight = unformat_number(Fr);
	  }
	  
	  var TaxPer = $("#TaxPer").val();
	  document.getElementById("Tax").value = addCommas(TaxPer/100*(DocTotalBefore-Disc));
	  loadgrandtotal();
  }
  function loadgrandtotal() //untuk reload grand total
  {
	  var Disc = unformat_number($("#Disc").val());
	  var Fr=$("#Freight").val();
	  if(Fr=='')
	  {
		  var Freight=0;
	  }
	  else
	  {
		  var Freight = unformat_number(Fr);
	  }
	  var Tax = unformat_number($("#Tax").val());
	  var DocTotalBefore=unformat_number(globalDocTotalBefore);
	  
	  
	  document.getElementById("DocTotal").value = addCommas(DocTotalBefore-Disc+Freight+Tax);
  }
  function loadtaxgroup()
  {
	  var BPCode = $("#BPCode").val();
	  for(var i=0;i<=totaltax;i++)
		{
			if(alltax_BPCode[i]==BPCode)
			{
				document.getElementById("TaxPer").value = alltax_Rate[i];
			}
		}
		loadtotal();
  }
  function loadprice() //untuk reload harga per item
  {
	   $(".loadingbar2").show();
	   $(':button').prop('disabled', true);
	   var BPCode = $("#BPCode").val();
	   var detailItem = $("#detailItem").val();
	   var detailUoM = $("#detailUoM").val();
	   $.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/loadprice", 
		data: "BPCode="+BPCode+"&detailItem="+detailItem+"&detailUoM="+detailUoM+"",
		cache: false, 
		success: function(data){ 
			//alert(data);
			document.getElementById("detailPrice").value = data;
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		} 
	  }); 
  }
  function loadheader(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				document.getElementById("BPCode").value = obj.BPCode;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("RefNum").value = obj.RefNum;
				document.getElementById("Status").value = obj.Status;
				document.getElementById("Service").value = obj.Service;
				if(obj.StatusCode=='O') //jika status Open maka sebagian header masih bisa dirubah
				{
					//$("#form-adddetail").show();
					//$("#buttonadddetail").show();
					//$("#buttoneditdetail").hide();
					//$("#buttoncanceldetail").hide();
					$('#RefNum').attr("disabled", false);
					//$('#DocDate').attr("disabled", false);
					$('#DelDate').attr("disabled", false);
					$('#SalesEmp').attr("disabled", false);
					$('#Remarks').attr("disabled", false);
					//$("#iconcloseheader").show();
					$("#iconcloseheader").hide();
					
				}
				else
				{
					$("#iconcloseheader").hide();
					$("#buttonedit").hide();
				}
				if(obj.idHeader==0 || dup==1)
				{
					$("#iconcloseheader").hide();
				}
				
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				$( "#DelDate" ).datepicker( "setDate", obj.DelDate);
				$( "#DueDate" ).datepicker( "setDate", obj.DueDate);
				document.getElementById("SalesEmp").value = obj.SalesEmp;
				document.getElementById("Remarks").value = obj.Remarks;
				
				document.getElementById("DocTotalBefore").value = addCommas(obj.DocTotalBefore);
				document.getElementById("DiscPer").value = addCommas(obj.DiscPer);
				document.getElementById("Disc").value = addCommas(obj.Disc);
				document.getElementById("Freight").value = addCommas(obj.Freight);
				document.getElementById("TaxPer").value = obj.TaxPer;
				document.getElementById("Tax").value = addCommas(obj.Tax);
				document.getElementById("DocTotal").value = addCommas(obj.DocTotal);
				document.getElementById("AppliedAmount").value = addCommas(obj.AppliedAmount);
				document.getElementById("BalanceDue").value = addCommas(obj.BalanceDue);
			},
			async: false
		});
		changeservice();
		
  }
  function loaddetailheader()
  {
	  $(".loadingbar2").show();
	  var daterange = $("#daterange").val();
	  daterange = daterange.split(' ').join('');
	  $('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'', function(){
		$(".loadingbar2").hide();
	  });   
  }
  function loadUoM() // untuk reload UoM
  {
	  document.getElementById('detailUoM').innerHTML = "";
	  var detailItem = $("#detailItem").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loadUoM?", 
			data: "detailItem="+detailItem+"&type=sls",
			cache: true, 
			success: function(data){
				var select = document.getElementById("detailUoM");
				var opt = document.createElement('option');
				opt.value = "2";
				opt.innerHTML = data;
				select.appendChild(opt);
			},
			async: false
	  });
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loadUoM?", 
			data: "detailItem="+detailItem+"&type=inv",
			cache: true, 
			success: function(data){
				var select = document.getElementById("detailUoM");
				var opt = document.createElement('option');
				opt.value = "1";
				opt.innerHTML = data;
				select.appendChild(opt);
			},
			async: false
	  });
  }
  function isiqty() // fungsi saat field qty diketik
  {
	  
  }
  function isiitem() // fungsi saat ketik field item
  {
	   var detailItem = $("#detailItem").val();
	  
		for(var i=0;i<=totaluom;i++)
		{
			if(alluom_detailItem[i]==detailItem)
			{
				document.getElementById("detailItemCode").value = alluom_detailCode[i];
			}
		}
	    loadUoM();
		loadprice();
  }
  function isiitemcode() // fungsi saat ketik field item
  {
	  var detailItemCode = $("#detailItemCode").val();
	  
	  for(var i=0;i<=totaluom;i++)
		{
			if(alluom_detailCode[i]==detailItemCode)
			{
				document.getElementById("detailItem").value = alluom_detailItem[i];
			}
		}
	  loadUoM();
	  loadprice();
  }
  function cancelselectitem() //fungsi saat modal item di close
  {
	  
  }
  function cancelselectbp() //fungsi saat modal item di close
  {
	  
  }
  function changeservice() //fungsi saat ganti service
  {
	  var Service = $("#Service").val();
	  if(Service==1)
	  {
		 $("#detailUoM").hide(); 
		 $("#detailUoMS").show(); 
	  }
	  else if(Service==0)
	  {
		 $("#detailUoM").show(); 
		 $("#detailUoMS").hide(); 
	  }
  }
  function initialadd() // fungsi saat tombol add di klik
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttoncopyfrom").show();
	  $("#buttonsave").show();
	  $("#buttonedit").hide();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#DocNum').attr("disabled", false);
	  $('#BPCode').attr("disabled", false);
	  $('#triggermodalbp').attr("data-toggle", "modal");
	  $('#BPName').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DelDate').attr("disabled", false);
	  $('#SalesEmp').attr("disabled", false);
	  $('#Remarks').attr("disabled", false);
	  $('#Service').attr("disabled", false);
	  
	  $('#DocTotalBefore').attr("disabled", false);
	  $('#DiscPer').attr("disabled", false);
	  $('#Disc').attr("disabled", true);
	  $('#Freight').attr("disabled", false);
	  $('#TaxPer').attr("disabled", false);
	  $('#Tax').attr("disabled", true);
	  $('#DocTotal').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			  
				//load ambil data header
				loadheader(0,0);
			}
		});
	
  }
  function initialedit(id) //fungsi saat klik edit header
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").hide();
	  $("#buttoncopyfrom").hide();
	  $("#buttonsave").hide();
	  $("#buttonedit").show();
	  $("#tittlemodaladd").hide();
	  $("#tittlemodaledit").show();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $('#DocNum').attr("disabled", true);
	  $('#BPCode').attr("disabled", true);
	  $('#triggermodalbp').removeAttr("data-toggle");
	  $('#BPName').attr("disabled", true);
	  $('#RefNum').attr("disabled", true);
	  $('#DocDate').attr("disabled", true);
	  $('#DelDate').attr("disabled", true);
	  $('#SalesEmp').attr("disabled", true);
	  $('#Remarks').attr("disabled", true);
	  $('#Service').attr("disabled", true);
	  
	  $('#DocTotalBefore').attr("disabled", true);
	  $('#DiscPer').attr("disabled", true);
	  $('#Disc').attr("disabled", true);
	  $('#Freight').attr("disabled", true);
	  $('#TaxPer').attr("disabled", true);
	  $('#Tax').attr("disabled", true);
	  $('#DocTotal').attr("disabled", true);
	  
	  //load detail ambil dari database dan masukkan ke session
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
			  
				//load ambil data header
				loadheader(id,0);
				<?php if($crudaccess->intUpdate==0) { ?>// jika tidak punya access edit disable semua form
					$("#form-adddetail").hide();
					$("#controldetail").hide();
					
					$('#RefNum').attr("disabled", true);
					$('#DocDate').attr("disabled", true);
					$('#DelDate').attr("disabled", true);
					$('#SalesEmp').attr("disabled", true);
					$('#Remarks').attr("disabled", true);
					//load html untuk menampilkan detail dari session
					$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');
					
				<?php }else{ ?>
					//load html untuk menampilkan detail dari session
					$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');
				<?php } ?>
			}
		});
  }
  function initialduplicate(id) // fungsi saat tombol add di klik
  {
	  emptyformdetail();
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttoncopyfrom").show();
	  $("#buttonsave").show();
	  $("#buttonedit").hide();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#DocNum').attr("disabled", false);
	  $('#BPCode').attr("disabled", false);
	  $('#triggermodalbp').attr("data-toggle", "modal");
	  $('#BPName').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#DelDate').attr("disabled", false);
	  $('#SalesEmp').attr("disabled", false);
	  $('#Remarks').attr("disabled", false);
	  $('#Service').attr("disabled", true);
	  
	  $('#DocTotalBefore').attr("disabled", false);
	  $('#DiscPer').attr("disabled", false);
	  $('#Disc').attr("disabled", true);
	  $('#Freight').attr("disabled", false);
	  $('#TaxPer').attr("disabled", false);
	  $('#Tax').attr("disabled", true);
	  $('#DocTotal').attr("disabled", false);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"&duplicated=true", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			  
				//load ambil data header
				loadheader(id,1);
			}
		});
	
  }
  
  
  /*
  
	HEADER FUNCTION
  
  */
  function closeall(bpcode) //fungsi saat pilih item dengan modal
  {
	  var idHeader = $("#idHeader").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/closeall?", 
			data: "idHeader="+idHeader+"", 
			cache: true, 
			success: function(data){ 
				$('#modal-closeall').modal('hide');
				initialedit(idHeader);
			},
			async: false
	  });
		
  }
  function insertBP(bpcode) //fungsi saat pilih item dengan modal
  {
	  document.getElementById("BPCode").value = bpcode;
	  $('#modal-bp').modal('hide');
	  //$("#RefNum").focus();
	  loadbpcode();
	  //loadprice();
  }
  function addHeader() // proses submit add
  {
		$(".loadertransaction").show();
		var DocNum = $("#DocNum").val();
		var BPCode = $("#BPCode").val();
		var BPName = $("#BPName").val();
		var RefNum = $("#RefNum").val();
		var DocDate = $("#DocDate").val();
		var DelDate = $("#DelDate").val();
		var SalesEmp = $("#SalesEmp").val();
		var Remarks = $("#Remarks").val();
		var Service = $("#Service").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var DiscPer = unformat_number($("#DiscPer").val());
		var Disc = unformat_number($("#Disc").val());
		var Freight = unformat_number($("#Freight").val());
		var TaxPer = unformat_number($("#TaxPer").val());
		var Tax = unformat_number($("#Tax").val());
		var DocTotal = unformat_number($("#DocTotal").val());
		
		var detail=0;
		var detailstock=0;
		var differentplan=0;
		var bp=0;
		
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetailstok?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detailstock=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdifferentplan?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				differentplan=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekbp?", 
			data: "BPCode="+BPCode+"", 
			cache: true, 
			success: function(data){ 
				bp=data;
			},
			async: false
		});
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});
		
		if(DocNum=='')
		{
			alert('Please Fill Doc. Number');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(BPCode=='')
		{
			alert('Please Fill Bussiness Partner');
			$("#BPCode").focus();
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(period!=1)
		{
			alert('This Period is locked');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(DelDate=='')
		{
			alert('Please Fill Delivery Date');
			$("#DelDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else if(detailstock!=1 && Service=='0')
		{
			alert(detailstock);
			$(".loadertransaction").hide();
		}
		else if(differentplan!=1)
		{
			alert('You cannot create transaction with different plan. Check your location data');
			//alert(differentplan);
			$(".loadertransaction").hide();
		}
		else if(bp==0)
		{
			alert('Bussiness Partner Not Found');
			$("#BPCode").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd", 
				data: "DocNum="+DocNum+"&BPCode="+BPCode+"&BPName="+BPName+"&RefNum="+RefNum+
				"&DocDate="+DocDate+"&DelDate="+DelDate+"&SalesEmp="+SalesEmp+
				"&DocTotalBefore="+DocTotalBefore+"&DiscPer="+DiscPer+"&Disc="+Disc+"&Freight="+Freight+
				"&TaxPer="+TaxPer+"&Tax="+Tax+"&DocTotal="+DocTotal+"&Remarks="+Remarks+"&Service="+Service+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						initialadd();
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  function editHeader() // proses submit edit
  {
		$(".loadertransaction").show();
		var idHeader = $("#idHeader").val();
		var DocNum = $("#DocNum").val();
		var RefNum = $("#RefNum").val();
		var DocDate = $("#DocDate").val();
		var DelDate = $("#DelDate").val();
		var SalesEmp = $("#SalesEmp").val();
		var Remarks = $("#Remarks").val();
		var Service = $("#Service").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var DiscPer = unformat_number($("#DiscPer").val());
		var Disc = unformat_number($("#Disc").val());
		var Freight = unformat_number($("#Freight").val());
		var TaxPer = unformat_number($("#TaxPer").val());
		var Tax = unformat_number($("#Tax").val());
		var DocTotal = unformat_number($("#DocTotal").val());
		
		var detail=0;
		var bp=0;
		
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(DelDate=='')
		{
			alert('Please Fill Delivery Date');
			$("#DelDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit", 
				data: "idHeader="+idHeader+"&RefNum="+RefNum+"&DocNum="+DocNum+
				"&DocDate="+DocDate+"&DelDate="+DelDate+"&SalesEmp="+SalesEmp+
				"&DocTotalBefore="+DocTotalBefore+"&DiscPer="+DiscPer+"&Disc="+Disc+"&Freight="+Freight+
				"&TaxPer="+TaxPer+"&Tax="+Tax+"&DocTotal="+DocTotal+"&Remarks="+Remarks+"&Service="+Service+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						initialedit(idHeader);
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  
  
  /*
  
	DETAIL FUNCTION
  
  */
  function insertItem(item) //fungsi saat pilih item dengan modal
  {
	  var detailItem = $("#detailItem").val();
	  if($("#detailQty").val()=='' || detailItem!=item)
	  {
		var detailQty=0;
	  }
	  else
	  {
		var detailQty=$("#detailQty").val();
	  }
	  document.getElementById("detailQty").value = parseInt(detailQty)+1;
	  document.getElementById("detailItem").value = item;
	  $('#modal-item').modal('hide');
	  $("#detailQty").focus();
	  document.getElementById('detailQty').select();
	  isiitem();
  }
  function emptyformdetail()
  {
	document.getElementById("detailItem").value = "";
	document.getElementById("detailItemCode").value = "";
	document.getElementById("detailQty").value = "";
	document.getElementById("detailUoM").value = "";
	document.getElementById("detailUoMS").value = "";
	document.getElementById("detailPrice").value = "";
	document.getElementById("detailDisc").value = "";
	$("#detailWhs").val(<?php echo $defaultwhs;?>);
  }
  function EnableDisableItem(val)
  {
	  $('#detailItem').attr("disabled", val);
	  $('#detailItemCode').attr("disabled", val);
  }
  function addDetail() //proses add detail
  {
		$(".loadertransaction").show();
		EnableDisableItem(false);
	    var detailItem = $("#detailItem").val();
		var detailQty = $("#detailQty").val();
		var detailUoM = $("#detailUoM").val();
		var detailUoMS = $("#detailUoMS").val();
		var detailPrice = $("#detailPrice").val();
		var detailDisc = $("#detailDisc").val();
		var detailWhs = $("#detailWhs").val();
		var Service = $("#Service").val();
		
		if(detailItem=='')
		{
			alert('Please Fill Item Name');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else if(detailQty=='' || detailQty==0)
		{
			alert('Please Fill Quantity');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		else if(detailPrice=='')
		{
			alert('Please Fill Price');
			$("#detailPrice").focus();
			$(".loadertransaction").hide();
		}
		else if(detailWhs==null || detailWhs=='' || detailWhs=='0' || detailWhs==0)
		{
			alert('Please Fill Location');
			$("#detailWhs").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/addDetail", 
				data: "detailItem="+detailItem+"&detailQty="+detailQty+"&detailUoM="+detailUoM+"&detailUoMS="+detailUoMS+"&detailPrice="+detailPrice+"&detailDisc="+detailDisc+"&detailWhs="+detailWhs+"&Service="+Service+"",  
				cache: false, 
				success: function(msg){ 
					if(msg!='false')
					{
						$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
						document.getElementById("detailItem").value = "";
						$('#triggermodalitem').attr("data-toggle", "modal");
						document.getElementById("detailItemCode").value = "";
						document.getElementById("detailQty").value = "";
						document.getElementById("detailUoM").value = "";
						document.getElementById("detailUoMS").value = "";
						document.getElementById("detailPrice").value = "";
						document.getElementById("detailDisc").value = "";
						$("#detailWhs").val(<?php echo $defaultwhs;?>);
						$("#buttonadddetail").show();
						$("#buttoneditdetail").hide();
						$("#buttoncanceldetail").hide();
						document.getElementById("detailItem").focus();
						loadtotal();
					}
					else
					{
						alert('Error');
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
		exit();
  }
  function delDetail(code, name, qty) // proses hapus detail
  {
	EnableDisableItem(false);
	var Service = $("#Service").val();
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/delDetail", 
		data: "code="+code+"&name="+name+"&qty="+qty+"&Service="+Service+"",  
		cache: false, 
		success: function(msg){ 
			$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
			document.getElementById("detailItem").value = "";
			$('#triggermodalitem').attr("data-toggle", "modal");
			document.getElementById("detailItemCode").value = "";
			document.getElementById("detailQty").value = "";
			document.getElementById("detailUoM").value = "";
			document.getElementById("detailUoMS").value = "";
			document.getElementById("detailPrice").value = "";
			document.getElementById("detailDisc").value = "";
			$("#detailWhs").val(<?php echo $defaultwhs;?>);
			$("#buttonadddetail").show();
			$("#buttoneditdetail").hide();
			$("#buttoncanceldetail").hide();
			loadtotal();
			$("#form-adddetail").focus();
		} 
	}); 
	
  }
  function editDetail(item, code, qty, uom, uoms, price, disc, whs) // fungsi saat klik edit detail
  {
		document.getElementById("detailItem").value = item;
		$('#triggermodalitem').removeAttr("data-toggle");
		document.getElementById("detailItemCode").value = code;
		loadUoM();
		document.getElementById("detailQty").value = qty;
		document.getElementById("detailUoM").value = uom;
		document.getElementById("detailUoMS").value = uoms;
		document.getElementById("detailPrice").value = price;
		document.getElementById("detailDisc").value = disc;
		document.getElementById("detailWhs").value = whs;
		$("#buttonadddetail").hide();
		$("#buttoneditdetail").show();
		$("#buttoncanceldetail").show();
		$("#detailQty").focus();
		EnableDisableItem(true);
  }
  function cancelDetail()// fungsi saat klik cancel edit detail
  {
		$("#detailWhs").val(<?php echo $defaultwhs;?>);
		document.getElementById("detailItem").value = "";
		$('#triggermodalitem').attr("data-toggle", "modal");
		document.getElementById("detailItemCode").value = "";
		document.getElementById("detailQty").value = "";
		document.getElementById("detailUoM").value = "";
		document.getElementById("detailUoMS").value = "";
		document.getElementById('detailUoM').innerHTML = "";
		document.getElementById("detailPrice").value = "";
		document.getElementById("detailDisc").value = "";
		
		$("#buttonadddetail").show();
		$("#buttoneditdetail").hide();
		$("#buttoncanceldetail").hide();
		EnableDisableItem(false);
  }
  
  function formatnumber()
  {  
	/*$('#DocTotalBefore').maskMoney({thousands:',', decimal:'.', allowZero:true});
	$('#DiscPer').maskMoney({thousands:',', decimal:'.', allowZero:true});
	$('#Disc').maskMoney({thousands:',', decimal:'.', allowZero:true});
	$('#Freight').maskMoney({thousands:',', decimal:'.', allowZero:true});
	//$('#TaxPer').maskMoney({thousands:',', decimal:'.', allowZero:true});
	$('#Tax').maskMoney({thousands:',', decimal:'.', allowZero:true});
	$('#DocTotal').maskMoney({thousands:',', decimal:'.', allowZero:true});*/
	
	$('#DocTotalBefore').mask("#,##0.00", {reverse: true});
	$('#DiscPer').mask("#,##0.00", {reverse: true});
	$('#Disc').mask("#,##0.00", {reverse: true});
	$('#Freight').mask("#,##0.00", {reverse: true});
	//$('#TaxPer').mask("#,##0.00", {reverse: true});
	$('#Tax').mask("#,##0.00", {reverse: true});
	$('#DocTotal').mask("#.##0,00", {reverse: true});
  }
  $(function () {
	  
	$(".loadertransaction").hide();
	$(document).bind('keydown', function(e) {// funsgsi shortcut
		if($('#modal-add-edit').hasClass('in')==true)
		{
			  if(e.ctrlKey && (e.which == 83)) { // Ctrl + s
				e.preventDefault();
				if($('#buttonsave').is(":visible")==true)
				{					
					addHeader();
				}
				else if($('#buttonedit').is(":visible")==true)
				{					
					editHeader();
				}
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 39)) { // Ctrl + right
				e.preventDefault();
				toolbar_next();
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 37)) { // Ctrl + left
				e.preventDefault();
				toolbar_previous();
				return false;
			  }
			  else if(e.which == 27) { // Esc
				e.preventDefault();
				$('#modal-add-edit').modal('hide');
				return false;
			  }
		}
		else
		{
			if(e.which == 78) {
				//$('#modal-add-edit').modal('show');
				//initialadd();
			}
		}
	});
	  
	formatnumber();//saat load awal buat input angka menjadi number format 
	changeservice();
	
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	document.getElementById("daterange").value = lastWeekT1+' - '+todayT1;
	
	//fungsi khusus multiple modal agar saat modal-2 di close modal-1 tetap bisa scroll
	$('.modal').on('hidden.bs.modal', function (e) {
		if($('.modal').hasClass('in')) {
		$('body').addClass('modal-open');
		}    
	});
	//end 
	
	//ubah inputan ke format datepiceker
	$('#DocDate').datepicker({
      autoclose: true
    });
	$('#DelDate').datepicker({
      autoclose: true
    });
	//end
	
	
	//fungsi load ajax
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'');
	//end
	
	//fungsi typeahead
	$('#detailItem').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#detailItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPCode').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPName').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	//end
	$("#example3").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
	
	$("#exampleSQ").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
	$("#exampleSO").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
	
	$("#exampleDN").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
	
  });
  
 
</script>
</body>
</html>
