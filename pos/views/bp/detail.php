<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
  


    <script src="<?php echo base_url(); ?>asset/yandex/highlight.min.js"></script>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
<div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesdelete/<?php echo $bp->intID; ?>">
		  <button type="button" <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?> class="btn btn-danger">Delete</button></a>
         </div>
       </div>
     </div>
</div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>


    <!-- Main content -->
    <section class="content">
	<div class="row">
        <div class="col-md-3">
		<!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" width="25px" height="25px" class="profile-user-img img-responsive img-circle" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $bp->vcName;?></h3>

              <p class="text-muted text-center"><?php echo $bp->vcCode;?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Account Balance</b> <a class="pull-right"><?php echo number_format($bp->intBalance,'2');?></a>
                </li>
                <li class="list-group-item">
                  <b>Deliveries</b> <a class="pull-right"><?php echo number_format($bp->intDelivery,'2');?></a>
                </li>
                <li class="list-group-item">
                  <b>Orders</b> <a class="pull-right"><?php echo number_format($bp->intOrder,'2');?></a>
                </li>
              </ul>
			
              <button data-toggle="modal" data-target="#modal-delete" class="btn btn-danger btn-block"
			  <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?>><b>Delete</b></button>
            </div>
          </div>
		
		</div>
		<form role="form" method="POST" enctype="multipart/form-data"
		action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit/<?php echo $bp->intID;?>">
		<div class="col-md-9">
          <div class="nav-tabs-custom">
		  
            <ul class="nav nav-tabs">
              <li class="active"><a href="#general" data-toggle="tab">General Data</a></li>
              <li><a href="#contact" data-toggle="tab">Contact</a></li>
			  <li><a href="#address" data-toggle="tab">Address</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="general">
                <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
					<div class="col-md-6">
						<div class="form-group">
							<label for="Code">Code</label>
							<div class="row">
							<div class="col-md-8">
							<input type="text" class="form-control" id="Code" name="Code" maxlength="25" readonly="true" value="<?php echo $bp->vcCode;?>" autofocus>
							</div>
							<div class="col-md-4">
							<select class="" id="Type" name="Type" style="width: 100%; height:35px" onchange="changetype()">
								<option value="C" <?php if($bp->vcType=='C'){ echo "selected";}?>>Customer</option>
								<option value="S" <?php if($bp->vcType=='S'){ echo "selected";}?>>Vendor</option>
								<option value="X" <?php if($bp->vcType=='X'){ echo "selected";}?>>Both</option>
							</select>
							</div>
							</div>
						</div>
						<div class="form-group">
							<label for="Name">Name</label>
							<input type="text" class="form-control" id="Name" name="Name" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> maxlength="75" required="true" value="<?php echo $bp->vcName;?>" >
						</div>
						<div class="form-group">
							<label for="TaxNumber">Federal Tax ID</label>
							<input type="text" class="form-control" id="TaxNumber" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="TaxNumber" maxlength="75" value="<?php echo $bp->vcTaxNum;?>" >
						</div>
						<div class="form-group">
							<label for="PaymentTerm">Payment Term</label>
							<div class="row">
								<div class="col-md-6">
								<input type="number" class="form-control" id="PaymentTerm" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="PaymentTerm" value="<?php echo $bp->intPaymentTerm;?>" maxlength="5">
								</div>
								<div class="col-md-6">
								Day After Invoice Created
								</div>
							</div>
						</div>
								
					</div>
					<div class="col-md-6">
						<div class="form-group">
						<label for="Category">Category</label>
						<div id="listcategory"></div>
						</div>
						<div class="form-group">
						<label for="PriceList">Price List</label>
						<select class="" id="PriceList" name="PriceList" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> style="width: 100%; height:35px">
						<?php 
						foreach($listpricelist->result() as $d) 
						{
						?>	
						<option value="<?php echo $d->intID;?>" <?php if($d->intID==$bp->intPriceList) { echo "selected";}?>><?php echo $d->vcName;?></option>
						<?php 
						}
						?>
						</select>
						</div>
						<div class="form-group">
						<label for="CreditLimit">Credit Limit</label>
						<input type="number" class="form-control" id="CreditLimit" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CreditLimit" maxlength="20" value="<?php echo $bp->intCreditLimit;?>" >
						</div>
						<div class="form-group">
						  <label for="Tax">Tax Group</label>
							<select class="" id="Tax" name="Tax" style="width: 100%; height:35px">
							<?php 
							foreach($listtax->result() as $d) 
							{
							?>	
							<option value="<?php echo $d->intID;?>" <?php if($bp->intTax==$d->intID){ echo "selected";}?>><?php echo $d->vcName;?></option>
							<?php 
							}
							?>
							</select>
						</div>
					</div>
                </div>
              </div>
			  <div class="tab-pane" id="contact">
                <div class="box-body">
					<div class="col-md-6">
						<div class="form-group">
							<label for="Phone1">Phone 1</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="Phone1" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="Phone1" maxlength="50" value="<?php echo $bp->vcTelp1;?>">
							</div>
						</div>
						<div class="form-group">
							<label for="Phone2">Phone 2</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="Phone2" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="Phone2" maxlength="50" value="<?php echo $bp->vcTelp2;?>">
							</div>
						</div>
						<div class="form-group">
							<label for="Fax">Fax</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-fax" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="Fax" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="Fax" maxlength="50" value="<?php echo $bp->vcFax;?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="Phone1">Email</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							<input type="text" class="form-control" id="Email" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="Email" maxlength="100" value="<?php echo $bp->vcEmail;?>">
							</div>
						</div>
						<div class="form-group">
							<label for="Website">Website</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="Website" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="Website" maxlength="100" value="<?php echo $bp->vcWebsite;?>">
							</div>
						</div>
						<div class="form-group">
							<label for="ContactPerson">Contact Person</label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="ContactPerson" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="ContactPerson" maxlength="100" value="<?php echo $bp->vcContactPerson;?>">
							</div>
						</div>
					</div>
				</div>
			  </div>
			  <div class="tab-pane" id="address">
                <div class="box-body">
					<div class="col-md-6">
						<div class="form-group">
						<label for="CityBillTo">City (Bill To)</label>
						<input type="text" class="form-control" id="CityBillTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CityBillTo" maxlength="50" value="<?php echo $bp->vcBillToCity;?>">
						</div>
						<div class="form-group">
						<label for="CountryBillTo">Country (Bill To)</label>
						<input type="text" class="form-control" id="CountryBillTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CountryBillTo" maxlength="50" value="<?php echo $bp->vcBillToCountry;?>">
						</div>
						<div class="form-group">
						<label for="AddressBillTo">Address (Bill To)</label>
						<textarea class="form-control" rows="3" id="AddressBillTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="AddressBillTo"><?php echo $bp->vcBillToAddress;?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						<label for="CityShipTo">City (Ship To)</label>
						<input type="text" class="form-control" id="CityShipTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CityShipTo" maxlength="50" value="<?php echo $bp->vcShipToCity;?>">
						</div>
						<div class="form-group">
						<label for="CountryShipTo">Country (Ship To)</label>
						<input type="text" class="form-control" id="CountryShipTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CountryShipTo" maxlength="50" value="<?php echo $bp->vcShipToCountry;?>">
						</div>
						<div class="form-group">
						<label for="AddressShipTo">Address (Ship To)</label>
						<textarea class="form-control" rows="3" id="AddressShipTo" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="AddressShipTo"><?php echo $bp->vcShipToAddress;?></textarea>
						</div>
					</div>
				</div>
			  </div>
			  <button type="submit" class="btn btn-primary" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>
            </div>
			
			
          </div>
		  
        </div>
		
		</form>
	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>


   function changetype()
   {
	   var Type = $("#Type").val();
	   var BP = '<?php echo $bp->intID;?>';
	   $('#listcategory').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisGroup?type='+Type+'&BP='+BP+'');
		
   }
  $(function () {
	  
	//alert('XXX');
	var Type = $("#Type").val();
	var BP = '<?php echo $bp->intID;?>';
	<?php if($crudaccess->intUpdate==0) { ?>
	$('#listcategory').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisGroup?type='+Type+'&BP='+BP+'&withoutcontrol=true');
	<?php }else{ ?>
	$('#listcategory').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisGroup?type='+Type+'&BP='+BP+'');
	<?php } ?>
	
	//document.getElementById("Category").value = '<?php echo $bp->intCategory;?>';
	//alert('<?php echo $bp->intCategory;?>');
   
  });
</script>
</body>
</html>
