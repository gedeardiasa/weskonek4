<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>
.modal-dialog {
  width: 90%;
 
}

</style>
<div class="wrapper">

  <div class="modal fade" id="modal-add">
  <form role="form" method="POST" enctype="multipart/form-data"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd/">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New BP</h3>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#general_tab" data-toggle="tab">General</a></li>
						<li><a href="#contact_tab" data-toggle="tab">Contact</a></li>
						<li><a href="#address_tab" data-toggle="tab">Address</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="general_tab">
							<div class="row">
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Code" style="padding-left:0px" class="col-sm-8 control-label">Code</label>
								  <label for="Type" style="padding-left:20px" class="col-sm-4 control-label">Type</label>
								  <div class="row">
								    <div class="col-md-8">
									  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code" readonly="true">
									</div>
									<div class="col-md-4">
									  <select class="" id="Type" name="Type" style="width: 100%; height:35px" onchange="reloadtype()">
									  <option value="C">Customer</option>
									  <option value="S">Vendor</option>
									  <option value="X">Both</option>
									  </select>
									</div>
								  </div>
								</div>
								<div class="form-group">
								  <label for="Name">Name</label>
								  <input type="text" class="form-control" id="Name" name="Name" maxlength="75" required="true" placeholder="Enter Name">
								</div>
								<div class="form-group">
								  <label for="TaxNumber">Federal Tax ID</label>
								  <input type="text" class="form-control" id="TaxNumber" name="TaxNumber" maxlength="75" placeholder="Enter Federal Tax ID">
								</div>
								<div class="form-group">
								  <label for="PaymentTerm">Payment Term</label>
								  <div class="row">
								    <div class="col-md-6">
								    <input type="number" class="form-control" id="PaymentTerm" name="PaymentTerm" maxlength="5">
									</div>
									<div class="col-md-6">
									Day After Invoice Created
									</div>
								  </div>
								</div>
								
							  </div>
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Category">Category</label>
								  <div id="listcategory"></div>
								</div>
								<div class="form-group">
								  <label for="PriceList">Price List</label>
								  <select class="" id="PriceList" name="PriceList" style="width: 100%; height:35px">
										<?php 
										foreach($listpricelist->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
								<div class="form-group">
								  <label for="CreditLimit">Credit Limit</label>
								  <input type="number" class="form-control" id="CreditLimit" name="CreditLimit" maxlength="20" placeholder="Enter Credit Limit">
								</div>
								<div class="form-group">
								  <label for="Tax">Tax Group</label>
								  <select class="" id="Tax" name="Tax" style="width: 100%; height:35px">
										<?php 
										foreach($listtax->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
							  </div>
							</div>
						</div>
						<div class="tab-pane" id="contact_tab">
							<div class="row">
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Phone1">Phone 1</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
								  <input type="text" class="form-control" id="Phone1" name="Phone1" maxlength="50" placeholder="Enter Phone 1">
								  </div>
								</div>
								<div class="form-group">
								  <label for="Phone2">Phone 2</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
								  <input type="text" class="form-control" id="Phone2" name="Phone2" maxlength="50" placeholder="Enter Phone 2">
								  </div>
								</div>
								<div class="form-group">
								  <label for="Fax">Fax</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-fax" aria-hidden="true"></i></span>
								  <input type="text" class="form-control" id="Fax" name="Fax" maxlength="50" placeholder="Enter Fax">
								  </div>
								</div>
							  </div>
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Phone1">Email</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								  <input type="text" class="form-control" id="Email" name="Email" maxlength="100" placeholder="Enter Email">
								  </div>
								</div>
								<div class="form-group">
								  <label for="Website">Website</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span>
								  <input type="text" class="form-control" id="Website" name="Website" maxlength="100" placeholder="Enter Website">
								  </div>
								</div>
								<div class="form-group">
								  <label for="ContactPerson">Contact Person</label>
								  <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
								  <input type="text" class="form-control" id="ContactPerson" name="ContactPerson" maxlength="100" placeholder="Enter Contact Person">
								  </div>
								</div>
							  </div>
							</div>
						</div>
						<div class="tab-pane" id="address_tab">
							<div class="row">
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="CityBillTo">City (Bill To)</label>
								  <input type="text" class="form-control" id="CityBillTo" name="CityBillTo" maxlength="50" placeholder="Enter City">
								</div>
								<div class="form-group">
								  <label for="CountryBillTo">Country (Bill To)</label>
								  <input type="text" class="form-control" id="CountryBillTo" name="CountryBillTo" maxlength="50" placeholder="Enter Country">
								</div>
								<div class="form-group">
								  <label for="AddressBillTo">Address (Bill To)</label>
								  <textarea class="form-control" rows="3" id="AddressBillTo" name="AddressBillTo" placeholder="Address ..."></textarea>
								</div>
							  </div>
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="CityShipTo">City (Ship To)</label>
								  <input type="text" class="form-control" id="CityShipTo" name="CityShipTo" maxlength="50" placeholder="Enter City">
								</div>
								<div class="form-group">
								  <label for="CountryShipTo">Country (Ship To)</label>
								  <input type="text" class="form-control" id="CountryShipTo" name="CountryShipTo" maxlength="50" placeholder="Enter Country">
								</div>
								<div class="form-group">
								  <label for="AddressShipTo">Address (Ship To)</label>
								  <textarea class="form-control" rows="3" id="AddressShipTo" name="AddressShipTo" placeholder="Address ..."></textarea>
								</div>
							  </div>
							</div>
						</div>
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#modal-add" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
				  <th>Name</th>
				  <th>Code</th>
				  <th>Type</th>
                  <th>Category</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcName; ?></td>
                  <td><?php echo $d->vcCode; ?></td>
				  <td><?php echo $d->vcType; ?></td>
				  <td><?php echo $d->vcCategory; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true"></i></a>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	var Type = $("#Type").val();
	$('#listcategory').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisGroup?type='+Type+'');
	$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "Type="+Type+"",
			cache: true, 
			success: function(data){
				document.getElementById("Code").value = data;
			},
			async: false
	  });
	  
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
  function reloadtype()
  {
	  var Type = $("#Type").val();
	  $('#listcategory').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisGroup?type='+Type+'');
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "Type="+Type+"",
			cache: true, 
			success: function(data){
				document.getElementById("Code").value = data;
			},
			async: false
	  });
  }
  
 
</script>
</body>
</html>
