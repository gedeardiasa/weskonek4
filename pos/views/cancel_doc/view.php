<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">

  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form id="demo-form2" method="GET" action="">
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success alert-dismissible" id="successalert" style="display:none">
							<i class="icon fa fa-check"></i>Success. You successfully process the data
						</div>
						<div class="alert alert-danger  alert-dismissible" id="dangeralert" style="display:none">
							<i class="icon fa fa-ban"></i><span id="errormessage"></span>
						</div>
					</div>
					<div class="col-sm-6">
						
						
						<div class="form-group">
						  <label for="DocType" class="col-sm-4 control-label" style="height:20px">Doc. Type</label>
							<div class="col-sm-8" style="height:45px">
							<select id="DocType" name="DocType" class="form-control" onclick="hidealert()"data-toggle="tooltip" data-placement="top" title="Doc. Type">
								<?php 
								foreach($listdoc->result() as $d) 
								{
									if($d->vcCode=='INPAY' or $d->vcCode=='OUTPAY' or $d->vcCode=='AP' or $d->vcCode=='AR'){
								?>	
								<option value="<?php echo $d->vcCode;?>"><?php echo $d->vcName;?></option>
								<?php 
									}
								}
								?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Document Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="" onclick="hidealert()" id="DocNum" name="DocNum" data-toggle="tooltip" data-placement="top" title="Document Number">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="button" class="btn btn-primary" onclick="execute()">Execute</button>
                      </div>
                    </div>
				</div>
				</form>
				
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
					document.getElementById('DocNum').value = '';
					document.getElementById('DocType').value = '';
  });
  
  function hidealert()
  {
	  $("#successalert").hide();
	  $("#dangeralert").hide();
  }
  function execute()
  {
	  var DocNum  = $("#DocNum").val();
	  var DocType = $("#DocType").val();
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/execute?", 
			data: "DocNum="+DocNum+"&DocType="+DocType+"", 
			cache: true, 
			success: function(data){ 
			
				if(data==1)
				{
					$("#successalert").show();
					$("#dangeralert").hide();
					document.getElementById('DocNum').value = '';
					document.getElementById('DocType').value = '';
				}
				else
				{	
					$("#dangeralert").show();
					$("#successalert").hide();
					document.getElementById('errormessage').innerHTML = data;
				}
			},
			async: false
		});
	  
	  //
  }
 
</script>
</body>
</html>
