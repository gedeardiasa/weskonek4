<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
           
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> You are successfully edit setting.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> You are failed to edit setting.
                </div>
				<?php }?>
				    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" 
						action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit/">

					<?php foreach($list->result() as $d){?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $d->vcName;?> 
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
					    <select class="" id="<?php echo $d->vcCode;?>" name="<?php echo $d->vcCode;?>" style="width: 100%; height:35px">
						  <?php 
						  foreach($listgl->result() as $h) 
						  {
						  ?>	
						  <option value="<?php echo $h->vcCode;?>" <?php if($h->vcCode==$d->vcValue){ echo "selected";}?>><?php echo $h->vcCode."-".$h->vcName;?></option>
						  <?php 
						  }
						  ?>
						</select>
                        
                      </div>
                    </div>
					<?php }?>
                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" name="edit">Edit</button>
						
                      </div>
                    </div>

					</form>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	
</script>
</body>
</html>
