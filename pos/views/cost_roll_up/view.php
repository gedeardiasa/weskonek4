<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
		<div class="box">
            <div class="box-body">
				<form id="demo-form2" method="GET" action="<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="Whs" class="col-sm-4 control-label" style="height:20px">Location</label>
							<div class="col-sm-8" style="height:45px">
							<input type="hidden" name="execute" id="execute" value="true">
							<select id="Whs" name="Whs" class="form-control" data-toggle="tooltip" data-placement="top" title="Location">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($Whs==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Execute</button>
                      </div>
                    </div>
				</div>
				</form>
				
				<hr>
				
				<?php if(isset($_GET['execute'])){?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Bom</th>
					  <th>Version</th>
					  <th>Location</th>
					  <th>Item Code</th>
					  <th>Item Name</th>
					  <th>Qty</th>
					  <th>Cost Before</th>
					  <th>Cost</th>
					  <th></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($cru->result() as $cr){?>
					<tr>
					  <td><?php echo $cr->vcDocNum; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcRef; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcLocation; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcItemCode; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')"><?php echo $cr->vcItemName; ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')" align="right"><?php echo number_format($cr->intQty,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')" align="right"><?php echo number_format($cr->intCostBefore,'0'); ?></td>
					  <td onclick="showdetail('<?php echo $cr->BomID; ?>')" align="right"><?php echo number_format($cr->intCost,'0'); ?></td>
					  <td></td>
					</tr>
					<?php }?>
					
					</tbody>

				</table>
				
				<?php } ?>
			</div>
		</div>
    </section>
    <!-- /.content -->
	<div class="modal fade" id="modal-detail">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail</h3>
            </div>
            
              <div class="box-body">
				<div id="detailbom"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	
  function showdetail(id)
  {
	  $('#detailbom').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/detailbom?id='+id+'');
	  $('#modal-detail').modal('show');
  }
	
  $(function () {
	$("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
