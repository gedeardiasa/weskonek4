<?php 
		
		$year=date('Y');
		$month=date('m');
		$db=$this->load->database('default', TRUE);
		
		$qinvval=$db->query("
		SELECT 
		c.`vcName`, SUM(a.intStock*a.`intHPP`) AS valueItem
		FROM mstock a
		LEFT JOIN mitem b ON a.`intItem`=b.`intID`
		LEFT JOIN mitemcategory c ON b.`intCategory`=c.intID
		GROUP BY c.`vcName`
		ORDER BY SUM(a.intStock*a.`intHPP`) DESC

		");
		
		
		$qwallet=$db->query("
		select SUM(intBalance) as balance from mwallet where intDeleted=0
		");
		$wallet=$qwallet->row()->balance;
		
		$qopenso=$db->query("
		SELECT
		SUM(a.intPriceAfterDisc*a.`intOpenQty`) AS total, SUM(c.intHPP*a.`intOpenQty`) AS intHPP
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.intID
		LEFT JOIN mstock c ON a.`intItem`=c.intItem AND a.`intLocation`=c.intLocation
		WHERE a.vcStatus='O' AND b.vcStatus='O'
		");
		$openso=$qopenso->row()->total;
		
		$qwd=$db->query("
		SELECT SUM(intValue) AS intValue FROM hWalletAdjustment a
		WHERE MONTH(dtDate)=$month AND YEAR(dtDate)=$year AND vcType='WD'
		");
		
		$qdp=$db->query("
		SELECT SUM(intValue) AS intValue FROM hWalletAdjustment a
		WHERE MONTH(dtDate)=$month AND YEAR(dtDate)=$year AND vcType='DP'
		");
		
		$wd=$qwd->row()->intValue;
		$dp=$qdp->row()->intValue;
		
		
		
?>

<?php 
		
		$year=date('Y');
		$month=date('m');
		$db=$this->load->database('default', TRUE);
		$qtopitem=$db->query("
		SELECT 
		a.vcItemCode, a.`vcItemName`,
		SUM(a.`intQtyInv`) AS QtyInv,
		a.`vcUoMInv`, c.`blpImage`
		FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		LEFT JOIN mitem c ON a.`intItem`=c.intID
		WHERE MONTH(b.`dtDate`)=$month AND YEAR(b.`dtDate`)=$year
		GROUP BY a.vcItemCode
		ORDER BY SUM(a.`intQtyInv`) DESC
		LIMIT 0,4
		");
		
		
		
		$qtotalinvvalue=$db->query("
		select 
		SUM(a.intStock*a.intHPP) as value
		from mstock a
		");
		$datainvvalue=$qtotalinvvalue->row();
		
		$qcost=$db->query("
		SELECT
		SUM(a.cost) AS cost, a.intMonth
		FROM
		(
		# ambil cost dari AR/Penjualan
		SELECT SUM(a.intLineCost) AS cost, MONTH(b.dtDate) AS intMonth FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.dtDate)=$year and b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		
		UNION ALL
		# ambil cost AP dengan tipe service
		SELECT SUM(a.intLineCost) AS cost,MONTH(b.dtDate) AS intMonth
		FROM dAP a
		LEFT JOIN hAP b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.`dtDate`)=$year AND b.vcStatus!='X' AND b.intService=1
		GROUP BY MONTH(b.dtDate)
		) a
		GROUP BY a.intMonth
		");
		
		$qrevenue=$db->query("
		# revenue di dapat dari line total AR yang sudah dikurangi Disc Header
		# revenua juga di dapat dari ongkir AR (masuk pendapatan lain2)
		SELECT
		SUM(a.revenue) AS revenue, a.intMonth
		FROM
		(
		SELECT SUM(a.intLineTotal-(b.intDiscPer/100*a.intLineTotal)) AS revenue, MONTH(b.dtDate) AS intMonth FROM dAR a
		LEFT JOIN hAR b ON a.`intHID`=b.`intID`
		WHERE YEAR(b.dtDate)=$year and b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		
		union all
		SELECT SUM(b.intFreight) AS revenue, MONTH(b.dtDate) AS intMonth FROM
		hAR b
		WHERE YEAR(b.dtDate)=$year AND b.vcStatus!='X'
		GROUP BY MONTH(b.dtDate)
		) a
		GROUP BY a.intMonth
		");
		$thismonth=date('m');
		$lastmonth=date('m')-1;
		
		$totalrevenue=0;
		$revthismonth=0;
		$revlastmonth=0;
		foreach($qrevenue->result() as $rev)
		{
			if($rev->intMonth==$thismonth)
			{
				$revthismonth=$rev->revenue;
			}
			if($rev->intMonth==$lastmonth)
			{
				$revlastmonth=$rev->revenue;
			}
			$totalrevenue=$totalrevenue+$rev->revenue;
		}
		if($thismonth>1)
		{
			if($revlastmonth==0)
			{
				$uprev=0;
			}
			else
			{
				$uprev=($revthismonth-$revlastmonth) / $revlastmonth *100;
			}
		}
		else
		{
			$uprev=0;
		}
		
		$totalcost=0;
		$costthismonth=0;
		$costlastmonth=0;
		foreach($qcost->result() as $rev)
		{
			if($rev->intMonth==$thismonth)
			{
				$costthismonth=$rev->cost;
			}
			if($rev->intMonth==$lastmonth)
			{
				$costlastmonth=$rev->cost;
			}
			$totalcost=$totalcost+$rev->cost;
		}
		if($thismonth>1)
		{
			if($costlastmonth==0)
			{
				$upcost=0;
			}
			else
			{
				$upcost=($costthismonth-$costlastmonth) / $costlastmonth *100;
			}
		}
		else
		{
			$upcost=0;
		}
		
		
		$profitthismonth=$revthismonth-$costthismonth;
		$profitlastmonth=$revlastmonth-$costlastmonth;
		if($thismonth>1)
		{
			if($profitlastmonth==0)
			{
				$upprofit=0;
			}
			else
			{	
				$upprofit=($profitthismonth-$profitlastmonth) / $profitlastmonth *100;
			}
		}
		else
		{
			$upprofit=0;
		}
		
		
?>	

<?php

$netprofit=($profitthismonth)-$wd

?>			
	<div class="row">
        <div class="col-md-12">
			<div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo 'IDR '.number_format($netprofit,'2');?></h5>
                    <span class="description-text">NET PROFIT THIS MONTH</span>
                  </div>
                  <!-- /.description-block -->
                </div>
				<div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo 'IDR '.number_format($datainvvalue->value+$wallet,'2');?></h5>
                    <span class="description-text">ASSET TOTAL</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                
              </div>
              <!-- /.row -->
            </div>
		</div>
	</div>
	<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Inventory Value</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php
				
				foreach($qinvval->result() as $ivl)
				{
					if($ivl->valueItem>0)
					{
				?>
                <li><a href="#"><?php echo $ivl->vcName; ?> <span class="pull-right text-green"><?php echo number_format($ivl->valueItem,0); ?></span></a>
                </li>
				<?php
					}
				}
				?>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->