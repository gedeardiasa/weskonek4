<?php 
		
		$year=date('Y');
		$month=date('m');
		$db=$this->load->database('default', TRUE);
		$qwallet=$db->query("
		select SUM(intBalance) as balance from mwallet where intDeleted=0
		");
		$wallet=$qwallet->row()->balance;
		
		$qopenso=$db->query("
		SELECT
		SUM(a.intPriceAfterDisc*a.`intOpenQty`) AS total, SUM(c.intHPP*a.`intOpenQty`) AS intHPP
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.intID
		LEFT JOIN mstock c ON a.`intItem`=c.intItem AND a.`intLocation`=c.intLocation
		WHERE a.vcStatus='O' AND b.vcStatus='O'
		");
		$openso=$qopenso->row()->total;
		
		$qwd=$db->query("
		SELECT SUM(intValue) AS intValue FROM hWalletAdjustment a
		WHERE MONTH(dtDate)=$month AND YEAR(dtDate)=$year AND vcType='WD'
		");
		
		$qdp=$db->query("
		SELECT SUM(intValue) AS intValue FROM hWalletAdjustment a
		WHERE MONTH(dtDate)=$month AND YEAR(dtDate)=$year AND vcType='DP'
		");
		
		$wd=$qwd->row()->intValue;
		$dp=$qdp->row()->intValue;
		
		
		
?>
   <div class="modal fade" id="modal-<?php echo $cart->vcCode;?>-TotalWallet">
   <?php
   $qlistwallet=$this->db->query("select * from mwallet where intDeleted=0");
   ?>
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Wallet List</h3>
            </div>
            
              <div class="box-body">
				<table id="example<?php echo $cart->vcCode;?>-TotalWallet" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Code</th>
				  <th>Name</th>
				  <th>Balance</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($qlistwallet->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcCode; ?></td>
                  <td><?php echo $d->vcName; ?></td>
				  <td align="right"><?php echo number_format($d->intBalance,'0'); ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
   
   
   <div class="modal fade" id="modal-<?php echo $cart->vcCode;?>-OpenSO">
   <?php
   $qlistopenso=$this->db->query("SELECT 
		b.vcBPName, SUM(a.intLineTotal) as intLineTotal
		FROM dSO a
		LEFT JOIN hSO b ON a.`intHID`=b.intID
		where a.vcStatus='O' and b.vcStatus='O'
		group by vcBPName
		ORDER by SUM(a.intLineTotal) desc
		");
   ?>
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Open SO</h3>
            </div>
            
              <div class="box-body">
				<table id="example<?php echo $cart->vcCode;?>-OpenSO" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>BP</th>
				  <th>IDR</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				$totallineopenso=0;
				foreach($qlistopenso->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcBPName; ?></td>
				  <td align="right"><?php echo number_format($d->intLineTotal,'0'); ?></td>
                </tr>
				<?php 
				$totallineopenso=$totallineopenso+$d->intLineTotal;
				}
				?>
                </tbody>
				
              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
<!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12" onclick="showDetail<?php echo $cart->vcCode;?>TotalWallet()">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-university"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Wallet</span>
              <span class="info-box-number"><?php echo number_format($wallet,'0');?><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12" onclick="showDetail<?php echo $cart->vcCode;?>OpenSO()">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-line-chart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Open SO</span>
              <span class="info-box-number"><?php echo number_format($openso,'0');?></span>
			  <?php echo number_format($openso-$qopenso->row()->intHPP,'0');?>(Possibility Profit)
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12" onclick="showDetail<?php echo $cart->vcCode;?>Withdrawal()">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-arrow-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Withdrawal</span>
              <span class="info-box-number"><?php echo number_format($wd,'0');?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12" onclick="showDetail<?php echo $cart->vcCode;?>Deposit()">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Deposit</span>
              <span class="info-box-number"><?php echo number_format($dp,'0');?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<script>
function showDetail<?php echo $cart->vcCode;?>TotalWallet()
{
	$("#modal-<?php echo $cart->vcCode;?>-TotalWallet").modal('show');
}
function showDetail<?php echo $cart->vcCode;?>OpenSO()
{
	$("#modal-<?php echo $cart->vcCode;?>-OpenSO").modal('show');
}
function showDetail<?php echo $cart->vcCode;?>Withdrawal()
{
	$("#modal-<?php echo $cart->vcCode;?>-Withdrawal").modal('show');
}
function showDetail<?php echo $cart->vcCode;?>Deposit()
{
	$("#modal-<?php echo $cart->vcCode;?>-Deposit").modal('show');
}

</script>