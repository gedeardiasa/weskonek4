<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-addedit">
  <form role="form" method="POST"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesaddedit">
	<input type="hidden" class="form-control" id="ID" name="ID" maxlength="25" >
	<input type="hidden" class="form-control" id="type" name="type" maxlength="25" >
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
			  <h3 class="box-title" id="tittleEdit">Edit Document</h3>
            </div>
            
              <div class="box-body">
				<div class="form-group">
                  <label for="Name">Name</label>
                  <input type="text" class="form-control" id="Name" name="Name" maxlength="25" required="true" placeholder="Enter Name">
                </div>
				<div class="form-group">
                  <label for="Code">Code</label>
                  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code" readonly="true">
                </div>
				<div class="form-group">
                  <label for="Width">Width (In cm)</label>
                  <input type="text" class="form-control" id="Width" name="Width" maxlength="10" required="true" placeholder="Enter Width">
                </div>
				<div class="form-group">
                  <label for="Height">Height (In cm)</label>
                  <input type="text" class="form-control" id="Height" name="Height" maxlength="10" placeholder="Enter Height">
                </div>
				<div class="form-group">
                  <label for="Font">Font</label>
                  <input type="text" class="form-control" id="Font" name="Font" maxlength="25" placeholder="Enter Font">
                </div>
				
                
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary" id="editbutton">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
           
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Name </th>
                  <th>Code </th>
				  <th>Width </th>
                  <th>Height </th>
				  <th>Font </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listdoc->result() as $d) 
				{
				?>
                <tr>
                  <td class=" "><?php echo $d->vcName; ?></td>
				  <td class=" "><?php echo $d->vcCode; ?></td>
				  <td class=" "><?php echo $d->intWidth; ?></td>
				  <td class=" "><?php echo $d->intHeight; ?></td>
				  <td class=" "><?php echo $d->vcFontFamily; ?></td>
				  <td align="center">
				  <i class="fa fa-search <?php echo $usericon;?>" data-toggle="modal" onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcName); ?>',
				  '<?php echo str_replace("'","\'",$d->vcCode); ?>','<?php echo str_replace("'","\'",$d->intWidth); ?>'
				  ,'<?php echo $d->intHeight; ?>','<?php echo $d->vcFontFamily; ?>')" data-target="#modal-addedit" aria-hidden="true"></i>
				 
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	function edit(id,name,code,width,height,font)
	{
		$("#tittleEdit").show();
		$("#editbutton").show();
		document.getElementById("type").value = 'edit';
		document.getElementById("ID").value = id;
		document.getElementById("Name").value = name;
		document.getElementById("Code").value = code;
		document.getElementById("Width").value = width;
		document.getElementById("Height").value = height;
		document.getElementById("Font").value = font;
		
		
	}
	
  $(function () {
	  
	$(".select2").select2();
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
