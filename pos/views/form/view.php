<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-addedit">
  <form role="form" method="POST"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesaddedit">
	<input type="hidden" class="form-control" id="ID" name="ID" maxlength="25" >
	<input type="hidden" class="form-control" id="type" name="type" maxlength="25" >
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" id="tittleAdd">Add New Form</h3>
			  <h3 class="box-title" id="tittleEdit">Edit Form</h3>
            </div>
            
              <div class="box-body">
				<div class="form-group">
                  <label for="Name">Name</label>
                  <input type="text" class="form-control" id="Name" name="Name" maxlength="25" required="true" placeholder="Enter Name">
                </div>
				<div class="form-group">
                  <label for="Code">Code</label>
                  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code">
                </div>
				<div class="form-group">
                  <label for="Header">Header</label>
                  <select class="" id="Header" name="Header" style="width: 100%; height:35px">
						<option value="0">--SET AS HEADER--</option>
						<?php 
						foreach($listheader->result() as $d) 
						{
						?>	
						<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
						<?php 
						}
						?>
				   </select>
                </div>
				<div class="form-group">
                  <label>Remarks</label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..."></textarea>
                </div>
                
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <!--<button type="button" class="btn btn-danger" id="deletebutton" <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?> data-toggle="modal" data-target="#modal-delete">Delete</button>
		   <button type="submit" class="btn btn-primary" id="addbutton" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>Save changes</button>
		   <button type="submit" class="btn btn-primary" id="editbutton" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>-->
         </div>
       </div>
     </div>
   </form>
   </div>
  
  <div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
		  <form role="form" method="POST"
		  action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesdelete/">
		  <input type="hidden" class="form-control" id="ID2" name="ID2" maxlength="25" >
          <button type="submit" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <button type="submit" class="btn btn-danger">Delete</button>
		  </form>
         </div>
       </div>
     </div>
  </div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <!--<button type="button" class="btn btn-dark" onclick="add()"  data-toggle="modal" data-target="#modal-addedit"
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Name </th>
                  <th>Code </th>
				  <th>Header Menu </th>
                  <th>Is Header Menu </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listform->result() as $d) 
				{
				?>
                <tr>
                  <td class=" "><?php echo $d->vcName; ?></td>
				  <td class=" "><?php echo $d->vcCode; ?></td>
				  <td class=" "><?php echo $d->vcHeader; ?></td>
				  <td class=" "><?php echo $d->vcIsHeader; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <i class="fa fa-search <?php echo $usericon;?>" data-toggle="modal" onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcName); ?>',
				  '<?php echo str_replace("'","\'",$d->vcCode); ?>','<?php echo str_replace("'","\'",$d->vcRemarks); ?>'
				  ,'<?php echo $d->intHeader; ?>')" data-target="#modal-addedit" aria-hidden="true"></i>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	function edit(id,name,code,remarks,header)
	{
		$("#tittleEdit").show();
		$("#tittleAdd").hide();
		$("#deletebutton").show();
		$("#addbutton").hide();
		$("#editbutton").show();
		document.getElementById("type").value = 'edit';
		document.getElementById("ID").value = id;
		document.getElementById("ID2").value = id;
		document.getElementById("Name").value = name;
		document.getElementById("Code").value = code;
		$("#Header").val(header);
		document.getElementById("Remarks").value = remarks;
		
		<?php if($crudaccess->intUpdate==0) { ?>
		$('#Code').attr("disabled", true);
		$('#Name').attr("disabled", true);
		$('#Header').attr("disabled", true);
		$('#Remarks').attr("disabled", true);
		
		<?php }else{ ?>
		$('#Code').attr("disabled", false);
		$('#Name').attr("disabled", false);
		$('#Header').attr("disabled", false);
		$('#Remarks').attr("disabled", false);
		
		<?php } ?>
	}
	function add()
	{
		 $("#tittleAdd").show();
		 $("#tittleEdit").hide();
		 $("#deletebutton").hide();
		 $("#addbutton").show();
		 $("#editbutton").hide();
		 document.getElementById("type").value = 'add';
		 document.getElementById("Name").value = '';
		 document.getElementById("Code").value = '';
		 $('#Header option:first-child').attr("selected", "selected");
		 document.getElementById("Remarks").value = '';
		 <?php if($crudaccess->intCreate==0) { ?>
		 $('#Code').attr("disabled", true);
		 $('#Name').attr("disabled", true);
		 $('#Header').attr("disabled", true);
		 $('#Remarks').attr("disabled", true);
		 
		 <?php }else{ ?>
		 $('#Code').attr("disabled", false);
	 	 $('#Name').attr("disabled", false);
		 $('#Header').attr("disabled", false);
		 $('#Remarks').attr("disabled", false);
		 
		 <?php } ?>
	}
  $(function () {
	  
	$(".select2").select2();
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
