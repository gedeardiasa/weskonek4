<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">

  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form role="form" method="POST" enctype="multipart/form-data"
				action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd/">
				<div class="row">
					<div class="col-sm-12">
						<?php if(isset($_GET['success'])){?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						  </button>
						  <strong>Success!</strong> <?php echo $message;?>.
						</div>
						<?php }?>
						<?php if(isset($_GET['error'])){?>
						<div class="alert alert-danger alert-dismissible fade in" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						  </button>
						  <strong>Error!</strong> <?php echo $message;?>.
						</div>
						<?php }?>
					</div>
					<div class="col-sm-6">
						
						
						<div class="form-group">
						  <label for="Type" class="col-sm-4 control-label" style="height:40px">Type</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Type" name="Type" class="form-control" onclick="hidealert()" onchange="reloaddownload()" data-toggle="tooltip" data-placement="top" title="Type">
								
								<option value="item" <?php if($datatype=='item'){ echo "selected";}?>>Item Master Data</option>
								<option value="ib" <?php if($datatype=='ib'){ echo "selected";}?>>Initial Balance Stock</option>
								<option value="price" <?php if($datatype=='price'){ echo "selected";}?>>Price List</option>
								<option value="bp" <?php if($datatype=='bp'){ echo "selected";}?>>Business Partner</option>
								<option value="ar" <?php if($datatype=='ar'){ echo "selected";}?>>A/R Invoice</option>
								<option value="ap" <?php if($datatype=='ap'){ echo "selected";}?>>A/P Invoice</option>
								<option value="bom" <?php if($datatype=='bom'){ echo "selected";}?>>BOM</option>
							</select>
							
							</div>
						</div>
						<div class="form-group">
						  <label for="Image" class="col-sm-4 control-label" style="height:40px">File</label>
							<div class="col-sm-8" style="height:45px">
							<input type="file" id="Image" name="Image" onchange="readURL(this);">
							</div>
						</div>
						
					</div>
					<div class="col-sm-6" id="datadownload"></div>
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Upload</button>
                      </div>
                    </div>
				</div>
				</form>
				
				<hr>
				<?php
				if(isset($_GET['data']))
				{
					if($_GET['data']=='item')
					{
				?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>Category</th>
					  <th>Code</th>
					  <th>Name</th>
					  <th>Price</th>
					  <th>Tax</th>
					  <th>Remarks</th>
					  <th>UoM</th>
					  <th>PurUoM</th>
					  <th>intPurUoM</th>
					  <th>SlsUoM</th>
					  <th>intSlsUoM</th>
					  <th>intSalesItem</th>
					  <th>intPurchaseItem</th>
					  <th>intSalesWithoutQty</th>
					  <th>Barcode</th>
					  
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['Category'][$i]; ?></td>
					  <td><?php echo $_SESSION['Code'][$i]; ?></td>
					  <td><?php echo $_SESSION['Name'][$i]; ?></td>
					  <td><?php echo $_SESSION['Price'][$i]; ?></td>
					  <td><?php echo $_SESSION['Tax'][$i]; ?></td>
					  <td><?php echo $_SESSION['Remarks'][$i]; ?></td>
					  <td><?php echo $_SESSION['UoM'][$i]; ?></td>
					  <td><?php echo $_SESSION['PurUoM'][$i]; ?></td>
					  <td><?php echo $_SESSION['intPurUoM'][$i]; ?></td>
					  <td><?php echo $_SESSION['SlsUoM'][$i]; ?></td>
					  <td><?php echo $_SESSION['intSlsUoM'][$i]; ?></td>
					  <td><?php echo $_SESSION['intSalesItem'][$i]; ?></td>
					  <td><?php echo $_SESSION['intPurchaseItem'][$i]; ?></td>
					  <td><?php echo $_SESSION['intSalesWithoutQty'][$i]; ?></td>
					  <td><?php echo $_SESSION['Barcode'][$i]; ?></td>
					  
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('item')">Execute</button>
					<?php } ?>
				<?php
					}
					else if($_GET['data']=='ib')
					{
				?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>Date</th>
					  <th>Item Code</th>
					  <th>Type</th>
					  <th>Location</th>
					  <th>Qty</th>
					  <th>Cost</th>
					  
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['Date'][$i]; ?></td>
					  <td><?php echo $_SESSION['ItemCode'][$i]; ?></td>
					  <td><?php echo $_SESSION['Type'][$i]; ?></td>
					  <td><?php echo $_SESSION['Location'][$i]; ?></td>
					  <td><?php echo $_SESSION['Qty'][$i]; ?></td>
					  <td><?php echo $_SESSION['Cost'][$i]; ?></td>
					  
					  
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('ib')">Execute</button>
					<?php } ?>
				<?php		
					}
					else if($_GET['data']=='price')
					{
					?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>Price Name</th>
					  <th>Item Code</th>
					  <th>Price</th>
					  
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['Category'][$i]; ?></td>
					  <td><?php echo $_SESSION['ItemCode'][$i]; ?></td>
					  <td><?php echo $_SESSION['Price'][$i]; ?></td>
					  
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('price')">Execute</button>
					<?php } ?>
				<?php
					}
					else if($_GET['data']=='bp')
					{
					?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>Type</th>
					  <th>Category</th>
					  <th>Code</th>
					  <th>Name</th>
					  <th>Tax Number</th>
					  <th>Price List</th>
					  <th>Credit Limit</th>
					  <th>Payment Term</th>
					  <th>Tax</th>
					  <th>Phone1</th>
					  <th>Phone2</th>
					  <th>Fax</th>
					  <th>Email</th>
					  <th>Website</th>
					  <th>Contact Person</th>
					  <th>City Bill To</th>
					  <th>Country Bill To</th>
					  <th>Address Bill To</th>
					  <th>City Ship To</th>
					  <th>Country Ship To</th>
					  <th>Address Ship To</th>
					  
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['Type'][$i]; ?></td>
					  <td><?php echo $_SESSION['Category'][$i]; ?></td>
					  <td><?php echo $_SESSION['Code'][$i]; ?></td>
					  <td><?php echo $_SESSION['Name'][$i]; ?></td>
					  <td><?php echo $_SESSION['TaxNumber'][$i]; ?></td>
					  <td><?php echo $_SESSION['PriceList'][$i]; ?></td>
					  <td><?php echo $_SESSION['CreditLimit'][$i]; ?></td>
					  <td><?php echo $_SESSION['PaymentTerm'][$i]; ?></td>
					  <td><?php echo $_SESSION['Tax'][$i]; ?></td>
					  <td><?php echo $_SESSION['Phone1'][$i]; ?></td>
					  <td><?php echo $_SESSION['Phone2'][$i]; ?></td>
					  <td><?php echo $_SESSION['Fax'][$i]; ?></td>
					  <td><?php echo $_SESSION['Email'][$i]; ?></td>
					  <td><?php echo $_SESSION['Website'][$i]; ?></td>
					  <td><?php echo $_SESSION['ContactPerson'][$i]; ?></td>
					  <td><?php echo $_SESSION['CityBillTo'][$i]; ?></td>
					  <td><?php echo $_SESSION['CountryBillTo'][$i]; ?></td>
					  <td><?php echo $_SESSION['AddressBillTo'][$i]; ?></td>
					  <td><?php echo $_SESSION['CityShipTo'][$i]; ?></td>
					  <td><?php echo $_SESSION['CountryShipTo'][$i]; ?></td>
					  <td><?php echo $_SESSION['AddressShipTo'][$i]; ?></td>
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('bp')">Execute</button>
					<?php } ?>
				<?php
					}else if($_GET['data']=='ar')
					{
					?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>BP Code</th>
					  <th>Ref. Number</th>
					  <th>Doc. Date</th>
					  <th>Del. Date</th>
					  <th>Sales Employee</th>
					  <th>Remarks</th>
					  <th>Doc. Total</th>
					  <th>Location</th>
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['BPCode'][$i]; ?></td>
					  <td><?php echo $_SESSION['RefNum'][$i]; ?></td>
					  <td><?php echo $_SESSION['DocDate'][$i]; ?></td>
					  <td><?php echo $_SESSION['DelDate'][$i]; ?></td>
					  <td><?php echo $_SESSION['SalesEmp'][$i]; ?></td>
					  <td><?php echo $_SESSION['Remarks'][$i]; ?></td>
					  <td><?php echo $_SESSION['DocTotal'][$i]; ?></td>
					  <td><?php echo $_SESSION['Location'][$i]; ?></td>
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('ar')">Execute</button>
					<?php } ?>
				<?php
					}else if($_GET['data']=='ap')
					{
					?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>BP Code</th>
					  <th>Ref. Number</th>
					  <th>Doc. Date</th>
					  <th>Del. Date</th>
					  <th>Purchasing Employee</th>
					  <th>Remarks</th>
					  <th>Doc. Total</th>
					  <th>Location</th>
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['BPCode'][$i]; ?></td>
					  <td><?php echo $_SESSION['RefNum'][$i]; ?></td>
					  <td><?php echo $_SESSION['DocDate'][$i]; ?></td>
					  <td><?php echo $_SESSION['DelDate'][$i]; ?></td>
					  <td><?php echo $_SESSION['SalesEmp'][$i]; ?></td>
					  <td><?php echo $_SESSION['Remarks'][$i]; ?></td>
					  <td><?php echo $_SESSION['DocTotal'][$i]; ?></td>
					  <td><?php echo $_SESSION['Location'][$i]; ?></td>
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('ap')">Execute</button>
					<?php } ?>
				<?php
					}else if($_GET['data']=='bom')
					{
					?>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
					<thead>
					<tr>
					  <th>Return</th>
					  <th>Doc. Date</th>
					  <th>FG Item</th>
					  <th>Location</th>
					  <th>FG Qty</th>
					  <th>Version</th>
					  <th>Remarks</th>
					  <th>Component Item</th>
					  <th>Component Qty</th>
					  <th>Auto Issue</th>
					</tr>
					</thead>
					<tbody>
					<?php for($i=0;$i<$_SESSION['totitemIMPORT'];$i++){?>
					<tr>
					  <td><?php echo $_SESSION['messageIMPORT'][$i]; ?></td>
					  <td><?php echo $_SESSION['DocDate'][$i]; ?></td>
					  <td><?php echo $_SESSION['FGItem'][$i]; ?></td>
					  <td><?php echo $_SESSION['Whs'][$i]; ?></td>
					  <td><?php echo $_SESSION['FGQty'][$i]; ?></td>
					  <td><?php echo $_SESSION['Rev'][$i]; ?></td>
					  <td><?php echo $_SESSION['Remarks'][$i]; ?></td>
					  <td><?php echo $_SESSION['itemcodeBOM'][$i]; ?></td>
					  <td><?php echo $_SESSION['qtyBOM'][$i]; ?></td>
					  <td><?php echo $_SESSION['autoBOM'][$i]; ?></td>
					</tr>
					<?php }?>
					
					</tbody>

					</table>
					<?php if(!isset($_GET['finish'])){?>
					<button type="button" class="btn btn-primary" onclick="upload('bom')">Execute</button>
					<?php } ?>
				<?php
					}
				
				}
				?>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
		reloaddownload();
		$("#example1").DataTable({
			//dom: 'Bfrtip',
			"oLanguage": {
			  "sSearch": "Search:"
			},
			'iDisplayLength': 10,
			//"sPaginationType": "full_numbers",
			"dom": 'T<"clear">lfrtip',
			"tableTools": {
			  "sSwfPath": ""
			},
			dom: 'Blfrtip',
			"aaSorting": [],
			buttons: [
			   {
				   extend: 'pdf',
				   footer: false,
			   },
			   {
				   extend: 'csv',
				   footer: false
				  
			   },
			   {
				   extend: 'excel',
				   footer: false
			   }         
			]  
		});
  });
  function reloaddownload()
  {
	 var Type = $("#Type").val();
	 $('#datadownload').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/reloaddownload?Type='+Type+'');
  }
  function upload(type)
  {
	  window.location.href = "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/upload?type="+type+"";
  }
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.xls)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .xls only.');
			fileInput.value = '';
			return false;
		}else{
			
		}
	}

         
   }
}
 
</script>
</body>
</html>
