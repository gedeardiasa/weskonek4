<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>

.modal-dialog {
  width: 90%;
 
}
</style>
<div class="wrapper">   
  
  
  <div class="modal fade" id="modal-add-edit">
  <div id="printarea_toolbar">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodaladd">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Add New Incoming Payment</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			<div class="box-header with-border" id="tittlemodaledit">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Detail Incoming Payment</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
              <div class="box-body">
			  <?php $this->load->view('loadingbar2'); ?>
				<div class="row">
				  <div class="alert alert-success alert-dismissible" id="successalert">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i>Success. You successfully process the data
				  </div>
				  <div class="alert alert-danger  alert-dismissible" id="dangeralert">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon fa fa-ban"></i>Error. You failed to process the data
				  </div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" data-toggle="tooltip" data-placement="top" title="Document Number" readonly="true">
							<input type="hidden" id="idHeader" name="idHeader">
							<input type="hidden" id="IdToolbar" name="IdToolbar">
							</div>
						</div>
						
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">BP Code</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group">
							  <div class="input-group-addon" id="triggermodalbp" data-toggle="modal" data-target="#modal-bp">
								<i class="fa fa-users" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control" id="BPCode" name="BPCode" placeholder="Bussiness Partner Code" data-toggle="tooltip" data-placement="top" title="Bussiness Partner Code" onchange="loadbpcode()" onpaste="loadbpcode()" >
							</div>
							</div>
							
						</div>
						<div class="form-group">
						  <label for="BPName" class="col-sm-4 control-label" style="height:20px">BP Name</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="BPName" name="BPName" placeholder="Bussiness Partner Name" onchange="loadbpname()" onpaste="loadbpname()" data-toggle="tooltip" data-placement="top" title="Bussiness Partner Name">
							</div>
						</div>
						
						
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label for="Status" class="col-sm-4 control-label" style="height:20px">Status</label>
							<div class="col-sm-8" style="height:45px">
							<table style="width:100%">
							  <tr>
								<td><input type="text" class="form-control" id="Status" name="Status" readonly="true" data-toggle="tooltip" data-placement="top" title="Status">
								</td>
								<td>
								<span data-toggle="modal" data-target="#modal-closeall">
								<i id="iconcloseheader" 
								data-toggle="tooltip" data-placement="top" title="Cancel Document" class="fa fa-close fa-2x" aria-hidden="true"></i>
								</span>
								</td>
							  </tr>
							</table>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Ref. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="RefNum" name="RefNum" placeholder="Ref. Number" data-toggle="tooltip" data-placement="top" title="Reference Number">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div id="mix_detail"></div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label>Remarks</label>
						  <textarea class="form-control" rows="6" id="Remarks" name="Remarks" placeholder="Remarks ..."
						  data-toggle="tooltip" data-placement="top" title="Remarks"
						  ></textarea>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="DocTotalBefore" class="col-sm-4 control-label" style="height:20px">Total</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocTotalBefore" name="DocTotalBefore" readonly="true" style="text-align:right;"
							data-toggle="tooltip" data-placement="top" title="Total" onclick="loadtotal()">
							</div>
						</div>
						<div class="form-group">
						
						
						  <label for="AppliedAmount" class="col-sm-4 control-label" style="height:20px">Applied Amount</label>
							<div class="col-sm-3" style="height:45px">
							<select id="Wallet" name="Wallet"class="form-control" data-toggle="tooltip" data-placement="top" title="Wallet">
								<?php 
								foreach($listwallet->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
							<div class="col-sm-5" style="height:45px">
							<input type="text" class="form-control" id="AppliedAmount" name="AppliedAmount" style="text-align:right;" data-toggle="tooltip" data-placement="top" 
							 onchange="loadbalancedue()" onpaste="loadbalancedue()" onkeyup="loadbalancedue()" onclick="loadbalancedue()" title="Applied Amount">
							</div>
						</div>
						<div class="form-group">
						  <label for="BalanceDue" class="col-sm-4 control-label" style="height:20px">Balance Due</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="BalanceDue" name="BalanceDue" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Balance Due">
							</div>
						</div>
					</div>
				</div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Cancel</button>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?> id="buttonsave">Save changes</button>
		 </div>
       </div>
     </div>
   </div>
   </div>
   <div class="modal fade" id="modal-closeall">
     <div class="modal-dialog">
       <div class="modal-content">
		
         <div class="modal-body" align="center">
		   
           <h2>Are you sure want to cancel this data?</h2>
		   <div class="loadertransaction"></div>
         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
		  <button type="button" class="btn btn-danger" onclick="closeall()">Yes</button></a>
         </div>
       </div>
     </div>
  </div>

  <?php $this->load->view('selectbp'); ?>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<?php $this->load->view('loadingbar2'); ?>
				<div class="col-sm-2" style="padding-left:0px;margin-left:0px">
                  <button type="button" style="width:100%" class="btn btn-dark" data-toggle="modal" data-target="#modal-add-edit" onclick="initialadd()" 
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
				</div>
				<div class="col-sm-7" style="padding-left:0px;margin-left:0px">
				&nbsp;
				</div>
				<div class="col-sm-3" style="padding-left:0px;margin-right:0px">
				<table align="center" style="width:100%">
					<tr>
						<td><input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff"></td>
						<td style="padding-left:5px" align="right"><button type="submit" class="btn btn-primary" onclick="loaddetailheader()">Reload</button>
						</td>
					</tr>
				</table>
				</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<div id="mix_header"></div>
				<div class="loader"></div>
			  
			  
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>

  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode == 13) //shortcut enter pada detail data untuk menambah data detail
	{
        addDetail();
    }
  });
  
  var globalDocTotalBefore;
  
  /*
  
	FUNGSI COPY FROM
  
  */
 
  /*
  
	FUNGSI LOAD
  
  */
  function loaddetailheader()
  {
	  $(".loadingbar2").show();
	  var daterange = $("#daterange").val();
	  daterange = daterange.split(' ').join('');
	  $('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'', function(){
		$(".loadingbar2").hide();
	  });   
  }
  function loadtotal()
  {
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  document.getElementById("AppliedAmount").value = addCommas(DocTotalBefore);
	  loadbalancedue();
  }
  function loadbalancedue()
  {
	  
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  if($("#AppliedAmount").val()=='')
	  {
		  AppliedAmount='0.00';
	  }
	  else
	  {
		var AppliedAmount = unformat_number($("#AppliedAmount").val());
	  }
	  if(AppliedAmount>DocTotalBefore)
	  {
		  alert('Applied Amount more than Total');
	  }
	  document.getElementById("BalanceDue").value = addCommas(DocTotalBefore-AppliedAmount);
  }
  function loaddataar()
  {
	  var BPCode = $("#BPCode").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddataar?", 
			data: "BPCode="+BPCode+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				document.getElementById("BalanceDue").value = '0.00';
				document.getElementById("AppliedAmount").value = '0.00';
				document.getElementById("DocTotalBefore").value = '0.00';
			}
	  });
  }
  function loadbpcode() //untuk reload BP saat Code diketik
  {
	  var BPCode = $("#BPCode").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getBPName?", 
			data: "BPCode="+BPCode+"",
			cache: true, 
			success: function(data){
				document.getElementById("BPName").value = data;
				loaddataar();
			},
		  });
	  
  }
  function loadbpname() //untuk reload BP saat Name diketik
  {
	  var BPName = $("#BPName").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getBPCode?", 
			data: "BPName="+BPName+"",
			cache: true, 
			success: function(data){
				document.getElementById("BPCode").value = data;
				loaddataar();
			},
		  });
	  
  }
  function loadheader(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				document.getElementById("BPCode").value = obj.BPCode;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("RefNum").value = obj.RefNum;
				document.getElementById("Status").value = obj.Status;
				if(obj.Status=='Cancel')
				{
					
					$("#iconcloseheader").hide();
				}
				else
				{
					$("#iconcloseheader").show();
				}
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				
				document.getElementById("Remarks").value = obj.Remarks;
				
				
				document.getElementById("DocTotalBefore").value = addCommas(obj.intAmount);
				
				document.getElementById("AppliedAmount").value = addCommas(obj.intApplied);
				document.getElementById("BalanceDue").value = addCommas(obj.intBalance);
				if(obj.intWallet!=0)
				{
					document.getElementById("Wallet").value = obj.intWallet;
				}
			
			},
			async: false
		});
		
  }
  
  function initialadd() // fungsi saat tombol add di klik
  {
	  $("#buttonsave").show();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $('#DocNum').attr("disabled", false);
	  $('#BPCode').attr("disabled", false);
	  $('#triggermodalbp').attr("data-toggle", "modal");
	  $('#BPName').attr("disabled", false);
	  $('#RefNum').attr("disabled", false);
	  $('#DocDate').attr("disabled", false);
	  $('#Remarks').attr("disabled", false);
	  
	  $('#AppliedAmount').attr("disabled", false);
	  $('#Wallet').attr("disabled", false);
	  $("#iconcloseheader").hide();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			  
				//load ambil data header
				loadheader(0,0);
			}
	  });
  }
  function initialedit(id) //fungsi saat klik edit header
  {
	  $("#buttonsave").hide();
	  $("#tittlemodaladd").hide();
	  $("#tittlemodaledit").show();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $('#DocNum').attr("disabled", true);
	  $('#BPCode').attr("disabled", true);
	  $('#triggermodalbp').removeAttr("data-toggle");
	  $('#BPName').attr("disabled", true);
	  $('#RefNum').attr("disabled", true);
	  $('#DocDate').attr("disabled", true);
	  $('#Remarks').attr("disabled", true);
	  
	  $('#AppliedAmount').attr("disabled", true);
	  $('#Wallet').attr("disabled", true);
	  $("#iconcloseheader").show();
	  //load detail ambil dari database dan masukkan ke session
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
			  
				//load ambil data header
				loadheader(id,0);
				
					//load html untuk menampilkan detail dari session
					$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');
			}
		});
  }
  function initialduplicate(id) // fungsi saat tombol add di klik
  {
	alert('You cannot use this function on Incoming Payment Modul');
  }
  /*
  
	HEADER FUNCTION
  
  */
  function closeall() //fungsi saat pilih item dengan modal
  {
	  $(".loadertransaction").show();
	  var idHeader = $("#idHeader").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/closeall?", 
			data: "idHeader="+idHeader+"", 
			cache: true, 
			success: function(data){
				//alert(data);
				$('#modal-closeall').modal('hide');
				initialedit(idHeader);
				$("#successalert").show();
				$(".loadertransaction").hide();
			},
			async: false
	  });
  }
  function insertBP(bpcode) //fungsi saat pilih item dengan modal
  {
	  document.getElementById("BPCode").value = bpcode;
	  $('#modal-bp').modal('hide');
	  //$("#RefNum").focus();
	  loadbpcode();
  }
  function addHeader() // proses submit add
  {
		$(".loadertransaction").show();
		var DocNum = $("#DocNum").val();
		var BPCode = $("#BPCode").val();
		var BPName = $("#BPName").val();
		var RefNum = $("#RefNum").val();
		var DocDate = $("#DocDate").val();
		var Remarks = $("#Remarks").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var AppliedAmount = unformat_number($("#AppliedAmount").val());
		var BalanceDue = unformat_number($("#BalanceDue").val());
		var Wallet = $("#Wallet").val();
		
		
		var detail=0;
		var bp=0;
		
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekbp?", 
			data: "BPCode="+BPCode+"", 
			cache: true, 
			success: function(data){ 
				bp=data;
			},
			async: false
		});
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});
		
		if(DocNum=='')
		{
			alert('Please Fill Doc. Number');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(BPCode=='')
		{
			alert('Please Fill Bussiness Partner');
			$("#BPCode").focus();
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(period!=1)
		{
			alert('This Period is locked');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$(".loadertransaction").hide();
		}
		else if(bp==0)
		{
			alert('Bussiness Partner Not Found');
			$("#BPCode").focus();
			$(".loadertransaction").hide();
		}
		else if( (AppliedAmount==0 || AppliedAmount=='0' || AppliedAmount=='') && DocTotalBefore!=0)
		{
			alert('Please Fill Applied Amount');
			$("#AppliedAmount").focus();
			$(".loadertransaction").hide();
		}
		else if(AppliedAmount<0)
		{
			alert('Applied Amount cannot less then 0');
			$("#AppliedAmount").focus();
			$(".loadertransaction").hide();
		}
		else if(BalanceDue<0)
		{
			alert('Balance Due cannot negative');
			$("#AppliedAmount").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd", 
				data: "DocNum="+DocNum+"&BPCode="+BPCode+"&BPName="+BPName+"&RefNum="+RefNum+
				"&DocDate="+DocDate+"&Remarks="+Remarks+"&DocTotalBefore="+DocTotalBefore+"&AppliedAmount="+AppliedAmount+"&BalanceDue="+BalanceDue+"&Wallet="+Wallet+
				"",
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						initialadd();
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  
  
  /*
  
	DETAIL FUNCTION
  
  */
  function addDetail(val,doc)
  {
	  var lfckv = document.getElementById("check"+doc+"").checked;
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
      if(lfckv==true)
	  {
		  
		  var res=parseFloat(DocTotalBefore)+parseFloat(val);
		  document.getElementById("DocTotalBefore").value=addCommas(res);
		  var type='add';
	  }
	  else
	  {
		  var res=parseFloat(DocTotalBefore)-parseFloat(val);
		  document.getElementById("DocTotalBefore").value=addCommas(res);
		  var type='min';
	  }
	  loadbalancedue();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/addDetail?", 
			data: "type="+type+"&doc="+doc+"", 
			cache: true, 
			success: function(data){ 
			}
	  });
  }
  
  function formatnumber()
  {  
	$('#AppliedAmount').mask("#,##0.00", {reverse: true});
  }
  $(function () {
	  
	$(".loadertransaction").hide();
	$(document).bind('keydown', function(e) {// funsgsi shortcut
		if($('#modal-add-edit').hasClass('in')==true)
		{
			  if(e.ctrlKey && (e.which == 83)) { // Ctrl + s
				e.preventDefault();
				if($('#buttonsave').is(":visible")==true)
				{					
					addHeader();
				}
				else if($('#buttonedit').is(":visible")==true)
				{					
					
				}
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 39)) { // Ctrl + right
				e.preventDefault();
				toolbar_next();
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 37)) { // Ctrl + left
				e.preventDefault();
				toolbar_previous();
				return false;
			  }
			  else if(e.which == 27) { // Esc
				e.preventDefault();
				$('#modal-add-edit').modal('hide');
				return false;
			  }
		}
		else
		{
			if(e.which == 78) {
				//$('#modal-add-edit').modal('show');
				//initialadd();
			}
		}
	});
	  
	formatnumber();//saat load awal buat input angka menjadi number format 
	
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	document.getElementById("daterange").value = lastWeekT1+' - '+todayT1;
	
	//fungsi khusus multiple modal agar saat modal-2 di close modal-1 tetap bisa scroll
	$('.modal').on('hidden.bs.modal', function (e) {
		if($('.modal').hasClass('in')) {
		$('body').addClass('modal-open');
		}    
	});
	//end 
	
	//ubah inputan ke format datepiceker
	$('#DocDate').datepicker({
      autoclose: true
    });
	
	//end
	
	
	//fungsi load ajax
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'');
	//end
	
	//fungsi typeahead
	
	$('#BPCode').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPName').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	//end
	$("#example3").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
	
  });
  
 
</script>
</body>
</html>
