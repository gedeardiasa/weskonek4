<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>
  <?php $this->load->view('treeok');?>
<div class="wrapper">
<script type="text/javascript">
    $(document).ready(function() {
		$('.tree').treegrid();
		//$('.tree').treegrid('collapseAll');
        $('.tree').treegrid({
          'initialState': 'expanded',
          'saveState': true,
        });
    });
	
	function expanded()
	{
		$('.tree').treegrid({
          'initialState': 'expanded',
        });
	}
	function collapsed()
	{
		$('.tree').treegrid({
          'initialState': 'collapsed',
        });
	}
</script>
  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form id="demo-form2" method="GET" action="">
				<div class="row">
				
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="ItemCode" class="col-sm-4 control-label" style="height:20px">Item Code</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="<?php echo $ItemCode;?>" id="ItemCode" name="ItemCode" data-toggle="tooltip" data-placement="top" title="Item Code">
							</div>
						</div>
						<div class="form-group">
						  <label for="ItemName" class="col-sm-4 control-label" style="height:20px">Item Name</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" value="<?php echo $ItemName;?>" id="ItemName" name="ItemName" data-toggle="tooltip" data-placement="top" title="Item Name">
							</div>
						</div>
						<div class="form-group">
						  <label for="ItemGroup" class="col-sm-4 control-label" style="height:20px">Item Group</label>
							<div class="col-sm-8" style="height:45px">
							<select id="ItemGroup" name="ItemGroup" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Group">
								<option value="0" >All</option>
								<?php 
								foreach($listgrp->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" <?php if($ItemGroup==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
							</select>
							</div>
						</div>
						<div class="form-group">
						  <label for="daterange" class="col-sm-4 control-label" style="height:20px">Post. Date</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control pull-right" value ="<?php echo $daterange;?>"id="daterange" name="daterange" readonly="true" style="background-color:#ffffff">
							
							</div>
						</div>
						
					</div>
					<div class="col-sm-6">
					<label for="Location" class="col-sm-12 control-label" style="height:20px">Location</label>
					<?php 
					foreach($listwhs->result() as $d) 
					{
					?>
					<div class="col-sm-4">
					<input type="checkbox" name="loc<?php echo $d->vcCode;?>" id="loc<?php echo $d->vcCode;?>"
					<?php if(isset($sloc[$d->vcCode])){echo 'checked=checked';}?>
					><?php echo $d->vcCode;?>-<?php echo $d->vcName;?>
					</div>
					<?php
					}
					?>
					</div>
					
					
				
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                      </div>
                    </div>
				</div>
				</form>
				<hr>
				<table class="table table-striped jambo_table tree" style="font-size:12px">
                <tr>
				  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Sys. Date</th>
				  <th>Post. Date</th>
				  <th>
				  <div class="col-sm-2">Doc</div>
				  <div class="col-sm-2">Type</div>
				  <div class="col-sm-2">Location</div>
				  <div class="col-sm-2">Qty</div>
				  <?php if($uservalue==1){?>
				  <div class="col-sm-2">Value</div>
				  <div class="col-sm-2">Total Value</div>
				  <?php } ?>
				  </th>
				  
                </tr>
				<?php
				$bos=$in+1;
				for($i=1;$i<$in;$i++)
				{
				?>
				<tr class="treegrid-<?php echo $allitemkey[$i]; ?> ">
				  <td><?php echo $allitem[$i]; ?></td>
				  <td><?php echo $allitemname[$i]; ?></td>
				  <td></td>
				  <td></td>
				  <td></td>
				  
                </tr>
					<?php 
					
					foreach($liststock->result() as $d) 
					{
						if($d->ItemCode==$allitem[$i])
						{
					?>
						<tr class="treegrid-<?php echo $bos; ?> <?php echo 'treegrid-parent-'.$allitemkey[$i].''; ?>">
						  <td></td>
						  <td></td>
						  <td><?php echo $d->dtDate; ?></td>
						  <td><?php echo $d->dtPost; ?></td>
						  <td>
						  <div class="col-sm-2"><?php echo $d->vcDoc; ?></div>
						  <div class="col-sm-2"><?php echo $d->vcType; ?> </div>
						  <div class="col-sm-2"><?php echo $d->LocationCode."-".$d->LocationName; ?></div>
						  <div class="col-sm-2" align="right"><?php echo number_format($d->intQty,'2'); ?></div>
						  <?php if($uservalue==1){?>
						  <div class="col-sm-2" align="right"><?php echo number_format($d->intCost,'0'); ?></div>
						  <div class="col-sm-2" align="right"><?php echo number_format($d->intCost*$d->intQty,'0'); ?></div>
						  <?php } ?>
						  </td>
						 
						</tr>
					<?php 
							$bos++;
						}
					}
					?>
				<?php
				}
				?>

              </table>
			  <div align="right">
			   <button type="button" class="btn btn-primary" onclick="expanded()">Expand</button>&nbsp;&nbsp;
			   <button type="button" class="btn btn-primary" onclick="collapsed()">Collapse</button>
			  </div>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	//fungsi typeahead
	$('#ItemName').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
    /*$("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});*/
   
  });
  
 
</script>
</body>
</html>
