<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">

  <div class="modal fade" id="modal-add">
  <form role="form" method="POST" enctype="multipart/form-data"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd/">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Item</h3>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#general_tab" data-toggle="tab">General</a></li>
						<li><a href="#setting_tab" data-toggle="tab">Setting</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="general_tab">
							<div class="row">
							  <div class="col-md-12">
							    <div class="form-group" align="center">
								  <div class="image-upload">
									<label for="Image">
										<img id="blah"  class="profile-user-img img-responsive img-circle" alt="User profile picture" src="<?php echo base_url(); ?>data/general/dist/img/upload.png"/>
									</label>

									<input type="file" id="Image" name="Image" onchange="readURL(this);"><br>
									Upload Image
								  </div>
								  <!--<label for="exampleInputFile">Image</label><br>
								  <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/box.png" width="35px" height="35px" class="img-circle" alt="User Image">
								  <input type="file" id="Image" name="Image" onchange="readURL(this);">-->
								</div>
							  </div>
							  <div class="col-md-6">
								
								<div class="form-group">
								  <label for="Category">Category</label>
								  <select class="" id="Category" name="Category" style="width: 100%; height:35px" onchange="reloadcategory()">
										<?php 
										foreach($listcategory->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
								<div class="form-group">
								  <label for="Code">Code</label>
								  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code" readonly="true">
								</div>
								<div class="form-group">
								  <label for="Name">Name</label>
								  <input type="text" class="form-control" id="Name" name="Name" maxlength="75" required="true" placeholder="Enter Name">
								</div>
								<div class="form-group">
								  <label for="Price">Default Price</label>
								  <input type="number" step="any" class="form-control" id="Price" name="Price" maxlength="25" placeholder="Enter Price">
								</div>
								
								<div class="form-group">
								  <label for="Remarks">Remarks</label>
								  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..."></textarea>
								</div>
								
							  </div>
							  <div class="col-md-6">
								<div class="form-group">
								  <label for="Tax">Tax Group</label>
								  <select class="" id="Tax" name="Tax" style="width: 100%; height:35px">
										<?php 
										foreach($listtax->result() as $d) 
										{
										?>	
										<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
										<?php 
										}
										?>
								   </select>
								</div>
								<div class="form-group">
								  <label for="Barcode">Barcode</label>
								  <input type="text" class="form-control" id="Barcode" name="Barcode" maxlength="50" placeholder="Enter Barcode">
								</div>
								<div class="form-group">
								  <label for="UoM">Inventory UoM</label>
								  <input type="text" class="form-control " id="UoM" name="UoM" maxlength="25" required="true" placeholder="Enter Inventory UoM">
								</div>
								<div class="form-group">
								  <label for="PurUoM" style="padding-left:0px" class="col-sm-8 control-label">Purchase UoM</label>
								  <label for="intPurUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
								  
								  <div class="col-sm-8" style="padding-left:0px">
								  <input type="text" class="form-control" id="PurUoM" name="PurUoM" maxlength="75" placeholder="Enter Purchase UoM">
								  </div>
								  <div class="col-sm-4" style="padding-left:0px">
								  <input type="number" step="any" class="form-control" id="intPurUoM" name="intPurUoM" value="1" maxlength="15" required="true" placeholder="Item Per Purchase Unit">
								  </div>
								</div>
								<div class="form-group">
								  <label for="SlsUoM" style="padding-left:0px" class="col-sm-8 control-label">Sales UoM</label>
								  <label for="intSlsUoM" style="padding-left:0px" class="col-sm-4 control-label">/Numerator</label>
								  
								  <div class="col-sm-8" style="padding-left:0px">
								  <input type="text" class="form-control" id="SlsUoM" name="SlsUoM" maxlength="25" placeholder="Sales UoM">
								  </div>
								  <div class="col-sm-4" style="padding-left:0px">
								  <input type="number" step="any" class="form-control" id="intSlsUoM" name="intSlsUoM" value="1" maxlength="15" required="true" placeholder="Item Per Sales Unit">
								  </div>
								</div>
								
							  </div>
							</div>
						</div>
						<div class="tab-pane" id="setting_tab">
						  <table class="table table-striped dt-responsive jambo_table tree">
							<tr>
								<td style="padding:5px">Sales Item?</td>
								<td style="padding:5px"><input type="checkbox" id="intSalesItem" name="intSalesItem" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Purchase Item?</td>
								<td style="padding:5px"><input type="checkbox" id="intPurchaseItem" name="intPurchaseItem" class="minimal"></td>
							</tr>
							<tr>
								<td style="padding:5px">Allowed to sell goods even though the stock is not available?</td>
								<td style="padding:5px"><input type="checkbox" id="intSalesWithoutQty" name="intSalesWithoutQty" class="minimal"></td>
							</tr>
						  </table>
							  
							  
							  
							
						</div>
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#modal-add"
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Picture</th>
				  <th>Name</th>
				  <th>Code</th>
                  <th>Category</th>
				  <th>Price</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
				  <td>
				  <?php if($d->blpImageMin!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $d->blpImageMin ).'" class="img-circle" style="width:25px;height:25px" alt="Item Image"/>';}else{ ?>
				  <img src="<?php echo base_url(); ?>data/general/dist/img/box.png" class="img-circle" style="width:25px;height:25px" alt="Item Image">
				  <?php } ?>
				  </td>
                  <td>
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <?php echo $d->vcName; ?>
				  </a>
				  <?php }else{ ?>
				  <?php echo $d->vcName; ?>
				  <?php } ?>
				  </td>
                  <td><?php echo $d->vcCode; ?></td>
				  <td><?php echo $d->vcCategory; ?></td>
				  <td><?php echo "IDR ".number_format($d->intPrice,'0'); ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true"></i></a>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	  
	reloadcategory();
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
  
  function reloadcategory()
  {
	  var Category = $("#Category").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "Category="+Category+"",
			cache: true, 
			success: function(data){
				document.getElementById("Code").value = data;
			},
			async: false
	  });
	  
  }
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}
 
</script>
</body>
</html>
