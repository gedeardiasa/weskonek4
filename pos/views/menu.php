<?php 
	$db=$this->load->database('default', TRUE);
	if(!isset($_SESSION[md5('posroot')]))
	{
		$q=$db->query("select blpImage from muser where intID='$_SESSION[IDPOS]'");
		$r=$q->row();
		$imageprofile=$r->blpImage;
	}
	else
	{
		$imageprofile=null;
	}
?> 
<script>
function viewprofile()
{
	window.location.href = "<?php echo $this->config->base_url()?>index.php/welcome/profile";
}

function checkconection() {
	setTimeout(function(){ 
	
	if(navigator.onLine==true)
	{
		$("#onlinestatus").show();
		$("#offlinestatus").hide();
	}
	else
	{
		$("#onlinestatus").hide();
		$("#offlinestatus").show();
	}
	checkconection();
	}, 1000);
}
</script>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image" onclick="viewprofile()">
		  <?php if($imageprofile!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $imageprofile ).'" class="img-circle" alt="User Image"/>';}else{ ?>
          <img src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="img-circle" alt="User Image">
		  <?php } ?>
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['NamePOS']; ?></p>
          <a href="#" id="onlinestatus"><i class="fa fa-circle text-success" ></i> Online</a>
		  <a href="#" id="offlinestatus" style="display:none"><i class="fa fa-circle text-default" ></i> Offline</a>
        </div>
      </div>
      <!-- search form -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
	  
      <ul class="sidebar-menu" data-widget="tree">
        <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="<?php echo base_url(); ?>" onclick="menuloading()">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              
            </span>
          </a>
        </li>
		<?php 
		if(!isset($_SESSION[md5('posroot')]))
		{
			if($form->num_rows()>0)
			{
				foreach($form->result() as $h) 
				{
					$classH="";
					$idheader=$h->FormID;
					$db=$this->load->database('default', TRUE);
					$q=$db->query("SELECT 
					a.`intID`, c.`vcName` AS vcFormName, c.`vcCode` AS vcFormCode, c.`intID` AS FormID, a.intSetup
					FROM maccess a
					LEFT JOIN mform c ON a.`intForm`=c.`intID`
					WHERE c.`IsHeader`=0 AND c.`intDeleted`=0 and c.intHeader='$idheader' and a.intUserID='$_SESSION[IDPOS]'
					order by c.intID
					");
					
					$controller=$this->uri->segment(1);
					$getheadercontroller=$db->query("SELECT b.`vcCode` FROM mform a LEFT JOIN mform b ON a.`intHeader`=b.`intID`
					WHERE a.vcCode='$controller'
					");
					if($getheadercontroller->num_rows()>0)
					{
						$rgetheadercontroller=$getheadercontroller->row();
						if($h->vcFormCode==$rgetheadercontroller->vcCode)
						{
							$classH=" active";
						}
					}
								
			?>
			<li class="treeview<?php echo $classH;?>">
			  <a href="#">
				<?php echo $h->vcIcon;?>
				<span><?php echo $h->vcFormName;?></span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<?php 
				
				foreach($q->result() as $d) 
				{
					
					
					$class="";
					if($this->uri->segment(1)==$d->vcFormCode)
					{
						$class='active';
					}
				?>
				<li class="<?php echo $class;?>"><a href="<?php echo base_url(); ?>index.php/<?php echo $d->vcFormCode?>" onclick="menuloading()"><i class="fa fa-circle-o"></i> <?php echo $d->vcFormName;?></a></li>
				<?php 
				}
				?>
			  </ul>
			</li>
			<?php 
				}
			}
		}
		else
		{
		?>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-gear"></i>
				<span>Root</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/client"><i class="fa fa-circle-o"></i> Client</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/user"><i class="fa fa-circle-o"></i> User</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/plan"><i class="fa fa-circle-o"></i> Plan</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/dashboard"><i class="fa fa-circle-o"></i> Dashboard</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/form"><i class="fa fa-circle-o"></i> Form</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/topform"><i class="fa fa-circle-o"></i> Top Form</a></li>
				<li class=""><a href="<?php echo base_url(); ?>index.php/admin/subform"><i class="fa fa-circle-o"></i> Sub Form</a></li>
			  </ul>
			</li>
		
		
		<?php
		}
		?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
