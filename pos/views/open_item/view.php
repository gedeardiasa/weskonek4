<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>

<div class="wrapper">

  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>
	
    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form id="demo-form2" method="GET" action="">
				<div class="row">
				
					<div class="col-sm-6">
						
						<div class="form-group">
						  <label for="ItemGroup" class="col-sm-4 control-label" style="height:20px">Document</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Document" name="Document" class="form-control" data-toggle="tooltip" data-placement="top" title="Document">
								<option value="0" >All</option>
								<option value="SQ" <?php if($Document=='SQ') { echo "selected";}?>>Sales Quotation</option>
								<option value="SO" <?php if($Document=='SO') { echo "selected";}?>>Sales Order</option>
								<option value="DN" <?php if($Document=='DN') { echo "selected";}?>>Delivery</option>
								<option value="SR" <?php if($Document=='SR') { echo "selected";}?>>Sales Return</option>
								<option value="AR" <?php if($Document=='AR') { echo "selected";}?>>A/R Invoice</option>
								<option value="ARCM" <?php if($Document=='ARCM') { echo "selected";}?>>A/R Credit Memo</option>
								
								<option value="PO" <?php if($Document=='PO') { echo "selected";}?>>Purchase Order</option>
								<option value="GRPO" <?php if($Document=='GRPO') { echo "selected";}?>>Good Receipt From PO</option>
								<option value="PR" <?php if($Document=='PR') { echo "selected";}?>>Purchase Return</option>
								<option value="AP" <?php if($Document=='AP') { echo "selected";}?>>A/P Invoice</option>
								<option value="APCM" <?php if($Document=='APCM') { echo "selected";}?>>A/P Credit Memo</option>
								
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </div>
				</div>
				</form>
				<hr>
				
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
				  <th>Doc. Num</th>
				  <th>BP Name</th>
				  <th>Date</th>
                  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Qty</th>
				  <th>Open Qty</th>
				  <th>UoM</th>
				  <th>Price</th>
				  <th>Line Total</th>
				  <th>Doc. Num</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->vcDocNum; ?></td>
				  <td><?php echo $d->vcBPName; ?></td>
				  <td><?php echo $d->dtDate; ?></td>
				  <td><?php echo $d->vcItemCode; ?></td>
				  <td><?php echo $d->vcItemName; ?></td>
				  <td align="right"><?php echo number_format($d->intQty,'2'); ?></td>
				  <td align="right"><?php echo number_format($d->intOpenQty,'2'); ?></td>
				  <td><?php echo $d->vcUoM; ?></td>
				  <td align="right"><?php echo number_format($d->intPrice,'0'); ?></td>
				  <td align="right"><?php echo number_format($d->intLineTotal,'0'); ?></td>
				  <td><?php echo $d->vcDocNum; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
<script>
  


  $(function () {
	
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
