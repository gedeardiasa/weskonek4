<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>
  <?php $this->load->view('treeok');?>
<div class="wrapper">
<script type="text/javascript">
    $(document).ready(function() {
		$('.tree').treegrid();
		//$('.tree').treegrid('collapseAll');
        $('.tree').treegrid({
          'initialState': 'expanded',
          'saveState': true,
        });
    });
	
	function expanded()
	{
		$('.tree').treegrid({
          'initialState': 'expanded',
        });
	}
	function collapsed()
	{
		$('.tree').treegrid({
          'initialState': 'collapsed',
        });
	}
</script>
  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
				<form id="demo-form2" method="GET" action="">
				<div class="row">
				
					<div class="col-sm-6">
						
						<div class="form-group">
						  <label for="daterange" class="col-sm-4 control-label" style="height:20px">Post. Date</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control pull-right" value ="<?php echo $daterange;?>"id="daterange" name="daterange" readonly="true" style="background-color:#ffffff">
							
							</div>
						</div>
						<div class="form-group">
						  <label for="plan" class="col-sm-4 control-label" style="height:20px">Plan</label>
							<div class="col-sm-8" style="height:45px">
							<select class="" id="plan" name="plan" style="width: 100%; height:35px">
							<?php if($haveaccessallplan==1){?><option value="0" >All</option><?php }?>
							<?php 
							foreach($listplan->result() as $d) 
							{
							?>	
							<option value="<?php echo $d->intID;?>" <?php if($plan==$d->intID){ echo "selected";}?>><?php echo $d->vcName;?></option>
							<?php 
							}
							?>
							</select>
							
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row">
					<div class="form-group" align="left">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                      </div>
                    </div>
				</div>
				</form>
				<hr>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
				  <th>Account</th>
				  <th>Balance</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				$profit=0;
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td><?php echo $d->Account; ?></td>
				  <td align="right"><?php echo number_format($d->val,'0'); ?></td>
                </tr>
				<?php 
					$profit=$profit+$d->val;
				}
				?>
                </tbody>
				<tfoot>
                <tr>
				  <td><b>Profit</b></td>
				  <td align="right"><b><?php echo number_format($profit,'0'); ?></b></td>
                </tr>
                </tfoot>
              </table>
			 
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
	
	
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
    /*$("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});*/
   
  });
  
 
</script>
</body>
</html>
