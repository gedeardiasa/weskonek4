<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>
<style>

.modal-dialog {
  width: 90%;
 
}
</style>
<div class="wrapper">
  <?php $this->load->view('selectbp'); ?>
  <?php $this->load->view('selectwhs'); ?>
  <?php $this->load->view('header'); ?>
  
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>
	<?php $this->load->view('toolbar/print/html'); ?>
	<div class="modal fade" id="modal-so">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">SO List</h3>
            </div>
            
              <div class="box-body">
				<div id="listso"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <div class="modal fade" id="modal-qty">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
			<h3 class="box-title">Detail</h3>
            </div>
            
              <div class="box-body">
				<div class="" id="form-adddetail2"  style="padding-bottom:20px">
				<div class="col-sm-5" style="padding-left:0px;padding-bottom:10px">
					  <input type="hidden" name="IDPO" id="IDPO">
					  <div class="form-group">
						<label for="detailItem2">Item Name</label>
						<input type="text" class="form-control inputenter" id="detailItem2" name="detailItem2" maxlength="100" required="true" 
						placeholder="Select Item Name" onchange="isiitem()" onpaste="isiitem()" readonly="true" data-toggle="tooltip" data-placement="top" title="Item Name">
					  </div>
					  
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <div class="form-group">
						<label for="detailQty2">Quantity</label>
						<input type="number" step="any" class="form-control inputenter" id="detailQty2" name="detailQty2"
						maxlength="15" required="true" data-toggle="tooltip" data-placement="top" title="Quantity"
						placeholder="Qty">
					  </div>
					  
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <div class="form-group">
						<label for="detailPrice2">Price</label>
						<input type="number" step="any" class="form-control inputenter" id="detailPrice2" name="detailPrice2" maxlength="15" required="true" 
						placeholder="Price" data-toggle="tooltip" data-placement="top" title="Price">
					  </div>
					  
				</div>
				<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
					  <div class="form-group">
						<label for="detailDisc2">Discount</label>
						<input type="number" step="any" class="form-control inputenter" id="detailDisc2" name="detailDisc2" maxlength="3" required="true" 
						placeholder="Discount" data-toggle="tooltip" data-placement="top" title="Discount">
					  </div>
					  
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <div class="form-group">
						<label for="detailUoM2">UoM</label>
						<input type="text" readonly="true" step="any" class="form-control inputenter" id="detailUoM2" name="detailUoM2" maxlength="15" required="true" 
						placeholder="UoM" data-toggle="tooltip" data-placement="top" title="UoM">
					  </div>
					  
				</div>
				
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="button" class="btn btn-primary" onclick="addDetail()">Save</button>
         </div>
       </div>
     </div>
   </div>
   <div class="modal fade" id="modal-pay">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Payment</h3>
            </div>
            
              <div class="box-body">
				<div class="row">
				  <div class="col-sm-4">
					<div class="form-group">
					  <label for="TotalPay">Wallet</label>
					  <div id="listwallet"></div>
					</div>
					<div class="form-group">
					  <label for="TotalPay">Total</label>
					  <input type="text" readonly="true" style="text-align:right;" class="form-control" id="TotalPay" name="TotalPay" maxlength="25" required="true">
					</div>
					<div class="form-group">
					  <label for="AmmountTendered">Ammount Tendered</label>
					  <input type="text" class="form-control" id="AmmountTendered" style="text-align:right;" name="AmmountTendered" maxlength="25" required="true" 
					  onchange="changeChange()" onpaste="changeChange()" onclick="changeChange()" onkeyup="changeChange()" readonly="true" >
					</div>
					<div class="form-group">
					  <label for="Change" id="labelChange">Remaining</label>
					  <input type="text" readonly="true" style="text-align:right;" class="form-control" id="Change" name="Change" maxlength="25" required="true">
					</div>
					<div class="form-group">
					  <label for="PaymentNote">Payment Note</label>
					  <input type="text" class="form-control" id="PaymentNote" name="PaymentNote" maxlength="25" required="true">
					</div>
					<div class="form-group">
					  <label for="PaymentCode">Payment Code</label>
					  <input type="text" class="form-control" id="PaymentCode" name="PaymentCode" maxlength="25" required="true">
					</div>
				  </div>
				  <div class="col-sm-4">
				    <div class="col-sm-12">
					  <div class="col-sm-3">
					
					  </div>
					  <div class="col-sm-3">
					
					  </div>
					  <div class="col-sm-3">
					
					  </div>
					  <div class="col-sm-3">
					
					  </div>
					</div>
					
					<table>
					  <tr>
						<td><button onclick="isiAmmount('1')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">1</button></td>
						<td><button onclick="isiAmmount('2')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">2</button></td>
						<td><button onclick="isiAmmount('3')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">3</button></td>
						<td><button onclick="isiAmmount('x')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">X</button></td>
					  </tr>
					  <tr>
						<td><button onclick="isiAmmount('4')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">4</button></td>
						<td><button onclick="isiAmmount('5')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">5</button></td>
						<td><button onclick="isiAmmount('6')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">6</button></td>
						<td><button onclick="isiAmmount('c')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">C</button></td>
					  </tr>
					  <tr>
						<td><button onclick="isiAmmount('7')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">7</button></td>
						<td><button onclick="isiAmmount('8')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">8</button></td>
						<td><button onclick="isiAmmount('9')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">9</button></td>
						<td><button onclick="isiAmmount('+-')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">+/-</button></td>
					  </tr>
					  <tr>
						<td><button onclick="isiAmmount('.')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">.</button></td>
						<td><button onclick="isiAmmount('0')" style="margin:5px; width:55px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">0</button></td>
						<td colspan="2"><button style="margin:5px; width:120px; height:55px" type="button" class="btn bg-grey btn-flat margin"onclick="">Done</button></td>
					  </tr>
					</table>
				  
				  </div>
				  <div class="col-sm-4">
						<button onclick="isiAmmount2('1000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 1.000
						</button>
						<button onclick="isiAmmount2('2000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 2.000
						</button>
						<button onclick="isiAmmount2('5000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 5.000
						</button>
						<button onclick="isiAmmount2('10000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 10.000
						</button>
						<button onclick="isiAmmount2('20000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 20.000
						</button>
						<button onclick="isiAmmount2('50000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 50.000
						</button>
						<button onclick="isiAmmount2('100000')" style="margin:5px; width:100%; height:55px; font-size:30px" type="button" class="btn bg-grey btn-flat margin"onclick="">
						IDR 100.000
						</button>
				  
				  </div>
				</div>
			  </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  id="buttonsaveheader">Save</button>
         </div>
       </div>
     </div>
   </div>
    <!-- Main content -->
    <section class="content">


	<div class="box">
      <div class="row">
		
		<div class="col-sm-5">
			<div class="alert alert-success alert-dismissible" id="successalert" style="display:none">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="icon fa fa-check"></i>Success. You successfully process the data
			</div>
			<div class="alert alert-danger  alert-dismissible" id="dangeralert" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="icon fa fa-ban"></i>Error. You failed to process the data
			</div>
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				<?php 
				$x=0;
				foreach($autoitemcategory->result() as $au)
				{
				?>
				<li <?php if($x==0){ echo 'class="active"';}?>><a href="#<?php echo str_replace(' ', '', $au->vcName);?>" data-toggle="tab"><?php echo $au->vcName;?></a></li>
				<?php
				$x++;
				}
				?>
				</ul>
				<div class="tab-content">
					<?php 
					$y=0;
					foreach($autoitemcategory->result() as $au)
					{
					?>
					<div class="<?php if($y==0){ echo 'active';}?> tab-pane" id="<?php echo str_replace(' ', '', $au->vcName);?>">
						<div class="box-body" style="overflow:scroll; height:400px;">
						<?php 
						
						$changecolor=0;
						foreach($autoitem->result() as $ai)
						{
							
							if($ai->intCategory==$au->intID)
							{
						?>
							<div class="col-sm-4 col-xs-6" align="center" style="padding:0px; margin:0px" onclick="insertItem('<?php echo $ai->vcName;?>')">
							    <button type="button" class="btn btn-block btn-default" style="padding:0px; margin:0px; width:100%">
								<div align="center"  style="height:37px"><?php echo $ai->vcName;?>
								</div>
								
								<?php if($ai->blpImageMin!=null){echo '<img align="center" src="data:image/jpeg;base64,'.base64_encode( $ai->blpImageMin ).'" style="width:100%;height:75px" class="col-sm-12 img-circle" alt="Item Image"/>';}else{ ?>
								<img align="center" src="<?php echo base_url(); ?>data/general/dist/img/box.png" class="col-sm-12 img-circle" style="width:100%;height:75px" alt="Item Image">
								<?php } ?>
								</button>
							</div>
						<?php
							}
						//}
						}
						?>
						</div>
					</div>
					<?php
					$y++;
					}
					?>
						
				</div>
			</div>
		</div>
		<div class="col-sm-7" style="padding-top:10px">
		<?php $this->load->view('loadingbar2'); ?>
			<button type="button" class="btn btn-block btn-default btn-xs" id="showdetail" onclick="showorhideformdetail()">Show Form Detail</button>
			<div class="" id="form-adddetail"  style="padding-bottom:20px; display:none">
				<b><i>Form Detail</i></b><br>
				<div class="col-sm-12" style="padding-left:0px;padding-bottom:10px">
				<div class="input-group">
					<div class="input-group-addon" id="groupbarcode">
					<i class="fa fa-barcode" aria-hidden="true"></i>
					</div>
					<input type="text" class="form-control inputenter" id="detailBarcode" name="detailBarcode" maxlength="100" 
					  placeholder="Barcode" readonly="true" onchange="isibarcode()" data-toggle="tooltip" data-placement="top" title="Barcode">
				</div>
				
				</div>
				<div class="col-sm-12" style="padding-left:0px;padding-bottom:10px">
				<input type="checkbox" class="flat" id="detailUseBarcode" name="detailUseBarcode" onclick="usebarcode()"> Use Barcode ?
				<input type="checkbox" class="flat" id="detailEnableEdit" name="detailEnableEdit"> Enable Edit ?
				</div>
				<div class="col-sm-5" style="padding-left:0px;padding-bottom:10px">
					  <input type="hidden" name="IDSO" id="IDSO">
					  <input type="text" class="form-control inputenter" id="detailItem" name="detailItem" maxlength="100" required="true" 
					  placeholder="Select Item Name" onchange="isiitem()" onpaste="isiitem()" data-toggle="tooltip" data-placement="top" title="Item Name">
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <input type="number" step="any" class="form-control inputenter" id="detailQty" name="detailQty"
					  maxlength="15" required="true" data-toggle="tooltip" data-placement="top" title="Quantity"
					  placeholder="Qty">
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <input type="number" step="any" class="form-control inputenter" id="detailPrice" name="detailPrice" maxlength="15" required="true" 
					  placeholder="Price" data-toggle="tooltip" data-placement="top" title="Price">
				</div>
				<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
					  <input type="number" step="any" class="form-control inputenter" id="detailDisc" name="detailDisc" maxlength="3" required="true" 
					  placeholder="Discount" data-toggle="tooltip" data-placement="top" title="Discount">
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					  <input type="text" readonly="true" step="any" class="form-control inputenter" id="detailUoM" name="detailUoM" maxlength="15" required="true" 
					  placeholder="UoM" data-toggle="tooltip" data-placement="top" title="UoM">
				</div>
				<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
					<button type="button" class="btn btn-primary" id="buttonadddetail" onclick="addDetail()">Add</button>
				</div>
			</div>
			<table id="mix_detail2" class="table table-striped dt-responsive jambo_table" width="100%"></table>
			<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
				<tbody>
					<tr>
						<td colspan="3"><b>Total</b></td>
						<td align="right"><input type="text" style="align:right; text-align:right" readonly="true"
						data-toggle="tooltip" data-placement="top" title="Total"
						name="DocTotalBefore" id="DocTotalBefore"></td>
						<td></td>
					</tr>
					
					<tr>
						<td colspan="3"><b>Disc</b></td>
						<td align="right"><input type="number" style="width:50px" name="DiscPer" id="DiscPer" placeholder="%" data-toggle="tooltip" 
						onchange="loaddisc()" onpaste="loaddisc()" onkeyup="loaddisc()" onclick="loaddisc()" data-placement="top" title="Discount %">
						<input type="text" name="Disc" id="Disc" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Discount"></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3"><b>Freight</b></td>
						<td align="right">
						<input type="text" id="Freight" name="Freight" style="text-align:right;" 
							data-toggle="tooltip" data-placement="top" title="Freight"
							onchange="loadgrandtotal()" onpaste="loadgrandtotal()" onkeyup="loadgrandtotal()" onclick="loadgrandtotal()"></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3"><b>Tax</b></td>
						<td align="right">
						<select id="TaxPer" name="TaxPer" data-toggle="tooltip" data-placement="top" title="Tax %" onchange="loadtax()">
						<?php 
						foreach($listtax->result() as $d) 
						{
							?>
		
							<option value="<?php echo $d->intRate; ?>"><?php echo $d->vcName;?></option>
						<?php
						}	
						?>
						</select>
						
						<input type="text" name="Tax" id="Tax" readonly="true" data-toggle="tooltip" data-placement="top" title="Tax" style="text-align:right;" ></td>
						<td></td>
					</tr>
					
					<tr>
						<td colspan="3"><b>Grand Total</b></td>
						<td align="right">
						<input type="text" name="DocTotal" id="DocTotal" readonly="true" style="text-align:right;" data-toggle="tooltip" data-placement="top" title="Grand Total"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
				<div class="col-sm-6">
					<div class="input-group">
						<div class="input-group-addon" id="triggermodalbp" data-toggle="modal" data-target="#modal-bp">
						<i class="fa fa-users" aria-hidden="true" style="width:25px" ></i>
						</div>
						<input type="text" class="form-control" id="BPName" name="BPName" placeholder="Bussiness Partner Name" 
						value="<?php echo $defaultbp;?>" data-toggle="tooltip" data-placement="top" title="Bussiness Partner Name" onchange="isibp()" onpaste="isibp()">
					</div>
					<div class="input-group">
						<div class="input-group-addon" id="triggermodalbp" data-toggle="modal" data-target="#modal-whs">
						<i class="fa fa-home" aria-hidden="true" style="width:25px" ></i>
						</div>
						<input type="text" class="form-control" id="WhsName" name="WhsName" 
						value="<?php echo $defaultwhs;?>" placeholder="Warehouse" readonly="true" data-toggle="tooltip" data-placement="top" title="Warehouse">
					</div>
					
					<div class="form-group">
						<select class="form-control" id="Table" name="Table" data-toggle="tooltip" data-placement="top" title="Table">
						<?php 
						foreach($listtable->result() as $d) 
						{
							?>
		
							<option value="<?php echo $d->intID; ?>"><?php echo $d->vcName;?></option>
						<?php
						}	
						?>
						</select>
					</div>
				</div>
				<div class="col-sm-6" style="margin:0px; padding:0px; padding-right:15px">
						<div class="col-sm-6" style="margin:0px; padding:0px">
						<button style="margin:0px; width:100%"type="button" class="btn btn-flat btn-success" id="buttonsave" onclick="saveso()">SAVE SO</button>
						<button style="margin:0px; width:100%; display:none"type="button" class="btn btn-flat btn-success" id="buttonedit" onclick="editso()">EDIT SO</button>
						
						</div>
						<div class="col-sm-6" style="margin:0px; padding:0px">
						<button style="margin:0px; width:100%"type="button" id="buttonopenso" class="btn btn-flat btn-info"
						onclick="copyfrom('SO')">OPEN SO</button>
						</div>
						<div class="col-sm-12" style="margin:0px; padding:0px">
						<button style="margin:0px; width:100%"type="button" id="buttonpay" onclick="payment()" class="btn btn-flat btn-primary btn-lg">PAY</button>
						</div>
						
						<br>
						<br><br>
						<br><br>
						<br><br>
				</div>
		</div>
	  
	  </div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  
  <?php $this->load->view('T2'); ?>
  <?php $this->load->view('localdata/item'); ?>
  <?php $this->load->view('localdata/disc'); ?>
  <?php $this->load->view('localdata/bp'); ?>
<script>
  
  var globalDocTotalBefore = '0';
  var globalDocTotalBeforeInt = 0;
  var dataSet;
  var ddidDPOS=[];
  var ddidBaseRef=[];
  var ddBaseRef=[];
  var ddItem=[];
  var ddQty=[];
  var ddPrice=[];
  var ddDisc=[];
  var perluGR=[];
	
  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode === 13) //shortcut enter pada detail data untuk menambah data detail
	{
		var detailBarcode = $("#detailBarcode").val();
		var x = document.getElementById("detailEnableEdit").checked;
		if(detailBarcode=='' && x==false)
		{
			addDetail();
		}
    }
  
  });
  /*
  
	FUNGSI COPY FROM
  
  */
  function copyfrom(val)
  {
	  $("#successalert").hide();
	  if(val=='SO')
	  {
		  $('#listso').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/listso/');
		  $('#modal-so').modal('show');
	  }
  }
  function usebarcode()
  {
	  $("#successalert").hide();
	  var detailUseBarcode = document.getElementById("detailUseBarcode").checked;
	  if(detailUseBarcode==true)
	  {
		  document.getElementById('detailBarcode').readOnly = false;
		  $("#detailBarcode").focus();
	  }
	  else
	  {
		  document.getElementById('detailBarcode').readOnly = true;
	  }
  }
  function payment()
  {
	  var DocTotal = $("#DocTotal").val();
	  var WhsName = $("#WhsName").val();
	  var loc=0;
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekloc?", 
			data: "WhsName="+WhsName+"", 
			cache: true, 
			success: function(data){ 
				loc=data;
			},
			async: false
	  });
	  if(loc==0)
	  {
		  alert('Location Not Found');
		  $("#WhsName").focus();
	  }
	  else
	  {
		  document.getElementById("TotalPay").value = DocTotal;
		  document.getElementById("Change").value =addCommas(unformat_number($("#DocTotal").val()));
		  
		  $("#AmmountTendered").focus();
		  document.getElementById("AmmountTendered").value = '';
		  $('#modal-pay').modal('show');
		  changeChange();
	  }
  }
  function isiAmmount(val)
  {
	  if(val=='c')
	  {
		  document.getElementById("AmmountTendered").value = '';
	  }
	  else if(val=='x')
	  {
		  var AmmountTendered = $("#AmmountTendered").val();
		  var count = AmmountTendered.length;
		  var nextcount=count-1;
		  var res = AmmountTendered.substring(0, nextcount);
		  document.getElementById("AmmountTendered").value = res;
	  }
	  else if(val=='+-')
	  {
		  var AmmountTendered = $("#AmmountTendered").val();
		  var res=AmmountTendered*-1;
		  document.getElementById("AmmountTendered").value = res;
	  }
	  else
	  {
		var AmmountTendered = $("#AmmountTendered").val();
		AmmountTendered=AmmountTendered+val;
		document.getElementById("AmmountTendered").value = AmmountTendered;
	  }
	  changeChange();
  }
  function isiAmmount2(val)
  {
	  if($("#AmmountTendered").val()=='')
	  {
		  var AmmountTendered = 0;
	  }
	  else
	  {
		var AmmountTendered = $("#AmmountTendered").val();
	  }
	  document.getElementById("AmmountTendered").value = parseInt(AmmountTendered)+parseInt(val);
	  changeChange();
  }
  function changeChange()
  {
	  if($("#AmmountTendered").val()=='')
	  {
		  var AmmountTendered = 0;
	  }
	  else
	  {
		var AmmountTendered = unformat_number($("#AmmountTendered").val());
	  }
	  var DocTotal = unformat_number($("#DocTotal").val());
	  
	  if((DocTotal-AmmountTendered<0))
	  {
		  var elem = document.getElementById("Change");
		  elem.style.color = "Green";
		  document.getElementById("labelChange").innerHTML="Change";
		  document.getElementById("Change").value =addCommas((DocTotal-AmmountTendered)*-1);
	  }
	  else
	  {
		  var elem = document.getElementById("Change");
		  elem.style.color = "Red";
		  document.getElementById("labelChange").innerHTML="Remaining";
		  document.getElementById("Change").value =addCommas(DocTotal-AmmountTendered);
	  }
  }
  function insertSO(id) //fungsi saat pilih item dengan modal
  {
	  emptyform();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetailso?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				var obj = JSON.parse(data);
				//alert(obj.rows.length);
				//alert(obj.rows[0].itemnamePOS);
				for(var i=0;i<obj.rows.length;i++)
				{
				  ddidDPOS[i]=obj.rows[i].idDPOS;
				  ddidBaseRef[i]=obj.rows[i].idBaseRefAR;
				  ddBaseRef[i]=obj.rows[i].BaseRefAR;
				  perluGR[i]='';
				  ddItem[i]=obj.rows[i].itemnamePOS;
				  ddQty[i]=obj.rows[i].qtyPOS;
				  ddPrice[i]=obj.rows[i].pricePOS;
				  ddDisc[i]=obj.rows[i].discPOS;
				}
				reloaddetail();
				$('#modal-so').modal('hide');
				loadheaderso(id);
				$("#buttonedit").show();
				$('#BPName').attr("disabled", true);
				$("#buttonsave").hide();
				
			}
		});
  }
  /*
  
	FUNGSI LOAD
  
  */
  function showorhideformdetail()
  {
	var element = document.getElementById('form-adddetail');
    style = window.getComputedStyle(element);
	display = style.display;
	if(display=='block')
	{
		document.getElementById('form-adddetail').style.display = 'none';
		document.getElementById("showdetail").innerHTML = "Show Form Detail";
	}
	else
	{
		document.getElementById('form-adddetail').style.display = 'block';
		document.getElementById("showdetail").innerHTML = "Hide Form Detail";
	}
  }
  function isibp()
  {
	  loadtaxgroup();
	  loadprice();
  }
  function loadheaderso(id)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderSO?", 
			data: "id="+id+"",
			cache: true, 
			success: function(data){
				var obj = JSON.parse(data);
				document.getElementById("IDSO").value = obj.idHeader;
				document.getElementById("BPName").value = obj.BPName;
				document.getElementById("WhsName").value = obj.WhsName;
				document.getElementById("Table").value = obj.Table;
				//document.getElementById("DocTotalBefore").value = addCommas(obj.DocTotalBefore);
				document.getElementById("DiscPer").value = addCommas(obj.DiscPer);
				//document.getElementById("Disc").value = addCommas(obj.Disc);
				document.getElementById("Freight").value = addCommas(obj.Freight);
				document.getElementById("TaxPer").value = obj.TaxPer;
				//document.getElementById("Tax").value = addCommas(obj.Tax);
				//document.getElementById("DocTotal").value = addCommas(obj.DocTotal);
				loadtotal();
			}
		});
  }
  function loadprice() //untuk reload harga per item
  {
	   var BPName = $("#BPName").val();
	   var detailItem = $("#detailItem").val();
		// ambil data price pengganti ajax agar tidak lambat
		for(var i=0;i<=totalprice;i++)
		{
			if(allprice_BPName[i]==BPName && allprice_detailItem[i]==detailItem)
			{
				document.getElementById("detailPrice").value = allprice_Price[i];
				document.getElementById("detailPrice2").value = allprice_Price[i];
			}
		}
  }
  function loaduom() //untuk reload harga per item
  {
	   var detailItem = $("#detailItem").val();
	   // ambil data uom pengganti ajax agar tidak lambat
		for(var i=0;i<=totaluom;i++)
		{
			if(alluom_detailItem[i]==detailItem)
			{
				document.getElementById("detailUoM").value = alluom_UoM[i];
				document.getElementById("detailUoM2").value = alluom_UoM[i];
			}
		}
  }
  function getdisc() //untuk reload harga per item
  {
	   var detailItem = $("#detailItem").val();
	   var WhsName = $("#WhsName").val();
		for(var i=0;i<=totaldisc;i++)
		{
			if(alldisc_Location[i]==WhsName && alldisc_detailItem[i]==detailItem)
			{
				document.getElementById("detailDisc").value = alldisc_Disc[i];
				document.getElementById("detailDisc2").value = alldisc_Disc[i];
			}
		}
  }
  function loadtotal()
  {
		globalDocTotalBefore=globalDocTotalBeforeInt.toString();
		document.getElementById("DocTotalBefore").value= addCommas(globalDocTotalBefore);
		loaddisc();
		loadgrandtotal();
		$(".loadingbar2").hide();
		$(':button').prop('disabled', false);
  }
  function loaddisc() //untuk reload disc
  {
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  var DiscPer = $("#DiscPer").val();
	  if(DiscPer=='')
	  {
		  var DiscPer = 0;
	  }
	  document.getElementById("Disc").value = addCommas(DiscPer/100*DocTotalBefore);
	  loadgrandtotal();
	  loadtax();
  }
  function loadtax() //untuk reload tax
  {
	  var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
	  var Disc = unformat_number($("#Disc").val());
	  var TaxPer = $("#TaxPer").val();
	  document.getElementById("Tax").value = addCommas(TaxPer/100*(DocTotalBefore-Disc));
	  loadgrandtotal();
  }
  function loadgrandtotal() //untuk reload grand total
  {  
	  var Disc = unformat_number($("#Disc").val());
	  var Tax = unformat_number($("#Tax").val());
	  var Fr=$("#Freight").val();
	  if(Fr=='')
	  {
		  var Freight=0;
	  }
	  else
	  {
		  var Freight = unformat_number(Fr);
	  }
	  var DocTotalBefore=unformat_number(globalDocTotalBefore);
	  
	  document.getElementById("DocTotal").value = addCommas(DocTotalBefore-Disc+Freight+Tax);
  }
  function loadtaxgroup()
  {
	  var BPName = $("#BPName").val();
	  for(var i=0;i<=totaltax;i++)
		{
			if(alltax_BPName[i]==BPName)
			{
				document.getElementById("TaxPer").value = alltax_Rate[i];
			}
		}
		loadtax();
  }
  function isiitem() // fungsi saat ketik field item
  {
		$("#successalert").hide();
	    $("#dangeralert").hide();
		loadprice();
		loaduom();
		getdisc();
  }
  function isibarcode()
  {
	  var detailBarcode = $("#detailBarcode").val();
	  document.getElementById("detailBarcode").value ='';
	  var data = '';
	  for(var i=0;i<=totalitem;i++)
	  {
			if(allitem_detailBarCode[i]==detailBarcode)
			{
				data = allitem_detailItem[i];
			}
	  }
	  var detailItem = $("#detailItem").val();
	  var detailQty = $("#detailQty").val();
	  var detailPrice = $("#detailPrice").val();
			
	  if(detailItem!='' && detailQty!='' && detailPrice!='' && detailItem!=data)
	  {
		addDetail();
	  }
	  var detailItem=$("#detailItem").val();
	  if($("#detailQty").val()=='' || detailItem!=data)
	  {
		var detailQty=0;
	  }
	  else
	  {
		var detailQty=$("#detailQty").val();
	  }
			document.getElementById("detailItem").value = data;
			document.getElementById("detailItem2").value = data;
			document.getElementById("detailQty").value = parseInt(detailQty)+1;
			//document.getElementById('detailQty').select();
			isiitem();
  }
  
  /*
  
	HEADER FUNCTION
  
  */
  function loadWallet(WhsName)
  {
	  WhsName = encodeURIComponent(WhsName);
	  var url = '<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/listwallet?WhsName='+WhsName+'';
	  $('#listwallet').load(url);
  }
  function insertBP(BPCode,BPName) //fungsi saat pilih item dengan modal
  {
	  document.getElementById("BPName").value = BPName;
	  loadtaxgroup();
	  $('#modal-bp').modal('hide');
  }
  function insertWhs(WhsName) //fungsi saat pilih item dengan modal
  {
	  document.getElementById("WhsName").value = WhsName;
	  loadWallet(WhsName);
	  $('#modal-whs').modal('hide');
  }
  function addHeader()
  {
		var BPName = $("#BPName").val();
		var WhsName = $("#WhsName").val();
		var Table = $("#Table").val();
		
		var PaymentNote = $("#PaymentNote").val();
		var PaymentCode = $("#PaymentCode").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var DiscPer = unformat_number($("#DiscPer").val());
		var Disc = unformat_number($("#Disc").val());
		var Freight = unformat_number($("#Freight").val());
		var TaxPer = unformat_number($("#TaxPer").val());
		var Tax = unformat_number($("#Tax").val());
		var DocTotal = unformat_number($("#DocTotal").val());
		var gddidDPOS=JSON.stringify(ddidDPOS);
		var gddidBaseRef=JSON.stringify(ddidBaseRef);
		var gddBaseRef=JSON.stringify(ddBaseRef);
		var gddItem=JSON.stringify(ddItem);
		var gddQty=JSON.stringify(ddQty);
		var gddPrice=JSON.stringify(ddPrice);
		var gddDisc=JSON.stringify(ddDisc);
		var gperluGR=JSON.stringify(perluGR);
			
		
		var Wallet = $("#Wallet").val();
		
		if($("#AmmountTendered").val()=='')
		{
			var AmmountTendered=0;
		}
		else
		{
			var AmmountTendered = unformat_number($("#AmmountTendered").val());
		}
		var Change = unformat_number($("#Change").val());
		
		var detailstock=0;
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetailstok?", 
			data: "WhsName="+WhsName+
			"&ddidDPOS="+gddidDPOS+"&ddidBaseRef="+gddidBaseRef+"&ddBaseRef="+gddBaseRef+"&ddItem="+gddItem+"&ddQty="+gddQty+"&ddPrice="+gddPrice+"&ddDisc="+gddDisc+"&perluGR="+gperluGR+"",
			cache: true, 
			success: function(data){ 
				detailstock=data;
			},
			async: false
		});
		
		if(DocTotal==0)
		{
			alert('Doc. Total is 0');
		}
		else if(DocTotal>AmmountTendered)
		{
			alert('Ammount Tendered must more than DocTotal');
		}
		else if(detailstock!=1)
		{
			alert(detailstock);
			//$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosessavepayment", 
				data: "BPName="+BPName+"&WhsName="+WhsName+"&Table="+Table+"&DocTotalBefore="+DocTotalBefore+
				"&DiscPer="+DiscPer+"&Disc="+Disc+"&Freight="+Freight+"&TaxPer="+TaxPer+"&Tax="+Tax+"&DocTotal="+DocTotal+"&AmmountTendered="+AmmountTendered+"&Change="+Change+
				"&PaymentNote="+PaymentNote+"&PaymentCode="+PaymentCode+"&Wallet="+Wallet+
				"&ddidDPOS="+gddidDPOS+"&ddidBaseRef="+gddidBaseRef+"&ddBaseRef="+gddBaseRef+"&ddItem="+gddItem+"&ddQty="+gddQty+"&ddPrice="+gddPrice+"&ddDisc="+gddDisc+"&perluGR="+gperluGR+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==0)
					{
						emptyform();
						alert("Error");
					}
					else
					{
						emptyform();
						$('#modal-pay').modal('hide');
						$('#modal-print-pos').modal('show');
						document.getElementById('dataloader-modal-print-pos').src = '<?php echo $this->config->base_url()?>index.php/toolbar/prints/'+msg+'?type=POS';
					}
				}
			}); 
		}
  }
  function saveso() 
  {
	    $(".loadingbar2").show();
	    $(':button').prop('disabled', true);
		var BPName = $("#BPName").val();
		var WhsName = $("#WhsName").val();
		var Table = $("#Table").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var DiscPer = unformat_number($("#DiscPer").val());
		var Disc = unformat_number($("#Disc").val());
		var Freight = unformat_number($("#Freight").val());
		var TaxPer = unformat_number($("#TaxPer").val());
		var Tax = unformat_number($("#Tax").val());
		var DocTotal = unformat_number($("#DocTotal").val());
		var gddidDPOS=JSON.stringify(ddidDPOS);
		var gddidBaseRef=JSON.stringify(ddidBaseRef);
		var gddBaseRef=JSON.stringify(ddBaseRef);
		var gddItem=JSON.stringify(ddItem);
		var gddQty=JSON.stringify(ddQty);
		var gddPrice=JSON.stringify(ddPrice);
		var gddDisc=JSON.stringify(ddDisc);
		var gperluGR=JSON.stringify(perluGR);
			
		
		var bp=0;
		var loc=0;
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekbp?", 
			data: "BPName="+BPName+"", 
			cache: true, 
			success: function(data){ 
				bp=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekloc?", 
			data: "WhsName="+WhsName+"", 
			cache: true, 
			success: function(data){ 
				loc=data;
			},
			async: false
		});
		
		
		if(BPName=='')
		{
			alert('Please Fill Bussiness Partner');
			$("#BPCode").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(ddItem.length==0)
		{
			alert('Please Fill Detail Data');
			$("#detailItem").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(bp==0)
		{
			alert('Bussiness Partner Not Found');
			$("#BPCode").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(loc==0)
		{
			alert('Location Not Found');
			$("#WhsName").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosessaveso", 
				data: "BPName="+BPName+"&WhsName="+WhsName+"&Table="+Table+"&DocTotalBefore="+DocTotalBefore+
				"&DiscPer="+DiscPer+"&Disc="+Disc+"&Freight="+Freight+"&TaxPer="+TaxPer+"&Tax="+Tax+"&DocTotal="+DocTotal+
				"&ddidDPOS="+gddidDPOS+"&ddidBaseRef="+gddidBaseRef+"&ddBaseRef="+gddBaseRef+"&ddItem="+gddItem+"&ddQty="+gddQty+"&ddPrice="+gddPrice+"&ddDisc="+gddDisc+"&perluGR="+gperluGR+"", 
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						emptyform();
						$("#successalert").show();
						window.scrollTo(0, 0);
						$(".loadingbar2").hide();
						$(':button').prop('disabled', false);
					}
					else
					{
						emptyform();
						alert(msg);
						$(".loadingbar2").hide();
						$(':button').prop('disabled', false);
					}
				}
			}); 
		}
  }
  function editso() 
  {
	    $(".loadingbar2").show();
	    $(':button').prop('disabled', true);
		var IDSO = $("#IDSO").val();
		var BPName = $("#BPName").val();
		var WhsName = $("#WhsName").val();
		var Table = $("#Table").val();
		
		var DocTotalBefore = unformat_number($("#DocTotalBefore").val());
		var DiscPer = unformat_number($("#DiscPer").val());
		var Disc = unformat_number($("#Disc").val());
		var Freight = unformat_number($("#Freight").val());
		var TaxPer = unformat_number($("#TaxPer").val());
		var Tax = unformat_number($("#Tax").val());
		var DocTotal = unformat_number($("#DocTotal").val());
		var gddidDPOS=JSON.stringify(ddidDPOS);
		var gddidBaseRef=JSON.stringify(ddidBaseRef);
		var gddBaseRef=JSON.stringify(ddBaseRef);
		var gddItem=JSON.stringify(ddItem);
		var gddQty=JSON.stringify(ddQty);
		var gddPrice=JSON.stringify(ddPrice);
		var gddDisc=JSON.stringify(ddDisc);
		var gperluGR=JSON.stringify(perluGR);
			
		
		var bp=0;
		var loc=0;
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekbp?", 
			data: "BPName="+BPName+"", 
			cache: true, 
			success: function(data){ 
				bp=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekloc?", 
			data: "WhsName="+WhsName+"", 
			cache: true, 
			success: function(data){ 
				loc=data;
			},
			async: false
		});
		
		
		if(BPName=='')
		{
			alert('Please Fill Bussiness Partner');
			$("#BPCode").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(ddItem.length==0)
		{
			alert('Please Fill Detail Data');
			$("#detailItem").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(bp==0)
		{
			alert('Bussiness Partner Not Found');
			$("#BPCode").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(loc==0)
		{
			alert('Location Not Found');
			$("#WhsName").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/proseseditso", 
				data: "IDSO="+IDSO+"&BPName="+BPName+"&WhsName="+WhsName+"&Table="+Table+"&DocTotalBefore="+DocTotalBefore+
				"&DiscPer="+DiscPer+"&Disc="+Disc+"&Freight="+Freight+"&TaxPer="+TaxPer+"&Tax="+Tax+"&DocTotal="+DocTotal+
				"&ddidDPOS="+gddidDPOS+"&ddidBaseRef="+gddidBaseRef+"&ddBaseRef="+gddBaseRef+"&ddItem="+gddItem+"&ddQty="+gddQty+"&ddPrice="+gddPrice+"&ddDisc="+gddDisc+"&perluGR="+gperluGR+"", 
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						emptyform();
						$("#successalert").show();
						window.scrollTo(0, 0);
						$(".loadingbar2").hide();
						$(':button').prop('disabled', false);
					}
					else
					{
						emptyform();
						alert(msg);
						$(".loadingbar2").hide();
						$(':button').prop('disabled', false);
					}
				}
			}); 
		}
  }
  /*
  
	DETAIL FUNCTION
  
  */
  function emptyform()
  {
	  $("#buttonedit").hide();
	  $('#BPName').attr("disabled", false);
	  $("#buttonsave").show();
	  $("#Table").val($("#Table option:first").val());
	   ddidDPOS=[];
	   ddidBaseRef=[];
	   ddBaseRef=[];
	   ddItem=[];
	   ddQty=[];
	   ddPrice=[];
	   ddDisc=[];
	   perluGR=[];
	   document.getElementById("IDSO").value ='';
		document.getElementById("DocTotalBefore").value = "0.00";
		document.getElementById("DiscPer").value = "";
		document.getElementById("Disc").value = "0.00";
		document.getElementById("Freight").value = "0.00";
		document.getElementById("Tax").value = "0.00";
		document.getElementById("DocTotal").value = "0.00";
				
		reloaddetail();
  }
  function insertItem(item) //fungsi saat pilih item dengan modal
  {
	   // Disable all the buttons
	  var element = document.getElementById('form-adddetail');
	  style = window.getComputedStyle(element);
	  display = style.display;
	  if(display=='block')
	  {
		  var detailItem = $("#detailItem").val();
		  if($("#detailQty").val()=='' || detailItem!=item)
		  {
			  var detailQty=0;
		  }
		  else
		  {
			  var detailQty=$("#detailQty").val();
		  }
		  document.getElementById("detailItem").value = item;
		  document.getElementById("detailQty").value = parseInt(detailQty)+1;
		  $('#modal-qty').on('shown.bs.modal', function () {
			$('#detailQty').focus();
		  })  
		  document.getElementById('detailQty').select();
	  }
	  else
	  {
		  $('#modal-qty').modal('show');
		  var detailItem = $("#detailItem").val();
		  var detailQty=0;
		  document.getElementById("detailItem2").value = item;
		  document.getElementById("detailItem").value = item;
		  document.getElementById("detailQty2").value = parseInt(detailQty)+1;
		  $('#modal-qty').on('shown.bs.modal', function () {
			$('#detailQty2').focus();
		  })  
		  document.getElementById('detailQty2').select();
	  }
	  isiitem();
	   // Enable all the buttons
	  
  }
  function submitdetail(detailItem,detailQty,detailPrice,detailDisc)
  {
	  var updateqty=0;
	  var lengthdata=ddItem.length;
	  if(lengthdata>0)
	  {
		  for(var i=0;i<=lengthdata;i++)
		  {
			  if(detailItem==ddItem[i])
			  {
				  ddQty[i]=parseInt(ddQty[i])+parseInt(detailQty);
				  ddPrice[i]=detailPrice;
				  ddDisc[i]=detailDisc;
				  var updateqty=1;
			  }
			  
		  }
	  }
	  if(updateqty==0)
	  {
		  ddidDPOS[lengthdata]='';
		  ddidBaseRef[lengthdata]='';
		  ddBaseRef[lengthdata]='';
		  perluGR[lengthdata]='';
		  ddItem[lengthdata]=detailItem;
		  ddQty[lengthdata]=detailQty;
		  ddPrice[lengthdata]=detailPrice;
		  ddDisc[lengthdata]=detailDisc;
	  }
	  reloaddetail();
  }
  function reloaddetail()
  {
	  //clear data
	  var oTable = $('#mix_detail2').dataTable();
	  oTable.fnClearTable();
	  // end clear data
	  
	  var lengthdata=ddItem.length-1;
	  var total=0;
	  for(var i=0;i<=lengthdata;i++)
	  {
		  if(ddDisc[i]=='')ddDisc[i]=0;
		  var disc=ddDisc[i]/100*ddPrice[i];
		  var linetotal=(ddPrice[i]-disc)*ddQty[i];
		  total = total + linetotal;
	  
		  $('#mix_detail2').dataTable().fnAddData( [
			ddItem[i],
			ddQty[i],
			ddPrice[i],
			ddDisc[i],
			linetotal,
			'<div id="controldetail"><a onclick="delDetail(\''+ddItem[i]+'\',\''+ddQty[i]+'\')"><i class="fa fa-trash" aria-hidden="true"></i></a></div>']
		  );
	  }
	  globalDocTotalBeforeInt = total;
  }
  function addDetail() //proses add detail
  {
	    $(".loadingbar2").show();
	    $(':button').prop('disabled', true);
	    var modeqtystatus = $('#modal-qty').hasClass('in');
		$('#modal-qty').modal('hide');
		if(modeqtystatus==true)
		{
			var detailItem = $("#detailItem2").val();
			var detailQty = $("#detailQty2").val();
			var detailPrice = $("#detailPrice2").val();
			var detailDisc = $("#detailDisc2").val();
		}
		else
		{
			var detailItem = $("#detailItem").val();
			var detailQty = $("#detailQty").val();
			var detailPrice = $("#detailPrice").val();
			var detailDisc = $("#detailDisc").val();
		}
		
		if(detailItem=='')
		{
			alert('Please Fill Item Name');
			$("#detailItem").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(detailQty=='' || detailQty==0)
		{
			alert('Please Fill Quantity');
			$("#detailQty").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else if(detailPrice=='')
		{
			alert('Please Fill Price');
			$("#detailPrice").focus();
			$(".loadingbar2").hide();
			$(':button').prop('disabled', false);
		}
		else
		{
			
			document.getElementById("detailItem").value = "";
			document.getElementById("detailQty").value = "";
			document.getElementById("detailPrice").value = "";
			document.getElementById("detailDisc").value = "";
			document.getElementById("detailUoM").value = "";
			document.getElementById("detailQty").blur();
			document.getElementById("detailPrice").blur();
			document.getElementById("detailDisc").blur();
						
			document.getElementById("detailItem2").value = "";
			document.getElementById("detailQty2").value = "";
			document.getElementById("detailPrice2").value = "";
			document.getElementById("detailDisc2").value = "";
			document.getElementById("detailUoM2").value = "";
			document.getElementById("detailQty2").blur();
			document.getElementById("detailPrice2").blur();
			document.getElementById("detailDisc2").blur();
			submitdetail(detailItem,detailQty,detailPrice,detailDisc);
						
			loadtotal();
			var detailUseBarcode = document.getElementById("detailUseBarcode").checked;
			if(detailUseBarcode==true)
			{
				$("#detailBarcode").focus();
			}
		}
  }
  function delDetail(name, qty) // proses hapus detail
  {
	  var lengthdata=ddItem.length;
	  if(lengthdata>0)
	  {
		  for(var i=0;i<=lengthdata;i++)
		  {
			  if(name==ddItem[i])
			  {
				  ddidDPOS.splice(i, 1);
				  ddidBaseRef.splice(i, 1);
				  ddBaseRef.splice(i, 1);
				  ddItem.splice(i, 1);
				  ddQty.splice(i, 1);
				  ddPrice.splice(i, 1);
				  ddDisc.splice(i, 1);
				  perluGR.splice(i, 1);
			  }
		  }
	  }
	  reloaddetail();
	  loadtotal();
  }
  function formatnumber()
  {  
	$('#DocTotalBefore').mask("#,##0.00", {reverse: true});
	$('#Disc').mask("#,##0.00", {reverse: true});
	$('#Freight').mask("#,##0.00", {reverse: true});
	$('#Tax').mask("#,##0.00", {reverse: true});
	$('#DocTotal').mask("#.##0,00", {reverse: true});
	document.getElementById("Freight").value = "0.00";
  }
  $(function () {
		//$("#loadingall").show();
		formatnumber();//saat load awal buat input angka menjadi number format 
		loadtotal();
		var BPName = $("#BPName").val();
		for(var i=0;i<=totaltax;i++)
		{
			if(alltax_BPName[i]==BPName)
			{
				document.getElementById("TaxPer").value = alltax_Rate[i];
			}
		}
		
		
		var WhsName = $("#WhsName").val();
		loadWallet(WhsName);
		$('#mix_detail2').DataTable( {
			data: dataSet,
			columns: [
				{ title: "Item" },
				{ title: "Qty" },
				{ title: "Price" },
				{ title: "Disc." },
				{ title: "Line Total" },
				{ title: "Control" }
			],
			"columnDefs": [
			  { className: "text-right", "targets": [1,2,3,4]},
			  {	className: "text-center", "targets": [5]}
			]
		} );
		
		$("#buttonedit").hide();
		$('#BPName').attr("disabled", false);
		$("#buttonsave").show();
		//fungsi typeahead
		$('#detailItem').typeahead({
			source: [
				<?php foreach($autoitem->result() as $au)
				{
				?>
			  '<?php echo str_replace("'","\'",$au->vcName);?>',
				<?php
				}
				?>
			],
			onselect: function (obj) {
			}
		});
		
	
	
	
		$('#BPName').typeahead({
			source: [
				<?php foreach($autobp->result() as $au)
				{
				?>
			  '<?php echo str_replace("'","\'",$au->vcName);?>',
				<?php
				}
				?>
			],
			onselect: function (obj) {
			}
		});
		
		$('#WhsName').typeahead({
			source: [
				<?php foreach($autowhs->result() as $au)
				{
				?>
			  '<?php echo str_replace("'","\'",$au->vcName);?>',
				<?php
				}
				?>
			],
			onselect: function (obj) {
			}
		});
  });
</script>
</body>
</html>
