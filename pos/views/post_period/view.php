<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-add">
  <form role="form" method="POST"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" id="tittleAdd">Add New Period</h3>
            </div>
            
              <div class="box-body">
				<div class="form-group">
                  <label for="Name">Name</label>
                  <input type="text" class="form-control" id="Name" name="Name" maxlength="25" required="true" placeholder="Enter Name">
                </div>
				<div class="form-group">
                  <label for="Year">Year</label>
                  <select id="Year" name="Year" class="form-control" >
					<?php for($iyear=date('Y');$iyear<date('Y')+20;$iyear++){?>
					<option value="<?php echo $iyear;?>" ><?php echo $iyear;?></option>
					<?php } ?>
				  </select>
                </div>
                
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary" id="addbutton" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
   
   <div class="modal fade" id="modal-edit">
  <form role="form" method="POST"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
			  <h3 class="box-title" id="tittleEdit">Edit Period</h3>
            </div>
			  <input type="hidden" class="form-control" id="ID" name="ID" maxlength="25">
              <div class="box-body">
				<div class="form-group">
                  <label for="NameE">Name</label>
                  <input type="text" class="form-control" id="NameE" name="NameE" maxlength="25" required="true" placeholder="Enter Name">
                </div>
				<div class="form-group">
                  <label for="YearE">Year</label>
                  <select id="YearE" name="YearE" class="form-control" readonly="true">
					<?php for($iyear=date('Y');$iyear<date('Y')+20;$iyear++){?>
					<option value="<?php echo $iyear;?>" ><?php echo $iyear;?></option>
					<?php } ?>
				  </select>
                </div>
                <div class="form-group">
                  <label for="Month">Month</label>
                  <select id="Month" name="Month" class="form-control" readonly="true">
					<?php for($imonth=1;$imonth<=12;$imonth++){
						$dateObj   = DateTime::createFromFormat('!m', $imonth);
						$monthName = $dateObj->format('F'); // March
						?>
					<option value="<?php echo $imonth;?>" ><?php echo $monthName;?></option>
					<?php } ?>
				  </select>
                </div>
				<div class="form-group">
                  <label for="Status">Status</label>
                  <select id="Status" name="Status" class="form-control">
					<option value="L" >Locked</option>
					<option value="U" >Unlocked</option>
				  </select>
                </div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary" id="editbutton" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>

  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <button type="button" class="btn btn-dark" onclick="add()"  data-toggle="modal" data-target="#modal-add"
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>><span class="glyphicon glyphicon-plus"></span> 
				  <?php echo 'Add New'; ?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Name </th>
				  <th>Year </th>
				  <th>Month </th>
				  <th>Status </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
					$dateObj   = DateTime::createFromFormat('!m', $d->intMonth);
					$monthName = $dateObj->format('F'); // March
				?>
                <tr>
                  <td class=" "><?php echo $d->vcName; ?></td>
				  <td class=" "><?php echo $d->intYear; ?></td>
				  <td class=" "><?php echo $monthName; ?></td>
				  <td class=" "><?php echo $d->StatusName; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <i class="fa fa-search <?php echo $usericon;?>" data-toggle="modal" 
				  onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcName); ?>','<?php echo $d->intYear; ?>','<?php echo $d->intMonth; ?>','<?php echo $d->vcStatus; ?>')" 
				  data-target="#modal-edit" aria-hidden="true"></i>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	function edit(id,name,year,month,status)
	{
		$("#tittleEdit").show();
		$("#tittleAdd").hide();
		$("#addbutton").hide();
		$("#editbutton").show();
		document.getElementById("ID").value = id;
		document.getElementById("NameE").value = name;
		document.getElementById("YearE").value = year;
		document.getElementById("Month").value = month;
		document.getElementById("Status").value = status;
		
		
		
		$('#Month').attr("disabled", true);
		$('#Year').attr("disabled", true);
		
		<?php if($crudaccess->intUpdate==0) { ?>
		$('#NameE').attr("disabled", true);
		$('#YearE').attr("disabled", true);
		$('#Status').attr("disabled", true);
		<?php }else{ ?>
		$('#NameE').attr("disabled", false);
		$('#YearE').attr("disabled", false);
		$('#Status').attr("disabled", false);
		<?php } ?>
	}
	function add()
	{
		 $("#tittleAdd").show();
		 $("#tittleEdit").hide();
		 $("#addbutton").show();
		 $("#editbutton").hide();
		 
		 <?php if($crudaccess->intCreate==0) { ?>
		$('#Name').attr("disabled", true);
		$('#Year').attr("disabled", true);
		
		<?php }else{ ?>
		$('#Name').attr("disabled", false);
		$('#Year').attr("disabled", false);
		
		<?php } ?>
	}
  $(function () {
	  
	$(".select2").select2();
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
