<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>
<style>
.modal-dialog {
  width: 90%;
 
}

</style>
<div class="wrapper">   
   
  <div class="modal fade" id="modal-add-edit">
  <div id="printarea_toolbar">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
		    <div class="box-header with-border" id="tittlemodaladd">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Add New Production Order</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
			<div class="box-header with-border" id="tittlemodaledit">
			<div class="col-sm-12">
			  <div class="col-sm-6">
              <h3 class="box-title">Detail Production Order</h3><div class="loadertransaction" style="float:right"></div>
			  </div>
			  <div class="col-sm-6">
			  <?php $this->load->view('toolbar'); ?>
			  </div>
			</div>
            </div>
              <div class="box-body">
			  <?php $this->load->view('loadingbar2'); ?>
				<div class="row">
					<div class="alert alert-success alert-dismissible" id="successalert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-check"></i>Success. You successfully process the data
					</div>
					<div class="alert alert-danger  alert-dismissible" id="dangeralert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon fa fa-ban"></i>Error. You failed to process the data
					</div>
					<div class="col-sm-4">
						<div class="form-group">
						  <label for="DocNum" class="col-sm-4 control-label" style="height:20px">Doc. Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="DocNum" name="DocNum" readonly="true"  data-toggle="tooltip" data-placement="top" title="Document Number">
							<input type="hidden" id="idHeader" name="idHeader">
							<input type="hidden" id="IdToolbar" name="IdToolbar">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Doc. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="DocDate" name="DocDate" placeholder="Doc. Date"  data-toggle="tooltip" data-placement="top" title="Document Date">
							</div>
							</div>
						</div>
						
						<div class="form-group">
						
						  <label for="FGItem" class="col-sm-4 control-label" style="height:20px">Item</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon" data-toggle="modal" data-target="#modal-itemheader">
								<i class="fa fa-cubes" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control" id="FGItem" name="FGItem" placeholder="Item"  data-toggle="tooltip" data-placement="top" title="Item">
							</div>
							</div>
						</div>
						
						
					</div>
					<div class="col-md-4">
						<div class="form-group">
						  <label for="FGQty" class="col-sm-4 control-label" style="height:20px">Base Quantity</label>
							<div class="col-sm-8" style="height:45px">
							<input type="number" class="form-control" id="FGQty" name="FGQty" placeholder="Quantity" 
							onchange="reloadplannedqty()" onpaste="reloadplannedqty()" onkeypress="reloadplannedqty()" onkeyup="reloadplannedqty()" 
							data-toggle="tooltip" data-placement="top" title="Quantity">
							</div>
						</div>
						<div class="form-group">
						  <label for="FGPlannedQty" class="col-sm-4 control-label" style="height:20px">Planned Qty</label>
							<div class="col-sm-8" style="height:45px">
							<input type="number" class="form-control" id="FGPlannedQty" name="FGPlannedQty" placeholder="Planned Quantity" 
							onchange="reloadplannedqty()" onpaste="reloadplannedqty()" onkeypress="reloadplannedqty()" onkeyup="reloadplannedqty()" 
							data-toggle="tooltip" data-placement="top" title="Planned Quantity">
							</div>
						</div>
						<div class="form-group">
						  <label for="Whs" class="col-sm-4 control-label" style="height:20px">Warehouse</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Whs" name="Whs"class="form-control" onchange="pilihwhs()"  data-toggle="tooltip" data-placement="top" title="Warehouse">
								<?php 
								foreach($listwhs->result() as $d) 
								{
								?>	
								<option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
								<?php 
								}
								?>
						   </select>
						   </div>
						</div>
					</div>
					<div class="col-md-4">
						
						<div class="form-group">
						  <label for="Status" class="col-sm-4 control-label" style="height:20px">Status</label>
							<div class="col-sm-8" style="height:45px">
							<select id="Status" name="Status"class="form-control" data-toggle="tooltip" data-placement="top" title="Status">
								<option value="0" >Planned</option>
								<option value="1" >Release</option>
								<option value="2" >Closed</option>
						   </select>
						   </div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Req. Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="ReqDate" name="ReqDate" placeholder="Req. Date"  data-toggle="tooltip" data-placement="top" title="Req. Date">
							</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Release Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="ReleaseDate" name="ReleaseDate" placeholder="Release Date" readonly="true" data-toggle="tooltip" data-placement="top" title="Release Date">
							</div>
							</div>
						</div>
						
						
					</div>
				</div>
				<hr>
				<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#detaildata" data-toggle="tab">Detail</a></li>
				  <li><a href="#summarydata" data-toggle="tab">Summary</a></li>
				</ul>
				<div class="tab-content">
				  <div class="active tab-pane" id="detaildata">
					<div class="" id="form-adddetail">
						<div class="col-sm-4" style="padding-left:0px;padding-bottom:10px">
							<div class="input-group">
							  <div class="input-group-addon" data-toggle="modal" data-target="#modal-item">
								<i class="fa fa-cubes" aria-hidden="true"></i>
							  </div>
							  <input type="text" class="form-control inputenter" id="detailItem" name="detailItem" maxlength="100" required="true" 
							  onchange="isiitem()" onpaste="isiitem()" placeholder="Select Item Name" data-toggle="tooltip" data-placement="top" title="Item Name">
							</div>
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" class="form-control inputenter" id="detailItemCode" name="detailItemCode" maxlength="50" required="true" 
							  onchange="isiitemcode()" onpaste="isiitemcode()" placeholder="Select Code" data-toggle="tooltip" data-placement="top" title="Item Code">
						</div>
						<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="detailQty" name="detailQty" onkeypress="isiqty()" onkeyup="isiqty()" 
							   data-toggle="tooltip" data-placement="top" title="Quantity" maxlength="15" required="true" 
							  placeholder="Qty">
						</div>
						<div class="col-sm-1" style="padding-left:0px;padding-bottom:10px">
							  <input type="text" step="any" class="form-control inputenter" id="detailUoM" name="detailUoM" readonly="true"
							   data-toggle="tooltip" data-placement="top" title="Unit of Measure" maxlength="15" required="true" 
							  placeholder="UoM">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <input type="number" step="any" class="form-control inputenter" id="detailCost" name="detailCost" maxlength="15" required="true"
							   data-toggle="tooltip" data-placement="top" title="Item Cost"
							  placeholder="Item Cost">
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							  <select name="detailAuto" id="detailAuto" class="form-control inputenter" data-toggle="tooltip" data-placement="top" title="Issue Method">
								<option value="1"> Auto Issue</option>
								<option value="0"> Manual Issue</option>
							  </select>
						</div>
						<div class="col-sm-2" style="padding-left:0px;padding-bottom:10px">
							<button type="button" class="btn btn-primary" id="buttonadddetail" onclick="addDetail()">Add</button>
							<button type="button" class="btn btn-primary" id="buttoneditdetail" onclick="addDetail()">Update</button>
							<button type="button" class="btn btn-default" id="buttoncanceldetail" onclick="cancelDetail()">Cancel</button>
						</div>
					</div>

					<div id="mix_detail"></div>
				  </div>
				  <div class="tab-pane" id="summarydata">
					<div class="row">
					  <div class="col-sm-6">
						<div class="form-group">
						  <label for="FGActualComponentCost" class="col-sm-4 control-label" style="height:20px">Actual Item Component Cost</label>
							<div class="col-sm-7" style="height:45px">
							<input type="text" class="form-control" id="FGActualComponentCost" name="FGActualComponentCost" placeholder="Actual Item Component Cost"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Actual Item Component Cost">
							</div>
							<div class="col-sm-1">
							<i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true" data-toggle="modal" onclick="showdetailactualcomponent()"></i>
							</div>
						</div>
						<div class="form-group">
						  <label for="FGActualProductCost" class="col-sm-4 control-label" style="height:20px">Actual Product Cost</label>
							<div class="col-sm-7" style="height:45px">
							<input type="text" class="form-control" id="FGActualProductCost" name="FGActualProductCost" placeholder="Actual Product Cost"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Actual Product Cost">
							</div>
							<div class="col-sm-1">
							<i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true" data-toggle="modal" onclick="showdetailactualproduct()"></i>
							</div>
						</div>
						<div class="form-group">
						  <label for="FGActualRejectCost" class="col-sm-4 control-label" style="height:20px">Actual Reject Cost</label>
							<div class="col-sm-7" style="height:45px">
							<input type="text" class="form-control" id="FGActualRejectCost" name="FGActualRejectCost" placeholder="Actual Reject Cost"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Actual Reject Cost">
							</div>
							<div class="col-sm-1">
							<i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true" data-toggle="modal" onclick="showdetailactualreject()"></i>
							</div>
						</div>
						<div class="form-group">
						  <label for="FGTotalVariance" class="col-sm-4 control-label" style="height:20px">Total Variance</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="FGTotalVariance" name="FGTotalVariance" placeholder="Total Variance"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Total Variance">
							</div>
						</div>
					  </div>
					  <div class="col-sm-6">
						<div class="form-group">
						  <label for="FGActualQty" class="col-sm-4 control-label" style="height:20px">Actual Qty</label>
							<div class="col-sm-7" style="height:45px">
							<input type="number" class="form-control" id="FGActualQty" name="FGActualQty" placeholder="Actual Quantity"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Actual Quantity">
							</div>
							<div class="col-sm-1">
							<i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true" data-toggle="modal" onclick="showdetailactualproduct()"></i>
							</div>
						</div>
						<div class="form-group">
						  <label for="FGRejectQty" class="col-sm-4 control-label" style="height:20px">Reject Qty</label>
							<div class="col-sm-7" style="height:45px">
							<input type="number" class="form-control" id="FGRejectQty" name="FGRejectQty" placeholder="Reject Quantity"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Reject Quantity">
							</div>
							<div class="col-sm-1">
							<i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true" data-toggle="modal" onclick="showdetailactualreject()"></i>
							</div>
						</div>
						<div class="form-group">
						  <label for="FGUoM" class="col-sm-4 control-label" style="height:20px">Unit of Measure</label>
							<div class="col-sm-8" style="height:45px">
							<input type="text" class="form-control" id="FGUoM" name="FGUoM" placeholder="Unit of Measure"  readonly="true"
							data-toggle="tooltip" data-placement="top" title="Unit of Measure">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" style="height:20px">Closed Date</label>
							<div class="col-sm-8" style="height:45px">
							<div class="input-group date">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control" id="ClosedDate" name="ClosedDate" placeholder="Closed Date" readonly="true" data-toggle="tooltip" data-placement="top" title="Closed Date">
							</div>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
					<div class="form-group">
					  <label>Remarks</label>
					  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..."  data-toggle="tooltip" data-placement="top" title="Remarks"></textarea>
					</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Cost</label>
							<div class="col-sm-8" style="height:45px">
							<input type="number" class="form-control" id="FGCost" name="FGCost" placeholder="Cost" readonly="true" data-toggle="tooltip" data-placement="top" title="FGCost">
							</div>
						</div>
						<div class="form-group">
						  <label for="RefNum" class="col-sm-4 control-label" style="height:20px">Version</label>
							<div class="col-sm-8" style="height:45px">
							<input type="hidden" class="form-control" id="LastRev" name="LastRev">
							<input type="number" class="form-control" id="Rev" name="Rev" placeholder="Version"  data-toggle="tooltip" data-placement="top" title="Version">
							</div>
						</div>
						<div class="form-group">
						  <label for="SONumber" class="col-sm-4 control-label" style="height:20px">SO Number</label>
							<div class="col-sm-8" style="height:45px">
							<input type="number" class="form-control" id="SONumber" name="SONumber" placeholder="SO Number"  data-toggle="tooltip" data-placement="top" title="SO Number">
							</div>
						</div>
					</div>
				</div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="buttoncancel">Cancel</button>
		   <div class="btn-group" id="buttoncopyfrom">
                  <button type="button" class="btn btn-default">Copy From</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu dropup" role="menu">
					<li onclick="copyfrom('BOM')"><a href="#">Bill of Material</a></li>
                  </ul>
           </div>
		   <button type="button" class="btn btn-primary" onclick="addHeader()"  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?> id="buttonsave">Save changes</button>
		   <button type="button" class="btn btn-primary" onclick="editHeader()"  <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> id="buttonedit">Edit changes</button>
         </div>
       </div>
     </div>
   </div>
   </div>
   <div class="modal fade" id="modal-actualproduct">
     <div class="modal-dialog" >
       <div class="modal-content" >
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">RFP List</h3>
            </div>
            
              <div class="box-body">
				<div id="lisrfpproduct"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
   
   <div class="modal fade" id="modal-actualcomponent">
     <div class="modal-dialog" >
       <div class="modal-content" >
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">IFP List</h3>
            </div>
            
              <div class="box-body">
				<div id="lisifpcomponent"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
   
   <div class="modal fade" id="modal-actualreject">
     <div class="modal-dialog" >
       <div class="modal-content" >
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">RFP List</h3>
            </div>
            
              <div class="box-body">
				<div id="lisrfpreject"></div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>
  <?php $this->load->view('selectitem'); ?>
  <?php $this->load->view('selectitemheader'); ?>
  <?php $this->load->view('selectbom'); ?>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
			<?php $this->load->view('loadingbar2'); ?>
				<div class="col-sm-2" style="padding-left:0px;margin-left:0px">
                  <button type="button" style="width:100%" class="btn btn-dark" data-toggle="modal" data-target="#modal-add-edit" onclick="initialadd()" 
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>
				</div>
				<div class="col-sm-7" style="padding-left:0px;margin-left:0px">
				&nbsp;
				</div>
				<div class="col-sm-3" style="padding-left:0px;margin-right:0px">
				<table align="center" style="width:100%">
					<tr>
						<td><input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff"></td>
						<td style="padding-left:5px" align="right"><button type="submit" class="btn btn-primary" onclick="loaddetailheader()">Reload</button>
						</td>
					</tr>
				</table>
				</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<div id="mix_header"></div>
				<div class="loader"></div>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>

  $(".inputenter").on('keyup', function (e) {
    if (e.keyCode == 13) {
        addDetail();
    }
  });
  
  /*
  
	FUNGSI COPY FROM
  
  */
  function copyfrom(val)
  {
	 if(val=='BOM')
	  {
		  $('#modal-bom').modal('show');
	  }
  }
  function insertBOM(id) //fungsi saat pilih item dengan modal
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetailbom?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				$('#modal-bom').modal('hide');
				loadheaderbom(id);
			}
		});
  }
  /*
  
	FUNGSI LOAD
  
  */
  function showdetailactualproduct()
  {
	var idHeader = $("#idHeader").val();
	$('#lisrfpproduct').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisrfpproduct?idpro='+idHeader+'');
	$('#modal-actualproduct').modal('show'); 
  }
  function showdetailactualreject()
  {
	  var idHeader = $("#idHeader").val();
	  $('#lisrfpreject').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisrfpreject?idpro='+idHeader+'');
	  $('#modal-actualreject').modal('show');
  }
  function showdetailactualcomponent()
  {
	  var idHeader = $("#idHeader").val();
	  $('#lisifpcomponent').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisifpcomponent?idpro='+idHeader+'');
	  $('#modal-actualcomponent').modal('show');
  }
  function pilihwhs()
  {
	  loadcost();
  }
  function isiqty()
  {
	  loadcost();
  }
  function isiitem() // fungsi saat ketik field item
  {
	   var detailItem = $("#detailItem").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemCode?", 
			data: "detailItem="+detailItem+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItemCode").value = data;
				loadcost();
				loaduom();
			},
		});
	  
  }
  function isiitemcode() // fungsi saat ketik field item
  {
	  var detailItemCode = $("#detailItemCode").val();
	  
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemName?", 
			data: "detailItemCode="+detailItemCode+"",
			cache: true, 
			success: function(data){
				document.getElementById("detailItem").value = data;
				loadcost();
				loaduom();
			},
	  });
	  
  }
  function loadcost()
  {
	  var detailItem = $("#detailItem").val();
	  var Whs = $("#Whs").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loadcost?", 
			data: "detailItem="+detailItem+"&Whs="+Whs+"", 
			cache: true, 
			success: function(data){ 
				document.getElementById("detailCost").value = data;
			},
			async: false
		});
  }
  function loaddetailheader()
  {
	  $(".loadingbar2").show();
	  var daterange = $("#daterange").val();
	  daterange = daterange.split(' ').join('');
	  $('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'', function(){
		$(".loadingbar2").hide();
	  });   
  }
  function loaduom()
  {
	  var detailItem = $("#detailItem").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaduom?", 
			data: "detailItem="+detailItem+"", 
			cache: true, 
			success: function(data){ 
				document.getElementById("detailUoM").value = data;
			},
			async: false
		});
  }
  function loadheader(id,dup) //untuk reload harga per item
  {
	//load ambil data header
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeader?", 
			data: "id="+id+"&dup="+dup+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				
				document.getElementById("idHeader").value = obj.idHeader;
				document.getElementById("DocNum").value = obj.DocNum;
				document.getElementById("IdToolbar").value = obj.DocNum;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				document.getElementById("FGItem").value = obj.FGItem;
				document.getElementById("FGQty").value = obj.FGQty;
				document.getElementById("Rev").value = obj.vcRef;
				document.getElementById("LastRev").value = obj.vcRef;
				document.getElementById("FGCost").value = obj.FGCost;
				if(id==0)
				{
					$("#Whs").val($("#Whs option:first").val());
				}
				else
				{
					document.getElementById("Whs").value = obj.Whs;
				}
				document.getElementById("Remarks").value = obj.Remarks;
				
				document.getElementById("FGPlannedQty").value = obj.PlannedQty;
				document.getElementById("FGActualQty").value = obj.ActualQty;
				document.getElementById("FGRejectQty").value = obj.RejectQty;
				document.getElementById("FGUoM").value = obj.FGUoM;
				document.getElementById("SONumber").value = obj.SONumber;
				document.getElementById("Status").value = obj.Status;
				document.getElementById("ReqDate").value = obj.ReqDate;
				document.getElementById("ReleaseDate").value = obj.ReleaseDate;
				document.getElementById("ClosedDate").value = obj.ClosedDate;
				
				document.getElementById("FGActualComponentCost").value = obj.FGActualComponentCost;
				document.getElementById("FGActualProductCost").value = obj.FGActualProductCost;
				document.getElementById("FGActualRejectCost").value = obj.FGActualRejectCost;
				document.getElementById("FGTotalVariance").value = obj.FGTotalVariance;
				
				
				if(obj.Status==2) //jika status 2/close maka readonly semua form
				{
					$('#Whs').attr("disabled", true);
					$('#Status').attr("disabled", true);
					$('#FGPlannedQty').attr("disabled", true);
					$('#ReqDate').attr("disabled", true);
					$('#SONumber').attr("disabled", true);
					$("#form-adddetail").hide();
				}
				
			},
			async: false
		});
		
  }
  function loadheaderbom(id) //untuk reload harga per item
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getDataHeaderBom?", 
			data: "id="+id+"",
			cache: true, 
			success: function(data){
				//alert(data);
				var obj = JSON.parse(data);
				
				
				document.getElementById("idHeader").value = obj.idHeader;
				$( "#DocDate" ).datepicker( "setDate", obj.DocDate);
				document.getElementById("FGItem").value = obj.FGItem;
				document.getElementById("FGQty").value = obj.FGQty;
				document.getElementById("Rev").value = obj.vcRef;
				document.getElementById("LastRev").value = obj.vcRef;
				document.getElementById("FGCost").value = obj.FGCost;
				document.getElementById("Whs").value = obj.Whs;
				document.getElementById("Remarks").value = obj.Remarks;
				document.getElementById("FGPlannedQty").value = obj.PlannedQty;
			},
			async: false
		});
  }
  function reloadplannedqty()
  {
	  var FGPlannedQty = $("#FGPlannedQty").val();
	  var FGQty = $("#FGQty").val();
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/reloadplannedqty?", 
			data: "FGQty="+FGQty+"&FGPlannedQty="+FGPlannedQty+"", 
			cache: true, 
			success: function(data){ 
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
	  });
  }
  function initialadd()
  {
	  $("#FGItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#buttonedit").hide();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#Whs').attr("disabled", false);
	  $('#FGItem').attr("disabled", false);
	  $('#FGQty').attr("disabled", false);
	  $('#DocDate').attr("disabled", true);
	  $('#DocNum').attr("disabled", false);
	  
	  $('#Rev').attr("disabled", false);
	  $('#FGQty').attr("disabled", false);
	  
	  $('#FGPlannedQty').attr("disabled", false);
	  $('#ReqDate').attr("disabled", false);
	  $('#SONumber').attr("disabled", false);
	  
	  $("#Status").val($("#Status option:first").val());
	  $('#Status').attr("disabled", true);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				loadheader(0,0);
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
  }
  function initialedit(id)
  {
	  $("#detailItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").hide();
	  $("#buttonedit").show();
	  $("#tittlemodaladd").hide();
	  $("#tittlemodaledit").show();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#Whs').attr("disabled", false);
	  $('#FGItem').attr("disabled", true);
	  $('#FGQty').attr("disabled", false);
	  $('#DocDate').attr("disabled", true);
	  $('#DocNum').attr("disabled", true);
	  
	  $('#Rev').attr("disabled", true);
	  $('#FGQty').attr("disabled", true);
	  
	  $('#FGPlannedQty').attr("disabled", false);
	  $('#ReqDate').attr("disabled", false);
	  $('#SONumber').attr("disabled", false);
	  
	  $('#Status').attr("disabled", false);
	  
	  
	  //load detail ambil dari database dan masukkan ke session
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				loadheader(id,0);
				//load html untuk menampilkan detail dari session
				
				var Status = $("#Status").val();
				
				if(Status==2)//jika status = 2 atau closed maka tampilkan detail data tanpa control delete dan edit
				{
					$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail?withoutcontrol=true');
				}
				else
				{
					$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
				}
			
			}
		});
  }
  function initialduplicate(id)
  {
	  $("#FGItem").focus();
	  $("#form-adddetail").show();
	  $("#buttonsave").show();
	  $("#buttonedit").hide();
	  $("#tittlemodaladd").show();
	  $("#tittlemodaledit").hide();
	  $("#successalert").hide();
	  $("#dangeralert").hide();
	  
	  $("#buttonadddetail").show();
	  $("#buttoneditdetail").hide();
	  $("#buttoncanceldetail").hide();
	  
	  $('#Remarks').attr("disabled", false);
	  $('#Whs').attr("disabled", false);
	  $('#FGItem').attr("disabled", false);
	  $('#FGQty').attr("disabled", false);
	  $('#DocDate').attr("disabled", true);
	  $('#DocNum').attr("disabled", false);
	  
	  $('#Rev').attr("disabled", false);
	  $('#FGQty').attr("disabled", false);
	  
	  $('#FGPlannedQty').attr("disabled", false);
	  $('#ReqDate').attr("disabled", false);
	  $('#SONumber').attr("disabled", false);
	  
	  $("#Status").val($("#Status option:first").val());
	  $('#Status').attr("disabled", true);
	  
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/loaddetail?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				loadheader(id,1);
				//load html untuk menampilkan detail dari session
				$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail');
			}
		});
  }
  /*
  
	HEADER FUNCTION
  
  */
  function insertItemHeader(item)
  {
	  document.getElementById("FGItem").value = item;
	  $('#modal-itemheader').modal('hide');
	  $("#FGQty").focus();
  }
  function addHeader()
  {
		$(".loadertransaction").show();
		var DocNum = $("#DocNum").val();
		var DocDate = $("#DocDate").val();
		var FGItem = $("#FGItem").val();
		var Whs = $("#Whs").val();
		var FGQty = $("#FGQty").val();
		var Remarks = $("#Remarks").val();
		var Rev = $("#Rev").val();
		
		var PlannedQty = $("#FGPlannedQty").val();
		var SONumber = $("#SONumber").val();
		var ReqDate = $("#ReqDate").val();
		
		var detail=0;
		var cekFGItem='';
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemCode?", 
			data: "detailItem="+FGItem+"", 
			cache: true, 
			success: function(data){ 
				cekFGItem=data;
			},
			async: false
		});
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});
		if(DocNum=='')
		{
			alert('Please Fill Doc. Number');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(cekFGItem=='' || cekFGItem==null)
		{
			alert('Please Fill FG Item');
			$("#FGItem").focus();
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(period!=1)
		{
			alert('This Period is locked');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$(".loadertransaction").hide();
		}
		
		else if(FGQty==0 || FGQty=='' || FGQty =='0')
		{
			alert('Please Fill FG Qty');
			$("#FGQty").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd", 
				data: "DocNum="+DocNum+"&DocDate="+DocDate+"&FGItem="+FGItem+"&Whs="+Whs+
				"&Remarks="+Remarks+"&FGQty="+FGQty+"&Rev="+Rev+"&PlannedQty="+PlannedQty+"&SONumber="+SONumber+"&ReqDate="+ReqDate+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						initialadd();
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
						
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  function editHeader() // proses submit edit
  {
		$(".loadertransaction").show();
		var idHeader = $("#idHeader").val();
		var DocNum = $("#DocNum").val();
		var DocDate = $("#DocDate").val();
		var FGItem = $("#FGItem").val();
		var Whs = $("#Whs").val();
		var FGQty = $("#FGQty").val();
		var Remarks = $("#Remarks").val();
		var Rev = $("#Rev").val();
		var LastRev = $("#LastRev").val();
		
		var PlannedQty = $("#FGPlannedQty").val();
		var SONumber = $("#SONumber").val();
		var ReqDate = $("#ReqDate").val();
		var Status = $("#Status").val();
		
		var detail=0;
		var cekFGItem='';
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekdetail?", 
			data: "", 
			cache: true, 
			success: function(data){ 
				detail=data;
			},
			async: false
		});
		
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getItemCode?", 
			data: "detailItem="+FGItem+"", 
			cache: true, 
			success: function(data){ 
				cekFGItem=data;
			},
			async: false
		});
		
		if(DocNum=='')
		{
			alert('Please Fill Doc. Number');
			$("#DocNum").focus();
			$(".loadertransaction").hide();
		}
		else if(cekFGItem=='' || cekFGItem==null)
		{
			alert('Please Fill FG Item');
			$("#FGItem").focus();
			$(".loadertransaction").hide();
		}
		else if(DocDate=='')
		{
			alert('Please Fill Doc. Date');
			$("#DocDate").focus();
			$(".loadertransaction").hide();
		}
		else if(detail==0)
		{
			alert('Please Fill Detail Data');
			$(".loadertransaction").hide();
		}
		else if(FGQty==0 || FGQty=='' || FGQty =='0')
		{
			alert('Please Fill FG Qty');
			$("#FGQty").focus();
			$(".loadertransaction").hide();
		}
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit", 
				data: "idHeader="+idHeader+"&DocNum="+DocNum+"&DocDate="+DocDate+"&FGItem="+FGItem+"&Whs="+Whs+
				"&Remarks="+Remarks+"&FGQty="+FGQty+"&Rev="+Rev+"&PlannedQty="+PlannedQty+"&SONumber="+SONumber+"&ReqDate="+ReqDate+"&Status="+Status+"",  
				cache: false, 
				success: function(msg){ 
					if(msg==1)
					{
						initialedit(idHeader);
						$("#successalert").show();
						$('#modal-add-edit').scrollTop(0);
					}
					else
					{
						alert(msg);
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
  }
  /*
  
	DETAIL FUNCTION
  
  */
  function insertItem(item)
  {
	  document.getElementById("detailItem").value = item;
	  $('#modal-item').modal('hide');
	  loadcost();
	  $("#detailQty").focus();
	  isiitem();
	  loaduom();
  }
  function EnableDisableItem(val)
  {
	  $('#detailItem').attr("disabled", val);
	  $('#detailItemCode').attr("disabled", val);
  }
  function addDetail()
  {
		$(".loadertransaction").show();
		EnableDisableItem(false);
	    var detailItem = $("#detailItem").val();
		var detailQty = $("#detailQty").val();
		var detailCost = $("#detailCost").val();
		var detailAuto = $("#detailAuto").val();
		var FGPlannedQty = $("#FGPlannedQty").val();
		var FGQty = $("#FGQty").val();
		
		if(detailItem=='')
		{
			alert('Please Fill Item Name');
			$("#detailItem").focus();
			$(".loadertransaction").hide();
		}
		else if(detailQty=='' || detailQty==0)
		{
			alert('Please Fill Quantity');
			$("#detailQty").focus();
			$(".loadertransaction").hide();
		}
		
		else
		{
			$.ajax({ 
				type: "POST",
				url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/addDetail", 
				data: "detailItem="+detailItem+"&detailQty="+detailQty+"&detailCost="+detailCost+"&detailAuto="+detailAuto+"&FGPlannedQty="+FGPlannedQty+"&FGQty="+FGQty+"",  
				cache: false, 
				success: function(msg){ 
					if(msg!='false')
					{
						$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
						document.getElementById("detailItem").value = "";
						document.getElementById("detailItemCode").value = "";
						document.getElementById("detailQty").value = "";
						document.getElementById("detailCost").value = "";
						document.getElementById("detailUoM").value = "";
						$("#detailAuto").val($("#detailAuto option:first").val());
						$("#buttonadddetail").show();
						$("#buttoneditdetail").hide();
						$("#buttoncanceldetail").hide();
						document.getElementById("detailItem").focus();
					}
					else
					{
						alert('Error');
					}
					$(".loadertransaction").hide();
				} 
			}); 
		}
		exit();
  }
  function delDetail(code, qty)
  {
	EnableDisableItem(false);
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/delDetail", 
		data: "code="+code+"&qty="+qty+"",  
		cache: false, 
		success: function(msg){ 
			$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
			document.getElementById("detailItem").value = "";
			document.getElementById("detailItemCode").value = "";
			document.getElementById("detailQty").value = "";
			document.getElementById("detailCost").value = "";
			document.getElementById("detailUoM").value = "";
			$("#detailAuto").val($("#detailAuto option:first").val());
			$("#buttonadddetail").show();
			$("#buttoneditdetail").hide();
			$("#buttoncanceldetail").hide();
		} 
	}); 
  }
  function editDetail(item, code, qty, cost, auto, uom)
  {
		document.getElementById("detailItem").value = item;
		document.getElementById("detailItemCode").value = code;
		document.getElementById("detailQty").value = qty;
		document.getElementById("detailCost").value = cost;
		document.getElementById("detailAuto").value = auto;
		document.getElementById("detailUoM").value = uom;
		$("#buttonadddetail").hide();
		$("#buttoneditdetail").show();
		$("#buttoncanceldetail").show();
		$("#detailQty").focus();
		EnableDisableItem(true);
  }
  function cancelDetail()
  {
		document.getElementById("detailItem").value = "";
		document.getElementById("detailItemCode").value = "";
		document.getElementById("detailQty").value = "";
		document.getElementById("detailCost").value = "";
		document.getElementById("detailUoM").value = "";
		$("#detailAuto").val($("#detailAuto option:first").val());
		$("#buttonadddetail").show();
		$("#buttoneditdetail").hide();
		$("#buttoncanceldetail").hide();
		EnableDisableItem(false);
  }
  $(function () {
	$("#FGItem").focus();
	$(".loadertransaction").hide();
	$(document).bind('keydown', function(e) {// fungsi shortcut
		if($('#modal-add-edit').hasClass('in')==true)
		{
			  if(e.ctrlKey && (e.which == 83)) { // Ctrl + s
				e.preventDefault();
				if($('#buttonsave').is(":visible")==true)
				{					
					addHeader();
				}
				else if($('#buttonedit').is(":visible")==true)
				{					
					editHeader();
				}
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 39)) { // Ctrl + right
				e.preventDefault();
				toolbar_next();
				return false;
			  }
			  else if(e.ctrlKey && (e.which == 37)) { // Ctrl + left
				e.preventDefault();
				toolbar_previous();
				return false;
			  }
			  else if(e.which == 27) { // Esc
				e.preventDefault();
				$('#modal-add-edit').modal('hide');
				return false;
			  }
		}
		else
		{
			if(e.which == 78) {
				//$('#modal-add-edit').modal('show');
				//initialadd();
			}
		}
	});
	  
	  
	$('#daterange').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
	//fungsi khusus multiple modal agar saat modal-2 di close modal-1 tetap bisa scroll
	$('.modal').on('hidden.bs.modal', function (e) {
		if($('.modal').hasClass('in')) {
		$('body').addClass('modal-open');
		}    
	});
	//end 
	$('#ReqDate').datepicker({
      autoclose: true
    });
	
	
	//fungsi load ajax
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_detail').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisDetail/');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisHeader?daterange='+daterange+'');
	//end
	
	
	$('#detailItem').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
			alert('Selected ');
		}
	});
	
	$('#FGItem').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
			alert('Selected ');
		}
	});
	$('#detailItemCode').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
   
	$("#exampleBOM").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
