<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
  
</head>
  <?php $this->load->view('body'); ?>
<style>

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-addedit">
  <form role="form" method="POST" enctype="multipart/form-data"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesaddedit">
	<input type="hidden" class="form-control" id="ID" name="ID" maxlength="25" >
	<input type="hidden" class="form-control" id="type" name="type" maxlength="25" >
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" id="tittleAdd">Add New Profile</h3>
			  <h3 class="box-title" id="tittleEdit">Edit Profile</h3>
            </div>
            
              <div class="box-body">
			  <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#general" data-toggle="tab">General</a></li>
				  <li><a href="#contact" data-toggle="tab">Contact</a></li>
				</ul>
				<div class="tab-content">
				  <div class="active tab-pane" id="general">
				    <div class="form-group" align="center">
						<div class="image-upload">
							<label for="Image">
							<img id="blah"  class="profile-user-img img-responsive img-circle" alt="User profile picture" src="<?php echo base_url(); ?>data/general/dist/img/upload.png"/>
							</label>
							<input type="file" id="Image" name="Image" onchange="readURL(this);"><br>
							Upload Image
						</div>
					</div>
					<div class="form-group">
					  <label for="Code">Code</label>
					  <input type="text" class="form-control" id="Code" name="Code" maxlength="25" required="true" placeholder="Enter Code">
					</div>
					<div class="form-group">
					  <label for="Name">Name</label>
					  <input type="text" class="form-control" id="Name" name="Name" maxlength="50" required="true" placeholder="Enter Name">
					</div>
					<div class="form-group">
					  <label for="City">City</label>
					  <input type="text" class="form-control" id="City" name="City" maxlength="50" required="true" placeholder="Enter City">
					</div>
					<div class="form-group">
					  <label for="State">State</label>
					  <input type="text" class="form-control" id="State" name="State" maxlength="50" required="true" placeholder="Enter State">
					</div>
					<div class="form-group">
					  <label for="Country">Country</label>
					  <input type="text" class="form-control" id="Country" name="Country" maxlength="50" required="true" placeholder="Enter Country">
					</div>
					<div class="form-group">
					  <label for="Address">Address</label>
					  <textarea class="form-control" rows="3" id="Address" name="Address" placeholder="Address ..."></textarea>
					</div>
					
			      </div>
				  <div class="tab-pane" id="contact">
					<div class="form-group">
					  <label for="Telp">Telp</label>
					  <input type="text" class="form-control" id="Telp" name="Telp" maxlength="30" placeholder="Enter Telp">
					</div>
					<div class="form-group">
					  <label for="Fax">Fax</label>
					  <input type="text" class="form-control" id="Fax" name="Fax" maxlength="30" placeholder="Enter Fax">
					</div>
					<div class="form-group">
					  <label for="Email">Email</label>
					  <input type="text" class="form-control" id="Email" name="Email" maxlength="50" placeholder="Enter Email">
					</div>
					
				  </div>
                </div>
			  </div>
			  </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <!--<button type="button" class="btn btn-danger" id="deletebutton" <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?> data-toggle="modal" data-target="#modal-delete">Delete</button>
		   <button type="submit" class="btn btn-primary" id="addbutton" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>Save changes</button>-->
		   <button type="submit" class="btn btn-primary" id="editbutton" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  <div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
		  <form role="form" method="POST"
		  action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesdelete/">
		  <input type="hidden" class="form-control" id="ID2" name="ID2" maxlength="25" >
          <button type="submit" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <button type="submit" class="btn btn-danger">Delete</button>
		  </form>
         </div>
       </div>
     </div>
  </div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <!--<button type="button" class="btn btn-dark" onclick="add()"  
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>
				  data-toggle="modal" data-target="#modal-addedit"><span class="glyphicon glyphicon-plus"></span> 
				  <?php echo 'Add New'; ?></button>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Code </th>
				  <th>Name </th>
				  <th>Address </th>
				  <th>City </th>
				  <th>State </th>
				  <th>Country </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td class=" "><?php echo $d->vcCode; ?></td>
				  <td class=" "><?php echo $d->vcName; ?></td>
				  <td class=" "><?php echo $d->vcAddress; ?></td>
				  <td class=" "><?php echo $d->vcCity; ?></td>
				  <td class=" "><?php echo $d->vcState; ?></td>
				  <td class=" "><?php echo $d->vcCountry; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <i class="fa fa-search <?php echo $usericon;?>" data-toggle="modal" onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcCode); ?>'
				  ,'<?php echo str_replace("'","\'",$d->vcName); ?>','<?php echo str_replace("'","\'",$d->vcAddress); ?>'
				  ,'<?php echo str_replace("'","\'",$d->vcCity); ?>','<?php echo str_replace("'","\'",$d->vcState); ?>','<?php echo str_replace("'","\'",$d->vcCountry); ?>'
				  ,'<?php echo str_replace("'","\'",$d->vcTelp); ?>','<?php echo str_replace("'","\'",$d->vcFax); ?>','<?php echo str_replace("'","\'",$d->vcEmail); ?>')" 
				  data-target="#modal-addedit" aria-hidden="true"></i>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
		
function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}
	function edit(id,code,name,address,city,state,country,telp,fax,email)
	{
		var source;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/getimage?", 
			data: "id="+id+"", 
			cache: true, 
			success: function(data){ 
				source=data;
			},
			async: false
		});
		
		if(source!='')
		{
			document.getElementById("blah").src='data:image/jpeg;base64,'+source;
		}
		else
		{
			document.getElementById("blah").src='<?php echo base_url(); ?>data/general/dist/img/upload.png';
		}
		
		$("#tittleEdit").show();
		$("#tittleAdd").hide();
		$("#deletebutton").show();
		$("#addbutton").hide();
		$("#editbutton").show();
		document.getElementById("type").value = 'edit';
		document.getElementById("ID").value = id;
		document.getElementById("ID2").value = id;
		document.getElementById("Code").value = code;
		document.getElementById("Name").value = name;
		document.getElementById("Address").value = address;
		document.getElementById("City").value = city;
		document.getElementById("State").value = state;
		document.getElementById("Country").value = country;
		document.getElementById("Telp").value = telp;
		document.getElementById("Fax").value = fax;
		document.getElementById("Email").value = email;
		
		<?php if($crudaccess->intUpdate==0) { ?>
		$('#Code').attr("disabled", true);
		$('#Name').attr("disabled", true);
		$('#Address').attr("disabled", true);
		$('#City').attr("disabled", true);
		$('#State').attr("disabled", true);
		$('#Country').attr("disabled", true);
		$('#Telp').attr("disabled", true);
		$('#Fax').attr("disabled", true);
		$('#Email').attr("disabled", true);
		<?php }else{ ?>
		$('#Code').attr("disabled", false);
		$('#Name').attr("disabled", false);
		$('#Address').attr("disabled", false);
		$('#City').attr("disabled", false);
		$('#State').attr("disabled", false);
		$('#Country').attr("disabled", false);
		$('#Telp').attr("disabled", false);
		$('#Fax').attr("disabled", false);
		$('#Email').attr("disabled", false);
		<?php } ?>
	}
	function add()
	{
		 $("#tittleAdd").show();
		 $("#tittleEdit").hide();
		 $("#deletebutton").hide();
		 $("#addbutton").show();
		 $("#editbutton").hide();
		 document.getElementById("type").value = 'add';
		 document.getElementById("Code").value = '';
		 document.getElementById("Name").value = '';
		 document.getElementById("Address").value = '';
		 document.getElementById("City").value = '';
		 document.getElementById("State").value = '';
		 document.getElementById("Country").value = '';
		 document.getElementById("Telp").value = '';
		 document.getElementById("Fax").value = '';
		 document.getElementById("Email").value = '';
		 
		 <?php if($crudaccess->intCreate==0) { ?>
		 $('#Code').attr("disabled", true);
		 $('#Name').attr("disabled", true);
		 $('#Address').attr("disabled", true);
		 $('#City').attr("disabled", true);
	  	 $('#State').attr("disabled", true);
		 $('#Country').attr("disabled", true);
		 $('#Telp').attr("disabled", true);
		 $('#Fax').attr("disabled", true);
		 $('#Email').attr("disabled", true);
		 <?php }else{ ?>
		 $('#Code').attr("disabled", false);
	 	 $('#Name').attr("disabled", false);
		 $('#Address').attr("disabled", false);
		 $('#City').attr("disabled", false);
		 $('#State').attr("disabled", false);
		 $('#Country').attr("disabled", false);
		 $('#Telp').attr("disabled", false);
		 $('#Fax').attr("disabled", false);
		 $('#Email').attr("disabled", false);
		 <?php } ?>
	}
  $(function () {
		
	$('#Country').typeahead({
		source: [
			<?php foreach($autocountry->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		]
	});
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
