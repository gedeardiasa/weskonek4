<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>
<style>

.modal-dialog {
  width: 90%;
 
}
<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
</style>
<div class="wrapper">
  <?php $this->load->view('header'); ?>
  
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>
	
    <!-- Main content -->
    <section class="content">
	<div class="box">
      <div class="row">
		<div class="col-sm-3">
			<select id="WithBom" name="WithBom"class="form-control" data-toggle="tooltip" data-placement="top" title="Type" onchange="changetype()">
			<option value="1" <?php if($WithBom==1){ echo "selected";}?>>With BOM</option>
			<option value="0" <?php if($WithBom==0){ echo "selected";}?>>Without BOM</option>
			</select>
		</div><br>
		<hr>
		<div class="col-sm-12">
		<div id="content"></div>
		</div>
	  </div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>

 
  function changetype()
  {
	  var WithBom = $("#WithBom").val();
	  
	  
	  window.location.href = "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>?WithBom="+WithBom+"";
  }
  $(function () {
	  myFunction();
  });
  function createPOD(idso,item,qty)
  {
	  var bom = 0;
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/cekbom?", 
			data: "item="+item+"&idso="+idso+"", 
			cache: true, 
			success: function(data){ 
				
				bom=data;
		},
		async: false
	  });
	  if(bom==0)
	  {
		  alert('BOM/Recipe Not Found');
	  }
	  else
	  {
		  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/createPOD?", 
			data: "idso="+idso+"&item="+item+"&qty="+qty+"", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		  });
	  }
  }
  function closePOD(idso,item)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/closePOD?", 
			data: "idso="+idso+"&item="+item+"", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		  });
	  
  }
  function updateloaddetail(idso,item)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/updateloaddetail?", 
			data: "idso="+idso+"&item="+item+"", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		  });
	  
  }
  function updatecreated(idso,item)
  {
	  $.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/updatecreated?", 
			data: "idso="+idso+"&item="+item+"", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		  });
	  
  }
  function myFunction() {
		setTimeout(function(){ 
		
		var WithBom = $("#WithBom").val();
		if(WithBom==1)
		{
			$('#content').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/getcontent/');
		}
		else
		{
			$('#content').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/getcontent2/');
		}
		myFunction();
		}, 1000);
  }
</script>
</body>
</html>
