<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('bodycollapse'); ?>

<div class="wrapper">

  
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
         <div class="box-body">
            <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li<?php if($setting=='customer'){ echo ' class="active"';}?>><a href="#customer" data-toggle="tab">Customer</a></li>
				  <li<?php if($setting=='items'){ echo ' class="active"';}?>><a href="#items" data-toggle="tab">Items</a></li>
				  <li<?php if($setting=='general'){ echo ' class="active"';}?>><a href="#general" data-toggle="tab">General</a></li>
				  
				</ul>
				<div class="tab-content">
				  <div class="<?php if($setting=='customer'){ echo 'active ';}?>tab-pane" id="customer">
					<div class="box-body">
					<form method="GET" action="">
					<input type="hidden" value="customer" name="setting">
					<div class="row">
						<div class="col-sm-6">
							
							<div class="form-group">
							  <label for="BPCodeC" class="col-sm-4 control-label" style="height:20px">BP Code</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $BPCodeC;?>" id="BPCodeC" name="BPCodeC" data-toggle="tooltip" data-placement="top" title="BP Code">
								</div>
							</div>
							<div class="form-group">
							  <label for="BPNameC" class="col-sm-4 control-label" style="height:20px">BP Name</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $BPNameC;?>" id="BPNameC" name="BPNameC" data-toggle="tooltip" data-placement="top" title="BP Name">
								</div>
							</div>
							<div class="form-group">
							  <label for="BPGroupC" class="col-sm-4 control-label" style="height:20px">BP Group</label>
								<div class="col-sm-8" style="height:45px">
								<select id="BPGroupC" name="BPGroupC" class="form-control" data-toggle="tooltip" data-placement="top" title="BP Group">
									<option value="0" >All</option>
									<?php 
									foreach($listgrpBP->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($BPGroupC==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="PlanC" class="col-sm-4 control-label" style="height:20px">Plan</label>
								<div class="col-sm-8" style="height:45px">
								<select id="PlanC" name="PlanC" class="form-control" data-toggle="tooltip" data-placement="top" title="Plan">
									<?php if($haveaccessallplan==1){?><option value="0" >All</option><?php }?>
									<?php 
									foreach($listplan->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($PlanC==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="CategoryC" class="col-sm-4 control-label" style="height:20px">Category</label>
								<div class="col-sm-8" style="height:45px">
								<select id="CategoryC" name="CategoryC" class="form-control" data-toggle="tooltip" data-placement="top" title="Type">
									<option value="A" <?php if($CategoryC=='A') { echo "selected";}?>>Annual</option>
									<!--<option value="M" <?php if($CategoryC=='M') { echo "selected";}?>>Monthly</option>-->
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="RefC" class="col-sm-4 control-label" style="height:20px">Ref.</label>
								<div class="col-sm-8" style="height:45px">
								<select id="RefC" name="RefC" class="form-control" data-toggle="tooltip" data-placement="top" title="Reference">
									<option value="AP" <?php if($RefC=='AP') { echo "selected";}?>>A/P Invoice</option>
									<option value="GRPO" <?php if($RefC=='GRPO') { echo "selected";}?>>Good Receipt From PO</option>
									<option value="APCM" <?php if($RefC=='APCM') { echo "selected";}?>>A/P Credit Memo</option>
									<option value="PR" <?php if($RefC=='PR') { echo "selected";}?>>Purchase Return</option>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="daterangeC" class="col-sm-4 control-label" style="height:20px">Post. Date</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control pull-right" value ="<?php echo $daterangeC;?>"id="daterangeC" name="daterangeC" readonly="true" style="background-color:#ffffff">
								
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group" align="left">
						  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
						  </div>
						</div>
					</div>
					</form>
					<?php
					if($setting=='customer' and $CategoryI=='A')
					{
					?>
					<hr>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
						<thead>
						<tr>
						  <th>BP Code</th>
						  <th>BP Name</th>
						  <th>Qty</th>
						  <th>UoM</th>
						  <th>Pur Ammount</th>
						  <?php if($userprofit==1){?>
						  <th>Cost</th>
						  <?php } ?>
						</tr>
						</thead>
						<tbody>
						<?php 
						$intLineTotal=0;
						$intLineCost=0;
						$intGrossProfit=0;
						foreach($listreport->result() as $d) 
						{
							$intLineTotal=$intLineTotal+$d->intLineTotal;
							$intLineCost=$intLineCost+$d->intLineCost;
							$intGrossProfit=$intGrossProfit+$d->intGrossProfit;
						?>
						<tr>
						  <td><?php echo $d->vcBPCode; ?></td>
						  <td><?php echo $d->vcBPName; ?></td>
						  <td align="right"><?php echo number_format($d->intQtyInv,'2'); ?></td>
						  <td><?php echo $d->vcUoMInv; ?></td>
						  <td align="right"><?php echo number_format($d->intLineTotal,'2'); ?></td>
						  <?php if($userprofit==1){?>
						  <td align="right"><?php echo number_format($d->intLineCost,'2'); ?></td>
						  <?php } ?>
						</tr>
						<?php 
						}
						?>
						</tbody>
						<tfoot>
							<tr>
							  <th colspan="4">Total</th>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineTotal,'2'); ?></th>
							   <?php if($userprofit==1){?>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineCost,'2'); ?></th>
							  <?php } ?>
							</tr>
						</tfoot>
					</table>
					
					<?php
					}
					else if($CategoryI=='M')
					{
					?>
					
					<?php
					}
					?>
					</div>
				  </div>
				  <div class="<?php if($setting=='items'){ echo 'active ';}?>tab-pane" id="items">
					<div class="box-body">
					<form method="GET" action="">
					<input type="hidden" value="items" name="setting">
					<div class="row">
						<div class="col-sm-6">
							
							<div class="form-group">
							  <label for="ItemCodeI" class="col-sm-4 control-label" style="height:20px">Item Code</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $ItemCodeI;?>" id="ItemCodeI" name="ItemCodeI" data-toggle="tooltip" data-placement="top" title="Item Code">
								</div>
							</div>
							<div class="form-group">
							  <label for="ItemNameI" class="col-sm-4 control-label" style="height:20px">Item Name</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $ItemNameI;?>" id="ItemNameI" name="ItemNameI" data-toggle="tooltip" data-placement="top" title="Item Name">
								</div>
							</div>
							<div class="form-group">
							  <label for="ItemGroupI" class="col-sm-4 control-label" style="height:20px">Item Group</label>
								<div class="col-sm-8" style="height:45px">
								<select id="ItemGroupI" name="ItemGroupI" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Group">
									<option value="0" >All</option>
									<?php 
									foreach($listgrp->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($ItemGroupI==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="PlanI" class="col-sm-4 control-label" style="height:20px">Plan</label>
								<div class="col-sm-8" style="height:45px">
								<select id="PlanI" name="PlanI" class="form-control" data-toggle="tooltip" data-placement="top" title="Plan">
									<?php if($haveaccessallplan==1){?><option value="0" >All</option><?php }?>
									<?php 
									foreach($listplan->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($PlanI==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="CategoryI" class="col-sm-4 control-label" style="height:20px">Category</label>
								<div class="col-sm-8" style="height:45px">
								<select id="CategoryI" name="CategoryI" class="form-control" data-toggle="tooltip" data-placement="top" title="Type">
									<option value="A" <?php if($CategoryI=='A') { echo "selected";}?>>Annual</option>
									<!--<option value="M" <?php if($CategoryI=='M') { echo "selected";}?>>Monthly</option>-->
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="RefI" class="col-sm-4 control-label" style="height:20px">Ref.</label>
								<div class="col-sm-8" style="height:45px">
								<select id="RefI" name="RefI" class="form-control" data-toggle="tooltip" data-placement="top" title="Reference">
									<option value="AP" <?php if($RefI=='AP') { echo "selected";}?>>A/P Invoice</option>
									<option value="GRPO" <?php if($RefI=='GRPO') { echo "selected";}?>>Good Receipt From PO</option>
									<option value="APCM" <?php if($RefI=='APCM') { echo "selected";}?>>A/P Credit Memo</option>
									<option value="PR" <?php if($RefI=='PR') { echo "selected";}?>>Purchase Return</option>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="daterangeI" class="col-sm-4 control-label" style="height:20px">Post. Date</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control pull-right" value ="<?php echo $daterangeI;?>"id="daterangeI" name="daterangeI" readonly="true" style="background-color:#ffffff">
								
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group" align="left">
						  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
						  </div>
						</div>
					</div>
					</form>
					<?php
					if($setting=='items' and $CategoryI=='A')
					{
					?>
					<hr>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
						<thead>
						<tr>
						  <th>Item Code</th>
						  <th>Item Name</th>
						  <th>Qty</th>
						  <th>UoM</th>
						  <th>Pur Ammount</th>
						  <?php if($userprofit==1){?>
						  <th>Cost</th>
						  <?php } ?>
						</tr>
						</thead>
						<tbody>
						<?php 
						$intLineTotal=0;
						$intLineCost=0;
						$intGrossProfit=0;
						foreach($listreport->result() as $d) 
						{
							$intLineTotal=$intLineTotal+$d->intLineTotal;
							$intLineCost=$intLineCost+$d->intLineCost;
							$intGrossProfit=$intGrossProfit+$d->intGrossProfit;
						?>
						<tr>
						  <td><?php echo $d->vcItemCode; ?></td>
						  <td><?php echo $d->vcItemName; ?></td>
						  <td align="right"><?php echo number_format($d->intQtyInv,'2'); ?></td>
						  <td><?php echo $d->vcUoMInv; ?></td>
						  <td align="right"><?php echo number_format($d->intLineTotal,'2'); ?></td>
						  <?php if($userprofit==1){?>
						  <td align="right"><?php echo number_format($d->intLineCost,'2'); ?></td>
						  <?php } ?>
						  
						</tr>
						<?php 
						}
						?>
						</tbody>
						<tfoot>
							<tr>
							  <th colspan="4">Total</th>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineTotal,'2'); ?></th>
							  <?php if($userprofit==1){?>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineCost,'2'); ?></th>
							  <?php } ?>
							</tr>
						</tfoot>
					</table>
					
					<?php
					}
					else if($CategoryI=='M')
					{
					?>
					
					<?php
					}
					?>
					</div>
				  </div>
				  <div class="<?php if($setting=='general'){ echo 'active ';}?>tab-pane" id="general">
					<div class="box-body">
					<form method="GET" action="">
					<input type="hidden" value="general" name="setting">
					<div class="row">
						<div class="col-sm-6">
							
							<div class="form-group">
							  <label for="BPCodeG" class="col-sm-4 control-label" style="height:20px">BP Code</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $BPCodeG;?>" id="BPCodeG" name="BPCodeG" data-toggle="tooltip" data-placement="top" title="BP Code">
								</div>
							</div>
							<div class="form-group">
							  <label for="BPNameG" class="col-sm-4 control-label" style="height:20px">BP Name</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $BPNameG;?>" id="BPNameG" name="BPNameG" data-toggle="tooltip" data-placement="top" title="BP Name">
								</div>
							</div>
							<div class="form-group">
							  <label for="BPGroupG" class="col-sm-4 control-label" style="height:20px">BP Group</label>
								<div class="col-sm-8" style="height:45px">
								<select id="BPGroupG" name="BPGroupG" class="form-control" data-toggle="tooltip" data-placement="top" title="BP Group">
									<option value="0" >All</option>
									<?php 
									foreach($listgrpBP->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($BPGroupG==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="daterangeG" class="col-sm-4 control-label" style="height:20px">Post. Date</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control pull-right" value ="<?php echo $daterangeG;?>"id="daterangeG" name="daterangeG" readonly="true" style="background-color:#ffffff">
								
								</div>
							</div>
							<div class="form-group">
							  <label for="PlanG" class="col-sm-4 control-label" style="height:20px">Plan</label>
								<div class="col-sm-8" style="height:45px">
								<select id="PlanG" name="PlanG" class="form-control" data-toggle="tooltip" data-placement="top" title="Plan">
									<?php if($haveaccessallplan==1){?><option value="0" >All</option><?php }?>
									<?php 
									foreach($listplan->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($PlanG==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="ItemCodeG" class="col-sm-4 control-label" style="height:20px">Item Code</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $ItemCodeG;?>" id="ItemCodeG" name="ItemCodeG" data-toggle="tooltip" data-placement="top" title="Item Code">
								</div>
							</div>
							<div class="form-group">
							  <label for="ItemNameG" class="col-sm-4 control-label" style="height:20px">Item Name</label>
								<div class="col-sm-8" style="height:45px">
								<input type="text" class="form-control" value="<?php echo $ItemNameG;?>" id="ItemNameG" name="ItemNameG" data-toggle="tooltip" data-placement="top" title="Item Name">
								</div>
							</div>
							<div class="form-group">
							  <label for="ItemGroupG" class="col-sm-4 control-label" style="height:20px">Item Group</label>
								<div class="col-sm-8" style="height:45px">
								<select id="ItemGroupG" name="ItemGroupG" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Group">
									<option value="0" >All</option>
									<?php 
									foreach($listgrp->result() as $d) 
									{
									?>	
									<option value="<?php echo $d->intID;?>" <?php if($ItemGroupG==$d->intID) { echo "selected";}?>><?php echo $d->vcName;?></option>
									<?php 
									}
									?>
								</select>
								</div>
							</div>
							<div class="form-group">
							  <label for="RefG" class="col-sm-4 control-label" style="height:20px">Ref.</label>
								<div class="col-sm-8" style="height:45px">
								<select id="RefG" name="RefG" class="form-control" data-toggle="tooltip" data-placement="top" title="Reference">
									<option value="AP" <?php if($RefG=='AP') { echo "selected";}?>>A/P Invoice</option>
									<option value="GRPO" <?php if($RefG=='GRPO') { echo "selected";}?>>Good Receipt From PO</option>
									<option value="APCM" <?php if($RefG=='APCM') { echo "selected";}?>>A/P Credit Memo</option>
									<option value="PR" <?php if($RefG=='PR') { echo "selected";}?>>Purchase Return</option>
								</select>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group" align="left">
						  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-0">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
						  </div>
						</div>
					</div>
					</form>
					<?php
					if($setting=='general')
					{
					?>
					<hr>
					<table id="example1" class="table table-striped dt-responsive jambo_table">
						<thead>
						<tr>
						  <th>Doc. Num.</th>
						  <th>BP Code</th>
						  <th>BP Name</th>
						  <th>Item Code</th>
						  <th>Item Name</th>
						  <th>Qty</th>
						  <th>UoM</th>
						  <th>Pur Ammount</th>
						  <?php if($userprofit==1){?>
						  <th>Cost</th>
						  <?php } ?>
						</tr>
						</thead>
						<tbody>
						<?php 
						$intLineTotal=0;
						$intLineCost=0;
						$intGrossProfit=0;
						foreach($listreport->result() as $d) 
						{
							$intLineTotal=$intLineTotal+$d->intLineTotal;
							$intLineCost=$intLineCost+$d->intLineCost;
							$intGrossProfit=$intGrossProfit+$d->intGrossProfit;
						?>
						<tr>
						  <td><?php echo $d->vcDocNum; ?></td>
						  <td><?php echo $d->vcBPCode; ?></td>
						  <td><?php echo $d->vcBPName; ?></td>
						  <td><?php echo $d->vcItemCode; ?></td>
						  <td><?php echo $d->vcItemName; ?></td>
						  <td align="right"><?php echo number_format($d->intQtyInv,'2'); ?></td>
						  <td><?php echo $d->vcUoMInv; ?></td>
						  <td align="right"><?php echo number_format($d->intLineTotal,'2'); ?></td>
						  <?php if($userprofit==1){?>
						  <td align="right"><?php echo number_format($d->intLineCost,'2'); ?></td>
						  <?php } ?>
						  
						</tr>
						<?php 
						}
						?>
						</tbody>
						<tfoot>
							<tr>
							  <th colspan="7">Total</th>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineTotal,'2'); ?></th>
							  <?php if($userprofit==1){?>
							  <th align="right" style="text-align: right; padding-right:9px"><?php echo number_format($intLineCost,'2'); ?></th>
							  <?php } ?>
							</tr>
						</tfoot>
					</table>
					
					<?php
					}
					else if($CategoryI=='M')
					{
					?>
					
					<?php
					}
					?>
					</div>
				  </div>
				</div>
			</div>
			
			
		</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script src="<?php echo base_url(); ?>asset/cdn/jszip.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/pdfmake.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>asset/cdn/buttons.html5.js"></script>
<script>
  $(function () {
	//fungsi typeahead
	$('#ItemNameI').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemCodeI').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemNameG').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#ItemCodeG').typeahead({
		source: [
			<?php foreach($autoitem->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPCodeC').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPNameC').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPCodeG').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcCode);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#BPNameG').typeahead({
		source: [
			<?php foreach($autobp->result() as $au)
			{
			?>
		  '<?php echo str_replace("'","\'",$au->vcName);?>',
			<?php
			}
			?>
		],
		onselect: function (obj) {
		}
	});
	
	$('#daterangeC').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
	$('#daterangeI').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	$('#daterangeG').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
	$('#daterangeG').daterangepicker({
		showDropdowns: true,
		locale: {
				format: 'YYYY/MM/DD'
		},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]
	});
   
  });
  
  $(function(){
    var table = $('#example').DataTable({
    	"footerCallback": function ( row, data, start, end, display ) {
        	var api = this.api(), data;
	        // Total over all pages
    	    total_ID = api.column(0).data().reduce( function (a, b) {
                return ~~a + ~~b;
            }, 0 );
            total_Duration = api.column(1).data().reduce( function (a, b) {
                return moment.duration(a).asMilliseconds() + moment.duration(b).asMilliseconds();
            }, 0 );
	        // Total over this page
    	    pageTotal_ID = api.column(0, { page: 'current'} ).data().reduce( function (a, b) {
                return ~~a + ~~b;
            }, 0 );
            pageTotal_Duration = api.column(1, { page: 'current'} ).data().reduce( function (a, b) {
                return moment.duration(a).asMilliseconds() + moment.duration(b).asMilliseconds();
            }, 0 );
	        // Update footer Column "quantita"
        	$( api.column(0).footer()).html(
        		pageTotal_ID + ' ('+ total_ID +' total)'
        	);
        	$( api.column(1).footer()).html(
        		moment.utc(pageTotal_Duration).format("HH:mm:ss") + ' ('+ moment.utc(total_Duration).format("HH:mm:ss") + ' total)'
        	);
		}   
    });
});
  
  
  
  
  
  
  
 
 
</script>
</body>
</html>
