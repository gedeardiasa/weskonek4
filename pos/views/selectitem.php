<div class="modal fade" id="modal-item">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Item List</h3>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					<?php 
					$x=0;
					foreach($autoitemcategory->result() as $au)
					{
					?>
					<li <?php if($x==0){ echo 'class="active"';}?>><a href="#<?php echo $au->intID;?>selectitem" data-toggle="tab"><?php echo $au->vcName;?></a></li>
					<?php
					$x++;
					}
					?>
					</ul>
					<div class="tab-content">
						<?php 
						$y=0;
						foreach($autoitemcategory->result() as $au)
						{
						?>
						<div class="<?php if($y==0){ echo 'active';}?> tab-pane" id="<?php echo $au->intID;?>selectitem">
							<div class="box-body">
							<?php foreach($autoitem->result() as $ai)
							{
								if($ai->intCategory==$au->intID)
								{
							?>
								<div class="col-sm-2" onclick="insertItem('<?php echo $ai->vcName;?>')">
									<div align="center"><?php echo substr($ai->vcName,0,20);?>
									</div>
									<?php if($ai->blpImageMin!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $ai->blpImageMin ).'" style="width:125px;height:85px" class="col-sm-12 img-circle" alt="Item Image"/>';}else{ ?>
									<img src="<?php echo base_url(); ?>data/general/dist/img/box.png" class="col-sm-12 img-circle" style="width:125px;height:85px" alt="Item Image">
									<?php } ?>
									
									
								</div>
							<?php
								}
							}
							?>
							</div>
						</div>
						<?php
						$y++;
						}
						?>
						
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>