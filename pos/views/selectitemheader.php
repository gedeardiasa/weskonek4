<div class="modal fade" id="modal-itemheader">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Item List</h3>
            </div>
            
              <div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					<?php 
					$x=0;
					foreach($autoitemcategory->result() as $au)
					{
					?>
					<li <?php if($x==0){ echo 'class="active"';}?>><a href="#<?php echo $au->intID;?>headerselectitem" data-toggle="tab"><?php echo $au->vcName;?></a></li>
					<?php
					$x++;
					}
					?>
					</ul>
					<div class="tab-content">
						<?php 
						$y=0;
						foreach($autoitemcategory->result() as $au)
						{
						?>
						<div class="<?php if($y==0){ echo 'active';}?> tab-pane" id="<?php echo $au->intID;?>headerselectitem">
							<div class="box-body">
							<?php foreach($autoitem->result() as $ai)
							{
								if($ai->intCategory==$au->intID)
								{
							?>
								<div class="col-sm-3" onclick="insertItemHeader('<?php echo $ai->vcName;?>')">
									<div align="center"><?php echo $ai->vcName;?>
									</div>
									<?php if($ai->blpImage!=null){echo '<img src="data:image/jpeg;base64,'.base64_encode( $ai->blpImage ).'" style="width:225px;height:150px" class="col-sm-12 img-circle" alt="Item Image"/>';}else{ ?>
									<img src="<?php echo base_url(); ?>data/general/dist/img/box.png" class="col-sm-12 img-circle" style="width:225px;height:150px" alt="Item Image">
									<?php } ?>
									
								</div>
							<?php
								}
							}
							?>
							</div>
						</div>
						<?php
						$y++;
						}
						?>
						
					</div>
				</div>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="cancelselectitem()">Cancel</button>
         </div>
       </div>
     </div>
   </div>