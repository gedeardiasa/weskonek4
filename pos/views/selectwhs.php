<div class="modal fade" id="modal-whs">
  
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Warehouse List</h3>
            </div>
            
              <div class="box-body">
				<table id="example3" class="table table-striped dt-responsive jambo_table" style="width:100%">
                <thead>
                <tr>
                  <th>Code</th>
				  <th>Name</th>
				  <th>City</th>
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($autowhs->result() as $d) 
				{
				?>
                <tr onclick="insertWhs('<?php echo $d->vcName; ?>');">
                  <td><?php echo $d->vcCode; ?></td>
                  <td><?php echo $d->vcName; ?></td>
				  <td><?php echo $d->vcCity; ?></td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
              </div>
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
         </div>
       </div>
     </div>
   </div>