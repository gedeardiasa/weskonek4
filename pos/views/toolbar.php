<div class="col-sm-12 hidden-xs" align="right">
	<i class="fa fa-file-o fa-2x" aria-hidden="true" onclick="initialadd()" data-toggle="tooltip" data-placement="top" title="New"></i>
	<i class="fa fa-files-o fa-2x" aria-hidden="true" onclick="toolbar_duplicate()" data-toggle="tooltip" data-placement="top" title="Duplicate"></i>
	<i class="fa fa-print fa-2x" aria-hidden="true" onclick="toolbar_print()" data-toggle="tooltip" data-placement="top" title="Print"></i>&nbsp;&nbsp;
	<i class="fa fa-envelope-o fa-2x" aria-hidden="true" onclick="toolbar_mail()" data-toggle="tooltip" data-placement="top" title="Send Mail"></i>&nbsp;&nbsp;
	<i class="fa fa-file-excel-o fa-2x" aria-hidden="true" onclick="toolbar_excel()" data-toggle="tooltip" data-placement="top" title="Excel"></i>&nbsp;&nbsp;
	<i class="fa fa-file-word-o fa-2x" aria-hidden="true" onclick="toolbar_word()" data-toggle="tooltip" data-placement="top" title="Word"></i>&nbsp;&nbsp;
	<i class="fa fa-file-pdf-o fa-2x" aria-hidden="true" onclick="toolbar_pdf()" data-toggle="tooltip" data-placement="top" title="PDF"></i>&nbsp;&nbsp;
	<i class="fa fa-arrow-left fa-2x" aria-hidden="true" onclick="toolbar_previous()" data-toggle="tooltip" data-placement="top" title="Previous"></i>
	<i class="fa fa-arrow-right fa-2x" aria-hidden="true" onclick="toolbar_next()" data-toggle="tooltip" data-placement="top" title="Next"></i>
	<i class="fa fa-refresh fa-2x" aria-hidden="true" onclick="toolbar_reload()" data-toggle="tooltip" data-placement="top" title="Refresh"></i>&nbsp;&nbsp;
	<i align="right"class="fa fa-minus fa-2x" aria-hidden="true" onclick="toolbar_cancel()" data-toggle="tooltip" data-placement="top" title="Back"></i>&nbsp;&nbsp;
</div>


<div class="col-sm-12 hidden-sm hidden-md hidden-lg" align="right">
	<i class="fa fa-file-o fa-2x" aria-hidden="true" onclick="initialadd()" data-toggle="tooltip" data-placement="top" title="New"></i>
	<i class="fa fa-files-o fa-2x" aria-hidden="true" onclick="toolbar_duplicate()" data-toggle="tooltip" data-placement="top" title="Duplicate"></i>
	<i class="fa fa-arrow-left fa-2x" aria-hidden="true" onclick="toolbar_previous()" data-toggle="tooltip" data-placement="top" title="Previous"></i>
	<i class="fa fa-arrow-right fa-2x" aria-hidden="true" onclick="toolbar_next()" data-toggle="tooltip" data-placement="top" title="Next"></i>
	<i class="fa fa-refresh fa-2x" aria-hidden="true" onclick="toolbar_reload()" data-toggle="tooltip" data-placement="top" title="Refresh"></i>&nbsp;&nbsp;
	<i align="right"class="fa fa-minus fa-2x" aria-hidden="true" onclick="toolbar_cancel()" data-toggle="tooltip" data-placement="top" title="Back"></i>&nbsp;&nbsp;
</div>


<script>
function toolbar_duplicate() 
{				
	var idHeader = $("#idHeader").val();
	if(idHeader==0)
	{
		//alert('xxx');
	}
	else
	{
		initialduplicate(idHeader);
	}
	
}
function toolbar_print() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/prints/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_print', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('Toolbar type is not set');
	<?php } ?>
}
function toolbar_mail() 
{
	alert('Send Mail');
}
function toolbar_excel() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/excel/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_excel', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('Toolbar type is not set');
	<?php } ?>
}
function toolbar_word() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/word/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_word', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('Toolbar type is not set');
	<?php } ?>
}
function toolbar_pdf() 
{				
	var IdToolbar=$("#IdToolbar").val();
	<?php if(isset($typetoolbar)){ ?>
	window.open('<?php echo base_url() ?>index.php/toolbar/pdf/'+IdToolbar+'?type=<?php echo $typetoolbar ?>', 'toolbar_pdf', 'width=1380,height=600,left=0,top=50,titlebar=no,toolbar=no,location=no, addressbar=no');
	<?php }else{ ?>
	alert('Toolbar type is not set');
	<?php } ?>
}
function toolbar_previous() 
{
	var idHeader = $("#idHeader").val();
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/toolbar/previous", 
		data: "idHeader="+idHeader+"&type=<?php echo $typetoolbar ?>",  
		cache: false, 
		success: function(data){ 

			if(data!=0 && data!='0')
			{
				initialedit(data);
			}
		} 
	}); 
}
function toolbar_next() 
{
	var idHeader = $("#idHeader").val();
	$.ajax({ 
		type: "POST",
		url: "<?php echo $this->config->base_url()?>index.php/toolbar/next", 
		data: "idHeader="+idHeader+"&type=<?php echo $typetoolbar ?>",  
		cache: false, 
		success: function(data){ 
			if(data!=0 && data!='0')
			{
				initialedit(data);
			}
		} 
	}); 
}
function toolbar_reload() 
{
	var idHeader = $("#idHeader").val();
	if(idHeader==0)
	{
		initialadd();
	}
	else
	{
		initialedit(idHeader);
	}
	
}
function toolbar_cancel() 
{
	$("#modal-add-edit").modal('hide');
}

</script>