	<div class="modal fade" id="modal-tutorial-dashboard">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang Dashboard. Dashboard merupakan seluruh laporan baik berupa grafik ataupun data table 
				yang ada di menu dashboard (Menu awal saat login).
				Anda tidak bisa melakukan penambahan, pengurangan, ataupun menghapus Dashboard yang ada. Anda hanya bisa melihat Dashboard apa saja yang ada pada aplikasi ini
				</p>
				Untuk melihat data Dashboard anda bisa masuk menu <b>Administration-Dashboard</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menudashboard.png"></center>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/dashboard";
	
}
</script>