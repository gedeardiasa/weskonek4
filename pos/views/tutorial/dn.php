	<div class="modal fade" id="modal-tutorial-dn">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">DN Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu DN (Delivery). Menu ini digunakan untuk mengirim barang pesanan yang sudah masuk dari customer.
				Untuk menbuat DN anda bisa masuk menu <b>Sales-Delivery</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menudn.png"></center>
				Berikut adalah tahap-tahap untuk membuat DN :
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"></li>
				<li>Isikan terlebih dahulu customer yang akan dikirimkan barangnya pada field "Business Partner Code" atau "Business Partner Name"</li>
				<li>Untuk menampilkan semua BP yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalbp.png">, lalu klik pada BP yang dikehendaki</li>
				<li>Field Ref. Number merupakan field untuk mengisikan nomer referensi pembuatan DN (kosongi jika tidak ada)</li>	
				<li>Isikan "Doc. Date" sesuai tanggal DN dibuat</li>
				<li>Isikan "Del. Date" sesuai tanggal barang dikirim</li>
				<li>Anda bisa mengetikkan nama barang yang akan di pesan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectitemname.png"></li>
				<li>Atau jika anda sudah hafal kode barang anda bisa mengetikkan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectcode.png"></li>
				<li>Untuk menampilkan semua barang yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalitem.png">, lalu klik pada item yang dikehendaki</li>
				<li>Isikan jumlah barang yang akan dikirim pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/qty.png"></li>
				<li>Isikan UoM barang yang akan dikirim pada field "UoM"</li>
				<li>Isikan harga barang yang akan dikirim pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/price.png"></li>
				<li>Isikan diskon barang yang akan dikirim pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/discount.png"></li> / Kosongi jika tidak ada diskon
				<li>Isikan lokasi barang yang akan dikirim pada field "Warehouse"</li>
				<li>Setelah itu tekan "Enter" / klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/add.png"></li>
				<li>Item yang sudah anda pilih akan muncul di bawah</li>
				<li>Untuk mengubah data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/iconedit.png"></li>
				<li>Untuk menghapus data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/icondelete.png"></li>
				<li>Jika data dirasa sudah benar klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"></li>
				
				Anda juga bisa membuat DN melalui referensi dari dokumen SQ ataupun SO. Caranya adalah klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/copyfrom.png">
				pilih "Sales Quotation" / "Sales Order" lalu pilih nomer dokumen SQ/ SO yang ada. Secara otomatis data pada dokumen SQ/ SO akan tercopy otomatis pada dokumen DN
				<br><br>
				<b>Note : pembuatan DN akan mengurangi stok yang ada pada gudang yang dipilih.</b>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/dn";
	
}
</script>