	<div class="modal fade" id="modal-tutorial-form">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang form. Form merupakan seluruh menu yang ada di bagian samping aplikasi.
				Anda tidak bisa melakukan penambahan, pengurangan, ataupun menghapus form yang ada. Anda hanya bisa melihat form apa saja yang ada pada aplikasi ini
				</p>
				Untuk melihat data Form anda bisa masuk menu <b>Administration-Form</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuform.png"></center>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/form";
	
}
</script>