
	<div class="modal fade" id="modal-tutorial-import">
     <div class="modal-dialog">
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Import Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang Import. Import merupakan menu untuk melakukan import data dari excel ke dalam sistem.
				Ada beberapa jenis data yang bisa di import seperti Item Master Data, Business Partner, Price List dll.
				Untuk melakukan import data anda bisa masuk menu <b>Administration-Import Data</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuimport.png"></center>
				Pilih tipe data yang akan di import dan juga file excelnya. Untuk excel aplikasi ini hanya mendukung format .xls.
				Template excel juga bisa di download sebagai contoh excel yang akan di import.
				klik "Upload" untuk mengupload file excel yang sudah dipilih.
				Data excel yang sdh di upload akan muncul pada halaman selanjutnya. Jika dirasa data sudah sesuai klik "Execute", maka data akan masuk ke dalam sistem
				</p>
           
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/import";
	
}
</script>