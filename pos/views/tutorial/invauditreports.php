	<div class="modal fade" id="modal-tutorial-invauditreports">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inv. Audit Reports Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu Inv. Audit Reports. Menu ini digunakan untuk melihat mutasi barang yang ada di gudang.
				Untuk melihat mutasi barang anda bisa masuk menu <b>Inventory-Inventory Audit Reports</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuinvaudit.png"></center>
				Berikut adalah tahap-tahap untuk melihat mutasi barang :
				<li>Isikan kode item pada field "Item Code" untuk melihat mutasi barang yang di inginkan. (Anda dapat mengosonginya jika menginginkan semua barang ditampilkan)</li>
				<li>Isikan nama item pada field "Item Name" untuk melihat mutasi barang yang di inginkan. (Anda dapat mengosonginya jika menginginkan semua barang ditampilkan)</li>
				<li>Anda juga bisa memilih item group pada field "Item Group" jika menginginkan hanya item group tertentu yang ingin di tampilkan</li>
				<li>Isi tanggal mutasi yang di inginkan pada field "Post. Date"</li>
				<li>Centang lokasi gudang yang ingin di tampilkan pada field "Location"</li>
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/search.png"></li> untuk menampilkan mutasi
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/invauditreports";
	
}
</script>