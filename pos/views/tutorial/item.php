	<div class="modal fade" id="modal-tutorial-item">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Item Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur Item Master. Item Master adalah menu untuk mengelola master data barang anda.
				</p>
				Untuk melakukan perubahan atau menambah data Item Master anda bisa masuk menu <b>Inventory-Item Master</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuitem.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat Item Master.
				Isikan data-data yang ada.<br>
				berikut adalah rincian isian pada data master data :<br>
				<li>Category (Kategori Barang)</li>
				<li>Tax Group (Settingan default apakah barang ini dikenakan pajak atau tidak)</li>
				<li>Code (Kode barang. Sudah terisi otomatis dan tidak bisa dirubah)</li>
				<li>Barcode (Barcode pada barang. Kosongi jika tidak menggunakan barcode)</li>
				<li>Name (Nama Barang)</li>
				<li>Inventory UoM (Satuan Barang. Misal KG, Gram, PC, Liter, Dll)</li>
				<li>Default Price (Harga Jual Barang secara default)</li>
				<li>Purchase UoM (Satuan beli barang)</li>
				<li>Purchase Numerator (Konversi satuan beli barang dengan satuan barang. Misal satuan barang Gram, dan satuan beli barang KG, maka isi dengan 1000 yang artinya 1 KG = 1000 Gram)</li>
				<li>Sales UoM (Satuan jual barang)</li>
				<li>Sales Numerator (Konversi satuan jual barang dengan satuan barang. Misal satuan barang Gram, dan satuan jual barang KG, maka isi dengan 1000 yang artinya 1 KG = 1000 Gram)</li>
				<li>Remarks (Isi dengan keterangan barang. kosongi jika tidak memiliki keterangan)</li>
				<li>Sales Item ? (Centang jika barang ini bisa dijual)</li>
				<li>Purchase Item ? (Centang jika barang ini bisa dibeli)</li>
				<li>Allowed to sell goods even though the stock is not available? (Ini merupakan settingan barang apakah diijinkan untuk dijual meski stoknya 0)</li>
				
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"> untuk menambah data barang.
				</p>
				<p align="justify">
				Untuk merubah data Item Master yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data Item Master yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/item";
	
}
</script>