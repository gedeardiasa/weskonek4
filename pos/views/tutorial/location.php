	<div class="modal fade" id="modal-tutorial-location">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Location Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur location. Location adalah gudang tempat menyimpan barang yang anda dimiliki.</p>
				Untuk melakukan perubahan data location anda bisa masuk menu <b>Inventory-Location</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menulocation.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat location baru.
				Isikan data-data yang ada.
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				<p align="justify">
				Untuk merubah data location yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data location yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/location";
	
}
</script>