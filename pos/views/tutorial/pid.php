	<div class="modal fade" id="modal-tutorial-pid">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">PID Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu PID (Physical Inv. Document). Menu ini digunakan melakukan pencatatan stok barang sesuai dengan fisik (Stock Opname).
				Untuk melakukan pencatatan PID anda bisa masuk menu <b>Inventory-Physical Inv. Document</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menupid.png"></center>
				Berikut adalah tahap-tahap untuk mencatat PID :
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"></li>
				<li>Isikan "Doc. Date" sesuai tanggal barang dicatat</li>
				<li>Field Ref. Number merupakan field untuk mengisikan nomer referensi pencatatan barang (kosongi jika tidak ada)</li>
				<li>Isikan gudang barang yang akan di catat stoknya pada field "Warehouse"</li>
				<li>Anda bisa mengetikkan nama barang yang akan di adjustment pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectitemname.png"></li>
				<li>Atau jika anda sudah hafal kode barang anda bisa mengetikkan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectcode.png"></li>
				<li>Untuk menampilkan semua barang yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalitem.png">, lalu klik pada item yang dikehendaki</li>
				<li>Stok yang terdapat pada sistem akan muncul pada field "Qty Before"</li>
				<li>Isikan stok yang sesuai dengan kondisi fisik di lapangan pada field "Qty"</li>
				<li>Setelah itu tekan "Enter" / klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/add.png"></li>
				<li>Item yang sudah anda pilih akan muncul di bawah</li>
				<li>Untuk mengubah data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/iconedit.png"></li>
				<li>Untuk menghapus data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/icondelete.png"></li>
				<li>Jika data dirasa sudah benar klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"></li>
				
				<b>Note : pembuatan PID tidak akan langsung mengubah stok yang ada pada gudang karena dokumen ini hanya bersifat pencatatan saja. Untuk merubah stok yang ada bisa dilakukan di menu
				Inventory Posting dimana pembuatannya bisa langsung mereferensikan dokumen PID sehingga tidak perlu melakukan input 1 per 1 lagi.</b>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/pid";
	
}
</script>