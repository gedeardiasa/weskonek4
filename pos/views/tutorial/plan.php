	<div class="modal fade" id="modal-tutorial-plan">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Plan Tutorial</h3>
            </div>
            
              <div class="box-body">
			    <p align="justify">Selanjutnya adalah cara mengatur plan. Plan adalah menu untuk mengatur cabang perusahan yang anda punya.</p>
				Untuk melakukan perubahan data plan anda bisa masuk menu <b>Administrator-Plan</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuplan.png"></center>
				
				<p align="justify">Klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data profil anda, lalu klik tanda 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">. Isi sesuai data yang anda miliki lalu klik 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/plan";
	
}
</script>