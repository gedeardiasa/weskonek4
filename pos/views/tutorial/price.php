	<div class="modal fade" id="modal-tutorial-price">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Price Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur Price List. Price List adalah menu untuk mengelola harga pada barang yang anda jual.
				Anda dapat memiliki beberapa jenis harga pada 1 barang yang anda punya, misal harga retail, ecer atapun grosir.
				</p>
				Untuk melakukan perubahan data Price List anda bisa masuk menu <b>Inventory-Price List</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuprice.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat Price List baru.
				Isikan nama Price List anda. Pada bagian bawah nama Price List akan muncul semua item yang anda punya. Isikan harga pada item tersebut.
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				<p align="justify">
				Untuk merubah data Price List yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data Price List yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/price";
	
}
</script>