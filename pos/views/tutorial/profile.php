<style>
.modal-tuto {
  width: 90%;
  margin: auto;
}

</style>		
	<div class="modal fade" id="modal-tutorial-profile">
     <div class="modal-dialog modal-tuto">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Profile Tutorial</h3>
            </div>
            
              <div class="box-body" style="overflow:scroll; height:400px;">
				<h1>Hallo <?php echo $_SESSION['NamePOS'];?>...</h1><br>
			    <p align="justify">Kami lihat ini adalah pertama kalinya anda menggunakan aplikasi ini.
				Untuk itu kami akan mencoba membantu anda dalam penggunakan aplikasi ini.
				Pertama, anda perlu melakukan perubahan data pada profil usaha anda.</p>
				Untuk melakukan perubahan data profile anda bisa masuk menu <b>Administrator-Profile</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuprofile.png"></center>
				
				<p align="justify">Klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data profil anda, lalu klik tanda 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">. Isi sesuai data yang anda miliki lalu klik 
				<img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">
				</p>
				<!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    Hahahaha

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item">
                    OK

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item">
                    SIP

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>-->
            </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/profile";
	
}
</script>