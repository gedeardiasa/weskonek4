	<div class="modal fade" id="modal-tutorial-table">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Table Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur table.
				Menu ini dikhususkan untuk model bisnis food and beverage ataupun restaurant dimana ada data table (meja di lokasi bisnisnya)<br>
				<br>
				<b>Note: Jika model bisnis anda tidak ada table (meja) abaikan tutorial ini.</b>
				</p>
				Untuk melakukan perubahan data table anda bisa masuk menu <b>Administrator-Table</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menutable.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat table baru.
				Isikan nama table. Setelah itu kli Tab 'Plan'. Centang Plan/Cabang yang menggunakan table tersebut.
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				<p align="justify">
				Untuk merubah data table/meja yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data table yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/table";
	
}
</script>