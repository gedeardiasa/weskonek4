	<div class="modal fade" id="modal-tutorial-transfer">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Transfer Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah tutorial tentang menu Transfer. Menu ini digunakan untuk melakukan transfer barang antar gudang.
				Untuk melakukan transfer barang anda bisa masuk menu <b>Inventory-Transfer Stock</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menutransfer.png"></center>
				Berikut adalah tahap-tahap melakukan transfer barang :
				<li>Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"></li>
				<li>Isikan "Doc. Date" sesuai tanggal barang ditransfer</li>
				<li>Field Ref. Number merupakan field untuk mengisikan nomer referensi transfer barang (kosongi jika tidak ada)</li>
				<li>Isikan asal gudang barang yang akan di transfer pada field "From Warehouse"</li>
				<li>Isikan tujuan gudang barang yang akan di transfer pada field "To Warehouse"</li>
				<li>Pilih barang yang akan di transfer</li>
				<li>Anda bisa mengetikkan nama barang yang akan di transfer pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectitemname.png"></li>
				<li>Atau jika anda sudah hafal kode barang anda bisa mengetikkan pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/selectcode.png"></li>
				<li>Untuk menampilkan semua barang yang ada anda bisa klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/modalitem.png">, lalu klik pada item yang dikehendaki</li>
				<li>Isikan jumlah barang yang akan di transfer pada field <img src="<?php echo base_url(); ?>application/views/tutorial/img/qty.png"></li>
				<li>Setelah itu tekan "Enter" / klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/add.png"></li>
				<li>Item yang sudah anda pilih akan muncul di bawah</li>
				<li>Untuk mengubah data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/iconedit.png"></li>
				<li>Untuk menghapus data yang sudah anda pilih klik icon <img src="<?php echo base_url(); ?>application/views/tutorial/img/icondelete.png"></li>
				<li>Jika data dirasa sudah benar klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png"></li>
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/transfer";
	
}
</script>