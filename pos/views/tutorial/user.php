	<div class="modal fade" id="modal-tutorial-user">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">User Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur user. User adalah pengguna yang dapat menggunakan aplikasi anda.
				Anda dapat merubah nama, password dan data-data lain pada user yang anda punya
				</p>
				Untuk melakukan perubahan data user anda bisa masuk menu <b>Administrator-User</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuuser.png"></center>
				Klik User ID yang ingin anda ubah datanya<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/listuser.png"></center>
				Maka akan muncul detail dari User yang anda pilih.<br>
				Ada beberapa data yang bisa anda ubah pada user yaitu,
				<li>User Data (Terdiri dari nama, email, dan nomer telepon)</li>
				<li>Change Password (Untuk merubah password user)</li>
				<li>Form Access (Untuk mengatur access yang di diperbolehkan pada user tersebut. Form access merupakan menu yang ada di sidebar aplikasi)</li>
				<li>Top Form Access (Untuk mengatur access juga. Untuk access yang ini merupakan menu yang ada di atas aplikasi)</li>
				<li>Plan Access (Mengatur cabang mana saja yang diperbolehkan untuk user tersebut)</li>
				<li>Dashboard (Mengatur dashboard apa saja yang bakal tampil di halaman dashboard)</li>
				<li>Setting (Mengatur beberapa aturan pada user tersebut seperti, default location, default wallet, default BP, dll)</li>
				<li>Activity (Untuk melihat aktifitas user tersebut pada aplikasi ini)</li>
				<br><br>
				Jika perubahan dirasa sudah cukup anda perlu menekan tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/user";
	
}
</script>