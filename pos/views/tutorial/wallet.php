	<div class="modal fade" id="modal-tutorial-wallet">
     <div class="modal-dialog" >
       <div class="modal-content" >
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Wallet Tutorial</h3>
            </div>
            
              <div class="box-body">
				<p align="justify">Selanjutnya adalah cara mengatur Wallet. Wallet (Dompet) adalah menu untuk mengelola jenis dompet pada usaha anda.
				Wallet / Dompet disini bisa diartikan sebagai media untuk menyimpan uang pada usaha anda. Misal anda memiliki uang di kasir, pada bank, dan juga pada dompet pribadi anda.
				Maka anda wajib memiliki 3 wallet yaitu kasir, bank, dan dompet pribadi.
				</p>
				Untuk melakukan perubahan atapun penambahan data Wallet anda bisa masuk menu <b>Banking-Wallet</b>.<br>
				<center><img src="<?php echo base_url(); ?>application/views/tutorial/img/menuwallet.png"></center>
				<p align="justify">
				Klik tombol <img src="<?php echo base_url(); ?>application/views/tutorial/img/addnew.png"> untuk membuat Wallet baru.
				Isikan kode, nama dan pada cabang mana wallet/dompet tersebut.
				Setelah itu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
				<p align="justify">
				Untuk merubah data Wallet yang ada klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/expand.png"> pada data Wallet yang akan dirubah.
				lalu klik tanda <img src="<?php echo base_url(); ?>application/views/tutorial/img/show.png">.
				Ubah data sesuai yang diinginkan lalu klik <img src="<?php echo base_url(); ?>application/views/tutorial/img/savechanges.png">.
				</p>
              </div>
          </div>
         </div>
         <div class="modal-footer">
		   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   
		   <input type="checkbox" id="skiptutorial" name="skiptutorial"> Skip All Tutorial?
           <button type="button" class="btn btn-primary" onclick="nexttutorial()">Next</button>
         </div>
       </div>
     </div>
   </div>
   
<script>
function nexttutorial()
{
	var val=document.getElementById('skiptutorial').checked;
	if(val==true)
	{
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/welcome/skiptutorial", 
			data: "", 
			cache: true, 
			success: function(data){ 
			},
			async: false
		});
	}
	window.location.href = "<?php echo base_url(); ?>index.php/wallet";
	
}
</script>