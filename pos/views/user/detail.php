<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>


    <script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
</head>
  <?php $this->load->view('body'); ?>
<style>
.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
</style>
<div class="wrapper">
<div class="modal fade" id="modal-delete">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body" align="center">
           <h2>Are you sure want to delete this data?</h2>
         </div>
         <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesdelete/<?php echo $user->intID; ?>"><button type="button" class="btn btn-danger">Delete</button></a>
         </div>
       </div>
     </div>
</div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>


    <!-- Main content -->
    <section class="content">
	<div class="row">
	<form role="form" method="POST" enctype="multipart/form-data"
		action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesedit/<?php echo $user->intID;?>">
        <div class="col-md-3">
		<!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
			  <div class="image-upload" align="center">
			  <label for="Image">
			  <?php if($user->blpImage!=null){echo '<img id="blah" src="data:image/jpeg;base64,'.base64_encode( $user->blpImage ).'" style="width:100px;height:100px" alt="User profile picture" class="profile-user-img img-responsive img-circle"/>';}else{ ?>
              <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" class="profile-user-img img-responsive img-circle" alt="User profile picture">
			  <?php } ?>
			  </label>
			  <input type="file" id="Image" name="Image" onchange="readURL(this);" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
			  </div>

              <h3 class="profile-username text-center"><?php echo $user->vcName;?></h3>

              <p class="text-muted text-center"><?php echo $user->vcEmail;?></p>

              <!--<ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>-->
			
              <!--<button data-toggle="modal" data-target="#modal-delete" class="btn btn-danger btn-block" <?php if($crudaccess->intDelete==0) { echo 'disabled="true"';}?>><b>Delete</b></button>-->
            </div>
          </div>
			<!-- About Me Box -->
          <!--<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <!--<div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          <!--</div>
          <!-- /.box -->
		</div>
		
		<div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#userdata" data-toggle="tab">User Data</a></li>
              <li><a href="#password" data-toggle="tab">Change Password</a></li>
              <li><a href="#access" data-toggle="tab">Form Access</a></li>
			  <li><a href="#accesstop" data-toggle="tab">Top Form Access</a></li>
			  <li><a href="#accessplan" data-toggle="tab">Plan Access</a></li>
			  <li><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
			  <li><a href="#setting" data-toggle="tab">Setting</a></li>
			  <li><a href="#activity" data-toggle="tab">Activity</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="userdata">
                <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['passworderror'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> Edit Password (Confirm Password and New Password not same).
                </div>
				
				<?php } ?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				
					<div class="form-group">
					  <label for="UserID">User ID</label>
					  <input type="text" class="form-control" id="UserID" name="UserID" maxlength="25" readonly="true" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $user->vcUserID;?>" required="true" placeholder="Enter user id">
					</div>
					<div class="form-group">
					  <label for="Email">Email address</label>
					  <input type="email" class="form-control" id="Email" name="Email"	 maxlength="50" readonly="true" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $user->vcEmail;?>" required="true"  placeholder="Enter email">
					</div>
					<div class="form-group">
					  <label for="Name">Name</label>
					  <input type="text" class="form-control" id="Name" name="Name" required="true" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $user->vcName;?>" maxlength="100" placeholder="Enter name">
					</div>
					<div class="form-group">
					  <label for="Name">Phone</label>
					  <input type="text" class="form-control" id="Phone" name="Phone" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> value="<?php echo $user->vcPhone;?>" maxlength="50" placeholder="Enter phone">
					</div>
				
                </div>
              </div>
              <div class="tab-pane" id="password">
                <div class="box-body">
				
					<!--<div class="form-group">
					  <label for="OldPassword">Old Password</label>
					  <input type="password" class="form-control" id="OldPassword" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="OldPassword" maxlength="50" placeholder="Password">
					</div>-->
					<div class="form-group">
					  <label for="NewPassword">New Password</label>
					  <input type="password" class="form-control" id="NewPassword" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="NewPassword" maxlength="50" placeholder="Password">
					</div>
					
					<div class="form-group">
					  <label for="ConfirmPassword">Confirm Password</label>
					  <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> maxlength="50" placeholder="Password">
					</div>
                </div>
              </div>
			 
              <div class="tab-pane" id="access">
				
               
				<table class="table table-striped dt-responsive jambo_table tree">
                    <tr id="trtableaccessgeneral">
                        <th>Name </th>
						<th>Control </th>
						<th>Create </th>
						<th> 
						<div class="col-sm-4">Read</div>
						<div class="col-sm-4">Update</div>
						<div class="col-sm-4">Delete</div>
						</th>
						
                    </tr>
					
					<?php 
					$db=$this->load->database('default', TRUE);
					foreach($accessuserHead->result() as $h) 
					{
						$iduser=$user->intID;
						$openform='';
						$idheader=$h->intID;
						$cekopenform=$db->query("
						SELECT 
						a.*
						FROM maccess a 
						LEFT JOIN mform b ON a.`intForm`=b.`intID`
						WHERE a.`intUserID`='$iduser' AND b.`intHeader`='$idheader' order by b.intID asc
						");
						
						if($cekopenform->num_rows()>0)
						{
							$openform='checked=checked';
						}
					?>	
                    <tr class="treegrid-<?php echo $h->intID; ?> <?php if($h->intHeader!=0){echo 'treegrid-parent-'.$h->intHeader.'';}; ?>">
                        <td><?php echo $h->vcName; ?></td>
						
						<td><input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="<?php echo $h->vcCode; ?>" onclick="cekhead('<?php echo $h->intID; ?>')" id="<?php echo $h->intID; ?>" <?php echo $openform; ?>></td>
						<td></td>
						<td></td>
						
					</tr>
					<?php 
						foreach($accessuser->result() as $g) 
						{
							if($g->intHeader==$h->intID)
							{
							?>	
							  <tr class="treegrid-<?php echo $g->intID; ?> <?php if($g->intHeader!=0){echo 'treegrid-parent-'.$g->intHeader.'';}; ?>">
								<td><?php echo $g->vcName; ?></td>
								
								<td>
								
								<input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="<?php echo $g->vcCode; ?>" onclick="cekdetail('<?php echo $h->intID; ?>','<?php echo $g->intID; ?>')" id="<?php echo $g->intID; ?>" <?php echo $g->OpenForm; ?>>
								
								</td>
								<td>
								
								<input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="CF<?php echo $g->vcCode; ?>" id="CF<?php echo $g->intID; ?>" <?php echo $g->OpenFormC; ?> <?php echo $g->DisabledForm; ?>>
								</td>
								<td>
								
								
								
								<div class="col-sm-4">
								<input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="RF<?php echo $g->vcCode; ?>" id="RF<?php echo $g->intID; ?>" <?php echo $g->OpenFormR; ?> <?php echo $g->DisabledForm; ?>>
								
								
								</div>
								<div class="col-sm-4">
								<input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="UF<?php echo $g->vcCode; ?>" id="UF<?php echo $g->intID; ?>" <?php echo $g->OpenFormU; ?> <?php echo $g->DisabledForm; ?>>
								
								
								</div>
								<div class="col-sm-4">
								<input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="DF<?php echo $g->vcCode; ?>" id="DF<?php echo $g->intID; ?>" <?php echo $g->OpenFormD; ?> <?php echo $g->DisabledForm; ?>>
								
								
								</div>
								
								</td>
								
							  </tr>
							<?php 
								$gintID=$g->intID;
								$iduser=$user->intID;
								$submenu=$db->query("
								SELECT a.* , b.FormID,
								CASE WHEN b.FormID>0 THEN 'checked=checked' ELSE '' END AS OpenForm
								FROM msubform a 

								LEFT JOIN
										(
											SELECT 
											c.`intID` AS FormID
											FROM muser a LEFT JOIN 
											(maccesssub b LEFT JOIN msubform c ON b.`intForm`=c.`intID`) ON a.`intID`=b.`intUserID`
											WHERE a.intID='$iduser'
										) b ON a.`intID`=b.FormID
								WHERE a.intHeader='$gintID' and a.intDeleted=0
								");
								
								foreach($submenu->result() as $m)
								{
									?>
									<tr class="treegrid-<?php echo $g->intID."".$m->intID; ?> <?php if($g->intHeader!=0){echo 'treegrid-parent-'.$m->intHeader.'';}; ?>">
									<td><?php echo $m->vcName; ?></td>
									
									<td><input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="subcode-<?php echo $g->vcCode."".$m->vcCode; ?>" id="<?php echo $g->intID."".$m->intID; ?>" <?php echo $m->OpenForm; ?>></td>
								  </tr>
									<?php
								}
							}
							
						}
					}
					?>

                  </table>
              </div>
			  <div class="tab-pane" id="accessplan">
				<table class="table table-striped dt-responsive jambo_table tree">
                    <tr>
                        <th>Name </th>
						<th>Control </th>
                    </tr>
					<?php
					$db=$this->load->database('default', TRUE);
					foreach($accessuserPlan->result() as $h) 
					{
						$iduser=$user->intID;
						$openplan='';
						$idplan=$h->intID;
						$cekopenform=$db->query("
						SELECT 
						a.*
						FROM maccessplan a 
						LEFT JOIN mplan b ON a.`intPlan`=b.`intID`
						WHERE a.`intUserID`='$iduser' and a.intPlan='$idplan'
						");
						
						
						if($cekopenform->num_rows()>0)
						{
							$openplan='checked=checked';
						}
					?>	
                    <tr>
                        <td><?php echo $h->vcName; ?></td>
						
						<td><input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="plan<?php echo $h->vcCode; ?>" id="plan<?php echo $h->intID; ?>" <?php echo $openplan; ?>></td>
                    </tr>
					<?php
					}
					?>
				</table>
			  </div>
			  <div class="tab-pane" id="accesstop">
				<table class="table table-striped dt-responsive jambo_table tree">
                    <tr>
                        <th>Name </th>
						<th>Control </th>
                    </tr>
					<?php
					$db=$this->load->database('default', TRUE);
					foreach($accessuserTop->result() as $h) 
					{
						$iduser=$user->intID;
						$openform='';
						$idform=$h->intID;
						$cekopenform=$db->query("
						SELECT 
						a.*
						FROM maccesstop a 
						LEFT JOIN mtopform b ON a.`intForm`=b.`intID`
						WHERE a.`intUserID`='$iduser' and a.intForm='$idform'
						");
						
						
						if($cekopenform->num_rows()>0)
						{
							$openform='checked=checked';
						}
					?>	
                    <tr>
                        <td><?php echo $h->vcName; ?></td>
						
						<td><input type="checkbox" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?> name="topform<?php echo $h->vcCode; ?>" id="topform<?php echo $h->intID; ?>" <?php echo $openform; ?>></td>
                    </tr>
					<?php
					}
					?>
				</table>
			  </div>
			  <div class="tab-pane" id="dashboard">
                <div class="box-body">
					<table class="table table-striped dt-responsive jambo_table tree">
						<tr>
						  <th>Set </th>
						  <th>Dashboard </th>
						  <th>Order </th>
						 
						</tr>
						<?php foreach($accessdash->result() as $h) {?>
						<tr>
						  <td class=" "><input type="checkbox" class="flat" name="<?php echo $h->vcCode; ?>" <?php echo $h->OpenDashboard; ?>></td>
						  <td class=" "><a href="javascript:window.open('<?php echo base_url(); ?>index.php/dashboard/cartdetail/<?php echo $h->intID; ?>', 'cartdetail_<?php echo $user->intID; ?>', 'width=1100,height=400,left=100,top=100')">
						  <?php echo $h->vcName; ?></a></td>
						  <td class=" "><input type="number" name="<?php echo $h->vcCode; ?>order" value="<?php echo $h->intOrder; ?>" style="width:50px"></td>
						</tr>
						<?php } ?>
					</table>
                </div>
              </div>
			  <div class="tab-pane" id="setting">
				<div class="box-body">
					<div class="form-group">
					  <label for="DefaultLoc">Default Location</label>
					  <select class="form-control" id="DefaultLoc" name="DefaultLoc" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="0" >Not Set</option>
					  <?php 
					  foreach($listloc->result() as $d) 
					  {
					  ?>	
					  <option value="<?php echo $d->intID;?>" <?php if($d->intID==$user->intDefaultLoc){ echo 'selected';}?>><?php echo $d->vcName;?></option>
					  <?php 
					  }
					  ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="DefaultBP">Default BP</label>
					  <select class="form-control" id="DefaultBP" name="DefaultBP" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="0" >Not Set</option>
					  <?php 
					  foreach($listbp->result() as $d) 
					  {
					  ?>	
					  <option value="<?php echo $d->intID;?>" <?php if($d->intID==$user->intDefaultBP){ echo 'selected';}?>><?php echo $d->vcName;?></option>
					  <?php 
					  }
					  ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="DefaultBP">Default Wallet</label>
					  <select class="form-control" id="DefaultWallet" name="DefaultWallet" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="0" >Not Set</option>
					  <?php 
					  foreach($listwallet->result() as $d) 
					  {
					  ?>	
					  <option value="<?php echo $d->intID;?>" <?php if($d->intID==$user->intDefaultWallet){ echo 'selected';}?>><?php echo $d->vcName;?></option>
					  <?php 
					  }
					  ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="SetValue">Allow Display Stock Value</label>
					  <select class="form-control" id="SetValue" name="SetValue" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="0" <?php if($user->intValue==0){ echo 'selected';}?>>Not Allow</option>
					  <option value="1" <?php if($user->intValue==1){ echo 'selected';}?>>Allow</option>
					  
					  </select>
					</div>
					<div class="form-group">
					  <label for="SetProfit">Allow Display Profit</label>
					  <select class="form-control" id="SetProfit" name="SetProfit" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="0" <?php if($user->intProfit==0){ echo 'selected';}?>>Not Allow</option>
					  <option value="1" <?php if($user->intProfit==1){ echo 'selected';}?>>Allow</option>
					  
					  </select>
					</div>
					<div class="form-group">
					  <label for="SetIcon">Icon Size</label>
					  <select class="form-control" id="SetIcon" name="SetIcon" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>
					  <option value="" <?php if($user->vcIcon==''){ echo 'selected';}?>>Small</option>
					  <option value="fa-2x" <?php if($user->vcIcon=='fa-2x'){ echo 'selected';}?>>Medium</option>
					  
					  </select>
					</div>
					
					
					
				</div>
			  </div>
			  <div class="tab-pane" id="activity">
				<div class="box-body">
				<?php $this->load->view('loadingbar2'); ?>
					<div class="row">
					<div class="form-group">
						<div class="col-sm-4">
						  <label for="daterange">Date</label>
						  <input type="text" class="form-control pull-right" id="daterange" name="daterange" readonly="true" style="background-color:#ffffff">
						<br>
						<button type="button" class="btn btn-primary" onclick="loadactivity()">Reload</button>
						</div>
					</div>
					</div>
					<hr>
					<div id="mix_header"></div>
				</div>
			  </div>
			  <button type="submit" class="btn btn-primary" <?php if($crudaccess->intUpdate==0) { echo 'disabled="true"';}?>>Save changes</button>
            </div>
          </div>
        </div>
		</form>
	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
$(document).ready(function() {
		$('.tree').treegrid();
		$('.tree').treegrid({
          'initialState': 'expanded',
        });
		$('#daterange').daterangepicker({
			showDropdowns: true,
			locale: {
					format: 'YYYY/MM/DD'
			},
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		});
		document.getElementById("daterange").value = lastWeekT1+' - '+todayT1;
       
    });
function loadactivity()
{
	$(".loadingbar2").show();
	var daterange = $("#daterange").val();
	daterange = daterange.split(' ').join('');
	$('#mix_header').load('<?php echo $this->config->base_url()?>index.php/<?php echo $this->uri->segment(1);?>/lisActivity?daterange='+daterange+'&iduser=<?php echo $user->intID;?>', function(){
		$(".loadingbar2").hide();
	});   
}
function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	var fileInput = document.getElementById('Image');
    var filePath = fileInput.value;
	var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb');
		document.getElementById("Image").value = "";
	}
	else
	{
		if(!allowedExtensions.exec(filePath)){
			alert('Please upload file having extensions .jpeg/.jpg/.png only.');
			fileInput.value = '';
			return false;
		}else{
			reader.onload = function (e) {
				$('#blah')
					.attr('src', e.target.result)
					.width(50)
					.height(50);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

         
   }
}
function cekhead(id)
{
	
	var form = new Array;
	<?php 
	$db=$this->load->database('default', TRUE);
	foreach($accessuserHead->result() as $h) 
	{
		?>
		form[<?php echo $h->intID;?>] = new Array;
		<?php
		$i=0;
		foreach($accessuser->result() as $g) 
		{
			if($g->intHeader==$h->intID)
			{
				?>
				form[<?php echo $h->intID;?>][<?php echo $i;?>]=<?php echo $g->intID;?>;
				<?php
				$i++;
			}
		}
	}
	?>	
	//alert('xxx');
	//alert(form[id].length);
	var val=document.getElementById(id).checked;
	
	for(var v=0;v<form[id].length;v++)
	{
		
		document.getElementById(form[id][v]).checked=val;
		cekdetail(id,form[id][v]);
	}
}
function cekdetail(id,code)
{
	var form = new Array;
	<?php 
	$db=$this->load->database('default', TRUE);
	foreach($accessuserHead->result() as $h) 
	{
		?>
		form[<?php echo $h->intID;?>] = new Array;
		<?php
		$i=0;
		foreach($accessuser->result() as $g) 
		{
			if($g->intHeader==$h->intID)
			{
				?>
				form[<?php echo $h->intID;?>][<?php echo $i;?>]=<?php echo $g->intID;?>;
				<?php
				$i++;
			}
		}
	}
	?>
	var hasil=false;
	for(var v=0;v<form[id].length;v++)
	{
		if(document.getElementById(form[id][v]).checked==true)
		{
			hasil=true;
		}
	}
	document.getElementById(id).checked=hasil;
	
	//
	var df = document.getElementById(""+code+"").checked;
	if(df==true)
	{
		document.getElementById("CF"+code+"").disabled= false;
		document.getElementById("RF"+code+"").disabled= false;
		document.getElementById("UF"+code+"").disabled= false;
		document.getElementById("DF"+code+"").disabled= false;
	}
	else
	{
		document.getElementById("CF"+code+"").disabled= true;
		document.getElementById("RF"+code+"").disabled= true;
		document.getElementById("UF"+code+"").disabled= true;
		document.getElementById("DF"+code+"").disabled= true;
	}
}

</script>
</body>
</html>
