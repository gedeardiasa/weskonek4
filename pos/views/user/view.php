<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">

  <div class="modal fade" id="modal-add">
  <form role="form" method="POST" enctype="multipart/form-data"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesadd/">
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New User</h3>
            </div>
            
              <div class="box-body">
				<div class="form-group">
                  <label for="UserID">User ID</label>
                  <input type="text" class="form-control" id="UserID" name="UserID" maxlength="25" required="true" placeholder="Enter user id">
                </div>
                <div class="form-group">
                  <label for="Email">Email address</label>
                  <input type="email" class="form-control" id="Email" name="Email" maxlength="50" required="true"  placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" id="Password" name="Password" required="true"  maxlength="50" placeholder="Password">
                </div>
				<div class="form-group">
                  <label for="Name">Name</label>
                  <input type="text" class="form-control" id="Name" name="Name" required="true"  maxlength="100" placeholder="Enter name">
                </div>
                <div class="form-group">
				  
                  <label for="exampleInputFile">Avatar</label><br>
				  <img id="blah" src="<?php echo base_url(); ?>data/general/dist/img/people.jpg" width="35px" height="35px" class="img-circle" alt="User Image">
                  <input type="file" id="Avatar" name="Avatar" onchange="readURL(this);">

                </div>
                
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary">Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <!--<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#modal-add" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>
				  <span class="glyphicon glyphicon-plus"></span> <?php echo 'Add New'; ?></button>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($listuser->result() as $d) 
				{
				?>
                <tr>
				  <?php if($crudaccess->intRead==1) { ?>
				  <td><a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>"><?php echo $d->vcUserID; ?></a></td>
				  <?php }else{ ?>
				  <td><?php echo $d->vcUserID; ?></td>
				  <?php } ?>
                  
                  <td><?php echo $d->vcEmail; ?></td>
				  <td><?php echo $d->vcName; ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <a href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/detail/<?php echo $d->intID;?>">
				  <i class="fa fa-search <?php echo $usericon;?>" aria-hidden="true"></i></a>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
  $(function () {
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
	if(input.files[0].size>256000)
	{
		alert('ERROR!! Max Size 256kb')
	}
	else
	{
		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result)
				.width(50)
				.height(50);
		};
	}

         reader.readAsDataURL(input.files[0]);
   }
  }
 
</script>
</body>
</html>
