<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('T1'); ?>
  
</head>
  <?php $this->load->view('body'); ?>

<div class="wrapper">
  
  
  
  <div class="modal fade" id="modal-addedit">
  <form role="form" method="POST" onsubmit="return checkform()"
	action="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1);?>/prosesaddedit">
	<input type="hidden" class="form-control" id="ID" name="ID" maxlength="25" >
	<input type="hidden" class="form-control" id="type" name="type" maxlength="25" >
     <div class="modal-dialog">
       <div class="modal-content">
        
         <div class="modal-body">
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title" id="tittleAdd">Add New Transfer</h3>
			  <h3 class="box-title" id="tittleEdit">View Transfer</h3>
            </div>
            
              <div class="box-body">
				<div class="form-group">
                  <label for="DocNum">Doc. Number</label>
                  <input type="text" class="form-control" id="DocNum" name="DocNum" maxlength="25" required="true" readonly="true" placeholder="Enter Doc. Number">
                </div>
				<div class="form-group">
					<label>Doc. Date</label>
					  <div class="input-group date">
						<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
						</div>
					    <input type="text" class="form-control" id="DocDate" name="DocDate" data-toggle="tooltip" data-placement="top" title="Document Date">
					  </div>
				</div>
				<div class="form-group">
				  <label for="WalletFrom">Wallet (From)</label>
				  <select class="" id="WalletFrom" name="WalletFrom" style="width: 100%; height:35px">
				  <?php 
				  foreach($liswallet->result() as $d) 
				  {
				  ?>	
				  <option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
				  <?php 
				  }
				  ?>
				  </select>
				</div>
				<div class="form-group">
				  <label for="WalletTo">Wallet (To)</label>
				  <select class="" id="WalletTo" name="WalletTo" style="width: 100%; height:35px">
				  <?php 
				  foreach($liswallet->result() as $d) 
				  {
				  ?>	
				  <option value="<?php echo $d->intID;?>" ><?php echo $d->vcName;?></option>
				  <?php 
				  }
				  ?>
				  </select>
				</div>
				<div class="form-group">
                  <label for="Value">Value</label>
                  <input type="number" class="form-control" id="Value" name="Value" maxlength="11" required="true" placeholder="Enter Value">
                </div>
                <div class="form-group">
                  <label for="Remarks">Remarks</label>
                  <textarea class="form-control" rows="3" id="Remarks" name="Remarks" placeholder="Remarks ..."></textarea>
                </div>
              </div>
            
          </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
		   <button type="submit" class="btn btn-primary" id="addbutton" <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>>Save changes</button>
         </div>
       </div>
     </div>
   </form>
   </div>
  
  <?php $this->load->view('header'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php $this->load->view('navigation'); ?>

    <!-- Main content -->
    <section class="content">
	<div class="box">
            <div class="box-header">
                  <button type="button" class="btn btn-dark" onclick="add()"  data-toggle="modal" data-target="#modal-addedit"
				  <?php if($crudaccess->intCreate==0) { echo 'disabled="true"';}?>><span class="glyphicon glyphicon-plus"></span> 
				  <?php echo 'Add New'; ?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?php if(isset($_GET['success'])){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Success!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				<?php if(isset($_GET['error'])){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error!</strong> <?php echo $message;?>.
                </div>
				<?php }?>
				
				<table id="example1" class="table table-striped dt-responsive jambo_table">
                <thead>
                <tr>
                  <th>Doc. Number </th>
				  <th>Doc. Date </th>
				  <th>Wallet (From)</th>
				  <th>Wallet (To)</th>
				  <th>Value </th>
                  <th style="width:15px">Control</th>
                 
                </tr>
                </thead>
                <tbody>
				<?php 
				foreach($list->result() as $d) 
				{
				?>
                <tr>
                  <td class=" "><?php echo $d->vcDocNum; ?></td>
				  <td class=" "><?php echo $d->dtDate; ?></td>
				  <td class=" "><?php echo $d->vcWalletNameFrom; ?></td>
				  <td class=" "><?php echo $d->vcWalletNameTo; ?></td>
				  <td class=" " align="right"><?php echo number_format($d->intValue,'0'); ?></td>
				  <td align="center">
				  <?php if($crudaccess->intRead==1) { ?>
				  <i class="fa fa-search <?php echo $usericon;?>" data-toggle="modal" onclick="edit('<?php echo $d->intID; ?>','<?php echo str_replace("'","\'",$d->vcDocNum); ?>'
				  ,'<?php echo str_replace("'","\'",date('m/d/Y',strtotime($d->dtDate))); ?>','<?php echo str_replace("'","\'",$d->intWalletFrom); ?>','<?php echo str_replace("'","\'",$d->intWalletTo); ?>'
				  ,'<?php echo str_replace("'","\'",$d->intValue); ?>','<?php echo str_replace("'","\'",$d->vcRemarks); ?>')" 
				  data-target="#modal-addedit" aria-hidden="true"></i>
				  <?php }else{ ?>
				  locked
				  <?php } ?>
				  </td>
                </tr>
				<?php 
				}
				?>
                </tbody>

              </table>
			</div>
	</div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  

  <!-- Control Sidebar -->
  <?php $this->load->view('controlsidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  <?php $this->load->view('T2'); ?>
<script>
	function checkform()
	{
		var DocDate = $("#DocDate").val();
		var period=0;
		$.ajax({ 
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/general_function/cekpostperiod?", 
			data: "DocDate="+DocDate+"", 
			cache: true, 
			success: function(data){ 
				period=data;
			},
			async: false
		});
		if(period!=1)
		{
			alert('This Period is locked');
			return false;
		}
		else
		{
			return 1;
		}
	}
	function edit(id,docnum,docdate,walletfrom,walletto,val,remarks)
	{
		$("#tittleEdit").show();
		$("#tittleAdd").hide();
		$("#addbutton").hide();
		document.getElementById("DocNum").value = docnum;
		$( "#DocDate" ).datepicker( "setDate", docdate);
		document.getElementById("WalletFrom").value = walletfrom;
		document.getElementById("WalletTo").value = walletto;
		document.getElementById("Value").value = val;
		document.getElementById("Remarks").value = remarks;
		
		
		$('#DocNum').attr("disabled", true);
		$('#DocDate').attr("disabled", true);
		$('#WalletFrom').attr("disabled", true);
		$('#WalletTo').attr("disabled", true);
		$('#Value').attr("disabled", true);
		$('#Remarks').attr("disabled", true);
		
	}
	function add()
	{
		 $("#tittleAdd").show();
		 $("#tittleEdit").hide();
		 $("#addbutton").show();
		 document.getElementById("type").value = 'add';
		 document.getElementById("DocNum").value = '<?php echo $DocNum;?>';
		 $( "#DocDate" ).datepicker( "setDate", '<?php echo $DocDate;?>');
		 $('#WalletFrom option:first-child').attr("selected", "selected");
		 $('#WalletTo option:first-child').attr("selected", "selected");
		 document.getElementById("Value").value = '';
		 document.getElementById("Remarks").value = '';
		 $('#DocNum').attr("disabled", false);
		 $('#DocDate').attr("disabled", false);
		 $('#WalletFrom').attr("disabled", false);
		 $('#WalletTo').attr("disabled", false);
		 $('#Value').attr("disabled", false);
		 $('#Remarks').attr("disabled", false);
		 
	}
  $(function () {
		
	//ubah inputan ke format datepiceker
	$('#DocDate').datepicker({
      autoclose: true
    });
	
    $("#example1").DataTable({
		//dom: 'Bfrtip',
		"oLanguage": {
		  "sSearch": "Search:"
		},
		'iDisplayLength': 10,
		//"sPaginationType": "full_numbers",
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
		  "sSwfPath": ""
		},
		dom: 'Blfrtip',
		"aaSorting": [],
		buttons: [
		   {
			   extend: 'pdf',
			   footer: false,
		   },
		   {
			   extend: 'csv',
			   footer: false
			  
		   },
		   {
			   extend: 'excel',
			   footer: false
		   }         
		]  
	});
   
  });
  
 
</script>
</body>
</html>
